<?php
error_reporting(E_ALL ^ E_NOTICE);
header('Content-type: text/xml');
//database connection
include 'database_connection.php';

/*
 * Query to get data from the database. Here I am getting data from two different database.
 * 1) Messaging database 2) pmsi database.
 * from "messaging" database I am using "message" and "user_message" tables.
 * while from "pmsi" database I am using "guest" table only.
 */
//$query='SELECT m.`message_id`,m.`subject`,m.`body`,`sent`,g.guestname AS author_name,md.conversation_id,um.user_name AS guestname,md.ip,um.msg_for,g.changeflag AS pc_ip FROM messaging.message AS m JOIN messaging.msg_to_delivery AS md ON md.message_id=m.message_id JOIN pmsi.guest as g ON g.guestid=m.author_id JOIN messaging.user_message as um On md.conversation_id=um.conversation_id
//';
$query='SELECT * FROM (SELECT msg2d.*,g.guestname,g.changeflag,g.sharestatus FROM `messaging_msg_to_delivery` as msg2d LEFT JOIN pmsi_guest as g on g.guestid=msg2d.guestid) as a
LEFT JOIN (SELECT umsg.user_id,umsg.user_name,umsg.is_read,umsg.is_deleted,umsg.msg_for,umsg.delivery_status,umsg.message_id, msg.subject, msg.body, msg.sent,u.user_display_name  AS author_name
FROM messaging_user_message AS umsg, messaging_message AS msg, dashboard_user AS u
WHERE msg.message_id = umsg.message_id AND umsg.is_deleted=0 AND u.user_id = msg.author_id) AS b on b.message_id=a.message_id';

//echo $query;die();


//execute the query and get results
$result= mysql_query($query);


//for the list of message we will do the iterations to send message one after another
while($user_message = mysql_fetch_assoc($result))
{

	//print_r($user_message);
	if ($user_message['msg_for']=="guest")
	{

		//Check message validity
		if($user_message['changeflag']==0)
		{

			$str="message : <root><msg>";
			//$str="<root><msg>";

			$str.="<id>dv".$user_message['conversation_id']."</id>";
			$str.= "<sub>".$user_message['subject']."</sub>";
			$str.= "<to>".$user_message['guestname']."</to>";
			$str.= "<from>".$user_message['author_name']."</from>";
			$str.= "<body>";
			$body= $user_message['body'];
			$body = str_replace("\r", "&#13;",$body);
			$body = str_replace("\n", "&#13;",$body);
			$str.= $body;
			$str.= "</body>";
			$str.= "<datetime>".date("YmdHis",strtotime($user_message['sent']))."</datetime>";
			$str.= "<token>".$user_message['token']."</token>";
			$str.= "</msg></root>";

			echo  $address = $user_message['ip'];
			$address;
			//$address = "172.16.0.93";
			$port = 7008;
			//echo $controller_ip."";





			/*
			 * Send XML to socket of the controller.
			 */

			// don't timeout!
			set_time_limit(0);

			/* Create a TCP/IP socket. */
			$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			if ($socket === false) {
				echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
			} else {
				echo "socket successfully created.\n";
			}

			echo "Attempting to connect to '$address' on port '$port'...";
			$sock_result = socket_connect($socket, $address, $port);

			if ($sock_result === false) {
				echo "socket_connect() failed.\nReason: ($sock_result) " . socket_strerror(socket_last_error($socket)) . "\n";
			} else {
				echo "successfully connected to $address.\n";
			}

			//$msg="Hello World\n";
			socket_write($socket, $str, strlen($str));

			echo "Closing socket...";
			socket_close($socket);

		}
		else if ($user_message['changeflag']==1)
		{
			/*
			 * Check out is pending. We will have to remove message from the queue.
			 * Write code here to do the same.
			 * Here we call a php page from CLi and we will pass required paramater to
			 * delete the message from the queue
			 */
			$del_cmd="/usr/bin/php -q /var/www/html/backend/api_dashboard/phpfiles/delete_message.php conversation_id=".$user_message['conversation_id'];
			exec($del_cmd);

		}
		else if ($user_message['changeflag']==-1)
		{
			/*
			 * Check in is pending we will have to keep message in queue.
			 * Write code here to do the same.
			 */

		}
	}


}

?>
