<?php
/*
 * Made By : Dharmendra/Ravindra
 * Date : 18 Mar 2013
 * Version : 1.0
 * =========================================================
 * This API will be called by either iOS application or JAVA Packeage. Both of the application will give
 * Read acknowledgement to this API by passing parameter "token". It will set promo_msg_status table
 * field "is_read" to 1. As it done promopt a message is as follow or on error prints "Error"
 */

//Include database connection file.
include 'database_connection.php';

//assign varialble with the parameter sent by iOS or Java application.
$token=$_REQUEST['token'];

if ($token!="")
{
	$query = "UPDATE promo_msg_status SET is_read = '1' WHERE token =".$token;
	$result = mysql_query($query);

	if (!$result)
	{
		//Getting error while try to update field of the database
		echo "Error";
	}
	else
	{
		//Field updated now show success msg.
		echo "Read status changed successfully!!";
	}

	// Job done close Database Connection
	mysql_close($con);
}
?>
