<?php
error_reporting(E_ALL ^ E_NOTICE);
logtofile("Promo Message Send to iPad Started");

set_time_limit(0);
date_default_timezone_set('Asia/Kolkata');


function encrypt($str,$key){
	$result="";
	for ($i=0;$i<strlen($str); $i++) {

		$char = substr($str, $i, 1);
		$keychar = substr($key, ($i % strlen($key)) - 1, 1);
		$char = chr(ord($char) + ord($keychar));
		$result.=$char;
	}
	return base64_encode($result);
}

function logtofile($logmsg) {
	$fp = fopen("/var/log/dvc/promo_msg_to_ipad.log", "a");
	fputs($fp, $logmsg);
	fclose($fp);
}

function convert_special_char($strToConvert)
{
	$strToConvert = str_replace("&", "&amp;",$strToConvert);
	$strToConvert = str_replace("\r", "&#13;",$strToConvert);
	$strToConvert = str_replace("\n", "&#13;",$strToConvert);
	$strToConvert = str_replace(":", "col;",$strToConvert);
	$strToConvert = str_replace("<", "&lt;",$strToConvert);
	$strToConvert = str_replace(">", "&gt;",$strToConvert);
	$strToConvert = str_replace("=", "eql;",$strToConvert);
	$strToConvert = str_replace("\"", "&quot;",$strToConvert);
	$strToConvert = str_replace("'", "&#39;",$strToConvert);
	$strToConvert = str_replace("...", "&hellip;",$strToConvert);

	return $strToConvert;
}
if (!is_dir('/var/log/dvc')) {
	mkdir('/var/log/dvc');
}

//get promo message thread id;
$my_pid = getmypid();
logtofile("My Process ID : ".$my_pid);

//database connection
include 'database_connection.php';






while (true)
{
	logtofile("\n I am in while a ");
	/*
	 * Query to get data from the database. Here I am getting data from two different database.
	 * 1) Messaging database 2) pmsi database.
	 * from "messaging" database I am using "message" and "user_message" tables.
	 * while from "pmsi" database I am using "guest" table only.
	 */
	 
	//$query='select msg.*,ds.MAC,guest.guestid,guest.guestname,guest.changeflag,p.conversation_id from (select pmd.*,pm.title,pm.msg_body,pm.extrainformation,pm.promo_page_url,pm.promo_msg_img,pm.end_date, pm.start_date from promo_msg_to_deliver as pmd, promo_msg as pm where pm.promo_msg_id=pmd.promo_msg_id) as msg,(select roomno, guestid, guestname, changeflag,pc_ip from pmsi_guest where changeflag=0) as guest,(select pms.conversation_id,pms.guest_id from promo_msg_status as pms,promo_msg_to_deliver as promo where pms.guest_id=promo.guest_id ) as p, digivalet_status as ds where ds.pc_ip=msg.ip and pc=1 and guest.guestid=msg.guest_id and  p.guest_id=msg.guest_id';
	$query='SELECT msg. * , ds.MAC, guest.guestid, guest.guestname, guest.changeflag,pms.is_deliver FROM (SELECT pmd. * , pm.title, pm.msg_body, pm.extrainformation, pm.promo_page_url, pm.promo_msg_img, pm.end_date, pm.start_date FROM promo_msg_to_deliver AS pmd, promo_msg AS pm WHERE pm.promo_msg_id = pmd.promo_msg_id) AS msg, (SELECT roomno, guestid, guestname, changeflag, pc_ip FROM pmsi_guest WHERE changeflag =0) AS guest, digivalet_status AS ds, promo_msg_status AS pms WHERE ds.pc_ip = msg.ip AND pc =1 AND guest.guestid = msg.guest_id AND pms.token = msg.token ORDER BY msg.promo_msg_id DESC';
	logtofile("Main Query : ".$query."\n");
	 
	//echo $query;die();


	//execute the query and get results
	$result="";
	$result= mysql_query($query);


	//$user_message="";
	$port = 7008;

	//for the list of message we will do the iterations to send message one after another
	while($user_message = mysql_fetch_assoc($result))
	{

		logtofile("I am in second while\n");
		//print_r($user_message);die('here');
		$current_time = time();
		$current = date('Y-m-d H:i:s', $current_time);
		//logtofile("End Date : ".$user_message['end_date'].", Current Time : ".$current);
		if($user_message['end_date']< $current && $user_message1['is_updated']!='1' && $user_message1['is_deliver']=='1'  ){
			$query="delete from promo_msg_to_deliver where token=".$user_message['token'];
			logtofile($query. "end date\n");
			//    echo $query;
			$result = mysql_query($query);



		}
		//Check message validity
		if($user_message['changeflag']==0)
		{
			$sql="";
			$sql="SELECT * FROM digivalet_status WHERE room_no='".$user_message['room_no']."' and pc_ip='".$user_message['ip']."'";
			logtofile($sql. "\n");

			//execute the query and get results
			//$res4msql="";
			$res= mysql_query($sql);
			//$digivale_status="";
			while($digivale_status = mysql_fetch_assoc($res))
			{
				/*
				 * checking controller and itouch device is available or not if both of them are available then we will make
				 * connection on socket and will send msg otherwise we will ignore it until we will not get both of them. It will save our time.
				 */

				//print_r($digivale_status); die();
				$current_time = time();
				$current = date('Y-m-d H:i:s', $current_time);
				$end_date= $user_message['end_date'];
				//logtofile("End Date : ".$user_message['end_date'].", Current Time : ".$current);
				if($digivale_status["pc"]==1 && trim($user_message['guestname'])!=""  )
				{
					logtofile($user_message['room_no']."====+++I am start preparing xml for ipad room no=== '$end_date':::::'$current'\n");
					 
					$str="";
					$str="promotionalmessage:<root><msg>";
					//$str="<root><msg>";

					$str.="<id>".$user_message['promo_msg_id']."</id>";
					$str.= "<subject>";
					$subject = convert_special_char($user_message['title']);
					//$subject = str_replace("&", "&amp;",$subject);
					//$subject = str_replace("\r", "&#13;",$subject);
					//$subject = str_replace("\n", "&#13;",$subject);
					//$subject = str_replace(":", "col;",$subject);
					$str.= $subject;
					$str.= "</subject>";

					$str.= "<description>";
					$description= convert_special_char($user_message['msg_body']);
					//$description = str_replace("&", "&amp;",$description);
					//$description = str_replace("\r", "&#13;",$description);
					//$description = str_replace("\n", "&#13;",$description);
					//$description = str_replace(":", "col;",$description);
					$str.= $description;
					$str.= "</description>";

					$str.= "<datetime>".date("ymdHi",strtotime($user_message['start_date']))."</datetime>";
					$str.= "<expirytime>".date("ymdHi",strtotime($user_message['end_date']))."</expirytime>";
					$str.= "<token>".$user_message['token']."</token>";

					$thumb_img_url = convert_special_char($user_message['promo_msg_img']);

					//$str.= "<thumbnailimageurl>".$user_message['promo_msg_img']."</thumbnailimageurl>";
					$str.= "<thumbnailimageurl>".$thumb_img_url."</thumbnailimageurl>";

					$str.= "<url>".convert_special_char($user_message['promo_page_url'])."</url>";

					$str.= "<extrainformation>";
					$extrainfo = convert_special_char($user_message['extrainformation']);
					//$extrainfo = str_replace("&", "&amp;",$extrainfo);
					//$extrainfo = str_replace("\r", "&#13;",$extrainfo);
					//$extrainfo = str_replace("\n", "&#13;",$extrainfo);
					//$extrainfo = str_replace(":", "col;",$extrainfo);
					$str.= $extrainfo;
					$str.= "</extrainformation>";

					$str.= "</msg></root>";
					logtofile("+++ XML Prepared\n");
					$address="";

					//Prepare key for encryption. Do remember mac address should be in lower char.
					$room_no=$digivale_status['room_no'];
					$mac_address=strtolower($digivale_status['MAC']);
					$key=$room_no.$mac_address;
					logtofile("Encrypttion Key : ".$key."\n");

					//encrypt message before sending to iPad
					$encrypt_key=encrypt($str,$key);
					logtofile("Encrypted Message : ".$encrypt_key);
					$encrypt_key=$encrypt_key."\n";  //add new line character for socket.

					$address = $user_message['ip'];

					logtofile("Controller IP : ".$address. "\n");

					/*
					 * Send XML to socket of the controller.
					 */

					// don't timeout!
					//set_time_limit(0);

					/* Create a TCP/IP socket. */
					$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
					if ($socket === false) {
						//echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
						logtofile("\nsocket_create() failed: reason: " . socket_strerror(socket_last_error()));
					} else {
						//echo "socket successfully created.\n";
						logtofile("\nsocket successfully created.");
					}

					logtofile( "Attempting to connect to '$address' on port '$port'...\n");
					$sock_result = socket_connect($socket, $address, $port);

					if ($sock_result === false) {
						//echo "socket_connect() failed.\nReason: ($sock_result) " . socket_strerror(socket_last_error($socket)) . "\n";
						logtofile("socket_connect() failed.\nReason: ($sock_result) " . socket_strerror(socket_last_error($socket))."\n" );
					} else {
						//echo "successfully connected to $address.\n";
						logtofile("successfully connected to $address.\n");
					}

					//$msg="Hello World\n";
					socket_write($socket, $encrypt_key, strlen($encrypt_key));

					logtofile("Closing socket...\n");
					socket_close($socket);

					unset($encrypt_key);
					unset($address);
				} //end of if of digivalet status checking
				else if($digivale_status["pc"]==1 && trim($user_message['guestname'])!="" && $user_message['is_updated']=='1' ){        ///This code is for resend of Delete Messages
					logtofile("+++I am start preparing xml for Deleted messages '$end_date':::::'$current'\n");
					 
					$str="";
					$str="promotionalmessage:<root><msg>";
					//$str="<root><msg>";

					$str.="<id>".$user_message['promo_msg_id']."</id>";
					$str.= "<subject>";
					$subject = convert_special_char($user_message['title']);
					//$subject = str_replace("&", "&amp;",$subject);
					//$subject = str_replace("\r", "&#13;",$subject);
					//$subject = str_replace("\n", "&#13;",$subject);
					//$subject = str_replace(":", "col;",$subject);
					$str.= $subject;
					$str.= "</subject>";

					$str.= "<description>";
					$description= convert_special_char($user_message['msg_body']);
					//$description = str_replace("&", "&amp;",$description);
					//$description = str_replace("\r", "&#13;",$description);
					//$description = str_replace("\n", "&#13;",$description);
					//$description = str_replace(":", "col;",$description);
					$str.= $description;
					$str.= "</description>";

					$str.= "<datetime>".date("ymdHi",strtotime($user_message['start_date']))."</datetime>";
					$str.= "<expirytime>".date("ymdHi",strtotime($user_message['end_date']))."</expirytime>";
					$str.= "<token>".$user_message['token']."</token>";

					$thumb_img_url = convert_special_char($user_message['promo_msg_img']);

					//$str.= "<thumbnailimageurl>".$user_message['promo_msg_img']."</thumbnailimageurl>";
					$str.= "<thumbnailimageurl>".$thumb_img_url."</thumbnailimageurl>";

					$str.= "<url>".convert_special_char($user_message['promo_page_url'])."</url>";

					$str.= "<extrainformation>";
					$extrainfo = convert_special_char($user_message['extrainformation']);
					//$extrainfo = str_replace("&", "&amp;",$extrainfo);
					//$extrainfo = str_replace("\r", "&#13;",$extrainfo);
					//$extrainfo = str_replace("\n", "&#13;",$extrainfo);
					//$extrainfo = str_replace(":", "col;",$extrainfo);
					$str.= $extrainfo;
					$str.= "</extrainformation>";

					$str.= "</msg></root>";
					logtofile("+++ XML Prepared\n");
					$address="";

					//Prepare key for encryption. Do remember mac address should be in lower char.
					$room_no=$digivale_status['room_no'];
					$mac_address=strtolower($digivale_status['MAC']);
					$key=$room_no.$mac_address;
					logtofile("Encrypttion Key : ".$key."\n");

					//encrypt message before sending to iPad
					$encrypt_key=encrypt($str,$key);
					logtofile("Encrypted Message : ".$encrypt_key);
					$encrypt_key=$encrypt_key."\n";  //add new line character for socket.

					$address = $user_message['ip'];

					logtofile("Controller IP : ".$address. "\n");

					/*
					 * Send XML to socket of the controller.
					 */

					// don't timeout!
					//set_time_limit(0);

					/* Create a TCP/IP socket. */
					$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
					if ($socket === false) {
						//echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
						logtofile("\nsocket_create() failed: reason: " . socket_strerror(socket_last_error()));
					} else {
						//echo "socket successfully created.\n";
						logtofile("\nsocket successfully created.");
					}

					logtofile( "Attempting to connect to '$address' on port '$port'...\n");
					$sock_result = socket_connect($socket, $address, $port);

					if ($sock_result === false) {
						//echo "socket_connect() failed.\nReason: ($sock_result) " . socket_strerror(socket_last_error($socket)) . "\n";
						logtofile("socket_connect() failed.\nReason: ($sock_result) " . socket_strerror(socket_last_error($socket))."\n" );
					} else {
						//echo "successfully connected to $address.\n";
						logtofile("successfully connected to $address.\n");
					}

					//$msg="Hello World\n";
					socket_write($socket, $encrypt_key, strlen($encrypt_key));

					logtofile("Closing socket...\n");
					socket_close($socket);

					unset($encrypt_key);
					unset($address);
				}
			}
			unset($sql);
			unset($res4msql);
			unset($digivale_status);
		}
		else if ($user_message['changeflag']==-1)
		{
			/*
			 * Check out is pending. We will have to remove message from the queue.
			 * Write code here to do the same.
			 * Here we call a php page from CLi and we will pass required paramater to
			 * delete the message from the queue
			 */
			$del_cmd="";
			logtofile("Got user checkout is pending! Deleting conversation message : ".$user_message['conversation_id']."\n");

			$del_cmd="/usr/bin/php -q /var/www/html/backend/api_dashboard/phpfiles/delete_message_cli.php conversation_id=".$user_message['conversation_id'];
			exec($del_cmd);

			unset($del_cmd);

		}
		else if ($user_message['changeflag']==1)
		{
			/*
			 * Check in is pending we will have to keep message in queue.
			 * Write code here to do the same.
			 */
			logtofile("Got user checkin is pending! I am keeping message in pending list. : ".$user_message['conversation_id']);
		}
		 

	} //end of while

	$query1="";
	$query1='SELECT msg. * , ds.MAC, guest.guestid, guest.guestname, guest.changeflag,pms.is_deliver FROM (SELECT pmd. * , pm.title, pm.msg_body, pm.extrainformation, pm.promo_page_url, pm.promo_msg_img, pm.end_date, pm.start_date FROM promo_msg_to_deliver AS pmd, promo_msg AS pm WHERE pm.promo_msg_id = pmd.promo_msg_id) AS msg, (SELECT roomno, guestid, guestname, changeflag, pc_ip FROM pmsi_guest WHERE changeflag =0) AS guest, digivalet_status AS ds, promo_msg_status AS pms WHERE ds.pc_ip = msg.ip AND pc=0 and guest.guestid = msg.guest_id AND pms.token = msg.token ORDER BY msg.promo_msg_id DESC';
	$result1= mysql_query($query1);
	while($user_message1 = mysql_fetch_assoc($result1))
	{
		$current_time = time();
		$current = date('Y-m-d H:i:s', $current_time);
		// logtofile("End Date : ".$user_message1['end_date'].", Current Time : ".$current);
		if($user_message1['end_date']< $current && $user_message1['is_updated']!='1' && $user_message1['is_deliver']=='1'  ){
			$query2="delete from promo_msg_to_deliver where token=".$user_message1['token'];
			logtofile($query2. "is_updateddddddd\n");
			//    echo $query;
			$result3 = mysql_query($query2);
		}
	}


	$sql="delete from promo_msg_to_deliver where `guest_id` not in (select guestid from pmsi_guest)";
	mysql_query($sql);
	logtofile($query2. "guest_change\n");




	unset($result);
	unset($user_message);
	unset($query);

	logtofile("Going to sleep for 30 Seconds.".date('h:i:s'). "\n");

	//sleep for 10 seconds
	sleep(30);

	//start again
	logtofile("Wake up at : ".date('h:i:s'). "\n");

} //end of infinite while loop

//NOTE :  We will have to generate $key using  roomNo+mac_address . Mac address should be in lowercase.

?>
