$(function() {
	updateStatus();
	commentSubmit();
	commentOpen();
	deleteUpdate();
	deleteComment();
	likePost();
	markInappro();
	tooltipShow();
});
function likePost() 
{
	$("[class^='like']").click(function() {
				var ID = $(this).attr("id");
				var dataString = 'msg_id='+ID;
				$.ajax( {
					type : "POST",
					url : "like_message_ajax",
					data : dataString,
					success : function(data) {
						if(data==1)
						{
							if ($.trim($(".like"+ID).html())=="Like") 
							{
								$(".like"+ID).html("Unlike");
							} 
							else if($.trim($(".like"+ID).html())=="Unlike")
							{
								$(".like"+ID).html("Like");
							}
							
							$.ajax( {
								type : "POST",
								url : "like_data",
								data : dataString,
								success : function(data) {
									$("#totalLikes"+ID).text(jQuery.parseJSON(data).length);
								}
							});
						}
						return false;
					}
				});
			});
}
function deleteComment() {
	$('.stcommentdelete')
			.live(
					"click",
					function() {
						var ID = $(this).attr("id");
						var __userId = $(this).attr('data-spider');
						var dataString = 'com_id='+ID+'&user_id='+__userId;
						var msg_id = $(this).attr('dir');
						
						jConfirm(
								'Are you sure you want to delete this thread?',
								'Confirmation Dialog',
								function(r) {
									if (r == true) {
										$
												.ajax( {
													type : "POST",
													url : "delete_comment_ajax",
													data : dataString,
													cache : false,
													success : function(html) {
														$("#stcommentbody" + ID)
																.slideUp();

														$
																.ajax( {
																	type : "POST",
																	url : "comment_data",
																	data : 'msg_id='
																			+ msg_id,
																	success : function(
																			data) {
																		$(
																				"#totalComments"
																						+ msg_id)
																				.text(
																						jQuery
																								.parseJSON(data).length);
																	}
																});
													}
												});
									}
								});
					});
}
function deleteUpdate() {
	$('.stdelete').live(
			"click",
			function() {
				var ID = $(this).attr("id");
				var dataString = 'msg_id='+ID+'&user_id='+$(this).attr('dir');
				jConfirm('Are you sure you want to delete this thread?',
						'Confirmation Dialog', function(r) {
							if (r == true) {
								$.ajax( {
									type : "POST",
									url : "delete_message_ajax",
									data : dataString,
									cache : false,
									success : function(html) {
										$("#stbody" + ID).slideUp();
									}
								});
							}
						});
			});
}
function commentOpen() {
	$('.commentopen').live("click", function() {
		var ID = $(this).attr("id");
		$("#commentbox" + ID).slideToggle('slow');
		return false;
	});
}
function commentSubmit() {
	$('.comment_button').live(
			"click",
			function() {

				var ID = $(this).attr("id");

				var comment = $("#ctextarea" + ID).val();
				var dataString = 'comment=' + comment + '&msg_id=' + ID;

				if (comment == '') {
					jAlert("Please Enter Comment Text", "Alert");
				} else {
					$.ajax( {
						type : "POST",
						url : "comment_ajax",
						data : dataString,
						cache : false,
						success : function(html) {
							$("#commentbox" + ID).after(html); // $("#commentload"
																// +
																// ID).before(html);
							$("#ctextarea" + ID).val('');
							$("#ctextarea" + ID).focus();
							$.ajax( {
								type : "POST",
								url : "comment_data",
								data : dataString,
								success : function(data) {
									$("#totalComments" + ID).text(
											jQuery.parseJSON(data).length);
								}
							});
						}
					});
				}
				return false;
			});
}
function updateStatus() {
	$(".update_button")
			.click(
					function() {
						var updateval = $("#update").val();
						var dataString = 'update=' + updateval;
						if (updateval == '') {
							jAlert("Please Enter Some Text", "Alert");// alert("Please
																		// Enter
																		// Some
																		// Text");
						} else {
							$("#flash").show();
							$("#flash").fadeIn(400).html('Loading Update...');
							$
									.ajax( {
										type : "POST",
										url : "message_ajax",
										data : dataString,
										cache : false,
										success : function(html) {
											$("#flash").fadeOut('slow');
											$("#content").prepend(html);
											get_m_id();
											$("#update").val('');
											$("#update").focus();
											$("#stexpand").oembed(updateval);
											
											$("[class^='likeAjax']")
													.click(
															function() {
																var ID = $(this)
																		.attr(
																				"id");
																var dataString = 'msg_id='
																		+ ID;

																$
																		.ajax( {
																			type : "POST",
																			url : "like_message_ajax",
																			data : dataString,
																			success : function(
																					data) {
																				if ($(
																						".likeAjax"
																								+ ID)
																						.html() == "Like") {
																					$(
																							".likeAjax"
																									+ ID)
																							.html(
																									"Unlike");
																				} else {
																					$(
																							".likeAjax"
																									+ ID)
																							.html(
																									"Like");
																				}
																				$
																						.ajax( {
																							type : "POST",
																							url : "like_data",
																							data : dataString,
																							success : function(
																									data) {
																								$(
																										"#totalLikes"
																												+ ID)
																										.text(
																												jQuery
																														.parseJSON(data).length);
																							}
																						});
																			}
																		});
															});
										}
									});
						$("#post_on_same_page").val(1);
						}

						return false;
					});
}
$('.detailCommentOpenAjax').click(function(event) {
	var ID = $(this).attr("id");
	$(".detailCommentBox" + ID).slideToggle('slow');
	event.preventDefault();
	return false;
});
function changeLikeStatus(ID, text)
{
	$(".likeAjax"+ID).html(text);
	$(".like"+ID).html(text);
}
$('.detail_comment_button').live("click", function() {

	var ID = $(this).attr("id");

	var comment = $(".detailCTextArea" + ID).val();
	var dataString = 'comment=' + comment + '&msg_id=' + ID;
	
	if (comment == '') 
	{
		alert("Please Enter Comment Text");
	} 
	else 
	{
		$.ajax( {
			type : "POST",
			url : "comment_ajax",
			data : dataString,
			cache : false,
			success : function(html) {
				$(".detailCommentBox" + ID).after(html);
				$("#commentbox" + ID).after(html);
				$(".detailCTextArea" + ID).val('');
				$(".detailCTextArea" + ID).focus();
				$.ajax({
					type : "POST",
					url : "comment_data",
					data : dataString,
					success : function(data) 
					{
						var tComments = "totalComments"+ID; 
						$("[id^='"+tComments+"']").text(jQuery.parseJSON(data).length);
					}
				});
			}
		});
	}
	return false;
});
function markInappro()
{
	$("[id^='markInappro']").click(function(){
		var __mode = $(this).attr('dir');
		var __id = $(this).attr('data-set');
		var dataString = 'mode='+__mode+'&id='+__id;
		var __dataGroup =  $(this).attr('data-group');
		$.ajax({
			type : "POST",
			url : "mark_as_inappropriate",
			cache: false,
			data : dataString,
			success : function(data) 
			{
				if(data==1)
				{
					if(__mode=="comment")
					{
						if(__dataGroup=="ap")
						{
							$("#stcommentbody"+__id).css("opacity", "0.2");
							$("#markInappro"+__id).attr("data-group", "notap");
							$("#markInappro"+__id).attr("title", "Mark As Appropriate");
						}
						else
						{
							$("#stcommentbody"+__id).css("opacity", "1");
							$("#markInappro"+__id).attr("data-group", "ap");
							$("#markInappro"+__id).attr("title", "Mark As Inappropriate");
						}
					}
					else if(__mode=="post")
					{
						if(__dataGroup=="ap")
						{
							$("#stbody"+__id).css("opacity", "0.2");
							$("#markInapproPost"+__id).attr("data-group", "notap");
							$("#markInapproPost"+__id).attr("title", "Mark As Appropriate");
						}
						else
						{
							$("#stbody"+__id).css("opacity", "1");
							$("#markInapproPost"+__id).attr("data-group", "ap");
							$("#markInapproPost"+__id).attr("title", "Mark As Inappropriate");
						}
					}
				}
			}
		});
	});
}
function tooltipShow()
{
	$("[id^='totalComments']").mouseover(
	function(){
		$(this).tooltip('Total Number of Comments');
	}).mouseout(
	function(){
		$(this).removeTooltip();
	});
	$("[id^='totalLikes']").mouseover(
			function(){
				$(this).tooltip('Total Number of Likes');
			}).mouseout(
			function(){
				$(this).removeTooltip();
	});
	$("[id^='markInappro']").mouseover(
			function(){
				$(this).tooltip($(this).attr('title'));
			}).mouseout(
			function(){
				$(this).removeTooltip();
	});
	$("[class^='stdelete']").mouseover(
			function(){
				$(this).tooltip('Delete this Post');
			}).mouseout(
			function(){
				$(this).removeTooltip();
	});
	$("[class^='stcommentdelete']").mouseover(
			function(){
				$(this).tooltip('Delete this Comment');
			}).mouseout(
			function(){
				$(this).removeTooltip();
	});
	
}
