function JT_init(){
	       $("a.jTip")
		   .hover(function()
			{
			   JT_show(this.href,this.id,this.name);
			   
			},
			   	function()
			   	{
					$("[id^='postNotificationDetailBox']").click(function(e){
							e.preventDefault();
					});
					$("[class^='detailLike']").click(function(){
						var ID = $(this).attr("id");
						var dataString = 'msg_id='+ID;
						$.ajax({
							type : "POST",
							url : "like_message_ajax",
							data : dataString,
							success : function(data) 
							{
								if($(".detailLike"+ID).html()=="Like")
								{
									$(".detailLike"+ID).html("Unlike");
									changeLikeStatus(ID, "Unlike");
								}
								else
								{
									$(".detailLike"+ID).html("Like");
									changeLikeStatus(ID, "Like");
								}
								$.ajax({
									type : "POST",
									url : "like_data",
									data : dataString,
									success : function(data) 
									{
										$(".detailTotalLikes"+ID).text(jQuery.parseJSON(data).length);
										$("#totalLikes"+ID).text(jQuery.parseJSON(data).length);
									}
								});
							}
						});
					});
				// delete comment
				$('.stcommentdelete').live(
						"click",
						function() {
							var ID = $(this).attr("id");
							var dataString = 'com_id=' + ID;
							var msg_id = $(this).attr('dir');
							jConfirm(
									'Are you sure you want ot delete this thread?',
									'Confirmation Dialog', function(r) {
										if (r == true) {
											$.ajax( {
												type : "POST",
												url : "delete_comment_ajax",
												data : dataString,
												cache : false,
												success : function(html) {
													$(".detailCommentBody" + ID).slideUp();
													$("#stcommentbody" + ID).slideUp();
													
													$.ajax({
														type : "POST",
														url : "comment_data",
														data : 'msg_id='+msg_id,
														success : function(data) 
														{
															$(".totalComments"+msg_id).text(jQuery.parseJSON(data).length);
															$("#totalComments"+msg_id).text(jQuery.parseJSON(data).length);
														}
													});
												}
											});
										}
									});
						});
					
					/*Scroll*/
					$('.feedDetail').customScroll({ /* options */ });
					/**/
					/*Close Detail*/
					$('#JT').mouseleave(function(){
						$('#JT').remove();
					});
					/*$("#closeDetail").click(function(){
			   			$('#JT').remove();
			   		});*/
					$(".setUserStatus").click(function(){
						var token = $(this).attr('id');
						var dataString = 'token='+token;
						$.ajax( {
							type : "POST",
							url : "set_user_status",
							data : dataString,
							cache : false,
							success : function(result) {
								var statusText = $(".setUserStatus").text();
								if(statusText=="Activate")
								{
									$(".setUserStatus").text('De-Activate');
								}
								else if(statusText=="De-Activate")
								{
									$(".setUserStatus").text('Activate');
								}
								else
								{
									$(".setUserStatus").text('Error');
								}
								return false;
							}
						});
					});
					$('.detailCommentOpenAjax').click(function(event) {
						var ID = $(this).attr("id");
						$(".detailCommentBox" + ID).slideToggle('slow');
						event.preventDefault();
						return false;
					});
					submitOnEnter();
			   		/**/
			   	})
           .click(function(){return false});	   
}

function JT_show(url,linkId,title)
{
	if(title == false)title="&nbsp;";
	var de = document.documentElement;
	var w = self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	var hasArea = w - getAbsoluteLeft(linkId);
	var clickElementy = getAbsoluteTop(linkId) - 3; //set y position
	
	var queryString = url.replace(/^[^\?]+\??/,'');
	var params = parseQuery( queryString );
	if(params['width'] === undefined){params['width'] = 600};
	if(params['link'] !== undefined){
	$('#' + linkId).bind('click',function(){window.location = params['link']});
	//$('#' + linkId).css('cursor','pointer');
	}
	
	if(hasArea>((params['width']*1)+75)){
		$("body").append("<div id='JT' style='width:"+params['width']*1+"px'><div id='JT_arrow_left'></div><div id='JT_copy'><div class='JT_loader'><div></div></div>");//right side
		var arrowOffset = getElementWidth(linkId) + 11;
		var clickElementx = getAbsoluteLeft(linkId) + arrowOffset; //set x position
	}else{
		$("body").append("<div id='JT' style='width:"+params['width']*1+"px'><div id='JT_arrow_right' style='left:"+((params['width']*1)+1)+"px'></div><div id='JT_close_right'>"+title+"</div><div id='JT_copy'><div class='JT_loader'><div></div></div>");//left side
		var clickElementx = getAbsoluteLeft(linkId) - ((params['width']*1) + 15); //set x position
	}
	
	$('#JT').css({left: clickElementx+"px", top: clickElementy+"px"});
	$('#JT').show();
	
	$('#JT_copy').load(url);
}

function getElementWidth(objectId) {
	x = document.getElementById(objectId);
	return x.offsetWidth;
}

function getAbsoluteLeft(objectId) {
	// Get an object left position from the upper left viewport corner
	o = document.getElementById(objectId)
	oLeft = o.offsetLeft            // Get left position from the parent object
	while(o.offsetParent!=null) {   // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent    // Get parent object reference
		oLeft += oParent.offsetLeft // Add parent left position
		o = oParent
	}
	return oLeft
}

function getAbsoluteTop(objectId) {
	// Get an object top position from the upper left viewport corner
	o = document.getElementById(objectId)
	oTop = o.offsetTop            // Get top position from the parent object
	while(o.offsetParent!=null) { // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent  // Get parent object reference
		oTop += oParent.offsetTop // Add parent top position
		o = oParent
	}
	return oTop
}

function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

function blockEvents(evt) {
              if(evt.target){
              evt.preventDefault();
              }else{
              evt.returnValue = false;
              }
}
$(function(){
	JT_init();
});