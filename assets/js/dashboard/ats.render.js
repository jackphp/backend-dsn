//$(document).ready(function () {
//    
//});

// ----------siren object
var sirenBlower = new Object();
sirenBlower.isAlreadyOn = false;
sirenBlower.blowSiren = function () {
    //console.log('blowSiren called');
    if (sirenBlower.isAlreadyOn == false) {
        var audio = document.getElementsByTagName("audio")[0];
        audio.play();
        sirenBlower.isAlreadyOn == true;
    }
}


/*
* this function populates view for ATS
*/
var roomCounter = 0;
var profile;
function populateByView(thisRoom, cssClass, key, dashboard, gridview) {
    
    var roomNumber = thisRoom.roomName;
   // console.log("room  :" + thisRoom.roomName);

    //=============================populate grid view========================================
    // populate <tr></tr>
    var maintIco = thisRoom.isMaint == 1 ? "<input type='checkbox' checked class='switch medium'>" : "<input type='checkbox' class='switch medium'>";
    var controllerStatus = thisRoom.pc == 1 ? "<span class='icon-like grey icon-size2'></span><span class='forPrint'>Ok</span>" : "<img src='../../assets/images/dashboard/downthumb.png'>" + moment.unix(thisRoom.dvcts).format('DD MMM, h:mm a');
    var rentedStatus = thisRoom.isRented == 1 ? "<span class='icon-tick green icon-size2'></span><span class='forPrint'>Y</span>" : "<span class='icon-cross grey icon-size2'></span><span class='forPrint'>N</span>";
    var occupiedStatus = thisRoom.isOccupied == 1 ? "<span class='icon-home green icon-size2'></span>" : "<span class='icon-home grey icon-size2'></span>";
    var dndStatus = thisRoom.isDnd == 1 ? "<span class='icon-price-tag red icon-size2'></span>" : "<span class='icon-price-tag grey icon-size2'></span>";
    var battStatus="";var battColor="orange";
    try {
    if(thisRoom.battery=='charging')
    {
        battStatus = "<span class='forPrint'>Charging</span><p class='relative'><span style='width: 100%;height:18px;border-radius:3px; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.35) inset, 0 1px 1px rgba(255, 255, 255, 0.5);' class='dark-stripes animated large with-border grey glossy'><span class='icon-lightning with-padding'> Charging</span></span></p>";
    }
    else {
        battColor = thisRoom.battery > 50 ? 'grey' : thisRoom.battery > 20 ? 'orange' : 'red';

        if (thisRoom.battery > 10) {
            battStatus = "<span class='progress large with-tooltip' title ='" + moment.unix(thisRoom.batteryts).format('DD MMMM, h:mm:ss a') + "' style='width: 100%;height:20px'><span class='progress-bar " + battColor + "-gradient ' style='width: " + thisRoom.battery + "%;height:20px'><span class='downthumb.png'>" + thisRoom.battery + "%</span></span></span>";
        }
        else {
            battStatus = "<span class='progress large with-tooltip' title ='" + moment.unix(thisRoom.batteryts).format('DD MMMM, h:mm:ss a') + "' style='width: 100%;height:20px'><span class='progress-bar " + battColor + "-gradient ' style='width: " + thisRoom.battery + "%;height:20px'></span><span class='progress-text zero-percent' style='margin-left:20px'>" + thisRoom.battery + "%</span></span>";
        }
    }
    } catch(err){console.log(err);}
    
    try {
    var ipadStatus = thisRoom.iremote == 1 ? "<span class='icon-like grey icon-size2'></span>" : "<img src='../../assets/images/dashboard/downthumb.png'>" + moment.unix(thisRoom.ipadts).format('DD MMM, h:mm a');
    var muteAlert = isRoomMuteForUser(roomNumber) == 0 ? "<input type='checkbox' id='" + roomNumber + "Mute' checked class='switch medium muteAlert'>" : "<input type='checkbox' id='" + roomNumber + "Mute' class='switch medium muteAlert'>";
    //console.log(roomNumber+"->thisRoom.iremote:" + thisRoom.iremote + "; thisRoom.digivalet_status:" + thisRoom.digivalet_status + "; battColor:" + battColor + "; thisRoom.isMaint:" + thisRoom.isMaint);
    } catch(err){console.log(err);}
     
    var siren = '';
    var sirenClass='';
   
    try {
    profile = hotel.user.profile_name;    
    if ( getAlarmForThisProfile(thisRoom,battColor,profile) && thisRoom.isMaint == 0) {
        console.log('critical alert');
        siren = "<img src='../../assets/images/dashboard/alarm.gif' >";
        sirenClass = 'sirenForRoom';
        //console.log("room alert for :" + thisRoom.roomName);
        sirenBlower.blowSiren();
    }
   
    } catch(err){console.log(err);}
    
    if(thisRoom.battery_mute==0 && battColor == 'red')
    {
        console.log('battery alert');
         siren = "<img src='../../assets/images/dashboard/alarm.gif' >";
        sirenClass = 'sirenForRoom';
        //console.log("room alert for :" + thisRoom.roomName);
        sirenBlower.blowSiren();
    }
//    else {
//        console.log("NO room alert for :" + thisRoom.roomName);
//    }

    
    try{ 
    var umIcon = "<span class='forPrint'>N</span>";
    if (thisRoom.isMaint == 1)
        umIcon = "<div class='with-tooltip' title='This room is under maintenance'><span class='icon-warning icon-size2 orange'></span><span class='forPrint'>Y</span></div>";

    var alertRemark = "<span class='button icon-gear' id='actionButton"+roomCounter+"' onClick=\"openUserAction("+roomCounter+",'"+thisRoom.roomName+"',"+thisRoom.isMaint+",\'"+thisRoom.pc_ip+"\', "+thisRoom.battery_mute+")\">Action</span><span style='margin-left:15px'></span>";
    roomCounter++;
    //(thisRoom.isMaint == 1 && thisRoom.maint_user != '' )||
    //if ( sirenClass == 'sirenForRoom') {
        if (thisRoom.maint_remark != 'null' && thisRoom.maint_remark != '')
            alertRemark = alertRemark+"<span class='info-spot'><span class='icon-info-round'></span><span class='info-bubble' style='min-width:200px'>User: " +thisRoom.maint_user+"<br>Comment: "+ thisRoom.maint_remark +"</span></span>";
    //}

    var gridViewRow = "<tr class='" + cssClass + " floor" + key + " " + sirenClass + "' style='display:none'>" +
                                                                    "<td>" + siren + "</td><td>" + roomNumber + " " + thisRoom.roomDesc + "</td><td>" + controllerStatus + "</td><td>" + battStatus + "</td><td>" + thisRoom.iPad_App + "</td><td align='center'>" + rentedStatus + "</td><td align='center'>" + umIcon + "</td><td style='white-space:nowrap'>" + alertRemark + "</td></tr>";
  // var gridViewRow = "<tr class='" + cssClass + " floor" + key + " " + sirenClass + "' style='display:none'>" +
                                                                    //"<td>" + siren + "</td><td>" + roomNumber + " " + thisRoom.roomDesc + "</td><td>" + controllerStatus + "</td><td>" + battStatus + "</td><td>" + thisRoom.iPad_App + "</td><td>" + rentedStatus + "</td><td>" + umIcon + "</td><td style='white-space:nowrap'>" + alertRemark + "</td></tr>";
    gridview.append(gridViewRow);
    } catch(err){console.log(err);}
    //$("#"+roomNumber + "Mute").bind("change", {room:roomNumber},function (event) { muteRoom(event.data.room); });
    //console.log(roomNumber + "-" + isRoomMuteForUser(roomNumber));
    //=============================end populate grid view========================================
}

function sortRooms(floor) {
    //console.log(floor.toString());
    
    var rows = $('tr.floor' + floor).get();
    rows.sort(function (a, b) {
        //console.log("objects");

        //console.log($(a).children().eq(1).html().split(" ")[0]);
        //console.log($(b).children().eq(1).html().split(" ")[0]);
        var room1 = $(a).children().eq(1).html().split(" ")[0];
        var room2 = $(b).children().eq(1).html().split(" ")[0];
        if (room1 < room2) {
            //console.log(room1 +"<"+room2);
            return -1;
        }
        else {
            //console.log(room1 + ">" + room2);
            return 1;
        }
    });

    $.each(rows, function (idx, itm) { $('#gridViewTableBody').append(itm); });
}
function muteRoom(room) {
    //console.log(room);
    isRoomMuteForUser(room); //ensures the property is created before accessing it.

    if ($("#" + room + "Mute").is(':checked')) {
        localStorage[globalUserObject.user.id + room] = 0;
    }
    else {
        localStorage[globalUserObject.user.id + room] = 1;
    }
}
function isRoomMuteForUser(roomNo) {
    return false;
//    if (supportsLocalStorage() == true) {
//        var isMute = localStorage[globalUserObject.user.id+roomNo];
//        if (isMute == undefined) {
//            console.log('not found');
//            localStorage[globalUserObject.user.id + roomNo] = 0;
//            return 0;
//        }
//        else {
//            return isMute;
//            //console.log('found '+isMute);
//        }
//    }
//    else {
//        console.log("browser does not support local storage");
//    }
}

function filterChanged() {
    console.log("filterChanged");

    var filterSelector = getClassSelectorForGrid().split("|")[0];
    var finalSelector = "";
    var str = "";
    $("#floorinput option").each(function () {
        str += $(this).text() + " ";
        
        var floorSelector = ".floor" + $(this).text();

        if ($(this).attr("selected") != "selected") {
            
            //$(rowSelector).attr("style", "display:none"); //for gridview
        }

        if ($(this).attr("selected") == "selected") {
            finalSelector += filterSelector + floorSelector + ",";
            //$(rowSelector).attr("style", "display:''"); //for gridview
        }
    });

   // console.log("finalselector"+finalSelector);

    
   // if ($('#filterButton').is(':checked')) {
        //console.log("filterChanged filterbutton");
        
        var filteredDivs = $(finalSelector);
       

        if (filteredDivs.length>0) {
            
            $('tbody tr').attr("style", "display:none"); //hide all
            $(finalSelector).attr("style", "display:"); //show the ones that match
            $('#errorMessage').animate({ opacity: 0.1 }, 300);
            $('#errorMessage').html("");
        }
        else {  // no results found
            $('tbody tr').attr("style", "display:none"); //hide all
            publishNoResult();
        }
    //}

   

    
}


function openModelForGridView() {

//    var temp = getClassSelectorForGrid();
//    if (temp == "#gridViewTableBody>tr|") {
//        //no filters selected animate for visual clue
//        for (var i = 0; i < 3; i++) {
//            $('#legendWrapper div').animate({ opacity: 0.7 }, 100);
//            $('#legendWrapper div').animate({ opacity: 1.0 }, 100);
//        }
//        return 0;
//    }
//    else {
//    }
//    var selector = temp.split("|")[0];
//    var modelTitle = temp.split("|")[1];

//    hotelUiModel = "filter"; //set global variable

//    openModal(modelTitle, {
//        'Print':
//        function () {
//            // Print the DIV.
//            $(".filterModalContainer").print("Dashboard Status on " + new Date().toLocaleString() + " , for user " + hotel.user.name, "<link rel='stylesheet' media='print' href='css/print.css?v=1'>");
//            return (false);
//        },
//        'Set as preferred view':
//            function (model) {
//                /*call ajax to set user preference*/
//                $.ajax(
//                {
//                    url: "../saveuserpreference.php",
//                    data: { "userId": hotel.user.id, "prefName": "preferredFilterForAtsDashboard", "prefValue": temp + "|icon" },
//                    asynch: true,
//                    success: function (data) {
//                        //save locally as well
//                        if (hotel.user.preference.preferredFilterForAtsDashboard !== undefined) {
//                            hotel.user.preference.preferredFilterForAtsDashboard = temp;
//                        }
//                        $("#messageInModel").attr("style", "display:inline-block");
//                        $("#messageInModel").html('Saved as your preferred view.');
//                        setTimeout(function () {
//                            $("#messageInModel").fadeOut(2000);
//                            $("#messageInModel").html("");
//                            $("#messageInModel").attr("style", "display:none");
//                        }, 8000);
//                    },
//                    error: function () { publishConnectionStatus(false); }
//                });

//            },
//        'Close': function (modal) { hotelUiModel = undefined; modal.closeModal() }

//    },"<table class='filterModalContainer simple-table'>","</table>");

//    refreshGridPopup();
}

function refreshIconView()
{ }

function refreshGridView() {
    var temp = getClassSelectorForGrid();
    var selector = temp.split("|")[0];

    var finalSelector = "";
    $("#floorinput option").each(function () {
        var floorSelector = ".floor" + $(this).text();

        if ($(this).attr("selected") != "selected") {}

        if ($(this).attr("selected") == "selected") {
            finalSelector += selector + floorSelector +",";
        }
    });

    var filteredDivs = $(finalSelector);
    //console.log("final selector: " + finalSelector);
    if (filteredDivs.is("tr")) {
        $("#gridViewTableBody tr").hide();
        filteredDivs.show();
    }
    else {
        publishNoResult();
    }
}

function publishNoResult() {
    $('#filterButton').tooltip('No Rooms Match Your Selected Filter!<br>Please Modify Your Search Filter.', { 'position': 'bottom' });
    setTimeout(function () {
        $('#filterButton').removeTooltip();
    }, 2000);
}

function openAllView() {
    console.log('open all view');
    clearFilterCheckBox();


    var j = 0;
    //select unselected floors programtically
    $('.select-value.alt').click();
    $("#floorinput option").each(function () {
        //console.log("j=" + j + " , " + $(this).attr("selected"));
        if ($(this).attr("selected") != "selected") {
            
            $('.drop-down.custom-scroll span.check')[j].click(); //select this one
        }
        j++;
    });

    //console.log('============= clicking filter button');
    $('#filterButton').click();
}

function openUserPreferredView() {
     if(hotel!=undefined) //ensure hotel object has been fethced using ajax
     {
        clearFilterCheckBox();

        //programatically simulate filtering
        if (hotel.user.preference.preferredFilterForAtsDashboard == undefined) {
            console.log('user object not found. cant open preferred view');
            return 0; 
        }
        else {
            //console.log('----------------opening preferred view');
            var filterArray = hotel.user.preference.preferredFilterForAtsDashboard.split("|");
            var filters = filterArray[0];
            //console.log(filters);
            var view = filterArray[2];
            //console.log(view);
            var floors = filterArray[3];
            console.log("floors:"+floors);

            
            $('.select-arrow').click();
            $('.select-arrow').click();
            var j = 0;
            //DEselect selected floors programtically
            $("#floorinput option").each(function () {
                if ($(this).attr("selected") == "selected") {
                    //console.log('should deselect this:' + $(this).text());
                    //alert('');
                    $($('.drop-down.custom-scroll span.check')[j]).click();
                }
                else {
                    //console.log('already selected :' + $(this).text());
                }
                j++;
            });

            i = 0;
            $("#floorinput option").each(function () {
                var floor = $(this).text();

                if (floors.indexOf(floor) != -1) {
                    //console.log("selecting " + floor + " , i" + i);
                    //alert('');
                    $($('.check')[i]).click();
                }
                else {
                    //console.log("===not selected:" + floor);
                }
                i++;
            });



            //select filters programatically
      
            if (filters.indexOf(".rented") != -1) {
                $('#rentedCheck').click();
            }
            if (filters.indexOf(".unrented") != -1) {
                $('#unrentedCheck').click();
            }
//            if (filters.indexOf(".mur") != -1) {
//                $('#mmrCheck').click();
//            }
//            if (filters.indexOf(".dnd") != -1) {
//                $('#dndCheck').click();
//            }
            if (filters.indexOf('.sirenForRoom') != -1) {
                $('#alertCheck').click();
            }
            if (filters.indexOf(".maintain") != -1) {
                $('#maintenanceCheck').click();
            }
        
        } //end else


        //select view programatically
        //***pending

        //console.log('=======clicking filter button');
        $('#filterButton').click(); 
        
       
    }
    else {
        publishConnectionStatus(false);
    }
}

function savePreferredView() {
    var temp = getClassSelectorForGrid();
    var viewOption = "list";

    var floorOption = "";
    $("#floorinput option").each(function () {
        if ($(this).attr("selected") == "selected") {
            floorOption += $(this).text() + ",";
        }
    });

    /*call ajax to set user preference*/
    $.ajax(
    {
        url: messageroot + "phpfiles/saveuserpreference.php",
        data: { "userId": globalUserObject.user.id, "prefName": "preferredFilterForAtsDashboard", "prefValue": temp + "|" + viewOption + "|" + floorOption },
        asynch: true,
        success: function (data) {
            //save locally as well
            if (hotel.user.preference.preferredFilterForAtsDashboard != undefined) {
                console.log('save preference locally');
                hotel.user.preference.preferredFilterForAtsDashboard = temp + "|" + viewOption + "|" + floorOption;
            }
            $('#savePreferredViewButton').tooltip('<span class="icon-tick green">Saved as your preferred view.</span>', { 'position': 'bottom', 'removeOnBlur': true });
            setTimeout(function () {
                $('#savePreferredViewButton').removeTooltip();
            }, 5000);
        },
        error: function () {
            publishConnectionStatus(false);
            $('#savePreferredViewButton').tooltip('<span class="icon-cross red">Error in saving.</span>', { 'position': 'bottom', 'removeOnBlur': true });
            setTimeout(function () {
                $('#savePreferredViewButton').removeTooltip();
            }, 5000);
        }
    });
}

//get the jquery selector string based on current checkboxes on ui
function getClassSelectorForGrid() {
    var selector = "#gridViewTableBody tr";
    var filterText = "";

    if ($('#rentedCheck').is(':checked'))  //rented or unrented
    {
        selector = selector + ".rented";
        filterText = filterText + " Rented ";
    }
    if ($('#unrentedCheck').is(':checked')) {
        selector = selector + ".unrented";
        filterText = filterText + " Unrented ";
    }
//    if ($('#occupiedCheck').is(':checked')) // occupied or unoccupied
//    {
//        selector = selector + ".occupied";
//        filterText = filterText + " Occupied ";
//    }
//    if ($('#unoccupiedCheck').is(':checked')) {
//        selector = selector + ".unoccupied";
//        filterText = filterText + " Unoccupied ";
//    }
//    if ($('#dndCheck').is(':checked')) {
//        selector = selector + ".dnd";
//        filterText = filterText + " DND ";
//    }
  
    if ($('#maintenanceCheck').is(':checked')) {
        selector = selector + ".maintain";
        filterText = filterText + " Maint. ";
    }
    if ($('#alertCheck').is(':checked')) {
        selector = selector + ".sirenForRoom";
        filterText = filterText + " Alert ";
    }
    
    //console.log(selector + "|" + filterText);
    return selector + "|" + filterText;
}

function supportsLocalStorage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        console.log(e);
        return false;
    }
}

function clearFilterCheckBox() {
    //uncheck all the selected checkboxes
    if ($('#rentedCheck').is(':checked'))
        $('#rentedCheck').click();
    if ($('#unrentedCheck').is(':checked'))
        $('#unrentedCheck').click();
    //if ($('#mmrCheck').is(':checked'))
        //$('#mmrCheck').click();
    //if ($('#dndCheck').is(':checked'))
        //$('#dndCheck').click();
    if ($('#maintenanceCheck').is(':checked'))
        $('#maintenanceCheck').click();
    if ($('#alertCheck').is(':checked'))
        $('#alertCheck').click();
}