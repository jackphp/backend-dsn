/**
* ths function determines the view front-office, ats, engg etc
* this function will be written in different .js files.
* the js file which is included in the page will execute.
* following method renders ui for front desk dashboard
*/
function populateByView(thisRoom, cssClass, key, dashboard, gridview) {
    //console.log("populateByView");
    try{
	var roomNumber = thisRoom.roomName;
    //=============================populate icon view========================================
    var roomDiv = "<div  class= 'roomStatus " + cssClass + " floor" + key + "' >" +
					                "<div id='" + thisRoom.roomNo + "' class='center' >" + roomNumber + "</div>" +
                                    "<div class='first'></div>" +
				                    "<div class='second'></div>" +
				                    "<div class='third'></div>" +
				                    "<div class='fourth'></div>" +
                                "</div>";



    dashboard.append(roomDiv);
    $('#' + thisRoom.roomNo).bind("click", function () {

        //openAction('Perform Actions On Room ' + $(this).html());
        //var id = $(this).attr("id");
       // $('#actionContainer').html("<div style='width:300px;height:200px'><div id='" + id + "' style='display:none' >" + id + "</div> <div id='userActionMarkup' style='height:150px;z-index:1000'></div></div>");
       //openAction($(this).attr("id"));
    });
	} catch(err){console.log(err);}
    //console.log(roomNumber + ":" + cssClass+":"+thisRoom.isDnd);
    //=============================end populate icon view========================================

    //=============================populate grid view========================================
    // populate <tr></tr>
    var gridViewRow = "<tr class='" + cssClass + " floor" + key + "'><td>" + roomNumber + "</td> <td>NA</td>  <td><div>" + cssClass + " floor" + key + "<div></td></tr>";
    gridview.append(gridViewRow);
    //=============================end populate grid view========================================
}

function savePreferredView() {
    var temp = getClassSelector();
    var viewOption ="icon";
    
    var floorOption = "";
    $("#floorinput option").each(function () {
        if ($(this).attr("selected") == "selected") {
            floorOption += $(this).text() + ",";
        }
    });

    /*call ajax to set user preference*/
    $.ajax(
    {
        url: messageroot + "phpfiles/saveuserpreference.php",
        data: { "userId": hotel.user.id, "prefName": "preferredFilterForRoomDashboard", "prefValue": temp + "|" + viewOption + "|" + floorOption },
        asynch: true,
        success: function (data) {
            //save locally as well
            if (hotel.user.preference.preferredFilterForRoomDashboard !== undefined) {
                hotel.user.preference.preferredFilterForRoomDashboard = temp + "|" + viewOption + "|" + floorOption;
            }
            $('#savePreferredViewButton').tooltip('<span class="icon-tick green">Saved as your preferred view.</span>', {'position':'bottom','removeOnBlur':true});
            setTimeout(function () {
                $('#savePreferredViewButton').removeTooltip();
            }, 5000);
        },
        error: function () {
            publishConnectionStatus(false);
            $('#savePreferredViewButton').tooltip('<span class="icon-cross red">Error in saving.</span>', { 'position': 'bottom', 'removeOnBlur': true });
            setTimeout(function () {
                $('#savePreferredViewButton').removeTooltip();
            }, 5000);
        }
    });
}

function filterChanged(calledFromUi) {
    //console.log("filterChanged " + calledFromUi);
    
    viewChanged();
    filterIconView();
}

function filterIconView() {

    refreshIconView();
}
function refreshGridView()
{ }

function refreshIconView() {
    var temp = getClassSelector();
    var selector = temp.split("|")[0];

    //if (hotelUiModel == "filter") {  //refresh data in model with filter if Filter popup isvisible
    var finalSelector = "";
    $("#floorinput option").each(function () {
        var floorSelector = ".floor" + $(this).text();

        if ($(this).attr("selected") != "selected") {

        }

        if ($(this).attr("selected") == "selected") {
            finalSelector += selector + floorSelector + ",.floorSeparator" + floorSelector + ",";
           // var sep = $(floorSelector+" [style:\"display:none\"]");
          // if (sep.length == 1) {
             //   console.log(sep);
               // console.log(" sep lentgh==1");
                //sep.hide();
            //}

        }
    });

    var filteredDivs = $(finalSelector);
    //console.log("final selector: "+finalSelector);
    if (filteredDivs.is(".roomStatus")) {
        $(".floorSeparator,.roomStatus").hide();
        filteredDivs.show();
        
    }
    else {  
        publishNoResult();
    }

}
function publishNoResult() {
    $('#filterButton').tooltip('No Rooms Match Your Selected Filter!<br>Please Modify Your Search Filter.', {'position':'bottom'});
    setTimeout(function () {
        $('#filterButton').removeTooltip();
    }, 2000);
    $(".floorSeparator,.roomStatus").hide();
}
function openModelForGridView()
{ console.log('gridview nto implemented'); }
function refreshGridPopup() { }

function openAllView() {
    if ($('#rentedCheck').is(':checked'))
        $('#rentedCheck').click();
    if ($('#unrentedCheck').is(':checked'))
        $('#unrentedCheck').click();
    if ($('#mmrCheck').is(':checked'))
        $('#mmrCheck').click();
    if ($('#dndCheck').is(':checked'))
        $('#dndCheck').click();
    if ($('#maintenanceCheck').is(':checked'))
        $('#maintenanceCheck').click();

    var j = 0;
    //select unselected floors programtically
    $("#floorinput option").each(function () {
        if ($(this).attr("selected") != "selected") {
            $('.select-value.alt').click();
            $('.drop-down.custom-scroll span.check')[j].click(); //select this one
        }
        j++;
    });

    $('#filterButton').click();

}

function sortRooms(flr) { 

}

function openUserPreferredView() {
    //deselct existing options
    //uncheck all the selected checkboxes
    if ($('#rentedCheck').is(':checked'))
        $('#rentedCheck').click();
    if ($('#unrentedCheck').is(':checked'))
        $('#unrentedCheck').click();
    if ($('#mmrCheck').is(':checked'))
        $('#mmrCheck').click();
    if ($('#dndCheck').is(':checked'))
        $('#dndCheck').click();
    if ($('#maintenanceCheck').is(':checked'))
        $('#maintenanceCheck').click();



    //programatically simulate filtering
    if (hotel.user.preference.preferredFilterForRoomDashboard == undefined) 
    {
        console.log('user object not found. cant open preferred view');
        return 0; 
    }
    else {
        console.log('----------------opening preferred view');
        var filterArray = hotel.user.preference.preferredFilterForRoomDashboard.split("|");
        var filters = filterArray[0];
        //console.log(filters);
        var view = filterArray[2];
        //console.log(view);
        var floors = filterArray[3];
        //console.log(floors);
        var j = 0;
        //DEselect floors programtically
        $("#floorinput option").each(function () {
            if ($(this).attr("selected") == "selected") {
                $('.select-value.alt').click();
                $('.drop-down.custom-scroll span.check')[j].click(); //deselect this one
            }
            j++;
        });
        var i = 0;
        $("#floorinput option").each(function () {
            var floor = $(this).text();

            if (floors.indexOf(floor) != -1) {
                $('.drop-down.custom-scroll span.check')[i].click();
            }
            //else { console.log("===not selected:" + floor); }

            i++;
        });


        //select filters programatically
        if(filters.indexOf(".rented")!=-1)
        {
            $('#rentedCheck').click();
        }
        if (filters.indexOf(".unrented") != -1)
        {
            $('#unrentedCheck').click();
        }
        if (filters.indexOf(".mur") != -1)
        {
            $('#mmrCheck').click();
        }
        if (filters.indexOf(".dnd") != -1)
        {
             $('#dndCheck').click();
        }
        if (filters.indexOf(".maintain") != -1)
        {
            $('#maintenanceCheck').click();
        }
    }//end else


    //select view programatically
    //***pending

    $('#filterButton').click();

    // Print the DIV.
    //$(".filterModalContainer").print("Dashboard Status on " + new Date().toLocaleString() + " , for user " + hotel.user.name, "<link rel='stylesheet' media='print' href='css/print.css?v=1'>");
}

function showHideFloors() {
    
      
}

//get the jquery selector string based on current checkboxes on ui
function getClassSelector() {
    var selector = ".roomStatus";
    var filterText = "";

    if ($('#rentedCheck').is(':checked'))  //rented or unrented
    {
        selector = selector + ".rented";
        filterText = filterText + " Rented ";
    }
    if ($('#unrentedCheck').is(':checked')) {
        selector = selector + ".unrented";
        filterText = filterText + " Unrented ";
    }
    if ($('#occupiedCheck').is(':checked')) // occupied or unoccupied NOT FOR ARMANI
    {
        selector = selector + ".occupied";
        filterText = filterText + " Occupied ";
    }
    if ($('#unoccupiedCheck').is(':checked')) {
        selector = selector + ".unoccupied";
        filterText = filterText + " Unoccupied ";
    }
    if ($('#dndCheck').is(':checked')) {
        selector = selector + ".dnd";
        filterText = filterText + " DND ";
    }
    if ($('#mmrCheck').is(':checked')) {
        selector = selector + ".mur";
        filterText = filterText + " MMR ";
    }

    if ($('#maintenanceCheck').is(':checked')) {
        selector = selector + ".maintain";
        filterText = filterText + " Maint. ";
    }
    //console.log(selector + "|" + filterText);
    return selector + "|" + filterText;
}