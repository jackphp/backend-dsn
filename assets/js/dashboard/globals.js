$(document).ready(function () { initGlobal(); });


var globalUserObject;

function initGlobal() {
    $.ajax(
    {
        url: apiroot + 'user_json',
        //url: 'userdata.json',
        data: { "param": "user" },
        isAsynch: false,
        success: function (data) {
            publishConnectionStatus(true); //connected
            //console.log('recieved room data from server'+data);
            try {
                //console.log(data);
                processUser("{ " + data + " }");
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}


function processUser(data)
{
    globalUserObject = eval("("+data+")");
    console.log("current user:"+globalUserObject.user.id);
}


function publishConnectionStatus(isConnected) {
    if (isConnected)
    {
        for (var i = 0; i < 2; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 400);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 400);
        }

        $("#serverConnectionStatus").html("Connection OK");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 200);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 200);
        }
        $("#serverConnectionStatus").html("Error in connection");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy red");
        
        publishServerHealth(isConnected); //connection error automatically means server error
    }
}

function publishServerHealth(isAlive) {
    if (isAlive) {
        for (var i = 0; i < 2; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 400);
            $('#serverHealth').animate({ opacity: 1.0 }, 400);
        }

        $("#serverHealth").html("Server live");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 200);
            $('#serverHealth').animate({ opacity: 1.0 }, 200);
        }
        $("#serverHealth").html("Error at server");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 red");
    }
}

function openProfileEditor() {
    var position = $('#menu').position();

    var actionModal = $.modal({
        title: "Edit your details ",
        content: '&nbsp;',
        beforeContent: '<div id="profileEditForm" style="display:">',
        afterContent: '</div>',
        buttons:
        {
            'Close & Refresh Page': {
                //classes: 'icon-tick',
                click: function (modal) { modal.closeModal(); location.reload(); }
            }
        }
    });
    actionModal.setModalPosition(position.left, position.top + 195, false);
    actionModal.setModalContentSize(200,270);
    //var maintIco = isMaint == 1 ? "<label for='name='isMaintSwitch'' class='label'>Maintenance</label><input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' checked class='switch medium'>" : "<label for='name='isMaintSwitch'' class='label'>Maintenance</label><input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' class='switch medium'>";

    $('#profileEditForm').html(profileEditMarkup);
   
    $('#userName').val(hotel.user.user_display_name);
    //prevent scroll behind modal window
    $('.modal').mousewheel(function (event, delta) { return false; });
}

var tipOptions = { position: "top", exclusive: true , onShow: function (target) { setTimeout(function () { target.removeTooltip(false, false); }, 3500); } };
function formTip(parent, message) {
    $("#"+parent).tooltip(message, tipOptions);
}
function uploadImage() {
    if ($('#imageFile').get(0).files.length==1)
    {
        console.log('not empty:');

        $("#imgUpload").ajaxSubmit({
            url: '../digivalet_dashboard/update_user',
            data: { "operation": "change_thumb" },
            type: 'POST',
            success: function (responseText, statusText, xhr, $form) {
                console.log('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');
                formTip("imageFile", "<span class='green'><span class='icon-tick'></span>Profile image changed. Page will refresh in 2 seconds...</span>");
                setTimeout(function () { location.reload(); }, 2000);
                //alert('page refresh pending...');
            },
            error: function () { formTip("imageFile", "<span class='red'><span class='icon-cross'></span>Profile image could not be changed. Please try later</span>"); }
        }); 
        
        //document.imgUpload.submit();
//        //ajax call to update image
//        $.ajax(
//        {
//            url: '../digivalet_dashboard/update_user',
//            data: { "operation": "change_thumb", "imageFile": $('#imageFile').get(0).files[0] },
//            success: function (data) {
//                formTip("imageFile", "<span class='green'>Image updated. The page will refresh in 2 seconds...</span>");
//                setTimeout(function () { location.reload(); }, 2000);
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                formTip("imageFile", "<span class='red'>Image could not be updated. Please try later.</span>");
//            }
//        });

    }
    else
    {
        formTip("imageFile","<span class='red'>Select an image file</span>");
    }
}
function changePass() {
    if(validatePass()) {
        
        //ajax call to update password
        $.ajax(
        {
            url: '../digivalet_dashboard/update_user',
            data: { "operation": "change_pass", "oldPass": $('#oldPass').val(), "newPass": $('#newPass').val() },
            success: function (data) {
                if (data.indexOf('success')!=-1)
                {
                    formTip("newPass", "<span class='green'><span class='icon-tick'>Password changed. The page will refresh in 2 seconds...</span>");
                    setTimeout(function () { location.reload(); }, 2000);
                }
                if (data.indexOf('mismatch') != -1) {
                    formTip("oldPass", "<span class='red'>Old password is incorrect.</span>");
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                formTip("newPass", "<span class='red'>Password could not be updated. Please try later.</span>");
            }
        });
    }
//    else
//    {
//         formTip("newPass","<span class='red'>minimum 4 characters</span>");
//    }
 }

function validatePass() {
    if ($('#oldPass').val() == '') {
        formTip("oldPass", "<span class='red'>Should not be empty.</span>");
        return false;
    }
    if ($('#newPass').val() == '') {
        formTip("newPass", "<span class='red'>Should not be empty.</span>");
        return false;
    }
    if ($('#newPass').val().length < 6 || $('#newPass').val().length >20) {
        formTip("newPass", "<span class='red'>Password length should be between 6-20 characters.</span>");
        return false;
    }
    return true;
}

function changeName() {
    if($('#userName').val()!='')
    {
        //ajax call to update display name
        //index.php/digivalet_dashboard/update_user?operation=change_name&userName=somevalue
         $.ajax(
        {
            url: '../digivalet_dashboard/update_user',
            data: { "operation": "change_name", "userName": $('#userName').val() },
            success: function (data) {
                formTip("userName", "<span class='green'><span class='icon-tick'>Name updated. The page will refresh in 2 seconds...</span>");
                setTimeout(function () { location.reload(); }, 2000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                formTip("userName", "<span class='red'>Name could not be updated. Please try later.</span>");
            }
        });
    }
    else
    {
        formTip("userName","<span class='red'>Should not be empty</span>");
    }
}

var profileEditMarkup = "<span class='clear-both'></span>" +
                        "<p class='button-height inline-normal-label'>" +
                            "<label for='userName' class='label'>Change Display Name</label>" +
                            "<input type='text' name='userName' id='userName' class='input'>" +
                            "<span class='button icon-tick' onClick='changeName();'></span><br>" +
                            "<label for='oldPass' class='label'>Change Password</label>" +
                            "<input type='password' name='oldPass' id='oldPass' class='input icon-lock' placeholder='Old Password'><br>" +
                            "<input type='password' name='newPass' id='newPass' class='input icon-lock' placeholder='New Password'>" +
                            "<span class='button icon-tick' onClick='changePass();'></span><br>" +
                            "<label for='userRemark' class='label'>Change Profile Image</label>" +
                            "<form name='imgUpload' id='imgUpload' method='post' style='display:inline'><input type='file' class='file' name='imageFile' id='imageFile' style='width:160px'></form>" +
                            "<span class='button icon-tick' onClick='uploadImage();'></span>" +
                        "</p>";


//'Save Profile': {
//                classes: 'icon-tick',
//                click: function (modal) {
//                    if ($('#userRemark').val() != '') {
//                        console.log('publish remark');
//                        var isMaintVal = $("#viewSelect").attr('checked') ? 1 : 0;
//                        $.ajax(
//                        {
//                            //url: 'data.json',
//                            url: '../api_dashboard/publish_maint_remark.php',
//                            //data: { user: globalUserObject.user.name, remark: $('#userRemark').val(), room: roomNumber, isMaint: $("#isMaintSwitch").attr('checked') ? 1 : 0 },
//                            success: function (data) {
//                                console.log('success ' + data);
//                                refreshDashBoard(true);
//                                $('.button.icon-cross').click();
//                            },
//                            error: function (jqXHR, textStatus, errorThrown) {
//                                console.log('server returned error');
//                            }
//                        });
//                    }
//                    else {
//                        for (var i = 0; i < 4; i++) {
//                            $('#userRemark').animate({ opacity: 0.1 }, 100);
//                            $('#userRemark').animate({ opacity: 1.0 }, 100);
//                        }
//                    }
//                }
//            },


//---------------------------ATS alarm behvior as per profile
function getAlarmForThisProfile(thisRoom,battColor,profile)
{
    
    //if(thisRoom.iremote == 0 || thisRoom.pc == 0 )   
     if(thisRoom.pc == 0 )   
    {
        return true;
        
        // ignore the rest ---------
        if(hotel.user ==undefined)
        {
            return true; //default is blow siren anyways
        } 
        else
        {
            
            var ret;
            switch(profile)
            {
                case 'front_office':
                    //console.log('front_office');
                    ret= true;
                    break;
                case 'house_keeping':
                    //console.log('house_keeping');
                    if(battColor == 'red')
                        ret= true;
                    else
                        ret= false;
                    break;
                case 'systems':
                    // ipad update not pushed ?? yet to me implemented
                    //console.log('systems');
                    ret= true;
                    break;
                default:
                    ret= true;                
            }
            
            return ret;
        }
    }  
    else
    {
        return false;
    }
}

//--------------------------------------show hide columns as per profile
function showHideColumns()
{
    console.log('showHideColumns');
    if(hotel.user!= undefined)
    {
        //profile names that begin with front_office
        /*
         UI view when any member from FO(FRONT OFFICE) login:
         Room No.|Controller status|I-Pad last seen| ipad battery status | rented |maintenance
         Option to change the maintenance should be provided for this login
         * */
         /*if(hotel.user.profile_name.indexOf('front_office')!=-1) 
         {
            console.log('front office. hide version column');
            $('#gridViewTable td:nth-child(6)').hide();
            $('#gridViewTable th:nth-child(6)').hide();
         }*/   
         
         //profile names that begin with house_keeping
         /* as per mail rcvd from yunus on 5 November 2012 6:21:31 PM 
            Ats view when any member from HK(HOUSE KEEPING) login:
            Room No.| ipad battery status | rented |maintenance
            Option to view the maintenance to be provided. changing the maintenance should NOT be provided for this login
          * */
         /*if(hotel.user.profile_name.indexOf('house_keeping')!=-1) 
         {
            $('#gridViewTable td:nth-child(3)').hide();
            $('#gridViewTable td:nth-child(5)').hide();
            $('#gridViewTable td:nth-child(6)').hide();
            $('#gridViewTable th:nth-child(3)').hide();
            $('#gridViewTable th:nth-child(5)').hide();
            $('#gridViewTable th:nth-child(6)').hide();
            
         }*/
         
         //profile names that begin with systems
         /*
          UI view when any member from SYSTEMS login:
            Room No.|Controller status|Version| I-Pad last seen| ipad battery status | rented |maintenance
            Alarm to configured if the I-pad batter status goes below 20%, Controller shuts down, Version update on the controller does not happen when updates are pushed from the server to the controller and If I-Pad goes missing from the room
            Option to change the maintenance should be provided for this login
          * */
         //if(hotel.user.profile_name.indexOf('systems')!=-1) 
         //{
            //show all columns
         //}
     }
}














































