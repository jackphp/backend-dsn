function openAction(title, buttonObj) {
    console.log("openAction");
    $.modal({
        title: title,
        content: 'filter',
        buttons: buttonObj,
        beforeContent: '<div id="actionContainer">',
        afterContent: '</div>',
        buttonsAlign: 'right',
        resizable: false
    }).centerModal(true);
    
}

function openUserAction(targetButton, roomNumber,isMaint, pc_ip1, muteBattery) {
   
   //hotel.user.userActionForAts(roomNumber);
    //console.log('not wired with user action hotel.user.userActionForAts');

    var position = $('#actionButton' + targetButton).position();
    var actionModal = $.modal({
        title: "Action on room " + roomNumber,
        content: '&nbsp;',
        beforeContent: '<div id="actionContainer" style="display:">',
        afterContent: '</div>',
        buttons:
        {
            'Save': {
                classes: 'icon-rss',
                click: function (modal) {
                   
                    if ($('#userRemark').val() != '') {
                        //try {
                                console.log('publish remark');
                                var isMaintVal = $("#viewSelect").attr('checked') ? 1 : 0;
                                $.ajax(
                                {
                                    //url: 'data.json',
                                    url: messageroot + 'publish_maint_remark.php',
                                    data: { user: globalUserObject.user.name, remark: $('#userRemark').val(), room: roomNumber, isMaint: $('#isMaintSwitch').attr('checked') ? 1 : 0, 'pc_ip': pc_ip1, 'battery_mute': $('#isBatteryMute').attr('checked') ? 0 : 1},
                                    success: function (data) {
                                        console.log('success ' + data);
                                        refreshDashBoard(true);
                                        $('.button.icon-cross').click();
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log('server returned error');
                                    }
                                });
                       //}
                       //catch(er){console.log(er);}

                    }
                    else {
                        for (var i = 0; i < 4; i++) {
                            $('#userRemark').animate({ opacity: 0.1 }, 100);
                            $('#userRemark').animate({ opacity: 1.0 }, 100);
                        }
                    }
                }
            },

            'Cancel': {
                classes: 'icon-cross',
                click: function (modal) { modal.closeModal(); }
            }
        }
    });
    actionModal.setModalPosition(position.left, position.top + 63, false);
    var maintIco="";
    if(hotel.user!= undefined && hotel.user.profile_name.indexOf('house_keeping')==-1)
    {
        console.log('NOT house keeping');
        maintIco = isMaint == 1 ? "<label for='name='isMaintSwitch'' class='label'>Maintenance</label>&nbsp;&nbsp;&nbsp;<input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' checked class='switch medium'>" : "<label for='name='isMaintSwitch'' class='label'>Maintenance</label>&nbsp;&nbsp;&nbsp;<input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' class='switch medium'>";
    }
    
    var muteBatteryButton = muteBattery == 0? "<label for='name='isBatteryMute'' class='label'>Battery Alarm</label>&nbsp;<input id='isBatteryMute' name='isBatteryMute' type='checkbox' checked class='switch medium'>" : "<label for='name='isBatteryMute'' class='label'>Battery Alarm</label>&nbsp;<input id='isBatteryMute' name='isBatteryMute' type='checkbox' class='switch medium'>";
    
    $('#actionContainer').html("<span class='clear-both'></span><p class='button-height inline-normal-label'><label for='userRemark' class='label'>Remark</label><input type='text' name='userRemark' id='userRemark' class='input'><br>" + maintIco + "<br>"+muteBatteryButton+"</p>");
    //prevent scroll behind modal window
    $('.modal').mousewheel(function (event, delta) {  return false; });
}

//<span class='clear-both'></span>
//<p class='button-height block-label'>
//<label for='userRemark' class='label'>Remark</label>
//<input type='text' name='userRemark' id='userRemark' class='input'>
//<label for='name='isMaintSwitch'' class='label'>Maintenance</label>

//</p>