/*
* this function populates view for engineering
*/
function populateByView(thisRoom, cssClass, key, dashboard, gridview) {
   //  console.log('engg view');     cssClass = getModifiedClass(thisRoom, cssClass);
     try {
        var setStatus = hotel.setTempStatus;
        var actStatus = hotel.actTempStatus;
        //console.log(setStatus);
        // console.log(actStatus);
    }
    catch (er) 
    { console.log(er); }
    
    if (setStatus[thisRoom.roomName] == undefined || actStatus[thisRoom.roomName] == undefined) {
            //console.log('empty');
            //console.log(thisRoom);    
            try { 
                
                renderEmpty(thisRoom, cssClass, key, dashboard, gridview);
             }
            catch (er) 
            { console.log(er); }
    }
    else {
           //console.log('full');
            //console.log(thisRoom);  
            var setTemp = setStatus[thisRoom.roomName].setTemp;
            var actTemp = actStatus[thisRoom.roomName].actTemp;
            try {
            renderReal(thisRoom, cssClass, key, dashboard, gridview, setTemp, actTemp);
            }
            catch (er) 
            { console.log(er); }
    }

  

  
}
function getModifiedClass(thisRoom, cssClass)
{
    if(thisRoom.temp_alert=='warn')
      {
          cssClass+= " warn ";
      }
    if(thisRoom.temp_alert=='ok')
      {
          cssClass+= " ok ";
      } 
    if(thisRoom.temp_alert=='alert')
      {
          cssClass+= " alert1 ";
      }  
    
    return cssClass;
}

function renderEmpty(thisRoom, cssClass, key, dashboard, gridview) {

    var roomDiv = "<div  id='" + thisRoom.roomName + "' class= 'tempStatus " + cssClass + " floor" + key + "' >" +
					            "<div  class='center' >" + thisRoom.roomName + "</div>" +
                                "<div class='first'><span class='setspan'></span></div>" +
				                "<div class='second'><span class='setspan'></span></div>" +
				                "<div class='third'></div>" +
				                "<div class='fourth'></div>" +
                            "</div>";
  
    //console.log(roomDiv);
    dashboard.append(roomDiv);
  
    /*try {
        var gridViewRow = "<tr><td>" + thisRoom.roomName + "</td> <td>" + thisRoom.digivalet_status + "</td>   <td>" + thisRoom.battery + "</td>   <td>NA (incorrect json)</td> <td>" + thisRoom.version + "</td>    <td>" + thisRoom.isRented + "</td> <td>" + thisRoom.isOccupied + "</td>   <td>" + thisRoom.isDnd + "</td>    <td> NA incorrect json</td>  <td>" + thisRoom.isMaint + "</td></tr>";
        gridview.append(gridViewRow);
    }
    catch (er){ console.log(er); }*/
}

function sortRooms(){
    //dummy
}

function renderReal(thisRoom, cssClass, key, dashboard, gridview, setTemp, actTemp)
{
    var tempClass= thisRoom.tempAlert;
    
    var roomNumber = thisRoom.roomName;
    
    //=============================populate icon view========================================
    var roomDiv = "<div  id='" + thisRoom.roomNo + "' class= 'tempStatus "+cssClass+" " + tempClass + " " + cssClass + " floor" + key + "' >" +
					    "<div  class='center' style='cursor:default'>" + roomNumber + "</div>" +
                        "<div class='first'><span class='setspan'>" + setTemp + "</span></div>" +
				        "<div class='second'><span class='actspan'>" + actTemp + "</span></div>" +
				        "<div class='third'></div>" +
				        "<div class='fourth'></div>" +
                  "</div>";

       
    //console.log(thisRoom.roomNo);
    dashboard.append(roomDiv);
    $('#' + thisRoom.roomNo).bind("click", function () {

        openAction(thisRoom);
       
    });


    
    //console.log(roomNumber + ":" + cssClass+":"+thisRoom.isDnd);
    //=============================end populate icon view========================================

    //=============================populate grid view========================================
    /* populate <tr></tr>
    var maintIco = thisRoom.isMaint == 1 ? "<input type='checkbox' checked class='switch medium'>" : "<input type='checkbox' class='switch medium'>";
    var ipadStatus = thisRoom.digivalet_status == 1 ? "<span class='icon-like green icon-size2'></span>" : "<span class='icon-cross red icon-size2'></span>";
    var rentedStatus = thisRoom.isRented == 1 ? "<span class='icon-tick green icon-size2'></span>" : "<span class='icon-cross red icon-size2'></span>";
    var occupiedStatus = thisRoom.isOccupied == 1 ? "<span class='icon-home green icon-size2'></span>" : "<span class='icon-home grey icon-size2'></span>";
    var dndStatus = thisRoom.isDnd == 1 ? "<span class='icon-price-tag red icon-size2'></span>" : "<span class='icon-price-tag grey icon-size2'></span>";
    var battColor = thisRoom.battery > 50 ? 'green' : thisRoom.battery > 20 ? 'orange' : 'red';
    var muteAlert = isRoomMuteForUser(roomNumber) == 0 ? "<input type='checkbox' id='" + roomNumber + "Mute' checked class='switch medium muteAlert'>" : "<input type='checkbox' id='" + roomNumber + "Mute' class='switch medium muteAlert'>";
    var gridViewRow = "<tr class='" + cssClass + " floor" + key + "' style='display:none'>" +
                                                                    "<td>" + roomNumber + "</td><td>" + ipadStatus + "</td><td><span class='progress large' style='width: 100%;height:20px'><span class='progress-bar " + battColor + "-gradient glossy' style='width: " + thisRoom.battery + "%;height:20px'><span class='progress-text'>" + thisRoom.battery + "%</span></span></span></td><td>NA (incorrect json)</td><td>" + thisRoom.version + "</td><td>" + rentedStatus + "</td><td>" + occupiedStatus + "</td><td>" + dndStatus + "</td><td>" + muteAlert + "</td><td>" + maintIco + "</td></tr>";
    gridview.append(gridViewRow);
    */
    //=============================end populate grid view========================================
}

function filterChanged(calledFromUi) {
    //console.log("filterChanged " + calledFromUi);
    
    //viewChanged();
    filterIconView();
}
function filterIconView() {

    refreshIconView();
}
function refreshGridView()
{ }

function refreshIconView() {
    var temp = getClassSelector();
    var selector = temp.split("|")[0];

    //if (hotelUiModel == "filter") {  //refresh data in model with filter if Filter popup isvisible
    var finalSelector = "";
    $("#floorinput option").each(function () {
        var floorSelector = ".floor" + $(this).text();

        if ($(this).attr("selected") != "selected") {

        }

        if ($(this).attr("selected") == "selected") {
            finalSelector += selector + floorSelector + ",.floorSeparator" + floorSelector + ",";
           // var sep = $(floorSelector+" [style:\"display:none\"]");
          // if (sep.length == 1) {
             //   console.log(sep);
               // console.log(" sep lentgh==1");
                //sep.hide();
            //}

        }
    });

    var filteredDivs = $(finalSelector);
    //console.log("final selector: "+finalSelector);
    if (filteredDivs.is(".tempStatus")) {
        $(".floorSeparator,.tempStatus").hide();
        filteredDivs.show();
        
    }
    else { 
        $(".floorSeparator,.tempStatus").hide(); 
        publishNoResult();
    }

}

function publishNoResult() {
    $('#filterButton').tooltip('No Rooms Match Your Selected Filter!<br>Please Modify Your Search Filter.', {'position':'bottom'});
    setTimeout(function () {
        $('#filterButton').removeTooltip();
    }, 2000);
    $(".floorSeparator,.roomStatus").hide();
}

function openAllView() {
    //simulate clearing of filters
    //refer frontoffice.render.js
}

function openModelForIconView() {

    modalObj = "";
    var temp = getClassSelector();
    if (temp == "#dashboard>.clearboth,#dashboard>.tempStatus|") {
        //no filters selected animate for visual clue
        for (var i = 0; i < 3; i++) {
            $('#legendWrapper div').animate({ opacity: 0.7 }, 100);
            $('#legendWrapper div').animate({ opacity: 1.0 }, 100);
        }
        return 0;
    }
    else {
    }
    var selector = temp.split("|")[0];
    var modelTitle = temp.split("|")[1];
    
    hotelUiModel = "filter"; //set global variable

    openModal(modelTitle, {
        'Print':
        function () {
            // Print the DIV.
            $(".filterModalContainer").print("Engineering Dashboard Status on " + new Date().toLocaleString() + " , for user " + hotel.user.name, "<link rel='stylesheet' media='print' href='css/print.css?v=1'>");
            return (false);
        },
        'Set as preferred view':
            function (model) {
                /*call ajax to set user preference*/
                $.ajax(
                {
                    url: "../saveuserpreference.php",
                    data: { "userId": hotel.user.id, "prefName": "preferredFilterForEnggDashboard", "prefValue": temp + "|icon" },
                    asynch: true,
                    success: function (data) {
                        //save locally as well
                        if (hotel.user.preference.preferredFilterForEnggDashboard !== undefined) {
                            hotel.user.preference.preferredFilterForEnggDashboard = temp;
                        }
                        $("#messageInModel").attr("style", "display:inline-block");
                        $("#messageInModel").html('Saved as your preferred view.');
                        setTimeout(function () {
                            $("#messageInModel").fadeOut(2000);
                            $("#messageInModel").html("");
                            $("#messageInModel").attr("style", "display:none");
                        }, 8000);
                    },
                    error: function () { publishConnectionStatus(false); }
                });

            },
            'Close': function (modal) { hotelUiModel=undefined;  modal.closeModal(); }
    });

    
    refreshIconPopup();
   
}

function refreshIconPopup() {
    var temp = getClassSelector();
    var selector = temp.split("|")[0];
    
    //console.log("-------selector" + selector);
    if (hotelUiModel == undefined) 
    {
        //alert("model not visible");
    }
    else if (hotelUiModel == "filter") 
    {  //refresh data in model with filter if Filter popup isvisible
       
        var filteredDivs = $(selector);
        if (filteredDivs.is(".tempStatus")) {
            $(".filterModalContainer").html(filteredDivs.clone());
            $(".filterModalContainer").append("<div id='messageInModel'></div>");
        }
        else {  // no results found
            $(".filterModalContainer").html("No Rooms Match Your Selected Filter!<br>Please Modify Your Search Filter.");
        }
    }
    else if (hotelUiModel == "preferred") 
    {  //refresh data with user's preferred view if user preferred view popup is visible
        var userFilter = hotel.user.preference.preferredFilterForEnggDashboard;
        var selector = userFilter.split("|")[0];
        //console.log(selector);
        $(".filterModalContainer").html($(selector).clone());
    }
   
}


function openModelForGridView()
{console.log('gridview nto implemented'); }
function refreshGridPopup() { }


function openUserPreferredView() {
    var userFilter = hotel.user.preference.preferredFilterForEnggDashboard;
    if (userFilter !== undefined) {
        hotelUiModel = "preferred";
        openModal(userFilter.split("|")[1], {
            'Print':
            function () {
                // Print the DIV.
                $(".filterModalContainer").print("Engineering Dashboard Status on " + new Date().toLocaleString() + " , for user " + hotel.user.name, "<link rel='stylesheet' media='print' href='css/print.css?v=1'>");
                return (false);
            },

            'Close': function (modal) { hotelUiModel = undefined; modal.closeModal(); }
        });

        
        var selector = userFilter.split("|")[0];
        //console.log(selector);
        $(".filterModalContainer").html($(selector).clone());
    }
    else {
        // no prefered filter set
    }
}

//get the jquery selector string based on current checkboxes on ui
function getClassSelector() {
    var selector = ".tempStatus";
    var filterText = "";

    if ($('#rentedCheck').is(':checked'))  //rented or unrented
    {
        selector = selector + ".rented";
        filterText = filterText + " Rented ";
    }
    if ($('#unrentedCheck').is(':checked')) {
        selector = selector + ".unrented";
        filterText = filterText + " Unrented ";
    }
    /*if ($('#occupiedCheck').is(':checked')) // occupied or unoccupied
    {
        selector = selector + ".occupied";
        filterText = filterText + " Occupied ";
    }
    if ($('#unoccupiedCheck').is(':checked')) {
        selector = selector + ".unoccupied";
        filterText = filterText + " Unoccupied ";
    }*/
   
    if ($('#maintenanceCheck').is(':checked')) {
        selector = selector + ".maintain";
        filterText = filterText + " Maint. ";
    }

    if ($('#okCheck').is(':checked')) {
        selector = selector + ".ok";
        filterText = filterText + " Ok ";
    }
    if ($('#warnCheck').is(':checked')) {
        selector = selector + ".warn";
        filterText = filterText + " Warn ";
    }
    if ($('#alert1Check').is(':checked')) {
        selector = selector + ".alert1";
        filterText = filterText + " Alert ";
    }
    //console.log(selector + "|" + filterText);
    return selector + "|" + filterText;
}
