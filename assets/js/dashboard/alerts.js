function getAlerts()
{
    $.ajax({
        url: "../useralert",
        data: { "operation": "query" },
        asynch: true,
        success: function (data) {
             try {
            //console.log(data);
            processAlerts(data);
            publishServerHealth(true);
            }
            catch (error) {
            publishServerHealth(false);
            }

        },
        error: function () { publishConnectionStatus(false); }
    });
}

function processAlerts(data) {
   
    var rooms = eval("({" + data + "})");
    console.log("got alerts");
    var visible = getVisibleFloors();
    /*iterate in all the room */
    for (var key in rooms) {
        var room = rooms[key];
      
        if (jQuery.inArray(room.floor, visible)!=-1 )   //show alerts if the floor is visible 
        {
            console.log("publish alert. Uncomment method call in alerts.js "+key);
            publishAlerts(key,room.messages);
         }
    }
}

function publishAlerts(roomNo, alerts) {
    
    var alert1;
    var title;
    var iconUrl;//= "./img/icon/info.png";
    for (var i = 0; i < alerts.length; i++) {
        alert1 = alerts[i];
        var alertid = alert1.alertId;
        
        //disaply only if the user has suitable role AND the alert has not been displayed already
        if (alert1.category == hotel.user.role && !alreadyShown(alertid)) {
            //--------------------logic for reflecting in dashboard-----------------------------------
//            if ("engineering" == alert1.category) {
//                //if the alert is related to temperature then show in dashboard
//                var div = $("#iconView #" + roomNo);
//                var cls = div.attr("class");
//                div.removeClass("ok").removeClass("warn").removeClass("alert");
//                div.addClass(alert1.alertLevel);
//                
//            }

            //--------------------pop up logic-----------------------------------
            //push into the published alert array
            displayedAlerts.push(alertid);

            title = alert1.category + ", Room-" + roomNo;

            iconUrl = "./img/icon/info.png"; //default
            if (alert1.alertLevel == "warn")
                iconUrl = "./img/icon/warn.png";
            if (alert1.alertLevel == "alert")
                iconUrl = "./img/icon/alert.png"


//            notify(title, alert1.alertMessage,
//            {
//                system: true,
//                autoClose: false,
//                icon: iconUrl,
//                inconOutside: false,
//                showCloseOnHover: false,
//                groupSimilar: false,
//                onClose: function () {
//                    markAttended(alertid);
//                }
//            });
        }
        else {
            console.log("dont show alert"+alert1.alertId);
        }
    }
}

function alreadyShown(alertid) {
    //empty if array is too large
    if (displayedAlerts.length > 100) {
       displayedAlerts.remove(0, 50);
   }

    return $.inArray(alertid, displayedAlerts)==-1?false:true;
}

function isMaintenance() { 
    
}
function markAttended(alertId) {
    console.log("user attended alert " + alertId);
    $.ajax({
        url: "../useralert",
        data: { "operation": "markAttended", "alertId": alertId, "user": hotel.user.id },
        asynch: true,
        success: function (data) {
            publishConnectionStatus(true);
        },
        error: function () { console.log("error marking attended");   publishConnectionStatus(false); }
    });

}

function getVisibleFloors() {
    var floors = new Array();
    $("#floorinput option").each(function () {
        if ($(this).attr("selected") == "selected") {
            floors.push($(this).text());
        }

    });
    return floors;
}

