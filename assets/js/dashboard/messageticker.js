$(document).ready(function () {
    if ($('#messageTicker').length > 0) //start ticker thread IF ticker div exists on page
    {
        setTimeout(function () { refreshMessageTicker(); }, 500);
        setInterval(function () { refreshMessageTicker(); }, 45000);
    }
    else {
        console.log("no #messageTicker div present on page");
    }
});

function refreshMessageTicker() {
    //$('#messageTicker').html("<li><span class='loader big'></span></li>");
    console.log("refreshMessageTicker" );
    $.ajax(
    {
        //url: 'data.json',
        url: apiroot + 'getallmessages.php',
        data: { "userid": hotel.user.id, "limit": "0,5" },
        success: function (data) {
            publishConnectionStatus(true); //connected
            try {

                showMessageList(data, 'messageTicker');
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

function showMessageList(data, targetDiv) {
    $('#' + targetDiv).html('');
    
    var list = eval("(" + data + ")");
    var msg;
    var msgli;
    var sentOn;
    var readStatus;
    var unreadCount = 0;
    for (var i = 0; i < list.length; i++) {
        msg = list[i];
        sentOn = getDateNicely(msg.sent);
        readStatus = msg.isRead == 1 ? "" : "<a href='#' id='read" + msg.conversationId + "' class='new-message' title='Mark as read' >New</a>";
        msgli = "<li><span class='message-status'>" +
                        readStatus +
                    "</span>" +
                    "<span class='message-info'>" +
                         "<span class='blue'>" + sentOn + "</span>" +
                    "</span>" +
                    "<a href='/inbox.php' id='" + msg.conversationId + "' title='Read message'>" +
                         "<strong class='blue'>" + msg.autherFirstName + " " + msg.autherLastName + "</strong><br>" + msg.subject +
                    "</a>" +
                "</li>";
        //console.log(msg.conversationId+"-->"+msgli);
        $('#' + targetDiv).append(msgli);
       

        if (msg.isRead != 1)
            unreadCount++;
    }
    
    $('#unreadCountDiv').html(unreadCount);
}

function getDateNicely(str) {
    var nw = moment();
    var sent = moment(str, "YYYY-MM-DD HH:mm:ss");
    //console.log(str + "=" + sent.format("MMMM, DD, HH:mm:ss"));
    var ret;
    var interval = nw.diff(sent);
    if (interval < 86400000) {
        if (interval < 3600000) {
            ret = sent.fromNow(); // xx minutes ago
        }
        else {
            ret = sent.fromNow(); // xx hours ago
        }
    }
    else {
        ret = sent.format("MMM,DD h:mm a"); //actual date
    }
    return ret;
}