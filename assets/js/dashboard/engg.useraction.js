//var baseUrl = 'http://172.16.0.101:8080/lightwebcontroller/LightController';
//var baseUrl = 'http://172.16.0.101:8080/lightwebcontroller/LightController';
var baseUrl = '';
$(document).ready(function () { 
    var loc = ''+location;
    var ip = loc.substring(loc.indexOf('://')+3, loc.indexOf('/',8));
    baseUrl = 'http://'+ip+':8080/lightwebcontroller/LightController';
    });
function openAction(roomObj) {
     if(hotel.user.profile.indexOf("Engineering Executive")==0)
        {
            var buttonObj = new Object();
            var acControl = $('#userActionMarkup');
            $.modal({
                title: 'Perform Actions On Room ' + roomObj.roomName,
                content: 'filter',
                buttons: buttonObj,
                width:400,
                height:300,
                beforeContent: '<div id="actionContainer">',
                afterContent: '</div>',
                buttonsAlign: 'right',
                resizable: false
            }).centerModal(true);
            
            
             $('#actionContainer').html("<div style='width:300px;height:200px'><div id='userActionMarkup' style='z-index:1000'><span class='loader big'></span></div></div>");
                //hotel.user.initializeUserAction($(this).attr("id"));
               
             if(getTempControls(roomObj.roomNo, roomObj.roomName, roomObj.pc_ip, acControl)==false)
             {
                    acControl.html("Room controller could not be contacted. Please try again later.");
             }
       }
       else
       {
           console.log('not an executive');
       }
}


function getTempControls(roomId, roomName, roomIp, acControl)
{
    var retMarkup = false;
    var acControl = $('#userActionMarkup');
    acControl.prepend("<div id='notification' style='display:none'></div>");
    
    console.log(roomId +", "+roomName +", "+roomIp);
    $.ajax(
    {
        //url: apiroot + 'json',
        crossDomain:true,
        url: baseUrl,
        data:{'op':'getstatus',
            'room':roomName,
            'ip':roomIp},
        success: function (data) {
            console.log('recieved temperature controls from server');
            
            try 
            {
                var chunks = data.split('$');
                acControl.html('');
                //TODO - room ip to be extracted from status string sent by server. it is working correct with one room BUT will have problems in case of suite-rooms
                
                for(var i=0 ; i< chunks.length ;i++)
                   { //<input type='checkbox' class='switch' id='AC"+i+"onOff'>
                     
                       var toProcess = chunks[i].split('@')[1];
                       
                       if(toProcess!="" && toProcess!= undefined)
                       {
                           //acControl.append("<fieldset class='fieldset' style='width:355px'>"+
                           //        "<div id='AC"+i+"' >"+
                           //        "<input type='hidden' id='AC"+i+"Ip' value='"+chunks[i].split('@')[0].split(',')[0]+"'> <input type='hidden' id='AC"+i+"Room' value='"+roomName+"'>"+
                           //        "<input class='input' disabled size='1' style='margin-right: 15px;' type='text' id='AC"+i+"Temp'><br><br>"+
                           //        "<span class='button-group' style='margin-left:24%' id='AC"+i+"fan-speed'><label for='AC"+i+"fan-speed-0' class='button grey-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-0' va1ue='0'>Off</label><label for='AC"+i+"fan-speed-1' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-1' va1ue='l'>Low</label><label for='AC"+i+"fan-speed-2' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-2' va1ue='2'>Medium</label><label for='AC"+i+"fan-speed-3' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-3' va1ue='3'>High</label></span>"+
                           //"</div></fieldset>");
                           
                           acControl.append("<div style='z-index:2000'><div class='standard-tabs' style='width:365px'>"+
                           "<ul class='tabs'><li class='active'><a href='#AC"+i+"Control'>Controls</a></li></ul>"+
                                   "<div class='tabs-content'><div id='AC"+i+"Control' class='with-padding tab-active'>"+
                                   "<input type='hidden' id='AC"+i+"Ip' value='"+chunks[i].split('@')[0].split(',')[0]+"'> <input type='hidden' id='AC"+i+"Room' value='"+roomName+"'>"+
                                   "<span>room temp unchanged since: 21 Dec 11:00 PM</span>"+
                                   "<input class='input' disabled size='1' style='margin-right: 15px;width:42px' type='text' id='AC"+i+"Temp'><br><br>"+
                                   "<span style=\'width:30px;height:30px;background:url(\"/backend/assets/images/dashboard/fan.png\") no-repeat fixed center;\'></span>"+
                                   "<span class='button-group' style='margin-left:24%' id='AC"+i+"fan-speed'><label for='AC"+i+"fan-speed-0' class='button grey-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-0' va1ue='0'>Off</label><label for='AC"+i+"fan-speed-1' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-1' va1ue='l'>Low</label><label for='AC"+i+"fan-speed-2' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-2' va1ue='2'>Medium</label><label for='AC"+i+"fan-speed-3' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-3' va1ue='3'>High</label></span>"+
                           "</div></div></div></div>");
                           
                           //
                           //"<ul class='tabs'><li class='active'><a href='#AC"+i+"Control'>Controls</a></li><li><a href='#AC"+i+"Graph'>History</a></li></ul>"+
                                   //"<div class='tabs-content'><div id='AC"+i+"Control' class='with-padding tab-active'>"+
                                   //"<input type='hidden' id='AC"+i+"Ip' value='"+chunks[i].split('@')[0].split(',')[0]+"'> <input type='hidden' id='AC"+i+"Room' value='"+roomName+"'>"+
                                   //"<input class='input' disabled size='1' style='margin-right: 15px;' type='text' id='AC"+i+"Temp'><br><br>"+
                                   //"<span class='button-group' style='margin-left:24%' id='AC"+i+"fan-speed'><label for='AC"+i+"fan-speed-0' class='button grey-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-0' va1ue='0'>Off</label><label for='AC"+i+"fan-speed-1' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-1' va1ue='l'>Low</label><label for='AC"+i+"fan-speed-2' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-2' va1ue='2'>Medium</label><label for='AC"+i+"fan-speed-3' class='button green-active'><input type='radio' name='fan-speed"+i+"' id='AC"+i+"fan-speed-3' va1ue='3'>High</label></span>"+
                           //"</div><div id='AC"+i+"Graph' class='with-padding'><div>graph here</div></div></div></div></div>");
                           
                           $("#AC"+i+"Temp").slider({
                               hideInput:false,
                               min:18,
                               max:28,
                               step:0.5,
                               /*stickToRound:true,*/
                               onChange:(function(idx){ return function(){ updateTemp(idx)};})("#AC"+i)
                               });
                               
                            $("#AC"+i+"onOff").bind("change",{ctr:i},function(event){
                               if($(this).is(":checked"))
                               {
                                   //console.log("#AC"+event.data.ctr+"fan-speed");
                                   $("#AC"+event.data.ctr).show();
                               }
                               else
                               {
                                     $("#AC"+event.data.ctr).hide();
                               }
                                
                            });
                       
                           //console.log('process-->'+  toProcess);
                           processRes( chunks[i].split('@')[1], "AC"+i);
                           retMarkup = true;
                       }//end if
                      
                   }//end for
                
            }
            catch (error) {
                console.log(error);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
    
    return retMarkup;
}

function  processRes(data, acDiv)
{
    var statusArray = data.split(':');
               
    for (var i = 1; i < statusArray.length; i++) {
    
        var record = statusArray[i].split('|')[1];
        // record ->  magenta=0
        var key = record.split('=')[0];
        var value = record.split('=')[1];
        
       
        switch(key) {
            case 'fsl':
                if (value == 100)
                    {
                        console.log('set fan 1');
                        $("label[for='"+acDiv+"fan-speed-1']").click();
                    }
                break;
            case 'fsm':
                if (value == 100)
                    {
                        console.log('set fan 2');
                        $("label[for='"+acDiv+"fan-speed-2']").click();
                    }
                break;
            case 'fsh':
                if (value == 100)
                   {
                       console.log('set fan 3');
                       $("label[for='"+acDiv+"fan-speed-3']").click();
                   }
                break;
            case 'foff':
                if (value == 100)
                   {
                       console.log('set fan 0');
                       $("label[for='"+acDiv+"fan-speed-0']").click();
                   }
                break;
            case 'actemp':
                setTemp(value,'#'+acDiv);
                break;
            case 'roomtemp':
                break;
            default:
                //addLight(key, value, filterArray, room);
                break;
        }
    }
    
    //binding the change of checkboxes AFTER the initial setup
    $('#AC0fan-speed-0').change(function(){ if($('#AC0fan-speed-0').is(':checked')){ updateFan("#AC0","foff"); } });
    $('#AC0fan-speed-1').change(function(){ if($('#AC0fan-speed-1').is(':checked')){ updateFan("#AC0","fsl"); } });
    $('#AC0fan-speed-2').change(function(){ if($('#AC0fan-speed-2').is(':checked')){ updateFan("#AC0","fsm"); } });
    $('#AC0fan-speed-3').change(function(){ if($('#AC0fan-speed-3').is(':checked')){ updateFan("#AC0","fsh"); } });
    
    $('#AC1fan-speed-0').change(function(){ if($('#AC1fan-speed-0').is(':checked')){ updateFan("#AC1","foff"); } });
    $('#AC1fan-speed-1').change(function(){ if($('#AC1fan-speed-1').is(':checked')){ updateFan("#AC1","fsl"); } });
    $('#AC1fan-speed-2').change(function(){ if($('#AC1fan-speed-2').is(':checked')){ updateFan("#AC1","fsm"); } });
    $('#AC1fan-speed-3').change(function(){ if($('#AC1fan-speed-3').is(':checked')){ updateFan("#AC1","fsh"); } });
}

var shouldHitServerForUpdatingTemp = true;
function setTemp(temp, acDiv)
{
    $(acDiv+'Temp').val(temp);
    shouldHitServerForUpdatingTemp = false;
    $(acDiv+'Temp').trigger('change');
    shouldHitServerForUpdatingTemp =true;
}

function updateFan(acDiv, speed)
{
    console.log('fan updating');
    $.ajax(
    {
        crossDomain:true,
        url: baseUrl ,
        data: {'op':'setfcu', 'room': $(acDiv+'Room').val(), 'ip':$(acDiv+'Ip').val(), 'speed': speed},
        success: function (data) {
            console.log('fan changed succesfully');
        },
        error: function (jqXHR, textStatus, errorThrown) {
                $('#notification').html('Room controller could not be contacted. Please try again later.');
                setTimeout(function(){ $('#notification').hide().html(''); },2000);
                console.log(errorThrown);
        }
    });
}
//update temperature slider
function updateTemp(acDiv)
{
  //console.log(acDiv);
    var temp = $(acDiv+'Temp').val();
    if(shouldHitServerForUpdatingTemp)
    {
        $.ajax(
        {
            crossDomain:true,
            url: baseUrl ,
            data: {'op':'settemp', 'room': $(acDiv+'Room').val(), 'ip':$(acDiv+'Ip').val(), 'temp': temp},
            success: function (data) {
                 console.log('temperature changed succesfully');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                    $('#notification').html('Room controller could not be contacted. Please try again later.');
                    setTimeout(function(){ $('#notification').hide().html(''); },2000);
                    console.log(errorThrown);
            }
        });
    }
}
