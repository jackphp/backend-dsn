$(document).ready(function () { initGlobal(); });


var globalUserObject;

function initGlobal() {
    $.ajax(
    {
        url: '../api_dashboard/user_preference.php',
        //url: 'userdata.json',
        data: { "param": "user" },
        isAsynch : false,
        success: function (data) {
            publishConnectionStatus(true); //connected
            //console.log('recieved room data from server'+data);
            try {

                processUser("{ " + data + " }");
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}


function processUser(data)
{
    globalUserObject = eval("("+data+")");
    console.log("current user:"+globalUserObject.user.id);
}


function publishConnectionStatus(isConnected) {
    if (isConnected)
    {
        for (var i = 0; i < 2; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 400);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 400);
        }

        $("#serverConnectionStatus").html("Connection OK");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 200);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 200);
        }
        $("#serverConnectionStatus").html("Error in connection");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy red");
        
        publishServerHealth(isConnected); //connection error automatically means server error
    }
}

function publishServerHealth(isAlive) {
    if (isAlive) {
        for (var i = 0; i < 2; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 400);
            $('#serverHealth').animate({ opacity: 1.0 }, 400);
        }

        $("#serverHealth").html("Server live");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 200);
            $('#serverHealth').animate({ opacity: 1.0 }, 200);
        }
        $("#serverHealth").html("Error at server");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 red");
    }
}

function openProfileEditor() {
    var position = $('#menu').position();

    var actionModal = $.modal({
        title: "Edit your details ",
        content: '&nbsp;',
        beforeContent: '<div id="profileEditForm" style="display:">',
        afterContent: '</div>',
        buttons:
        {
            'Done': {
                //classes: 'icon-tick',
                click: function (modal) { modal.closeModal(); }
            }
        }
    });
    actionModal.setModalPosition(position.left, position.top + 195, false);
    actionModal.setModalContentSize(200,270);
    //var maintIco = isMaint == 1 ? "<label for='name='isMaintSwitch'' class='label'>Maintenance</label><input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' checked class='switch medium'>" : "<label for='name='isMaintSwitch'' class='label'>Maintenance</label><input id='isMaintSwitch' name='isMaintSwitch' type='checkbox' class='switch medium'>";

    $('#profileEditForm').html(profileEditMarkup);
    //prevent scroll behind modal window
    $('.modal').mousewheel(function (event, delta) { return false; });
}

var tipOptions = { position: "top", exclusive: false , onShow: function (target) { setTimeout(function () { target.removeTooltip(false, false); }, 2500); } };
function uploadImage() {

    $("#imageFiles").tooltip("<span class='red'>Select an image file</span>", tipOptions);
}
function changePass() {
    $("#oldPass").tooltip("<span class='red'>did not match</span>", tipOptions);
    $("#newPass").tooltip("<span class='red'>minimum 4 characters</span>", tipOptions);
}
function changeName() {
    $("#userName").tooltip("<span class='red'>Should not be empty</span>", tipOptions);
}

var profileEditMarkup = "<span class='clear-both'></span>" +
                        "<p class='button-height inline-normal-label'>" +
                            "<label for='userName' class='label'>Change Display Name</label>" +
                            "<input type='text' name='userName' id='userName' class='input'>" +
                            "<span class='button icon-tick' onClick='changeName();'></span><br>" +
                            "<label for='oldPass' class='label'>Change Password</label>" +
                            "<input type='password' name='oldPass' id='oldPass' class='input icon-lock' placeholder='Old Password'><br>" +
                            "<input type='password' name='newPass' id='newPass' class='input icon-lock' placeholder='New Password'>" +
                            "<span class='button icon-tick' onClick='changePass();'></span><br>" +
                            "<label for='userRemark' class='label'>Change Profile Image</label>" +
                            "<input type='file' class='file' id='imageFiles' style='width:160px'>" +
                            "<span class='button icon-tick' onClick='uploadImage();'></span>" +
                        "</p>";


//'Save Profile': {
//                classes: 'icon-tick',
//                click: function (modal) {
//                    if ($('#userRemark').val() != '') {
//                        console.log('publish remark');
//                        var isMaintVal = $("#viewSelect").attr('checked') ? 1 : 0;
//                        $.ajax(
//                        {
//                            //url: 'data.json',
//                            url: '../api_dashboard/publish_maint_remark.php',
//                            //data: { user: globalUserObject.user.name, remark: $('#userRemark').val(), room: roomNumber, isMaint: $("#isMaintSwitch").attr('checked') ? 1 : 0 },
//                            success: function (data) {
//                                console.log('success ' + data);
//                                refreshDashBoard(true);
//                                $('.button.icon-cross').click();
//                            },
//                            error: function (jqXHR, textStatus, errorThrown) {
//                                console.log('server returned error');
//                            }
//                        });
//                    }
//                    else {
//                        for (var i = 0; i < 4; i++) {
//                            $('#userRemark').animate({ opacity: 0.1 }, 100);
//                            $('#userRemark').animate({ opacity: 1.0 }, 100);
//                        }
//                    }
//                }
//            },
