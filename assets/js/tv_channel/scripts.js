$(document).ready(function () { initDom(); });
var displayedAlerts = new Array();
var hotelUiModel;

//---------JS hack for adding methods remove to array------------
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};
//---------------------


function initDom() {


    refreshDashBoard(true);
    //refreshMessageTicker();

    $("#viewSelect").change(function () { viewChanged(); });
    //$("#floorinput").change(function () { floorChanged(); });
    $('#filterSelectRadioPref').change(function () { showChanged(); });
    $('#filterSelectRadioAll').change(function () { showChanged(); });
    setTimeout(function(){$('#filterSelectRadioPrefLabel').click();},300);
   // $("#viewSelect").trigger("click");
//    setInterval(function () {
//                refreshDashBoard();
//                //refreshMessageTicker()
//         }, 5000);

    //dev
//   setTimeout(function () {
//        getAlerts();
//        }, 5000);
   


    // deploy
//     setInterval(function () {
//                 getAlerts();
//     }, 15000);
    
}

/**
* Four methods decide what is rendered on page and wht is hidden.
* showChanged - weather all rows should be displayed or filtered rows
* viewChanged - weather to show in grid view or icon view
* filterChanged - when the filters change, this method renders the rooms matching the filter. this is implemented in *.render.js files since this logic varied from page to page
*/

function showChanged() {
    if ($("#filterSelectRadioAll").attr('checked')) //show all rows
    {
        $('.btnWrap').show();
        $('#legendWrapper').show();
        $('#floorSelect').show();
    }
    if ($("#filterSelectRadioPref").attr('checked'))//show rows filtered by saved preference
    {
        $('.btnWrap').hide();
        $('#legendWrapper').hide();
        $('#floorSelect').hide();
        //uncheck all the selected checkboxes
        if ($('#rentedCheck').is(':checked'))
            $('#rentedCheck').click();
        if ($('#unrentedCheck').is(':checked'))
            $('#unrentedCheck').click();
        if ($('#mmrCheck').is(':checked'))
            $('#mmrCheck').click();
        if ($('#dndCheck').is(':checked'))
            $('#dndCheck').click();
        if ($('#maintenanceCheck').is(':checked'))
            $('#maintenanceCheck').click();
      

        openUserPreferredView();
    } 
}



function viewChanged() {
    isIcon =  $("#viewSelect").attr('checked');
    if (isIcon) {
        $("#gridView").attr("style", "width:100%;display:none");
        $("#iconView").attr("style", "width:100%;display:inline-block");
    }
    else {
        $("#gridView").attr("style", "width:100%;display:inline-block");
        $("#iconView").attr("style", "width:100%;display:none");
       // var oTable = $('#gridViewTable').dataTable();
        
       

    }

}

function customeFilter() {}

function openModal(modelTitle, buttons, before, after) {
    if (before == undefined)
        before = '<div class="filterModalContainer">';
    if (after == undefined)
        after = '</div>';

        $.modal({
            title: modelTitle,
            content: '<div></div>',
            buttons: buttons,
            beforeContent: before,
            afterContent: after,
            buttonsAlign: 'right',
            resizable: false
        }).centerModal(true);
    }

function openComplexModal() 
{
    isIcon = $("#viewSelect").attr('checked');
    if (isIcon) {
        openModelForIconView();
    }
    else {
        openModelForGridView();
    }
}


function refreshDashBoard(isAsynch) {
    $.ajax(
    {
        //url: 'data.json',
        url: '../api_dashboard/dashboard_json.php',
        asynch: isAsynch,
        success: function (data) {
            publishConnectionStatus(true); //connected
            console.log('recieved room data from server');
            try {
                
                processJson("{ " + data + " }");
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

var hotel;
function processJson(data) {
   
    hotel = eval("(" + data + ")");
    
    var dashboard =  $("#dashboard"); 
    var gridview = $('#gridViewTableBody');

    dashboard.html(''); //clear the old content
    gridview.html('');

   
    if ($('#floorinput').html() == '') //for the first redering of page, generate the select box for floors
        populateFloorSelect();

    var roomDiv;
    
    
    for (var key in hotel.floors) //iterate through hotel floors
    {
        dashboard.append("<div class='floorSeparator floor" + key + "' style='clear:both'><span class='count left silver-gradient' style='background: none; border: none; color: grey; text-shadow: none; box-shadow: none;background-color:white;margin: -8px 0 0 -6px;'>" + key + "</span></div>");
        //dashboard = $("#dashboard");
        
        if (hotel.floors.hasOwnProperty(key))  //ensure the property is not from prototype/superclass
        {
            if (key != "count") 
            {
                //console.log("floor "+key);
                var thisFloor = hotel.floors[key];
                for(var room in thisFloor)//iterate all rooms on ths floor
                {
                    if(room !="count")//its a room object
                    {
                        //console.log("room" + room + " -> " + thisFloor[room].isMur + "," + thisFloor[room].isDnd);
                        var roomStatus, roomNumber;
                        var thisRoom = thisFloor[room];
                        roomNumber = thisRoom.roomName;
                        var cssClass = 'roomStatus';
                        //determine the css class
                        if (thisRoom.isRented == true) 
                        {
                            //if (thisRoom.isOccupied == true) 
                            //{
                                cssClass = 'rented_occ rented occupied';
                                if (thisRoom.isDnd ==true) 
                                {
                                    cssClass = 'rented_occ_dnd rented occupied dnd';
                                }

                                if (thisRoom.isMur == true) 
                                {
                                    cssClass = 'rented_occ_mur rented occupied mur';
                                }

                            /*}
                            else 
                            {
                                cssClass = 'rented_unocc rented unoccupied';
                                if (thisRoom.isMur == true)
                                {cssClass = 'rented_unocc_mur rented unoccupied mur'; }
                            }*/
                        }
                        else 
                        {
                            cssClass = 'unrented';
                        }

                        if (thisRoom.isMaint == true) 
                        {
                            cssClass = 'maint maintain';
                            //console.log("-------------"+thisRoom.roomName);   
                        }

                        //------------------ to decide the class based on selected floors in floorSelect
                        //=============================populate dashboard view========================================
                                      //room object,     , floor name, dashboard div, gridview div
                        
                        try {

                            populateByView(thisRoom, cssClass, key, dashboard, gridview);
                        }
                        catch (er)
                        {console.log(er);}
                    }
                    else //its a count object
                    {
                    }
                }
            }
            else 
            {
                //console.log("else");
                //process global counts
            }
        }
        dashboard.append("<div class='clearboth' style='clear:both'></div>");
       // dashboard.append("<div class='floorSeparator' style='clear:both'></div>");  //line break at each floor
    }


    if (refreshIconView != undefined)
        refreshIconView();
    if (refreshGridView != undefined)
        refreshGridView();

    //showPreferredView();
}

function populateFloorSelect() {                  //''  
    //console.log('populateFloorSelect');           //'select multiple-as-single easy-multiple-selection check-list replacement' 
    var selectDiv = " ";
    for (var key in hotel.floors) //iterate through hotel floors
    {
       selectDiv = selectDiv + "<option value='"+key+"' selected='selected'>"+key+"</option>";
    }


   $('#floorinput').html(selectDiv);
}


function publishConnectionStatus(isConnected) {
    if (isConnected) {
        for (var i = 0; i < 2; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 400);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 400);
        }

        $("#serverConnectionStatus").html("Connection OK");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverConnectionStatus').animate({ opacity: 0.25 }, 200);
            $('#serverConnectionStatus').animate({ opacity: 1.0 }, 200);
        }
        $("#serverConnectionStatus").html("Error in connection");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy red");

        publishServerHealth(isConnected); //connection error automatically means server error
    }
}

function publishServerHealth(isAlive) {
    if (isAlive) {
        for (var i = 0; i < 2; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 400);
            $('#serverHealth').animate({ opacity: 1.0 }, 400);
        }

        $("#serverHealth").html("Server live");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 green");
    }
    else {
        for (var i = 0; i < 8; i++) {
            $('#serverHealth').animate({ opacity: 0.25 }, 200);
            $('#serverHealth').animate({ opacity: 1.0 }, 200);
        }
        $("#serverHealth").html("Error at server");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 red");
    }
}
