$(document).ready(function () {
    //$("#inboxLink").trigger("click");
    $('#newMessageButton').bind("click", function () { composeMessage(); });
    setTimeout(function () { showInbox(); }, 300);
    setTimeout(function () { showSentBox(); }, 350);
   
    //    var editorTextarea = $('#cleditor');
    //    editorWrapper = editorTextarea.parent();
    //    editor = editorTextarea.cleditor({
    //        width: '97%',
    //        
    //    })[0];

    // Update size when resizing
    //    editorWrapper.sizechange(function () {
    //        editor.refresh();
    //    });
});

function showInbox() {
    console.log("getting mails for user id:" + globalUserObject.user.id);
    $.ajax(
    {
        //url: 'data.json',
        url: '../api_dashboard/getallmessages.php',
        data: { "userid": globalUserObject.user.id, "limit":"0,50" },
        success: function (data) {
            publishConnectionStatus(true); //connected
            try {

                showMessageList( data , 'inboxSummary');
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

function showMessageList(data, targetDiv) {
    $('#' + targetDiv).html('');
    //console.log("---------"+data);
    var list = eval("(" + data + ")");
    var msg;
    var msgli;
    var sentOn;
    var readStatus;
    var unreadCount = 0;
    for (var i = 0; i < list.length; i++) {
        msg = list[i];
        sentOn = getDateNicely(msg.sent);
        readStatus = msg.isRead == 1 ? "" : "<a href='#' id='read" + msg.conversationId + "' class='new-message' title='Mark as read' >New</a>";
        msgli = "<li><span class='message-status'>" +
                        readStatus+
                    "</span>"+
                    "<span class='message-info'>" +
                         "<span class='blue'>"+sentOn+"</span>" +
                    "</span>" +
                    "<a href='#' id='"+msg.conversationId+"' title='Read message'>" +
                         "<strong class='blue'>"+msg.autherFirstName+" "+msg.autherLastName+"</strong><br>"+msg.subject+
                    "</a>"+
                "</li>";
        //console.log(msg.conversationId);
        $('#' + targetDiv).append(msgli);
        $('#' + msg.conversationId).bind("click", { "message": msg }, function (event) { showMessage(event.data.message); })
        
        if (msg.isRead != 1)
            unreadCount++;
    }
    msgli = "<li><a href='#' id='getMoreMessage' title='Get Older Messages'>" +
                         "<strong class='blue icon-list-add'>Get Older Messages</strong></a></li>";
    $('#' + targetDiv).append(msgli);
    $('#unreadCountDiv').html(unreadCount);
}

function showSentBox() {
    console.log("getting sent mails for user id:" + globalUserObject.user.id);
    //http: //172.16.1.152/digivalet_dashboard/api_dashboard/getsentmessages.php?userid=%271%27&limit=0,20
    $.ajax(
    {
        //url: 'data.json',
        url: '../api_dashboard/getsentmessages.php',
        data: { "userid": globalUserObject.user.id, "limit": "0,50" },
        success: function (data) {
            publishConnectionStatus(true); //connected
            console.log('recieved messages from server');
            try {

                showSentMessageList(data, 'sentSummary');
                publishServerHealth(true);
            }
            catch (error) {
                console.log(error);
                publishServerHealth(false);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

function showSentMessageList(data,targetDiv) {
    //console.log("sent messages" + data);
    $('#' + targetDiv).html('');
  
    var list = eval("(" + data + ")");
    var msg;
    var msgli;
    var sentOn;
    var readStatus;
    for (var i = 0; i < list.length; i++) {
        msg = list[i];
        sentOn = getDateNicely(msg.sent);
        readStatus = msg.isRead == 1 ? "" : "<a href='#' class='new-message' title=''>New</a>";
        msgli = "<li><span class='message-status'>" +
                        readStatus +
                    "</span>" +
                    "<span class='message-info'>" +
                         "<span class='blue'>" + sentOn + "</span>" +
                    "</span>" +
                    "<a href='#' id='sent" + msg.conversationId + "' title='Read message'>" +
                         "<strong class='blue'>" + msg.to+"</strong><br>" + msg.subject +
                    "</a>" +
                "</li>";
        //console.log(msg.conversationId);
        $('#' + targetDiv).append(msgli);
        
        //CHANGE IT ------------------------------
        $('#sent' + msg.conversationId).bind("click", { "message": msg }, function (event) { showSentMessage(event.data.message); })
      
    }
}

function showSentMessage(message) {
    //console.log(message.body);
    try {
       
        //--------display related
        $('#messageBox').attr("style", "display:''");
        $('#subjectDiv').html(message.subject);
        $('#fromDiv').html("To:"+message.to);
        $('#bodyContainer').html(message.body);

        //---------event binding
        $('#replyButton').unbind("click");
        $('#forwardButton').unbind("click");
        $('#deleteButton').unbind("click");
        showMessage(message);
        //$('#replyButton').bind("click", { "msg": message }, function (event) { replyMessage(event.data.msg); });
        //$('#forwardButton').bind("click", { "msg": message }, function (event) { forwardMessage(event.data.msg); });
        //$('#deleteButton').bind("click", { "msg": message }, function (event) { deleteMessage(event.data.msg); });

    }
    catch (error) {
        console.log(error);
    }
}


function getDateNicely(str) {
   
    var nw = moment();
    var sent = moment(str, "YYYY-MM-DD HH:mm:ss");
    //console.log(str + "=" + sent.format("MMMM, DD, HH:mm:ss"));
    var ret;
    var interval = nw.diff(sent);
    if ( interval < 86400000) {
        if (interval <3600000) {
            ret = sent.fromNow(); // xx minutes ago
        }
        else {
            ret = sent.fromNow(); // xx hours ago
        }
    }
    else {
        ret = sent.format("MMM,DD h:mm a"); //actual date
    }
    return ret;
}
function showMessage(message) {
    try {
        console.log(message.conversationId);
        setAsRead(message);
        //--------display related
        $('#messageBox').attr("style","display:''");
        $('#subjectDiv').html(message.subject);
        $('#fromDiv').html(message.autherFirstName + " " + message.autherLastName);
        getMessageBody(message.conversationId);

        //---------event binding
        $('#replyButton').unbind("click");
        $('#forwardButton').unbind("click");
        $('#deleteButton').unbind("click");
        $('#replyButton').bind("click", {"msg":message}, function (event) { replyMessage(event.data.msg); });
        $('#forwardButton').bind("click", {"msg":message}, function (event) { forwardMessage(event.data.msg); });
        $('#deleteButton').bind("click", {"msg":message}, function (event) { deleteMessage(event.data.msg); });

    }
    catch (error) {
        console.log(error);
    }
}

function getMessageBody(cid) {

    $.ajax(
    {
        //url: 'data.json',
        url: '../api_dashboard/getmessage',
        data: { "conversationId": cid },
        success: function (data) {
            console.log("got mail trail of" + cid);
            var trail = eval("(" + data + ")");
            var container = $('#bodyContainer');
            container.html('');
            for (var i = 0; i < trail.length; i++) {
                var msg = trail[i];

                if (i == 0) {
                    
                    container.append(msg.body);

                } else {
                    container.append("<div id='trail" + msg.conversationId + "'>" + msg.body + "</div>");
                    container = $('#trail' + msg.conversationId);
                    container.attr("class", "left-border grey");
                }

            }

            publishConnectionStatus(true); //connected
            publishServerHealth(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

function replyMessage(message) {
    console.log(message);
    $('#messageDisplay').message('Message sent', { classes: ['icon-info'] });
    setTimeout(function () { $('#messageDisplay').clearMessage() }, 1000);
    $.modal({
        title: 'Reply to message',
        content: replyFormTemplate,
        buttons: {
            "Reply": {
                classes: 'icon-reply',
                click: function (model) {
                    //         (senderId,                 recipients, subject,                   body,                       inResponse                        ,recipientIds             )
                    sendMessage(globalUserObject.user.id, ''         , $('#mailSubjectDiv').val(), $('#mailBodyDiv').html(), $('#replyToConversationId').html(),$('#replyToUserId').html());
                } 
            },
            'Cancel': { classes: 'icon-cross', click: function (modal) { modal.closeModal(); } }

        },
        beforeContent: '<div id="replyForm">',
        afterContent: '</div>',
        buttonsAlign: 'center',
        resizable: false
    });

    $('#replyFormTemplate').attr("style", "display:''");
    $('#mailSubjectDiv').val("Re:" + message.subject);
    $('#mailToDiv').val($('#fromDiv').html());
    $('#replyToUserId').html(message.authorId);
    $('#replyToConversationId').html(message.conversationId);
}

function forwardMessage(message) {
    console.log("forward" + message.conversationId);
    $.modal({
        title: 'Forward the message:' + message.conversationId,
        content: composeFormTemplate,
        buttons: {
            "Forward": {
                classes: 'icon-extract',
                click: function (model) {
                    //         (senderId,                 recipients,          subject,                   body,                       inResponse, recipientIds)
                    sendMessage(globalUserObject.user.id, $('#mailToDiv').val(), $('#mailSubjectDiv').val(), $('#mailBodyDiv').html(),message.conversationId,'');
                } 
            },
            'Cancel': { classes: 'icon-cross', click: function (modal) { modal.closeModal(); } }

        },
        beforeContent: '<div id="forwardForm">',
        afterContent: '</div>',
        buttonsAlign: 'center',
        resizable: false
    });

    $('#composeFormTemplate').attr("style", "display:''");
    $('#mailSubjectDiv').val("Fw:" + message.subject);
    $('#mailBodyDiv').html($('#bodyContainer').html());


}

function deleteMessage(message) {
    console.log("delete" + message.conversationId);
   
    $.ajax(
    {
        //url: 'data.json',
        url: '../deletemessage.php',
        data: { "conversationId": message.conversationId },
        success: function (data) {
            console.log("success as delete" + message.conversationId);
            publishConnectionStatus(true); //connected
            publishServerHealth(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });

}

function setAsRead(message) {
    console.log("set as read" + message.conversationId);
    $.ajax(
    {
        //url: 'data.json',
        url: '../readmessage.php',
        data: { "conversationId": message.conversationId },
        success: function (data) {
            console.log("success as read" + message.conversationId);
            $('#read' + message.conversationId).attr("style","display:none");
            publishConnectionStatus(true); //connected
            publishServerHealth(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            publishConnectionStatus(false); //not connected or some error
        }
    });
}
var userList=new Array();
function composeMessage() {
    console.log("composeMessage");
    $.ajax(
    {
        //url: "username.json",
        url: '../api_dashboard/getusername.php',
        data: { 'term': '\%\%' },
        success: function (data) {

            console.log("got user names");
            try {
                userList = eval( data );
            }
            catch (er)
            { console.log(er); }
            //publishConnectionStatus(true); //connected
            //publishServerHealth(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });

    var modalObj;
    modalObj = $.modal({
        title: 'Compose a message',
        content: "content",
        onClose:function(){$('#ui-datepicker-div').appendTo($("body"))},
        buttons: {
            "Send": {
               
                classes: 'icon-arrow',
                click: function (model) {
                    var sendTime = '';
                    if ($("#sendLater").attr('checked'))
                        sendTime = $('#sendTimeDiv').val();                                                               //.replace(/\n/gi, "<br/>")                          
                    sendMessage(modalObj,globalUserObject.user.id, $('#mailToDiv').val(), $('#mailSubjectDiv').val(), $('#mailBodyDiv').val(), '', '', sendTime);
                    console.log('sent');
                }
            },
            'Cancel': { classes: 'icon-cross', click: function (modal) { modal.closeModal(); } }

        },
        beforeContent: '<div id="composeForm">',
        afterContent: '</div>',
        buttonsAlign: 'center',
        resizable: false
    });

    $('#composeForm').html(composeFormTemplate);
    $('#composeForm>#composeFormTemplate').attr("style", "display:''");

    setTimeout(function () {
        $('#mailToDiv').autocomplete({
            source: userList,
            select: function (event, ui) {
                console.log(ui.item ?
                    "Selected: " + ui.item.value + " aka " + ui.item.label :
                    "Nothing selected, input was " + this.value);
                $('#mailBodyDiv').val('Dear ' + ui.item.label);
            },
            appendTo: $("#composeFormTemplate")
        });
    }, 300);

    $('#sendTimeDiv').datetimepicker({
         hourGrid: 4,
        minuteGrid: 15,
        stepMinute: 5,
        minDate:0,
        beforeShow: function () {
            $('#ui-datepicker-div').appendTo($("#composeFormTemplate"))
        }
    });

    $("#sendLater").click(function () {
        if ($("#sendLater").attr("checked")) {
            $("#sendTimeDiv").removeAttr("disabled");
            $("#sendTimeDiv").trigger("focus");
           
        }
        else {
            $("#sendTimeDiv").attr("disabled", "");
        }
    });
}


function sendMessage(modalObj,senderId, recipients, subject, body, inResponse, recipientIds, sendTime) {
    console.log("send msg called " + $('#mailToDiv').val() + "," + $('#mailSubjectDiv').val() + "," + $('#mailBodyDiv').val());

    $.ajax(
    {
        //url: 'data.json',
        url: '../sendmail.php',
        type: 'post',
        data: { 'userid': senderId, 'recipientlist': recipients, 'subject': subject,
            'body': body, 'inresponseof': inResponse, 'recipientidlist': recipientIds,
            'sendLater': sendTime
        },
        success: function (data) {
            console.log("successfully sent" + data);
            modalObj.closeModal();
            $('#messageDisplay').message('Message sent', { classes: ['icon-info'] });
            setTimeout(function () { $('#messageDisplay').clearMessage() }, 1000);
            publishConnectionStatus(true); //connected
            publishServerHealth(true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.button.icon-arrow').tooltip('Message could not be sent due to error.<br>Please try again.', {'exclusive':true,'removeOnBlur':true});
            publishConnectionStatus(false); //not connected or some error
        }
    });
}

var composeFormTemplate =  
"<div id='composeFormTemplate' style='display:none'>"+
    "<fieldset class='fieldset fields-list' style='width:400px'>"+
        "<legend class='legend'> </legend>"+
	    "<div class='field-block button-height'>"+
		    "<label for='mailToDiv' class='label'><b>To</b></label>"+
		    "<input type='text' name='mailToDiv' id='mailToDiv' class='input'><br>" +
            "<label for='mailSubjectDiv' class='label'><b>Subject</b></label>"+
		    "<input type='text' name='mailSubjectDiv' id='mailSubjectDiv' class='input'><br>" +
            "<label for='sendLater' class='label'><b>Send Later</b></label>" +
		    "<input type='checkbox' name='sendLater' id='sendLater' class='input' style='margin-right:6px;margin-left:-19px'></span><input type='text' id='sendTimeDiv' class='input left-icon icon-clock' disabled></div><br><br>" +
        "<div><textarea class='input full-width' name='mailBodyDiv' id='mailBodyDiv' class='input' rows='7'></textarea></div>" +
    "</fieldset></div>";

var replyFormTemplate =
"<div id='replyFormTemplate' style='display:none'>" +
    "<fieldset class='fieldset fields-list' style='width:400px'>" +
        "<legend class='legend'> </legend>" +
	    "<div class='field-block button-height'>" +
		    "<label for='mailToDiv' class='label'><b>To</b></label>" +
		    "<input type='text' name='mailToDiv' id='mailToDiv' class='input' disabled></div>" +
        "<div class='field-block button-height'>" +
		    "<label for='mailSubjectDiv' class='label'><b>Subject</b></label>" +
		    "<input type='text' name='mailSubjectDiv' id='mailSubjectDiv' class='input' disabled></div><br><br><br>" +
        "<div><textarea class='input full-width' name='mailBodyDiv' id='mailBodyDiv' class='input' rows='7'></textarea></div>" +
        "<div id='replyToUserId' style='display:none'></div>" +
        "<div id='replyToConversationId' style='display:none'></div>" +
    "</fieldset></div>";


