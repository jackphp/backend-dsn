<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of activity
 *
 * @author pbsl
 */
class activity extends CI_Controller {

	var $data = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = 'Users activity';
		$this->load->model('activity_model');
		$perm = array('access activity');
		$this->user->set_access_permission($perm);

		$menu = array('menu'=>'User Management', 'links'=>array(array('label'=>'Audit Trail', 'path'=>'activity', 'permission'=>'access activity', 'menu_order'=>6)));
		$this->menu->set_menu($menu);
	}


	public function index() {
		if($this->user->is_user_access()) {

			$header = array(
			array('data'=>'#'),
			array('data'=>'User'),
			array('data'=>'Total activity'),
                     'class'=>'listheading');         
			$q = $this->activity_model->get_users_activity();
			$rows = array();
			$i=0;
			foreach ($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('activity/user/'.$row->uid, $row->name)),
				array('data'=>anchor('activity/user/'.$row->uid, $row->ctr), 'attributes'=>array('align'=>'center')),
                            'attributes'=>array('class'=>$class),
				);
			}

			$this->data['page_heading'] = 'Users activities';
			$this->data['activity_list'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/activity_list_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function user($uid) {
		if($this->user->is_user_access()) {
			if($uid == "" || $uid == 0) {
				redirect('activity');
			}

			$user = $this->user->user_load($uid);
			$username = $user[0]['name'];
			$this->data['back'] = '<div style="margin-bottom: 5px; margin-left: 5px;"><div class="tab-button tab-button-primary" style="float: left;" id="send_letter" onclick="window.location.href=\''.base_url().'admin/user/edit/'.$uid.'\'"><div tipval="Back to '.$username.' Profile" class="tooltip"><label>Back</label></div></div><div style="clear: both;"></div></div>';

			//            $config['total_rows'] = $this->smodels_model->get_model_count($did);
			//            $config['per_page'] = 20;
			//            $config['full_tag_open'] = '<p>';
			//            $config['full_tag_close'] = '</p>';
			//            $config['uri_segment'] = 4;
			//            $config['base_url'] = base_url().'index.php/activity/user/'.$uid;
			//
			//            $this->pagination->initialize($config);
			//
			//            $this->data['pager_link'] = $this->pagination->create_links();

			$header = array(
			array('data'=>'#'),
			array('data'=>'Module'),
			array('data'=>'Description'),
			array('data'=>'IP'),
			array('data'=>'Date'),
                   'class'=>'listheading');

			$q = $this->activity_model->get_user_activity($uid);
			$rows = array();
			 
			$i=0;
			foreach ($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>$row->module),
				array('data'=>$row->message),
				array('data'=>$row->ip),
				array('data'=>format_date($row->access_time)),
                            'attributes'=>array('class'=>$class)
				);
			}

			$this->data['page_heading'] = $username."'s activity";
			$this->data['title'] = $this->data['page_heading'];
			$this->data['activity_list'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/activity_list_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}
}
?>