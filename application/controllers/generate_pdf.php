<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class generate_pdf extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("room_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Upload Shopping PDF';
		$perm = array('update shopping pdf');
		$this->load->library('pagination');
		$this->user->set_access_permission($perm);
	}


	public function index() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->data['page_heading']='Upload PDF';

			//Upload Start
			$this->data['form_open'] = form_open_multipart('generate_pdf/submit2','name="first" id="first"');
			$this->data['form_close'] = form_close();
			$this->data['file'] = form_upload("file",'','size=18 id="filen" class="file" style="position: relative;z-index: 2;text-align: right;" ');
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			//$this->data['page_heading'] = 'Video Song SubCategory';
			//$this->data['cat_id']=$id;
			//$q = $this->video_song_category_model->get_subcategory($id);
			//$this->data['result']=$q->result();
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			$vars = $this->theme->theme_vars('pages/upload_pdf', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 
	public function submit2() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$name= $this->input->post('pdf');
			//print_r($_POST);die();
			//  print_r($_FILES);die();
			if($_FILES['file']['name']!=''){
				$_FILES['file']['name'];
				if($name=='spa'){
					$_FILES['file']['name']="spa.pdf";
				}
				else if ($name=='shopping') {
					$_FILES['file']['name']="shopping.pdf";
				}
				else{
					$_FILES['file']['name']="WelcomSlumber.pdf";
				}
				// $_FILES['file']['name']="shopping.pdf";
				$config['upload_path'] = PDF_UPLOAD;
				$config['overwrite'] = true;
				 
				$config['allowed_types'] = 'pdf';
				$config['is_image'] = 0;
				$config['max_size']  = '51200';
				//                        print_r($_FILES);die();

				$this->load->library("upload", $config);
				if(!$this->upload->do_upload('file')) {
					$err=$this->upload->display_errors();
					$this->message->set($err, "error", TRUE);
					//  echo $this->upload->display_errors();
				}
				else {
					exec("chmod -R 777 ".PDF_UPLOAD);
					$d= $this->upload->data();

					$this->message->set("File Uploaded successfully.", "success", TRUE);
					redirect('generate_pdf');

				}
				redirect('generate_pdf');
			}
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	function generate() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			send_request(SHOPPING_PUSH_UPDATE);
			$this->message->set("Update successfully sent to rooms.", "Success", TRUE);
			redirect('generate_pdf');
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 

}// End Of class
?>
