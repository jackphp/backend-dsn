<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class room extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("room_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Room Type';
		$perm = array('room management');
		$this->load->library('pagination');
		$this->user->set_access_permission($perm);
	}


	//    public function index($offset=0, $limit=30) {
	//        if($this->user->is_user_access()) {
	//            $config['base_url'] = base_url()."room";
	//            $config['total_rows'] = $this->db->count_all("rooms");
	//            $config['per_page'] = $limit;
	//            $config['num_links'] = 1;
	//            $config['first_link'] = "First";
	//            $config['last_link'] = "Last";
	//            $config['uri_segment'] = 3;
	//
	//            $this->pagination->initialize($config);
	//            $this->data['links'] = $this->pagination->create_links();
	//
	//            $this->data['page_heading'] = 'Food Addons';
	//
	//            $this->data['addons'] = $this->food_addon_model->get_addons($offset, $limit);
	//            $vars = $this->theme->theme_vars('pages/food_addons/addon_list', $this->data);
	//            $this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	//
	//        }
	//        else {
	//            $this->user->user_access_denied();
	//        }
	//    }


	 
	public function index() {
		if($this->user->is_user_access()) {

			$floor_query=$this->room_model->get_floor();
			$room_query=$this->room_model->get_list();
			$this->data['floor_list'] = $floor_query;
			$this->data['room_list'] = $room_query;
			$this->data['page_heading']='Bulk Room Entry Form';
			$vars = $this->theme->theme_vars('pages/rooms/room_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function generate()
	{
		if($this->user->is_user_access()) {
			$floor_query=$this->room_model->get_floor();
			$room_query=$this->room_model->get_list();
			$this->data['floor_list'] = $floor_query;
			$this->data['room_list'] = $room_query;
			$this->data['from']=$this->input->post('from');
			$this->data['to']=$this->input->post('to');
			$this->data['room_type']=$this->input->post('room1');
			$this->data['floor_type']=$this->input->post('floor1');
			$this->data['controller_type']=$this->input->post('controller1');
			$this->data['page_heading']='Bulk Room Entry Form';
			$vars = $this->theme->theme_vars('pages/rooms/room_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}
	public function rooms(){
		$rs=$this->room_model->rooms();
		echo $rs;
	}
	public function check_room($from,$to){
		$room=$this->room_model->chk($from,$to);
	}

	public function save_room() {
		//echo  print_r($this->input->post('room'));echo '\n';
		// echo  print_r($this->input->post('floor'));
		//  echo  print_r($this->input->post('controller'));
		// echo  print_r($this->input->post('no'));
		// echo  print_r($this->input->post('ip'));
		// echo  print_r($this->input->post('mac'));
		if($this->user->is_user_access()) {
			$room=$this->input->post('room');
			$floor=$this->input->post('floor');
			$controller=$this->input->post('controller');
			$ip=$this->input->post('ip');
			$mac=$this->input->post('mac');
			$friendly=$this->input->post('friendly');
			$bed=$this->input->post('bed');
			$no=$this->input->post('no');
			 
			$count=count($this->input->post('room'));
			$name ='Room  No -';
			for($i=0;$i<$count;$i++)
			{
				$this->room_model->insert_bulk_room($room[$i],$floor[$i],$controller[$i],$ip[$i],$mac[$i],$friendly[$i],$bed[$i],$no[$i]);
				$name.=$no[$i];
				$name.=',';
			}
			$name.=' are created successfully';
			$this->watchdog->save('bulk room add', 'Room', 'bulk_room_add',0,$name);
			redirect('room/room_list');
		}
		else {
			$this->user->user_access_denied();
		}
	}
	public function update_room() {
		//echo  print_r($this->input->post('room'));echo '\n';
		// echo  print_r($this->input->post('floor'));
		//  echo  print_r($this->input->post('controller'));
		// echo  print_r($this->input->post('no'));
		// echo  print_r($this->input->post('ip'));
		// echo  print_r($this->input->post('mac'));
		if($this->user->is_user_access()) {
			$id=$this->input->post('id');
			$room=$this->input->post('room_no');
			$room_type=$this->input->post('room_type');
			$floor=$this->input->post('floor');
			$controller=$this->input->post('controller');
			$ip=$this->input->post('ip');
			$mac=$this->input->post('mac');
			$friendly=$this->input->post('friendly');
			$bed=$this->input->post('bed');
			 
			$this->watchdog->save('edit', 'Room', 'room_edit',$id,$room);
			$this->room_model->update_room($id,$room,$room_type,$floor,$controller,$ip,$mac,$friendly,$bed);
			redirect('room/room_list');
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function save() {
		if($this->user->is_user_access()) {
			if($this->input->post('id')) {
				$id=$this->input->post('id');
				$room_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				 
				$this->watchdog->save('edit', 'Room', 'room_type_edit',$id,$room_type);
				$this->room_model->update($room_type,$des,$id);

			}
			else {
				$room_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				 
				$this->watchdog->save('add', 'Room', 'room_type_add',0,$room_type);
				$this->room_model->insert($room_type,$des);
				 
				 
				 
				 

			}
			redirect('room_type');
			 
			 
		}
		else {
			$this->user->user_access_denied();
		}


	}
	public function list_edit($id=0){
		if($this->user->is_user_access()) {
			if($id>0)
			{
				 
				$res=$this->room_model->get_room($id);
				 
			}
			 
			$floor_query=$this->room_model->get_floor();
			 
			$this->data['floor_list'] = $floor_query;

			 
			$query=$this->room_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['id'] = $id;
			$this->data['edit'] = $res;
			//$query=$this->room_model->get_room_list();
			$this->data['room_list'] = $query;
			$this->data['page_heading']='Details of Room';
			$vars = $this->theme->theme_vars('pages/rooms/edit_room_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}


		 
	}
	public function edit($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			{

				$res=$this->room_model->get($id);
				$this->data['edit'] = $res;

			}
			 
			$query=$this->room_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			 
			$this->data['page_heading']='Room Type';
			$vars = $this->theme->theme_vars('pages/rooms/room_type_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}



	}
	public function delete($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			$name=$this->room_model->get_room_type($id);
			$nam=$name->row();
			$name=$nam->room_type;
			{
				$this->watchdog->save('delete', 'Room', 'room_type_delete',$id,$name);
				$query=$this->room_model->delete($id);
				redirect('room_type');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}
	public function delete_room($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			$name=$this->room_model->get_room_nam($id);
			$nam=$name->row();
			$name=$nam->room_no;
			 
			 
			{
				$this->watchdog->save('delete', 'Room', 'room_delete',$id,$name);
				$query=$this->room_model->delete_room($id);
				 
				redirect('room/room_list');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}
	function delete_all(){
		if($this->user->is_user_access()) {
			$del=$this->input->post('check');
			$room_type='Room type  ';
			foreach($del as $row) {
				$room_no=$this->room_model->get_no($row);
				$rooms=$room_no->row();
				$room=$rooms->room_no;
				$room_type.=$room;
				$room_type.=',';
				//$query=$this->db->query("delete from  room_types where room_type_id='".$row."'");

			}
			$room_type.=' are deleted ';
			$this->watchdog->save('delete', 'Room', 'room_type_selected_delete',$id,$room_type);
			$query=$this->room_model->delete_all($del);
			redirect('room_type');
		}
		else {
			$this->user->user_access_denied();
		}


	}

	function delete_all_room(){
		if($this->user->is_user_access()) {
			$del=$this->input->post('check');
			 
			$room='Room No ';
			foreach($del as $row) {
				$room_no=$this->room_model->get_no($row);
				$r=$room_no->row();
				$rooms=$r->room_no;
				$room.=$rooms;
				$room.=',';
				 
				 
			}
			$room.=' are deleted.';
			$this->watchdog->save('delete', 'Room', 'room_selected_delete',0,$room);
			$query=$this->room_model->delete_all_room($del);
			redirect('room/room_list');
		}
		else {
			$this->user->user_access_denied();
		}
	}
	 
	function room_list($offset=0, $limit=20){

		if($this->user->is_user_access()) {
			$config['base_url'] = base_url()."room/room_list";
			$config['total_rows'] = $this->db->count_all("rooms");
			$config['per_page'] = $limit;
			$config['num_links'] = 3;
			//            $config['use_page_numbers'] = TRUE;
			//            $config['full_tag_open'] = '<b style="font-size:12px;">';
			//
			//            $config['full_tag_close'] = '</b>';
			//            $config['first_link'] = "First";
			//            $config['cur_tag_open'] = '<b style="color:blue;padding-left:6px;padding-right:6px;font-size:16px;" >';
			//
			//
			//            $config['cur_tag_close'] = '</b>';
			//
			////            $config['cur_tag_open'] = '<p style="font-color:red;font-size:16px;">';
			////            $config['cur_tag_close'] = '</p>';
			//            $config['next_link'] = '&gt;';
			//            $config['last_link'] = "Last";
			$config['uri_segment'] = 3;
			//            $config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$this->data['links'] = $this->pagination->create_links();
			$query=$this->room_model->get_room_list($offset,$limit);
			$this->data['room_list'] = $query;
			$this->data['page_heading']='Room Details';
			$vars = $this->theme->theme_vars('pages/rooms/room_list_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}
	 

}// End Of class
?>
