<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class generate_pdf extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("room_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Room Type';
		$perm = array('update shopping pdf');
		$this->load->library('pagination');
		$this->user->set_access_permission($perm);
	}


	public function index() {
		if($this->user->is_user_access()) {
			$this->data['page_heading']='Upload Shopping PDF';

			//Upload Start
			$this->data['form_open'] = form_open_multipart('generate_pdf/submit2','name="first" id="first"');
			$this->data['form_close'] = form_close();
			$this->data['file'] = form_upload("file",'','size=18 id="filen"');
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			//$this->data['page_heading'] = 'Video Song SubCategory';
			//$this->data['cat_id']=$id;
			//$q = $this->video_song_category_model->get_subcategory($id);
			//$this->data['result']=$q->result();
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			$vars = $this->theme->theme_vars('pages/upload_pdf', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}
	 
	public function submit2() {

		if($this->user->is_user_access()) {
			//  print_r($_FILES);die();
			if($_FILES['file']['name']!=''){
				$_FILES['file']['name'];

				$_FILES['file']['name']="shopping.pdf";
				$config['upload_path'] = PDF_UPLOAD;
				$config['overwrite'] = true;
				 
				$config['allowed_types'] = 'pdf';
				$config['is_image'] = 0;
				$config['max_size']  = '51200';
				//                        print_r($_FILES);die();

				$this->load->library("upload", $config);
				if(!$this->upload->do_upload('file')) {
					$err=$this->upload->display_errors();
					$this->message->set($err, "error", TRUE);
					//  echo $this->upload->display_errors();
				}
				else {
					exec("chmod -R 777 ".PDF_UPLOAD);
					$d= $this->upload->data();

					$this->message->set("File Uploaded successfully.", "success", TRUE);
					redirect('generate_pdf');

				}
				redirect('generate_pdf');
			}
			 
			else {
				$this->user->user_access_denied();
			}
		}

	}

	function generate() {
		send_request(SHOPPING_PUSH_UPDATE);
		$this->watchdog->save('update', 'Shopping PDF', 'shopping_pdf_update',0,'Commit Changes');
		$this->message->set("Update successfully sent to rooms.", "Success", TRUE);
		redirect('generate_pdf');
	}
	 

}// End Of class
?>
