<?php
/**
 * Description of service_dir
 *
 * @author pbsl
 */
class service_dir extends CI_Controller {
	var $data = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = 'Servise Directory';
		$perm = array('manage service directory');
		$this->user->set_access_permission($perm);
		$this->load->model('sdir_model');
	}

	public function index() {
		$id=$this->session->userdata("user_id");
		if($id){
			$q = $this->common_model->get_records('sdirectory');
			$pages = array(''=>'--Select--');
			foreach($q->result() as $row) {
				$pages[$row->id] = $row->name;
			}
			$this->data['page_heading'] = 'Service Directory';
			$this->data['form_open'] = form_open('service_dir/submit');
			$this->data['form_close'] = form_close();
			$this->load->model("dashboard_login");
			$this->data['left']=$this->dashboard_login->user_profile();
			$this->data['page_id'] = form_dropdown('page_id', $pages, '', 'id="edit_page_id" class="select"');
			$this->data['content'] = form_textarea(array('name'=>'content', 'rows'=>'40', 'cols'=>'100'), '', 'id="edit_content" style="readonly:true" class="input"');

			$vars = $this->theme->theme_vars('pages/service_dir/page', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	public function get_page($id) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->common_model->get_record_by_condition('sdirectory', 'id='.$id);
			if($q->num_rows() > 0) {
				$rs = $q->result();
				$row = $rs[0];
				$content = '<style type="text/css">
        p{font-size: 18px; font-family: "HelveticaNeue Regular, HelveticaNeue"; text-align: justify;}
        td{font-size: 18px; font-family: "HelveticaNeue Regular, HelveticaNeue"; text-align: justify;}
        h2{font-size: 35px; font-family:"HelveticaNeue Regular, HelveticaNeue"; color:#916D00;}
        li{font-size: 18px; font-family:"HelveticaNeue Regular, HelveticaNeue";}
        b{font-size: 18px; font-family:"HelveticaNeue Regular, HelveticaNeue"; color:#916D00; font-weight:bold;}
        h4{font-family:"HelveticaNeue Regular, HelveticaNeue"; color:#916D00;}
        h3{font-family:"HelveticaNeue Regular, HelveticaNeue"; color:#916D00;}
        .small{font-size: 14px; font-family:"HelveticaNeue Regular, HelveticaNeue";}
    </style>';
				$content .= stripcslashes($row->content);
				echo form_textarea(array('name'=>'content', 'rows'=>'40', 'cols'=>'100'),  $content, 'id="edit_content"');
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}


	public function submit() {
		$page_id = $this->input->post("page_id");
		$content = $this->input->post("content");

		if($page_id > 0) {
			$param = array('content'=> mysql_real_escape_string( $content ));
			if($this->sdir_model->save_sdir_content($param, $page_id)) {
				$this->message->set("Content has been saved.", "success", true);
			}
			else {
				$this->message->set("Some error in content saving.", "error", true);
			}

			redirect('service_dir');
		}
		else {
			redirect('service_dir');
		}

	}
}
?>
