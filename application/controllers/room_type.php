<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class room_type extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("room_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Room Type';
		$perm = array('room management');
		$this->user->set_access_permission($perm);



	}

	public function index() {
		if($this->user->is_user_access()) {
			 
			$query=$this->room_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$this->data['page_heading']='Room Type';
			$vars = $this->theme->theme_vars('pages/rooms/room_type_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function save() {
		if($this->user->is_user_access()) {
			if($this->input->post('id')) {
				$id=$this->input->post('id');
				$room_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				$this->watchdog->save('edit', 'Room', 'room_type_edit',$id,$room_type);
				$this->room_model->update($room_type,$des,$id);

			}
			else {
				$room_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				$this->watchdog->save('add', 'Room', 'room_type_add',0,$room_type);
				$this->room_model->insert($room_type,$des);
				 
				 
				 
				 

			}
			redirect('room_type');
			 
			 
		}
		else {
			$this->user->user_access_denied();
		}


	}
	public function edit($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			{

				 
				$res=$this->room_model->get($id);
				 
				 
				 
			}
			 
			$query=$this->room_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$this->data['edit'] = $res;
			$this->data['page_heading']='Room Type';
			$vars = $this->theme->theme_vars('pages/rooms/room_type_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}



	}
	public function delete($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			$name=$this->room_model->get_room_type($id);
			$nam=$name->row();
			$name=$nam->room_type;
			{
				$this->watchdog->save('delete', 'Room', 'room_type_delete',$id,$name);
				$query=$this->room_model->delete($id);
				redirect('room_type');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}
	function delete_all(){
		if($this->user->is_user_access()) {
			$del=$this->input->post('check');
			$room_type='Room type  ';
			foreach($del as $row) {
				$room_type.=$row;
				$room_type.=',';
				//$query=$this->db->query("delete from  room_types where room_type_id='".$row."'");

			}
			$room_type.=' are deleted ';
			$this->watchdog->save('delete', 'Room', 'room_type_selected_delete',$id,$room_type);
			$query=$this->room_model->delete_all($del);
			redirect('room_type');
		}
		else {
			$this->user->user_access_denied();
		}


	}


}// End Of class
?>
