<?php

class User_Form extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('user_feature_model');
		$this->load->model('dashboard_login');
	}

	public function create_user() {
		$user_page = $this->uri->segment(2);
		$id = $this->session->userdata("user_id");
		if ($id == "1") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			// $data['page'] = $this->dashboard_login->getPageTitle();
			$this->load->model('user_feature_model');
			$res = $this->user_feature_model->getAllProfile();
			$data['user_profile'] = $res;
			$this->load->view('user_pages/create_user.php', $data);
		} else {
			redirect('digivalet_dashboard/login');
		}
	}

	public function user_insert() {
		//print_r($_POST);die();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[ConfirmPassword]|min_length[6]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('profile_id', 'Profile', 'trim|required|number|xss_clean');
		if ($this->form_validation->run() != False) {
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->insert_user_module($_POST);
			redirect('user_form/all_user');
		} else {
			$user_page = $this->uri->segment(2);
			$id = $this->session->userdata("user_id");
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			// $data['page'] = $this->dashboard_login->getPageTitle();
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->getAllProfile();
			$data['user_profile'] = $result;
			$this->load->view('user_pages/create_user.php', $data);
		}
	}

	public function all_user() {
		$id = $this->session->userdata("user_id");
		if ($id == "1") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$user_id = $this->uri->segment(3);
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->getAllUser();
			$data['user'] = $result;
			$this->load->view('user_pages/all_user.php', $data);
		} else {
			redirect('digivalet_dashboard/login');
		}
	}

	public function user_edit() {
		$id = $this->session->userdata("user_id");
		if ($id = "1") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$user_id = $this->uri->segment(3);
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->editUser($user_id);
			$profile = $this->user_feature_model->getAllProfile();
			$data['user_profile'] = $profile;
			$data['user'] = $result;
			$this->load->view('user_pages/user_edit.php', $data);
		} else {
			redirect('digivalet_dashboard/login');
		}
	}

	public function user_update() {

		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required|xss_clean');
		if (isset($_POST['password'])) {
			$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[ConfirmPassword]|min_length[6]|max_length[20]|xss_clean');


			$this->form_validation->set_rules('Confirm Password', 'Comfirm Password', 'trim|alpha_dash|xss_clean');
		}
		$this->form_validation->set_rules('profile_id', 'Profile Assign', 'trim|required|number|xss_clean');
		if ($this->form_validation->run() != False) {

			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->update_user($_POST);
			redirect('user_form/all_user');
		} else {

			$user_id = $_POST['user_id'];
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->editUser($user_id);
			$profile = $this->user_feature_model->getAllProfile();
			$data['user_profile'] = $profile;
			$data['user'] = $result;
			$this->load->view('user_pages/user_edit.php', $data);
		}
	}

	public function delete_user() {
		$this->load->model("dashboard_login");
		$result = $this->dashboard_login->getUserInfo();
		$data['user_info'] = $result;
		$data['left'] = $this->dashboard_login->user_profile();
		$user_id = $this->uri->segment(3);
		$this->load->model('user_feature_model');
		$result = $this->user_feature_model->delete_user($user_id);
		redirect('user_form/all_user');
	}

	public function profile_mapping() {
		$user_page = $this->uri->segment(2);
		$id = $this->session->userdata("user_id");
		if ($id == "1") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$this->load->model('user_feature_model');
			$result_categories = $this->user_feature_model->get_categories();
			$data['categories'] = $result_categories;
			$this->load->view('user_pages/module_feature.php', $data);
		} else {
			redirect('digivalet_dashboard/login');
		}
	}

	public function insert_user_profile() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('profile_name', 'Profile Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('profile_display_name', 'Profile DIsplay Name', 'trim|required|number|xss_clean');
		if ($this->form_validation->run() != False) {
			$this->load->model('user_feature_model');
			$result = $this->user_feature_model->insert_user_profile($_POST);
			redirect('user_form/all_user');
		} else {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$this->load->model('user_feature_model');
			$result_categories = $this->user_feature_model->get_categories();
			$data['categories'] = $result_categories;
			$this->load->view('user_pages/module_feature.php', $data);
		}
	}
	public function check_username(){
		$username=$_POST['username'];
		$this->load->model('user_feature_model');
		echo $result = $this->user_feature_model->get_username_avaliable($username);
		 
	}

}
?>

