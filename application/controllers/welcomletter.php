<?php
/**
 * Description of welcomletter
 *
 * @author pbsl
 */
class welcomletter extends CI_Controller {
	var $data;

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = 'Welcome Letter';
		$this->load->model("welcomletter_model");
		 
		$param = array("welcome letter management");
		$this->load->library('pagination');
		$this->user->set_access_permission($param);
	}


	public function index($cat=0, $offset=0, $limit=30) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$config['base_url'] = base_url()."welcomletter/wlist/".$cat;
			$config['total_rows'] = $this->welcomletter_model->count_welcom_letter($cat);
			$config['per_page'] = $limit;
			$config['num_links'] = 1;
			$config['uri_segment'] = 4;
			//$this->load->model("dashboard_login");
			//$this->data['left']=$this->dashboard_login->user_profile();
			$this->pagination->initialize($config);
			$this->data['links'] = $this->pagination->create_links();

			$q = $this->welcomletter_model->get_letters($cat,$offset);
			$this->data['rows'] = $q->result();
			$this->data['cat_name'] = '';
			$q1 = $this->common_model->get_records("welcome_letter_types", "name");
			$categories = array();
			foreach ($q1->result() as $row) {
				$cnt=0;
				if($row->id == $cat) {
					$this->data['cat_name'] = $row->name;
				}

				$cnt = $this->welcomletter_model->count_welcom_letter($row->id);
				$categories[] = array('row'=>$row, 'count'=>$cnt);
			}
			$this->data['catid'] = $cat;
			 
			$this->data['page_heading'] = $this->data['title'];
			$this->data['category'] = $categories;

			 
			//$this->data['left']=$this->dashboard_login->user_profile();
			$vars = $this->theme->theme_vars('pages/welcome_letter/welcomeletter', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function wlist($cat=0, $offset=0, $limit=30) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->index($cat, $offset, $limit);
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function delete_scheduled($room_no){
		$cnt = $this->welcomletter_model->delete_scheduled($room_no);
	}

	public function add_letter($id=0, $cbox=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($cbox==1) {
				$this->data['colorbox_close'] = $cbox;
			}

			$q1 = $this->common_model->get_records('welcome_letter_types', 'name');
			$types = array(''=>'--Select--');
			foreach($q1->result() as $row) {
				$types[$row->id] = $row->name;
			}

			$title = "";
			$type_id = "";
			$content = "";
			$use_logo = false;
			$logo_align = "";
			$type = "";
			$annexure = "";
			$name = "";
			if($id > 0) {
				$q = $this->common_model->get_record_by_condition('welcome_letters', 'lid='.$id);
				if($q->num_rows() > 0) {
					$row = $q->result();
					$title = $row[0]->title;
					$type_id = $row[0]->type_id;
					$content = $row[0]->content;
					$use_logo = $row[0]->use_logo;
					$logo_align = $row[0]->logo_align;
					$type = $row[0]->type;
					$annexure = $row[0]->annexure;
					$name = $row[0]->let_name;
					$this->data['heading'] = 'Edit Letter <em>'.$title.'</em>';
				}
			}
			else {
				$this->data['heading'] = 'Create Welcome Letter';
				$use_logo = true;
			}

			$logo_align_opt = array(''=>'--Select--', '1'=>'Left', '2'=>'Center', '3'=>'Right');
			$type_opt = array(''=>'--Select--', 'First timer'=>'First timer', 'Welcome Back'=>'Welcome Back');
			$annexure_opt = array(''=>'--Select--', 'Entitlements'=>'Entitlements', 'With Breakfast'=>'With Breakfast', 'Without Breakfast'=>'Without Breakfast');
			$this->data['code'] =$id;
			$this->data['form_open'] = form_open('welcomletter/submit');
			$this->data['form_close'] = form_close();
			$this->data['form_id'] = form_hidden('form_id', 'add_edit_letter');
			$this->data['id'] = form_hidden('id', $id);
			$this->data['title'] = form_input('title', $title, 'id="edit-title" class="input" size="20"');
			$this->data['name'] = form_input('name', $name, 'id="edit-name" class="input" size="20"');
			$this->data['types'] = form_dropdown('types', $types, $type_id, 'id="edit-type" style="width:120px;"');
			$this->data['content'] = form_textarea(array('name'=>'content', 'id'=>'edit-content', 'rows'=>'25', 'cols'=>'74','style'=>'width:80%;'), $content);
			$this->data['use_logo'] = form_checkbox('use_logo', 1, $use_logo, 'id="edit-use_logo"');
			$this->data['logo_align'] = form_dropdown('logo_align', $logo_align_opt, $logo_align, 'id="edit-logo_align"');
			$this->data['type'] = form_dropdown('type', $type_opt, $type, 'id="edit-ctype" style="width:120px;"');
			$this->data['annexure'] = form_dropdown('annexure', $annexure_opt, $annexure, 'id="edit-annexure" style="width:120px;"');
			//$this->data['title'] = 'Add New Letter';
			if($id=='0'){
				$this->data['page_heading'] = 'Add New Letter';
			}
			else{
				$this->data['page_heading'] = 'Edit Letter';
			}
			$vars = $this->theme->theme_vars('pages/welcome_letter/add_letter', $this->data,array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function letter_code_check($code,$id=''){
		 
		$code=str_replace("%20", " ", $code);
		if($id==''){
			$sql="select * from welcome_letters where title='".$code."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				$ch=$row->title;
			}

			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}
		}
		else {
			 
			$sql="select * from welcome_letters where title='".$code."' and  lid<>'".$id."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				$ch=$row->title;
			}
			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}
		}
	}

	public function my_fun_scheduled($data){

		$q = $this->welcomletter_model->get_scheduled_let($data);



		if($q->num_rows() > 0)  {
			$output='';
			 
			$rows=$q->result();
			$i=1;
			foreach($rows as $row) {
				if($i%2==0) {$class="even";}else {$class="odd";}

				$output.='<tr class="'.$class.'">
                    <td align="center" align="center" width="10%">'.$i.'</td>
                    <td align="center" width="10%">'.$row->room_no.'</td> 
                    <td align="center" width="20%">'.$row->letter_code.'</td> 
                    <td width="20%">'.$row->guest_name.'</td>
                    <td align="center" width="20%">'.$row->created.'</td>    
                    <td align="center" width="20%"><a href="'.base_url().'welcomletter/send_letter/'.$row->room_no.'/'.$row->created.'" class="welcome_letter">Edit</a> | <a href="'.base_url().'welcomletter/delete_letters/'.$row->room_no.'/'.$row->created.'" class="delete">Delete</a></td>
                </tr>';               
				$i++;
			}
			 

			 
		}
		else {
			echo '<tr>
                         <td colspan="5" style="text-align:center"><b align="center" style="margin-top:0px;fontsize:12px;">No Letter Found </b></td>
                         
                     </tr>';
		}
		$script='<script type="text/javascript">$(".delete").click(function(){
                              var del_location = $(this).attr("href");
                              jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                                  if(r == true) {
                                      //alert(del_location);
                                      window.location.href=del_location;
                                  }
                              });

                   </script>';
		echo $output.$script;

	}


	public function my_fun($data){

		$q = $this->welcomletter_model->get_let($data);



		if($q->num_rows() > 0)  {
			$output='';
			 
			$rows=$q->result();
			$i=1;
			foreach($rows as $row) {
				if($i%2==0) {$class="even";}else {$class="odd";}

				$output.='<tr class="'.$class.'">
                    <td align="center" width="10%">'.$i.'</td>
                    <td width="20%">'.$row->title.'</td> 
                    <td width="20%">'.$row->let_name.'</td> 
                    <td align="center" width="25%">'.$row->modified.'</td>
                    <td align="center" width="25%"> <a href="'.base_url().'welcomletter/preview_letters/'.$row->lid.'" class="preview_letter">Preview</a> | <a href="#" onclick="view_message('.$row->lid.')">View</a> | <a href="'.base_url().'welcomletter/add_letter/'.$row->lid.'" class="welcome_letter">Edit</a> | <a href="'.base_url().'welcomletter/delete_letter/'.$row->lid.'" class="delete">Delete</a></td>
                </tr>';               
				$i++;
			}
			 

			 
		}
		else {
			echo '<tr>
                         <td colspan="5" style="text-align:center"><b align="center" style="margin-top:0px;fontsize:12px;">No Letter Found </b></td>
                         
                     </tr>';
		}
		$script='<script type="text/javascript">$(".delete").click(function(){
                              var del_location = $(this).attr("href");
                              jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                                  if(r == true) {
                                      //alert(del_location);
                                      window.location.href=del_location;
                                  }
                              });

        //                    if(confirm("Are you sure delete this record.")) {
        //                        return true;
        //                    }
        //                    else {
        //                        return false;
        //                    }
                                return false;
                        })
                    $(".preview_letter").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top);
            $(".preview_letter").colorbox({width:"902", height:"740", iframe:true, overlayClose:false});
            //$(".welcome_letter").click();

        })   
          
           $(".welcome_letter").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top);
            $(".welcome_letter").colorbox({width:"750", height:"650", iframe:true, overlayClose:false});
            //$(".welcome_letter").click();

        })
                   </script>';
		echo $output.$script;

	}

	public function set_default_message($id=0, $cat_id) {
		 
		if($this->welcomletter_model->set_default($id)) {
			$this->message->set("Your configuration saved.", "success", TRUE);
		}
		else {
			$this->message->set("Error in saving configuration.", "error", TRUE);
		}

		redirect('welcomletter/wlist/'.$cat_id);
		 
	}

	public function add_category($id=0, $cbox=0) {
		 
		if($cbox==1) {
			$this->data['colorbox_close'] = $cbox;
		}
		$name = "";
		if($id > 0) {
			$q = $this->common_model->get_record_by_condition('welcome_letter_types', 'id='.$id);
			if($q->num_rows() > 0) {
				$row = $q->result();
				$name = $row[0]->name;
				$this->data['heading'] = 'Edit Category <em>'.$name.'</em>';
			}
		}
		else {
			$this->data['heading'] = 'Create Category';
		}

		$this->data['form_open'] = form_open('welcomletter/submit');
		$this->data['form_close'] = form_close();
		$this->data['form_id'] = form_hidden('form_id', 'add_edit_cat');
		$this->data['id'] = form_hidden('id', $id);
		$this->data['name'] = form_input('name', $name, 'id="edit-name"');
		$vars = $this->theme->theme_vars('pages/welcome_letter/add_category', $this->data, array('right'=>'inc/right'));
		$this->load->view(MAIN_PAGE_DIR.'simple_page', $vars);

		 
	}

	public function send_letter($room_no=0, $date_str='') {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q1 = $this->common_model->get_records('welcome_letter_types', 'name');
			$types = array(''=>'--Category--');
			foreach($q1->result() as $row) {
				$types[$row->id] = $row->name;
			}

			$q1 = $this->common_model->get_records('rooms', 'room_no');
			$rooms = array(''=>'--Select--');
			foreach($q1->result() as $row) {
				$rooms[$row->room_no] = $row->room_no;
			}

			if($room_no != "" && $date_str != "") {

				$q = $this->common_model->get_record_by_condition('rooms_message', 'room_no='.$room_no.' AND created= "'.$date_str.'" ');
				$rs = $q->result();
				$row = $rs[0];
				if($row->guest_name=="Pick From Opera"){$row->guest_name='';}
				if($row->guest_name != "") {
					$arr = explode(".", $row->guest_name);
					$salutation = $arr[0].'.';
					$name = str_replace($salutation, "", $row->guest_name);

				}
				else {
					$salutation = "";
					$name = "";
				}

				$date_arr = explode("-", $row->created);
				$op=$row->pick_from_pmsi;
				if($op=='1'){
					$pick1 = true;$pick2 = false;
				}
				else{
					$pick1 = false;$pick2 = true;
				}

				$room = $row->room_no;

				$message = $row->message;
				$date = $date_arr[1]."/".$date_arr[2]."/".$date_arr[0];
				$letter_code = $row->letter_code;
				$letter_name = $this->get_letter_name($letter_code, 1);
				$id = $row->id;
				$this->data['cancel_location'] = 'welcomletter/rooms_letter';
			}
			else {
				$this->data['cancel_location'] = 'welcomletter';
				$salutation = "";
				$name = "";
				$id = "";
				$room = "";
				$message = "";
				$date = "";
				$letter_code = "";
				$letter_name = "";
				$pick1=true;
				$pick2=false;
				 
			}
			//echo $letter;die();
			$this->data['page_heading'] = 'Assign Template';
			$this->data['form_open'] = form_open('welcomletter/submit');
			$this->data['form_close'] = form_close();
			$type_opt = array(''=>'--Type--', 'First timer'=>'First timer', 'Welcome Back'=>'Welcome Back');
			$annexure_opt = array(''=>'--Annexure--', 'Entitlements'=>'Entitlements', 'With Breakfast'=>'With Breakfast', 'Without Breakfast'=>'Without Breakfast');

			$this->data['date'] = form_input("date", $date, "placeholder='Select date'  readonly='yes' size='20' id='edit-date' class='input' ");

			// $this->data['is_default'] = form


			//$letter_code_options=array(''=>'-Select Letter Code',''=>);
			$salutations = array('Mr.'=>'Mr.', 'Mrs.'=>'Mrs.', 'Miss'=>'Miss', 'Ms.'=>'Ms.', 'Dr.'=>'Dr.', 'Prof.'=>'Prof.', 'Rev.'=>'Rev.');
			$this->data['option']=form_radio('op1', '1',$pick1,'id="radio1" style="vertical-align:middle"'). "";
			$this->data['option1']=form_radio('op1', '0',$pick2,'id="radio2" style="vertical-align:middle"'). "";
			$this->data['gname'] = form_input('gname', $name, 'id="g_name" placeholder="Guest Name" class="input" size="62"');
			$this->data['salutation'] = form_dropdown('salutation', $salutations, $salutation, 'id="edit-salutation" class="select" ');
			$this->data['titles'] = form_dropdown('titles', array('--Select Letter--'), '', 'id="edit-titles" style="width:535px;" onchange="get_letter_content(this.value)"');
			$this->data['room_no'] = form_input('room_no', $room, 'id="room_no" value="" onkeypress="return isNumberKey(event)" placeholder="Room No." maxlength="4" class="input" size="8" autocomplete="off"');
			$this->data['form_id'] = form_hidden('form_id', 'send_letter');
			$this->data['letter_code'] = form_input('letter_code', $letter_code, 'id="letter_code"  autocomplete="off" class="input" placeholder="Code of letter"');
			$this->data['letter_name'] = form_input('letter_name', $letter_name, 'id="letter_name" size="20" maxlength="2" autocomplete="off" class="input" placeholder="Name of letter" readonly="yes"');
			$this->data['content'] = form_textarea(array('name'=>'content', 'id'=>'edit-content', 'rows'=>'25', 'cols'=>'75'), $message);
			$this->data['type'] = form_dropdown('type', $type_opt, '', 'id="edit-ctype" style="width:120px;" onchange="get_letters()"');
			$this->data['annexure'] = form_dropdown('annexure', $annexure_opt, '', 'id="edit-annexure" style="width:120px;" onchange="get_letters()"');
			$this->data['id'] = form_hidden('id', $id);


			$vars = $this->theme->theme_vars('pages/welcome_letter/send_letter', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function preview_letters($code="") {
		 
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($code != "") {
				$q = $this->common_model->get_record_by_condition('welcome_letters', 'lid="'.$code.'"');
				if($q->num_rows() > 0) {
					$row = $q->result();
					 
					$this->data['content']=  $row[0]->content;

				}
				 
			}
			 
			 
			$this->load->view('pages/welcome_letter/preview',$this->data);
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}





	public function get_letters() {
		$letters = array();
		$cat = $this->input->post('cat');
		$type = $this->input->post('type');
		$annaxture = $this->input->post('annaxture');

		$condition = array();
		if($cat != "") {
			$condition[] = 'type_id='.$cat;
		}

		if($type != "") {
			$condition[] = 'type_id="'.$type.'"';
		}

		if($annaxture != "") {
			$condition[] = 'annexure="'.$annaxture.'"';
		}

		$condition_str = implode(' and ', $condition);

		if($condition_str != "") {
			$opts = array('--Select Letter--');
			$q = $this->common_model->get_record_by_condition('welcome_letters', $condition_str);
			foreach($q->result() as $row) {
				$opts[$row->lid] = $row->title;
			}
		}
		else {
			$opts = array('--Select Letter--');
		}

		echo form_dropdown('titles', $opts, '', 'id="edit-titles" style="width:535px;" onchange="get_letter_content(this.value)"');
	}


	public function get_letter_content($code="") {
		if($code != "") {
			//          echo "Got from URL : ".$code;
			//          echo "<br>after processing : ". urldecode($code);
			//          die();

			$code= urldecode($code);
			// echo 'ya'. $code = str_replace(array("\r\n", "\r", "\n"), null, $code);
			$q = $this->common_model->get_record_by_condition('welcome_letters', 'title="'.$code.'"');
			if($q->num_rows() > 0) {
				$row = $q->result();
				echo form_textarea(array('name'=>'content', 'id'=>'edit-content', 'rows'=>'25', 'cols'=>'75'), $row[0]->content);
			}
		}
		else {
			echo form_textarea(array('name'=>'content', 'id'=>'edit-content', 'rows'=>'25', 'cols'=>'75'));
		}
	}

	public function get_letter_html($lid=0) {
		if($lid > 0) {
			$q = $this->common_model->get_record_by_condition('welcome_letters', 'lid='.$lid);
			if($q->num_rows() > 0) {
				$row = $q->result();
				echo $row[0]->content;
			}
		}
	}

	public function delete_category($cid=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($this->welcomletter_model->delete_category($cid)) {
				$this->welcomletter_model->delete_letter(0, $cid);
				$this->message->set('Category successfully deleted.', 'success', TRUE);
			}
			else {
				$this->message->set('Error in category delition.', 'error', TRUE);
			}

			redirect('welcomletter');
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function delete_letter($lid=0) {

		$let=$this->welcomletter_model->get($lid);
		$nam=$let->row();
		$name=$nam->let_name;

		if($this->welcomletter_model->delete_letter($lid, 0)) {

			$this->watchdog->save('delete', 'Letters', 'letter_delete', $lid, $name);
			$this->message->set('Letter successfully deleted.', 'success', TRUE);
		}
		else {
			$this->message->set('Error in Letter delition.', 'error', TRUE);
		}

		redirect('welcomletter');

	}

	public function search($cid="") {
		 
		$result = "";
		$q = $_GET['q'];
		//$cid = $_GET['cid'];
		if($q != "") {
			if($cid > 0) {
				$rs = $this->common_model->get_record_by_condition('welcome_letters', ' type_id='.$cid.' AND title LIKE "'.$q.'%"');
			}
			else {
				$rs = $this->common_model->get_record_by_condition('welcome_letters', ' title LIKE "'.$q.'%"');
			}

			if($rs->num_rows() > 0) {
				foreach($rs->result() as $row) {
					$val = $row->title;
					$result .= "<div><a href='#'>$val</a></div>|$row->lid\n";
				}
			}
		}
		echo $result;
		 
	}


	public function get_letter_code() {
		$q = $_GET['q'];
		// echo $q = str_replace(array("\r\n", "\r", "\n"), null, $q);
		if($q != "") {
			$rs = $this->common_model->get_record_by_condition("welcome_letters", "title LIKE '".$q."%'");
		}
		else {
			$rs = $this->common_model->get_records("welcome_letters");
		}

		$results = "";
		foreach($rs->result() as $row) {
			$val = $row->title;
			$results .= "<div><a href='#'><label>$val</label></a></div>|$val\n";
		}

		echo $results;

	}

	public function get_room_no() {
		$q = $_GET['q'];
		if($q != "") {
			$rs = $this->common_model->get_record_by_condition("iremote.rooms", "room_name  LIKE '".$q."%'");
		}
		else {
			$rs = $this->common_model->get_records("rooms");
		}

		$results = "";
		foreach($rs->result() as $row) {
			$val = $row->room_name;
			$results .= "<div><a href='#'><label>$val</label></a></div>|$val\n";
		}

		echo $results;

	}

	public function get_letter_name($code="", $ret=0) {
		if($code == "") {
			$code = $this->input->post("code");
		}

		if($code != "") {
			$q = $this->common_model->get_record_by_condition('welcome_letters', 'title="'.$code.'"');
			if($q->num_rows() > 0 ) {
				$row = $q->result();
				if($ret == 0) {
					echo $row[0]->let_name;
				}
				else {
					return $row[0]->let_name;
				}
			}
		}
	}


	public function rooms_letter($order_by=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			//$q = $this->common_model->get_records('rooms_message', 'id DESC');
			$date=date('Y-m-d');
			$q = $this->common_model->get_record_by_condition('rooms_message', 'created >= "'.$date.'" ');
			$this->data['rooms_message'] = $q->result();
			$this->data['page_heading'] = 'Scheduled Letters';
			$this->data['title'] = 'Rooms Letter';
			$vars = $this->theme->theme_vars('pages/welcome_letter/alloted_letters', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function change_opera($val){
		$this->common_model->change_opera($val);

	}
	 
	 
	public function update_status($id){
		$this->common_model->reset($id);
	}
	 
	public function default_letter($order_by=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->common_model->get_records('welcome_letters', '');
			$val=$this->common_model->get_val();
			 
			$val=$val->row();
			$this->data['val']=$val->from_opera;
			$this->data['rooms_message'] = $q->result();
			$this->data['page_heading'] = 'Default Letters';
			$this->data['title'] = 'Rooms Letter';
			$vars = $this->theme->theme_vars('pages/welcome_letter/default_letters', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}
	 

	public function delete_letters($room_no=0, $date='') {

		if($date == "") {
			$date = date('Y-m-d');
		}

		$this->welcomletter_model->delete_rooms_letter($room_no, $date);
		$this->watchdog->save('delete', 'Letters', 'letter_delete', $date, $room_no);
		$this->message->set("Letter successfully deleted.", "success", TRUE);
		redirect('welcomletter/rooms_letter');

	}


	public function submit() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$form_id = $this->input->post("form_id");
			switch ($form_id) {
				case "add_edit_cat":
					$name = $this->input->post("name");
					$id = $this->input->post("id");
					$param = array('name'=>$name);
					if($this->welcomletter_model->save_category($param, $id)) {
						$this->message->set("Category successfully saved.", "success", TRUE);
					}
					else {
						$this->message->set("Error in category save.", "error", TRUE);
					}

					$this->add_category(0, 1);

					break;

				case "send_letter":
					$letter_code = $this->input->post("letter_code");
					$opt = $this->input->post("op1");
					$room_no = $this->input->post("room_no");
					$gname = $this->input->post("gname");
					if($gname=='Pick From Opera'){
						$gname='';
					}
					$salutation = $this->input->post("salutation");
					$content = $this->input->post("content");
					$date = $this->input->post("date");
					$id = $this->input->post("id");
					$date_arr = explode("/", $date);
					$date_str = $date_arr[2].'-'.$date_arr[0].'-'.$date_arr[1];

					if($id > 0) {
						$this->welcomletter_model->delete_room_letter_by_id($id);
					}

					if($gname != "") {
						$gname = $salutation." ".$gname;
					}
					else{
						$gname='Pick From Opera';
					}

					$param = array('room_no'=>$room_no, 'letter_code '=>$letter_code, 'guest_name'=>$gname, 'message'=>$content, 'created'=>$date_str,'pick_from_pmsi'=>$opt);

					if($this->welcomletter_model->save_rooms_message($room_no, $date_str, $param)){

						$this->watchdog->save('sent', 'Letters', 'letter_sent', $letter_code, $room_no);
						$this->message->set("Letter scheduled successfully.", "success", TRUE);
					}
					else {
						$this->message->set("Error in Letter sending.", "error", TRUE);
					}
					//echo "<pre>"; print_r($param); die();

					redirect('welcomletter/rooms_letter');

					break;

				case "add_edit_letter":
					$id = $this->input->post("id");
					$title = $this->input->post("title");
					$content = $this->input->post("content");
					if($id=='320'){
						$content = strip_tags( $content, '<p>,<b>,<table>,<tr>,<td>,<font color>,<ul>,<i>,<li>,<br>' );
						$content = preg_replace("/margin-bottom: 0cm/", "", $content);
						$content = preg_replace("/JUSTIFY/", "", $content);
						$content = preg_replace("/–/", "-", $content);
						$content = preg_replace("/'/", "`", $content);
					}
					$type_id = $this->input->post("types");

					$use_logo = $this->input->post("use_logo");
					$logo_align = $this->input->post("logo_align");
					$type = $this->input->post("type");
					$name = $this->input->post("name");
					$annexure = $this->input->post("annexure");

					$param = array('type_id'=>$type_id, 'title'=>$title, 'let_name'=>$name, 'content'=>$content, 'use_logo'=>$use_logo, 'logo_align'=>$logo_align, 'type'=>$type, 'annexure'=>$annexure, 'modified'=>date('Y-m-d H:i:s'));
					if($this->welcomletter_model->save_letter($param, $id)) {
						if($id>0){
							$this->watchdog->save('edit', 'Letters', 'letter_edit', $id, $name);
							$this->message->set("Letter updated successfully.", "success", TRUE);
						}
						else{
							$this->watchdog->save('add', 'Letters', 'letter_add', 0, $name);
							$this->message->set("Letter saved successfully.", "success", TRUE);
						}


					}
					else {
						$this->message->set("Error in Letter save.", "error", TRUE);
					}
					redirect('welcomletter');
					//$this->add_letter(0, 1);
					break;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}
}
?>
