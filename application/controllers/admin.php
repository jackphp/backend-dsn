<?php
/**
 * Description of adminOperation
 * Admin Operation define all administrator
 * operations
 * @author pbsl
 */
class admin extends CI_Controller {

	var $data = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = 'Admin operations';
		$this->load->model('admin_operation_model');
		$this->load->model('watchdog_model');
		$perm = array('user management');
		$this->user->set_access_permission($perm);

		$menu = array('menu'=>'User Management',
                     'links'=>array(array('label'=>'Users', 'path'=>'admin/users', 'permission'=>'user management', 'menu_order'=>0),
		array('label'=>'Roles', 'path'=>'admin/roles', 'permission'=>'user management', 'menu_order'=>1),
		array('label'=>'Departments', 'path'=>'admin/departments', 'permission'=>'user management', 'menu_order'=>2),
		array('label'=>'Permissions', 'path'=>'admin/permissions', 'permission'=>'user management', 'menu_order'=>3)));
		$this->menu->set_menu($menu);

		$this->load->library('forms');
		$this->load->model('role_model');
		$this->load->helper('fusion_charts_helper');
		$this->load->helper('reports_helper');
	}

	public function users($sort= "") {
		if($this->user->is_user_access()) {
			$header = array(
			array('data'=>'#'),
			array('data'=>'Name'),
			array('data'=>'Department'),
			array('data'=>'Status'),
			array('data'=>'Roles'),
			array('data'=>'Created'),
			array('data'=>'Last access'),
			array('data'=>'Action'),
                   'class'=>'listheading');

			$departments_res = $this->common_model->get_records('departments', 'name');
			$departments = array(0=>'-');
			foreach ($departments_res->result() as $row) {
				$departments[$row->did] = $row->name;
			}

			$users = $this->common_model->get_records('users', $sort);
			$rows = array();
			$i=0;
			foreach($users->result() as $user) {
				$i++;
				$roles_res = $this->user_model->user_roles($user->uid);
				$roles = $roles_res->result_array();
				$user_roles = object_to_array($roles);

				//print_r($user_roles); die();
				if(count($roles) > 0) {
					$user_roles = implode("<br />", $user_roles);
				}
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				if($user->uid == 1) {
					$links = '-';
				}
				else {
					$links = anchor('admin/user/edit/'.$user->uid, 'edit')." | ".anchor('admin/delete_user/delete/'.$user->uid, 'delete', 'class="delete"');
				}
				 
				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('admin/user/edit/'.$user->uid, $user->name)),
				array('data'=>$departments[$user->department_id], 'attributes'=>array('align'=>'center')),
				array('data'=>user_status_text($user->status), 'attributes'=>array('align'=>'center')),
				array('data'=>$user_roles, 'attributes'=>array('align'=>'center')),
				array('data'=>format_date($user->created, 'medium'), 'attributes'=>array('align'=>'center')),
				array('data'=>format_date($user->access, 'medium'), 'attributes'=>array('align'=>'center')),
				array('data'=>$links, 'attributes'=>array('align'=>'center')),
                        'attributes'=>array('class'=>$class));
			}

			//echo "<pre>"; print_r($rows); die();
			$this->data['title'] = 'Users List';
			$this->data['page_heading'] = 'Users List';
			$this->data['add_new_user'] = anchor('admin/add_user', 'Add new user');
			$this->data['users_list'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/users_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function add_user() {
		if($this->user->is_user_access()) {
			$this->data = $this->forms->add_user_form();
			$this->data['title'] = 'Add new user';
			$this->data['page_heading'] = 'Add new user';
			$this->data['users_list'] = anchor('admin/users', 'Users list');
			$vars = $this->theme->theme_vars('pages/add_new_user_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}

	}

	public function user($edit, $uid) {
		if($this->user->is_user_access('edit profile') || $this->user->is_user_access()) {
			if($edit == 'edit' && $uid > 0) {
				$param = $this->user->user_load($uid);
				//echo "<pre>"; print_r($param); die();
				$this->data = $this->forms->edit_user_form($param, false);
				$this->data['title'] = "Edit ".ucfirst($param[0]['name'])."'s details";
				$this->data['last_access'] = "Last access: ".format_date($param[0]['access']);
				$this->data['page_heading'] = $this->data['title'];
				$this->data['full_details'] = anchor('activity/user/'.$uid, 'Detail View');

				$accessLog = array();
				$accessq = $this->watchdog_model->getUsersActivityLog($uid);
				foreach($accessq->result() as $row) {
					$accessLog[] = array('module'=>$row->module, 'count'=>$row->ctr);
				}

				$graph_str = user_activity_graph($accessLog);

				$this->data['graph_str'] = $graph_str;

				if($this->user->is_user_access('user management')) {
					$this->data['users_list'] = anchor('admin/users', 'Users list');
				}
				$vars = $this->theme->theme_vars('pages/add_new_user_view', $this->data);
				$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			}
			else {

			}
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function loggdin_users() {
		if($this->user->is_user_access()) {
			$this->data['title'] = "Loggedin Users";
			$this->data['page_heading'] = "Loggedin Users";
			$q = $this->common_model->get_record_by_condition('users', 'login=1 AND uid<>1');
			$this->data['res'] = $q->result();
			$vars = $this->theme->theme_vars('pages/loggedin', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete_user($delete="", $uid=0) {
		if($this->user->is_user_access()) {
			if($delete=="delete" && $uid > 0) {

				if($uid == 1) {
					$this->message->set('This is Master admin it can not be deleted.', 'error', TRUE);
					$this->watchdog->save('error', 'user', 2, $uid);
					redirect('admin/users');
				}

				if($this->user_model->delete_user($uid)){
					$this->message->set('User successfully deleted.', 'success', TRUE);
					$this->watchdog->save('success', 'user', 2, $uid);
					redirect('admin/users');
				}
				else {
					$this->message->set('Some problem in user deletion.', 'error', TRUE);
					$this->watchdog->save('error', 'user', 2, $uid);
					redirect('admin/users');
				}
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function roles() {
		if($this->user->is_user_access()) {
			$header = array(
			array('data'=>'#'),
			array('data'=>'Name'),
			array('data'=>'Action'),
                   'class'=>'listheading');
			$roles = $this->common_model->get_records('role', 'rid');
			$rows = array();
			$i=0;
			foreach($roles->result() as $role) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				$link = '';
				$name_link = '';
				if($role->name == "administrator") {
					$link = " - ";
					$name_link = $role->name;
				}
				else if($role->name == "authenticated user") {
					$link = " - ";
					$name_link = $role->name;
				}
				else {
					$link = anchor('admin/edit_role/edit/'.$role->rid, 'edit')." | ".anchor('admin/delete_role/delete/'.$role->rid, 'delete');
					$name_link = anchor('admin/edit_role/edit/'.$role->rid, $role->name);
				}

				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>$name_link),
				array('data'=>$link, 'attributes'=>array('align'=>'center')),
                            'attributes'=>array('class'=>$class));
			}

			$this->data = $this->forms->role_form();
			$this->data['title'] = 'Role List';
			$this->data['page_heading'] = 'Role List';
			$this->data['add_new_role'] = anchor('admin/add_role', 'Add new role');
			$this->data['roles_list'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/roles_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}


	public function edit_role($edit, $rid) {
		if($this->user->is_user_access()) {
			$q = $this->common_model->get_record_by_condition('role', 'rid='.$rid.' AND name<>"administrator" AND name<>"authenticated user"');
			$res = $q->result_array();
			if(isset($res[0]['rid']) && $res[0]['rid'] > 0) {
				$this->data = $this->data = $this->forms->role_form($res[0]);
			}
			else {
				redirect('admin/roles');
			}

			$this->data['title'] = 'Edit role';
			$this->data['page_heading'] = 'Edit role';
			$this->data['role_list_link'] = anchor('admin/roles', 'Role list');
			$vars = $this->theme->theme_vars('pages/roles_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete_role($delete, $rid) {
		if($this->user->is_user_access()) {
			if($delete=='delete' && $rid > 0) {
				if($this->role_model->delete_role($rid)) {
					$this->message->set('Role successfully deleted.', 'success', TRUE);
					$this->watchdog->save('success', 'role', 2, $rid);
					redirect('admin/roles');
				}
				else {
					$this->message->set('Error in role deletion.', 'error', TRUE);
					$this->watchdog->save('error', 'role', 2, $rid);
					redirect('admin/roles');
				}
			}
			else {
				redirect('admin/roles');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}


	public function departments() {
		if($this->user->is_user_access()) {
			$header = array(
			array('data'=>'#'),
			array('data'=>'Name'),
			array('data'=>'Action'),
                   'class'=>'listheading');
			$departments = $this->common_model->get_records('departments', 'name');
			$rows = array();
			$i=0;
			foreach($departments->result() as $department) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				 
				$link = anchor('admin/edit_department/edit/'.$department->did, 'edit')." | ".anchor('admin/delete_department/delete/'.$department->did, 'delete');


				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('admin/edit_department/edit/'.$department->did, $department->name)),
				array('data'=>$link, 'attributes'=>array('align'=>'center')),
                            'attributes'=>array('class'=>$class));
			}

			$this->data = $this->forms->department_form();
			$this->data['title'] = 'Department List';
			$this->data['page_heading'] = 'Department List';
			//$this->data['add_new_department'] = anchor('admin/add_role', 'Add new department');
			$this->data['department_list'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/department_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function edit_department($edit, $did) {
		if($this->user->is_user_access()) {
			$q = $this->common_model->get_record_by_condition('departments', 'did='.$did.' AND name<>"administrator" AND name<>"authenticated user"');
			$res = $q->result_array();
			if(isset($res[0]['did']) && $res[0]['did'] > 0) {
				$this->data = $this->data = $this->forms->department_form($res[0]);
			}
			else {
				redirect('admin/roles');
			}

			$this->data['title'] = 'Edit department';
			$this->data['page_heading'] = 'Edit department';
			$this->data['role_list_link'] = anchor('admin/departments', 'Departments list');
			$vars = $this->theme->theme_vars('pages/department_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete_department($delete, $did) {
		if($this->user->is_user_access()) {
			if($delete=='delete' && $did > 0) {
				if($this->user_model->delete_department($did)) {
					$this->message->set('Department successfully deleted.', 'success', TRUE);
					$this->watchdog->save('success', 'department', 2, $did);
					redirect('admin/departments');
				}
				else {
					$this->message->set('Error in department deletion.', 'error', TRUE);
					$this->watchdog->save('error', 'department', 2, $did);
					redirect('admin/departments');
				}
			}
			else {
				redirect('admin/departments');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function permissions() {
		if($this->user->is_user_access()) {
			$roles_qry = $this->common_model->get_records('role');
			$roles = array();
			foreach($roles_qry->result() as $row) {
				$roles[$row->rid] = $row->name;
			}

			$perm_qry = $this->common_model->load_all_permissions();
			$temp_perm = array();
			foreach($perm_qry->result() as $row) {
				//$perms[] = $row->pid;
				$pid = $row->pid;
				$rid = $row->rid;
				$perm = $row->perm;
				if($rid > 0) {
					if(isset($temp_perm[$row->pid])) {
						$temp_perm[$row->pid] .= ",".$rid;
					}
					else {
						$temp_perm[$row->pid] = $rid;
					}
				}
				else {
					$temp_perm[$row->pid] = $rid;
				}
			}

			$permission_arr = array();
			foreach($temp_perm as $k=>$p) {
				if(strchr($p, ',')) {
					$arr = explode(',', $p);
					$permission_arr[$k] = $arr;
				}
				else {
					$permission_arr[$k] = array($p);
				}
			}

			//echo "<pre>"; print_r($permission_arr); die();

			$permissionForm = $this->forms->permission_form($roles, $permission_arr);

			$this->data['title'] = 'Users role permission';
			$this->data['page_heading'] = 'Users role permission';
			$this->data['permission_list'] = $this->theme->generate_list($permissionForm['header'], $permissionForm['rows']);
			$attributes = array('method'=>'POST', 'name'=>'form1');
			$this->data['form_open'] = form_open('admin/submit', $attributes);
			$this->data['save_permission'] = form_submit('submit', 'Save permission');
			$this->data['form_id'] = form_hidden('form_id', 'edit-permission');
			$this->data['form_close'] = form_close();
			$vars = $this->theme->theme_vars('pages/permission_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}


	// Submiting all forms data
	public function submit() {
		if($this->user->is_user_access() || $this->user->is_user_access('edit profile')) {
			$form_id = $this->input->post('form_id');

			switch ($form_id) {
				case "edit-add-user":
					$username = $this->input->post('name');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$retype_password = $this->input->post('retype_password');
					$status = $this->input->post('status');
					$roles = $this->input->post('roles');
					$department = $this->input->post('department');
					$param = array('username'=>$username, 'email'=>$email, 'department_id'=>$department, 'password'=>$password, 'retype_password'=>$retype_password, 'status'=>$status, 'roles'=>$roles);
					if(!$this->forms->add_user_validate($param)) {
						$pass = $this->user->encode_password($password);
						$roles[] = 'authenticated user';
						$users_roles = array();
						$all_roles_qry = $this->common_model->get_records('role');
						foreach($all_roles_qry->result() as $row) {
							foreach($roles as $role) {
								if($row->name == $role) {
									$users_roles[$row->name] = $row->rid;
								}
							}
						}

						$newParam = array('name'=>$username, 'mail'=>$email, 'department_id'=>$department, 'pass'=>$pass, 'status'=>$status, 'roles'=>$users_roles);
						$uid = $this->user_model->save_user($newParam);
						if($uid) {
							$this->message->set("User Successfully created", "success", TRUE);
							$this->watchdog->save('success', 'user', 0, $uid);
						}
						else {
							$this->message->set("Problem with user creation.", "error", TRUE);
							$this->watchdog->save('error', 'user', 0);
						}

						$this->get_last_post->set("");
						redirect('admin/users');
					}
					else {
						redirect('admin/add_user');
					}
					break;

				case "update-user":
					$uid = $this->input->post('uid');
					$username = $this->input->post('name');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$retype_password = $this->input->post('retype_password');
					$status = $this->input->post('status');
					$roles = $this->input->post('roles');
					$department = $this->input->post('department');
					$param = array('uid'=>$uid, 'username'=>$username, 'email'=>$email, 'department_id'=>$department, 'password'=>$password, 'retype_password'=>$retype_password, 'status'=>$status, 'roles'=>$roles);

					if(!$this->forms->add_user_validate($param)) {
						$pass = $this->user->encode_password($password);
						$roles[] = 'authenticated user';
						$users_roles = array();
						$all_roles_qry = $this->common_model->get_records('role');
						foreach($all_roles_qry->result() as $row) {
							foreach($roles as $role) {
								if($row->name == $role) {
									$users_roles[$row->name] = $row->rid;
								}
							}
						}

						if(strlen($password) > 0) {
							$newParam = array('uid'=>$uid, 'name'=>$username, 'mail'=>$email, 'department_id'=>$department, 'pass'=>$pass, 'status'=>$status, 'roles'=>$users_roles);
						}
						else {
							$newParam = array('uid'=>$uid, 'name'=>$username, 'mail'=>$email, 'department_id'=>$department, 'status'=>$status, 'roles'=>$users_roles);
						}
						if($this->user_model->save_user($newParam)) {
							$this->message->set("User Successfully updated", "success", TRUE);
							$this->watchdog->save('success', 'user', 1, $uid);
						}
						else {
							$this->message->set("Problem with user updation.", "error", TRUE);
							$this->watchdog->save('error', 'user', 1, $uid);
						}


						$this->get_last_post->set("");
						redirect('admin/user/edit/'.$uid);
					}
					else {
						redirect('admin/user/edit/'.$uid);
					}

					break;

				case "add-role":
					$name = $this->input->post('name');
					$param = array('name'=>$name);
					if(!$this->forms->role_validate($param)) {
						$rid = $this->role_model->save_role($param);
						if($rid) {
							$this->message->set('Role successfully created.', 'success', TRUE);
							$this->watchdog->save('success', 'role', 0, $rid);
							redirect('admin/roles');
						}
						else {
							$this->message->set('Error in role creation.', 'error', TRUE);
							$this->watchdog->save('error', 'role', 0);
							redirect('admin/roles');
						}
					}
					else {
						redirect('admin/roles');
					}
					break;

				case "edit-role":
					$name = $this->input->post('name');
					$rid = $this->input->post('rid');
					$param = array('name'=>$name, 'rid'=>$rid);
					if(!$this->forms->role_validate($param)) {
						if($this->role_model->save_role($param)) {
							$this->message->set('Role successfully updated.', 'success', TRUE);
							$this->watchdog->save('success', 'role', 1, $rid);
							redirect('admin/roles');
						}
						else {
							$this->message->set('Error in role updation.', 'error', TRUE);
							$this->watchdog->save('error', 'role', 1, $rid);
							redirect('admin/edit_role/edit/'.$rid);
						}
					}
					else {
						redirect('admin/edit_role/edit/'.$rid);
					}
					break;

				case "add-department":
					$name = $this->input->post('name');
					$param = array('name'=>$name);
					if(!$this->forms->department_validate($param)) {
						$did = $this->user_model->save_department($param);
						if($did) {
							$this->message->set('Department successfully created.', 'success', TRUE);
							$this->watchdog->save('success', 'department', 0, $did);
							redirect('admin/departments');
						}
						else {
							$this->message->set('Error in Department creation.', 'error', TRUE);
							$this->watchdog->save('error', 'department', 0);
							redirect('admin/departments');
						}
					}
					else {
						redirect('admin/departments');
					}
					break;


				case "edit-department":
					$name = $this->input->post('name');
					$did = $this->input->post('did');
					$param = array('name'=>$name, 'did'=>$did);
					if(!$this->forms->department_validate($param)) {
						if($this->user_model->save_department($param)) {
							$this->message->set('Department successfully updated.', 'success', TRUE);
							$this->watchdog->save('success', 'department', 1, $did);
							redirect('admin/departments');
						}
						else {
							$this->message->set('Error in department updation.', 'error', TRUE);
							$this->watchdog->save('error', 'department', 1);
							redirect('admin/edit_department/edit/'.$did);
						}
					}
					else {
						redirect('admin/edit_department/edit/'.$did);
					}
					break;


				case "edit-permission":
					$post = $_POST;
					//echo "<pre>"; print_r($post); die();
					foreach($post as $rid=>$p) {
						if($rid != "submit" && $rid != "form_id") {
							$this->user_model->delete_role_permission($rid);
							foreach($p as $pid) {
								$this->user_model->save_permissions(array('rid'=>$rid, 'pid'=>$pid));
							}
						}
					}

					$this->message->set('Your configuration has been saved.', 'success', TRUE);
					$this->watchdog->save('success', 'permission', 3);
					redirect('admin/permissions');
					break;
			}
		}
		else {
			$this->user->user_access_denied();
		}

	} // End of Submit function

	 
} //End of class
?>