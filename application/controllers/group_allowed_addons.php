<?php
/**
 * Description of food_addons
 *
 * @author pbsl
 */
class group_allowed_addons extends CI_Controller {
	var $data = array();
	var $groups = array();
	public function  __construct() {
		parent::__construct();
		$this->data['title'] = "Food Revenue Centers";
		$perm = array('menuitem management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Add Group Addons', 'path'=>'group_addons', 'permission'=>'menuitem management', 'menu_order'=>10)
		));
		$this->menu->set_menu($menu);

		$this->load->model("food_application_model");
		$this->load->model("group_allowed_addon_model");
		$this->load->library('pagination');
		$this->load->library('food_library');

		$q = $this->common_model->get_records("food_allowed_addon_group_master");
		foreach($q->result() as $row) {
			$this->groups[$row->gid] = $row->display_name;
		}

		$this->data['menu_section'] = 'food';
	}

	public function index($offset=0, $limit=100) {
		 
		$config['base_url'] = base_url()."group_allowed_addons/addon_allowed_list/";
		$config['total_rows'] = $this->db->count_all("food_allowed_addon_group_master");
		$config['per_page'] = $limit;
		$config['num_links'] = 1;
		$config['first_link'] = "First";
		$config['last_link'] = "Last";
		$config['uri_segment'] = 3;
		//            $this->load->model("dashboard_login");
		// 	    $this->data['left']=$this->dashboard_login->user_profile();
		$this->pagination->initialize($config);
		$this->data['links'] = $this->pagination->create_links();

		$this->data['page_heading'] = 'Group Allowed Addons';

		$this->data['addons'] = $this->group_addon_model->get_addons($offset, $limit);
		$vars = $this->theme->theme_vars('pages/group_allowed_addons/addon_allowed_list', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		 
	}


	public function glist($id) {

		//            $config['base_url'] = base_url()."group_addons/glist/";
		//            $config['total_rows'] = $this->db->count_all("food_addons_master");
		//            $config['per_page'] = $limit;
		//            $config['num_links'] = 1;
		//            $config['first_link'] = "First";
		//            $config['last_link'] = "Last";
		//            $config['uri_segment'] = 3;
		//
		//            $this->pagination->initialize($config);
		//            $this->data['links'] = $this->pagination->create_links();
		//
		$this->data['page_heading'] = 'Allowed Addons';

		$this->data['addons'] = $this->group_allowed_addon_model->get_list_addons($id);
		$vars = $this->theme->theme_vars('pages/group_allowed_addons/group_allowed_addon_list', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		 
	}


	public function addon_list($offset=0, $limit=100) {
		$this->index($offset, $limit);
	}

	public function delete_addon($id=0) {
		 
		$name=$this->group_allowed_addon_model->get_addon_name($id);
		$nam=$name->row();
		$name=$nam->display_name;


		if($this->group_allowed_addon_model->delete_addon($id)){
			$this->watchdog->save('deleted', 'IRD', 'alowed_adddon_family_deleted', $id, $name);

			$name=$this->group_allowed_addon_model->get_family_addon_name($id);
			$nam=$name->result();
			//echo count($nam);die();
			if(count($nam)>0){
				$menu_item='Allowed Addons ';
				foreach($nam as $row){
					$menu_item.=$row->display_name;
					$menu_item.=',';
				}
				$menu_item.=' are deleted';
				//                   echo $menu_item;die();
				$this->watchdog->save('delete', 'IRD', 'allowed_addon_deleted',0,$menu_item);

				$this->group_allowed_addon_model->del_addons($id);
			}

			$this->message->set("Allowed Addon family successfully deleted", "success", TRUE);
		}
		else {
			$this->message->set("Error in Group deletion", "error", TRUE);
		}

		redirect("group_allowed_addons");
		 
	}

	public function add($id=0) {
		 
		if($id > 0) {
			$q = $this->common_model->get_record_by_condition('food_allowed_addon_group_master', 'gid='.$id);
			$rs = $q->result();
			$row = $rs[0];
			$gid = $row->gid;
			$addon_code = $row->group_code;
			$addon_name = $row->group_name;
			$display_name = $row->display_name;
			//                $size = $row->size;
			//                $price = $row->price;
		}
		else {
			$gid = '';
			$addon_code = '';
			$addon_name = '';
			$display_name = '';
			$size = '';
			$price = '';
		}

		$this->data['heading'] = 'Add Allowed Group Addons';
		$this->data['form_open'] = form_open('group_allowed_addons/submit',' target="_parent" ');
		$this->data['form_close'] = form_close();
		$this->data['form_id'] = form_hidden('form_id', "edit_new_addon");
		$this->data['id'] = form_hidden('id', $id);

		//$this->data['group'] = form_dropdown('group', $this->groups, $gid);
		$this->data['group_code'] = form_input('group_code', $addon_code, 'id="edit-addon-code" class="input" ');
		$this->data['group_name'] = form_input('group_name', $addon_name, 'id="edit-addon-name" class="input" ');
		$this->data['display_name'] = form_input('display_name', $display_name, 'id="edit-display-name" class="input" ');
		//           $this->data['size'] = form_input('size', $size, 'id="edit-size"');
		//            $this->data['price'] = form_input('price', $price, 'id="edit-price"');
		 
		$vars = $this->theme->theme_vars('pages/group_allowed_addons/add_addon', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'simple_page', $vars);


	}

	public function submit() {

		$form_id = $this->input->post("form_id");
		switch ($form_id) {
			case "edit_new_addon":
				//                    print_r($_POST);die();
				//                    $images = $this->food_library->upload_files($_FILES);
				$id = $this->input->post('id');
				$group = $this->input->post('group');
				$addon_code = $this->input->post('group_code');
				$addon_name = $this->input->post('group_name');
				$display_name = $this->input->post('display_name');
				$size = $this->input->post('size');
				$price = $this->input->post('price');

				$param = array('group_code'=>$addon_code, 'group_name'=>$addon_name, 'display_name'=>$display_name);
				if($id > 0){
					 
					$this->watchdog->save('edit', 'IRD', 'allowed_addon_family_edit',$id,$display_name);
				}
				else {
					$this->watchdog->save('add', 'IRD', 'allowed_addon_family_add',0,$display_name);
				}
				$aid = $this->group_allowed_addon_model->add_group($param, $id);

				if($aid > 0) {
					//                        $this->food_application_model->save_images($images, 'addon', $aid);
					//                        $this->food_application_model->delete_food_locale('addon', $aid);
					//                        $locale_param = array('lang_code'=>'en', 'type'=>'addon', 'type_id'=>$aid, 'name'=>$display_name, 'description'=>$display_name);
					//                        $this->food_addon_model->save_locale($locale_param);
					 

					$this->message->set("Allowed Addon successfully added.", "success", TRUE);
					 
				}
				else {
					$this->message->set("Error in allowed addon add.", "error", TRUE);
				}

				redirect('group_allowed_addons');

				break;
		}

	}
}
?>
