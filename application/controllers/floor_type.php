<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class floor_type extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("floor_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Floor Type';
		$perm = array('floor management');
		$this->user->set_access_permission($perm);



	}

	public function index() {
		if($this->user->is_user_access()) {
			 
			$query=$this->floor_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$this->data['page_heading']='Floor Types';
			$vars = $this->theme->theme_vars('pages/floors/floor_type_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function save() {
		if($this->user->is_user_access()) {
			if($this->input->post('id')) {
				$id=$this->input->post('id');
				$floor_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				$this->watchdog->save('edit', 'Floors', 'floor_type_edit',$id,$floor_type);
				$this->floor_model->update($floor_type,$des,$id);

			}
			else {
				$floor_type=$this->input->post('name');
				//echo $room_type;
				$des=$this->input->post('desc');
				$this->watchdog->save('add', 'Floors', 'floor_type_add',0,$floor_type);
				$this->floor_model->insert($floor_type,$des);
				 
				 
				 
				 

			}
			redirect('floor_type');
			 
			 
		}
		else {
			$this->user->user_access_denied();
		}


	}
	public function edit($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			{

				 
				$res=$this->floor_model->get($id);
				 
				 
				 
			}
			 
			$query=$this->floor_model->get_list();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$this->data['edit'] = $res;
			$this->data['page_heading']='Floor Types';
			$vars = $this->theme->theme_vars('pages/floors/floor_type_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}



	}
	public function delete($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)

			{
				$name=$this->floor_model->get_floor_type_name($id);
				$nam=$name->row();
				$name=$nam->floor_type;
				 
				$this->watchdog->save('delete', 'Floors', 'floor_type_delete',$id,$name);
				$query=$this->floor_model->delete($id);
				redirect('floor_type');

			}

		}
		else {
			$this->user->user_access_denied();
		}
	}
	function delete_all(){
		if($this->user->is_user_access()) {
			$del=$this->input->post('check');
			print_r($del);die();
			$floor_type='Floor Type ';
			foreach($del as $row) {
				$floor_type.=$row;
				$floor_type.=',';

				//$query=$this->db->query("delete from  floor_types where floor_type_id='".$row."'");

			}
			$floor_type.=' are deleted. ';
			$this->watchdog->save('delete', 'Floors', 'floor_type_selected_delete',$id,$floor_type);
			$query=$this->floor_model->delete_all($del);
			redirect('floor_type');
		}
		else {
			$this->user->user_access_denied();
		}


	}


}// End Of class
?>
