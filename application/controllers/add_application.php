<?php
/**
 * Description of add_application
 *
 * @author pbsl
 */
class add_application extends CI_Controller {

	public function  __construct() {
		parent::__construct();

		$this->data['title'] = 'DigiValet application management';
		$perm = array('application management');
		$this->user->set_access_permission($perm);
	}

	public function index() {
		if($this->user->is_user_access()) {
			$header = array(
			array('data'=>'Name'),
			array('data'=>'Application URL'),
			array('data'=>'Reporting Path'),
			array('data'=>'Action')
			);


		}
		else {
			$this->user->user_access_denied();
		}
	}

}
?>
