<?php
/**
 * Description of food_category
 *
 * @author pbsl
 */
class food_category extends CI_Controller {
	var $data;
	var $logo;
	var $description;
	var $video;
	var $revenue = array();
	var $section = array();
	var $category = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = "Category";
		$perm = array('food category');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Categories', 'path'=>'food_category', 'permission'=>'food category', 'menu_order'=>4),
		array('label'=>'Add Category', 'path'=>'food_category/add', 'permission'=>'food category', 'menu_order'=>5),
		array('label'=>'Subcategory', 'path'=>'food_category/subcat', 'permission'=>'food category', 'menu_order'=>6)));
		$this->menu->set_menu($menu);
		$this->load->library('food_library');
		$this->load->library('food_item_library');
		$this->load->model('food_application_model');

		$this->revenue = $this->food_library->get_table_data('food_revenue_primary', 'rvc_number', 'rvc_name');
		$this->section = $this->food_library->get_table_data('food_section', 'section_id', 'name');
		$this->category = $this->food_library->get_table_data('food_maincategory', 'maincategory_id', 'name');
		$this->food_library->initialize_food_vars(array('revenue'=>$this->revenue, 'sections'=>$this->section, 'category'=>$this->category));

		$this->data['menu_section'] = 'food';
	}

	public function index($offset="", $rs="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($offset=="") $offset=0;

			if($rs != "") {
				$q = $rs;
			}
			else {
				$q = $this->food_application_model->get_categories($offset);
				$this->data['categ'] = $q;
				$config['total_rows'] = $this->db->count_all("food_maincategory");
				$config['per_page'] = '25';
				$config['base_url'] = base_url().'food_category/food_cat_list';
				$this->pagination->initialize($config);
				$this->data['pager'] = $this->pagination->create_links();
			}

			$header = array(array('data'=>'#'), array('data'=>'Name'), array('data'=>'Revenue'), array('data'=>'Section'), array('data'=>'Description'),array('data'=>'Status'), array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				//echo "<pre>"; print_r($this->section); die();
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$img='<div class="handle"><img class="tabledrag-handle" href="#" title="Drag to re-order" src="'.get_assets_path('image').'arrow1.png" style="cursor: move;"/></div>';
				$active='<input type="checkbox" name="chk[]"  onclick="check('.$row->maincategory_id.','.$c.');"  '.$c. '  />';
				$rows[] = array(
				//                         array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>$img, 'attributes'=>array('align'=>'center')),
				 
				array('data'=>anchor('food_category/edit/'.$row->maincategory_id, $row->display_name)),
				//  array('data'=>anchor('revenue/edit/'.$row->rvc_number, $this->revenue[$row->rvc_number])),
				array('data'=>anchor('sections/edit/'.$row->section_id, $this->section[$row->section_id])),
				array('data'=>$row->description),
				array('data'=>$active, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('food_category/subcategory/'.$row->maincategory_id, 'subcategories').' | '.anchor('food_category/edit/'.$row->maincategory_id, 'edit').' | '.anchor('food_category/delete/cat/'.$row->maincategory_id, 'delete', array('class'=>'delete')), 'attributes'=>array('align'=>'center')),
                         'attributes'=>array('class'=>$class,'id'=>$row->maincategory_id));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->load->model("dashboard_login");
			$this->data['left']=$this->dashboard_login->user_profile();
			$this->data['list'] = $list;
			$this->data['add_link'] = anchor('food_category/add', 'Add Category');
			$this->data['page_heading'] = $this->data['title'];
			$this->data['search']['form_open'] = form_open('food_category/search_category');
			$this->data['search']['form_close'] = form_close();
			$vars = $this->theme->theme_vars('pages/food_category_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function update_cat_status($id,$val){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->food_application_model->update_category_status($id,$val);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function update_subcat_status($id,$val){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->food_application_model->update_subcategory_status($id,$val);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function search_category() {
		 
		$search_term = $this->input->post("search_term");
		$search_val = $this->input->post("search_val");
		$rs = $this->food_application_model->get_category_search($search_term, $search_val);
		$this->index(0, $rs);
		 
	}

	function sub_sort_save() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$ids = $this->input->post('ids');
			foreach($ids as $key=>$id) {
				echo "Key=> ".$key." Val=> ".$id." <br/>";
				// $this->fav_channels->update_tag($id, $key);
				//$this->group_addon_model->update_position($key,$id );
				// $this->food_application_model->update_cat($key, $id);
				$this->food_application_model->update_subcat($key,$id);
			}

			echo 1;
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	function sort_save() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			 
			$ids = $this->input->post('ids');
			foreach($ids as $key=>$id) {
				echo "Key=> ".$key." Val=> ".$id." <br/>";
				// $this->fav_channels->update_tag($id, $key);
				//$this->group_addon_model->update_position($key,$id );
				$this->food_application_model->update_cat($key, $id);
			}

			echo 1;
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}



	public function sort() {
		 
		$ids = $this->input->post("ids");
		$pos = explode(",", $ids);
		$i = 0;
		 
		foreach($pos as $qid) {

			 
			$this->food_application_model->update_cat($i, $qid);

			$i++;
		}

	}



	public function sub_sort() {
		 
		$ids = $this->input->post("ids");
		$pos = explode(",", $ids);
		$i = 0;
		 
		foreach($pos as $qid) {

			 
			$this->food_application_model->update_subcat($i, $qid);

			$i++;
		}

	}

	public function food_cat_list($offset=0) {

		$this->index($offset);
		 
	}

	public function add() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->data = $this->food_library->category_form();
			$this->data['title'] = 'Add New Category';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_food_category_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function edit($id = 0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($id == 0) { redirect('food_category'); }

			$param = array();
			$q = $this->food_application_model->get_categories(0,0,$id);
			foreach($q->result_array() as $row) {
				$param = $row;
			}
			//echo "<pre>"; print_r($param); die();
			$this->data = $this->food_library->category_form($param, $id);
			$this->data['title'] = 'Edit Category';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_food_category_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function subcategory($id=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->food_application_model->sub_categories($id);
			$this->data['sub_categ'] = $q;
			$header = array(array('data'=>'Order'), array('data'=>'Name'), array('data'=>'Parent Category'), array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "odd";
				}
				else {
					$class = "even";
				}
				$img='<div class="handle"><img class="tabledrag-handle" href="#" title="Drag to re-order" src="'.get_assets_path('image').'arrow1.png" style="cursor: move;"/></div>';
				$rows[] = array(
				array('data'=>$img, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('food_category/subcat_add/'.$row->sub_cat_id, $row->name)),
				array('data'=>anchor('food_category/edit/'.$row->maincategory_id, $this->category[$row->maincategory_id])),
				array('data'=>anchor('food_category/subcat_add/'.$row->sub_cat_id.'/'.$row->maincategory_id, 'edit').' | '.anchor('food_category/delete/subcat/'.$row->sub_cat_id.'/'.$row->maincategory_id, 'delete', array('class'=>'delete')), 'attributes'=>array('align'=>'center')),
                         'attributes'=>array('class'=>$class,'id'=>$row->sub_cat_id));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;

			$this->data['add_link'] = anchor('food_category/subcat_add/0/'.$id, "Add Subcategory");
			$this->data['page_heading'] = $this->category[$id];
			$vars = $this->theme->theme_vars('pages/food_subcategory_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function subcat() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->food_application_model->get_sub_categories();
			$header = array(array('data'=>'#'), array('data'=>'Name'), array('data'=>'Parent Category'), array('data'=>'Revenue'), array('data'=>'Section'), array('data'=>'Description'),array('data'=>'Status'), array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}

				$active='<input type="checkbox" name="chk[]"  onclick="check('.$row->sub_cat_id.','.$c.');"  '.$c. '  />';

				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('food_category/subcat_add/'.$row->sub_cat_id, $row->name)),
				array('data'=>anchor('food_category/edit/'.$row->maincategory_id, $this->category[$row->maincategory_id])),
				array('data'=>anchor('revenue/edit/'.$row->rvc_id, $this->revenue[$row->rvc_number])),
				array('data'=>anchor('sections/edit/'.$row->section_id, $this->section[$row->section_id])),
				array('data'=>$row->description),
				array('data'=>$active, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('food_category/subcat_add/'.$row->sub_cat_id, 'edit').' | '.anchor('food_category/delete/subcat/'.$row->section_id, 'delete', array('class'=>'delete')), 'attributes'=>array('align'=>'center')),
                         'attributes'=>array('class'=>$class));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			$this->data['add_link'] = anchor('food_category/add', 'Add Category')." &nbsp; ".anchor('food_category/subcat_add', 'Add Sub Category');
			$this->data['title'] = 'Food Subcategories';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/food_subcategories_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function subcat_add($id=0, $cat_id=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($id == 0) {
				$this->data = $this->food_library->sub_category_form(array('maincategory_id'=>$cat_id));
				$this->data['title'] = 'Add New Subcategory';
			}
			else {
				$param = array();
				$q = $this->food_application_model->get_sub_categories($id);
				foreach($q->result_array() as $row) {
					$param = $row;
				}
				 
				$this->data = $this->food_library->sub_category_form($param);
				$this->data['title'] = 'Edit Subcategory';
			}

			$this->data['cat_id'] = $cat_id;
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_food_subcategory_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function delete($opt="", $id="",$sid="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){


			if($opt == 'cat') {
				 
				$name=$this->common_model->get_cat_name($id);
				$nam=$name->row();
				$name=$nam->name;
				$this->watchdog->save('delete', 'IRD', 'food_category_delete',$id,$name);


				$name1=$this->common_model->get_sub_name($id);

				$name='Subcategories  ';
				$nam=$name1->result();
				if(count($nam)>0){
					//print_r($nam);die();
					foreach($nam as $row){
						$name.=$row->name;
						$name.=',';
					}
					$name.=' are deleted';
					$this->watchdog->save('delete', 'IRD', 'food_subcategory_delete',0,$name);
				}
				$main=$this->common_model->get_menu_name($id);

				$name='Menuitems  ';
				$nam=$main->result();
				//print_r($nam);die();
				if(count($nam)>0){
					foreach($nam as $row){
						$name.=$row->menuitem_name;
						$name.=',';
					}
					$name.=' are deleted';
					$this->watchdog->save('delete', 'IRD', 'menuitem_delete',0,$name);
				}

				$sub_cat=$this->common_model->get_sub($id);
				if($this->common_model->delete_record_for_id('food_subcategory', 'maincategory_id', $id)) {
					if($this->common_model->delete_record_for_id('food_maincategory', 'maincategory_id', $id)) {
						foreach($sub_cat->result() as $row){
							 
							$this->common_model->delete_record_for_id('food_menuitem_primary', 'sub_cat_id', $row->sub_cat_id);
							$this->common_model->delete_record_for_id('food_menuitem_details', 'sub_cat_id', $row->sub_cat_id);

						}
						$this->message->set('Category deleted successfully.', 'success', TRUE);
						redirect('food_category');
					}
					else {
						$this->message->set('Category child (subcategory) successfully deleted, but error in deletion main category.', 'error', TRUE);
						redirect('food_category');
					}
				}
				else {
					$this->message->set('Error in category deletion.', 'error', TRUE);
					redirect('food_category');
				}
			}

			if($opt == 'subcat') {
				$name=$this->common_model->get_subcat_name($id);
				$nam=$name->row();
				$name=$nam->name;

				$this->watchdog->save('delete', 'IRD', 'food_subcategory_delete',$id,$name);
				if($this->common_model->delete_record_for_id('food_subcategory', 'sub_cat_id', $id)) {

					$name=$this->common_model->get_food_name($id);

					$menu_item='Menu items ';
					$nam=$name->result();
					if(count($nam)>0){
						foreach($nam as $row){
							$menu_item.=$row->menuitem_name;
							$menu_item.=',';
						}
						$menu_item.=' are deleted';

						$this->watchdog->save('delete', 'IRD', 'menuitem_delete',0,$menu_item);
					}
					$this->common_model->delete_record_for_id('food_menuitem_primary', 'sub_cat_id', $id);
					$this->common_model->delete_record_for_id('food_menuitem_details', 'sub_cat_id', $id);


					$this->message->set('Subcategory deleted successfully.', 'success', TRUE);
					redirect('food_category/subcategory/'.$sid);
				}
				else {
					$this->message->set('Error in subcategory deletion.', 'error', TRUE);
					redirect('food_category');
				}
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function submit() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$form_id = $this->input->post('form_id');
			switch ($form_id) {
				case "add_category":
					//echo "<pre>"; print_r($_POST); die();
					if(!$this->food_library->category_validate($_POST)) {
						$name = $this->input->post('name');
						$display_name = $this->input->post('display_name');
						$description =''; //$this->input->post('description');

						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$q = $this->common_model->get_records_by_field('food_section', 'section_id', $this->input->post('section_id'));
						$rvc_id = 0;
						foreach($q->result() as $row) {
							$rvc_id = $row->rvc_number;
						}
						$d=0;
						$param = array('rvc_number'=>$rvc_id, 'sid'=>$this->input->post('section_id'), 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en'],'position'=>$d);
						$id = $this->food_application_model->save_categories($param);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name, 'description'=>$description), 'category', $id);
						if($id > 0) {
							$this->food_application_model->delete_food_locale('category', $id);
							foreach($locale as $row) {
								$this->food_application_model->save_food_cat($row);
							}
							$this->food_application_model->save_images($images, 'category', $id);

							$this->watchdog->save('add', 'IRD', 'food_category_add',0,$name);
							$this->message->set('Category added successfully.', 'success', TRUE);
							redirect('food_category');
						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in adding category.', 'error', TRUE);
							redirect('food_category/add');
						}

					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('food_category/add');
					}
					break;

				case "edit_category":
					//echo "<pre>"; print_r($_POST); die();
					if(!$this->food_library->category_validate($_POST)) {
						$cid = $this->input->post('cid');
						$name = $this->input->post('name');
						$display_name = $this->input->post('display_name');
						$description = $this->input->post('description');
						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$description['en']='';
						$q = $this->common_model->get_records_by_field('food_section', 'section_id', $this->input->post('section_id'));
						$rvc_id = 0;
						foreach($q->result() as $row) {
							$rvc_id = $row->rvc_number;
						}


						$param = array('rvc_number'=>$rvc_id, 'section_id'=>$this->input->post('section_id'), 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en']);
						$id = $this->food_application_model->save_categories($param, $cid);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name, 'description'=>$description), 'category', $id);
						// echo "<pre>"; print_r($locale); die();

						if($id > 0) {
							$this->food_application_model->delete_food_locale('category', $id);
							foreach($locale as $row) {
								$this->food_application_model->save_food_cat($row);
							}
							$this->food_application_model->save_images($images, 'category', $id);

							$this->watchdog->save('edit', 'IRD', 'food_category_edit',$cid,$name);
							$this->message->set('Category updated successfully.', 'success', TRUE);
							redirect('food_category');
						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in category updation.', 'error', TRUE);
							redirect('food_category/edit/'.$id);
						}
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('food_category/edit/'.$id);
					}
					break;

				case "add_subcategory":
					$cid = $this->input->post('maincategory_id');
					if(!$this->food_library->sub_category_validate($_POST)) {
						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$name = $this->input->post('name');
						$display_name = $this->input->post('display_name');
						$description = $this->input->post('description');
						$q = $this->common_model->get_records_by_field('food_maincategory', 'maincategory_id', $cid);
						$rvc_id = 0;
						$sid = 0;
						$rvc_number = 0;
						$description['en']='';
						foreach($q->result() as $row) {
							//$rvc_id = $row->rvc_id;
							$sid = $row->section_id;
							$rvc_number = $row->rvc_number;
						}

						$d=0;
						$param = array('rvc_number'=>$rvc_number, 'section_is'=>$sid, 'maincategory_id'=>$cid, 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en'],'paragon'=>$d);
						//echo "<pre>"; print_r($_POST); die();
						$id = $this->food_application_model->save_sub_categories($param);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name), 'subcategory', $id);

						if($id > 0) {
							$this->food_application_model->delete_food_locale('subcategory', $id);
							foreach($locale as $row) {
								$this->food_application_model->save_food_cat($row);
							}

							$this->food_application_model->save_images($images, 'subcategory', $id);

							$this->watchdog->save('add', 'IRD', 'food_subcategory_add',0,$name);
							$this->message->set('Subcategory added successfully.', 'success', TRUE);
							redirect('food_category/subcategory/'.$cid);
						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in adding category.', 'error', TRUE);
							redirect('food_category/subcat_add/0/'.$cid);
						}

					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('food_category/subcat_add/0/'.$cid);
					}
					break;

				case "edit_subcategory":
					$cid = $this->input->post('maincategory_id');
					if(!$this->food_library->sub_category_validate($_POST)) {
						$id = $this->input->post('id');

						$name = $this->input->post('name');
						$display_name = $this->input->post('display_name');
						$description = $this->input->post('description');
						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$q = $this->common_model->get_records_by_field('food_maincategory', 'maincategory_id', $cid);
						$rvc_id = 0;
						$sid = 0;
						$rvc_number = 0;
						foreach($q->result() as $row) {
							$sid = $row->section_id;
							$rvc_number = $row->rvc_number;
						}

						$q1 = $this->common_model->get_records_by_field('food_revenue_primary', 'rvc_number', $rvc_number);
						$row2 = $q1->result();
						$rvc_id = $row2[0]->rvc_id;
						$param = array('rvc_id'=>$rvc_id, 'rvc_number'=>$rvc_number, 'section_id'=>$sid, 'maincategory_id'=>$cid, 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en']);
						//echo '<pre>'; print_r($row2); die();
						$id = $this->food_application_model->save_sub_categories($param, $id);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name), 'subcategory', $id);

						if($id > 0) {
							$this->food_application_model->delete_food_locale('subcategory', $id);
							foreach($locale as $row) {
								$this->food_application_model->save_food_cat($row);
							}

							$this->food_application_model->save_images($images, 'subcategory', $id);

							$this->watchdog->save('add', 'IRD', 'food_subcategory_edit',$id,$name);
							$this->message->set('Subcategory updated successfully.', 'success', TRUE);
							redirect('food_category/subcategory/'.$cid);
						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in category updation.', 'error', TRUE);
							redirect('food_category/subcat_add/'.$id.'/'.$cid);
						}

					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('food_category/subcat_add/'.$id.'/'.$cid);
					}
					break;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

}
?>
