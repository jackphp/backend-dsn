<?php
/**
 * Description of tv_reports
 *
 * @author pbsl
 */
class tv_reports extends CI_Controller {
	var $floor_id;
	var $room_id;
	var $from_date;
	var $to_date;
	var $floor_name;
	var $room_name;
	var $view_as;
	var $age_group;
	var $room_type;
	var $param;
	var $rooms;
	public function  __construct() {
		parent::__construct();
		$perm = array('access room history');
		$this->user->set_access_permission($perm);
		$this->load->model('rms_model');
		$this->load->model('room_management_model');
		$this->load->library("forms");
		$this->load->helper('fusion_charts_helper');
		$form_id = $this->input->post("form_id");

		$selected_floor = "";
		$selected_room = "";
		if($form_id == "left_view_form") {
			$this->room_id = $this->input->post('rooms');
			$this->floor_id = $this->input->post('floors');
			$this->data['view_as'] = $this->input->post('viewas');
			$this->room_type = $this->input->post('room_type');
			//Room Configuration
			if($this->data['view_as'] == "room") {
				$this->data['rf_id'] = $this->room_id;
				$selected_room = $this->room_id;
				$q = $this->common_model->get_records_by_field('rooms', 'room_id', $this->room_id);
				$row = $q->result();
				$this->room_name = $row[0]->room_name;
				$this->rooms[] = $this->room_name;
				$q = $this->rms_model->get_room_info($this->room_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}
			}
			//Floor Configuration
			else if($this->data['view_as'] == 'floor') {
				$this->data['rf_id'] = $this->floor_id;
				$selected_floor = $this->floor_id;
				$q = $this->common_model->get_records_by_field('floors', 'fid', $this->floor_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}

				$q = $this->room_management_model->load_floor_rooms($this->floor_id);
				foreach($q->result() as $row) {
					$this->rooms[] = $row->room_name;
				}

			}
			else {
				$this->data['rf_id'] = "";
			}

			$this->from_date = $this->input->post('from_date');
			$this->to_date = $this->input->post('to_date');
		}
		else {
			$this->data['rf_id'] = $this->uri->segment(4);
			$this->data['view_as'] = $this->uri->segment(3);
			if($this->data['view_as'] == "room") {
				$selected_room = $this->uri->segment(4);
				$this->rooms[] = $selected_room;
				$q = $this->common_model->get_records_by_field('rooms', 'room_id', $this->data['rf_id']);
				$row = $q->result();
				$this->room_name = $row[0]->room_name;
				$this->room_id = $this->data['rf_id'];

				$q = $this->rms_model->get_room_info($this->room_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}
			}
			else if($this->data['view_as']=='floor'){
				$selected_floor = $this->uri->segment(4);
				$q = $this->common_model->get_records_by_field('floors', 'fid', $selected_floor);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
					$this->floor_id = $this->data['rf_id'];
				}
				$q = $this->room_management_model->load_floor_rooms($this->floor_id);

				foreach($q->result() as $row) {
					$this->rooms[] = $row->room_name;
				}
			}
			else {
				$this->data['rf_id'] = "";
			}


			$this->from_date = '2011-03-01';
			$this->to_date = date('Y-m-d');
		}


		$q = $this->room_management_model->load_floors();
		$rooms = result_to_array($q->result(), 'room_id', 'room_name');
		$floors = result_to_array($q->result(), 'fid', 'fname');

		$this->data['rooms'] =  form_dropdown('rooms', $rooms, $selected_room);
		$this->data['floors'] = form_dropdown('floors', $floors, $selected_floor);
		$this->data['from_date'] = $this->from_date;
		$this->data['to_date'] = $this->to_date;
		$this->data['title'] = 'Room History';
		$this->data['room_type'] = $this->room_type;
		//Loading RMS Library
		$this->param = array('view_as'=>$this->data['view_as'], 'room_id'=>$this->room_id, 'floor_id'=>$this->floor_id, 'from_date'=>$this->from_date, 'to_date'=>$this->to_date, 'room_type'=>$this->room_type);
		$this->load->library("rms_library", $this->param);
		$this->load->library('tv_library', $this->param);
	}

	public function reports($view_as, $room_id="") {
		if($this->user->is_user_access()) {
			$rooms = array();
			$tmp_view = $this->input->post('viewas');
			if($tmp_view != "") {
				$view_as = $tmp_view;
			}

			if($view_as == "room") {
				$heading = "TV Reports of (".$this->room_name.")".$this->from_date." to ".$this->to_date;
				$rooms[] = $room_id;
			}

			else if($view_as == "floor") {
				$heading = 'TV Reports of ('.$this->floor_name.') '.$this->from_date.' to '.$this->to_date;
			}
			else{
				$heading = 'TV Reports of all Rooms '.$this->from_date.' to '.$this->to_date;
			}

			if(count($this->rooms) > 0) {
				$this->param['rooms'] = implode(',', $this->rooms);
			}
			else {
				$this->param['rooms'] = 0;
			}

			$tv_usages = $this->rms_model->get_tv_usages_data_room_wise($this->param);
			$channel_usage = $this->rms_model->get_channel_usages_data($this->param);
			$floor_wise_tv_use = $this->rms_model->get_floor_tv_usages_wise_hrs($this->param);
			$avg_tv_use = $this->rms_model->average_tv_usages($this->param);

			$this->data['page_heading'] = $heading;
			$this->data['title'] = $heading;

			$this->data['data'] = $this->tv_library->tv_graphs(array('tv_data'=>$tv_usages->result(), 'channel_data'=>$channel_usage->result(), 'floor_wise_tv_use'=>$floor_wise_tv_use->result(), 'avg_tv_use'=>$avg_tv_use->result(), 'rooms'=>$this->param['rooms']), $this->param);

			$vars = $this->theme->theme_vars('pages/tv_history_view', $this->data, array('left'=>'left/room_history_left', 'left_items'=>array('tab'=>'4', 'menu1'=>'TV', 'menu2'=>'', 'left_action'=>'tv_reports/reports/'.$view_as.'/'.$this->data['rf_id'])));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}


	public function hour_wise_channel_data($opt) {
		$room_id = $this->input->post('room_id');
		$from_date = $this->input->post('from_date');
		$to_date = $this->input->post('to_date');
		$channel_name = $this->input->post('channel_name');
		$room_type = $this->input->post('room_type');

		$formatted_from_date = "";
		$formatted_to_date = "";
		$formatted_from_date = format_date($from_date, 'custom', 'D, m/d/Y');

		if($to_date != "") {
			$formatted_to_date = format_date($to_date, 'custom', 'D, m/d/Y');
		}

		$formatted_from_date = '2011-03-01';
		$this->param['from_date'] = '2011-03-01';

		switch ($opt) {
			case "channel_hour_wise":
				$this->param['rooms'] = $room_id;
				$this->param['channel_name'] = $channel_name;
				$this->param['room_type'] = $room_type;
				//echo "<pre>";  print_r($this->param); die();
				$q = $this->rms_model->get_tv_channel_data_hour_wise($this->param);
				$rows = array();
				foreach($q->result() as $row) {
					$hrsArr = explode(":", $row->total_hrs);
					$total_time = $hrsArr[0].'.'.$hrsArr[1];
					$rows[] = array('hour'=>$row->hour, 'total_hrs'=>$total_time);
				}

				$param = array('from_date'=>$formatted_from_date, 'to_date'=>$formatted_to_date, 'channel_name'=>$channel_name, 'data'=>$rows, 'room_type'=>$room_type);
				//echo "<pre>"; print_r($rows); die();
				echo tv_channel_view_hour_wise_graph($param);
				break;
		}
	}
	 
}
?>