<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of permission
 *
 * @author pbsl
 */
class permission extends CI_Controller {

	public function  __construct() {
		parent::__construct();
	}

	public function access_denied() {
		$this->data['title'] = 'Access Denied';
		$this->data['page_heading'] = 'Access Denied';
		$this->data['role_list_link'] = anchor('admin/roles', 'Role list');
		$vars = $this->theme->theme_vars('pages/access_denied_view', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	}


}
?>
