<?php
/**
 *
 * @author pbsl
 */
class restaurants extends CI_Controller {
	var $data = array();
	var $lang = array();
	var $rvc  = array();

	var $revenue = array();
	var $section = array();
	var $category = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = "Food Restaurents";
		$perm = array('restaurent management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Restaurants', 'path'=>'restaurants', 'permission'=>'restaurent management', 'menu_order'=>0)));

		$this->menu->set_menu($menu);
		$this->load->library('food_library');
		$this->load->library('food_item_library');
		$this->load->model('food_application_model');

		$this->revenue = $this->food_library->get_table_data('food_revenue_primary', 'rvc_id');
		$this->section = $this->food_library->get_table_data('food_section', 'section_id');
		$this->category = $this->food_library->get_table_data('food_maincategory', 'maincategory_id');
		$this->food_library->initialize_food_vars(array('revenue'=>$this->revenue, 'sections'=>$this->section, 'category'=>$this->category));
	}


	public function index() {
		if($this->user->is_user_access()) {
			$q = $this->food_application_model->get_restaurant_list();
			$header = array(array('data'=>'#'), array('data'=>'Name'), array('data'=>'Description'), array('data'=>'Active'),array('data'=>'Action'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				if($row->is_active == 0) { $active = "Deactive"; } else { $active = "Active"; }

				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('restaurants/add/'.$row->restaurant_id, $row->restaurant_name)),
				array('data'=>$row->description),
				array('data'=>$active, 'attributes'=>array('align'=>'center')) ,
				array('data'=>anchor('restaurants/delete/'.$row->restaurant_id,'Delete'),'attributes'=>array('align'=>'center','onclick'=>'return chk();')),
                        'attributes'=>array('class'=>$class));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			$this->data['add_link'] = anchor('restaurants/add', 'Add Restaurant');
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/revenue_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete($id=""){
		if($this->user->is_user_access()) {
			if($id > 0) {
				$this->food_application_model->delete_restaurant($id);
				redirect('restaurants');
			}
		}
		else {
			$this->user->user_access_denied();
		}

	}

	public function add($id="") {
		if($this->user->is_user_access()) {
			if($id > 0) {
				$q = $this->food_application_model->get_restaurant_list($id);
				$row = $q->result_array();
				$this->data = $this->food_library->restaurant_form($row[0]);
				$this->data['title'] = 'Edit Restaurant';
			}
			else {
				$this->data = $this->food_library->restaurant_form();
				$this->data['title'] = 'Add Restaurant';
			}
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_restaurant_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function submit() {
		$form_id = $this->input->post('form_id');
		switch ($form_id) {
			case "add_restaurant":
				if(!$this->food_library->restaurant_validate($_POST)) {
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');
					$name = $this->input->post("name");
					$rvc_id = $this->input->post("rvc_id");
					$description = $this->input->post("description");

					if(isset($videos['video']['file_name'])) {
						$details_param = array('name'=>$name['en'], 'description'=>$description['en'], 'video'=>$videos['video']['file_name'], 'is_active'=>$this->input->post('active'));
					}
					else {
						$details_param = array('name'=>$name['en'], 'description'=>$description['en'], 'video'=>'', 'is_active'=>$this->input->post('active'));
					}

					$primary_param = array('name'=>$name['en'], 'modified'=>time(), 'is_active'=>$this->input->post('active'));
					$param = array('primary'=>$primary_param, 'details'=>$details_param);

					$rid = $this->food_application_model->save_restaurant($param);
					$restaurant_locale = $this->food_library->prepare_locale_row(array('name'=>$name, 'description'=>$description), 'restaurant', $rid);
					if($rid > 0) {
						// Restaurant Image.
						$this->food_application_model->save_images($images, 'restaurant', $rid);

						$this->food_application_model->delete_food_locale('restaurant', $rid);
						foreach($restaurant_locale as $row) {
							$this->food_application_model->save_food_locale($row);
						}

						$this->food_application_model->delete_restaurant_rvc($rid);
						foreach($rvc_id as $rno) {
							$this->food_application_model->save_restaurant_rvc($rid, $rno);
						}

						$this->message->set('Restaurant successfully added.', 'success', TRUE);
						redirect('restaurants');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in adding restaurant.', 'error', TRUE);
						redirect('restaurants/add');
					}
				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('restaurants/add');
				}
				break;

			case "edit_restaurant":
				if(!$this->food_library->restaurant_validate($_POST)) {
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');
					$name = $this->input->post("name");
					$rvc_id = $this->input->post("rvc_id");
					$description = $this->input->post("description");

					if(isset($videos['video']['file_name'])) {
						$details_param = array('restaurant_name'=>$name['en'], 'description'=>$description['en'], 'video'=>$videos['video']['file_name'], 'is_active'=>$this->input->post('active'));
					}
					else {
						$details_param = array('restaurant_name'=>$name['en'], 'description'=>$description['en'], 'is_active'=>$this->input->post('active'));
					}

					$primary_param = array('name'=>$name['en'], 'modified'=>time(), 'is_active'=>$this->input->post('active'));
					$param = array('primary'=>$primary_param, 'details'=>$details_param);

					$rid = $this->food_application_model->save_restaurant($param, $this->input->post('restaurant_id'));
					$restaurant_locale = $this->food_library->prepare_locale_row(array('name'=>$name, 'description'=>$description), 'restaurant', $rid);
					if($rid > 0) {
						// Restaurant Image.
						$this->food_application_model->save_images($images, 'restaurant', $rid);
						$this->food_application_model->delete_food_locale('restaurant', $rid);
						foreach($restaurant_locale as $row) {
							$this->food_application_model->save_food_locale($row);
						}

						$this->food_application_model->delete_restaurant_rvc($rid);
						foreach($rvc_id as $rno) {
							$this->food_application_model->save_restaurant_rvc($rid, $rno);
						}

						$this->message->set('Restaurant successfully updated.', 'success', TRUE);
						redirect('restaurants');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in restaurant updation.', 'error', TRUE);
						redirect('restaurants/add'.$this->input->post('restaurant_id'));
					}
				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('restaurants/add/'.$this->input->post('restaurant_id'));
				}

				break;
		}
	}
}
?>