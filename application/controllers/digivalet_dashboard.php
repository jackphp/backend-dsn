<?php

class Digivalet_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 * 	- or -
	 * 		http://example.com/index.php/welcome/index
	 * 	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$db2 = $this->load->database('iremote', TRUE);
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'url'));
		$this->load->model('dashboard_login');
	}

	public function front_office() {
		//ini_set('display_error',1);
		//error_reporting(-1);
		$user_page = $this->uri->segment(2);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['page'] = $this->dashboard_login->getPageTitle();
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(1);
			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('dashboard_pages/dashboard.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function front_office1() {
		//ini_set('display_error',1);
		//error_reporting(-1);
		$user_page = $this->uri->segment(2);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['page'] = $this->dashboard_login->getPageTitle();
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(1);
			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('dashboard_pages/dashboard1.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function inbox() {
		$user_page = $this->uri->segment(2);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$data['page'] = $this->dashboard_login->getPageTitle();
			$modules = $this->dashboard_login->getUserModules(3);
			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('dashboard_pages/inbox.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function login() {

		$this->load->view('dashboard_pages/login.php');
	}

	public function check_login() {

		//checking or run validation

		$this->load->model("dashboard_login");
		$result = $this->dashboard_login->validate_login($_REQUEST['login'], $_REQUEST['pass']);
		if ($result == 1) {
			$url = $this->dashboard_login->getUserUrl();
			// $res = $this->dashboard_login->update_islogin();
			// print_r($url);die();
			$url = $url['module_url'];
			echo '{"logged":"true","newLocation":"' . $url . '"}';
		} else if ($result == 0) {
			// echo '{"logged":"false","newLocation":"login"}';
			// $data['error'] = "User already login or User name and password does not match";
		}
	}

	function update_user() {

		$operation = trim($_REQUEST['operation']);
		if ($operation == "change_name") {

			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->update_name($_REQUEST['userName']);
		}
		if ($operation == "change_pass") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->update_password($_REQUEST['oldPass'], $_REQUEST['newPass']);
		}
		if ($operation == "change_thumb") {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->do_upload($_FILES['imageFile']);
		}
	}

	function logout() {
		//insert into auditlog table
		$this->load->model("dashboard_login");
		$id = $this->session->userdata("user_id");
		$this->db->set("user", $id);
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->db->set("ip", $ip);
		$this->db->set("event", "logged out");
		$result = $this->db->insert('auditlog');
		// $res= $this->dashboard_login->user_logout();

		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		$this->load->view('dashboard_pages/login.php');
	}

	public function ats() {
		$user_page = $this->uri->segment(2);
		$page['page'] = $this->uri->segment(2);
		$this->session->set_userdata($page);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['page'] = $this->dashboard_login->getPageTitle();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(2);
			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('dashboard_pages/ats', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function json() {

		$this->load->view('dashboard_pages/dashboard_json.php');
	}

	public function user_json() {

		$this->load->view('dashboard_pages/user_preference.php');
	}

	public function audit_log() {
		error_reporting(E_ALL ^ E_NOTICE);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['page'] = $this->dashboard_login->getPageTitle();

			$data['allroom'] = $this->dashboard_login->getAllRooms();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(8);
			if ($_POST['page'] == "filter") {
				$this->load->model("dashboard_login");
				$result = $this->dashboard_login->getUserFilter($_POST);
				$data['detail'] = $result;
				$data['aud'] = "audit";
				$this->load->view('dashboard_pages/user_log.php', $data);
			} else {
				$this->load->view('dashboard_pages/user_log.php', $data);
			}
			/* if ($modules=="0")
			 {
			 redirect('digivalet_dashboard/login');
			 }
			 else{
			 $this->load->view('dashboard_pages/engg',$data);
			 } */
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function engg() {
		$user_page = $this->uri->segment(2);
		$page['page'] = $this->uri->segment(2);
		$this->session->set_userdata($page);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['page'] = $this->dashboard_login->getPageTitle();

			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(7);
			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('dashboard_pages/engg.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function temperature_json() {

		$this->load->view('dashboard_pages/temperature_json.php');
	}

	public function message_log() {
		error_reporting(E_ALL ^ E_NOTICE);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$result = $this->dashboard_login->getUserInfo();
			$data['page'] = $this->dashboard_login->getPageTitle();
			$data['alluser'] = $this->dashboard_login->getAllUser();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(8);
			if ($_POST['page'] == "filter") {
				$this->load->model("dashboard_login");
				$result = $this->dashboard_login->getMessageFilter($_POST);
				$data['detail'] = $result;
				$data['aud'] = "audit";
				$this->load->view('dashboard_pages/message_log.php', $data);
			} else {
				$this->load->view('dashboard_pages/message_log.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function room_registration() {
		$user_page = $this->uri->segment(2);
		$page['page'] = $this->uri->segment(2);
		$this->session->set_userdata($page);
		$id = $this->session->userdata("user_id");
		if ($id) {
			$this->load->model("dashboard_login");
			$this->load->model("room_registration_model");
			$result = $this->dashboard_login->getUserInfo();
			$data['page'] = $this->dashboard_login->getPageTitle();
			$data['user_info'] = $result;
			$data['left'] = $this->dashboard_login->user_profile();
			$modules = $this->dashboard_login->getUserModules(14);

			$rs_token = $this->room_registration_model->get_token_list();
			$str_token_list = '';
			$str_token_list = "<option value=''>Select token</option>";
			foreach ($rs_token as $token) {
				$str_token_list = $str_token_list . "<option value = '" . $token->token . "'>{$token->token}</option>";
			}

			$data['token_list'] = $str_token_list;

			if ($modules == "0") {
				redirect('digivalet_dashboard/login');
			} else {
				$this->load->view('room_registration/registration.php', $data);
			}
		} else {
			$this->load->view('dashboard_pages/login.php');
		}
	}

	public function check_token() {
		error_reporting(E_ALL ^ E_NOTICE);
		$token = $_REQUEST['token'];
		$this->load->model("room_registration_model");
		//check token is new or already exist
		$data['result'] = $this->room_registration_model->check_token($token);
		$this->load->view('room_registration/token_detail.php', $data);
	}


	public function update_room() {

		error_reporting(E_ALL ^ E_NOTICE);
		$room_no = trim($_REQUEST['room_no']);
		$token = trim($_REQUEST['token']);
		$old_token = trim($_REQUEST['old_token']);
		$this->load->model("room_registration_model");
		//$tokev_val=$this->dashboard_login->check_token($token);
		$result = $this->room_registration_model->update_room_token($token, $room_no,$old_token);
		if ($result == 1) {
			//echo "<h2 style='color: #4F8A10; background-color: #DFF2BF;font-size: 14px;padding-left: 15px; width: 150px;'>Update Sucessfuly</h2>";
			//$str.='<script src='.get_assets_path('js').'dashboard/developr.message.js></script>';
			$str.='<script>$("#sucess_msg").message("update sucessfully", {classes: ["green-gradient"],arrow: "top"})</script>';
			// $str.='<script>$("#sucess_msg").message("update sucessfully.", { append: false,classes: ["green-gradient"],  groupSimilar: false, showCloseOnHover: false }); $(".message").fadeOut(3000)</script>';
			 
			echo $str;
		} else {
			$str.='<script>$("#sucess_msg").message("Error Wrong Token", {classes: ["red-gradient"],arrow: "top"})</script>';
			//$str.='<script>$("#sucess_msg").message("update sucessfully.", { append: false,classes: ["green-gradient"],  groupSimilar: false, showCloseOnHover: false }); $(".message").fadeOut(3000)</script>';
			echo $str;
		}
	}
	public function new_room_token() {

		error_reporting(E_ALL ^ E_NOTICE);
		$room_no = trim($_REQUEST['room_no']);
		$token = trim($_REQUEST['token']);
		$this->load->model("room_registration_model");
		//$tokev_val=$this->dashboard_login->check_token($token);
		$result = $this->room_registration_model->new_room_token($token,$room_no);
		if ($result == 1) {
			//echo "<h2 style='color: #4F8A10; background-color: #DFF2BF;font-size: 14px;padding-left: 15px; width: 150px;'>Update Sucessfuly</h2>";
			//$str.='<script src='.get_assets_path('js').'dashboard/developr.message.js></script>';
			$str.='<script>$("#sucess_msg").message("update sucessfully", {classes: ["green-gradient"],arrow: "top"})</script>';
			echo $str;
		} else {
			$str.='<script>$("#sucess_msg").message("Error Wrong Token", {classes: ["red-gradient"],arrow: "top"})</script>';
			echo $str;
		}
	}

	public function check_room() {
		error_reporting(E_ALL ^ E_NOTICE);
		$this->load->model("room_registration_model");
		$room_no = $_REQUEST[room_no];
		if (!empty($room_no)) {
			$room_id = $this->room_registration_model->check_room($room_no);
			$room_already_token = $this->room_registration_model->check_token_room(($room_no));

			if (empty($room_id)) {
				echo $str.='<script>$("#room_detail").message("Invalid roon number! Room ' . $room_no . ' not available", {classes: ["red-gradient"],arrow: "top"})</script>';
			} else if (!empty($room_already_token)) {
				//                echo trim($room_already_token['token']);
				echo "tokenval";
				//
			} else {
				echo "success";
			}
		}
	}

	public function get_room_detail() {
		// print_r($_REQUEST);die();
		error_reporting(E_ALL ^ E_NOTICE);
		$this->load->model("room_registration_model");
		$room_no = $_REQUEST[room_no];
		$token = trim($_REQUEST['token']);
		$data['rooms'] = $this->room_registration_model->check_room_status($room_no);
		$data['room_no']=$room_no;
		$data['token']=$token;
		$this->load->view('room_registration/token_detail.php', $data);
		//        $strResult = "";
		//        if (count($room_no) > 0) {
		//            $strResult = $strResult .'<div style="width:560px;">';
		//            $strResult = $strResult . '<table class="table" style="width: 100%;margin-top: 20px;float:left">';
		//            $strResult = $strResult . '<thead>';
		//            $strResult = $strResult . '<tr>';
		//            $strResult = $strResult . '<th scope="col">Room No</th>';
		//            $strResult = $strResult . '<th scope="col" class="align-center">IP</th>';
		//            $strResult = $strResult . '<th scope="col" class="align-center">MAC Address</th>';
		//            $strResult = $strResult . '<th scope="col" class="align-center">Token</th>';
		//            $strResult = $strResult . '<th scope="col" class="align-center"> -</th>';
		//            $strResult = $strResult . '</tr>';
		//            $strResult = $strResult . '</thead>';
		//            $strResult = $strResult . '<tfoot>';
		//            $strResult = $strResult . '<tr>';
		//            $strResult = $strResult . '<td colspan="5">&nbsp;</td>';
		//            $strResult = $strResult . '</tr>';
		//            $strResult = $strResult . '</tfoot>';
		//            $strResult = $strResult . '<tbody>';
		//            foreach ($rooms as $room) {
		//                $strResult = $strResult . '<tr>';
		//                $strResult = $strResult . '<td class="align-center">' . $room->room_no . '</td>';
		//                $strResult = $strResult . '<td class="align-center">' . $room->pc_ip . '</td>';
		//                $strResult = $strResult . '<td class="align-center">' . $room->MAC . '</td>';
		//                $strResult = $strResult . '<td class="align-center">' . $room->token . '</td>';
		//                $strResult = $strResult . '<td class="align-center"><a href="#" class="button compact" onclick="updateFunction("'.$room_no.'","'.$token.'","'.$room->token.'")";>Button</a></td>';
		//                $strResult = $strResult . '</tr>';
		//            }
		//
		//            $strResult = $strResult . '</tbody>';
		//            $strResult = $strResult . '</table>';
		//            $strResult = $strResult . '</div>';
		//            echo $strResult;
		//        }
	}
	public function gettoken(){
		error_reporting(E_ALL ^ E_NOTICE);
		$this->load->model("room_registration_model");
		$rs_token = $this->room_registration_model->get_token_list();
		$str_token_list = '';
		$str_token =' <select id="token" name="token" class="select">';
		$str_token_list = $str_token."<option value=''>Select token</option>";
		foreach ($rs_token as $token) {
			$str_token_dropdown.=  "<option value = '" . $token->token . "'>{$token->token}</option>";
		}
		$token_list ='</select>';
		echo $str_token_list.$str_token_dropdown.$token_list;

	}
	public function delete_ipaddress() {
		error_reporting(E_ALL ^ E_NOTICE);
		$ipaddress = $_REQUEST[ipaddress];
		$this->load->model("room_registration_model");
		$rs_token = $this->room_registration_model->delete_ipaddress($ipaddress);
		if ($rs_token==1) {
			$str.='<script>$("#error").message("Deleted sucessfully", {classes: ["green-gradient"],arrow: "top"})</script>';
			echo $str;
		} else {
			$str.='<script>$("#error").message("Wrong Ip Address", {classes: ["red-gradient"],arrow: "top"})</script>';
			echo $str;
		}
	}
}
