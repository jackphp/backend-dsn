<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class floor extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("floor_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'Floors';
		$perm = array('floor management');
		$this->user->set_access_permission($perm);



	}

	public function index() {
		if($this->user->is_user_access()) {
			 
			$query=$this->floor_model->get_floor();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$result=$this->floor_model->get_list();
			$this->data['type_list']=$result;
			$this->data['page_heading']='Floor Details';
			$vars = $this->theme->theme_vars('pages/floors/floor_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function save() {
		if($this->user->is_user_access()) {
			if($this->input->post('floor_add')){
				$floor_type= $this->input->post('floor_add');
				$floor_desc= $this->input->post('floor_desc');
				$this->watchdog->save('add', 'Floors', 'floor_type_add',0,$floor_type);
				$this->floor_model->add_floor_type($floor_type,$floor_desc);
			}
			else if($this->input->post('id')) {
				$id=$this->input->post('id');
				$floor_type_id=$this->input->post('type');
				//echo $room_type;
				$name=$this->input->post('name');
				$this->watchdog->save('edit', 'Floors', 'floor_edit',$id,$name);
				$this->floor_model->update_floors($floor_type_id,$name,$id);

			}
			else {
				$floor_type_id=$this->input->post('type');
				//echo $room_type;
				$name=$this->input->post('name');
				$this->watchdog->save('add', 'Floors', 'floor_add',$floor_type_id,$name);
				$this->floor_model->insert_floors($floor_type_id,$name);
				 
				 
				 
				 

			}
			redirect('floor');
			 
			 
		}
		else {
			$this->user->user_access_denied();
		}


	}
	public function edit($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			{

				 
				$res=$this->floor_model->get_floors($id);
				 
				 
				 
			}
			$result=$this->floor_model->get_list();
			$this->data['type_list']=$result;
			$query=$this->floor_model->get_floor();
			// $this->load->view('/pages/rooms/room_type_view');
			$this->data['result'] = $query;
			$this->data['page_heading']='Floor Details';
			$this->data['edit'] = $res;
			$vars = $this->theme->theme_vars('pages/floors/floor_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}



	}
	public function floors(){
		$rs=$this->floor_model->floors();
		echo $rs;
	}
	public function floors_up($id=0){
		$rs=$this->floor_model->floors_up($id);
		echo $rs;
	}
	public function delete($id=0) {
		if($this->user->is_user_access()) {
			if($id>0)
			{
				$name=$this->floor_model->get_name($id);
				$nam=$name->row();
				$name=$nam->floor_no;
				$this->watchdog->save('delete', 'Floors', 'floor_delete',$id,$name);
				$query=$this->floor_model->delete_floors($id);
				redirect('floor');
				 
				 
				 
			}
			 
			 
			 

		}
		else {
			$this->user->user_access_denied();
		}
	}
	function delete_all(){
		if($this->user->is_user_access()) {
			$del=$this->input->post('check');

			$floor='Floor No ';
			foreach($del as $row) {
				$floor_no=$this->floor_model->get_no($row);
				$floors=$floor_no->row();
				$fl=$floors->floor_no;
				$floor.=$fl;
				$floor.=',';
			}
			$floor.=' are deleted';
			$this->watchdog->save('delete', 'Floors', 'floor_selected_delete',0,$floor);
			$query=$this->floor_model->delete_all_floors($del);
			redirect('floor');
		}
		else {
			$this->user->user_access_denied();
		}


	}


}// End Of class
?>
