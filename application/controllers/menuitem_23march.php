<?php
/**
 * Description of menuitem
 *
 * @author pbsl
 */
class menuitem extends CI_Controller {
	var $data;
	var $item_id;
	var $logo;
	var $description;
	var $video;
	var $revenue  = array();
	var $section  = array();
	var $category = array();
	var $subcat   = array();
	var $menuitem = array();

	public function __construct() {
		parent::__construct();
		$this->data['title'] = "Food Menu Items";
		$perm = array('menuitem management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Menuitems', 'path'=>'menuitem', 'permission'=>'menuitem management', 'menu_order'=>8),
		array('label'=>'Add Menuitem', 'path'=>'menuitem/item', 'permission'=>'menuitem management', 'menu_order'=>9)
		));
		$this->menu->set_menu($menu);
		$this->load->library('message');
		$this->load->library('food_library');
		$this->load->library('food_item_library');
		$this->load->library('pagination');
		$this->load->model('food_application_model');
		$this->load->model('food_addon_model');
		$this->load->model('group_addon_model');
		$this->revenue = $this->food_library->get_table_data('food_revenue_primary', 'rvc_number', 'rvc_name');

		$this->section = $this->food_library->get_table_data('food_section', 'section_id', 'name');
		$this->category = $this->food_library->get_table_data('food_maincategory', 'maincategory_id', 'name', '', 'name');
		$this->subcat = $this->food_library->get_table_data('food_subcategory', 'sub_cat_id', 'name', '', 'name');
		$this->menuitem = $this->food_library->get_table_data('food_menuitem_primary', 'mid', 'name');
		$this->food_library->initialize_food_vars(array('revenue'=>$this->revenue, 'sections'=>$this->section, 'category'=>$this->category, 'subcat'=>$this->subcat, 'menuitem'=>$this->menuitem));
		$this->data['menu_section'] = 'food';
	}

	public function index($offset="", $rs="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($offset=="") $offset=0;

			if($rs != "") {
				$q = $rs;
			}
			else {
				$q = $this->food_application_model->get_menuitems($offset);
				$config['total_rows'] = $this->db->count_all("food_menuitem_details");
				$config['per_page'] = '25';
				$config['num_links'] = 5;
				$config['base_url'] = base_url().'menuitem/menulist';
				$this->pagination->initialize($config);
				//$this->data['pager'] = $this->pagination->create_links();
			}


			$cat_opt = $this->category;
			$cat_opt["0"] = "All";

			$subcat_opt = $this->subcat;
			$subcat_opt["0"] = "All";

			$filter_cat = form_multiselect('filter_cat', $cat_opt, "0", "id='filter_cat' style='width:100px'");
			$filter_subcat = form_multiselect('filter_subcat', $subcat_opt, $subcat_opt, "id='filter_subcat' style='width:100px'");

			$header = array(array('data'=>'&nbsp;&nbsp;#&nbsp;'), array('data'=>'Name'), array('data'=>'Display Name', 'attributes'=>array('style'=>'text-align:center;')), array('data'=>'Category<br/>'.$filter_cat, 'attributes'=>array('style'=>'text-align:left;')),  array('data'=>'Subcategory'.$filter_subcat, 'attributes'=>array('style'=>'text-align:left;')), array('data'=>'Description','attributes'=>array('style'=>'text-align:center;')), array('data'=>'Is Active'),array('data'=>'Operation'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$active='<input type="checkbox" name="chk[]" id="'.$row->mid.'"  onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
				//                if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
				$rows[] =array(
				array('data'=>$i, 'attributes'=>array('style'=>'width:2%;text-align:center;')),
				array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%;text-align:left;')),
				array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:23%;text-align:center;')),
				array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
				array('data'=>anchor('menuitem/item/'.$row->mid, 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%;text-align:center;')),
                         'attributes'=>array('class'=>$class));
			}

			$total = $this->food_application_model->get_update_menuitems($offset);
			$this->data['total']=$total->num_rows();
			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			//$this->data['add_link']= array('data'=>anchor('menuitem/item','Add Menu Item'), 'attributes'=>array('class'=>'primary btn'));
			$this->data['add_link'] = anchor('menuitem/item', 'Add Menu Item',array('class' => 'primary btn'));
			$this->data['page_heading'] = $this->data['title'];
			$this->load->model("dashboard_login");
			$this->data['left']=$this->dashboard_login->user_profile();
			$this->data['search']['form_open'] = form_open('menuitem/search');
			$this->data['search']['form_close'] = form_close();

			$vars = $this->theme->theme_vars('pages/revenue_view', $this->data);
			 
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function update($offset="", $rs="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($offset=="") $offset=0;

			if($rs != "") {
				$q = $rs;
			}
			else {
				$q = $this->food_application_model->get_update_menuitems($offset);
				$config['total_rows'] = $this->db->count_all("food_menuitem_details");
				$config['per_page'] = '25';
				$config['num_links'] = 5;
				$config['base_url'] = base_url().'menuitem/menulist';
				$this->pagination->initialize($config);
				//$this->data['pager'] = $this->pagination->create_links();
			}


			$cat_opt = $this->category;
			$cat_opt["0"] = "All";

			$subcat_opt = $this->subcat;
			$subcat_opt["0"] = "All";

			$filter_update_cat = form_multiselect('filter_update_cat', $cat_opt, "0", "id='filter_update_cat' style='width:100px'");
			$filter_update_subcat = form_multiselect('filter_update_subcat', $subcat_opt, $subcat_opt, "id='filter_update_subcat' style='width:100px'");

			$header = array(array('data'=>'&nbsp;&nbsp;&nbsp;#&nbsp;&nbsp;'), array('data'=>'Name'), array('data'=>'Display Name'), array('data'=>'Category<br/>'.$filter_update_cat, 'attributes'=>array('style'=>'text-align:left;')),  array('data'=>'Subcategory<br/>'.$filter_update_subcat, 'attributes'=>array('style'=>'text-align:left;')), array('data'=>'Description'), array('data'=>'Is Active'),array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$active='<input type="checkbox" name="chk[]" id="'.$row->mid.'"  onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
				//                if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
				$rows[] =array(
				array('data'=>$i, 'attributes'=>array('style'=>'width:2%;text-align:center;')),
				array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%;text-align:left;')),
				array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:23%;text-align:left;')),
				array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
				array('data'=>anchor('menuitem/item/'.$row->mid.'/up', 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%;text-align:center;')),
                         'attributes'=>array('class'=>$class));
			}

			$this->data['title'] = "Updated Menu Items";
			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			//$this->data['add_link']= array('data'=>anchor('menuitem/item','Add Menu Item'), 'attributes'=>array('class'=>'primary btn'));
			$this->data['add_link'] = anchor('menuitem/item', 'Add Menu Item',array('class' => 'primary btn'));
			$this->data['page_heading'] = $this->data['title'];

			$this->data['search']['form_open'] = form_open('menuitem/search');
			$this->data['search']['form_close'] = form_close();

			$vars = $this->theme->theme_vars('pages/update_revenue_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function food_report() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->load->model("report_model");
			$q = $this->report_model->get_report();
			if($q==''){
				$this->data['row'] ='';
			}
			else{
				$this->data['row'] = $q->result();
			}
			 
			//$this->data['page_heading'] = 'Scheduled Letters';
			$this->data['page_heading'] = 'Food Order Report';
			$vars = $this->theme->theme_vars('pages/food_report', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function add_more_time(){
		$mycontent='<tr style="height:30px">';
		$mycontent .='<td><em style=" margin-left: 10px"><b> From</b></em> <input class="ck input" type="text" name="from1[]" value="" autocomplete="off" readonly="yes"  size="5"     /><em><b>To</b></em> <input style="margin-left:5px;" class="input" type="text"  name="to1[]" autocomplete="off" value="" readonly="yes" size="5"/><a  href="#?" class="delete" onclick="delete_row(this)" title="Click here to remove entry" style="color:red;margin-left:40px;"><strong>X</strong></a></td>';
		$mycontent .='</tr>';
		echo $mycontent;
	}

	public function generate() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			exec("sh ".$_SERVER['DOCUMENT_ROOT']."/backend/sqlite/mysql_sqlite backend");
			exec("sudo chmod -R 777 ".BASE_PATH);
			rename(BASE_PATH."/backend.sqlite", FOOD_DB_PATH);
			$this->drope_tables();

			//send_request(FOOD_PUSH_UPDATE);

			$this->message->set("Database successfully generated.", "success", TRUE);
			$this->watchdog->save('Update', 'IRD', 'update_room',0,'Commit Changes');
			redirect("menuitem");

		}else{
			redirect('/digivalet_dashboard/login');
		}


	}

	public function sync() {

		//exec("sh ".$_SERVER['DOCUMENT_ROOT']."/testing/sqlite/mysql_sqlite testing");
		//exec("sudo chmod -R 777 ".BASE_PATH);
		//rename(BASE_PATH."/testing.sqlite", FOOD_DB_PATH);
		//$this->drope_tables();
		//  echo 'yes';
		//send_request(FOOD_PUSH_UPDATE);
		$this->cmd_to_socket('sync');
		$this->message->set("Sync request send to server.", "success", TRUE);
		//$this->watchdog->save('Update', 'IRD', 'update_room',0,'Commit Changes');
		redirect("menuitem");

	}

	public function cmd_to_socket($cmd)
	{
		error_reporting(E_ALL);

		$address = "172.16.0.5";
		echo $port = 8009;

		/* Create a TCP/IP socket. */
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket === false) {
			echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
		} else {
			echo "socket successfully created.\n";
		}

		echo "Attempting to connect to '$address' on port '$port'...";
		$result = socket_connect($socket, $address, $port);
		if ($result === false) {
			echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
		} else {
			echo "successfully connected to $address.\n";
		}

		//$msg="Hello World\n";
		socket_write($socket, $cmd, strlen($cmd));

		echo "Closing socket...";
		socket_close($socket);

	}


	public function drope_tables() {
		 
		$tbls = "`departments`,`dashboard_feature`,`dashboard_module`,`dashboard_module_feature`,`dashboard_parameter`,`dashboard_role`,`dashboard_role_module`,`dashboard_ui_preference`,`dashboard_user`,`dashboard_user_attends_alert`,`dashboard_user_preference` ,`dashboard_user_role`,`guest_status`,`welcome_letter_opera`,`digivalet_services`, `digivalet_status`, `fav`, `fav_cat`, `floors`, `floor_types`,`notification_log`, `permission`, `perms`, `role`, `rooms`, `rooms_message`, `room_types`, `sdirectory`, `service_notification_emails`, `service_notification_sms_no`, `staff_info`, `system_vars`, `users`, `users_roles`, `watchdog`, `welcome_letters`, `welcome_letters_q`, `welcome_letter_types`";
		$str = str_replace('`','', $tbls);
		$tables = explode(',', $str);

		$pdo = new PDO('sqlite:'.FOOD_DB_PATH);
		foreach($tables as $table) {
			$pdo->exec("DROP TABLE ".trim($table));
		}
		$pdo->exec("VACUUM");

		exec("sudo mkdir ".$_SERVER['DOCUMENT_ROOT'].'/foodmenu');
		exec("sudo mkdir ".$_SERVER['DOCUMENT_ROOT'].'/foodmenu/files');
		exec("sudo cp -r ".BASE_PATH.'/files/* '.$_SERVER['DOCUMENT_ROOT'].'/foodmenu/files/');
		exec("sudo chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/foodmenu/');

		if(!copy(FOOD_DB_PATH, $_SERVER['DOCUMENT_ROOT'].'/foodmenu/'.FOOD_DB_NAME)) {
			$this->message->set("Database not copied.");
		}

		exec("sudo chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/foodmenu/');
	}


	public function delete($mid=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$name=$this->food_application_model->get_item_name($mid);
			$nam=$name->row();
			$name=$nam->menuitem_name;
			$this->watchdog->save('delete', 'IRD', 'menuitem_delete',$mid,$name);

			if($this->food_application_model->delete_menuitem($mid)) {

				$this->message->set("Menuitem successfully deleted.", "success", TRUE);
			}
			else {
				$this->message->set("Error in menuitem deletion.", "error", TRUE);
			}

			redirect('menuitem');
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function sort_save() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$ids = $this->input->post('ids');
			foreach($ids as $key=>$id) {
				echo "Key=> ".$key." Val=> ".$id." <br/>";
				// $this->fav_channels->update_tag($id, $key);
				$this->group_addon_model->update_position($key,$id );
			}

			echo 1;
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function sort_allowed_save() {
		$ids = $this->input->post('ids');
		foreach($ids as $key=>$id) {
			echo "Key=> ".$key." Val=> ".$id." <br/>";
			// $this->fav_channels->update_tag($id, $key);
			$this->group_allowed_addon_model->update_position($key,$id );
		}

		echo 1;
	}

	public function update_status($id,$val){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->food_application_model->update_stat($id,$val);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function new_update_status($id,$val){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$res=$this->food_application_model->get_val($id);
			$row = $res->row();

			$dis=$row->display_name;
			if($dis!=''){
				$this->food_application_model->new_update_stat($id,$val);
				echo 'yes';
			}
			else{
				echo 'no';
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function menulist($offset=0) {
		 
		$this->index($offset);
		 
	}

	function sort_list_save() {
		$ids = $this->input->post('ids');
		foreach($ids as $key=>$id) {
			echo "Key=> ".$key." Val=> ".$id." <br/>";
			// $this->fav_channels->update_tag($id, $key);
			// $this->group_addon_model->update_position($key,$id );
			$this->food_application_model->update_position($key, $id);
		}

		echo 1;
	}

	public function subcat($sub) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$sub_cat=$this->food_application_model->get_sub($sub);
			foreach($sub_cat->result() as $row) {
				$cat=$row->display_name;
			}
			$q = $this->food_application_model->get_menuitems_sub($sub);
			$this->data['row']=$q;
			//            $sub = str_replace(array("%20"), " ", $sub);
			$this->data['page_heading'] = $cat;
			$vars = $this->theme->theme_vars('pages/subcat_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function my_fun($data,$cat){

		$q = $this->food_application_model->set_menuitems($data,$cat);
		 
		$rows = array();
		$i = 0;
		foreach($q->result() as $row) {
			$i++;
			if($i%2==0) {
				$class = "even";
			}
			else {
				$class = "odd";
			}
			$c='0';
			if($row->is_active=='1'){$c='checked';}
			$active='<input type="checkbox" name="chk[]" id="'.$row->mid.'"  onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
			//                if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
			$rows[] =array(
			array('data'=>$i, 'attributes'=>array('style'=>'width:2%;text-align:center;')),
			array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
			array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
			array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%;text-align:left;')),
			array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
			array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:25%;text-align:left;')),
			array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
			array('data'=>anchor('menuitem/item/'.$row->mid, 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%;text-align:center;')),
                         'attributes'=>array('class'=>$class));
		}
		$output = "";
		if(count($rows) > 0 && count($rows) > 0) {

			// Parsing Rows
			$cnt = 0;
			foreach($rows as $row) {
				$rowCnt = count($row);
				if(isset($row['attributes']) && is_array($row['attributes'])) {
					$attribs = "";
					foreach($row['attributes'] as $key=>$val) {
						$attribs .= ' '.$key.'="'.$val.'"';
					}
					$output .= "<tr ".$attribs.">";
					$rowCnt -= 1;
				}
				else{
					$output .= "<tr>";
				}
				 
				for($i=0; $i<$rowCnt; $i++) {
					//print_r($row); die();
					$attribs = "";
					if(isset($row[$i]['attributes'])) {
						foreach($row[$i]['attributes'] as $key=>$val) {
							$attribs .= ' '.$key.'="'.$val.'"';
						}
						$output .= "<td '".$attribs."'>".$row[$i]['data']."</td>";
					}
					else {
						$output .= "<td>".$row[$i]['data']."</td>";
					}
				}

				$output .= "</tr>";
				 
				$cnt++;
			}
			 
		}
		else {
			echo '<tr><td style="width:2%;text-align:center">&nbsp;</td>
                 <td style="width:15%;text-align:center">&nbsp;</td>
                 <td style="width:15%;text-align:center">&nbsp;</td>
                 <td colspan="2" style="width:27%;text-align:center"><h4 style="margin-top:20px;fontsize:13px;" align="center">No Menuitem Found </h4></td>
                 <td style="width:25%;text-align:center">&nbsp;</td>
                 <td style="width:6%;text-align:center">&nbsp;</td>
                 <td style="width:10%;text-align:center">&nbsp;</td>
                 </tr>';
		}
		$script='<script type="text/javascript">$(".delete").click(function(){
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location;
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                })</script>';
		echo $output.$script;

	}
	 
	public function my_update_fun($data,$cat){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->food_application_model->set_update_menuitems($data,$cat);

			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$active='<input type="checkbox" name="chk[]" id="'.$row->mid.'"  onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
				//                if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
				$rows[] =array(
				array('data'=>$i, 'attributes'=>array('style'=>'width:2%;text-align:center;')),
				array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%;text-align:left;')),
				array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%;text-align:left;')),
				array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:25%;text-align:left;')),
				array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
				array('data'=>anchor('menuitem/item/'.$row->mid, 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%;text-align:center;')),
                         'attributes'=>array('class'=>$class));
			}
			$output = "";
			if(count($rows) > 0 && count($rows) > 0) {

				// Parsing Rows
				$cnt = 0;
				foreach($rows as $row) {
					$rowCnt = count($row);
					if(isset($row['attributes']) && is_array($row['attributes'])) {
						$attribs = "";
						foreach($row['attributes'] as $key=>$val) {
							$attribs .= ' '.$key.'="'.$val.'"';
						}
						$output .= "<tr ".$attribs.">";
						$rowCnt -= 1;
					}
					else{
						$output .= "<tr>";
					}
					 
					for($i=0; $i<$rowCnt; $i++) {
						//print_r($row); die();
						$attribs = "";
						if(isset($row[$i]['attributes'])) {
							foreach($row[$i]['attributes'] as $key=>$val) {
								$attribs .= ' '.$key.'="'.$val.'"';
							}
							$output .= "<td '".$attribs."'>".$row[$i]['data']."</td>";
						}
						else {
							$output .= "<td>".$row[$i]['data']."</td>";
						}
					}

					$output .= "</tr>";
					 
					$cnt++;
				}
				 
			}
			else {
				echo '<tr><td style="width:2%;text-align:center">&nbsp;</td>
                 <td style="width:15%;text-align:center">&nbsp;</td>
                 <td style="width:15%;text-align:center">&nbsp;</td>
                 <td colspan="2" style="width:28%;text-align:center"><span style="margin-top:20px;fontsize:13px;" align="center">No Menuitem Found </span></td>
                 <td style="width:25%;text-align:center">&nbsp;</td>
                 <td style="width:5%;text-align:center">&nbsp;</td>
                 <td style="width:10%;text-align:center">&nbsp;</td>
                 </tr>';
			}
			$script='<script type="text/javascript">$(".delete").click(function(){
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location;
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                })</script>';
			echo $output.$script;
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}



	public function sort() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$ids = $this->input->post("ids");
			$pos = explode(",", $ids);
			$i = 0;
			 
			foreach($pos as $qid) {
				$this->food_application_model->update_position($i, $qid);
				$i++;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function item($id=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$selected_addons = array();
			if($id == 0) {
				$this->data = $this->food_library->menuitem_form();
				$this->data['title'] = 'Add Menu item';
				$q = $this->common_model->get_records("food_addon_group_master");
				$groups = array(''=>'--Select Group--');
				foreach($q->result() as $row) {
					$groups[$row->gid] = $row->display_name;
				}

				$this->data['groups'] = form_dropdown('addon_groups[]', $groups, '', 'id="edit-group-addons"  style="width:150px;" class="select" onchange="group_onchange(this)"');
				$this->data['group_addons'] = form_multiselect('addons_master[]', array(), '', 'class="addons_master" style="width:150px;"');

			}
			else {
				$param = array();
				$q = $this->food_application_model->get_menuitems(0, 0, $id);
				foreach($q->result_array() as $row) {
					$param = $row;
				}

				$addon_id = array();
				$q = $this->food_application_model->get_menu_addons($id);
				foreach($q->result() as $row) {
					$addon_id[] = $row->group_number;
				}

				$param['addon_id'] = $addon_id;


				$allowed_addon = array();
				$q = $this->food_application_model->get_menu_allowed_addons($id);
				foreach($q->result() as $row) {
					$allowed_addon[] = $row->group_number;
				}

				$param['allowed_addon'] = $allowed_addon;
				$time=array();
				$q= $this->food_application_model->get_time($id);
				$i=0;

				foreach($q->result_array() as $row){
					// $time[$i]=$row;
					$time[$i]=$row;
					$i++;
				}
				$this->data['time'] =$time;
				//echo '<pre>'; print_r($time);die();
				$this->data = $this->food_library->menuitem_form($param, $time);
				//print_r($this->data);die();
				$this->data['title'] = 'Edit Menuitem';

				$q = $this->common_model->get_record_by_condition("food_menuitem_addons", "mid=".$id);
				foreach($q->result() as $row) {
					$selected_addons[$row->addon_id] = $row->addon_name;
				}
			}

			$q = $this->common_model->get_records("food_addon_group_master");
			$groups = array(''=>'--Select Group--');
			 
			foreach($q->result() as $row) {
				$groups[$row->gid] = $row->display_name;
			}





			$this->data['selected_addons'] = $selected_addons;
			 
			$addons = array();
			if($id > 0) {
				$q = $this->food_application_model->get_menuitem_groups($id);
				foreach($q->result() as $row) {
					$addons[$row->gid] = $row->display_name;
				}
			}

			$menuitem_addon_row = array();
			foreach($addons as $gid=>$gval) {
				$q = $this->common_model->get_record_by_condition("food_menuitem_addons", "mid=".$id." AND gid=".$gid);
				$maddons = array();
				foreach($q->result() as $row) {
					$maddons[] = $row->addon_id;
				}

				$groups_addon = array();
				$q2 = $this->common_model->get_record_by_condition('food_addons_master', 'gid='.$gid);
				foreach($q2->result() as $row) {
					$groups_addon[$row->addon_id] = $row->display_name;
				}

				$group_field = form_dropdown('addon_groups[]', $groups, $gid, 'onchange="group_onchange(this)" class="addon_group" style="width:150px;font-size:14px"');
				$addon_field = form_multiselect('addons_master[]', $groups_addon, $maddons, 'class="addons_master" style="width:150px;"');
				$menuitem_addon_row[] = array('groups'=>$group_field, 'addons'=>$addon_field);

			}

			$this->data['menuitem_addon_row'] = $menuitem_addon_row;

			//echo "<pre>"; print_r($menuitem_addon_row); die();

			$q = $this->common_model->get_records('food_addon_group_master', 'position');

			foreach($q->result() as $row) {
				if(!in_array($row->display_name, $addons)) {
					$addons[$row->gid] = $row->display_name;
				}
			}

			$this->data['addons_master'] = $addons;

			$q = $this->common_model->get_records("food_addon_group_master");
			$groups = array(''=>'--Select Group--');
			foreach($q->result() as $row) {
				$groups[$row->gid] = $row->display_name;
			}

			$this->data['groups'] = form_dropdown('addon_groups[]', $groups, '', 'id="edit-group-addons" class="addon_group " style="width:150px;font-size:14px" onchange="group_onchange(this)"');
			$this->data['group_addons'] = form_multiselect('addons_master[]', array(), '', 'class="addons_master" style="width:150px;font-size:14px"');

			// echo '<pre>'; print_r($this->data);die();
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_menuitem_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}


	}

	public function get_addon_row() {

		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$q = $this->common_model->get_records("food_addon_group_master");
			$groups = array(''=>'--Select Group--');
			foreach($q->result() as $row) {
				$groups[$row->gid] = $row->display_name;
			}

			$groups = form_dropdown('addon_groups[]', $groups, '', 'id="edit-group-addons" class="addon_group " style="width:150px;font-size:14px" onchange="group_onchange(this)"');
			$group_addons = form_multiselect('addons_master[]', array(), '', 'class="addons_master" style="width:150px;font-size:14px"');

			echo '<div class="ui-state-defaul"><table><tr> <td><div style="padding-top:4px" class="handle"><img class="tabledrag-handle" style="cursor: move;" src="'.get_assets_path('image').'backend_images/arrow1.png" title="Drag to re-order" href="#"></div></td> <td style="padding-left:10px">'.$groups.'</td> <td style="padding-left:10px">'.$group_addons.'</td> <td align="left" style="padding-left:10px;padding-top:4px"><a title="Click here to remove addon" style="color:red;font-weight:bold;font-size: 14px" href="#" onclick="$(this).parent().parent().remove(); return false;">X</a></td> </tr></table></div>';
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function get_group_addons() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$gid = $this->input->post("gid");
			$addons = array();
			if($gid > 0) {
				$q = $this->common_model->get_record_by_condition("food_addons_master", "gid=".$gid);
				foreach($q->result() as $row) {
					$addons[$row->addon_id] = $row->display_name;
				}
			}
			echo form_multiselect('addons_master[]', $addons, '', 'class="addons_master" style="width:150px;"');
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}

	public function sub($id) {
		 
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$mid = $this->input->post("mid");
			$maddons = array();
			if($mid > 0) {
				$q = $this->common_model->get_record_by_condition('food_menuitem_addons', 'mid='.$mid);
				foreach($q->result() as $row) {
					$maddons[$row->addon_id] = $row->addon_name;
				}
			}


			$q = $this->common_model->get_records_by_field('food_addons_master','gid',$id);
			$sub_addons = array();
			$i=0;
			echo '<table>';
			foreach($q->result() as $row) {

				if($i%4 == 0) {
					echo '</td></tr><tr><td width="20%">';
				}
				else {
					echo '</td><td width="20%">';
				}

				if(array_key_exists($row->addon_id, $maddons)) {
					$checked = "checked='checked'";
				}
				else {
					$checked = "";
				}

				echo  '<div style="font-size:10px;" ><input type="checkbox" name="addons_master[]" value="'.$row->addon_id.'" '.$checked.' />'.$row->display_name.'</div>';

				$i++;
				$sub_addons[$row->addon_id] = $row->display_name;
			}
			echo '</table>';
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function selectdata() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$name = $this->input->post('name');
			$val = $this->input->post('val');
			$selected = $this->input->post('selected');
			$select = array('--Select--');
			if($name == "section") {
				if($val != "")
				$select = $this->food_library->get_table_data('food_maincategory', 'maincategory_id', 'name', 'section_id='.$val);
				else
				$select = $this->food_library->get_table_data('food_maincategory', 'maincategory_id', 'name');

				 
				$field = form_dropdown("maincategory_id", $select, $selected, 'id="edit-cid" class="select" onchange="get_sub_cat(this.value)" style="width:243px" ');
			}

			if($name == "cat") {
				if($val != "")
				$select = $this->food_library->get_table_data('food_subcategory', 'sub_cat_id', 'name', 'maincategory_id='.$val);
				else
				$select = $this->food_library->get_table_data('food_subcategory', 'maincategory_id', 'name');
				 
				$field = form_dropdown("sub_cat_id", $select, $selected, 'id="edit-subcat" style="width:243px" class="select" ');
			}
			 
			echo $field;
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	public function search_in($cid="") {

		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$result = "";
			$q = $_GET['q'];
			//$cid = $_GET['cid'];
			if($q != "") {

				$rs = $this->common_model->get_record_by_condition('food_menuitem_details', ' display_name LIKE "'.$q.'%"');


				if($rs->num_rows() > 0) {
					foreach($rs->result() as $row) {
						$val = $row->display_name;
						//  $result .= "<div><a href='".base_url()."menuitem/item/$row->mid'>$val</a></div>\n";
						$result .= "<div><a href='".base_url()."menuitem/item/$row->mid'>$val</a></div>|$row->mid\n";
					}
				}
			}
			echo $result;
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}
	 


	public function search() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$search_term = $this->input->post("search_term");
			$search_val = $this->input->post("search_val");
			$rs = $this->food_application_model->get_menuitems_search($search_term, $search_val);
			$this->index(0, $rs);
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	public function removefood_image() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$pos = $this->input->post('pos');
			$mid = $this->input->post('mid');
			if($pos > -1 && $mid > 0) {
				$this->food_application_model->delete_images(1, 'menuitem', $mid, $pos);
				$this->food_application_model->reorder_food_image('menuitem', $mid);
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}


	public function show_groups($mid=0) {

		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$mgroups = array();
			$q = $this->common_model->get_record_by_condition('food_menuitem_addons', 'mid='.$mid.' GROUP BY gid');
			if($q->num_rows() > 0) {
				foreach($q->result() as $row) {
					$mgroups[] = $row->gid;
				}

				echo implode(',', $mgroups);
			}
			else {
				echo 0;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function sort_group() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$ids = $this->input->post("ids");
			$pos = explode(",", $ids);
			$i = 0;

			foreach($pos as $qid) {
				$this->group_addon_model->update_position($i, $qid);
				$i++;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function sort_addons() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$ids = $this->input->post("ids");
			$pos = explode(",", $ids);
			$i = 0;

			foreach($pos as $qid) {
				$this->group_addon_model->update_addon_position($i, $qid);
				$i++;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	public function get_filter_subcat() {

		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$cat_ids = $this->input->post("cat_ids");
			$subcat_ids = $this->input->post("subcat_ids");

			//echo "Cat_id : ".$cat_ids;
			//echo "SubCatID : ".$subcat_ids;

			//print_r($_POST);
			//die();

			if($subcat_ids != "") {
				$subcat_arr = explode(",", $subcat_ids);
			}
			else {
				$subcat_arr = 0;
			}

			if($cat_ids == 0) {
				$subcat_opt = $this->subcat;
				$subcat_opt["0"] = "All";
			}
			else {
				$q = $this->common_model->get_record_by_condition('food_subcategory', 'maincategory_id IN('.$cat_ids.')', 'display_name');
				$subcat_opt = array();
				foreach($q->result() as $row) {
					$subcat_opt[$row->sub_cat_id] = $row->display_name;
				}
				$subcat_opt["0"] = "All";
			}

			echo form_multiselect('filter_subcat', $subcat_opt, $subcat_arr, "id='filter_subcat' style='width:100px'");
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function get_filter_update_subcat() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$cat_ids = $this->input->post("cat_ids");
			$subcat_ids = $this->input->post("subcat_ids");
			if($subcat_ids != "") {
				$subcat_arr = explode(",", $subcat_ids);
			}
			else {
				$subcat_arr = 0;
			}

			if($cat_ids == 0) {
				$subcat_opt = $this->subcat;
				$subcat_opt["0"] = "All";
			}
			else {
				$q = $this->common_model->get_record_by_condition('food_subcategory', 'maincategory_id IN('.$cat_ids.')', 'display_name');
				$subcat_opt = array();
				foreach($q->result() as $row) {
					$subcat_opt[$row->sub_cat_id] = $row->display_name;
				}
				$subcat_opt["0"] = "All";
			}

			echo form_multiselect('filter_update_subcat', $subcat_opt, $subcat_arr, "id='filter_update_subcat' style='width:100px'");
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}







	public function get_filter_menuitem() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$cat = $this->input->post("cat_ids");
			$subcat = $this->input->post("subcat_ids");
			$q = $this->food_application_model->get_filterd_menuitems($cat, $subcat);
			$i=0;
			$rows = array();
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$active='<input type="checkbox" name="chk[]"  id="'.$row->mid.'" onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
				//              if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
				$rows[] =array(
				array('data'=>$i, 'attributes'=>array('style'=>'width:2%')),
				array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%')),
				array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:23%')),
				array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
				array('data'=>anchor('menuitem/item/'.$row->mid, 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%')),
                         'attributes'=>array('class'=>$class));
			}

			if(count($rows) > 0) {
				echo $this->theme->generate_list(array(), $rows);
			}
			else {
				echo '<tr><td colspan="8" align="center">Menuitem Not  Found.</td></tr>';
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function get_filter_update_menuitem() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$cat = $this->input->post("cat_ids");
			$subcat = $this->input->post("subcat_ids");
			$q = $this->food_application_model->get_filterd_update_menuitems($cat, $subcat);
			$i=0;
			$rows = array();
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				$c='0';
				if($row->is_active=='1'){$c='checked';}
				$active='<input type="checkbox" name="chk[]" id="'.$row->mid.'"  onclick="check('.$row->mid.','.$c.');"  '.$c. '  />';
				//              if($row->is_active == 0) {$active = "Deactive";}else{$active = "Active";}
				$rows[] =array(
				array('data'=>$i, 'attributes'=>array('style'=>'width:2%')),
				array('data'=>$row->name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->display_name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->cat_name, 'attributes'=>array('style'=>'width:13%')),
				array('data'=>$row->subcat_name, 'attributes'=>array('style'=>'width:15%')),
				array('data'=>$row->synopsis, 'attributes'=>array('style'=>'width:23%')),
				array('data'=>$active, 'attributes'=>array('style'=>'width:7%;text-align:center;')),
				array('data'=>anchor('menuitem/item/'.$row->mid, 'Edit')." | ".anchor('menuitem/delete/'.$row->mid, 'Delete', 'class="delete"'), 'attributes'=>array('style'=>'width:10%')),
                         'attributes'=>array('class'=>$class));
			}

			if(count($rows) > 0) {
				echo $this->theme->generate_list(array(), $rows);
			}
			else {
				echo '<tr><td colspan="8" align="center">Menuitem Not Found.</td></tr>';
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}


	 



	public function submit() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$form_id = $this->input->post('form_id');
			$rvc_id = $this->input->post('rvc_id');
			$descriptive_name = $this->input->post('descriptive_name');
			$synopsis = $this->input->post('synopsis');



			$name = $this->input->post('name');

			$position = $this->input->post('position');
			$delivery_time = $this->input->post('delivery_time');

			$number = $this->input->post('number');
			$is_breakfast = $this->input->post('has_breakfast');
			$is_special=$this->input->post('special');
			$is_porch=$this->input->post('porch');
			if($is_porch){$is_porch=1;}
			if($is_special){$is_special=1;}
			$is_veg = $this->input->post('is_vag');

			$is_beverages = $this->input->post('is_beverages');
			//echo $is_beverages;die();
			if($is_beverages==1)
			$is_beverages=0;
			else
			$is_beverages=1;

			$spicy_level = $this->input->post('spicy_level');
			$size_price = $this->input->post('size_price');
			$section_id = $this->input->post('section_id');
			$maincat_id = $this->input->post('maincategory_id');
			$subcat_id = $this->input->post('sub_cat_id');
			$active = $this->input->post('active');
			$has_addon = $this->input->post('has_addons');
			$addon_master = $this->input->post('addons_master');
			$groupids = $this->input->post('groupids');
			$addons_group = $this->input->post('addons_group');
			$addon_groups = $this->input->post('addon_groups');

			$addon_groups = array_unique($addon_groups);
			$tmpaddon_master = array_unique($addon_master);
			$addon_master = array();
			foreach($tmpaddon_master as $addon) {
				if($addon != "on" && $addon != "0") {
					$addon_master[] = $addon;
				}
			}

			//echo "<pre>"; print_r(array($addon_master, $addon_groups)); die();

			//echo "<pre>"; print_r($_FILES); die();
			switch ($form_id) {
				case "add_menuitem":
					// echo "<pre>"; print_r($_POST['from1']);die();

					$time1 = $this->input->post('time');
					$from1 = $this->input->post('from1[]');
					$to1 = $this->input->post('to1[]');
					$size = $this->input->post('size');
					$price = $this->input->post('price');
					 
					if(!$this->food_library->menuitem_validate($_POST)) {
						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');
						$has_addon = $this->input->post('has_addons');
						if($has_addon > 0) {
							//$required_addon = $this->input->post('required_addon');
							//$addons = $this->input->post('addons');
							$required_addon = "";
							$addons = array();
						}
						else {
							$required_addon = "";
							$addons = array();
						}
						$from1 = $this->input->post('from1');
						$to1 = $this->input->post('to1');
						//echo '<pre>';echo print_r($from1); echo 't';die();

						if(isset($videos['video']['file_name'])) {
							$details_param = array('from1'=>$from1,'to1'=>$to1,'descriptive_name'=>$descriptive_name['en'], 'section_id'=>$section_id, 'maincategory_id'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'synopsis'=>$synopsis['en'], 'name'=>$name, 'number'=>$number, 'is_breakfast'=>$is_breakfast, 'is_veg'=>$is_veg,'show_veg_nonveg'=>$is_beverages,'is_special'=>$is_special,'is_porch'=>$is_porch,'spicy_level'=>$spicy_level, 'video'=>$videos['video']['file_name'], 'is_active'=>$active, 'required_addon'=>$required_addon,'position'=>$position,'delivery_time'=>$delivery_time);
						}
						else {
							$details_param = array('from1'=>$from1,'to1'=>$to1,'descriptive_name'=>$descriptive_name['en'], 'section_id'=>$section_id, 'maincategory_id'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'synopsis'=>$synopsis['en'], 'name'=>$name, 'number'=>$number, 'is_breakfast'=>$is_breakfast, 'is_veg'=>$is_veg,'show_veg_nonveg'=>$is_beverages,'is_special'=>$is_special,'is_porch'=>$is_porch,'spicy_level'=>$spicy_level, 'is_active'=>$active, 'required_addon'=>$required_addon,'position'=>$position,'delivery_time'=>$delivery_time);
						}

						$primary_param = array('rvc_number'=>$rvc_id, 'menuitem_number'=>$number, 'name'=>$name, 'has_addons'=>$has_addon, 'sid'=>$section_id, 'cid'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'is_changed'=>'0', 'is_active'=>$active);
						// $param = array('primary'=>$primary_param, 'details'=>$details_param);
						$param = array('primary'=>$primary_param, 'details'=>$details_param,'time'=>$time1);

						$mid = $this->food_application_model->save_menuitem($param);
						//$this->food_application_model->insert_price($size,$price,$mid);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$descriptive_name, 'description'=>$synopsis), 'menuitem', $mid);
						if($mid > 0) {
							$this->food_application_model->save_images($images, 'menuitem', $mid);
							if($this->input->post('has_addons') == 1){
								$this->food_application_model->save_menuitem_addons($mid, $this->input->post('addons'));
								$this->food_application_model->save_allowed_addons($mid, $this->input->post('allowed_addons'));
							}

							// Deleting and saving menuitem price
							$this->food_application_model->delete_menuitem_price($mid);
							$price_row = $this->food_library->prepare_price_details($size_price, $mid);
							foreach($price_row as $r) {
								$this->food_application_model->add_menuitem_price($r);
							}

							//Deleting and saving menuitem locale
							$this->food_application_model->delete_food_locale('menuitem', $mid);
							foreach($locale as $row) {
								$this->food_application_model->save_food_locale($row);
							}

							$addon_id_str = implode(',', $addon_master);
							if($addon_id_str != "") {
								$q = $this->common_model->get_record_by_condition('food_addons_master', 'addon_id IN('.$addon_id_str.')');
								foreach($q->result() as $row) {
									$param = array('addon_id'=>$row->addon_id, 'mid'=>$mid, 'gid'=>$row->gid, 'addon_code'=>$row->addon_code, 'addon_name'=>$row->display_name, 'addon_size'=>$row->size, 'addon_prise'=>$row->price);
									$this->food_addon_model->save_menuitem_addons($param);
								}
							}

							$group_arr = explode(',', $groupids);
							$this->food_application_model->save_menuitem_groups($addon_groups, $mid, $addons_group);
							$this->watchdog->save('add', 'IRD', 'menuitem_add', 0, $descriptive_name['en']);
							$this->message->set('Menuitem successfully added.', 'success', TRUE);
							redirect('menuitem/item');

						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in adding menuitem.', 'error', TRUE);
							redirect('menuitem/item');
						}
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('menuitem/item');
					}
					break;
					 
				case "edit_menuitem":
					//echo "<pre>"; print_r($_POST);die();
					//                    $from_time = $this->input->post('from_time');
					//                  $to_time = $this->input->post('to_time');
					$up=$this->input->post('up');
					$time1 = $this->input->post('time');
					$from1 = $this->input->post('from1');
					$to1 = $this->input->post('to1');
					$size= $this->input->post('size');
					$price=$this->input->post('price');

					$id = $this->input->post('mid');
					if(!$this->food_library->menuitem_validate($_POST)) {
						$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
						$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');

						if($this->input->post('has_addons') > 0) {
							$required_addon = $this->input->post('required_addon');
							$addons = $this->input->post('addons');
						}
						else {
							$required_addon = "";
							$addons = array();
						}

						if(isset($videos['video']['file_name'])) {
							$details_param = array('display_name'=>$descriptive_name['en'], 'section_id'=>$section_id, 'maincategory_id'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'synopsis'=>$synopsis['en'], 'menuitem_name'=>$name, 'menuitem_number'=>$number, 'is_breakfast'=>$is_breakfast,'is_special'=>$is_special,'is_porch'=>$is_porch, 'is_veg'=>$is_veg,'show_veg_nonveg'=>$is_beverages, 'spicy_level'=>$spicy_level, 'video'=>$videos['video']['file_name'], 'is_active'=>$active, 'required_addon'=>$required_addon,'position'=>$position,'delivery_time'=>$delivery_time);
							$time=array('from_time'=>$from1,'to_time'=>$to1,);

						}
						else {
							$details_param = array('display_name'=>$descriptive_name['en'], 'section_id'=>$section_id, 'maincategory_id'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'synopsis'=>$synopsis['en'], 'menuitem_name'=>$name, 'menuitem_number'=>$number, 'is_breakfast'=>$is_breakfast, 'is_special'=>$is_special,'is_porch'=>$is_porch, 'is_veg'=>$is_veg,'show_veg_nonveg'=>$is_beverages, 'spicy_level'=>$spicy_level, 'is_active'=>$active, 'required_addon'=>$required_addon,'position'=>$position,'delivery_time'=>$delivery_time);
							$time=array('from_time'=>$from1,'to_time'=>$to1,'time1'=>$time1);

						}

						$primary_param = array('rvc_number'=>$rvc_id, 'name'=>$this->input->post('name'), 'has_addons'=>$has_addon, 'section_id'=>$section_id, 'maincategory_id'=>$maincat_id, 'sub_cat_id'=>$subcat_id, 'is_active'=>$active,'is_changed'=>'0');
						$param = array('primary'=>$primary_param, 'details'=>$details_param,'time'=>$time);

						//echo "<pre>"; print_r(array($primary_param, $details_param)); die();
						//Price update new()
						//$this->food_application_model->update_price($size,$price,$id);
						$mid = $this->food_application_model->save_menuitem($param, $id);
						$locale = $this->food_library->prepare_locale_row(array('name'=>$descriptive_name, 'description'=>$synopsis), 'menuitem', $mid);
						if($mid > 0) {
							$this->food_application_model->save_images($images, 'menuitem', $mid);
							$has_addon = $this->input->post('has_addons');
							if($has_addon == 1){
								//die("reached..!");
								$addons = $this->input->post('addons');
								$allowed_addons = $this->input->post('allowed_addons');
								$this->food_application_model->save_menuitem_addons($mid, $addons);
								$this->food_application_model->save_allowed_addons($mid, $allowed_addons);
							}

							// Deleting and saving menuitem price
							$this->food_application_model->delete_menuitem_price($mid);
							$price_row = $this->food_library->prepare_price_details($size_price, $mid);
							foreach($price_row as $r) {
								$this->food_application_model->add_menuitem_price($r);
							}

							$this->food_application_model->delete_food_locale('menuitem', $mid);
							foreach($locale as $row) {
								$this->food_application_model->save_food_locale($row);
							}

							$addon_id_str = implode(',', $addon_master);
							if($addon_id_str != "") {
								$this->food_application_model->delete_menuitem_addons($mid);
								$q = $this->common_model->get_record_by_condition('food_addons_master', 'addon_id IN('.$addon_id_str.')');
								foreach($q->result() as $row) {
									$param = array('addon_id'=>$row->addon_id, 'mid'=>$mid, 'gid'=>$row->gid, 'addon_code'=>$row->addon_code, 'addon_name'=>$row->display_name, 'addon_size'=>$row->size, 'addon_prise'=>$row->price);
									$this->food_addon_model->save_menuitem_addons($param);
								}
							}

							$group_arr = explode(',', $groupids);
							$this->food_application_model->save_menuitem_groups($addon_groups, $mid, $addons_group);
							$this->watchdog->save('edit', 'IRD', 'menuitem_edit', $mid, $descriptive_name['en']);

							$this->message->set('Menuitem successfully updated.', 'success', TRUE);
							if($up!=''){
								redirect('menuitem/update');
							}
							else{ redirect('menuitem');}
						}
						else {
							$this->get_last_post->set($_POST, '', TRUE);
							$this->message->set('Error in menuitem updation.', 'error', TRUE);
							redirect('menuitem/item/'.$id);
						}
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						redirect('menuitem/item/'.$id);
					}

					break;
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}
}
?>
