<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of get_letter
 *
 * @author pbsl
 */
class get_letter extends CI_Controller {
	var $data;
	var $room_no;
	public function  __construct() {
		parent::__construct();
	}

	public function get($room_no = 0) {
		if($room_no > 0) {
			$html = "";
			$created = date('Y-m-d');
			$q = $this->common_model->get_record_by_condition("rooms_message", "room_no = $room_no AND 	created = '".$created."'");
			if($q->num_rows() > 0) {
				$rs = $q->result();
				$row = $rs[0];
				$code = $row->letter_code;
				$html = $row->message;
				$guest_name = $row->guest_name;
				if($guest_name == "") {
					$this->load->model("pmsi_model");
					$guest_name = $this->pmsi_model->get_guest_name($room_no);
				}

				//$html = $this->get_letter_content($code);
				if($html) {
					$html = str_replace('%guest', $guest_name, $html);
					echo $this->response($html);
				}
				else {
					echo "Letter Not found.";
				}
			}
			else {
				$q = $this->common_model->get_record_by_condition("welcome_letters", "is_default = 1");
				if($q->num_rows() > 0) {
					$rs = $q->result();
					$row = $rs[0];
					$html = $row->content;

					$val=$this->common_model->get_val();
					$val=$val->row();
					$val=$val->from_opera;

					if($val=='1'){
						$guest_name = "";
						$this->load->model("pmsi_model");
						$guest_name = $this->pmsi_model->get_guest_name($room_no);
						if($guest_name != "") {
							 
						}
						else{
							$guest_name='Guest';
						}
						$html = str_replace('%guest', $guest_name, $html);
					}
					else{
						$guest_name='Guest';
						$html = str_replace('%guest', $guest_name, $html);
					}

					echo $this->response($html);
				}
				else {
					$html="Letter Not Found";
					echo $this->response($html);
					// echo "Letter Not found.";
				}
			}
		}
		else {
			echo "Room No. not defined.";
		}
	}


	private function get_letter_content($code="") {
		$q = $this->common_model->get_record_by_condition("welcome_letters", "title='".$code."'");
		if($q->num_rows() > 0) {
			$row = $q->result();
			return $content = $row[0]->content;
		}
		else {
			return false;
		}

	}

	private function response($content="") {
		$this->data['html'] = $content;
		return $this->load->view("pages/welcome_letter/letter_template", $this->data, TRUE);
	}

}
?>
