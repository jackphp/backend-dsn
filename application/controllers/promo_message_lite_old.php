<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
//echo "I am in lite version";die();
class promo_message_lite  extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("promo_msg_model_lite");
		// $this->load->model("common_model");
		$this->load->library('autocontact');
		$this->data['main_title'] = 'Promotional Message';
		//$perm = array('update shopping pdf');
		$this->load->library('pagination');
		//$this->user->set_access_permission($perm);
	}


	public function index(){
		$this->data['page_heading']='Promotional Message';
		$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "draft=1 or sent=0");
		$result=$q->result();
		$this->data['result']=$result;
		$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	}

	public function edit($mid=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($mid > 0){
				$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "promo_msg_id=".$mid);
				foreach($q->result() as $row) {
					// print_r($row);die();
					$title= $row->title;
					$extra_info=$row->extrainformation;
					$msg_body = $row->msg_body;
					$promo_msg_img = $row->promo_msg_img;
					$promo_page_url = $row->promo_page_url;
					$end_date = $row->end_date;
					$start_date = $row->start_date;
					$mid=$mid;
					 
				}

				if($end_date=='0000-00-00 00:00:00'){
					$end_date ='';
					 
				}

				if($start_date=='0000-00-00 00:00:00'){
					$start_date ='';
					$checked ='';
				}
				else{
					$checked='checked';
				}

				$url = $promo_msg_img;
				$file = substr($url,strrpos($url,'/'),strlen($url));
				$file=substr($file,1);
			}
			else{
				$checked='';
				$title='';
				$extra_info='';
				$msg_body='';
				$promo_msg_img='';
				$promo_page_url='';
				$end_date='';
				$start_date='';
				$file='';
				$mid='';
			}

			 
			//echo $file;die();
			 
			 
			$this->data['page_heading']='Promotional Message';

			//Upload Start
			$this->data['form_open'] = form_open_multipart('promo_message_lite/submit','name="first" id="first"');
			$this->data['form_close'] = form_close();
			$this->data['file'] = form_upload("file",'','size=18 id="filen" onchange="fileSelected();" class="file" style="width:312px;position: relative;z-index: 2;text-align: right;" ');
			$this->data['title']=$title;
			$this->data['extra_info']=$extra_info;
			$this->data['promo_page_url']=$promo_page_url;
			$this->data['msg_body']= $msg_body;
			$this->data['promo_msg_img']= $promo_msg_img;
			$this->data['start_date']= $start_date;
			$this->data['end_date']= $end_date;
			$this->data['image'] = $file;
			$this->data['checked']=$checked;
			$this->data['mid']=$mid;
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			//$this->data['page_heading'] = 'Video Song SubCategory';
			//$this->data['cat_id']=$id;
			//$q = $this->video_song_category_model->get_subcategory($id);
			//$this->data['result']=$q->result();
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			$vars = $this->theme->theme_vars('pages/promotional_message_lite', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 

	 

	public function messages(){
		$this->data['page_heading']='Promotional Message';
		$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "draft=1 or sent=0");
		$result=$q->result();
		$this->data['result']=$result;
		$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		 
		 
	}

	public function send_message($mid) {
		$this->data['page_heading']='Promotional Message';
		$ch=$this->promo_msg_model_lite->get_time($mid);
		$time=$ch->row($ch);
		$time=$time->start_date;
		//echo 'time';echo $time;die();
		$q= $this->promo_msg_model_lite->send($mid,$time);//echo 'mid='; echo $mid;die();
		//$this->message->set("Message successfully saved.", "success", TRUE);
		 
		if($time=='0000-00-00 00:00:00'){
			$rs= $this->promo_msg_model_lite->get_all_guest(); //entry in promo_msg_to_deliver
			$result=$rs->result();
			//print_r($result);
			foreach($result as $row){ //print_r($row);echo $row->roomno;die();
				//room_no,guest_id,promo_msg_id,ip
				$param = array('room_no'=>$row->roomno,'guest_id'=>$row->guestid,'promo_msg_id'=>$mid, 'ip'=>$row->pc_ip);

				//guest_id,guest_name,promo_msg_id,token
				//  $param='';
				$last_token_id= $this->promo_msg_model_lite->promo_msg_to_deliver($param);
				$data = array('guest_id'=>$row->guestid,'guest_name'=>$row->guestname,'promo_msg_id'=>$mid, 'token'=>$last_token_id);


				$d= $this->promo_msg_model_lite->promo_msg_status($data);
				$this->promo_msg_model_lite->set_sent($mid);

			}
			$this->message->set("Message successfully sent.", "success", TRUE);
		}
		else{
			$this->message->set("Message Scheduled successfully.", "success", TRUE);
		}
		 
		$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		//redirect('promo_message_lite/messages');
		 
		 
	}

	public function rr_upload(){
		if ($_FILES["file"]["error"] > 0)
		{
			echo "Error: " . $_FILES["file"]["error"] . "<br>";
		}
		else
		{
			 
			$uploaddir = '/var/www/html/backend/images/promo_images/';
			$uploadfile = $uploaddir . basename($_FILES['file']['name']);

			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				echo "File is valid, and was successfully uploaded.\n";
			} else {
				echo "Possible file upload attack!\n";
			}


		}
	}


	public function save_send(){ //print_r($_POST);die();

		$login_id=$this->session->userdata("user_id");
		$value=$this->input->post('value');
		if($value=='1'){
			$sent='0';
			 
		}
		else{
			$sent='1';
		}
		$mid= $this->input->post('mid');
		$title= $this->input->post('title');
		$extra_info=$this->input->post('extra_info');
		$promo_page_url=$this->input->post('promo_page_url');
		$result1 = substr($promo_page_url, 0, 7);
		$result2 = substr($promo_page_url, 0, 8);
		if($result1== 'http://'){

		}
		else if($result2== 'https://'){
			 
		}
		else{
			$promo_page_url ='http://'.$promo_page_url;
		}
		$msg_body = $this->input->post('promotional_message');
		$start_date= $this->input->post('start_date');
		if($start_date==''){
			$date = date('Y-m-d H:i:s');
			$start_date=$date;
		}
		 
		$end_date =$this->input->post('end_date');
		$name= $this->input->post('fileName');


		if($mid !=''){
			if($_POST['fileName']==''){
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info,'promo_page_url'=>$promo_page_url, 'end_date'=>$end_date,'start_date'=>$start_date,'published'=>'1','draft'=>'0','sent'=>$sent);
			}
			else{

				$name=$_POST['fileName'];
				$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
				$promo_msg_img .=$name;
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date,'published'=>'1','draft'=>'0','sent'=>$sent);

			}
			$last_id = $this->promo_msg_model_lite->update_message($param,$mid);
		}
		else{

			$name= $this->input->post('fileName');
			$value=$this->input->post('value');
			if($value=='1'){
				$sent='0';
				 
			}
			else{
				$sent='1';
			}
			$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
			$promo_msg_img .=$name;
			$msg_body=$this->input->post('promotional_message');
			$title = $this->input->post('title');
			$extra_info = $this->input->post('extra_info');
			 
			 
			$author_id=$login_id;
			$end_date=$this->input->post('end_date');
			$start_date= $this->input->post('start_date');
			if($start_date==''){
				$date = date('Y-m-d H:i:s');
				$start_date=$date;
			}
			 
			 
			//echo $promo_msg_img;die();
			$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'published'=>'1','author_id'=>$author_id,'end_date'=>$end_date,'start_date'=>$start_date,'draft'=>'0','sent'=>$sent);
			$mid = $this->promo_msg_model_lite->send_message($param);
			 
		}
		if($value=='0'){
			$rs= $this->promo_msg_model_lite->get_all_guest(); //entry in promo_msg_to_deliver
			$result=$rs->result();
			//print_r($result);
			foreach($result as $row){ //print_r($row);echo $row->roomno;die();
				//room_no,guest_id,promo_msg_id,ip
				$param = array('room_no'=>$row->roomno,'guest_id'=>$row->guestid,'promo_msg_id'=>$mid, 'ip'=>$row->pc_ip);

				//guest_id,guest_name,promo_msg_id,token
				//  $param='';
				$last_token_id= $this->promo_msg_model_lite->promo_msg_to_deliver($param);
				$data = array('guest_id'=>$row->guestid,'guest_name'=>$row->guestname,'promo_msg_id'=>$mid, 'token'=>$last_token_id);


				$d= $this->promo_msg_model_lite->promo_msg_status($data);
				$this->promo_msg_model_lite->set_sent($mid);
			}
			$this->message->set("Message successfully sent.", "success", TRUE);
		}
		else{
			//Scheduled messages
			$this->message->set("Message Scheduled successfully.", "success", TRUE);
		}



		 
		// print_r($_POST); die();
	}

	public function submit() {  //echo '<pre>';print_r($_POST);die();
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$mid=$this->input->post('mid');
			$promo_page_url= $this->input->post('promo_page_url');
			$result1 = substr($promo_page_url, 0, 7);
			$result2 = substr($promo_page_url, 0, 8);
			if($result1== 'http://'){

			}
			else if($result2== 'https://'){
				 
			}
			else{
				$promo_page_url ='http://'.$promo_page_url;
			}
			if($mid !=''){
				$last_id=$mid;
				//echo '<>';print_r($_POST);die();
				$msg_body = $this->input->post('promotional_message');
				$start_date =$this->input->post('start_date');
				$end_date =$this->input->post('end_date');
				$title= $this->input->post('title');
				$extra_info = $this->input->post('extra_info');



				if($_FILES['file']['name']!=''){
					$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
					$promo_msg_img .=$_FILES['file']['name'];
					//echo 'yes';die();
					$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date);


					$last_id = $this->promo_msg_model_lite->update_img_message($param,$mid);
				}
				else{
					//echo 'no';die();
					$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date);
					$last_id = $this->promo_msg_model_lite->update_message($param,$mid);
					redirect('promo_message_lite');
					//redirect('promo_message_lite/index/'.$last_id);

				}

				 



			}
			else{
				$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
				 
				$title= $this->input->post('title');
				$extra_info =$this->input->post('extra_info');

				$msg_body=$this->input->post('promotional_message');
				$promo_msg_img .=$_FILES['file']['name'];
				$author_id=$login_id;
				$end_date=$this->input->post('end_date');
				$start_date=$this->input->post('start_date');
				 
				//echo $promo_msg_img;die();
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'published'=>'0','author_id'=>$author_id,'end_date'=>$end_date,'start_date'=>$start_date);
				$last_id = $this->promo_msg_model_lite->save_message($param);
			}
			$name= $this->input->post('pdf');
			//print_r($_POST);die();
			//  print_r($_FILES);die();
			if($_FILES['file']['name']!=''){
				// $_FILES['file']['name'];

				//  $_FILES['file']['name']=$name;
				 
				// $_FILES['file']['name']="shopping.pdf";
				$config['upload_path'] = PROMO_IMAGE_UPLOAD;
				$config['overwrite'] = true;
				 
				$config['allowed_types'] = '*';
				$config['is_image'] = 0;
				$config['max_size']  = '51200';
				//                        print_r($_FILES);die();

				$this->load->library("upload", $config);
				if(!$this->upload->do_upload('file')) {
					$err=$this->upload->display_errors();
					$this->message->set($err, "error", TRUE);
					//  echo $this->upload->display_errors();
				}
				else {
					exec("chmod -R 777 ".PROMO_IMAGE_UPLOAD);
					$d= $this->upload->data();

					$this->message->set("Message successfully saved.", "success", TRUE);
					//redirect('promo_message_lite/index/'.$last_id);
					redirect('promo_message_lite');

				}
				redirect('promo_message_lite');
			}
			 
			 
			 
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}



	function generate() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			send_request(SHOPPING_PUSH_UPDATE);
			$this->message->set("Update successfully sent to rooms.", "Success", TRUE);
			redirect('generate_pdf');
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 

}// End Of class
?>
