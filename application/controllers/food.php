<?php
/**
 * Description of food
 *
 * @author pbsl
 */
class food extends CI_Controller {
	var $data = array();
	var $room;
	var $from_date;
	var $to_date;

	public function  __construct() {
		parent::__construct();
		$this->load->helper('fusion_charts_helper');
		$this->load->helper('reports_helper');
		$this->load->model("food_model");
		$perm = array('access food reports');
		$this->user->set_access_permission($perm);
		$this->data['title'] = 'Rports - Food';
		$this->data['menu_section'] = 'food';
	}


	public function index() {
		if($this->user->is_user_access()) {
			$param = array();
			if($this->input->post('submit')) {
				if($this->input->post('room') != 'all') {
					$this->room = $param['room'] = $this->input->post('room');
				}

				if($this->input->post('from_date') != '' && $this->input->post('to_date')) {
					$this->from_date = $param['from_date'] = $this->input->post('from_date');
					$this->to_date = $param['to_date'] = $this->input->post('to_date');
				}
			}

			$rooms = array('all'=>'All Rooms');
			$rq = $this->common_model->get_records('rooms', 'room_name');
			foreach($rq->result() as $row) {
				$rooms[$row->room_id] = $row->room_name;
			}

			$q = $this->food_model->get_food_data($param);
			$foods = array();
			foreach($q->result() as $row) {
				$foods[] = array('date'=>$row->event_start_time, 'count'=>$row->cnt, 'name'=>$row->value);
			}

			$graph_str = food_report($foods);


			if($this->from_date != "" && $this->to_date != "") {
				if($this->room != "all" && $this->room != "") {
					$this->data['subheading'] = 'Food Report '.$this->from_date." to ".$this->to_date." and Room No. ".$rooms[$this->room];
				}
				else {
					$this->data['subheading'] = 'Food Report '.$this->from_date." to ".$this->to_date;
				}
			}
			else {
				if($this->room != "all" && $this->room != "") {
					$this->data['subheading'] = 'Food Report'.' room no '.$rooms[$this->room];
				}
				else {
					$this->data['subheading'] = 'Food Report';
				}

			}

			$this->data['form_open'] = form_open('food');
			$this->data['form_close'] = form_close();
			$this->data['graph_str'] = $graph_str;
			$this->data['from_date'] = form_input('from_date', '',  'id="edit-from-date"');
			$this->data['to_date'] = form_input('to_date', '', 'id="edit-to-date"');
			$this->data['rooms'] = form_dropdown('room', $rooms, '', 'id="edit-room"');
			$this->data['submit'] = form_submit('submit', 'View report', 'edit-form-submit');
			$vars = $this->theme->theme_vars('pages/food_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user_access_denied();
		}
	}

}
?>
