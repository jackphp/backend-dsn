<?php
/**
 * Description of user
 * after login user will
 * redirect here
 * @author pbsl
 */

class home extends CI_Controller {
	var $data;
	var $uid;

	public function  __construct() {
		parent::__construct();

		$this->load->helper('reports_helper');
		 
		$username = $this->user->get_user_info('name');
		$perm = array('view dashbord', 'change own password');
		$this->user->set_access_permission($perm);
		$this->data['title'] = 'Welcome '.ucfirst( $username );
		$this->uid = $this->user->get_user_info('uid');
	}

	public function index() {
		if($this->user->is_user_access()) {
			$digi_settings_path = get_assets_path('digivale_settings');
			$this->data['page_heading'] = 'Admin Home';
			$reports_var = array();


			$vars = $this->theme->theme_vars('pages/user_home', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			//$this->user->user_access_denied();
			$this->data['content'] = '<label>ITC Backend 1.0</label>';
			//$this->data['left'] = 'Left Navigation';
			$vars = $this->theme->theme_vars('pages/main', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
	}

	public function generate() {
		if($this->user->is_user_access()) {
			if(exec("sh ".BASE_PATH."/sqlite/mysql_sqlite newmymovie")) {
				$this->message->set("Database created successfully.", "success", TRUE);
			}
			else{
				$this->message->set("Error in database creation.", "error", TRUE);
			}

			redirect('home');
		}
		else {
			$this->user->user_access_denied();
		}
	}


	public function change_password($pass="", $close=0) {
		if($this->user->is_user_access('change own password')) {
			//$encod_pass = $this->user->encode_password($pass);
			$this->data['form_open'] = form_open('home/submit');
			$this->data['form_close'] = form_close();
			$this->data['old_password'] = form_input('old_password', $pass);
			$this->data['password'] = form_password('password');
			$this->data['confirm_password'] = form_password('confirm_password');
			$this->data['page_heading'] = "";
			$this->data['page_heading'] = 'Change Password';
			$this->data['submit'] = form_submit('submit', 'Change', 'id="edit_submit"');
			$this->data['cancel'] = form_button('button', 'Cancel', 'onclick="location.href=\''.base_url().'\'"');
			$this->data['colorbox_close'] = $close;
			$vars = $this->theme->theme_vars('pages/change_password', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->data['content'] = '<center><b>Login has timeout. '.anchor('login', 'Click here to login').'</b></center>';
			$vars = $this->theme->theme_vars('pages/main', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
	}


	public function submit() {
		//echo "<pre>";print_r($_POST); die("reached..!");
		if($this->user->is_user_access() > 0) {
			$old_password = $this->input->post("old_password");
			$pass = $this->input->post("password");
			$cpass = $this->input->post("confirm_password");

			if($pass == "") {
				$this->message->set("Please enter valid password.", "error", TRUE);
				redirect('home/change_password/'.$old_password);
			}

			$decoded_old_pass = $this->user->encode_password($old_password);
			$q = $this->common_model->get_record_by_condition("users", "pass='".$decoded_old_pass."' AND uid=".$this->uid);
			if($q->num_rows() > 0) {
				if($pass != $cpass) {
					$this->message->set("Password not matched.", "error", TRUE);
					redirect('home/change_password/'.$old_password);
				}
				else {
					$encoded_pass = $this->user->encode_password($pass);
					if($this->user_model->change_password($this->uid, $encoded_pass)){
						$this->message->set("Password successfully changed.", "success", TRUE);
						redirect('home/change_password/0/1');
					}
					else {
						$this->message->set("Some error in changing password.", "error", TRUE);
						redirect('home/change_password/'.$old_password);
					}
				}
			}
			else {
				$this->message->set("Old password not matched.", "error", TRUE);
				redirect('home/change_password/'.$old_password);
			}
		}
		else {
			$this->data['content'] = '<center><b>Login has timeout. '.anchor('login', 'Click here to login').'</b></center>';
			echo $this->load->view('pages/main', $this->data, TRUE);
		}
	}
}
?>