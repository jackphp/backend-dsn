<?php
/**
 * Description of user
 * after login user will
 * redirect here
 * @author pbsl
 */
class dashboard extends CI_Controller {
	var $data;

	public function  __construct() {
		parent::__construct();
		$this->load->helper('fusion_charts_helper');
		$this->load->helper('reports_helper');
		$this->load->model('ats_model');
		$this->load->model('room_management_model');
		$username = $this->user->get_user_info('name');
		$perm = array('view dashbord');
		$this->user->set_access_permission($perm);
		$this->data['title'] = 'Welcome '.ucfirst( $username );
	}

	public function index() {

		if($this->user->is_user_access()) {
			$digi_settings_path = get_assets_path('digivale_settings');
			if(!file_exists($digi_settings_path)) {
				//$this->message->set('DigiValet Settings sqlite file <em>'.$digi_settings_path.'</em> is missing, or file is not writeable.', 'error');
			}
			$this->data['page_heading'] = 'Admin Dashbord';
			$reports_var = array();

			$rentedq = $this->ats_model->room_count_status_wise('rented');
			$rented_row = $rentedq->result();

			$unrentedq = $this->ats_model->room_count_status_wise('rented', 0);
			$unrented_row = $unrentedq->result();

			$keytagq = $this->ats_model->room_count_status_wise('keytag');
			$keytag_row = $keytagq->result();

			$reports_var['rented'] = $rented_row[0]->ctr;
			$reports_var['keytag'] = $keytag_row[0]->ctr;
			$reports_var['unrented'] = $unrented_row[0]->ctr;

			$rented_report = rented_reports($reports_var);
			$this->data['rented_report'] = $rented_report;
			$this->data['block1_heading'] = "Quick Room's Status";
			$this->data['block2_heading'] = "DigiValet Status";
			$q = $this->ats_model->rooms_status_wise();
			$status_array = array();
			foreach($q->result() as $row) {
				$status_array[] = $row;
			}

			$dnd = $this->ats_model->room_count_status_wise('privacy_please');
			$dndr = $dnd->result();
			$this->data['dnd_count'] = $dndr[0]->ctr;

			$dndq = $this->ats_model->room_rows_status_wise('privacy_please');
			$dnd_str = '';
			foreach($dndq->result() as $row) {
				$dnd_str .= "Floor: ".$row->fname.", Room:".$row->room_name."<br/>";
			}
			$this->data['dnd_str'] = $dnd_str;

			$make_my_room = $this->ats_model->room_count_status_wise('make_my_room');
			$make_my_roomr = $make_my_room->result();
			$this->data['make_my_room'] = $make_my_roomr[0]->ctr;

			$makroomyq = $this->ats_model->room_rows_status_wise('make_my_room');
			$makeroom_str = '';
			foreach($makroomyq->result() as $row) {
				$makeroom_str .= "Floor: ".$row->fname.", Room:".$row->room_name."<br/>";
			}
			$this->data['makeroom_str'] = $makeroom_str;

			$maintnance = $this->ats_model->room_count_status_wise('maintenance');
			$maintnancer = $maintnance->result();
			$this->data['maintnance'] = $maintnancer[0]->ctr;

			$maintenanceq = $this->ats_model->room_rows_status_wise('maintenance');
			$maintenance_str = '';
			foreach($maintenanceq->result() as $row) {
				$maintenance_str .= "Floor: ".$row->fname.", Room:".$row->room_name."<br/>";
			}
			$this->data['maintenance_str'] = $maintenance_str;

			$digivalet_status = $this->ats_model->room_count_status_wise('digivalet_status', 0);
			$digivalet_statusr = $digivalet_status->result();
			$this->data['digivalet_status'] = $digivalet_statusr[0]->ctr;

			$digiq = $this->ats_model->room_rows_status_wise('digivalet_status', 0);
			$digi_str = '';
			foreach($digiq->result() as $row) {
				$digi_str .= "Floor: ".$row->fname.", Room:".$row->room_name."<br/>";
			}
			$this->data['digi_str'] = $digi_str;

			// Getting Controller and Ipod Status count
			$ipod_status = $this->ats_model->room_count_status_wise('ipod_status', 0);
			$ipod_statusr = $ipod_status->result();
			$this->data['ipod_status'] = $ipod_statusr[0]->ctr;

			$controller_status = $this->ats_model->room_count_status_wise('controller_status', 0);
			$controller_statusr = $controller_status->result();
			$this->data['controller_status'] = $controller_statusr[0]->ctr;

			$battry = $this->ats_model->room_count_status_wise('battry', 25);
			$battryr = $battry->result();
			$this->data['battry'] = $battryr[0]->ctr;
			$this->data['battryLevel'] = 25;

			$this->data['digiValet_report'] = digivalet_reports($this->data);

			//Getting Graph's XML for rented
			$this->data['quick_room_view'] = quick_room_status($status_array);

			$vars = $this->theme->theme_vars('pages/dashboard', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			//$this->user->user_access_denied();
			$this->data['content'] = 'DigiValet Adminstration 1.0';
			//$this->data['left'] = 'Left Navigation';
			$vars = $this->theme->theme_vars('pages/main', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
	}
}
?>
