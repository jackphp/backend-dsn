<?php
/************************************************************************************************************************************************
	 * 																																			*
	 * 											Project Name	: Digivalet Social Network														*									*
	 * 											Author			: Paras Sahu		 															*
	 * 											Creation Date	: 4 July 2013 - 22 July 2013													*										*
	 * 											Snippet Name	: DSN Controller																*
	 **************************************************************************************************************************************************/

Class Dsn extends CI_Controller
{
	/******************************************************** constructor START *****************************************************************/

	function __construct()
	{
		parent::__construct();
		$this -> load-> library('session');
		$this -> load-> library('Wall_Updates');
		$this -> load-> library('Time_stamp');
		$this -> load-> library('Tolink');
		$this -> load-> library('session');
		$this -> load -> helper(array('form', 'url'));
		$this -> load -> model('dsn_model');
		$isWebService 				= 		$this -> checkIsWebService();
		if($isWebService==TRUE)
		{
			$this -> session -> set_userdata(array( 'webService' => 1 ));
		}
		else
		{
			$this -> session -> set_userdata(array( 'uId' => 1 ));
		}
	}
	/******************************************************** constructor FINISH ****************************************************************/

	/******************************************************** index START ***********************************************************************/

	function index()
	{
		redirect('dsn/home');
	}

	/******************************************************** index FINISH ***********************************************************************/

	/********************************************************************************************************************************************
	 * 																																			*
	 * 							This is a initial page, user will be launch in digivalet social network by this 								*
	 * 							page. The page will show up the flow in two part. The first part i.e., left will show the 						*
	 * 							WALL CONTENT and the right part will show the jabber client users list											*
	 * 																																			*
	 *******************************************************************************************************************************************/

	/******************************************************** home START ***********************************************************************/

	function home()
	{
		$user_page 						= 		$this->uri->segment(2);
		$id 							= 		$this->session->userdata("uId");
		if ($id)
		{
			$this -> load -> model("dashboard_login");
			$result 					= 		$this->dashboard_login->getUserInfo();

			$data['user_info'] 			= 		$result;
			$data['page'] 				= 		$this->dashboard_login->getPageTitle();
			$data['left'] 				= 		$this->dashboard_login->user_profile();

			$wallObject					=		new Wall_Updates();

			$data['updatesarray']		=		$wallObject -> Updates($this -> session -> userdata('uId'));
			
			$data['wallObject']			=		$wallObject;

			$data['linkObject']			=		new Tolink();
			$data['timeStampObject']	=		new Time_stamp();



			$modules 					= 		$this->dashboard_login->getUserModules(1);

			if ($modules == "0")
			{
				redirect('digivalet_dashboard/login');
			}
			else
			{
				$this -> load -> view('dsn/__home', $data);
			}
		}
		else
		{
			$this->load->view('dashboard_pages/login.php');
			exit;
		}
	}
	/******************************************************** home FINISH ***********************************************************************/
	
	/******************************************************** ajax_load_content START ***********************************************************************/
	function ajax_load_content()
	{
		$user_page 						= 		$this->uri->segment(2);
		$id 							= 		$this->session->userdata("uId");
		if ($id)
		{
			$this -> load -> model("dashboard_login");
			$result 					= 		$this->dashboard_login->getUserInfo();

			$data['user_info'] 			= 		$result;
			$data['page'] 				= 		$this->dashboard_login->getPageTitle();
			$data['left'] 				= 		$this->dashboard_login->user_profile();

			$wallObject					=		new Wall_Updates();

			$data['updatesarray']		=		$wallObject -> Updates($this -> session -> userdata('uId'), $this -> uri -> segment(3), $this -> uri -> segment(4));

			$data['wallObject']			=		$wallObject;

			$data['linkObject']			=		new Tolink();
			$data['timeStampObject']	=		new Time_stamp();
			$data['ajaxLoadContent']	=		TRUE;
			
			$modules 					= 		$this->dashboard_login->getUserModules(1);

			if ($modules == "0")
			{
				redirect('digivalet_dashboard/login');
			}
			else
			{
				$this -> load -> view('dsn/load_messages', $data);
			}
		}
		else
		{
			$this->load->view('dashboard_pages/login.php');
		}
	}
	/******************************************************** ajax_load_content END ***********************************************************************/
	
	/*************************************************************START ceckIsWebService ********************************************************************/
	function checkIsWebService()
	{
		$uriOne = $this -> uri -> segment(3);
		
		if(trim($uriOne)!=FALSE && $uriOne==SECURITY_TOKEN)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	/*************************************************************END checkIsWebService ********************************************************************/
	/*************************************************************START checkWhichSessionExist ********************************************************************/
	function checkWhichSessionExist()
	{
		$webService 	= $this -> session -> userdata('webService');
		$uId 			= $this -> session -> userdata('uId');
		if($webService!=FALSE)
		{
			return 1;
		}
		elseif($uId!=FALSE)
		{
			return 2;
		}		
	}
	/*************************************************************END checkWhichSessionExist ********************************************************************/
	
	function message_ajax()					/*To post a message or to update a post*/
	{
		$wallObject							=		new Wall_Updates();
		$linkObject							=		new Tolink();
		$vars['timeStampObject']			=		new Time_stamp();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$userId 						= 		$this -> uri -> segment(4);
			$update 						= 		$this -> uri -> segment(5);
		}
		elseif($checkWhichSessionExist==2)
		{
			$userId							=		$this -> session -> userdata('uId');
			$update							=		$this -> input -> post('update');
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
		if($update)
		{
			$dataSet						=		$wallObject->Insert_Update($userId, $update);
			if($dataSet)
			{
				if($checkWhichSessionExist==2)				/*It is a browser call*/
				{
					$vars['msg_id']			=		$dataSet['msg_id'];
					$vars['message']		=		$linkObject->tolink_evaluate(htmlentities($dataSet['message']));
					$vars['uid']			=		$dataSet['uid_fk'];
					$vars['username']		=		$dataSet['username'];
					$vars['face']			=		$wallObject->Gravatar($userId);
					$vars['time']			=		$dataSet['created'];
					$vars['commentsarray']	=		$wallObject->Comments($dataSet['msg_id']);
					$vars['likessarray']	=		$wallObject->likes($dataSet['msg_id']);
					$this -> load -> view('dsn/message_ajax', $vars);
				}
				elseif($checkWhichSessionExist==1)										/*It is a web service call*/
				{
					echo json_encode(array(1, "Post Successfully Added"));
				}
				else 
				{
					echo json_encode(array(0, "Error Occurred"));
				}
			}
		}
	}
	function message_image_ajax($update, $imageName)
	{
		$wallObject						=		new Wall_Updates();
		$linkObject						=		new Tolink();
		$vars['timeStampObject']		=		new Time_stamp();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$userId 						= 		$this -> uri -> segment(4);
			$update 						= 		$this -> uri -> segment(5);
			$imageName 						= 		$this -> uri -> segment(6);
		}
		elseif($checkWhichSessionExist==2)
		{
			$update 						= 		$this -> uri -> segment(3);
			$imageName 						= 		$this -> uri -> segment(4);
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
		
		if($update)
		{
			$dataSet					=		$wallObject->Insert_Update_Image($this -> session -> userdata('uId'), rawurldecode($update), $imageName);
			if($dataSet)
			{
				if($checkWhichSessionExist==2)				/*It is a browser call*/
				{
					$vars['msg_id']			=		$dataSet['msg_id'];
					$vars['message']		=		$linkObject->tolink_evaluate(htmlentities($dataSet['message']));
					$vars['uid']			=		$dataSet['uid_fk'];
					$vars['username']		=		$dataSet['username'];
					$vars['face']			=		$wallObject->Gravatar($this -> session -> userdata('uId'));
					$vars['time']			=		$dataSet['created'];
					//$vars['time'] 			=	 	$timeStampObject -> time_stamp_evaluate($dataSet['created']);
					$vars['commentsarray']	=		$wallObject->Comments($dataSet['msg_id']);
					$vars['likessarray']	=		$wallObject->likes($dataSet['msg_id']);
					$vars['attached_image']	=		$imageName;
					$this -> load -> view('dsn/message_ajax', $vars);
				}
				elseif($checkWhichSessionExist==1)										/*It is a web service call*/
				{
					echo json_encode(array(1, "Post Successfully Added"));
				}
				else 
				{
					echo json_encode(array(0, "Error Occurred"));
				}
			}
		}
	}
	function delete_message_ajax()
	{
		$wallObject					=		new Wall_Updates();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$user_id 		= 	$this -> uri -> segment(4);
			$msg_id 		= 	$this -> uri -> segment(5);
		}
		elseif($checkWhichSessionExist==2)
		{
			$msg_id 		= 	$this -> input -> post('msg_id');
			$user_id 		= 	$this -> input -> post('user_id');
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
		if($msg_id)
		{
			$data					=		$wallObject -> Delete_Update($user_id, $msg_id);
			if($checkWhichSessionExist==2)				/*It is a browser call*/
			{
				echo $data;
			}
			elseif($checkWhichSessionExist==1)
			{
				echo json_encode(array(1, "Post Successfully Deleted"));
			}
			else 
			{
				echo json_encode(array(0, "Error Occurred"));
			}
		}
	}
	function delete_comment_ajax()
	{
		$wallObject					=		new Wall_Updates();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$user_id				=		$this -> uri -> segment(4);
			$com_id					=		$this -> uri -> segment(5);
		}
		elseif($checkWhichSessionExist==2)
		{
			$user_id				=		$this -> input -> post('user_id');
			$com_id					=		$this -> input -> post('com_id');
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
		if($com_id)
		{
			$data					=		$wallObject -> Delete_Comment($user_id, $com_id);
			if($checkWhichSessionExist==2)				/*It is a browser call*/
			{
				echo $data;
			}
			elseif($checkWhichSessionExist==1)
			{
				echo json_encode(array(1, "Comment Successfully Deleted"));
			}
			else 
			{
				echo json_encode(array(0, "Error Occurred"));
			}
		}
	}
	function comment_ajax()
	{
		$wallObject 				= 	new Wall_Updates();
		$vars['timeStampObject']	=	new Time_stamp();
		$linkObject					=	new Tolink();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$userId 				= 			$this -> uri -> segment(4);
			$msgId 					= 			$this -> uri -> segment(5);
			$comment 				= 			$this -> uri -> segment(6);
		}
		elseif($checkWhichSessionExist==2)
		{
			$userId 				= 			$this -> session -> userdata('uId');
			$msgId 					= 			$this -> input -> post('msg_id');
			$comment 				= 			$this -> input -> post('comment');
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
		
			$dataSet					=	$wallObject -> Insert_Comment($userId, $msgId, $comment, $_SERVER['REMOTE_ADDR']);
			if($dataSet)
			{
				if($checkWhichSessionExist==2)				/*It is a browser call*/
				{
					$vars['com_id']				=	$dataSet['com_id'];
					$vars['comment']			=	$linkObject -> tolink_evaluate(htmlentities($dataSet['comment']));
					$vars['username']			=	$dataSet['username'];
					$vars['uid']				=	$dataSet['uid_fk'];
					$vars['cface']				=	$wallObject -> Gravatar($this -> session -> userdata('uId'));
					$vars['time'] 				= 	$dataSet['created'];
					$vars['msg_id'] 			= 	$dataSet['msg_id_fk'];
					$this -> load -> view('dsn/comment_ajax', $vars);
				}
				elseif($checkWhichSessionExist==1)
				{
					echo json_encode(array(1, "Comment Successfully Added"));
				}
				else 
				{
					echo json_encode(array(0, "Error Occurred"));
				}
			}
	}
	function uploadStatusImage()
	{
		$this -> dsn_model -> uploadStatusImageProcess();
		$this -> message_image_ajax('Paragon Test Message', $_FILES['Filedata']['name']);
	}
	function like_message_ajax()
	{
		$wallObject					=		new Wall_Updates();
		if($this -> input -> post('msg_id'))
		{
			$msg_id					=		$this -> input -> post('msg_id');
			$data					=		$wallObject -> like_Update($this -> session -> userdata('uId'), $msg_id);
			echo $data;
		}
	}
	function like_data()
	{
		$wallObject					=		new Wall_Updates();
		print_r(json_encode($wallObject -> likes()));
	}
	function comment_data()
	{
		$wallObject					=		new Wall_Updates();
		print_r(json_encode($wallObject -> Comments()));
	}
	function get_realtime_activity_feeds()
	{
		$wallObject					=		new Wall_Updates();
		$wallObject -> get_real_time_updates();
	}
	function get_l_c_id()
	{
		$wallObject					=		new Wall_Updates();
		$l_c_id						=		$wallObject -> get_l_c_id();
		$data['realtime_l_id']		=		$l_c_id[0];
		$data['realtime_c_id']		=		$l_c_id[1];
	}
	function load_activity_feed()
	{
		$data['feed'] 				= 		$this -> input -> post('activity_feed');
		$data['timeStampObject']	=		new Time_stamp();
		$this -> load -> view('dsn/load_activity_feed', $data);
	}
	function loadDetailsOnHover()
	{
		$this -> load -> view('dsn/hoverDetails');
	}
	function get_message()
	{
		$vars['wallObject']					=		new Wall_Updates();
		$vars['linkObject']					=		new Tolink();
		$vars['timeStampObject']			=		new Time_stamp();

		if($this -> uri -> segment(3))
		{
			$dataSet					=		$vars['wallObject']->get_message_by_id($this -> uri -> segment(3));

			if(is_array($dataSet) && count($dataSet) > 0)
			{
				$dataSet 				= 		$dataSet[0];
			}
			else
			{
				echo 'This post has been deleted';
				exit;
			}

			if(is_object($dataSet))
			{
				$vars['msg_id']			=		$dataSet -> msg_id;
				$vars['message']		=		$vars['linkObject']->tolink_evaluate(htmlentities($dataSet -> message));
				$vars['uid']			=		$dataSet ->  uid_fk;
				$vars['username']		=		$dataSet -> username;
				$vars['face']			=		$vars['wallObject']->Gravatar($this -> session -> userdata('uId'));
				$vars['time']			=		$dataSet -> created;
				$vars['attached_image']	=		$dataSet -> attached_image;
				$vars['likeStatus']		=		$dataSet -> like_status;
				$vars['orimessage']		=		$dataSet -> message;
				//$vars['time'] 			=	 	$timeStampObject -> time_stamp_evaluate($dataSet['created']);
				
				$this -> load -> view('dsn/hoverDetails', $vars);
			}
		}
	}
	function chat()
	{
		$this -> load -> view('dsn/chatBox');
	}
	function get_realtime_wall_feeds()
	{
		$wallObject					=		new Wall_Updates();
		$wallObject -> get_real_time_wall_updates();
	}
	function get_m_id()
	{
		$wallObject					=		new Wall_Updates();
		$m_id						=		$wallObject -> get_m_id();
		$data['realtime_m_id']		=		$m_id[0];
	}
	function load_wall_feed()
	{
		$data['wall_feed'] 				= 		$this -> input -> post('wall_feed');
		$data['timeStampObject']		=		new Time_stamp();
		$data['wallUpdates']			=		new Wall_Updates();
		$data['linkObject']				=		new Tolink();
		$this -> load -> view('dsn/load_wall_feed', $data);
	}
	function isMobileDevice()
	{
		$iphone 	= strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$android 	= strpos($_SERVER['HTTP_USER_AGENT'],"Android");
		$berry 		= strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
		$ipod 		= strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$ipad 		= strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		
		if ($iphone || $android || $ipod || $berry || ipad== true)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}		
	}
	function showComment()
	{
		$vars['comment'] 				= 		base64_decode($this -> uri -> segment(3));
		$vars['msg_id'] 				= 		base64_decode($this -> uri -> segment(4));
		$vars['username'] 				= 		base64_decode($this -> uri -> segment(5));
		$vars['com_id'] 				= 		base64_decode($this -> uri -> segment(6));
		$vars['time'] 					=	 	base64_decode($this -> uri -> segment(7));
		$vars['timeStampObject']		=		new Time_stamp();
		$this -> load -> view('dsn/showDetailComment', $vars);
	}
	function uNameDetails()
	{
		$vars['uid']					=		base64_decode($this -> uri -> segment(3));
		$vars['uNameDetails']			=		$this -> dsn_model-> uNameDetails($vars['uid']);
		$this -> load -> view('dsn/userNameDetails', $vars);
	}
	function set_user_status()
	{
		$vars['setUserStatus']			=		$this -> dsn_model-> setUserStatus($this -> input -> post('token'));
	}
	function mark_as_inappropriate()
	{
		$mode  = $this -> input -> post('mode');
		$id  = $this -> input -> post('id');
		$this -> dsn_model -> mark_as_inappropriate($mode, $id);
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function getWallContent()
	{
		$timeStampObj = new Time_stamp();
		/*Code Snippet for checking, wheather it is web service call or a browser call*/
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$wallObject					=		new Wall_Updates();
			$content					=		$wallObject -> Updates();
			$i							=		0;
			
			foreach($content as $iContent)
			{
				foreach($iContent as $key => $value)
				{
					if($key=='created')
					{
						$content[$i]["created"] = $timeStampObj -> time_stamp_evaluate($value);
						$i++;
					}
				}
			}
			echo json_encode($content);
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
		/**/
	}
	function loginAsAnonymous()
	{
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$guestId		= $this -> uri -> segment(4);
			$displayName 	= $this -> uri -> segment(5);
			if($guestId=="" || $displayName=="")
			{
				echo json_encode(array(0, "Bad Request For Accessing DSN"));
				exit;
			}
			else
			{
				echo json_encode(array(1, $this -> dsn_model -> generateAnonymousUser($guestId, $displayName)));
				exit;
			}
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
	}
	function guestCheckout()
	{
		$checkWhichSessionExist				=		$this -> checkWhichSessionExist();
		if($checkWhichSessionExist==1)
		{
			$userId			= $this -> uri -> segment(4);
			if($userId=="")
			{
				echo json_encode(array(0, "Bad Request For Accessing DSN"));
				exit;
			}
			else
			{
				if($this -> dsn_model -> guestCheckout($userId))
				{
					echo json_encode(array(1, 'Guest Checkout Successfull'));
				}
				exit;
			}
		}
		else 
		{
			$this->load->view('dashboard_pages/login.php');
			return false;
		}
	}
}
