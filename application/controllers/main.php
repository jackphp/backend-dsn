<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of main
 *
 * @author pbsl
 */
class main extends CI_Controller {

	var $data;
	function  __construct(){
		parent::__construct();
		$this->data['title'] = "Welcome" ;
	}

	function index() {
		$uid = $this->user->get_user_info('uid');
		if($uid != "" && $uid > 0) {
			redirect('home');
		}
		$this->data['content'] = '<b>ITC Hotel Management 1.0</b>';
		//$this->data['left'] = 'Left Navigation';
		$vars = $this->theme->theme_vars('pages/main', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	}
}
?>
