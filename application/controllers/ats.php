<?php
/**
 * Description of ats
 *
 * @author pbsl
 */
class ats extends CI_Controller {
	var $data = array();

	public function  __construct() {
		parent::__construct();
		$perm = array('access ats reports');
		$this->user->set_access_permission($perm);

		$menu = array('menu'=>'Reports', 'links'=>array(array('label'=>'ATS', 'path'=>'ats', 'permission'=>'access ats reports', 'menu_order'=>0)));
		$this->menu->set_menu($menu);

		$this->data['title'] = 'Rports - ATS';
		$this->load->model('ats_model');
	}

	public function index() {
		if($this->user->is_user_access()) {
			$this->data['page_heading'] = 'Floors overview';
			$q = $this->ats_model->floors_overview();
			$header = array(array('data'=>'#'), array('data'=>'Floor'), array('data'=>'Total rooms'),'class'=>'listheading');
			$i = 0;
			$rows = array();
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				$fid = $row->fid;
				$fname = anchor('ats/floor/'.$fid, $row->fname);
				$count = anchor('ats/floor/'.$fid, $row->ctr);

				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>$fname),
				array('data'=>$count, 'attributes'=>array('align'=>'center')),
                    'attributes'=>array('class'=>$class)
				);
			}

			$this->data['floors_overview'] = $this->theme->generate_list($header, $rows);
			$vars = $this->theme->theme_vars('pages/floors_overview_view', $this->data);

			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function floor($fid="") {
		if($this->user->is_user_access()) {
			if($fid == "") {
				redirect('ats');
			}
			else {

				$floors = array();
				$floor_q = $this->common_model->get_records('floors');
				foreach($floor_q->result() as $row) {
					$floors[$row->fid] = $row->fname;
				}

				$location = base_url().'ats/floor/';
				$this->data['floors'] = form_dropdown('floors', $floors, $fid, 'onchange="location.href=\''.$location.'\'+this.value"');

				$header = array(array('data'=>'Room no'), array('data'=>'iRemote'), array('data'=>'Room Controller'), array('data'=>'Battry Status %'), array('data'=>'Tv Status'), array('data'=>'Version'), array('data'=>'Rented'), array('data'=>'Room Occupied'), array('data'=>'DND'), array('data'=>'Room Service'), array('data'=>'Maintenance'), array('data'=>'Temp.'), 'class'=>'listheading');
				$q = $this->ats_model->load_floor_rooms($fid);
				$rows = array();

				$i = 0;
				$fname = '';
				foreach($q->result() as $row) {
					$i++;
					if($i%2==0) {
						$class = "even";
					}
					else {
						$class = "odd";
					}
					$fname = $row->fname;

					$temperature = $row->temperature;
					$keytag = $row->keytag;
					$rented = $row->rented;
					$controller = $row->controller;
					$iremote = $row->iremote;
					$tv = $row->tv;
					$dnd = $row->privacy_please;
					$room_service = $row->make_my_room;


					if($row->temperature == 0) {
						$temperature = 'N/A';
					}

					if($keytag == 0) {
						$keytag = 'No';
					}
					else {
						$keytag = 'Yes';
					}

					if($rented == 0) {
						$rented = 'No';
					}
					else {
						$rented = 'Yes';
					}

					if($controller == 0) {
						$controller = '<img src="'.ASSETS_PATH.'images/notok.png" height="32">';
					}
					else {
						$controller = '<img src="'.ASSETS_PATH.'images/ok.png" height="32">';
					}

					if($iremote == 0) {
						$iremote = '<img src="'.ASSETS_PATH.'images/notok.png" height="32">';
					}
					else {
						$iremote = '<img src="'.ASSETS_PATH.'images/ok.png" height="32">';
					}

					if($tv == 0) {
						$tv = '<img src="'.ASSETS_PATH.'images/notok.png" height="32">';
					}
					else {
						$tv = '<img src="'.ASSETS_PATH.'images/ok.png" height="32">';
					}

					if($room_service == 0) {
						$room_service = '<img src="'.ASSETS_PATH.'images/off.png" height="32">';
					}
					else {
						$room_service = '<img src="'.ASSETS_PATH.'images/on.png" height="32">';
					}

					if($dnd == 0) {
						$dnd = '<img src="'.ASSETS_PATH.'images/off.png" height="32">';
					}
					else {
						$dnd = '<img src="'.ASSETS_PATH.'images/on.png" height="32">';
					}


					$rows[] = array(
					array('data'=>$row->room_name, 'attributes'=>array('align'=>'center')),
					array('data'=>$iremote, 'attributes'=>array('align'=>'center')),
					array('data'=>$controller, 'attributes'=>array('align'=>'center')),
					array('data'=>$row->battry, 'attributes'=>array('align'=>'center')),
					array('data'=>$tv, 'attributes'=>array('align'=>'center')),
					array('data'=>$row->version, 'attributes'=>array('align'=>'center')),
					array('data'=>$rented, 'attributes'=>array('align'=>'center')),
					array('data'=>$keytag, 'attributes'=>array('align'=>'center')),
					array('data'=>$dnd, 'attributes'=>array('align'=>'center')),
					array('data'=>$room_service, 'attributes'=>array('align'=>'center')),
					array('data'=>form_checkbox('maintenance[]', $row->maintenance, $row->maintenance, 'id="'.$row->room_id.'" class="multiple_chk"'), 'attributes'=>array('align'=>'center')),
					array('data'=>$temperature, 'attributes'=>array('align'=>'center')),
                        'attributes'=>array('class'=>$class)
					);
				}

				$this->data['page_heading'] = $fname." Floor Status";
				$this->data['floor_rooms'] = $this->theme->generate_list($header, $rows);
				$vars = $this->theme->theme_vars('pages/floor_rooms_view', $this->data);

				$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function save_room_status() {
		if($this->user->is_user_access()) {
			if($this->input->post('room_id') > 0) {
				$room_id = $this->input->post('room_id');
				$field = $this->input->post('type');
				$val = $this->input->post('val');
				if($this->ats_model->update_room_status($room_id, $field, $val)) {
					$this->message->set("Your configuration has been saved.", "success", TRUE);
					if($val > 0) {
						$this->watchdog->save('success', 'ats', 4, $room_id);
					}
					else {
						$this->watchdog->save('success', 'ats', 5, $room_id);
					}
					echo 1;
				}
				else {
					$this->message->set("Error in record saving.", "error", TRUE);
					$this->watchdog->save('error', 'ats', 7, $room_id);
					echo 0;
				}

			}
		}
	}

} // End of class
?>