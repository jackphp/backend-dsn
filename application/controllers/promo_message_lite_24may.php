<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
//echo "I am in lite version";die();
class promo_message_lite  extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("promo_msg_model_lite");
		// $this->load->model("common_model");
		$this->load->library('autocontact');
		$this->data['main_title'] = 'Promotions';
		//$perm = array('update shopping pdf');
		$this->load->library('pagination');
		//$this->user->set_access_permission($perm);
	}


	public function index(){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$this->data['page_heading']='Promotions';
			$date = date('Y-m-d H:i:s');
			$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "(published=1 and sent=0) and end_date > '".$date."' and start_date > '".$date."' order by start_date ");
			$result=$q->result();
			$this->data['s_result']=$result;
			$q1 = $this->common_model->get_record_by_condition("iremote.promo_msg", "((published=1 and sent=1) and end_date > '".$date."') or (start_date < '".$date."' and end_date > '".$date."') order by end_date ");
			$result1=$q1->result();
			$this->data['p_result']=$result1;
			$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}


	}

	public function edit($mid=0) {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($mid > 0){
				$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "promo_msg_id=".$mid);
				foreach($q->result() as $row) {
					// print_r($row);die();
					$title= $row->title;
					$extra_info=$row->extrainformation;
					$msg_body = $row->msg_body;
					$promo_msg_img = $row->promo_msg_img;
					$promo_page_url = $row->promo_page_url;
					$end_date = $row->end_date;
					$start_date = $row->start_date;
					$mid=$mid;
					 
				}

				if($end_date=='0000-00-00 00:00:00'){
					$end_date ='';
					 
				}

				if($start_date=='0000-00-00 00:00:00'){
					$start_date ='';
					$checked ='';
				}
				else{
					$checked='checked';
				}

				$url = $promo_msg_img;
				$file = substr($url,strrpos($url,'/'),strlen($url));
				$file=substr($file,1);
			}
			else{
				$checked='';
				$title='';
				$extra_info='';
				$msg_body='';
				$promo_msg_img='';
				$promo_page_url='';
				$end_date='';
				$start_date='';
				$file='';
				$mid='';
			}

			 
			//echo $file;die();
			 
			 
			$this->data['page_heading']='Promotions';

			//Upload Start
			$this->data['form_open'] = form_open_multipart('promo_message_lite/submit','name="first" id="first"');
			$this->data['form_close'] = form_close();
			$this->data['file'] = form_upload("file",'','size=18 id="filen" onchange="fileSelected();" class="file" style="width:315px;position: relative;z-index: 2;text-align: right;" ');
			$this->data['title']=$title;
			$this->data['extra_info']=$extra_info;
			$this->data['promo_page_url']=$promo_page_url;
			$this->data['msg_body']= $msg_body;
			$this->data['promo_msg_img']= $promo_msg_img;
			$this->data['start_date']= $start_date;
			$this->data['end_date']= $end_date;
			$this->data['image'] = $file;
			$this->data['checked']=$checked;
			$this->data['mid']=$mid;
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			//$this->data['page_heading'] = 'Video Song SubCategory';
			//$this->data['cat_id']=$id;
			//$q = $this->video_song_category_model->get_subcategory($id);
			//$this->data['result']=$q->result();
			//$this->data['form_id'] = form_hidden('form_id', 'upload_torrent');
			$vars = $this->theme->theme_vars('pages/promotional_message_lite', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 

	 

	public function messages(){
		$this->data['page_heading']='Promotions';
		$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "draft=1 or sent=0");
		$result=$q->result();
		$this->data['result']=$result;
		$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		 
		 
	}
	 
	public function delete_message($mid){
		$q = $this->promo_msg_model_lite->delete_message($mid);
		$this->message->set("Promotional Message deleted successfully.", "success", TRUE);
	}
	 
	public function send_message($mid) {
		$this->data['page_heading']='Promotions';
		$ch=$this->promo_msg_model_lite->get_time($mid);
		$time=$ch->row($ch);
		$time=$time->start_date;
		//echo 'time';echo $time;die();
		$q= $this->promo_msg_model_lite->send($mid,$time);//echo 'mid='; echo $mid;die();
		//$this->message->set("Message successfully saved.", "success", TRUE);
		 
		if($time=='0000-00-00 00:00:00'){
			$rs= $this->promo_msg_model_lite->get_all_guest(); //entry in promo_msg_to_deliver
			$result=$rs->result();
			//print_r($result);
			foreach($result as $row){ //print_r($row);echo $row->roomno;die();
				//room_no,guest_id,promo_msg_id,ip
				$param = array('room_no'=>$row->roomno,'guest_id'=>$row->guestid,'promo_msg_id'=>$mid, 'ip'=>$row->pc_ip);

				//guest_id,guest_name,promo_msg_id,token
				//  $param='';
				$last_token_id= $this->promo_msg_model_lite->promo_msg_to_deliver($param);
				$data = array('guest_id'=>$row->guestid,'guest_name'=>$row->guestname,'promo_msg_id'=>$mid, 'token'=>$last_token_id);


				$d= $this->promo_msg_model_lite->promo_msg_status($data);
				$this->promo_msg_model_lite->set_sent($mid);

			}
			$this->message->set("Message successfully sent.", "success", TRUE);
		}
		else{
			$this->message->set("Message Scheduled successfully.", "success", TRUE);
		}
		 
		$vars = $this->theme->theme_vars('pages/all_promotional_message_lite', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		//redirect('promo_message_lite/messages');
		 
		 
	}

	public function rr_upload(){
		if ($_FILES["file"]["error"] > 0)
		{
			echo "Error: " . $_FILES["file"]["error"] . "<br>";
		}
		else
		{
			 
			$uploaddir = '/var/www/html/backend/images/promo_images/';
			$_FILES['file']['name']= str_replace(' ','_',$_FILES['file']['name']);
			$uploadfile = $uploaddir . basename($_FILES['file']['name']);


			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				echo "File is valid, and was successfully uploaded.\n";
			} else {
				echo "Possible file upload attack!\n";
			}


		}
	}

	 
	public function delete_send($mid){
		$last_id = $this->promo_msg_model_lite->update_delete_send($mid);
		$rs= $this->promo_msg_model_lite->get_all_guest(); //entry in promo_msg_to_deliver
		$result=$rs->result();
		//print_r($result);
		foreach($result as $row){ //print_r($row);echo $row->roomno;die();
			//room_no,guest_id,promo_msg_id,ip
			$param = array('room_no'=>$row->roomno,'guest_id'=>$row->guestid,'promo_msg_id'=>$mid, 'ip'=>$row->pc_ip,'is_updated'=>'1');

			//guest_id,guest_name,promo_msg_id,token
			//  $param='';
			$last_token_id= $this->promo_msg_model_lite->delete_promo_msg_to_deliver($param);
			$data = array('guest_id'=>$row->guestid,'guest_name'=>$row->guestname,'promo_msg_id'=>$mid, 'token'=>$last_token_id);


			$d= $this->promo_msg_model_lite->promo_msg_status($data);
			$this->promo_msg_model_lite->set_sent($mid);
		}
		//$q = $this->promo_msg_model_lite->delete_message($mid);
		$this->message->set("Promotional Message deleted successfully.", "success", TRUE);

		 
		 
	}

	public function save_send(){ //print_r($_POST);die();

		$login_id=$this->session->userdata("user_id");
		$value=$this->input->post('value');
		//          if($value=='1'){
		//               $sent='0';
		//
		//           }
		//           else{
		//               $sent='1';
		//           }
		$mid= $this->input->post('mid');
		$title= $this->input->post('title');
		$extra_info=$this->input->post('extra_info');
		$promo_page_url=$this->input->post('promo_page_url');
		$result1 = substr($promo_page_url, 0, 7);
		$result2 = substr($promo_page_url, 0, 8);
		if($result1== 'http://'){

		}
		else if($result2== 'https://'){
			 
		}
		else{
			$promo_page_url ='http://'.$promo_page_url;
		}
		$tmp=parse_url($promo_page_url);
		$tmp['host'];
		$msg_body = $this->input->post('promotional_message');
		$start_date= $this->input->post('start_date');
		if($this->input->post('start')!=''){
			 
			$start_date= $this->input->post('start');
			 
		}
		if($start_date==''){
			$date = date('Y-m-d H:i:s');
			$start_date=$date;
		}
		 
		$end_date =$this->input->post('end_date');
		$name= $this->input->post('fileName');


		if($mid !=''){
			if($_POST['fileName']==''){
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info,'promo_page_url'=>$promo_page_url, 'end_date'=>$end_date,'start_date'=>$start_date,'published'=>'1','draft'=>'0','sent'=>$sent);
			}
			else{

				$name=$_POST['fileName'];
				$name = str_replace(' ','_',$name);
				$promo_msg_img='http://'.$tmp['host'].'/backend/images/promo_images/';
				$promo_msg_img .=$name;
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date,'published'=>'1','draft'=>'0','sent'=>$sent);

			}
			$last_id = $this->promo_msg_model_lite->update_message($param,$mid);
		}
		else{

			$name= $this->input->post('fileName');
			$value=$this->input->post('value');
			if($value=='1'){
				$sent='0';
				 
			}
			else{
				$sent='1';
			}
			$promo_msg_img='http://'.$tmp['host'].'/backend/images/promo_images/';
			$name = str_replace(' ','_',$name);
			$promo_msg_img .=$name;
			$msg_body=$this->input->post('promotional_message');
			$title = $this->input->post('title');
			$extra_info = $this->input->post('extra_info');
			 
			 
			$author_id=$login_id;
			$end_date=$this->input->post('end_date');
			$start_date= $this->input->post('start_date');
			if($start_date==''){
				$date = date('Y-m-d H:i:s');
				$start_date=$date;
			}
			 
			 
			//echo $promo_msg_img;die();
			$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'published'=>'1','author_id'=>$author_id,'end_date'=>$end_date,'start_date'=>$start_date,'draft'=>'0','sent'=>$sent);
			$mid = $this->promo_msg_model_lite->send_message($param);
			 
		}
		if($value=='0'){
			$rs= $this->promo_msg_model_lite->get_all_guest(); //entry in promo_msg_to_deliver
			$result=$rs->result();
			//print_r($result);
			foreach($result as $row){ //print_r($row);echo $row->roomno;die();
				//room_no,guest_id,promo_msg_id,ip
				$param = array('room_no'=>$row->roomno,'guest_id'=>$row->guestid,'promo_msg_id'=>$mid, 'ip'=>$row->pc_ip);

				//guest_id,guest_name,promo_msg_id,token
				//  $param='';
				$last_token_id= $this->promo_msg_model_lite->promo_msg_to_deliver($param);
				$data = array('guest_id'=>$row->guestid,'guest_name'=>$row->guestname,'promo_msg_id'=>$mid, 'token'=>$last_token_id);


				$d= $this->promo_msg_model_lite->promo_msg_status($data);
				$this->promo_msg_model_lite->set_sent($mid);
			}
			$this->message->set("Promotional Message sent successfully .", "success", TRUE);
		}
		else{
			 
			$date = date('Y-m-d H:i:s');

			if($start_date > $date){
				$this->message->set("Promotional Message Scheduled successfully.", "success", TRUE);
			}
			else{
				$this->message->set("Promotional Message updated successfully.", "success", TRUE);
			}
			//Scheduled messages
			 
		}



		 
		// print_r($_POST); die();
	}

	 
	public function published_json(){
		//           $sql=mysql_query("SELECT * FROM promo_msg where published='1' and sent='0'");
		//            while($row=mysql_fetch_assoc($sql)){
		//            echo "<pre>";print_r($row);
		//
		//            }
		//            die();
		//
		$query = $this->db->query("SELECT * FROM iremote.promo_msg where published='1' and sent='0'");
		$res = $query->result();
		foreach($res as $row)
		{
			// echo "<pre>";print_r($row);
			$output['aaData'][] = $row;
			//$output[]=$row;
		}

		return (json_encode($output));
		 
	}
	 
	public function scheduled_refresh(){
		$date = date('Y-m-d H:i:s');
		$q = $this->common_model->get_record_by_condition("iremote.promo_msg", "(published=1 and sent=0) and end_date > '".$date."' and start_date > '".$date."' order by start_date ");
		$s_result=$q->result();
		$output='';
		$script='';
		$script.='<script src="'.get_assets_path("js").'dashboard/libs/jquery-1.7.2.min.js"></script>';
		$script.='<link rel="stylesheet" href="'.get_assets_path("css").'dashboard/styles/table.css?v=1">';
		$script.='<script src="'.ASSETS_PATH.'datatable/js/media.js"></script>';
		$script.='<script src="'.ASSETS_PATH.'datatable/js/dataTable.js"></script>';

		 
		$output.=$script;
		$script1='';
		$script1.='<script type="text/javascript" charset="utf-8">';
		$script1.=         "$('#user_table').dataTable({";

		 
		$script1.=                       '  "iDisplayLength": 5,';
		$script1.=                       '  "sPaginationType": "full_numbers",';
		//  $script1.=                       '  "aaSorting": [[ 3, "asc" ]],';
		$script1.=                       '  "aoColumnDefs": [ ';

		$script1.=     " { 'bSortable': false, 'aTargets': [-1,2,3,4,5]}]";
		$script1.=                    ' });';
		$script1.=                 '</script>';
		$output.=$script1;
		$output.= '<table class="table" id="user_table" >';
		$output.=    '<thead>';
		$output.= '<tr>';
		$output.=     '<th width="10%" align="center">S. No.</th>';
		$output.=     '<th width="15%" align="center">Title</th>';
		$output.=     '<th width="15%" align="center">Image</th>';
		$output.=     '<th width="15%" align="center">Start Date</th>';
		$output.=     '<th width="15%" align="center">Expiry Date</th>';
		$output.=     '<th width="15%" align="center">Status</th>';
		$output.=     '<th width="15%" align="center">Operations</th>';
		$output.= '</tr>';
		$output.='</thead>';

		 
		$i=1; foreach($s_result as $row){
			 
			$output = $output.'<tr class="gradeA">';
			$output = $output.'<td width="10%" style="vertical-align:middle" align="center">'.$i.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$row->title.'</td>';
			$date = $row->start_date;$d= date('d-m-Y H:i', strtotime($date));
			$output = $output.'<td width="15%" align="center"><img width="50" height="50"  src="'.$row->promo_msg_img.'" /></td>';
			if($d=='01-01-1970 05:30')
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >-</td>';
			}else
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$d.'</td>';
			}
			$end_date = $row->end_date;
			$display_end_date = date('d-m-Y H:i', strtotime($end_date));
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$display_end_date.'</td>';
			$date = $row->start_date;
			$date2 = date('Y-m-d H:i');
			//$date2 = "2013-04-20 13:44:01";

			$diff = abs(strtotime($date2) - strtotime($date));

			$years   = floor($diff / (365*60*60*24));
			$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

			$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

			//$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
			$time='';
			if($years){$time= $years;}
			if($months){ $time.= $months.'months, ';}
			if($days){
				if ($days == '1') {
					$time.= $days . ' day, ';
				} else {
					$time.= $days . ' days, ';
				}
			}
			if ($hours) {
				if ($hours == '1') {
					$time.= $hours . ' hour ';
				} else {
					$time.= $hours . ' hours ';
				}
			}
			if($minuts){
				if ($minuts == '1') {
					$time.= $minuts . 'minute';
				} else {
					$time.= $minuts . ' minutes ';
				}
			}
			$time.= ' to go.';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$time.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center"><a class="button icon-pencil" href="promo_message_lite/edit/'.$row->promo_msg_id.'" >Edit</a>  <a href="#" class="button icon-trash"  onclick="delete_message('.$row->promo_msg_id.');" >Delete</a></td>';

			 
			$output = $output.'</tr>';


			$i++; }
			$output.= '</table>';
			 

			 
			 
			 
			$style='';
			$style.='<style>';
			$style.='#current_table_length{font-size:20px;font-weight:bold;color:666666}';
			$style.='#user_table_length {font-size:20px;font-weight:bold;color:666666}';
			$style.='.dataTables_wrapper, .dataTables_header, .dataTables_footer {background:#e6e7ec !important;color:black}';
			$style.='.dataTables_wrapper{border-radius:15px;color:#666666;box-shadow: 1px 0 0 1px #ececf1 inset, 0 2px 0 rgba(255, 255, 255, 0.35) inset !important}';
			$style.='.paginate_disabled_previous,a.paginate_active , a.paginate_active.first,  .paginate_enabled_previous, .paginate_disabled_next,paginate_active,.paging_full_numbers a.paginate_active, .paging_full_numbers a.paginate_active.first, .paging_full_numbers a.paginate_active.last, .paginate_enabled_next, .paging_full_numbers a{';

			/*        background-image: -webkit-linear-gradient(top, #666666, #666666); */

			$style.= 'background-image: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#666666), to(#666666));';
			$style.='background-image: -webkit-linear-gradient(top, #666666, #666666);';
			$style.= 'background-image: -moz-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666) !important;';
			/*         background-image: -webkit-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666);*/
			 
			 
			$style.=  'border:1px solid #cfcfcf !important;';
			$style.='}';
			$style.='#user_table_wrapper   {border:2px solid #CFCFCF !important}';
			$style.='#current_table_wrapper{border:2px solid #CFCFCF !important}';
			$style.='.sorting_asc{cursor: pointer}';
			$style.='.sorting_desc{cursor: pointer}';
			$style.='</style>';
			 
			$output.=$style;
			echo $output;
	}
	 
	public function published_refresh(){

		$date = date('Y-m-d H:i:s');
		$q1 = $this->common_model->get_record_by_condition("iremote.promo_msg", "((published=1 and sent=1) and end_date > '".$date."') or (start_date < '".$date."' and end_date > '".$date."') order by end_date  ");
		$s_result=$q1->result();
		$output='';
		$script='';
		$script.='<script src="'.get_assets_path("js").'dashboard/libs/jquery-1.7.2.min.js"></script>';
		$script.='<link rel="stylesheet" href="'.get_assets_path("css").'dashboard/styles/table.css?v=1">';
		$script.='<script src="'.ASSETS_PATH.'datatable/js/media.js"></script>';
		$script.='<script src="'.ASSETS_PATH.'datatable/js/dataTable.js"></script>';

		 
		$output.=$script;
		$script1='';
		$script1.='<script type="text/javascript" charset="utf-8">';
		$script1.=         "$('#current_table').dataTable({";

		 
		$script1.=                       '  "iDisplayLength": 5,';
		$script1.=                       '  "sPaginationType": "full_numbers",';
		//$script1.=                       '  "aaSorting": [[ 4, "asc" ]],';
		$script1.=                       '  "aoColumnDefs": [ ';

		$script1.=     " { 'bSortable': false, 'aTargets': [-1,2,3,4,5]}]";
		$script1.=                    ' });';
		$script1.=                 '</script>';
		$output.=$script1;
		$output.= '<table class="table" id="current_table" >';
		$output.=    '<thead>';
		$output.= '<tr>';
		$output.=     '<th width="10%" align="center">S. No.</th>';
		$output.=     '<th width="15%" align="center">Title</th>';
		$output.=     '<th width="15%" align="center">Image</th>';
		$output.=     '<th width="15%" align="center">Start Date</th>';
		$output.=     '<th width="15%" align="center">Expiry Date</th>';
		$output.=     '<th width="15%" align="center">Status</th>';
		$output.=     '<th width="15%" align="center">Operations</th>';
		$output.= '</tr>';
		$output.='</thead>';

		 
		$i=1; foreach($s_result as $row){
			 
			$output = $output.'<tr class="gradeA">';
			$output = $output.'<td width="10%" style="vertical-align:middle" align="center">'.$i.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$row->title.'</td>';
			$date = $row->start_date;$d= date('d-m-Y H:i', strtotime($date));
			$output = $output.'<td width="15%" align="center"><img width="50" height="50"  src="'.$row->promo_msg_img.'" /></td>';
			if($d=='01-01-1970 05:30')
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >-</td>';
			}else
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$d.'</td>';
			}
			$end_date = $row->end_date;
			$display_end_date = date('d-m-Y H:i', strtotime($end_date));
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$display_end_date.'</td>';
			$date = $row->start_date;
			$date2 = date('Y-m-d H:i');
			//$date2 = "2013-04-20 13:44:01";

			$diff = abs(strtotime($date2) - strtotime($date));

			$years   = floor($diff / (365*60*60*24));
			$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

			$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

			//$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
			$time='';
			$time.= 'Since ';
			if($years){$time= $years;}
			if($months){ $time.= $months.'months, ';}
			if($days){
				if ($days == '1') {
					$time.= $days . ' day, ';
				} else {
					$time.= $days . ' days, ';
				}
			}
			if ($hours) {
				if ($hours == '1') {
					$time.= $hours . ' hour ';
				} else {
					$time.= $hours . ' hours ';
				}
			}
			if($minuts){
				if ($minuts == '1') {
					$time.= $minuts . ' minute';
				} else {
					$time.= $minuts . ' minutes ';
				}
			}
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$time.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center"><a class="button icon-pencil" href="promo_message_lite/edit/'.$row->promo_msg_id.'" >Edit</a>  <a href="#" class="button icon-trash"  onclick="delete_send_message('.$row->promo_msg_id.');" >Delete</a></td>';

			 
			$output = $output.'</tr>';


			$i++; }
			$output.= '</table>';
			 

			 
			 
			 
			$style='';
			$style.='<style>';
			$style.='#current_table_length{font-size:20px;font-weight:bold;color:666666}';
			$style.='#user_table_length {font-size:20px;font-weight:bold;color:666666}';
			$style.='.dataTables_wrapper, .dataTables_header, .dataTables_footer {background:#e6e7ec !important;color:black}';
			$style.='.dataTables_wrapper{border-radius:15px;color:#666666;box-shadow: 1px 0 0 1px #ececf1 inset, 0 2px 0 rgba(255, 255, 255, 0.35) inset !important}';
			$style.='.paginate_disabled_previous,a.paginate_active , a.paginate_active.first,  .paginate_enabled_previous, .paginate_disabled_next,paginate_active,.paging_full_numbers a.paginate_active, .paging_full_numbers a.paginate_active.first, .paging_full_numbers a.paginate_active.last, .paginate_enabled_next, .paging_full_numbers a{';

			/*        background-image: -webkit-linear-gradient(top, #666666, #666666); */

			$style.= 'background-image: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#666666), to(#666666));';
			$style.='background-image: -webkit-linear-gradient(top, #666666, #666666);';
			$style.= 'background-image: -moz-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666) !important;';
			/*         background-image: -webkit-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666);*/
			 
			 
			$style.=  'border:1px solid #cfcfcf !important;';
			$style.='}';
			$style.='#user_table_wrapper   {border:2px solid #CFCFCF !important}';
			$style.='#current_table_wrapper{border:2px solid #CFCFCF !important}';
			$style.='.sorting_asc{cursor: pointer}';
			$style.='.sorting_desc{cursor: pointer}';
			$style.='</style>';
			 
			$output.=$style;
			echo $output;
	}
	 
	 
	public function published1_refresh(){
		$date = date('Y-m-d H:i:s');
		$q1 = $this->common_model->get_record_by_condition("iremote.promo_msg", "((published=1 and sent=1) and end_date > '".$date."') or (start_date < '".$date."' and end_date > '".$date."') order by end_date ");
		$p_result=$q1->result();
		$output='';
		$i=1; foreach($p_result as $row){
			$output = $output.'<tr class="gradeA">';
			$output = $output.'<td width="10%" style="vertical-align:middle" align="center">'.$i.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$row->title.'</td>';
			$date = $row->start_date;$d= date('d-m-Y H:i', strtotime($date));
			$output = $output.'<td width="15%" align="center"><img width="50" height="50"  src="'.$row->promo_msg_img.'" /></td>';
			if($d=='01-01-1970 05:30')
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >-</td>';
			}else
			{
				$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$d.'</td>';
			}
			$end_date = $row->end_date;
			$display_end_date = date('d-m-Y H:i', strtotime($end_date));
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center" >'.$display_end_date.'</td>';
			$date = $row->start_date;
			$date2 = date('Y-m-d H:i');
			//$date2 = "2013-04-20 13:44:01";

			$diff = abs(strtotime($date2) - strtotime($date));

			$years   = floor($diff / (365*60*60*24));
			$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

			$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

			//$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
			$time='';
			$time.= 'Since';
			if($years){$time= $years;}
			if($months){ $time.= $months.'months, ';}
			if($days){
				if ($days == '1') {
					$time.= $days . ' day, ';
				} else {
					$time.= $days . ' days, ';
				}
			}
			if ($hours) {
				if ($hours == '1') {
					$time.= $hours . ' hour ';
				} else {
					$time.= $hours . ' hours ';
				}
			}
			if($minuts){
				if ($minuts == '1') {
					$time.= $minuts . 'minute';
				} else {
					$time.= $minuts . ' minutes ';
				}
			}
			 
			$output = $output.'<td width="15%" style="vertical-align:middle" align="left">'.$time.'</td>';
			$output = $output.'<td width="15%" style="vertical-align:middle" align="center"><a class="button icon-pencil" href="promo_message_lite/edit/'.$row->promo_msg_id.'" >Edit</a>  <a href="#" class="button icon-trash"  onclick="delete_message('.$row->promo_msg_id.');" >Delete</a></td>';

			 
			$output = $output.'</tr>';

			$i++; }
			echo $output;
	}
	 
	 
	public function submit1() {  //echo '<pre>';print_r($_POST);die();
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$mid=$this->input->post('mid');
			$promo_page_url= $this->input->post('promo_page_url');
			$result1 = substr($promo_page_url, 0, 7);
			$result2 = substr($promo_page_url, 0, 8);
			if($result1== 'http://'){

			}
			else if($result2== 'https://'){
				 
			}
			else{
				$promo_page_url ='http://'.$promo_page_url;
			}
			if($mid !=''){
				$last_id=$mid;
				//echo '<>';print_r($_POST);die();
				$msg_body = $this->input->post('promotional_message');
				$start_date =$this->input->post('start_date');
				$end_date =$this->input->post('end_date');
				$title= $this->input->post('title');
				$extra_info = $this->input->post('extra_info');



				if($_FILES['file']['name']!=''){
					$_FILES['file']['name']= str_replace(' ','_',$_FILES['file']['name']);
					$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
					$promo_msg_img .=$_FILES['file']['name'];
					//echo 'yes';die();
					$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date);


					$last_id = $this->promo_msg_model_lite->update_img_message($param,$mid);
				}
				else{
					//echo 'no';die();
					$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_page_url'=>$promo_page_url,'end_date'=>$end_date,'start_date'=>$start_date);
					$last_id = $this->promo_msg_model_lite->update_message($param,$mid);
					redirect('promo_message_lite');
					//redirect('promo_message_lite/index/'.$last_id);

				}

				 



			}
			else{
				$promo_msg_img='http://'.$_SERVER['HTTP_HOST'].'/backend/images/promo_images/';
				 
				$title= $this->input->post('title');
				$extra_info =$this->input->post('extra_info');

				$msg_body=$this->input->post('promotional_message');
				$_FILES['file']['name']= str_replace(' ','_',$_FILES['file']['name']);
				$promo_msg_img .=$_FILES['file']['name'];
				$author_id=$login_id;
				$end_date=$this->input->post('end_date');
				$start_date=$this->input->post('start_date');
				 
				//echo $promo_msg_img;die();
				$param = array('title'=>$title,'msg_body'=>$msg_body,'extrainformation'=>$extra_info, 'promo_msg_img'=>$promo_msg_img,'promo_page_url'=>$promo_page_url,'published'=>'0','author_id'=>$author_id,'end_date'=>$end_date,'start_date'=>$start_date);
				$last_id = $this->promo_msg_model_lite->save_message($param);
			}
			$name= $this->input->post('pdf');
			//print_r($_POST);die();
			//  print_r($_FILES);die();
			if($_FILES['file']['name']!=''){
				// $_FILES['file']['name'];

				//  $_FILES['file']['name']=$name;
				 
				// $_FILES['file']['name']="shopping.pdf";
				$config['upload_path'] = PROMO_IMAGE_UPLOAD;
				$config['overwrite'] = true;
				 
				$config['allowed_types'] = '*';
				$config['is_image'] = 0;
				$config['max_size']  = '51200';
				//                        print_r($_FILES);die();

				$this->load->library("upload", $config);
				if(!$this->upload->do_upload('file')) {
					$err=$this->upload->display_errors();
					$this->message->set($err, "error", TRUE);
					//  echo $this->upload->display_errors();
				}
				else {
					exec("chmod -R 777 ".PROMO_IMAGE_UPLOAD);
					$d= $this->upload->data();

					$this->message->set("Message successfully saved.", "success", TRUE);
					//redirect('promo_message_lite/index/'.$last_id);
					redirect('promo_message_lite');

				}
				redirect('promo_message_lite');
			}
			 
			 
			 
			 
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}



	function generate() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			send_request(SHOPPING_PUSH_UPDATE);
			$this->message->set("Update successfully sent to rooms.", "Success", TRUE);
			redirect('generate_pdf');
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}
	 

}// End Of class
?>
