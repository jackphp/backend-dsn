<?php
/**
 * Description of room_history
 *
 * @author pbsl
 */
class room_history extends CI_Controller {
	var $data;
	var $floor_id;
	var $room_id;
	var $from_date;
	var $to_date;
	var $floor_name;
	var $room_name;
	var $view_as;
	var $age_group;
	var $room_type;

	public function  __construct() {
		parent::__construct();
		$perm = array('access room history');
		$this->user->set_access_permission($perm);
		$this->load->model('rms_model');
		$this->load->model('room_management_model');
		$this->load->library("forms");
		$this->load->helper('fusion_charts_helper');

		$form_id = $this->input->post("form_id");
		$selected_floor = "";
		$selected_room = "";
		if($form_id == "left_view_form") {
			$this->room_id = $this->input->post('rooms');
			$this->floor_id = $this->input->post('floors');
			$this->data['view_as'] = $this->input->post('viewas');
			$this->room_type = $this->input->post('room_type');

			if($this->data['view_as'] == "room") {
				$this->data['rf_id'] = $this->room_id;
				$selected_room = $this->room_id;
				$q = $this->common_model->get_records_by_field('rooms', 'room_id', $this->room_id);
				$row = $q->result();
				$this->room_name = $row[0]->room_name;

				$q = $this->rms_model->get_room_info($this->room_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}
			}
			else {
				$this->data['rf_id'] = $this->floor_id;
				$selected_floor = $this->floor_id;
				$q = $this->common_model->get_records_by_field('floors', 'fid', $this->floor_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}
			}

			$this->from_date = $this->input->post('from_date');
			$this->to_date = $this->input->post('to_date');
		}
		else {
			$this->data['rf_id'] = $this->uri->segment(4);
			$this->data['view_as'] = $this->uri->segment(3);
			if($this->data['view_as'] == "room") {
				$selected_room = $this->uri->segment(4);
				$q = $this->common_model->get_records_by_field('rooms', 'room_id', $this->data['rf_id']);
				$row = $q->result();
				$this->room_name = $row[0]->room_name;
				$this->room_id = $this->data['rf_id'];

				$q = $this->rms_model->get_room_info($this->room_id);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
				}
			}
			else {
				$selected_floor = $this->uri->segment(4);
				$q = $this->common_model->get_records_by_field('floors', 'fid', $selected_floor);
				$row = $q->result();
				if(isset($row[0])) {
					$this->floor_name = $row[0]->fname;
					$this->floor_id = $this->data['rf_id'];
				}
			}

			$this->from_date = date('Y-m-d', mktime(0, 0, 0, date('m')-1, 1, date('Y')));
			$this->to_date = date('Y-m-d');
		}

		$q = $this->room_management_model->load_floors();
		$rooms = result_to_array($q->result(), 'room_id', 'room_name');
		$floors = result_to_array($q->result(), 'fid', 'fname');

		$this->data['rooms'] = form_dropdown('rooms', $rooms, $selected_room);
		$this->data['floors'] = form_dropdown('floors', $floors, $selected_floor);
		$this->data['from_date'] = $this->from_date;
		$this->data['to_date'] = $this->to_date;
		$this->data['room_type'] = $this->room_type;
		$this->data['title'] = 'Room History';

		//Loading RMS Library
		$param = array('view_as'=>$this->data['view_as'], 'room_id'=>$this->room_id, 'floor_id'=>$this->floor_id, 'from_date'=>$this->from_date, 'to_date'=>$this->to_date, 'room_type'=>$this->room_type);
		$this->load->library("rms_library", $param);
	}


	public function rms($view_as, $room_id="") {
		if($this->user->is_user_access()) {
			$tmp_view = $this->input->post('viewas');
			if($tmp_view != "") {
				$view_as = $tmp_view;
			}

			if($view_as == "room") {
				$heading = "Temperature of Room (".$this->room_name.") From: ".$this->from_date." to ".$this->to_date;

			}

			if($view_as == "floor") {
				$heading = "Temperature of Floor (".$this->floor_name.") From: ".$this->from_date." to ".$this->to_date;
			}

			if($view_as == "hotel") {
				$heading = "Temperature of All Floors From: ".$this->from_date." to ".$this->to_date;
			}

			$this->data['data'] = $this->rms_library->get_graph_data('temperature');
			$this->data['fname'] = $this->floor_name;
			$this->data['page_heading'] = $heading;

			// Next previous form
			$dates = $this->rms_library->calculate_next_previous_date($this->from_date, $this->to_date);
			$this->data['next_form'] = $this->forms->next_form('room_history/rms/'.$view_as.'/'.$this->data['rf_id'], $dates['next_date']['from_date'], $dates['next_date']['to_date'], $view_as, $this->room_id, $this->floor_id);
			$this->data['previous_form'] = $this->forms->previous_form('room_history/rms/'.$view_as.'/'.$this->data['rf_id'], $dates['previous_date']['from_date'], $dates['previous_date']['to_date'], $view_as, $this->room_id, $this->floor_id);

			$perms = $view_as.'/'.$room_id.'/'.$this->from_date.'/'.$this->to_date;
			$this->data['link_on_command'] = anchor('rms/details/ac_on/'.$perms, "View details", 'class="popup"');
			$this->data['link_off_command'] = anchor('rms/details/ac_off/'.$perms, "View details", 'class="popup"');
			$this->data['link_temp_change_command'] = anchor('rms/details/temperature_change/'.$perms, "View details", 'class="popup"');

			$this->data['title'] = $heading;
			$vars = $this->theme->theme_vars('pages/temperature_view', $this->data, array('left'=>'left/room_history_left', 'left_items'=>array('tab'=>'4', 'menu1'=>'AC', 'menu2'=>'', 'left_action'=>'room_history/rms/'.$view_as.'/'.$this->data['rf_id'])));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function ats($view_as, $room_id="") {
		if($this->user->is_user_access()) {
			if($view_as == "") {
				$q = $this->common_model->get_records_by_field('rooms', 'room_id', $room_id);
				$row = $q->result();
				$heading = "Room History of (".$row[0]->room_name.") 01-04-2011 to 01-05-2011";
			}
			else {
				$heading = 'Room History 01-04-2011 to 01-05-2011';
			}

			$this->data['page_heading'] = $heading;
			$this->data['title'] = $heading;
			$vars = $this->theme->theme_vars('pages/ats_history_view', $this->data, array('left'=>'left/room_history_left', 'left_items'=>array('tab'=>'0', 'menu1'=>'Key Tag', 'menu2'=>'', 'left_action'=>'room_history/rms/'.$view_as.'/'.$this->data['rf_id'])));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function tv($view_as, $room_id="") {
		if($this->user->is_user_access()) {

			$tmp_view = $this->input->post('viewas');
			if($tmp_view != "") {
				$view_as = $tmp_view;
			}

			if($view_as == "room") {
				$heading = "TV Reports of (".$this->room_name.")".$this->from_date." to ".$this->to_date;
			}

			else if($view_as == "floor") {
				$heading = 'TV Reports of ('.$this->floor_name.') '.$this->from_date.' to '.$this->to_date;
			}
			else{
				$heading = 'TV Reports of all Rooms '.$this->from_date.' to '.$this->to_date;
			}

			$q = $this->rms_model->get_tv_on_off_command_data($this->from_date, $this->to_date);
			//$this->rms_library->get_device_on_off_data($q->result(), array('on'=>'tv_on', 'off'=>'tv_off'));

			$this->data['page_heading'] = $heading;
			$this->data['title'] = $heading;
			$vars = $this->theme->theme_vars('pages/tv_history_view', $this->data, array('left'=>'left/room_history_left', 'left_items'=>array('tab'=>'4', 'menu1'=>'TV', 'menu2'=>'', 'left_action'=>'room_history/rms/'.$view_as.'/'.$this->data['rf_id'])));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function lights($view_as, $room_id="") {
		if($this->user->is_user_access()) {
			$room_id = $this->room_id;
			$tmp_view = $this->input->post('viewas');
			if($tmp_view != "") {
				$view_as = $tmp_view;
			}
			 
			if($view_as == "room") {
				$heading = "Light & Dimmer Reports of Room (".$this->room_name.") ".$this->from_date." to ".$this->to_date;
			}

			else if($view_as == "floor") {
				$heading = "Light & Dimmer Reports of floor (".$this->floor_name.") ".$this->from_date." to ".$this->to_date;
				$rooms = array();
				$q = $this->common_model->get_records_by_field('floor_rooms', 'fid', $this->floor_id);
				foreach($q->result() as $row) {
					$rooms[] = $row->room_id;
				}

				if(count($rooms) > 0) {
					$room_id = implode(",", $rooms);
				}
			}
			else {
				$heading = 'Light & Dimmer Reports of all Rooms '.$this->from_date.' to '.$this->to_date;
			}

			$q = $this->rms_model->get_lights_on_off_command_data($this->from_date, $this->to_date, $room_id);

			$this->data['data'] = $this->rms_library->get_lights_on_off_data($q->result(), array('room_id'=>$room_id));

			//echo "<pre>"; print_r($this->data['data']); die();

			$this->data['room_ids'] = form_hidden('room_ids', $room_id);

			$this->data['page_heading'] = $heading;
			$this->data['title'] = $heading;
			$vars = $this->theme->theme_vars('pages/lights_dimmer_view', $this->data, array('left'=>'left/room_history_left', 'left_items'=>array('tab'=>'4', 'menu1'=>'Lights & Dimmer', 'menu2'=>'', 'left_action'=>'room_history/lights/'.$view_as.'/'.$this->data['rf_id'])));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}
}
?>