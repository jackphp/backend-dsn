<?php
/**
 * Description of sections
 *
 * @author pbsl
 */
class sections extends CI_Controller {
	var $data;
	var $logo;
	var $description;
	var $video;
	var $revenue = array();

	public function  __construct() {
		parent::__construct();
		$this->data['title'] = "Food Sections";
		$perm = array('section management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Sections', 'path'=>'sections', 'permission'=>'section management', 'menu_order'=>2),
		array('label'=>'Add Section', 'path'=>'sections/add', 'permission'=>'section management', 'menu_order'=>3)
		));
		$this->menu->set_menu($menu);
		$this->load->library('food_library');
		$this->load->library('food_item_library');
		$this->load->model('food_application_model');

		$this->revenue = $this->food_library->get_table_data('food_revenue_primary', 'rvc_id', 'rvc_name');
		$this->food_library->initialize_food_vars(array('revenue'=>$this->revenue));
	}

	public function index() {
		if($this->user->is_user_access()) {
			$q = $this->food_application_model->get_section_list();
			$header = array(array('data'=>'#'), array('data'=>'Name'), array('data'=>'Revenue'), array('data'=>'Description'), array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}
				//echo "<pre>"; print_r($row); die();
				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('sections/edit/'.$row->section_id, $row->name)),
				array('data'=>anchor('revenue/edit/'.$row->rvc_id, $this->revenue[$row->rvc_id])),
				array('data'=>$row->description),
				array('data'=>anchor('sections/edit/'.$row->section_id, 'edit')." | ".anchor('sections/delete/'.$row->section_id, 'delete', array('class'=>'delete')), 'attributes'=>array('align'=>'center')),
                         'attributes'=>array('class'=>$class));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			$this->data['add_link'] = anchor('sections/add', 'Add Section');
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/sections_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function add() {
		if($this->user->is_user_access()) {
			$this->data = $this->food_library->section_form();
			$this->data['title'] = 'Add Section';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_section_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function edit($id = 0) {
		if($this->user->is_user_access()) {
			if($id == 0) { redirect('sections'); }

			$param = array();
			$q = $this->food_application_model->get_section_list($id);
			foreach($q->result_array() as $row) {
				$param = $row;
			}

			$this->data = $this->food_library->section_form($param, $id);
			$this->data['title'] = 'Update Section';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_section_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete($id="") {
		if($this->user->is_user_access()) {
			if($id == "") {redirect('sections');}

			if($this->common_model->delete_record_for_id('food_section', 'sid', $id)) {
				$this->message->set('Section successfully deleted.', 'success', TRUE);
				redirect('sections');
			}
			else {
				$this->message->set('Error in section deletion.', 'error', TRUE);
				redirect('sections');
			}
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function submit() {
		$form_id = $this->input->post('form_id');
		switch ($form_id) {
			case "add_section":
				if(!$this->food_library->section_validate($_POST)) {
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$name = $this->input->post('name');
					$display_name = $this->input->post('display_name');
					$description = $this->input->post('description');
					$rvc_id = $this->input->post('rvc_id');
					$q = $this->common_model->get_records_by_field("food_revenue_primary", "rvc_id", $rvc_id);
					$row = $q->result();
					$rvc_number = $row[0]->rvc_number;

					$param = array('rvc_id'=>$rvc_id, 'rvc_number'=>$rvc_number, 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en']);

					$id = $this->food_application_model->save_section($param);
					$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name, 'description'=>$description), 'section', $id);
					if($id > 0) {
						$this->food_application_model->delete_food_locale('section', $id);
						foreach($locale as $row) {
							$this->food_application_model->save_food_locale($row);
						}

						$this->food_application_model->save_images($images, 'section', $id);
						$this->message->set('Section successfully added.', 'success', TRUE);
						redirect('sections');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in adding section.', 'error', TRUE);
						redirect('sections/add');
					}

				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('sections/add');
				}
				break;

			case "edit_section":
				//echo "<pre>"; print_r($_POST); die();
				if(!$this->food_library->section_validate($_POST)) {
					$sid = $this->input->post('sid');
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$name = $this->input->post('name');
					$description = $this->input->post('description');
					$display_name = $this->input->post('display_name');
					$param = array('rvc_id'=>$this->input->post('rvc_id'), 'name'=>$name, 'display_name'=>$display_name['en'], 'description'=>$description['en']);

					$id = $this->food_application_model->save_section($param, $sid);
					$locale = $this->food_library->prepare_locale_row(array('name'=>$display_name, 'description'=>$description), 'section', $id);
					if($id > 0) {
						$this->food_application_model->save_images($images, 'section', $id);

						$this->food_application_model->delete_food_locale('section', $id);
						foreach($locale as $row) {
							$this->food_application_model->save_food_locale($row);
						}

						$this->message->set('Section successfully updated.', 'success', TRUE);
						redirect('sections');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in section updation.', 'error', TRUE);
						redirect('sections/edit/'.$id);
					}

				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('sections/edit/'.$id);
				}
				break;
		}
	}

}
?>
