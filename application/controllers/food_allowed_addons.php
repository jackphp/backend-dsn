<?php
/**
 * Description of food_addons
 *
 * @author pbsl
 */
class food_allowed_addons extends CI_Controller {
	var $data = array();
	var $groups = array();
	public function  __construct() {
		parent::__construct();
		$this->data['title'] = "Food Revenue Centers";
		$perm = array('menuitem management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Add Addon', 'path'=>'food_addons', 'permission'=>'menuitem management', 'menu_order'=>10)
		));
		$this->menu->set_menu($menu);

		$this->load->model("food_application_model");
		$this->load->model("food_allowed_addon_model");
		$this->load->library('pagination');
		$this->load->library('food_library');

		$q = $this->common_model->get_records("food_allowed_addon_group_master");
		foreach($q->result() as $row) {
			$this->groups[$row->gid] = $row->display_name;
		}

	}

	public function index($gid=0) {

		//            $config['base_url'] = base_url()."food_addons/addon_list/";
		//            $config['total_rows'] = $this->db->count_all("food_addons_master");
		//            $config['per_page'] = $limit;
		//            $config['num_links'] = 1;
		//            $config['first_link'] = "First";
		//            $config['last_link'] = "Last";
		//            $config['uri_segment'] = 3;

		//$this->pagination->initialize($config);
		//$this->data['links'] = $this->pagination->create_links();
		$this->data['links'] = '';

		$this->data['page_heading'] = 'Food Allowed Addons';
		$this->data['gid'] = $gid;
		$this->data['addons'] = $this->food_allowed_addon_model->get_addons($gid);
		$vars = $this->theme->theme_vars('pages/food_allowed_addons/addon_allowed_list', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		 
	}


	public function addon_list($gid=0) {
		$this->index($gid);
	}

	public function delete_addon($id=0,$gid=0) {

		$add=$this->food_allowed_addon_model->get($id);
		$nam=$add->row();
		$name=$nam->addon_name;
		$this->watchdog->save('deleted', 'IRD', 'addon_deleted',$id,$name);
		if($this->food_addon_model->delete_addon($id)){
			$this->message->set("Allowed Addon successfully deleted", "success", TRUE);
		}
		else {
			$this->message->set("Error in allowed addon deletetion", "error", TRUE);
		}

		redirect("food_allowed_addons/addon_allowed_list/".$gid);

	}

	public function add($id=0, $gid=0) {
		 
		if($id > 0) {
			$q = $this->common_model->get_record_by_condition('food_allowed_addons_master', 'addon_id='.$id);
			$rs = $q->result();
			$row = $rs[0];
			$gid = $row->gid;
			$addon_code = $row->addon_code;
			$addon_name = $row->addon_name;
			$display_name = $row->display_name;
			$size = $row->size;
			$price = $row->price;
		}
		else {
			//$gid = '';
			$addon_code = '';
			$addon_name = '';
			$display_name = '';
			$size = '';
			$price = '';
		}

		$this->data['heading'] = 'Add Allowed Addon';
		$this->data['form_open'] = form_open_multipart('food_allowed_addons/submit');
		$this->data['form_close'] = form_close();
		$this->data['form_id'] = form_hidden('form_id', "edit_new_addon");
		$this->data['id'] = form_hidden('id', $id);
		$this->data['group'] = form_dropdown('group', $this->groups, $gid,'class="select" ');
		$this->data['gid'] = $gid;
		$this->data['addon_code'] = form_input('addon_code', $addon_code, 'id="edit-addon-code" class="input"');
		$this->data['addon_name'] = form_input('addon_name', $addon_name, 'id="edit-addon-name" class="input" ');
		$this->data['display_name'] = form_input('display_name', $display_name, 'id="edit-display-name" class="input" ');
		$this->data['size'] = form_input('size', $size, 'id="edit-size" class="input" ');
		$this->data['price'] = form_input('price', $price, 'id="edit-price" class="input" ');

		$vars = $this->theme->theme_vars('pages/food_allowed_addons/add_allowed_addon', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'simple_page', $vars);

		 
	}

	public function submit() {
		 
		$form_id = $this->input->post("form_id");
		// echo $form_id;die();
		switch ($form_id) {

			case "edit_new_addon":
				$images = $this->food_library->upload_files($_FILES);
				$id = $this->input->post('id');
				$group = $this->input->post('group');
				$addon_code = $this->input->post('addon_code');
				$addon_name = $this->input->post('addon_name');
				$display_name = $this->input->post('display_name');
				$size = $this->input->post('size');
				$price = $this->input->post('price');

				$param = array('gid'=>$group, 'addon_code'=>$addon_code, 'addon_name'=>$addon_name, 'display_name'=>$display_name, 'size'=>$size, 'price'=>$price);
				$aid = $this->food_addon_model->add_addons($param, $id);
				if($aid > 0) {
					$this->food_application_model->save_images($images, 'addon', $aid);
					$this->food_application_model->delete_food_locale('addon', $aid);
					$locale_param = array('lang_code'=>'en', 'type'=>'addon', 'type_id'=>$aid, 'name'=>$display_name, 'description'=>$display_name);
					$this->food_addon_model->save_locale($locale_param);
					if($id>0){

						$this->watchdog->save('edit', 'IRD', 'addon_edit',$id,$display_name);
					}
					else{
						$this->watchdog->save('add', 'IRD', 'addon_add',0,$display_name);
					}
					$this->message->set("Addon successfully added.", "success", TRUE);

				}
				else {
					$this->message->set("Error in addon add.", "error", TRUE);
				}

				redirect('food_allowed_addons/addon_allowed_list/'.$group);

				break;
		}

	}
}
?>
