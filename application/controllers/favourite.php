<?php
/**
 * Description of favorites
 *
 * @author pbsl
 */
class favourite extends CI_Controller {

	var $obj;
	var $default_db;
	var $tvchannel_db;
	var $data;
	function  __construct() {
		parent::__construct();
		$this->load->model("fav_category");
		$this->load->model("fav_channels");
		$this->load->model("dashboard_login");

		$this->load->helper("url");
		$this->load->helper("form");
		//$perm = array('favorit management');
		//$this->user->set_access_permission($perm);
		$this->data['title'] = 'TV-Channels';
	}

	function index($c='') {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			 
			$q = $this->fav_channels->get_all_channels();
			$this->data['query'] = $q->result();

			$q = $this->common_model->get_records("fav_cat", "position");

			$this->data['category'] = $q->result();

			//echo "<pre>"; print_r($this->data['query']); die();
			$this->data['page_title'] = 'TV Channels';
			$this->data['page_heading'] = 'TV Channels';
			$q = $this->fav_channels->get_channel_count();
			$pos = array();
			$images = array();
			foreach($q->result() as $r){
				$pos[$r->tag] = $r->tag;
				$images[$r->chid] = $this->load_blob_image($r->iPad_image, $r->chname, TRUE);
			}

			exec("chmod 777 -R ".BASE_PATH);

			$this->data['pos'] = $pos;
			$this->data['images'] = $images;
			$this->data['base_path'] = base_url();
			$this->load->model("dashboard_login");
			$this->data['left']=$this->dashboard_login->user_profile();
			$result = $this->dashboard_login->getUserInfo();
			$vars['user_info']=$result;
			$vars['profile']=$this->dashboard_login->user_profile();
			 
			if($c=='un'){
				redirect("favourite/unfavorites");
			}

			$vars = $this->theme->theme_vars('pages/fav/all_channels', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}
		 
	}

	function my_fun($data,$cat){
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$result=$this->fav_channels->get_selected($data,$cat);
			$query = $result->result();
			$pos = array();
			$images = array();
			foreach($result->result() as $r){
				$pos[$r->tag] = $r->tag;
				$images[$r->chid] = $this->load_blob_image($r->iPad_image, $r->chname, TRUE);
			}

			if(count($query) > 0) {
				$i = 0; foreach($query as $row) {
					 
					echo '<li itemID="'.$row->chid.'" id="'.$row->chid.'"><div class="ch_list" title="'.$row->chname.': '.$row->Channel_No.'">
                        '.$images[$row->chid].'
                        <br/>
                        <a href="'.base_url().'favourite/edit/'.$row->chid.'/1">edit</a> | <a href="'.base_url().'favourite/delete_channel/'.$row->chid.'/1" class="delete1">disable</a>
                        </div>
                        </li><input type="checkbox" name="channelid[]" value="'.$row->chid.'" checked="checked" style="display:none"/>';
					$i++;} }
					else{
						echo '<b style="margin-left:300px">'.'No matching channel found'.'</b>';
					}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function cat_channels($cat_id=0) {

		if($cat_id > 0) {
			$q = $this->fav_channels->get_cat_channels($cat_id);
			$this->data['query'] = $q->result();
			$pos = array();
			$images = array();
			foreach($q->result() as $r){
				$pos[$r->tag] = $r->tag;
				$images[$r->chid] = $this->load_blob_image($r->iPad_image, $r->chname, TRUE);
			}

			$this->data['pos'] = $pos;
			$this->data['images'] = $images;
			$this->data['base_path'] = base_url();
			echo $this->load->view("pages/fav/cat_channels", $this->data, TRUE);
		}
		 
	}

	function edit($cid,$c='') {

		$this->channels($cid,$c);
	}

	function unfavorites() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			 
			$this->data['page_title'] = 'Disabled Channels';
			$this->data['page_heading'] = 'Disabled Channels';
			$this->data['query'] = $this->fav_channels->get_unfavorit_channels();
			$q = $this->fav_channels->get_channel_count();
			$pos = array();
			$images = array();
			foreach($q->result() as $r){
				$pos[$r->tag] = $r->tag;
				$images[$r->chid] = $this->load_blob_image($r->chfile, $r->chname);
			}

			//echo "<pre>"; print_r($images); die();

			$this->data['pos'] = $pos;
			$this->data['images'] = $images;
			$this->data['base_path'] = base_url();

			$vars = $this->theme->theme_vars('pages/fav/unfavorites_channels', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);


		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function save() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$submit = $this->input->post('name');
			if($submit) {
				//echo '<pre>';print_r($_POST);die();
				$id = $this->input->post('id');
				$cat = $this->input->post('tab_val');
				$type = $this->input->post('type');
				$category = $this->input->post('category');
				$chname = $this->input->post('name');
				$port=$this->input->post('port');
				$ip=$this->input->post('ip');
				$hd=$this->input->post('hd');
				$desc=$this->input->post('desc');
				$url=$this->input->post('url');
				$iptv=$this->input->post('iptv');
				$tag = '';
				// print_r($_POST);die();
				$channel_no = $this->input->post('channel_no');


				$epgfilename = $this->input->post("epgfilename");
				$channel_code = $this->input->post("channel_code");

				if($id > 0) {
					$q = $this->common_model->get_record_by_condition("fav", "chid=".$id);
					$row = $q->result();
					$channel_row = $row[0];
					//print_r($row[0]);die();
					$ipod_image = $channel_row->chfile;
					$ipad_image = $channel_row->iPad_image;
					//$chname = $channel_row->chname;

					//$this->load_blob_image($ipod_image, $chname);
					//$this->load_blob_image($ipad_image, $chname, TRUE);

					$ipod_img_data = '';
					$ipad_img_data = '';

					$chimage = $this->do_upload('ipadImage');


					if($chimage != "" && file_exists(BASE_PATH.'/images/ipad_image/'.$chimage)) {
						// print_r($chimage);echo 'yes';die();
						$fp2 = file_get_contents(BASE_URL.'/images/ipad_image/'.$chimage);
						$ipad_img_data = $fp2;
						$ipod_image = $ipad_img_data;
						//print_r($ipod_image);die();
					}
					else {
						$ipad_img_data = $ipad_image;
						$ipod_image = $ipad_image;
					}


					//(chname, chfile, tag, Channel_No, visibility, Type, iPad_image, epgfilename, channel_code)
					$param = array('chname'=>$chname, 'chfile'=>$ipod_img_data, 'Channel_No'=>$channel_no,'Type'=>$category, 'iPad_image'=>$ipad_img_data, 'epgfilename'=>$epgfilename, 'channel_code'=>$channel_code,'channel_text'=>$desc,'url'=>$url,'port'=>$port,'ip'=>$ip,'is_hd'=>$hd,'is_iptv'=>$iptv);
				}
				else {

					$ipod_img_data = '';
					$ipad_img_data = '';

					$chimage = $this->do_upload('ipadImage');

					exec('sudo chmod -R 777 '.BASE_PATH.'/images/ipad_image/'.$chimage);

					if($chimage == "") {
						$this->message->set($this->data['error'], "error", TRUE);
						redirect("favourite/channels");
					}

					if( file_exists( BASE_PATH.'/images/ipod_image/'.$chimage ) ) {
						$fp = file_get_contents(BASE_URL.'/images/ipod_image/'.$chimage);
						$ipod_img_data = $fp;
					}

					if(file_exists( BASE_PATH.'/images/ipad_image/'.$chimage )) {
						$fp2 = file_get_contents(BASE_URL.'/images/ipad_image/'.$chimage);
						$ipad_img_data = $fp2;
						$ipad_image = $fp2;
						$ipod_img_data = $fp2;
					}

					$tag = $this->fav_channels->get_max_position();

					$param = array('chname'=>$chname, 'chfile'=>$ipod_img_data, 'tag'=>$tag, 'Channel_No'=>$channel_no, 'visibility'=>1, 'Type'=>$category, 'iPad_image'=>$ipad_image, 'epgfilename'=>$epgfilename, 'channel_code'=>$channel_code,'channel_text'=>$desc,'url'=>$url,'port'=>$port,'ip'=>$ip,'is_hd'=>$hd,'is_iptv'=>$iptv);
				}
				// echo $param['chname'];die();
				//print_r($_POST);
				if($this->fav_channels->save($param, $id)){

					if($id > 0) {
						 
						//
						//redirect('favourite/message/3/favourite/channels');

						if($submit == 1) {
							$this->watchdog->save('edit', 'Tv Channel', 'channel_edit', $id,$param['chname']);
							$this->message->set("Channel successfully updated.", "success", TRUE);
							echo "ok";
						}
						else {
							$this->watchdog->save('edit', 'Tv Channel', 'channel_edit', $id,$param['chname']);
							$this->session->set_flashdata("Channel successfully updated.", "success", TRUE);
							$this->message->set("Channel successfully updated.", "success", TRUE);

							$ct=$this->fav_channels->find_tab($id);
							$na=$ct->row();
							$cid=$na->position;
							$cid=$cid+1;
							if($cat==''){
								redirect("favourite#ui-tabs-".$cid);

							}
							else if($cat=='un'){
								redirect("favourite/index/un");
								//redirect("favourite/unfavorites");
							}
							else{
								redirect("favourite");
							}



							//redirect("favourite");
						}
					}
					else {
						if($submit == 1) {
							$this->watchdog->save('add', 'Tv Channel', 'channel_add', 0,$param['chname']);
							$this->session->set_flashdata("Channel successfully inserted.", "success", TRUE);
							echo "no";
						}
						else {
							//redirect('favourite/message/1/favourite/channels');
							$this->watchdog->save('add', 'Tv Channel', 'channel_add', 0,$param['chname']);
							$this->session->set_flashdata("Channel successfully inserted.", "success", TRUE);
							$this->message->set("Channel successfully added.", "success", TRUE);
							redirect("favourite");
						}
					}
				}
				else {
					if($id > 0) {
						//redirect('favourite/message/4/favourite_channels_'.$id.'/channels');
						$this->session->set_flashdata("Error in channel updation.", "error", TRUE);
						redirect("favourite");
					}
					else {
						//redirect('favourite/message/2/favourite/channels');
						$this->session->set_flashdata("Error in channel insertion.", "error", TRUE);
						redirect("favourite");
					}
				}
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function channels($id="",$c="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$row = array();
			$category = array(''=>'--Select--');
			$selected_type = '';
			$category_id = '';
			$name = '';
			$channel_no = '';
			$tag = '';
			$port='';
			$desc='';
			$hd='';
			$url='';
			$ip='';
			$hd_id='';
			$iptv_id='';
			$image = '';
			$channel_code = '';
			$epgfilename = '';
			$q = $this->fav_category->get_all_category('category');

			foreach($q->result() as $row) {
				$category[$row->category] = $row->category;
			}

			$hd['0']='No';
			$hd['1']='Yes';
			$iptv['0']='No';
			$iptv['1']='Yes';
			//echo '<pre>'; print_r($hd);die();
			if($id > 0) {
				$cq2 = $this->fav_channels->get_channels($id);
				$row = $cq2->result();
				//$selected_type = $row[0]->type;
				$category_id = $row[0]->Type;
				$name = $row[0]->chname;
				$channel_no = $row[0]->Channel_No;
				$tag = $row[0]->tag;
				$url=$row[0]->url;
				$desc=$row[0]->channel_text;
				$port=$row[0]->port;
				$ip=$row[0]->ip;
				$iptv_id=$row[0]->is_iptv;
				$hd_id=$row[0]->is_hd;
				$image = $this->load_blob_image($row[0]->chfile, $name);
				$channel_code = $row[0]->channel_code;
				$epgfilename = $row[0]->epgfilename;
			}

			$type = array('RF'=>'2 Degit channel', 'IR'=>'3 Degit channel');
			$value = "";

			$sql = "SELECT tag, chname FROM fav";
			$q = $this->db->query($sql);
			$used_tag = array();

			$channel_name = array();

			foreach ($q->result() as $row) {
				$used_tag[$row->tag] = $row->tag;
				$channel_name[] = "'".$row->chname."'";
			}

			if($id > 0) {
				$tags = array($tag=>$tag);
			}
			else {
				$tags = array();
			}
			for($i = 1; $i<=300; $i++) {
				if(!in_array($i, $used_tag)) {
					$tags[$i] = $i;
				}
			}


			$channel_str = implode(",", $channel_name);

			$channels = array(''=>'--Select--');
			//echo "SELECT * FROM channels WHERE name NOT IN(".$channel_str.")"; die();
			//            $q = $this->common_model->get_record_by_condition("channels", "name NOT IN(".$channel_str.")");
			//
			//
			//            foreach($q->result() as $row) {
			//                $channels[$row->id] = $row->name;
			//            }
				$this->data['cat']=$id;
				$this->data['tab_val']=$c;
				$this->data['epgfilename'] = form_input('epgfilename', $epgfilename, 'id="epgfilename" class="input"');
				$this->data['channel_code'] = form_input('channel_code', $channel_code, 'id="channel_code" class="input"');
				$this->data['type'] = form_dropdown('type', $type, $selected_type, 'id="type"');
				$this->data['form_open'] = form_open_multipart('favourite/save', array('name'=>'form1','id'=>'form1'));
				$this->data['form_close'] = form_close();
				$this->data['category'] = form_dropdown('category', $category, $category_id, 'id="category" class="select"');
				$this->data['ip'] = form_input('ip', $ip, 'id="ip" class="input" ');
				$this->data['port'] = form_input('port', $port, 'id="port" class="input"');
				$this->data['desc'] = form_textarea(array('name'=>'desc', 'rows'=>'8', 'cols'=>'40'), $desc, 'id="desc"  class="input" style="resize:none" ' );
				$this->data['hd'] = form_dropdown('hd', $hd, $hd_id, 'id="hd" class="select"');
				$this->data['iptv'] = form_dropdown('iptv', $iptv,$iptv_id, 'id="iptv" class="select"');
				$this->data['url'] = form_input('url', $url, 'id="url" style="border:0px;box-shadow:none" size="40" ');
				$this->data['img'] = form_upload('img_file');
				 
				$this->data['channel_no'] = form_input('channel_no', $channel_no, 'id="channel_no" onkeypress="return isNumberKey(event)" maxlength="4" class="input" ');
				$this->data['tag'] = form_dropdown('tag', $tags, $tag, 'id="tag"');

				if($id > 0) {
					$this->data['image_name'] = $image;
					$this->data['name'] = form_input('name', $name, 'id="name" class="input" ');
					$this->data['id'] = form_hidden('id', $id);
					$this->data['submit'] = form_submit('submit', 'Update', 'id="submit" class="primary btn" onclick="return chk_form()"');
				}
				else {
					$this->data['name'] = form_input('name', "", 'id="name" class="input"  ');
					$this->data['submit'] = form_submit('submit', 'Submit', 'id="submit" class="primary btn"');
				}

				if($id > 0) {
					$this->data['title'] = 'Edit Channel';
					$this->data['page_heading'] = 'Edit Channel';
				}
				else {
					$this->data['title'] = 'Add Channel';
					$this->data['page_heading'] = 'Add Channel';
				}

				$this->data['cancel'] = anchor('favourite', 'Cancel','id="cancel" class="bt"');
				$vars = $this->theme->theme_vars('pages/fav/main', $this->data, array('right'=>'inc/right'));
				$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}else{
			redirect('/digivalet_dashboard/login');
		}

	}


	function upload_image($name) {
		if($name == 'ipod_images') {
			$path = BASE_PATH.'/images/ipod_image/'.$_FILES[$name]['name'];
		}
		else {
			$path = BASE_PATH.'/images/ipad_image/'.$_FILES[$name]['name'];
		}

		if(move_uploaded_file($_FILES[$name]['tmp_name'], $path)) {
			exec("chmod -R 777 ".BASE_PATH.'/images/');
			return $_FILES[$name]['name'];
		}
	}


	// File Upload
	function do_upload($name)
	{
		// echo $_SERVER['DOCUMENT_ROOT']; die();
		//echo BASE_PATH.'/images/ipad_image/';
		$config['upload_path'] = BASE_PATH.'/images/ipad_image/';
		$config['allowed_types'] = 'png|jpeg|jpg|bmp';
		$config['max_size']	= '80000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($name))
		{
			$this->data['error'] = $this->upload->display_errors();
		}
		else
		{
			$this->data = array('upload_data' => $this->upload->data());
			//echo "<pre>"; print_r($this->data); die();
			$name = $this->data['upload_data']['file_name'];
			$ext = $this->data['upload_data']['file_ext'];
			return $name;
		}
	}


	function sort_save_cat() {
		$ids = $this->input->post('ids');
		foreach($ids as $key=>$id) {
			//echo "Key=> ".$key." Val=> ".$id." <br/>";
			//$this->fav_channels->update_tag($id, $key);
			$this->fav_category->update_position($key, $id);
		}

		echo 1;
	}

	function delete_channel($id,$cat='') {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			if($id > 0) {
				$name=$this->fav_channels->get_channel_name($id);
				$nam=$name->row();
				$name=$nam->chname;

				$this->watchdog->save('disable', 'Tv Channel', 'channel_disable',$id,$name);
				if($this->fav_channels->delete_channels($id)) {
					//redirect('favourite/message/6/favourite/channels');
					$ct=$this->fav_channels->find_tab($id);
					$na=$ct->row();
					$cid=$na->position;
					$this->session->set_flashdata("msg","Channel successfully removed.");
					$this->session->set_flashdata("css_class","success");
					if($cat==''){
						redirect("favourite#ui-tabs-".$cid);
						 
					}
					else{
						redirect("favourite");
					}
				}
				else {
					//redirect('favourite/message/7/favourite/channels');
					$this->session->set_flashdata("msg","Error in channel removing.");
					$this->session->set_flashdata("css_class","error");
					redirect("favourite");
				}
			}
			else {
				redirect('favorite');
			}
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	function unfav_to_fav($id) {
		if($id > 0) {
			if($this->fav_channels->unfavorites_to_favorites($id)) {
				//redirect('favourite/message/9/favourite_unfavorites/Unfavorites');
				$this->message->set("Channel successfully re-stored in favorite.", "success", TRUE);
				redirect("favourite/unfavorites");
			}
			else {
				//redirect('favourite/message/10/favourite_unfavorites/Unfavorites');
				$this->message->set("Error in channel redtoring.", "error", TRUE);
				redirect("favourite/unfavorites");
			}
		}
		else {
			redirect('favourite');
		}
	}


	function load_blob_image($data, $name, $ipad=FALSE) {
		if($ipad == TRUE) {
			$file = fopen($_SERVER['DOCUMENT_ROOT'].'/backend/images/ipad_image/'.$name.'.png', 'w');
		}
		else {
			//echo 'ya';die();
			$file = fopen($_SERVER['DOCUMENT_ROOT'].'/backend/images/ipod_image/'.$name.'.png', 'w');
		}

		fwrite($file, $data);
		fclose($file);

		//echo '<img src="'.base_url().'images/channels/'.$name.'.png" />'; die();
		return '<img src="'.BASE_URL.'images/ipad_image/'.$name.'.png" width="75" height="75"/>';
	}

	/*#################################    Category Functionality Start     ###########################################*/

	function category($id="") {
		$login_id=$this->session->userdata("user_id");
		if($login_id){

			$this->data = array();
			$q = $this->db->query("SELECT MAX(position) as pos FROM fav_cat");
			$row = $q->result();
			$this->data['query'] = $this->fav_category->get_all_category();
			$this->data['base_path'] = base_url();
			$name = '';
			$position = '';
			if($id > 0) {
				$this->data['id'] = form_hidden('id', $id);
				$this->data['submit'] = form_submit('submit', 'Update','class="primary btn"');
				$q = $this->fav_category->get_category($id);
				$row = $q->result();
				$name = $row[0]->category;
				$position = $row[0]->position;
			}
			else {
				$position = ($row[0]->pos)+1;
				$this->data['submit'] = form_submit('submit', 'Add','class="primary btn"');
			}

			$this->data['cat'] =$id;
			$this->data['last_type'] = form_hidden('last_type', $name);
			$this->data['name'] = form_input('name', $name, 'id="name" style="margin-top:5px" ');
			//$this->data['position'] = form_hidden('position', $position, 'id="position"');
			$this->data['title'] = 'Channel category';
			$this->data['page_heading'] = 'Channel category';

			$vars = $this->theme->theme_vars('pages/fav/category', $this->data, array('right'=>'inc/right'));
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);

		}else{
			redirect('/digivalet_dashboard/login');
		}

	}


	function sort() {

		$ids = $this->input->post("ids");
		$pos = explode(",", $ids);
		$i = 0;
		 
		foreach($pos as $qid) {
			$this->fav_category->update_position($i, $qid);
			$i++;
		}

	}

	function category_edit($id) {
		if($id > 0) {
			$this->category($id);
		}
		else {
			redirect('favourite/category');
		}
	}

	function category_save() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			$submit = $this->input->post("submit");
			$id = trim($this->input->post("id"));
			$name = trim($this->input->post("name"));
			//$position = trim($this->input->post("position"));
			$param = array();
			$param['name'] = $name;
			//$param['position'] = $position;
			$param['id'] = $id;
			$param['last_type'] = $this->input->post('last_type');

			$maxQuery = $this->fav_category->get_max_cat_id();
			$res = $maxQuery->result();
			$param['mid'] = $res[0]->id;

			if($id > 0) {
				if($this->fav_category->save_category($param)) {
					//redirect('favourite/category_message/3/favourite/category');
					$this->watchdog->save('edit', 'Tv Channel', 'channel_category_edit', $id,$name);
					$this->session->set_flashdata("Category successfully updated.", "success", TRUE);

					$this->message->set('Channel category successfully updated.', 'success', TRUE);

					redirect("favourite/category");
				}
				else {
					//redirect('favourie/category_message/4/favourite/category');
					$this->session->set_flashdata("Error in category updation.", "error", TRUE);

					$this->message->set('Error in category updation.', 'success', TRUE);

					redirect("favourite/category");
				}
			}
			else {
				$q = $this->fav_category->category_exist($param['name']);
				$row = $q->result();
				if(isset($row[0]->id) && $row[0]->id > 0) {
					//redirect('favourite/category_message/9/favourite/category');
					$this->session->set_flashdata("Category already exist.", "error", TRUE);
					redirect("favourite/category");

				}
				//echo $q=$this->fav_category->save_category($param);die();
				if($this->fav_category->save_category($param)) {
					//redirect('favourite/category_message/1/favourite/category');
					//  echo $id;echo $name; die();
					$this->watchdog->save('add', 'Tv Channel', 'channel_category_add',0,$name);
					//$this->message->set("Category successfully added.", "success", TRUE);
					 
					$this->message->set("Channel category successfully added.", "success", TRUE);

					redirect("favourite/category");
				}
				else {
					//redirect('favourite/category_message/2/favourite/category');
					$this->message->set("Error in category insertion.", "error", TRUE);
					redirect("favourite/category");
				}
			}

		}else{
			redirect('/digivalet_dashboard/login');
		}


	}

	function delete_category($id, $type) {
		if($id > 0) {
			//$type = base64_decode($type);
			$q = $this->fav_channels->get_category_channels($type);
			$row = $q->result();
			if(isset($row[0]->chid) && $row[0]->chid > 0) {

				foreach($row as $r){
					$this->fav_channels->del_channles($r->chname);

				}
				$this->fav_category->delete_category($id);
				//redirect('favourite/category_message/10/favourite/category');
				//              $this->message->set("This category is assigned to tv channels, before deleting it please remove the category from channels.", "error", TRUE);
				redirect("favourite/category");
			}
			else {
				$name=$this->fav_category->get_name($id);
				$nam=$name->row();
				$name=$nam->category;
				if($this->fav_category->delete_category($id)) {
					//redirect('favourite/category_message/6/favourite/category');
					$this->watchdog->save('delete', 'Tv Channel', 'channel_category_delete', $id,$name);
					$this->message->set("Channel category successfully deleted.", "success", TRUE);
					redirect("favourite/category");
				}
				else {
					//redirect('favourite/category_message/7/favourite/category');
					$this->message->set("Error in category deletion.", "error", TRUE);
					redirect("favourite/category");
				}
			}
		}
	}

	function ajax_check($channel_no, $id="") {
		//$this->db->where("Channel_No=".$channel_no);
		//$q = $this->db->get("fav");
		if($id > 0) {
			$sql = "SELECT * FROM fav WHERE Channel_No='".$channel_no."' AND chid<>".$id;
		}
		else {
			$sql = "SELECT * FROM fav WHERE Channel_No='".$channel_no."'";
		}
		$rs = $this->db->query($sql);
		//$row = $rs->result();
		//print_r($row);
		//$row = $q->result();
		$chtext = "";
		foreach($rs->result() as $row) {
			$chtext .= $row->chname."\n";
		}

		if($chtext != "") {
			echo $chtext;
		}
		else {
			echo "No";
		}

	}
	 
	public  function channel_no_check($channel,$cat=''){
		 
		//  echo 'xx';die();
		if($cat==''){
			$sql="select chname from fav where Channel_No='".$channel."' and visibility='1' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				if($ch!=''){
					$ch.="\n";
				}


				$ch.=$row->chname;
			}


			if($ch!=''){
				echo $ch;
			}
			else{
				echo 'No';
			}
			 
		}
		else {
			 
			$sql="select * from fav where Channel_No='".$channel."' and chid<>'".$cat."' and visibility='1' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				if($ch!=''){
					$ch.="\n";
				}
				 

				$ch.=$row->chname;
			}

			 
			if($ch!=''){
				echo $ch;
			}
			else{
				echo 'No';
			}

		}

	}


	public function channel_check($channel,$cat='') {
		 
		$channel=str_replace("%20", " ", $channel);
		if($cat==''){
			$sql="select * from fav where chname='".$channel."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){
				 
				$ch=$row->chname;
			}

			 
			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}
		}
		else {
			 
			$sql="select * from fav where chname='".$channel."' and chid<>'".$cat."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){
				 
				$ch=$row->chname;
			}

			 
			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}

		}

	}

	 
	function sort_save() {
		$ids = $this->input->post('ids');
		foreach($ids as $key=>$id) {
			//echo "Key=> ".$key." Val=> ".$id." <br/>";
			$this->fav_channels->update_tag($id, $key);
		}

		echo 1;
	}

	public  function category_check($channel='',$cat=''){
		 
		$channel=str_replace("%20", " ", $channel);
		if($cat==''){
			$sql="select * from fav_cat where category='".$channel."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				$ch=$row->category;
			}


			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}
		}
		else{
			$sql="select * from fav_cat where category='".$channel."' and id<>'".$cat."' ";
			$rs=$this->db->query($sql);
			$ch='';
			foreach($rs->result() as $row){

				$ch=$row->category;
			}


			if($ch!=''){
				echo 'yes';
			}
			else{
				echo 'No';
			}
			 
		}
	}

	public function generate() {
		$login_id=$this->session->userdata("user_id");
		if($login_id){
			exec("sh ".BASE_PATH."/sqlite/mysql_sqlite backend");
			exec("sudo chmod -R 777 ".$_SERVER['DOCUMENT_ROOT']);
			rename(BASE_PATH."/backend.sqlite", FAV_DB_PATH);
			$this->drope_tables();
			//echo FAV_DB_PATH; echo '-----'; echo $_SERVER['DOCUMENT_ROOT'].'/html/'.FAV_DB_NAME; die();
			if (copy(FAV_DB_PATH , $_SERVER['DOCUMENT_ROOT'].'/'.FAV_DB_NAME)) {
				exec("sudo chmod -R 777 ".$_SERVER['DOCUMENT_ROOT'].'/'.FAV_DB_NAME);
				$this->watchdog->save('Update', 'Tv Channel', 'channel_update', 0,'Changes commited');
				$this->message->set("Database successfully generated.", "success", TRUE);
			}
			else {
				$this->message->set("Error in database generation.", "error", TRUE);
			}

			// send_request(TV_CHANNEL_PUSH_UPDATE);

			redirect("favourite");
		}else{
			redirect('/digivalet_dashboard/login');
		}
	}

	public function  drope_tables() {
		$tbls = "`dashboard_action_log`, `dashboard_alert`, `dashboard_feature`, `dashboard_module`, `dashboard_module_feature`, `dashboard_parameter`, `dashboard_role`, `dashboard_role_module`, `dashboard_ui_preference`, `dashboard_user`, `dashboard_user_attends_alert`, `dashboard_user_preference`, `dashboard_user_role`, `departments`, `digivalet_services`, `digivalet_status`, `digivale_addons`, `floors`, `floor_types`, `food_addons_master`, `food_addon_group_master`, `food_application`, `food_application_images`, `food_languages`, `food_locale`, `food_maincategory`, `food_menuitem_addons`, `food_menuitem_allowed_addons`, `food_menuitem_details`, `food_menuitem_groups`, `food_menuitem_price_detail`, `food_menuitem_primary`, `food_order_details`, `food_order_info`, `food_order_status`, `food_restaurant`, `food_restaurant_details`, `food_restaurant_rvc`, `food_revenue_primary`, `food_section`, `food_subcategory`, `guest_status`, `menu`, `menu_links`, `notification_log`, `permission`, `perms`, `role`, `rooms`, `rooms_message`, `room_types`, `sdirectory`, `service_notification_emails`, `service_notification_sms_no`, `staff_info`, `system_vars`, `users`, `users_roles`, `watchdog`, `welcome_letters`, `welcome_letters_q`, `welcome_letter_opera`, `welcome_letter_types`";
		$str = str_replace('`','', $tbls);
		$tables = explode(',', $str);

		$pdo = new PDO('sqlite:'.FAV_DB_PATH);
		foreach($tables as $table) {
			$pdo->exec("DROP TABLE ".trim($table));
		}


		$q = $this->common_model->get_records('fav');
		//print_r($q->result());
		$i=0;
		foreach($q->result() as $row) {
			echo $i++; echo "<br/>";
			$fp = fopen(FAV_IMAGE.$row->chname.'.png','rb');
			$image = fread($fp, filesize(FAV_IMAGE.$row->chname.'.png'));
			fclose($fp);

			$sql = "UPDATE fav SET iPad_image=?, chfile=? WHERE chname = '".$row->chname."'";
			$q = $pdo->prepare($sql);
			$q->bindParam(1, $image, PDO::PARAM_LOB);
			$q->bindParam(2, $image, PDO::PARAM_LOB);
			$q->execute();
		}
		$pdo->exec("alter table fav RENAME TO fav");
		$pdo->exec("alter table fav_cat RENAME TO fav_cat");
		$pdo->exec("VACUUM");

	}


}
?>
