<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author pbsl
 */
class login extends CI_Controller {
	var $data;
	var $CI;
	public function __construct() {
		parent::__construct();
		$this->load->model("login_model");
		$this->load->library('autocontact');
		$this->data['title'] = 'User Login';
	}

	public function index() {
		$uid = $this->user->get_user_info('uid');
		if($uid != "" && $uid > 0) {
			redirect('home');
		}
		$attributes = array('name'=>'login', 'method'=>'post');
		$this->data['form_open'] = form_open('login/authentication', $attributes);
		$this->data['form_close'] = form_close();
		$this->data['name'] = form_input('name', '', 'id="edit-name" autocomplete="off" placeholder="Username" style="width:280px; height:32px; font-weight:bold; padding-top:5px;"');
		$this->data['password'] = form_password('password', '', 'id="edit-password" placeholder="Password" style="width:280px; height:32px; font-weight:bold; padding-top:5px;"');
		$this->data['submit'] = form_submit('submit', 'Login', 'id="edit-submit" class="button" ');
		$this->data['forgot_password'] = anchor('login/forgot_password', 'Forgot password/username');
		$this->data['page_heading'] = 'User Login';

		$vars = $this->theme->theme_vars('pages/login_page', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	}

	public function authentication() {
		if($this->input->post('name')) {
			$uname = $this->input->post('name');
			$pass = $this->user->encode_password($this->input->post('password'));
			$q = $this->login_model->auntenticate($uname, $pass);
			$res = $q->result_array();
			if(count($res) > 0 && $res[0]['uid'] > 0) {
				$uid = $res[0]['uid'];
				 
				//Updating account access time
				$this->login_model->update_account_access_time($uid);
				$user_info = array();
				$permissions = array();
				 
				$roles_query = $this->user_model->user_roles($uid);
				$roles_res = $roles_query->result_array();
				$roles = object_to_array($roles_res);
				if(count($roles) > 0) {
					$tmp_roles = implode(",", array_keys($roles));
					$permissions_query = $this->user_model->user_permission($tmp_roles);
					$permissions_res = $permissions_query->result_array();
					$permissions = permissions_to_array($permissions_res);
				}

				$user_info[] = $res[0];
				$user_info['roles'] = $roles;
				$user_info['perm'] = $permissions;
				//echo "<pre>"; print_r($user_info); die();
				 
				$this->user->set_user_info($user_info);
				$this->message->set('Login successfull.', 'success', TRUE);
				echo 1;
				//redirect('welcomletter/send_letter');
			}
			else {
				//$this->message->set('Invalid username or password.', 'error', TRUE);
				//redirect('login');
				echo 0;
			}
			 
		}
		else {
			$this->message->set('Username and password is required.', 'error', TRUE);
			redirect('login');
		}
	}


	public function forgot_password() {
		$this->data['title'] = 'Recover your password';
		$this->data['page_heading'] = 'Recover your password';
		$this->data['login'] = anchor('login', 'Login');
		$this->data['name'] = form_input('name', '', 'id="edit-name"');
		$this->data['submit'] = form_submit('submit', 'Send', 'id="edit-submit"');
		$this->data['form_open'] = form_open('login/send');
		$this->data['form_close'] = form_close();
		$this->data['help_text'] = 'Please enter your username or email and click send button, your account details will send on your mail account';
		$vars = $this->theme->theme_vars('pages/forgot_password_view', $this->data);
		$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
	}

	public function send() {
		$name = $this->input->post('name');
		$found = FALSE;
		if($name) {
			$q = $this->common_model->get_records_by_field('users', 'name', $name);
			$rs = $q->result_array();
			if(!isset($rs[0])) {
				$q = $this->common_model->get_records_by_field('users', 'mail', $name);
				$rs = $q->result_array();

				if(!isset($rs[0])) {
					$this->message->set("Invalid username or e-mail", "error", TRUE);
					redirect('login/forgot_password');
				}
				else {
					$found = TRUE;
				}
			}
			else {
				$found = TRUE;
			}

			// Sending Email
			if($found == TRUE) {
				$username = $rs[0]['name'];
				$pass = $this->user->decode_password( $rs[0]['pass'] );
				$email = $rs[0]['mail'];
				$subject = 'Password recovery';
				$body = 'Hello '.$username.'/ \n';
				$body .= 'Your password is '.$pass;

				$message['from_name'] = 'Logmanagement';
				$message['from_email'] = 'support@gmail.com';
				$message['to'] = $email;
				$message['message'] = $body;

				if($this->autocontact->send_mail($message)) {
					$this->message->set('Password has been sent on your email account.', 'success', TRUE);
					redirect('login');
				}
				else {
					$this->message->set('Error in sending password, please try later.', 'error', TRUE);
					redirect('login/forgot_password');
				}
			}

		}
		else {
			$this->message->set("Please enter username or e-mail.", "error", TRUE);
			redirect('login/forgot_password');
		}
	}

	public function logout() {
		$this->user->unset_user_info();
		$this->message->set('You are logout successfully.', 'success', TRUE);
		redirect('login');
	}

}// End Of class
?>