<?php
/************************************************************************************************************************************************
 * 																																			*
 * 											Project Name	: Digivalet Social Network														*									*
 * 											Author			: Paras Sahu		 															*
 * 											Creation Date	: 4 July 2013 - 22 July 2013													*										*
 * 											Snippet Name	: DSN Model																		*
 **************************************************************************************************************************************************/
Class Dsn_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getCIinstance()
	{
		$ci =& get_instance();
		$db_dsn = $ci->load->database('local', TRUE);
		return $db_dsn;
	}
	
	function uploadStatusImageProcess()
	{
		$targetFolder 					= 		getcwd().'/upload/dsn';
		$verifyToken 					= 		md5('unique_salt' . $this -> input -> post('timestamp'));
		
		
		if (!empty($_FILES) && $this -> input -> post('token') == $verifyToken)
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

			// Validate the file type
			$fileTypes = array('jpg','jpeg','png'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);

			if (in_array($fileParts['extension'],$fileTypes))
			{
				move_uploaded_file($tempFile,$targetFile);
				
				$this->load->library('image_lib', $this -> getImageResizeParams($targetFolder.'/'.$_FILES['Filedata']['name'], 
												  $targetFolder.'/iPad/nonRetina/', 
												  100, 
												  100));
				$this->image_lib->resize();
				$this->image_lib->initialize($this -> getImageResizeParams($targetFolder.'/'.$_FILES['Filedata']['name'], 
																				$targetFolder.'/iPad/retina/', 
																				200, 
																				200));
				$this->image_lib->resize();
				echo $this->image_lib->display_errors();
				echo '1';
				return true;
			}
			else
			{
				echo 'Invalid file type.';
			}
		}
	}
	
	function getImageResizeParams($source_image, $new_image, $width, $height)
	{
		$config['source_image']			= 		$source_image;
		$config['new_image']			=		$new_image;
		$config['image_library'] 		= 		'gd2';
		$config['create_thumb'] 		= 		TRUE;
		$config['maintain_ratio'] 		= 		TRUE;
		$config['thumb_marker']			=		false;
		$config['width']	 			= 		$width;
		$config['height']				= 		$height;
		return $config;
	}
	
	function uNameDetails($uid)
	{
		$db_dsn = $this -> getCIinstance();
		$userDetails = $db_dsn -> get_where('users', array('uid' => $uid));

		if($userDetails -> num_rows() > 0)
		{
			$result = $userDetails -> row_array();
			if(isset($result))
			{
				return $result;
			}
		}
		else
		{
			echo 'User has been deleted';
			exit;
		}
	}
	
	function setUserStatus($uid)
	{
		$db_dsn = $this -> getCIinstance();
		$updated = $db_dsn -> query("UPDATE `users` SET `status`=NOT(`status`) WHERE uid='$uid'");
		echo $updated;
	}
	
	function mark_as_inappropriate($mode, $id)
	{
		if($mode=="post")
		{
			$table_name = "messages";
			$where_condition  = "msg_id";
			$column_name = "inappropriate_p";
		}
		else if($mode=="comment")
		{
			$table_name = "comments";
			$where_condition  = "com_id";
			$column_name = "inappropriate";
		}
		
		$db_dsn = $this -> getCIinstance();
		$updated = $db_dsn -> query("UPDATE ".$table_name." SET `$column_name`=NOT(`$column_name`) WHERE ".$where_condition."=".$id."");
		echo $updated;
	}
	
	function generateAnonymousUser($guestId, $displayName)
	{
		$db_dsn = $this -> getCIinstance();
		$user_data = $this -> getAnonymousUserImage();
		/**/
		$data = array(
						'username'		=>	ucfirst(urldecode($displayName)),
						'password'		=>	'digivalet',
						'user_image'	=>	$user_data['image_name'],
						'status'		=>	1,
						'guest_id'		=>	$guestId
		);
		$db_dsn -> insert('users', $data);
		$uid = $db_dsn -> insert_id();
		$this -> updateAnonymousUser($uid, $user_data['aui_id']);
		return $uid; 
		/**/
	}
	
	function getAnonymousUserImage()
	{
		$db_dsn = $this -> getCIinstance();
		$db_dsn -> select('*');
		$db_dsn -> from('anony_user_image');
		$db_dsn -> where('status', 0);
		$db_dsn -> limit(1);
		$db_dsn -> order_by('aui_id', 'asc');
		$query = $db_dsn -> get();
		if($query -> num_rows() > 0)
		{
			$result = $query -> row_array();
			return $result;
		}
		else 
		{
			$db_dsn -> select('*');
			$db_dsn -> from('anony_user_image');
			$db_dsn -> where('status', 2);
			$query = $db_dsn -> get();
			if($query -> num_rows() > 0)
			{
				$result = $query -> row_array();
				return $result['image_name']; 
			}
			else
			{
				return NULL;
			}
		}
	}
	
	function updateAnonymousUser($userId, $aui_id)
	{
		$db_dsn = $this -> getCIinstance();
		$data = array(
		'status'	=>	1,
		'uid'		=>	$userId
		);
		$db_dsn -> where(array('aui_id' => $aui_id));
		return $db_dsn -> update('anony_user_image', $data);
	}
	
	function guestCheckout($userId)
	{
		$db_dsn = $this -> getCIinstance();
		
		/**/
		$dataAnony = array(
		'status'	=>	0,
		'uid'		=>	NULL
		);
		$db_dsn -> where(array('uid'	=>	$userId));
		if($db_dsn -> update('anony_user_image', $dataAnony))
		{
			/**/
			$dataUser = array(
			'status'		=>	0	
			);
			$db_dsn -> where(array('uid' => $userId));
			return $db_dsn -> update('users', $dataUser);
			/**/
		}
	}
}
