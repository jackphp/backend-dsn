<?php
/**
 * Description of room_management_model
 *
 * @author pbsl
 */
class room_management_model extends CI_Model {

	var $db;

	public function  __construct() {
		parent::__construct();
		$this->db = $this->load->database('reports', TRUE);
	}

	public function save_floor($param) {
		if(isset($param['fid']) && $param['fid'] > 0) {
			$sql = "UPDATE floors SET fname='".$param['name']."' WHERE fid='".$param['fid']."'";
			return $this->db->query($sql);
		}
		else {
			$sql = "INSERT INTO floors (fname)VALUES('".$param['name']."')";
			return $this->db->query($sql);
		}
	}

	public function load_floors() {
		$sql = "SELECT r.room_id, r.room_name, f.fname, f.fid FROM rooms AS r LEFT JOIN floor_rooms AS fr ON r.room_id = fr.room_id LEFT JOIN floors AS f ON f.fid = fr.fid";
		return $this->db->query($sql);
	}

	public function save_room($param) {
		//echo " <pre>"; print_r($param); die();
		if(isset($param['room_id']) && $param['room_id'] > 0 ) {
			if(!$this->room_is_exist_in_floor_room($param['room_id'])) {
				$this->save_floor_room($param);
			}

			$param['update'] = 1;
			$this->room_status_save($param);
			$sql = "UPDATE rooms SET room_name = '".$param['name']."' WHERE room_id = ".$param['room_id'];
			return $this->db->query($sql);

		}
		else {
			$sql = "INSERT INTO rooms(room_name)VALUES('".$param['name']."')";
			$this->db->query($sql);
			$room_id = $this->db->insert_id();
			$param = array('room_id'=>$room_id, 'maintenance'=>$param['maintenance']);
			$this->save_floor_room($param);
			$this->room_status_save($param);
			return $room_id;
		}
	}

	public function save_floor_room($param, $insert = TRUE) {
		if($insert) {
			$sql = "INSERT INTO floor_rooms (fid, room_id)VALUES(".$param['fid'].", ".$param['room_id'].")";
			return $this->db->query($sql);
		}
		else {
			$sql = "UPDATE floor_rooms SET fid=".$param['fid']." WHERE room_id = ".$param['room_id'];
			return $this->db->query($sql);
		}
	}

	public function room_is_exist_in_floor_room($room_id) {
		$sql = "SELECT * FROM floor_rooms WHERE room_id = ".$room_id;
		$q = $this->db->query($sql);
		foreach($q->result() as $row) {
			return $row->room_id;
		}
	}

	public function get_room($room_id) {
		$sql = "SELECT r.room_id, fr.fid, r.room_name, info.* FROM floor_rooms AS fr LEFT JOIN rooms AS r ON fr.room_id = r.room_id LEFT JOIN room_status_info AS info ON info.room_id=r.room_id WHERE fr.room_id=".$room_id;
		return $this->db->query($sql);
	}

	public function load_floor_rooms($floor_id) {
		$sql = "SELECT r.room_id, r.room_name, f.fname, f.fid FROM rooms AS r LEFT JOIN floor_rooms AS fr ON r.room_id = fr.room_id LEFT JOIN floors AS f ON f.fid = fr.fid WHERE fr.fid=".$floor_id;
		return $this->db->query($sql);
	}

	// Inserting room_id in room_status_info table
	public function room_status_save($param) {
		if(isset($param['update']) && $param['update'] > 0) {
			$sql = "UPDATE room_status_info SET maintenance = ".$param['maintenance']." WHERE room_id = ".$param['room_id'];
		}
		else {
			$sql = "INSERT INTO room_status_info(room_id, maintenance)VALUES(".$param['room_id'].", ".$param['maintenance'].")";
		}

		return $this->db->query($sql);
	}

}// End of class
?>
