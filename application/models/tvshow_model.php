<?php
/**
 * Description of tvshow_model
 *
 * @author pbsl
 */

class tvshow_model extends CI_Model {

	public function get_tvshow_cat($offset=0, $limit=60) {
		$sql = "SELECT * FROM vd_playlist_cat ORDER BY name LIMIT $offset, $limit";
		return $this->db->query($sql);
	}

	public function get_tvshow_subcat($show_id=0, $offset=0, $limit=1) {
		$sql = "SELECT subcat.* FROM vd_playlist_subcat AS subcat LEFT JOIN vd_playlist_songs AS l ON subcat.id = l.cat_id LEFT JOIN vd_song AS s ON s.vd_song_code=l.vd_song_id WHERE subcat.cat_id=".$show_id."  GROUP BY subcat.name HAVING COUNT(s.vd_song_code) > 0  ORDER BY name LIMIT $offset, $limit ";
		return $this->db->query($sql);
	}

	public function get_tvshow_subcat_search_result($show_id=0, $q="") {
		$sql = "SELECT subcat.* FROM vd_playlist_subcat AS subcat LEFT JOIN vd_playlist_songs AS l ON subcat.id = l.cat_id LEFT JOIN vd_song AS s ON s.vd_song_code=l.vd_song_id WHERE subcat.cat_id=".$show_id." AND LOWER(s.vd_song_title) LIKE '$q%' GROUP BY subcat.name";
		return $this->db->query($sql);
	}

	public function count_tv_show_subcat($cat_id=0) {
		$sql = "SELECT id  FROM vd_playlist_subcat WHERE cat_id=".$cat_id;
		$q = $this->db->query($sql);
		$i=1;

		foreach($q->result() as $row) {
			$q2 = $this->get_tvshow($row->id);
			if($q2->num_rows() > 0) {
				$i++;
			}
		}

		return ($i-1);
	}

	public function get_tvshow($subcat_id=0) {
		$sql = "SELECT s.*, l.id, l.cat_id FROM vd_song AS s LEFT JOIN vd_playlist_songs AS l ON s.vd_song_code=l.vd_song_id WHERE l.cat_id=".$subcat_id;
		return $this->db->query($sql);
	}

	public function tvshow_save($param = array(), $sid=0) {
		if($sid > 0) {
			$this->db->where(array('vd_song_code'=>$sid));
			$this->db->update('vd_song', $param);
			return $sid;
		}
		else {
			$sql = "INSERT INTO vd_song(vd_song_title, vd_song_genre, lang, keywords, vd_song_image, music_director, lyricist, rating, entry_date, cast, synopsis)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function tvshow_in_list($param = array()) {
		$sql = "INSERT INTO vd_playlist_songs(cat_id, vd_song_id )VALUES(?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function search_tvshow($q="") {
		if($q != "") {
			$sql = "SELECT * FROM vd_playlist_cat WHERE LOWER(name) LIKE '".strtolower($q)."%'";
			return $this->db->query($sql);
		}
	}

	public function search_episode($subcat_id=0, $q=0) {
		$sql = "SELECT subcat.*, s.* FROM vd_playlist_subcat AS subcat LEFT JOIN vd_playlist_songs AS l ON subcat.id = l.cat_id LEFT JOIN vd_song AS s ON s.vd_song_code=l.vd_song_id WHERE subcat.cat_id=".$subcat_id." AND LOWER(s.vd_song_title) LIKE '".strtolower($q)."%'";
		return $this->db->query($sql);
	}

	public function save_category($param = array()) {
		$sql = "INSERT INTO vd_playlist_cat(name, position)VALUES(?,?)";
		return $this->db->query($sql, $param);
	}

	public function save_subcategory($param = array()) {
		$sql = "INSERT INTO vd_playlist_subcat(cat_id, name)VALUES(?,?)";
		return $this->db->query($sql, $param);
	}


	public function get_tvshow_category($mid=0) {
		$sql = "SELECT cat.id AS cat_id, cat.name AS cat_name, subcat.id AS subcat_id, subcat.name AS subcat_name FROM vd_playlist_songs AS ml LEFT JOIN vd_playlist_subcat AS subcat ON ml.cat_id = subcat.id LEFT JOIN vd_playlist_cat AS cat ON cat.id = subcat.cat_id  WHERE ml.vd_song_id = ".$mid;
		return $this->db->query($sql);
	}

	public function delete_tvshow_list($mid=0) {
		$sql = "DELETE FROM vd_playlist_songs WHERE vd_song_id = ".$mid;
		return $this->db->query($sql);
	}
}
?>