<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pmsi_model
 *
 * @author pbsl
 */
class pmsi_model extends CI_Model {

	var $pmsi;

	public function  __construct() {
		parent::__construct();
		$this->pmsi = $this->load->database("iremote", TRUE);
	}

	public function get_guest_name($room_no=0) {
		$sql = "SELECT guestname FROM pmsi_guest WHERE roomno='".$room_no."' and sharestatus='primary' ";
		$q = $this->pmsi->query($sql);
		if($q->num_rows() > 0) {
			$row = $q->result();
			return $row[0]->guestname;
		}
		else {
			return 0;
		}
	}

}
?>
