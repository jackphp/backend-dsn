<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of food_addon_model
 *
 * @author pbsl
 */
class group_allowed_addon_model extends  CI_Model {

	public function get_addons($offset=0, $limit=30) {
		$sql = "SELECT * FROM food_allowed_addon_group_master ORDER BY position LIMIT $offset, $limit";
		return $this->db->query($sql);
	}

	public function update_position($i, $qid){
		if($qid!=''){
			$sql="update food_allowed_addon_group_master set position='".$i."' where gid='".$qid."'";
			return  $this->db->query($sql);
		}
	}

	public function update_addon_position($i, $qid){
		if($qid!=''){
			$sql="update food_allowed_addons_master set position='".$i."' where addon_id='".$qid."'";
			return  $this->db->query($sql);
		}
	}

	public function get_family_addon_name($id){
		$sql="select display_name from food_allowed_addons_master where gid='".$id."'";
		return $this->db->query($sql);
	}
	 
	public function del_addons($id){
		$sql="delete from food_allowed_addons_master WHERE gid='".$id."'";
		return $this->db->query($sql);
		 
	}

	public function get_addon_name($id){
		$sql="select display_name from  food_allowed_addon_group_master where gid='".$id."' ";
		return $this->db->query($sql);
	}
	 

	public function get_list_addons($id) {
		$sql = "SELECT * FROM food_allowed_addons_master where gid='".$id."'  ORDER BY position ";
		return $this->db->query($sql);
	}

	public function update_group($i, $qid){
		if($qid!=''){
			$sql="update food_allowed_addon_group_master set position='".$i."' where gid='".$qid."'";
			return  $this->db->query($sql);
		}
	}


	public function add_group($param = array(), $id=0) {
		if($id > 0) {
			$this->db->where(array('gid'=>$id));
			$this->db->update('food_allowed_addon_group_master', $param);
			return $id;
		}
		else {

			$sql="select MAX(position)as position from food_allowed_addon_group_master";
			$max=$this->db->query($sql);
			$res=$max->row();
			$r=$res->position;
			$r=$r+1;
			$param['position']=$r;
			$sql = "INSERT INTO food_allowed_addon_group_master(group_code, group_name, display_name,position)VALUES(?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function save_locale($param = array()) {
		$sql = "INSERT INTO food_locale (lang_code, type, type_id, name, description) VALUES (?,?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function save_menuitem_addons($apram = array()) {
		$sql = "INSERT INTO food_allowed_menuitem_addons(addon_id, mid, gid, addon_code,  addon_name, addon_size, addon_price)VALUES(?,?,?,?,?,?,?)";
		return $this->db->query($sql, $apram);
	}

	public function delete_addon($id=0) {
		//        $sql = "DELETE FROM food_locale WHERE type = 'addon' and type_id = $id";
		//        $this->db->query($sql);
		//
		//        $sql = "DELETE FROM food_application_images WHERE item_type = 'addon' and item_type_id  = $id";
		//        $this->db->query($sql);

		$sql = "DELETE FROM food_allowed_addon_group_master WHERE gid = $id";
		return $this->db->query($sql);
	}
}
?>
