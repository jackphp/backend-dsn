<?php

class User_Form extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}
	public function module_form()
	{

		$this->load->view('user_pages/module_view.php');
	}

	public function module_insert()
	{

		$module_name = $this->input->post('module_name');
		$module_url = $this->input->post('module_url');
		$this->load->model('user_feature_model');
		$result = $this->user_feature_model->insert_user_module($module_name,$module_url);
		redirect('user_form/feature_form');
	}
	public function feature_form(){

		$this->load->model('user_feature_model');
		$result = $this->user_feature_model->get_module();
		$data['module_list']=$result;
		$this->load->view('user_pages/feature_form_view.php',$data);
	}
	public function feature_insert()
	{
		print_r($_POST);('cont');
		$this->load->model('user_feature_model');
		$result = $this->user_feature_model->insert_user_feature($_POST);
		redirect('user_form/feature_form');
	}
}
?>

