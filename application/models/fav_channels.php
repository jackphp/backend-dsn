<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fav_favorites
 *
 * @author pbsl
 */
class fav_channels extends CI_Model {
	public function  __construct() {
		parent::__construct();
	}

	public function get_all_channels() {
		$sql = "SELECT * FROM fav WHERE visibility = 1 ORDER BY tag";
		return $this->db->query($sql);
	}

	public function get_channel_count() {
		$sql = "SELECT * FROM fav ORDER BY tag";
		return $this->db->query($sql);
	}


	public function get_cat_channels($cat_id=0) {
		$sql = "SELECT f.* FROM fav AS f LEFT JOIN fav_cat AS c ON f.Type = c.category WHERE c.id=".$cat_id." AND visibility=1 ORDER BY tag";
		return $this->db->query($sql);
		 
	}

	//   public function get_channel_count() {
	//       $sql = "SELECT tag, chfile, chid, chname FROM fav WHERE visibility = 1";
	//       return $this->db->query($sql);
	//   }

	public function get_unfav($data){
		if($data!='0'){
			$sql="select * from fav where chname LIKE '".$data."%' AND visibility=0 ORDER BY tag   ";
		}
		else{
			$sql="select * from fav where visibility=0 ORDER BY tag ";
		}
		$sql = str_replace(array("%20"), " ", $sql);
		return  $this->db->query($sql);
	}
	 
	public function get_selected($data,$cat){
		if($data!='0'){
			if($cat=='All'){
				$sql="select * from fav where chname LIKE '".$data."%' AND visibility=1 ORDER BY tag   ";
			}
			else{
				$sql="select * from fav where chname LIKE '".$data."%' and Type='".$cat."' AND visibility=1 ORDER BY tag  ";
			}
		}
		else{
			if($cat=='All'){
				$sql="select * from fav where visibility=1 ORDER BY tag ";
			}
			else{
				$sql="select * from fav where  Type='".$cat."' AND visibility=1 ORDER BY tag ";
			}

		}
		$sql = str_replace(array("%20"), " ", $sql);
		return  $this->db->query($sql);
	}
	 
	 
	 
	 
	public function get_channels($id) {
		$sql = "SELECT * FROM fav WHERE chid = ".$id;
		return $this->db->query($sql);
	}

	public function get_category_channels($category) {
		$sql = "SELECT * FROM fav WHERE Type = '".$category."'";
		return $this->db->query($sql);
	}

	public function save($param, $id=0) {
		if($id > 0) {
			//echo "<pre>";  print_r($param); die();
			$this->db->where(array('chid'=>$id));
			return $this->db->update("fav", $param);
		}
		else {
			$sql = "INSERT INTO fav(chname, chfile, tag, Channel_No, visibility, Type, iPad_image, epgfilename, channel_code,channel_text,url,port,ip,is_hd,is_iptv)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}

	}

	public function find_tab($cat){
		$sql="select a.position from fav_cat as a,fav as b where a.category=b.type and b.chid='".$cat."'";
		return $result= $this->db->query($sql);

	}

	public function saveBulkChannel($param) {
		$conn = new PDO("sqlite:/var/www/tv_channels/settings.sqlite");
		$sql = "INSERT INTO fav(chname, chfile, Channel_No, iPad_image)VALUES(?,?,?,?)";
		$q = $conn->prepare($sql);
		$q->bindParam(1, $param['chname']);
		$q->bindParam(2, $param['chfile'], PDO::PARAM_LOB);
		$q->bindParam(3, $param['Channel_No']);
		$q->bindParam(4, $param['iPad_image'], PDO::PARAM_LOB);
		$q->execute();
	}

	public function get_unfavorit_channels() {
		$sql = "SELECT * FROM fav WHERE visibility = 0 ORDER BY latest DESC";
		return $this->db->query($sql);
	}

	public function unfavorites_to_favorites($id) {
		$query = $this->db->query('SELECT max(tag) as maxid FROM fav');
		$row = $query->row();
		$cnt = $row->maxid+1;
		$sql = "UPDATE fav SET visibility = 1, tag=$cnt WHERE chid=".$id;
		return $this->db->query($sql);
	}

	public function delete_channels($id) {
		//$this->db->query("UPDATE fav SET tag=1 WHERE tag=0");
		$sql = "UPDATE fav SET visibility = 0, tag = '',latest=CURRENT_TIMESTAMP WHERE chid=".$id;
		return $this->db->query($sql);
	}

	public function get_channel_name($id){
		$sql="select chname from fav where chid='".$id."' ";
		return $this->db->query($sql);

	}
	public function update_tag($id, $tag) {
		$sql = "UPDATE fav SET tag=".$tag." WHERE chid=".$id;
		return $this->db->query($sql);
	}

	public function get_max_position() {
		$query = $this->db->query('SELECT max(tag) as maxid FROM fav');
		$row = $query->row();
		$cnt = $row->maxid+1;
		return $cnt;
	}

	public function del_channles($r){
		$sql="update fav set visibility= 0 where chname='".$r."'";
		$this->db->query($sql);
	}
}
?>
