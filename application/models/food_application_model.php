<?php
/**
 * Description of food_application_model
 *
 * @author pbsl
 */
class food_application_model extends CI_Model {
	public function  __construct() {
		parent::__construct();
	}

	###################  Restaurant Start  #####################
	//                    <div id="wide"><div style="background-color:#c59e2f;width:463px;height:630px">
	//        <p><img src="images/page1.png" style="padding-top:70px;padding-left:115px;" width="200" height="200" /> </p> </br>
	//                   <span style="padding-left:135px;">ENTER A WORLD OF</span></br>
	//                   <span style="padding-left:125px;"> LUXURY AND LEISURE</span></br>
	//        <p style="padding-top:50px;padding-left:45px;"><img src="images/oberoig3.png" style="padding-top:40px" width="350" height="85" /> </p>
	//
	//
	//
	//
	//
	//
	//
	//      </div>
	//





	public function get_restaurant_list($id=0) {
		if($id > 0) {
			$sql = "SELECT r.*, d.* FROM food_restaurant AS r LEFT JOIN food_restaurant_details AS d ON r.restaurant_id = d.restaurant_id WHERE r.restaurant_id = ".$id;
		}
		else {
			$sql = "SELECT r.*, d.* FROM food_restaurant AS r LEFT JOIN food_restaurant_details AS d ON r.restaurant_id = d.restaurant_id";
		}

		return $this->db->query($sql);
	}

	public function save_menuitem_dump($param = array()) {
		$sql = "INSERT INTO food_menuitem_details(mid, menuitem_name, display_name, section_id, maincategory_id, sub_cat_id)VALUES(?,?,?,?,?,?)";
		$this->db->query($sql, $param);
	}

	public function save_restaurant($param=array(), $id=0) {
		if($id > 0) {
			//$this->db->where(array('restaurant_id'=>$id));
			//$this->db->update('food_restaurant', $param['primary']);

			$this->db->where(array('restaurant_id'=>$id));
			$this->db->update('food_restaurant_details', $param['details']);

			return $id;
		}
		else {
			$sql = "INSERT INTO food_restaurant(restaurant_name, modified, is_active)VALUES(?,?,?)";
			$this->db->query($sql, $param['primary']);
			$rid = $this->db->insert_id();
			if($rid > 0) {
				$sql = "INSERT INTO food_restaurant_details(restaurant_id, description, video, restaurant_name, is_active, modified)VALUES($rid, '".$param['details']['description']."', '".$param['details']['video']."', '".$param['details']['name']."', '".$param['details']['is_active']."', '".time()."')";
				$this->db->query($sql);
			}

			return $rid;
		}
	}

	public function save_restaurant_rvc($restaurant_id, $rvc_number) {
		$sql = "INSERT INTO food_restaurant_rvc(restaurant_id, rvc_id, rvc_number)VALUES(?,?,?)";
		$q = $this->common_model->get_records_by_field("food_revenue_primary", "rvc_number", $rvc_number);
		$row = $q->result();
		return $this->db->query($sql, array($restaurant_id, $row[0]->rvc_id, $rvc_number));
	}

	public function delete_restaurant($id){
		$sql="Delete from food_restaurant where  restaurant_id=".$id;
		return $this->db->query($sql);
	}

	public function delete_restaurant_rvc($restaurant_id) {
		$sql = "DELETE FROM food_restaurant_rvc WHERE restaurant_id=".$restaurant_id;
		return $this->db->query($sql);
	}

	###################  Revenue Start  #####################
	public function get_revenue_list($id=0) {
		if($id > 0) {
			$sql = "SELECT * FROM food_revenue_primary WHERE rvc_id = ".$id;
		}
		else {
			$sql = "SELECT * FROM food_revenue_primary";
		}

		return $this->db->query($sql);
	}

	public function save_revenue($param=array(), $id=0) {
		if($id > 0) {
			if(isset($param['primary'])) {
				$this->db->where(array('rvc_id'=>$id));
				$this->db->update('food_revenue_primary', $param['primary']);
			}

			//echo "<pre>"; print_r($param['details']); die();
			//            if(isset($param['details'])) {
			//                $this->db->where(array('rvc_id'=>$id));
			//                $this->db->update('food_revenue_details', $param['details']);
			//            }

			return $id;
			}
			else {
				$sql = "INSERT INTO food_revenue_primary(rvc_number, rvc_name, is_changed, modified, created, is_active)VALUES(?,?,?,?,?,?)";
				$this->db->query($sql, $param['primary']);
				$rv_id = $this->db->insert_id();

				//            if($rv_id > 0) {
				//                $detailsArr = array($rv_id, $param['details']['description'], $param['details']['video']);
				//                $sql = "INSERT INTO food_revenue_details(rvc_id, description, video)VALUES(?,?,?)";
				//                $this->db->query($sql, $detailsArr);
				//            }

				return $rv_id;
			}
		}

		public function get_sub($sub){
			$sql="select display_name from food_subcategory where sub_cat_id='".$sub."' ";
			return  $s= $this->db->query($sql);
		}

		public function delete_revenue($id) {
			$sql = "DELETE FROM food_revenue_primary WHERE rvc_id=".$id;
			return $this->db->query($sql);
		}

		########### Sections Start ######################

		public function get_section_list($id=0) {
			if($id > 0) {
				$sql = "SELECT * FROM food_section WHERE section_id = ".$id;
			}
			else {
				$sql = "SELECT * FROM food_section";
			}

			return $this->db->query($sql);
		}

		public function save_section($param = array(), $id = 0) {
			//echo "<pre>".$id; print_r($param); die();
			if($id > 0) {
				$this->db->where(array('section_id'=>$id));
				$this->db->update('food_section', $param);
				return $id;
			}
			else {
				$sql = "INSERT INTO food_section(rvc_id, rvc_number, name, display_name, description)VALUES(?,?,?,?,?)";
				$this->db->query($sql, $param);
				return $this->db->insert_id();
			}
		}

		####################### Category List ##################
		public function get_categories($offset=0, $numofrec=25, $id=0) {
			if($id > 0) {
				$sql = "SELECT * FROM food_maincategory WHERE maincategory_id = $id";
			}
			else {
				$sql = "SELECT * FROM food_maincategory order by position LIMIT $offset, $numofrec" ;
			}

			return $this->db->query($sql);
		}

		public function get_addons($offset=0, $numofrec=25, $id=0) {
			if($id > 0) {
				$sql = "SELECT * FROM food_addons_master WHERE addon_id = $id";
			}
			else {
				$sql = "SELECT * FROM food_addons_master order by position LIMIT $offset, $numofrec" ;
			}

			return $this->db->query($sql);
		}

		public function get_group_addons($offset=0, $numofrec=25, $id=0) {
			if($id > 0) {
				$sql = "SELECT * FROM food_addon_group_master WHERE gid = $id";
			}
			else {
				$sql = "SELECT * FROM food_addon_group_master order by position LIMIT $offset, $numofrec" ;
			}

			return $this->db->query($sql);
		}



		public function save_categories($param = array(), $id=0) {
			if($id > 0) {
				$this->db->where(array('maincategory_id'=>$id));
				$this->db->update('food_maincategory', $param);
				return $id;
			}
			else {
				$sql="select MIN(position)as position from food_maincategory";
				$max=$this->db->query($sql);
				$res=$max->row();
				$r=$res->position;
				$r=$r-1;
				$param['position']=$r;

				$sql = "INSERT INTO food_maincategory(rvc_number, section_id, name, display_name,position)VALUES(?,?,?,?,?)";
				$this->db->query($sql, $param);
				return $this->db->insert_id();
			}
		}


		public function get_sub_categories($id=0) {
			if($id > 0) {
				$sql = "SELECT * FROM food_subcategory WHERE sub_cat_id = ".$id;
			}
			else {
				$sql = "SELECT * FROM food_subcategory";
			}

			return $this->db->query($sql);
		}
		 
		public function update_price($size,$price,$id){
			$sql="update  food_menuitem_price_detail set price='".$price."',size='".$size."' where mid='".$id."' ";
			$this->db->query($sql);
		}
		 
		public function insert_price($size,$price,$mid){
			$sql="insert into food_menuitem_price_detail (size,price,mid) values ('".$size."','".$price."','".$mid."') ";
			$this->db->query($sql);
		}
		 
		 
		public function sub_categories($id) {
			if($id > 0) {
				$sql="select a.*,b.name as cat_name from food_subcategory as a left join food_maincategory as b on a.maincategory_id=b.maincategory_id where b.maincategory_id='".$id."' order by position";

				// $sql = "SELECT * FROM food_subcategory WHERE maincategory_id ='".$id."' order by position ";
			}
			 

			return $this->db->query($sql);
		}
		 
		public function save_sub_categories($param = array(), $id=0) {

			// echo '<pre>';echo $param['maincategory_id']; print_r($param);die();

			if($id > 0) {
				$this->db->where(array('sub_cat_id'=>$id));
				$this->db->update('food_subcategory', $param);
				$this->db->query("update food_menuitem_details set maincategory_id='".$param['maincategory_id']."' where sub_cat_id='".$id."' ");
				$this->db->query("update food_menuitem_primary set maincategory_id='".$param['maincategory_id']."' where sub_cat_id='".$id."' ");
				return $id;
			}
			else {

				$sql="select MIN(position)as position from food_subcategory";
				$max=$this->db->query($sql);
				$res=$max->row();
				$r=$res->position;
				$r=$r-1;
				$param['position']=$r;

				$sql = "INSERT INTO food_subcategory(rvc_number, section_id, maincategory_id, name, display_name, description,position)VALUES(?,?,?,?,?,?,?)";
				$this->db->query($sql, $param);
				return $this->db->insert_id();
			}
		}
		 
		public function update_category_status($id,$val){
			if($val=='false'){$value=0;}else {$value=1;}
			echo $sql="update food_maincategory set is_active='".$value."' where maincategory_id='".$id."'";
			$this->db->query($sql);
		}

		public function update_subcategory_status($id,$val){
			if($val=='false'){$value=0;}else {$value=1;}
			echo $sql="update food_subcategory set is_active='".$value."' where sub_cat_id='".$id."'";
			$this->db->query($sql);
		}
		 
		public function update_stat($mid,$val){
			if($val=='false'){$value=0;}else {$value=1;}
			$sql1="update food_menuitem_primary set is_active='".$value."' where mid='".$mid."'";
			$this->db->query($sql1);
			$sql="update food_menuitem_details set is_active='".$value."' where mid='".$mid."'";
			$this->db->query($sql);
		}

		public function new_update_stat($mid,$val){
			if($val=='false'){$value=0;}else {$value=1;}
			$sql1="update food_menuitem_primary set is_active='".$value."',is_changed='0' where mid='".$mid."'";
			$this->db->query($sql1);
			$sql="update food_menuitem_details set is_active='".$value."' where mid='".$mid."'";
			$this->db->query($sql);
		}


		####################### Menuitems ##########################

		public function get_menuitems($offset=0, $numofrec=25, $id=0) {
			if($id > 0) {
				$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.mid = ".$id." ORDER BY cat.display_name, subcat.display_name, d.position";
			}
			else {
				$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position ASC";

	   //$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id ORDER BY cat.display_name, subcat.display_name, d.position ASC LIMIT $offset, $numofrec";
			}

			return $this->db->query($sql);
		}

		public function get_val($id){
			$sql="select display_name from food_menuitem_details where mid='".$id."' ";
			return $this->db->query($sql);

		}

		public function get_update_menuitems($offset=0, $numofrec=25, $id=0) {
			if($id > 0) {
				$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.mid = ".$id." & p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
			}
			else {
				$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position ASC";

	   //$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id ORDER BY cat.display_name, subcat.display_name, d.position ASC LIMIT $offset, $numofrec";
			}

			return $this->db->query($sql);
		}

		 
		 
		 
		 
		public function get_filterd_menuitems($cat_ids, $subcat_ids) {
			if(($cat_ids !="" && $subcat_ids != "") && ($cat_ids !=0 && $subcat_ids != 0)) {
				echo '1'.$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.sub_cat_id IN(".$subcat_ids.") and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
			}
			else {

				if($cat_ids != "" && $cat_ids != 0) {
					echo   '2'.$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.maincategory_id IN (".$cat_ids.") and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
				}
				else if($subcat_ids != "" && $subcat_ids != 0) {
					echo $cat_ids; echo'-'; echo $subcat_ids;   echo    '3'.$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.sub_cat_id IN(".$subcat_ids.") and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
				}
				else {
					echo $cat_ids; echo'-'; echo $subcat_ids;    echo  '4'.$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where d.display_name!=''  ORDER BY cat.display_name, subcat.display_name, d.position";
				}

				//$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id ORDER BY cat.display_name, subcat.display_name, d.position ASC";
			}
			//Ravindra --- start
			//echo $sql."<br/>";
			//$rs= $this->db->query($sql);
			//print($rs); die();

			//Ravindra ---end
			return $this->db->query($sql);
		}
		 
		public function get_filterd_update_menuitems($cat_ids, $subcat_ids) {
			//echo "I am in get_filtered_update_menuitems";
			if(($cat_ids !="" && $subcat_ids != "") && ($cat_ids !=0 && $subcat_ids != 0)) {
				$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.sub_cat_id IN(".$subcat_ids.") and p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
			}
			else {

				if($cat_ids != "" && $cat_ids != 0) {
					$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.maincategory_id IN (".$cat_ids.") and p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
				}
				else if($subcat_ids != "" && $subcat_ids != 0) {
					$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.sub_cat_id IN(".$subcat_ids.") and p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
				}
				else {
					$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where p.is_changed='1'  ORDER BY cat.display_name, subcat.display_name, d.position";
				}

				//$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id ORDER BY cat.display_name, subcat.display_name, d.position ASC";
			}

			return $this->db->query($sql);
		}
		 
		 
		 
		 
		public function get_time($id){
			$sql="select * from available_time where mid='".$id."' order by id";
			return $this->db->query($sql);
		}
		 
		public function get_menuitems_sub($sub) {

			//            $re = str_replace(array("%20"), " ", $sub);
			$sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where subcat.sub_cat_id='".$sub."' ORDER BY cat.display_name, subcat.display_name, d.position ASC";

			return $this->db->query($sql);
		}
		 
		public function update_position($i,$qid) {

			if($qid!=''){
				$sql="update food_menuitem_details set position='".$i."' where mid='".$qid."'";
				return  $this->db->query($sql);
			}
		}
		 
		public function update_subcat($i, $qid){
			if($qid!=''){
				echo  $sql="update food_subcategory set position='".$i."' where sub_cat_id='".$qid."'";
				return  $this->db->query($sql);
			}
		}
		 
		public function update_cat($i, $qid){
			if($qid!=''){
				$sql="update food_maincategory set position='".$i."' where maincategory_id='".$qid."'";
				return  $this->db->query($sql);
			}
		}
		 
		public function get_menuitems_search($field="", $value="") {
			$sql = "SELECT p.*, d.* FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid WHERE d.$field = '".$value."'";
			return $this->db->query($sql);
		}

		public function get_category_search($field="", $value="") {
			$sql = "SELECT * FROM food_maincategory WHERE $field = '".$value."'";
			return $this->db->query($sql);
		}

		public function save_menuitem($param=array(), $id=0) {
			//echo "<pre>"; print_r($param); die();
			if($id > 0) {
				if(isset($param['primary'])) {
					$this->db->where(array('mid'=>$id));
					$this->db->update('food_menuitem_primary', $param['primary']);
				}

				$this->db->where(array('mid'=>$id));
				$q1 = $this->db->get('food_menuitem_details');
				if($q1->num_rows() > 0) {
					if(isset($param['details'])) {
						$this->db->where(array('mid'=>$id));
						//echo "<pre>"; print_r($param);echo $id;echo 'dd'; die();
						$this->db->update('food_menuitem_details', $param['details']);
						$sql="delete  from available_time where mid='".$id."' ";
						$q=$this->db->query($sql);


						//                    $ids=array();
						//                    foreach($q->result() as $res){
						//                        echo $res->id;
						//                        $ids[]=$res->id;
						//                    }
						////                   print_r($ids); echo $ids['0'];die();
						//                    //echo $q->num_rows();echo count($param['details']['from_time']);die();
						//                if($q->num_rows()>0){
						//                        for($i=0;$i<count($param['time']['from_time']);$i++){
						//
						//                          //print_r($param['details']['from_time']);die();
						//                           $sql="update available_time set from_time=".$param['time']['from_time'][$i]." , to_time=".$param['time']['to_time'][$i]." where id='".$ids[$i]."'";
						//
						//                            $this->db->query($sql,$ans);
						//                        }
						//                }
						////                else{
						//                           for($i=0;$i<count($param['time']['from_time']);$i++)
						//              {

						//print_r($param['details']['from_time']);die();
						// echo $time;
						//   echo $param['time']['time1']; echo count($param['time']['from_time']); echo '<pre>'; print_r($param['time']);
						if(($param['time']['time1']==1)){
							//   echo count($param['time']['from_time']);die();
							for($i=0;$i<count($param['time']['from_time']);$i++){

								//           echo 'yes';
								//print_r($param['details']['from_time']);die();

								$sql="insert into available_time(mid,from_time,to_time) values (?,?,?)";
								$ans=array($id,$param['time']['from_time'][$i],$param['time']['to_time'][$i]);
								//$ans=array($mid,$param['details']['from1'][$i],$param['details']['to1'][$i]);
								$this->db->query($sql,$ans);
							}
						}
						//              }




					}
				}
				else {
					if(isset($param['details']['video']) && $param['details']['video'] != "") {
						$detailsArr = array($mid, $param['details']['name'], $param['details']['number'], $param['details']['descriptive_name'], $param['details']['is_breakfast'],$param['details']['is_special'],$param['details']['is_porch'], $param['details']['is_veg'], $param['details']['spicy_level'], $param['details']['required_addon'], $param['details']['synopsis'], $param['details']['video'], $param['details']['is_active'], time());
						$sql = "INSERT INTO food_menuitem_details(mid, menuitem_name, menuitem_number, display_name, is_breakfast,is_special,is_porch,is_veg, spicy_level, required_addon, synopsis, video, is_active, modified)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					}
					else {
						$detailsArr = array($mid, $param['details']['name'], $param['details']['number'], $param['details']['descriptive_name'], $param['details']['is_breakfast'],$param['details']['is_special'],$param['details']['is_porch'], $param['details']['is_veg'], $param['details']['spicy_level'], $param['details']['required_addon'], $param['details']['synopsis'], $param['details']['is_active'], time());
						$sql = "INSERT INTO food_menuitem_details(mid, menuitem_name, menuitem_number, display_name, is_breakfast,is_special,is_porch, is_veg, spicy_level, required_addon, synopsis, is_active, modified)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
					}
					$this->db->query($sql, $detailsArr);
				}

				return $id;
			}
			else {
				$sql = "INSERT INTO food_menuitem_primary(rvc_number, menuitem_number, name, has_addons, section_id, maincategory_id, sub_cat_id, is_active)VALUES(?,?,?,?,?,?,?,?)";
				$this->db->query($sql, $param['primary']);
				$mid = $this->db->insert_id();
				// echo $mid;print_r($param);die();
				//              for($i=0;$i<count($param['details']['from_time']);$i++)
				//              {
				// echo '<pre>'; print_r($param['details']['from1']);echo $param['details']['from1'][0];die();
				 
				if(($param['time']['time1']==1)){
					for($i=0;$i<count($param['details']['from1']);$i++){


						//print_r($param['details']['from_time']);die();

						$sql="insert into available_time(mid,from_time,to_time) values (?,?,?)";
						$ans=array($mid,$param['details']['from1'][$i],$param['details']['to1'][$i]);
						//$ans=array($mid,$param['details']['from1'][$i],$param['details']['to1'][$i]);
						$this->db->query($sql,$ans);
					}
				}
				//              }
				if($mid > 0) {
					$detailsArr = array($mid, $param['details']['name'], $param['details']['number'], $param['details']['descriptive_name'], $param['details']['is_breakfast'],$param['details']['is_special'],$param['details']['is_porch'], $param['details']['is_veg'], $param['details']['spicy_level'], $param['details']['required_addon'], $param['details']['synopsis'], $param['details']['video'], $param['details']['is_active'], time());
					if(isset($param['details']['video']) && $param['details']['video'] != "") {
						$detailsArr = array($mid, $param['details']['name'], $param['details']['number'], $param['details']['descriptive_name'], $param['details']['section_id'], $param['details']['maincategory_id'], $param['details']['sub_cat_id'], $param['details']['is_breakfast'],$param['details']['is_special'],$param['details']['is_porch'], $param['details']['is_veg'], $param['details']['spicy_level'], $param['details']['required_addon'], $param['details']['synopsis'], $param['details']['video'], $param['details']['is_active'], time(),$param['details']['position'],$param['details']['delivery_time']);

						$sql = "INSERT INTO food_menuitem_details(mid, menuitem_name, menuitem_number, display_name, section_id, maincategory_id, sub_cat_id, is_breakfast,is_special,is_porch,is_veg, spicy_level, required_addon, synopsis, video, is_active, modified,position,delivery_time)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					}
					else {
						$detailsArr = array($mid, $param['details']['name'], $param['details']['number'], $param['details']['descriptive_name'], $param['details']['section_id'], $param['details']['maincategory_id'], $param['details']['sub_cat_id'], $param['details']['is_breakfast'],$param['details']['is_special'],$param['details']['is_porch'], $param['details']['is_veg'], $param['details']['spicy_level'], $param['details']['required_addon'], $param['details']['synopsis'], $param['details']['is_active'], time(),$param['details']['position'],$param['details']['delivery_time']);

						$sql = "INSERT INTO food_menuitem_details(mid, menuitem_name, menuitem_number, display_name, section_id, maincategory_id, sub_cat_id, is_breakfast,is_special,is_porch, is_veg, spicy_level, required_addon, synopsis, is_active, modified,position,delivery_time)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					}
					$this->db->query($sql, $detailsArr);
				}

				return $mid;
			}
		}

		public function save_menuitem_addons($mid, $param = array()) {
			if(isset($param[0]) && $param[0] != "") {
				$this->delete_menuitem_addons($mid);
				foreach($param as $aid) {
					//echo $aid; die();
					$sql1 = "SELECT * FROM food_menuitem_addons WHERE group_number = ".$aid." GROUP BY addon_name";
					$q = $this->db->query($sql1);
					foreach($q->result() as $row) {
						$sql = "INSERT INTO food_menuitem_addons(mid, addon_code, addon_name, addon_size, addon_price, group_number, group_name)VALUES($mid, '".$row->addon_code."', '".mysql_escape_string($row->addon_name)."', '".$row->addon_size."', '".$row->addon_price."', '".$row->group_number."', '".$row->group_name."')";
						$this->db->query($sql);
					}
				}
			}
		}

		public function  set_menuitems($q,$c){
			if($q!= '0'){
				if($c=='0'){
					echo '1';     echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.name LIKE '".$q."%' and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
				}
				 
				else if($c!='0'){
					echo '2';      echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.name LIKE '".$q."%' and d.maincategory_id in('".$c."')    and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
				}


			}
			else{          if($c=='0'){
				echo '3';        echo     $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position ASC";

			}
			else if($c!='0'){
				echo '4';                  echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE  d.maincategory_id in('".$c."') and d.display_name!='' ORDER BY cat.display_name, subcat.display_name, d.position";
			}


			}
			$sql = str_replace(array("%20"), " ", $sql);
			return $this->db->query($sql);
		}
		 

		public function  set_update_menuitems($q,$c){ echo $q;echo $c;
		if($q!= '0'){
			if($c=='0'){
				echo '1';     echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.name LIKE '".$q."%' and  p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
			}
			 
			else if($c!='0'){
				echo '2';      echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE p.name LIKE '".$q."%' and d.maincategory_id in('".$c."') and p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
			}


		}
		else{          if($c=='0'){
			echo '3';        echo     $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id where p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position ASC";

		}
		else if($c!='0'){
			echo '4';                  echo  $sql = "SELECT p.*, d.*, cat.display_name AS cat_name, subcat.display_name AS subcat_name,subcat.sub_cat_id AS subcat_id FROM food_menuitem_primary AS p LEFT JOIN food_menuitem_details AS d ON p.mid = d.mid LEFT JOIN food_maincategory AS cat ON d.maincategory_id = cat.maincategory_id LEFT JOIN food_subcategory AS subcat ON d.sub_cat_id = subcat.sub_cat_id  WHERE  d.maincategory_id in('".$c."') and p.is_changed='1' ORDER BY cat.display_name, subcat.display_name, d.position";
		}


		}
		$sql = str_replace(array("%20"), " ", $sql);
		return $this->db->query($sql);
		}
		 
		 
		 
		 

		public function save_allowed_addons($mid, $param = array()) {
			if(isset($param[0]) && $param[0] != "") {
				$this->delete_allowed_addons($mid);
				foreach($param as $aid) {
					$sql1 = "SELECT * FROM food_menuitem_allowed_addons WHERE group_number = ".$aid." GROUP BY addon_name";
					$q = $this->db->query($sql1);
					foreach($q->result() as $row) {
						$sql = "INSERT INTO food_menuitem_allowed_addons(mid, addon_code, addon_name, addon_size, addon_price, group_number, group_name)VALUES($mid, '".$row->addon_code."', '".mysql_escape_string($row->addon_name)."', '".$row->addon_size."', '".$row->addon_price."', '".$row->group_number."', '".$row->group_name."')";
						$this->db->query($sql);
					}
				}
			}
		}

		public function delete_menuitem_addons($mid) {
			$sql = "DELETE FROM food_menuitem_addons WHERE mid=$mid ";
			return $this->db->query($sql);
		}

		public function delete_allowed_addons($mid) {
			$sql = "DELETE FROM food_menuitem_allowed_addons WHERE mid=$mid";
			return $this->db->query($sql);
		}

		public function get_menuitem_price($mid=0) {
			$sql = "SELECT * FROM food_menuitem_price_detail WHERE mid = ".$mid;
			return $this->db->query($sql);
		}

		public function add_menuitem_price($param = array()) {
			$sql = "INSERT INTO food_menuitem_price_detail(mid, price, size, uom)VALUES(?,?,?,?)";
			return $this->db->query($sql, $param);
		}

		public function delete_menuitem_price($mid=0) {
			$sql = "DELETE FROM food_menuitem_price_detail WHERE mid = ".$mid;
			return $this->db->query($sql);
		}

		public function get_menu_addons($mid) {
			$sql = "SELECT * FROM  food_menuitem_addons WHERE mid=$mid GROUP BY group_number";
			return $this->db->query($sql);
		}

		public function get_menu_allowed_addons($mid) {
			$sql = "SELECT * FROM food_menuitem_allowed_addons WHERE mid=$mid GROUP BY group_number";
			return $this->db->query($sql);
		}


		public function get_item_name($mid){
			$sql="select menuitem_name from food_menuitem_details  where mid='".$mid."' ";
			return $this->db->query($sql);
		}


		public function delete_menuitem($mid=0) {
			if($mid > 0) {
				$sql = "DELETE FROM food_locale WHERE type ='menuitem' AND type_id  = ".$mid;
				$this->db->query($sql);

				$sql = "DELETE FROM food_menuitem_details WHERE mid = ".$mid;
				$this->db->query($sql);

				$sql = "DELETE FROM food_menuitem_price_detail WHERE mid = ".$mid;
				$this->db->query($sql);

				$sql = "DELETE FROM food_application_images WHERE item_type ='menuitem' AND item_type_id = ".$mid;
				$this->db->query($sql);

				$sql ="DELETE FROM food_menuitem_primary WHERE mid = ".$mid;
				return $this->db->query($sql);
			}
		}

		####################### Food Addons #######################
		public function get_food_addons_group() {
			$sql = "SELECT * FROM food_menuitem_addons GROUP BY group_number ORDER BY group_name";
			return $this->db->query($sql);
		}

		public function get_food_allowed_addons_group() {
			$sql = "SELECT * FROM food_menuitem_allowed_addons GROUP BY group_number ORDER BY group_name";
			return $this->db->query($sql);
		}

		####################### End Food Addons #######################

		####################### Food Application Images #######################
		public function save_images($images = array(), $type, $type_id=0) {
			$aid = 1;
			$sql = "INSERT INTO food_application_images(aid, item_type, item_type_id, image_name, position, data)VALUES(?,?,?,?,?,?)";
			$images_id = array();
			$i=0;

			$max_pos = $this->get_max_image_pos($type, $type_id);
			if($max_pos) {
				$i = $max_pos+1;
			}

			foreach($images as $data) {
				$this->delete_images($aid, $type, $type_id, $i);
				$param = array($aid, $type, $type_id, $data['file_name'], $i, serialize($data));
				$this->db->query($sql, $param);
				$images_id[] = $this->db->insert_id();
				$i++;
			}

			$this->reorder_food_image($type, $type_id);

			return $images_id;
		}


		public function reorder_food_image($type, $type_id=0) {
			$q = $this->common_model->get_record_by_condition('food_application_images', 'item_type="'.$type.'" AND item_type_id='.$type_id);
			$i=0;
			foreach($q->result() as $row) {
				$sql = "UPDATE food_application_images SET position = $i WHERE id=".$row->id;
				$this->db->query($sql);
				$i++;
			}
		}

		public function get_max_image_pos($type='menuitem', $type_id=0) {
			$sql = "SELECT MAX(position) AS mx FROM food_application_images WHERE item_type='".$type."' AND item_type_id=".$type_id;
			$q=$this->db->query($sql);
			$row = $q->result();
			return $row[0]->mx;
		}

		public function delete_images($aid, $type, $type_id, $position) {
			$sql = "DELETE FROM food_application_images WHERE aid=$aid AND item_type = '".$type."' AND item_type_id='".$type_id."' AND position=".$position;
			return $this->db->query($sql);
		}

		public function get_food_images($type, $type_id="") {
			$sql = "SELECT * FROM food_application_images WHERE item_type = '".$type."' AND item_type_id = ".$type_id." ORDER BY position";
			return $this->db->query($sql);
		}


		##################### Group Sorting ##########################
		public function save_menuitem_groups($groups=array(), $mid=0, $addons_group=array()) {
			if(count($groups) > 0) {
				$sql = "DELETE FROM food_menuitem_groups WHERE mid=".$mid;
				$this->db->query($sql);
				$i=0;
				foreach($groups as $g) {
					if($g != "" && $g != "on") {
						$i++;
						$sql = "INSERT INTO food_menuitem_groups(mid, gid, position)VALUES($mid, $g, $i)";
						$this->db->query($sql);
					}
				}
			}
		}


		public function get_menuitem_groups($mid=0) {
			$sql = "SELECT gm.gid, gm.group_code, gm.group_name, gm.display_name, gm.is_allowed, ig.position FROM food_addon_group_master AS gm INNER JOIN food_menuitem_groups as ig on gm.gid = ig.gid AND ig.mid=$mid ORDER BY ig.position ASC";
			return $this->db->query($sql);
		}

		####################        End        ########################


		##################### Food Application Locale ###########################

		public function save_food_locale($param, $type="", $type_id="") {
			$sql = "INSERT INTO food_locale(lang_code, type, type_id, name, description)VALUES(?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}

		public function save_food_cat($param, $type="", $type_id="") {
			$sql = "INSERT INTO food_locale(lang_code, type, type_id, name)VALUES(?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}


		public function delete_food_locale($type, $type_id) {
			$sql = "DELETE FROM food_locale WHERE type = '".$type."'AND type_id = '".$type_id."'";
			return $this->db->query($sql);
		}

		public function get_food_locale($type, $type_id) {
			$this->db->where(array('type'=>$type, 'type_id'=>$type_id));
			return $this->db->get('food_locale');
		}
}
?>
