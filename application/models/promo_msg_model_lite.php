<?php
/**
 * Description of welcomletter_model
 *
 * @author pbsl
 */
class promo_msg_model_lite extends CI_Model {

	var $db2;
	public function  __construct() {
		parent::__construct();
		//$this->db2 = $this->load->database('stats', TRUE);
	}

	public function update_delete_send($mid){
		$date = date('Y-m-d H:i');
		$sql="Update iremote.promo_msg set end_date='".$date."' where promo_msg_id='".$mid."' ";
		$this->db->query($sql);
		return $mid;
	}
	 
	public function save_message($param=array(), $id=0) {
		$sql = "INSERT INTO iremote.promo_msg(title,msg_body,extrainformation,promo_msg_img,promo_page_url,published,author_id,end_date,start_date)VALUES(?,?,?,?,?,?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function send_message($param=array(), $id=0) {
		$sql = "INSERT INTO iremote.promo_msg(title,msg_body,extrainformation,promo_msg_img,promo_page_url,published,author_id,end_date,start_date,draft,sent)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function delete_message($mid){
		$sql="delete from iremote.promo_msg where promo_msg_id ='".$mid."'";
		$this->db->query($sql);
	}


	public function update_message($param=array(), $mid=0) {
		if($mid > 0) {
			$this->db->where(array('promo_msg_id'=>$mid));
			$this->db->update('iremote.promo_msg', $param);
			return $mid;
		}

	}

	public function set_sent($mid){
		$sql= "update iremote.promo_msg set sent=1 where promo_msg_id='".$mid."'";
		$this->db->query($sql);
	}

	public function send($mid,$time){
		if($time=='0000-00-00 00:00:00'){
			$sql= "update iremote.promo_msg set draft=0,published=1,sent=1 where promo_msg_id='".$mid."'";
			$this->db->query($sql);
		}
		else{
			$sql= "update iremote.promo_msg set draft=0,published=1,sent=0 where promo_msg_id='".$mid."'";
			$this->db->query($sql);
		}
	}

	public function get_time($mid){

		$sql="select start_date from iremote.promo_msg where promo_msg_id='".$mid."'";
		return $this->db->query($sql);
	}
	public function update_img_message($param=array(), $mid=0) {
		if($mid > 0) {
			$this->db->where(array('promo_msg_id'=>$mid));
			$this->db->update('iremote.promo_msg', $param);
			return $mid;
		}

	}

	public function get_all_guest(){
		$sql="select * from iremote.pmsi_guest where changeflag=0 AND sharestatus = 'primary' ";
		return $this->db->query($sql);


	}

	public function delete_promo_msg_to_deliver($param){
		$sql = "INSERT INTO iremote.promo_msg_to_deliver(room_no,guest_id,promo_msg_id,ip,is_updated)VALUES(?,?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function promo_msg_to_deliver($param){
		$sql = "INSERT INTO iremote.promo_msg_to_deliver(room_no,guest_id,promo_msg_id,ip)VALUES(?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}
	 
	public function promo_msg_status($data){

		$sql = "INSERT INTO iremote.promo_msg_status(guest_id,guest_name,promo_msg_id,token)VALUES(?,?,?,?)";
		$this->db->query($sql, $data);
		return $this->db->insert_id();
	}
	 
}
?>
