<?php
/**
 * Description of welcomletter_model
 *
 * @author pbsl
 */
class welcomletter_model extends CI_Model {

	 
	public function get_let($data) {
		if($data!='0'){
			$sql = "SELECT l.*, t.name, t.id FROM welcome_letters AS l LEFT JOIN welcome_letter_types AS t ON t.id = l.type_id where l.title like '".$data."%'  ORDER BY l.title ";
		}
		else{
			$sql = "SELECT l.*, t.name, t.id FROM welcome_letters AS l LEFT JOIN welcome_letter_types AS t ON t.id = l.type_id   ORDER BY l.title ";

		}

		return $this->db->query($sql);
	}




	public function get_letters($cat, $offset=0, $limit=30) {
		if($cat > 0) {
			$sql = "SELECT l.*, t.name, t.id FROM welcome_letters AS l LEFT JOIN welcome_letter_types AS t ON t.id = l.type_id WHERE t.id=$cat ORDER BY l.title LIMIT $offset, $limit";
		}
		else {
			$sql = "SELECT l.*, t.name, t.id FROM welcome_letters AS l LEFT JOIN welcome_letter_types AS t ON t.id = l.type_id ORDER BY l.title LIMIT $offset, $limit";
		}

		return $this->db->query($sql);
	}

	public function save_letter($param=array(), $id=0) {
		if($id > 0) {
			$this->db->where(array('lid'=>$id));
			$this->db->update('welcome_letters', $param);
			return $id;
		}
		else {
			$sql = "INSERT INTO welcome_letters(type_id, title, let_name, content, use_logo, logo_align, type, annexure, modified)VALUES(?,?,?,?,?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function save_rooms_message($room_no=0, $date_str="", $param=array()) {
		if($room_no > 0) {
			$q = $this->db->query("SELECT * FROM rooms_message WHERE room_no=$room_no AND created = '".$date_str."'");
			if($q->num_rows() > 0) {
				$row = $q->result();
				$room_no = $row[0]->room_no;
				$this->db->where(array('room_no'=>$room_no, 'created'=>$date_str));
				$this->db->update('rooms_message', $param);
				return $room_no;
			}
			else {
				$sql = "INSERT INTO rooms_message(room_no, letter_code, guest_name, message, created,pick_from_pmsi)VALUES(?,?,?,?,?,?)";
				return $this->db->query($sql, $param);
			}
		}

		return false;
	}

	public function save_category($param=array(), $id=0) {
		if($id > 0) {
			$this->db->where(array('id'=>$id));
			$this->db->update('welcome_letter_types', $param);
			return $id;
		}
		else {
			$sql = "INSERT INTO welcome_letter_types(name)VALUES(?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function count_welcom_letter($cat=0) {
		if($cat > 0) {
			$q = $this->db->query("SELECT COUNT(*) AS ctr FROM welcome_letters AS l LEFT JOIN welcome_letter_types AS t ON t.id = l.type_id WHERE t.id=$cat ");
			$row = $q->result();
			return $row[0]->ctr;
		}
		else {
			return $this->db->count_all("welcome_letters");
		}
	}

	public function delete_category($cid=0) {
		$sql = "DELETE FROM welcome_letter_types WHERE id=".$cid;
		return $this->db->query($sql);
	}

	public function delete_letter($lid=0, $cid=0) {
		if($lid > 0) {
			$sql = "DELETE FROM welcome_letters WHERE lid=".$lid;
		}
		else {
			$sql = "DELETE FROM welcome_letters WHERE type_id=".$cid;
		}

		return $this->db->query($sql);
	}

	public function get($lid){
		$sql="select let_name from welcome_letters where lid='".$lid."'";
		return $this->db->query($sql);

	}
	 
	public function set_default($id=0) {
		if($id > 0 ) {
			$sql = "UPDATE welcome_letters SET is_default=0";
			$this->db->query($sql);

			$sql = "UPDATE welcome_letters SET is_default=1 WHERE lid=".$id;
			return $this->db->query($sql);
		}
	}

	public function delete_rooms_letter($room_no=0, $date_str=0) {
		$sql = "DELETE FROM rooms_message WHERE room_no=$room_no AND created='".$date_str."'";
		return $this->db->query($sql);
	}

	public function delete_room_letter_by_id($id=0) {
		$sql = "DELETE FROM rooms_message WHERE id=$id";
		return $this->db->query($sql);
	}
}
?>