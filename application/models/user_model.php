<?php
/**
 * Description of user_model
 *
 * @author pbsl
 */
class user_model extends CI_Model {

	public function  __construct() {
		parent::__construct();
	}

	public function user_roles($uid) {
		$sql = "SELECT r.rid, r.name FROM role AS r, users_roles AS ur WHERE ur.rid = r.rid AND ur.uid IN (".$uid.")";
		return $this->db->query($sql);
	}

	public function user_permission($roles) {
		$sql = "SELECT pr.perm, p.pid, p.rid FROM perms AS pr LEFT JOIN permission AS p ON pr.pid=p.pid WHERE p.rid IN(".$roles.")";
		return $this->db->query($sql);
	}

	public function save_user($param) {
		$insertparam = array($param['name'], $param['pass'], $param['mail'], $param['department_id'], time(), $param['status']);
		if(isset($param['uid']) && $param['uid'] > 0) {
			$uid = $param['uid'];
			if(isset($param['pass'])) {
				$updateParam = array('name'=>$param['name'],'pass'=>$param['pass'], 'mail'=>$param['mail'], 'department_id'=>$param['department_id'], 'status'=>$param['status']);
			}
			else {
				$updateParam = array('name'=>$param['name'], 'mail'=>$param['mail'], 'department_id'=>$param['department_id'], 'status'=>$param['status']);
			}
			//print_r($updateParam); die();
			$this->db->where('uid', $uid);
			$this->db->update('users', $updateParam);
			$this->save_user_roles($uid, $param['roles']);
			return true;
		}
		else {
			//print_r($param); die();
			$sql = "INSERT INTO users (name, pass, mail, department_id, created, status)VALUES(?,?,?,?,?,?)";
			$this->db->query($sql, $insertparam);
			$uid = $this->db->insert_id();
			//echo $uid; die();
			if(count($param['roles']) > 0) {
				$this->save_user_roles($uid, $param['roles']);
			}
			if($uid > 0) {
				return $uid;
			}
			else {
				return FALSE;
			}
		}

	}


	public function save_department($param=array()) {
		if($param['did'] > 0) {
			$sql = "UPDATE departments SET name='".$param['name']."' WHERE did = ".$param['did'];
			$this->db->query($sql);
			return $this->db->insert_id();
		}
		else {
			$sql = "INSERT INTO departments (name)VALUES('".$param['name']."')";
			return $this->db->query($sql);
		}
	}


	public function delete_department($did) {
		if($did > 0) {
			$sql = "UPDATE users SET department_id = 0 WHERE department_id=".$did;
			$this->db->query($sql);

			$sql = "DELETE FROM departments WHERE did=".$did;
			if($this->db->query($sql)){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function save_user_roles($uid, $roles = array()) {
		if($uid > 0) {
			$this->delete_user_roles($uid);
			foreach($roles as $role) {
				$sql = "INSERT INTO users_roles(uid, rid)VALUES(".$uid.", ".$role.")";
				$this->db->query($sql);
			}
		}
		else {
			return false;
		}
	}

	public function delete_user($uid) {
		$this->delete_user_roles($uid);
		$sql = "DELETE FROM users WHERE uid = ".$uid;
		if($this->db->query($sql)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function delete_user_roles($uid) {
		$sql = "DELETE FROM users_roles WHERE uid = ".$uid;
		if($this->db->query($sql)) {
			return true;
		}
		else {
			return false;
		}
	}


	public function save_permissions($param) {
		$sql = "INSERT INTO permission(pid, rid) VALUES (".$param['pid'].", ".$param['rid'].")";
		return $this->db->query($sql);
	}

	public function delete_role_permission($rid) {
		$sql = "DELETE FROM permission WHERE rid = ".$rid;
		return $this->db->query($sql);
	}


	public function change_password($uid=0, $password="") {
		$sql = "UPDATE users SET pass = '".$password."' WHERE uid=".$uid;
		return $this->db->query($sql);
	}
}
?>
