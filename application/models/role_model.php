<?php
/**
 * Description of role_model
 *
 * @author pbsl
 */
class role_model extends CI_Model {

	public function save_role($param = array()) {
		$name = $param['name'];
		$rid = $param['rid'];
		if($rid > 0) {
			$this->db->where('rid', $rid);
			return $this->db->update('role', array('name'=>$name));
		}
		else {
			$sql = "INSERT INTO role (name)VALUES(?)";
			$this->db->query($sql, array('name'=>$name));
			return $this->db->insert_id();
		}
	}

	public function delete_role($rid) {
		$this->delete_users_assigned_role($rid);
		$sql = "DELETE FROM role WHERE rid = ".$rid;
		return $this->db->query($sql);
	}

	public function delete_users_assigned_role($rid) {
		$sql = "DELETE FROM users_roles WHERE rid = ".$rid;
		return $this->db->query($sql);
	}
}
?>
