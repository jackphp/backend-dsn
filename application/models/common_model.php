<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed.');
/**
 * Description of common_model
 *
 * @author pbsl
 */
class common_model extends CI_Model{
	public function  __construct() {
		parent::__construct();
		 
	}

	public function get_records($table, $sort="") {
		if($sort == "") {
			$sql = "SELECT * FROM ".$table;
		}
		else {
			$sql = "SELECT * FROM ".$table." ORDER BY ".$sort;
		}

		return $this->db->query($sql);
	}


	public function get_val(){
		$sql="select from_opera from welcome_letter_opera";
		return $this->db->query($sql);
	}


	public function change_opera($val){
		$sql="update welcome_letter_opera set from_opera='".$val."'";
		$this->db->query($sql);
	}


	public function reset($id){
		$sql="update welcome_letters set is_default=0 ";
		$this->db->query($sql);
		$sql="update welcome_letters set is_default=1 where lid='".$id."'";
		$this->db->query($sql);
	}


	public function get_records_by_field($table, $field, $value, $sort="") {
		if($sort == "") {
			$sql = "SELECT * FROM ".$table." WHERE ".$field."="."'".$value."'";
		}
		else {
			$sql = "SELECT * FROM ".$table." WHERE ".$field." = '".$value."' ORDER BY ".$sort;
		}
		return $this->db->query($sql);
	}

	public function get_record_by_condition($table, $condition, $sort="") {
		if($sort != "") {
			$sql = "SELECT * FROM ".$table." WHERE ".$condition." ORDER BY ".$sort;
		}
		else {
			$sql = "SELECT * FROM ".$table." WHERE ".$condition;
		}

		return $this->db->query($sql);
	}

	public function delete_record_for_id($table, $field, $value) {
		$sql = "DELETE FROM $table WHERE $field = '".$value."' ";
		return $this->db->query($sql);
	}

	public function get_subcat_name($id){
		$sql="select name from food_subcategory where sub_cat_id='".$id."' ";
		return $this->db->query($sql);

	}

	public function get_sub_name($id){
		$sql="select name from food_subcategory where maincategory_id='".$id."' ";
		return $this->db->query($sql);


	}
	public function get_sub($id){
		$sql="select sub_cat_id from food_subcategory where maincategory_id='".$id."'  ";
		return $this->db->query($sql);
	}

	public function get_menu_name($id){
		$sql="select menuitem_name from food_menuitem_details where maincategory_id='".$id."' ";
		return $this->db->query($sql);


	}

	public function get_cat_name($id){
		$sql="select name from food_maincategory where maincategory_id='".$id."'";
		return $this->db->query($sql);
	}

	public function get_food_name($id){
		$sql="select menuitem_name from food_menuitem_details where sub_cat_id='".$id."' ";
		return $this->db->query($sql);
	}

	public function load_all_permissions() {
		$sql = "SELECT pr.pid, pr.perm, p.rid, r.name FROM perms AS pr LEFT JOIN permission AS p ON pr.pid = p.pid LEFT JOIN role AS r ON r.rid = p.rid";
		return $this->db->query($sql);
	}

	public function is_permission_exist($pname) {
		$sql = "SELECT * FROM perms WHERE perm = '".$pname."'";
		return $this->db->query($sql);
	}

	public function save_permission($pname) {
		$sql = "INSERT INTO perms (perm)VALUES('".$pname."')";
		return $this->db->query($sql);
	}


}// End Of Class
?>
