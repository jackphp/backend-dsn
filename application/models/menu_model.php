<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu_model
 *
 * @author pbsl
 */
class menu_model extends CI_Model {
	public function  __construct() {
		parent::__construct();
	}

	public function save_menu($menu, $mid = "") {
		if($mid > 0) {
			$sql = "UPDATE menu SET name = '".$menu."' WHERE mid = ".$mid;
			return $this->db->query($sql);
		}
		else {
			$sql = "INSERT INTO menu(name)VALUES(?)";
			$this->db->query($sql, $menu);
			return $this->db->insert_id();
		}
	}

	public function save_menu_link($param, $link_id = "") {
		if( count($param) > 0 ) {
			if($link_id > 0) {
				$this->db->where(array('link_id'=>$link_id));
				return $this->db->update('menu_links', $param);
			}
			else {
				$sql = "INSERT INTO menu_links(mid,path,label,permission,menu_order)VALUES(?,?,?,?,?)";
				$this->db->query($sql, $param);
				return $this->db->insert_id();
			}
		}
	}

	public function load_application_menu($perms="") {
		if($perms != "") {
			$sql = "SELECT m.*, l.* FROM menu_links AS l LEFT JOIN  menu AS m ON l.mid = m.mid WHERE l.status<>0 AND l.permission IN( ".$perms." ) ORDER BY m.mid, l.menu_order";
		}
		else {
			$sql = "SELECT m.*, l.* FROM menu_links AS l LEFT JOIN  menu AS m ON l.mid = m.mid WHERE l.status<>0 ORDER BY l.menu_order";
		}
		return $this->db->query($sql);
	}

	public function load_all_menu() {
		$sql = "SELECT m.*, l.* FROM menu_links AS l LEFT JOIN  menu AS m ON l.mid = m.mid  ORDER BY l.label, m.name";
		return $this->db->query($sql);
	}

	public function is_menu_exist($menu) {
		$sql = "SELECT mid FROM menu WHERE name='".$menu."'";
		return $this->db->query($sql);
	}

	public function is_menu_link_exist($mid, $label) {
		$sql = "SELECT link_id FROM menu_links WHERE mid = ".$mid." AND label='".$label."'";
		return $this->db->query($sql);
	}
}
?>
