<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of food_addon_model
 *
 * @author pbsl
 */
class food_addon_model extends  CI_Model {

	public function get_addons($gid=0) {
		//$sql = "SELECT * FROM food_addons_master ORDER BY addon_name LIMIT $offset, $limit";
		if($gid > 0) {
			$sql = "SELECT * FROM food_addons_master WHERE gid=".$gid." ORDER BY addon_name";
			return $this->db->query($sql);
		}
		else {
			$sql = "SELECT * FROM food_addons_master ORDER BY addon_name";
			return $this->db->query($sql);
		}
	}

	public function get($id){
		$sql="select addon_name from food_addons_master where addon_id='".$id."'";
		return $this->db->query($sql);

	}

	public function add_addons($param = array(), $id=0) {
		if($id > 0) {
			$this->db->where(array('addon_id'=>$id));
			$this->db->update('food_addons_master', $param);
			return $id;
		}
		else {
			$sql = "INSERT INTO food_addons_master(gid, addon_code, addon_name, display_name, size, price)VALUES(?,?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function save_locale($param = array()) {
		$sql = "INSERT INTO food_locale (lang_code, type, type_id, name, description) VALUES (?,?,?,?,?)";
		$this->db->query($sql, $param);
		return $this->db->insert_id();
	}

	public function save_menuitem_addons($apram = array()) {
		$sql = "INSERT INTO food_menuitem_addons(addon_id, mid, gid, addon_code,  addon_name, addon_size, addon_price)VALUES(?,?,?,?,?,?,?)";
		return $this->db->query($sql, $apram);
	}

	public function delete_addon($id=0) {
		$sql = "DELETE FROM food_locale WHERE type = 'addon' and type_id = $id";
		$this->db->query($sql);

		$sql = "DELETE FROM food_application_images WHERE item_type = 'addon' and item_type_id  = $id";
		$this->db->query($sql);

		$sql = "DELETE FROM food_addons_master WHERE addon_id = $id";
		return $this->db->query($sql);
	}
}
?>
