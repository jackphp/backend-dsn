<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of config_model
 *
 * @author pbsl
 */
class config_model extends CI_Model {
	var $db;
	public function  __construct() {
		parent::__construct();
		$this->db = $this->load->database('reports', TRUE);
	}

	public function load_addons() {
		$sql = "SELECT * FROM digivale_addons";
		return $this->db->query($sql);
	}

	public function save_addons($param, $id="") {
		if($id > 0) {
			$this->db->where(array('id'=>$id));
			return $this->db->update('digivale_addons', $param);
		}
		else {
			$sql = "INSERT INTO digivale_addons(name,url,reporting,reporting_path,perm,status)VALUES(?,?,?,?,?,?)";
			$this->db->query($sql, $param);
			return $this->db->insert_id();
		}
	}

	public function get_vars($name="") {
		if($name == "") {
			$sql = 'SELECT * FROM system_vars';
		}
		else {
			$sql = 'SELECT * FROM system_vars WHERE name= "'.$name.'"';
		}

		return $this->db->query($sql);
	}


} // End of Class
?>
