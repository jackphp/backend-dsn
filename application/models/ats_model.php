<?php
/**
 * Description of ats_model
 *
 * @author pbsl
 */
class ats_model extends CI_Model {
	var $db;
	public function  __construct() {
		parent::__construct();
		$this->db = $this->load->database("reports", TRUE);
	}

	public function floors_overview() {
		$sql = "SELECT f.fid, f.fname, COUNT(f.fname) as ctr FROM floors AS f, floor_rooms AS r WHERE r.fid = f.fid GROUP BY f.fname";
		return $this->db->query($sql);
	}

	public function load_floor_rooms($fid) {
		$sql = "SELECT status.*, r.room_name, f.fname FROM room_status_info AS status LEFT JOIN floor_rooms AS fr ON fr.room_id = status.room_id LEFT JOIN rooms AS r ON fr.room_id = r.room_id LEFT JOIN floors AS f ON f.fid = fr.fid WHERE fr.fid = ".$fid;
		return $this->db->query($sql);
	}

	public function update_room_status($room_id, $field, $val) {
		$sql = "UPDATE room_status_info SET ".$field."='".$val."' WHERE room_id = ".$room_id;
		return $this->db->query($sql);
	}

	public function room_count_status_wise($status_type, $status = 1) {
		if($status_type != 'rented' || $status_type != 'maintenance') {
			if($status_type == 'battry') {
				$sql = "SELECT COUNT(*) AS ctr FROM room_status_info WHERE ".$status_type." <= ".$status;
			}
			else {
				$sql = "SELECT COUNT(*) AS ctr FROM room_status_info WHERE ".$status_type." = ".$status;
			}
		}
		else  {
			$sql = "SELECT COUNT(*) AS ctr FROM room_status_info WHERE ".$status_type." = ".$status." AND rented=1";
		}
		return $this->db->query($sql);
	}


	public function room_rows_status_wise($status_type, $status = 1) {
		if($status_type != 'rented' || $status_type != 'maintenance') {
			if($status_type == 'battry') {
				$sql = "SELECT f.fname, r.room_name FROM room_status_info AS info LEFT JOIN floor_rooms AS f_rooms ON info.room_id = f_rooms.room_id LEFT JOIN floors AS f ON f.fid = f_rooms.fid LEFT JOIN rooms AS r ON info.room_id = r.room_id WHERE info.".$status_type." <= ".$status;
			}
			else {
				$sql = "SELECT f.fname, r.room_name FROM room_status_info AS info LEFT JOIN floor_rooms AS f_rooms ON info.room_id = f_rooms.room_id LEFT JOIN floors AS f ON f.fid = f_rooms.fid LEFT JOIN rooms AS r ON info.room_id = r.room_id WHERE info.".$status_type." = ".$status;
			}
		}
		else  {
			$sql = "SELECT f.fname, r.room_name FROM room_status_info AS info LEFT JOIN floor_rooms AS f_rooms ON info.room_id = f_rooms.room_id LEFT JOIN floors AS f ON f.fid = f_rooms.fid LEFT JOIN rooms AS r ON info.room_id = r.room_id WHERE info.".$status_type." = ".$status." AND rented=1";
		}
		return $this->db->query($sql);
	}

	public function rooms_status_wise($battry_level=25) {
		$sql = "SELECT info.*, r.*, f.fid FROM room_status_info AS info LEFT JOIN rooms AS r ON r.room_id = info.room_id LEFT JOIN floor_rooms AS f ON f.room_id = r.room_id WHERE (info.iremote = 0 OR info.controller_status = 0 OR info.battry <= ".$battry_level.") AND digivalet_status = 1 LIMIT 0,20";
		return $this->db->query($sql);
	}

	public function delete_unchecked() {
		exec("chmod -R 777 /var/www");

		$pdo = new PDO('sqlite:/var/www/foodmenu/fooddb.sqlite');

		exec("chmod -R 777 /var/www");

		$pdo->exec("delete from food_maincategory where is_active=0");
		$pdo->exec("delete from food_subcategory where is_active=0");
		$pdo->exec("delete from food_menuitem_primary where is_active=0");
		$pdo->exec("delete from food_menuitem_details where is_active=0");
		 
		$pdo->exec("DELETE FROM food_menuitem_details WHERE mid NOT IN (SELECT `item_type_id` FROM food_application_images WHERE `item_type`='menuitem')");
		$pdo->exec("DELETE FROM food_menuitem_primary WHERE mid NOT IN (SELECT `item_type_id` FROM food_application_images WHERE `item_type`='menuitem')");
		$pdo->exec("VACUUM");

	}
}
?>
