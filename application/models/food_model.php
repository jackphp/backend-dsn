<?php
/**
 * Description of food_model
 *
 * @author pbsl
 */
class food_model extends CI_Model {

	public function  __construct() {
		parent::__construct();
	}

	public function get_food_data($param = array()) {
		if(count($param) > 0) {

			$condition = array();
			if(isset($param['room']) && $param['room'] != "all") {
				$condition[] = ' l.room_id = '.$param['room'];
			}

			if((isset($param['from_date']) && $param['from_date'] != "") && (isset($param['to_date']) && $param['to_date'] != "")) {
				$condition[] = ' l.event_start_time BETWEEN "'.$param['from_date'].'" AND "'.$param['to_date'].'" ';
			}

			if(count($condition) > 1) {
				$condi = implode(" AND ", $condition);
			}
			else {
				$condi = $condition[0];
			}

			$sql = "SELECT l.*, count(v.value) AS cnt, e.event_name, v.value FROM logs AS l LEFT JOIN log_events AS e ON l.eid = e.eid LEFT JOIN log_values AS v ON l.lid = v.lid WHERE e.eid = 22 AND v.name = 'food' AND ".$condi." GROUP BY v.value";
		}
		else {
			$sql = "SELECT l.*, count(v.value) AS cnt, e.event_name, v.value FROM logs AS l LEFT JOIN log_events AS e ON l.eid = e.eid LEFT JOIN log_values AS v ON l.lid = v.lid WHERE e.eid = 22 AND v.name = 'food' GROUP BY v.value";
		}
		return $this->db->query($sql);
	}
}
?>
