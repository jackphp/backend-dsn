<?php

class Movies_Model extends CI_Model {

	var $data = array();

	public function __construct() {
		parent::__construct();
	}

	public function insert_movie($file) {

		$this->db->set("file_location", $file);
		$this->db->set("privacy_level_id", 2);
		$id = $this->session->userdata("user_id");
		$this->db->set("owner_id ", $id);
		$result = $this->db->insert('movie');
		return mysql_insert_id();
	}
	//old all movie fatch query
	/* public function movie_list($sub_cat_id) {
	$id = $this->session->userdata("user_id");
	$search = $_REQUEST['search'];
	$sql.= "SELECT m.*,p.poster_id,p.url,p.filepath FROM movie AS m LEFT JOIN poster AS p ON p.movie_id=m.movie_id WHERE  m.`owner_id`= $id  OR `privacy_level_id`!=2";
	if (isset($search) && (!empty($search))) {
	$sql.=" AND m.title LIKE '%$search%'";
	}
	if (isset($sub_cat_id) && (!empty($sub_cat_id))) {
	$sql.=" AND m.movie_id IN (SELECT movie_id FROM user_subcategorizes_movie WHERE subcat_id=$sub_cat_id)";
	}

	// echo $sql;die('here');
	$sql.=" ORDER BY m.movie_id DESC";
	$query = $this->db->query($sql);
	return $query->result();
	}*/
	public function movie_list($sub_cat_id,$per_page,$page,$language) {
		$id = $this->session->userdata("user_id");
		$search = $_REQUEST['search'];

		if (isset($search) && (!empty($search))) {
			//$sql.=" AND m.title LIKE '%$search%'";
			$sql.="SELECT  * FROM (SELECT m.*,p.poster_id,p.url AS prul,p.filepath FROM movie AS m LEFT JOIN poster AS p ON p.movie_id=m.movie_id WHERE  m.`owner_id`= $id  OR `privacy_level_id`!=2) AS mov WHERE mov.title  LIKE '%$search%' ORDER BY mov.movie_id DESC";
		}
		else if (isset($sub_cat_id) && (!empty($sub_cat_id))) {
			//$sql.=" AND m.movie_id IN (SELECT movie_id FROM user_subcategorizes_movie WHERE subcat_id=$sub_cat_id)";
			$sql.="SELECT  * FROM (SELECT m.*,p.poster_id,p.url AS purl,p.filepath FROM movie AS m LEFT JOIN poster AS p ON p.movie_id=m.movie_id WHERE  m.`owner_id`= $id  OR `privacy_level_id`!=2)  AS mov WHERE mov.movie_id IN (SELECT movie_id FROM user_subcategorizes_movie WHERE subcat_id=$sub_cat_id) ORDER BY mov.movie_id DESC";
		}
		else if (isset($language) && (!empty($language))) {
			//$sql.=" AND m.movie_id IN (SELECT movie_id FROM user_subcategorizes_movie WHERE subcat_id=$sub_cat_id)";
			$sql.="SELECT  * FROM (SELECT m.*,p.poster_id,p.url AS purl,p.filepath FROM movie AS m LEFT JOIN poster AS p ON p.movie_id=m.movie_id WHERE  m.`owner_id`= $id  OR `privacy_level_id`!=2)  AS mov WHERE mov.movie_id IN (SELECT DISTINCT (movie_id) FROM   language WHERE language='$language') ORDER BY mov.movie_id DESC";
		}
		else{
			$sql.= "SELECT m.*,p.poster_id,p.url,p.filepath FROM movie AS m LEFT JOIN poster AS p ON p.movie_id=m.movie_id WHERE  m.`owner_id`= $id  OR `privacy_level_id`!=2 ORDER BY m.movie_id DESC";
		}
		$sql.=" LIMIT $page,$per_page";

		// $sql.=" ORDER BY m.movie_id DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function movie_list_count($sub_cat_id,$language) {
		$id = $this->session->userdata("user_id");
		$search = $_REQUEST['search'];

		//         if (isset($sub_cat_id) && (!empty($sub_cat_id))) {
		//            $sql=" AND movie_id IN (SELECT movie_id FROM user_subcategorizes_movie WHERE subcat_id=$sub_cat_id)";
		//        }
		if (isset($search) && (!empty($search))) {
			$sql="SELECT * FROM (SELECT movie_id AS  total_movie,title FROM movie  WHERE owner_id=$id OR `privacy_level_id`!=2)AS mov WHERE mov.title LIKE '%$search%'";
			$query = $this->db->query($sql);
			return $this->db->affected_rows();
		}else   if (isset($sub_cat_id) && (!empty($sub_cat_id))){
			$sql = "SELECT DISTINCT(movie_id) FROM  user_subcategorizes_movie WHERE `subcat_id`=$sub_cat_id";
			$count=mysql_query($sql);
			return $num_rows = mysql_num_rows($count);
		}
		else   if (isset($language) && (!empty($language))){
			$sql = "SELECT DISTINCT(movie_id) FROM  language WHERE `language`='$language'";
			$count=mysql_query($sql);
			return $num_rows = mysql_num_rows($count);
		}else{
			$sql = "SELECT movie_id AS  total_movie FROM movie  WHERE owner_id=$id OR `privacy_level_id`!=2";
			$query = $this->db->query($sql);
			return $this->db->affected_rows();
		}
		// echo $sql;

}

public function movie_list_title() {
	$id = $this->session->userdata("user_id");
	$query = $this->db->query("select tilte FROM movie  WHERE owner_id=" . $id . " ORDER BY title DESC");
	return $query->result();
}

public function movie_delete($movie_id) {
	$this->db->where("movie_id", $movie_id);
	$this->db->delete('movie');
}

public function movie_edit($movie_id) {
	$id = $this->session->userdata("user_id");
	$query = "SELECT * FROM `movie` WHERE `movie_id` = '$movie_id'  LIMIT 0,1";
	$query_1 = $this->db->query($query);
	if ($query_1) {
		//movie information array
		$movie = $query_1->result_array();

		//Get usersub category information
		// $query_2 = $this->db->query("SELECT DISTINCT * FROM `user_subcategorizes_movie` WHERE `movie_id` = '$movie_id' AND `user_id` = '$id'");
		$query_2 = $this->db->query("SELECT DISTINCT * FROM `user_subcategorizes_movie` WHERE `movie_id` = $movie_id AND `subcat_id` IN(SELECT `subcat_id` FROM movie_subcategory WHERE `owner_id`=1)");
		// user category array
		$user_subcategories = $query_2->result_array();

		//Get movie poster information
		$query_3 = $this->db->query("SELECT * FROM `poster` WHERE `movie_id` = '$movie_id' LIMIT 0,1");
		// user category array
		$poster = $query_3->result_array();

		//Get movie user rating information
		$query_4 = $this->db->query("SELECT * FROM `user_rates_movie` WHERE `movie_id` = '$movie_id' AND `user_id` = '$id' LIMIT 0,1");
		// user category array
		$user_rating = $query_4->result_array();

		$query_4 = $this->db->query("SELECT DISTINCT language FROM `language` WHERE `movie_id`= '$movie_id'");
		// user category array
		$movie_lang = $query_4->result_array();

		return $rt_arr = array($movie, $user_subcategories, $poster, $user_rating, $movie_lang);

	} else {
		return false;
	}
}

public function get_movie_categories() {
	$id = $this->session->userdata("user_id");
	$ret_arr[] = '';
	//  $query_str = " 	SELECT * FROM `movie_category` WHERE `owner_id` = '$id' ORDER BY `cat_id` ASC";
	//added by satish category are commen for all user so removeowner id
	$query_str = " 	SELECT * FROM `movie_category` ORDER BY `cat_id` ASC";
	$resource = mysql_query($query_str);
	while ($obj = mysql_fetch_object($resource)) {
		$ret_arr[$obj->cat_id] = $obj->name;
	}
	return $ret_arr;
}

public function get_movie_sub_categories_parent() {
	$id = $this->session->userdata("user_id");
	$ret_arr[] = '';
	//$query_str = " 	SELECT * FROM `movie_subcategory` WHERE `owner_id` = '$id' ORDER BY `subcat_id` ASC";
	$query_str = "SELECT * FROM `movie_subcategory`  ORDER BY `subcat_id` ASC";
	// $query_str="SELECT * FROM (SELECT us.*, ms.parent_cat  FROM `user_subcategorizes_movie`as us JOIN movie_subcategory as ms ON us.`subcat_id`=ms.`subcat_id` WHERE us.movie_id=$movie_id) AS total WHERE total.user_id=$id OR total.parent_cat=3";
	$resource = mysql_query($query_str);
	while ($obj = mysql_fetch_object($resource)) {
		$ret_arr[$obj->subcat_id] = $obj->parent_cat;
	}
	return $ret_arr;
}

public function get_movie_language() {
	$ret_arr[] = '';
	$query_str = "SELECT * FROM `language`";
	$resource = mysql_query($query_str);
	while ($obj = mysql_fetch_object($resource)) {
		$ret_arr[$obj->movie_id] = $obj->movie_id;
	}
	return $ret_arr;
}

public function get_movie_sub_categories() {
	$id = $this->session->userdata("user_id");
	$ret_arr[] = '';
	// $query_str = " 	SELECT * FROM `movie_subcategory` WHERE `owner_id` = '$id' ORDER BY `subcat_id` ASC";
	$query_str = "SELECT * FROM `movie_subcategory` WHERE owner_id=$id ORDER BY `subcat_id` ASC";
	$resource = mysql_query($query_str);
	while ($obj = mysql_fetch_object($resource)) {
		$ret_arr[$obj->subcat_id] = $obj->name;
	}
	return $ret_arr;
}

public function movie_detail() {
	$id = $this->session->userdata("user_id");
	$query_str = " 	SELECT * FROM `movie` WHERE `owner_id` = '$id' ORDER BY `title` ASC";
	$resource = mysql_query($query_str);
	if (mysql_num_rows($resource) > 0) {
		return $resource;
	} else {
		return false;
	}
}

public function movie_list_options() {
	$list = $this->movie_detail();
	if ($list) {
		$rt_option = '';
		while ($obj = mysql_fetch_object($list)) {
			if ($obj->title != "") {
				$rt_option .= '<option value="' . $obj->title . '">' . $obj->title . '</option>';
			}
		}
		return $rt_option;
	} else {
		return false;
	}
}

public function capture_poster_from_url($url,$filepath) {
	$content = file_get_contents($url, "r");
	//print_r($content);die('here');
	$fp = fopen($filepath, "w");
	if (fwrite($fp, $content)) {
		$rt = true;
	} else {
		$rt = false;
	}
	fclose($fp);
	return $rt;
}

public function smart_insert_subcategorizes_movie($sub_category_id,$movie_id) {
	$id = $this->session->userdata("user_id");
	$this->db->set("user_id", $id);
	$this->db->set("subcat_id", $sub_category_id);
	$this->db->set("movie_id", $movie_id);
	$result = $this->db->insert('user_subcategorizes_movie');
	return mysql_insert_id();
}

public function smart_insert_movie_languages($name, $movie_id) {

	$this->db->set("language", $name);
	$this->db->set("movie_id", $movie_id);
	$result = $this->db->insert('language');
	return mysql_insert_id();
}

public function smart_insert_movie_subcategory($name, $gener_id) {
	$id = $this->session->userdata("user_id");
	$this->db->set("owner_id", $id);
	$this->db->set("name", $name);
	$this->db->set("parent_cat", $gener_id);
	$result = $this->db->insert('movie_subcategory');
	return mysql_insert_id();
}

public function smart_insert_categorizes_movie($cat_id, $movie_id) {
	$id = $this->session->userdata("user_id");
	$this->db->set("user_id", $id);
	$this->db->set("cat_id", $cat_id);
	$this->db->set("movie_id", $movie_id);
	$result = $this->db->insert('user_categorizes_movie');
	return mysql_insert_id();
}

public function smart_insert_user_rating($user_raing, $movie_id) {
	$id = $this->session->userdata("user_id");
	$this->db->set("user_id", $id);
	$this->db->set("rating", $user_raing);
	$this->db->set("movie_id", $movie_id);
	$result = $this->db->insert('user_rates_movie');
	return mysql_insert_id();
}

public function smart_insert_movie_studio($studio_id, $movie_id) {
	$this->db->set("movie_id", $movie_id);
	$this->db->set("studio_id", $studio_id);
	$result = $this->db->insert('movie_studio');
	return mysql_insert_id();
}

public function smart_insert_studio($name, $url, $id) {

	$this->db->set("name", $name);
	$this->db->set("url", $url);
	$this->db->set("tmdb_studio_id", $id);
	$result = $this->db->insert('studio');
	return mysql_insert_id();
}

public function smart_insert_poster($filename, $movie_id, $thumb_url) {
	$query = "SELECT poster_id FROM `poster` WHERE `movie_id`=$movie_id";
	$que= mysql_query($query);
	$rows = mysql_num_rows($que);
	if($rows==1){
		//update into poster table
		 
		$this->db->set("url", $thumb_url);
		$this->db->set("filepath", $filename);
		$this->db->where("movie_id", $movie_id);
		$result = $this->db->update('poster');
		return mysql_insert_id();
	}else{
		//insert into poster table
		$this->db->set("movie_id", $movie_id);
		$this->db->set("url", $thumb_url);
		$this->db->set("filepath", $filename);
		$result = $this->db->insert('poster');
		return mysql_insert_id();
	}
	 

}

public function smart_update_movie($user_id, $tmdb_id, $imdb_id, $title, $url, $overview, $movie_rating, $movie_runtime, $movie_adult, $movie_crtify, $movie_released, $movie_type, $trailer, $lang_id,$movie_producers,$movie_director,$movie_writer, $movie_id) {
	// echo "update";
	if (isset($tmdb_id) && (!empty($tmdb_id))) {
		$this->db->set("tmdb_id", $tmdb_id);
	}
	if (isset($imdb_id) && (!empty($imdb_id))) {
		$this->db->set("imdb_id", $imdb_id);
	}
	if (isset($title) && (!empty($title))) {
		$this->db->set("title", $title);
	}
	if (isset($url) && (!empty($url))) {
		$this->db->set("url", $url);
	}
	if (isset($overview) && (!empty($overview))) {
		$this->db->set("overview", $overview);
	}
	if (isset($movie_rating) && (!empty($movie_rating))) {
		$this->db->set("global_rating", $movie_rating);
	}
	if (isset($movie_runtime) && (!empty($movie_runtime))) {
		$this->db->set("runtime", $movie_runtime);
	}
	if (isset($movie_adult) && (!empty($movie_adult))) {
		$this->db->set("suited_for_kids", $movie_adult);
	}
	if (isset($movie_crtify) && (!empty($tmdb_id))) {
		$this->db->set("parental_rating", $movie_crtify);
	}
	if (isset($movie_released) && (!empty($movie_released))) {
		$this->db->set("release_date", $movie_released);
	}
	if (isset($movie_producers) && (!empty($movie_producers))) {
		$this->db->set("movie_producer",$movie_producers);
	}
	if (isset($movie_writer) && (!empty($movie_writer))) {
		$this->db->set("movie_director", $movie_writer);
	}
	if (isset($movie_writer) && (!empty($movie_writer))) {
		$this->db->set("movie_writer", $movie_writer);
	}

	$this->db->set("privacy_level_id", $movie_type);

	if (isset($trailer) && (!empty($trailer))) {
		$this->db->set("trailer_url", $trailer);
	}
	if (isset($lang_id) && (!empty($lang_id))) {
		$this->db->set("lang_id", $lang_id);
	}
	$this->db->where("movie_id", $movie_id);
	$result = $this->db->update('movie');
}

public function ImageProcess($image, $crop = null, $size = null) {
	$imgprocessed = $image;
	$image = ImageCreateFromString(file_get_contents($imgprocessed));

	if (is_resource($image) === true) {
		$x = 0;
		$y = 0;
		$width = imagesx($image);
		$height = imagesy($image);

		/*
		 CROP (Aspect Ratio) Section
		 */

		if (is_null($crop) === true) {
			$crop = array($width, $height);
		} else {
			$crop = array_filter(explode(':', $crop));

			if (empty($crop) === true) {
				$crop = array($width, $height);
			} else {
				if ((empty($crop[0]) === true) || (is_numeric($crop[0]) === false)) {
					$crop[0] = $crop[1];
				} else if ((empty($crop[1]) === true) || (is_numeric($crop[1]) === false)) {
					$crop[1] = $crop[0];
				}
			}

			$ratio = array(0 => $width / $height, 1 => $crop[0] / $crop[1]);

			if ($ratio[0] > $ratio[1]) {
				$width = $height * $ratio[1];
				$x = (imagesx($image) - $width) / 2;
			} else if ($ratio[0] < $ratio[1]) {
				$height = $width / $ratio[1];
				$y = (imagesy($image) - $height) / 2;
			}
		}

		/*
		 Resize Section
		 */

		if (is_null($size) === true) {
			$size = array($width, $height);
		} else {
			$size = array_filter(explode('x', $size));

			if (empty($size) === true) {
				$size = array(imagesx($image), imagesy($image));
			} else {
				if ((empty($size[0]) === true) || (is_numeric($size[0]) === false)) {
					$size[0] = round($size[1] * $width / $height);
				} else if ((empty($size[1]) === true) || (is_numeric($size[1]) === false)) {
					$size[1] = round($size[0] * $height / $width);
				}
			}
		}

		$result = ImageCreateTrueColor($size[0], $size[1]);

		if (is_resource($result) === true) {
			ImageSaveAlpha($result, true);
			ImageAlphaBlending($result, true);
			ImageFill($result, 0, 0, ImageColorAllocate($result, 255, 255, 255));
			ImageCopyResampled($result, $image, 0, 0, $x, $y, $size[0], $size[1], $width, $height);

			ImageInterlace($result, true);
			ImageJPEG($result, $imgprocessed, 90);
		}
	}

	return false;
}

public function movie_file($movies_id) {
	$query = "SELECT file_location FROM movie WHERE movie_id=$movies_id";
	$query = mysql_query($query);
	return $result = mysql_fetch_assoc($query);
}

public function movie_subcategory_delete($movie_id, $subcategory) {
	$query = "DELETE FROM `user_subcategorizes_movie` WHERE movie_id=" . $movie_id . " AND subcat_id=(SELECT `subcat_id` FROM movie_subcategory WHERE `name`='$subcategory')";
	$query = mysql_query($query);
	return $this->db->affected_rows();
}

public function movie_languages_delete($movie_id) {
	$query = "DELETE FROM `language` WHERE `movie_id`=$movie_id";
	$query = mysql_query($query);
	return $this->db->affected_rows();
}

public function movie_details($movies_id) {
	$query = "SELECT m.movie_id,m.title,m.trailer_url,m.url,m.overview,m.release_date,m.movie_director,m.movie_producer,m.movie_writer,m.runtime,m.suited_for_kids,m.parental_rating,s.studio_id,s.name FROM `movie` AS m LEFT JOIN movie_studio AS ms ON m.movie_id=ms.movie_id LEFT JOIN studio AS s ON s.studio_id=ms.studio_id WHERE m.movie_id=$movies_id";
	$query = mysql_query($query);
	return $result = mysql_fetch_assoc($query);
}
public function get_subcat_name($movie_id){
	$sql="SELECT DISTINCT(ms.name) FROM  movie_subcategory AS ms JOIN  user_subcategorizes_movie AS usm ON ms.subcat_id=usm.subcat_id WHERE usm.movie_id=$movie_id AND ms.parent_cat=2";
	$res = mysql_query($sql);
	while ($obj = mysql_fetch_object($res)) {
		$sub_arr[] = $obj->name;
	}
	return $sub_arr;
}
public function get_subcat_geners($movie_id){
	$sql="SELECT DISTINCT(ms.name) FROM  movie_subcategory AS ms JOIN  user_subcategorizes_movie AS usm ON ms.subcat_id=usm.subcat_id WHERE usm.movie_id=$movie_id AND ms.parent_cat=1";
	$res = mysql_query($sql);
	while ($obj = mysql_fetch_object($res)) {
		$sub_arr[] = $obj->name;
	}
	return $sub_arr;
}
public function get_movie_poster($movie_id){
	$sql="SELECT filepath FROM  poster WHERE movie_id=$movie_id";
	$res = mysql_query($sql);
	return $result = mysql_fetch_assoc($res);
}
public function delete_user_subcategorizes_movie($movie_id){
	$query = "DELETE FROM `user_subcategorizes_movie` WHERE `movie_id`= $movie_id";
	$query = mysql_query($query);
	return $this->db->affected_rows();
}
public function delete_user_categorizes_movie($movie_id){
	$query = "DELETE FROM `user_categorizes_movie` WHERE `movie_id`= $movie_id";
	$query = mysql_query($query);
	return $this->db->affected_rows();
}
public function poster_upload($movies_id,$image_name){
	$path = $_SERVER['DOCUMENT_ROOT'] . '/mymovie/images/poster/';
	$valid_formats = array("jpg","Jpg", "png", "gif", "bmp", "jpeg");
	if (isset($_POST)) {
		$name = $_FILES['thumbURL']['name'];
		$size = $_FILES['thumbURL']['size'];
		if (strlen($name)) {
			list($txt, $ext) = explode(".", $name);
			if (in_array($ext, $valid_formats)) {
				if ($size < (1024 * 1024)) { // Image size max 1 MB
					$actual_image_name = time() . "." . $ext;//change file name with time
					$tmp = $_FILES['thumbURL']['tmp_name'];
					if (move_uploaded_file($tmp, $path . $actual_image_name)) {
						//check avalibality of movie id
						$this->ImageProcess($path . $actual_image_name, '', "");
						$query = "SELECT poster_id FROM `poster` WHERE `movie_id`=$movies_id";
						$que= mysql_query($query);
						$rows = mysql_num_rows($que);
						if($rows==1){
							//update into poster table
							$this->db->set("filepath", $actual_image_name);
							$this->db->where("movie_id", $movies_id);
							$result = $this->db->update('poster');
						}else{
							//insert into poster table
							$this->db->set("movie_id", $movies_id);
							$this->db->set("filepath", $actual_image_name);
							$result = $this->db->insert('poster');
						}
					}
					else
					return "failed";
				}
				else
				return "Image file size max 1 MB";
			}
			else
			return "Invalid file format";
		}
		else
		return "Please select image";
	}
	 
}
 
public function getUserName()
{
	$user_id = $this->session->userdata("user_id");
	$query = "SELECT name FROM user WHERE user_id=$user_id";
	$query = mysql_query($query);
	return $result = mysql_fetch_assoc($query);
}
public function save_position($pos){
	$len=  count($pos['pos']);
	$j=1;
	for($i=0;$i<$len;$i++){
		$query="UPDATE movie_category set pos =".$j." WHERE cat_id=".$pos[pos][$i];
		mysql_query($query);
		$j++;
	}
}
public function delete_subcat($subcat_id)
{
	$user_id = $this->session->userdata("user_id");
	$this->db->where('subcat_id', $subcat_id);
	$this->db->where('user_id', $user_id);
	$this->db->delete('user_subcategorizes_movie');

	$this->db->where('subcat_id', $subcat_id);
	$this->db->where('owner_id', $user_id);
	$this->db->delete('movie_subcategory');
}
public function get_category_by_id($category_id)
{
	$query = "SELECT `name` FROM `movie_category` WHERE `cat_id`=$category_id";
	$query = mysql_query($query);
	return $result = mysql_fetch_assoc($query);
}
public function get_movie_catsub_categories($catid_id) {
	$id = $this->session->userdata("user_id");
	$ret_arr[] = '';
	// $query_str = " 	SELECT * FROM `movie_subcategory` WHERE `owner_id` = '$id' ORDER BY `subcat_id` ASC";
	$query_str = " 	SELECT * FROM `movie_subcategory` WHERE parent_cat = $catid_id ORDER BY `subcat_id` ASC";
	$resource = mysql_query($query_str);
	while ($obj = mysql_fetch_object($resource)) {
		$ret_arr[$obj->subcat_id] = $obj->name;
	}
	return $ret_arr;
}
public function getTotalMOvieCount(){
	$this->db->from('movie');
	$query = $this->db->get();
	return $rowcount = $query->num_rows();
}
public function getallcatsubcat($subcatid)
{
	$id = $this->session->userdata("user_id");
	$query = "SELECT `cat_id`,`name` FROM `movie_category` ORDER BY name";
	if(($subcatid!="")&&($subcatid!="English")&&($subcatid!="Hindi") ){
		$sub=$this->getSubCAtegoryById($subcatid);
		$catname = "SELECT `name` FROM `movie_category` WHERE cat_id=".$sub['parent_cat'];
		$catname = mysql_query($catname);
		$parent_cat= mysql_fetch_assoc($catname);
	}else{
		$lang=$this->uri->segment(3);
		$lang_name=$this->uri->segment(4);
		$language=$lang." > ".$lang_name;
	}
	$query = mysql_query($query);
	$str.="<ul class='sf-menu' id='example' style='margin:0px;'>";
	$str.="<li class='current' style='margin:6px'>";
	if($parent_cat){
		$str.="<a href='".base_url()."index.php/movies/show_movies'>Category > ".$parent_cat['name'] ." > ".$sub['name']."</a>";
	}else if($lang!='language'){
		$str.="<a href='".base_url()."index.php/movies/show_movies'>Category > All</a>";
	}else{
		$str.="<a href='#'>Category > $language</a>";
	}
	$str.="<ul>";
	$str.="<li class='current' style='width:150px;'><a href='".base_url()."index.php/movies/show_movies'>All</a></li>";

	while ($category = mysql_fetch_array($query)){
		$str.="<li class='current' style='width:150px;'>";
		$str.="<a href='#'>".$category['name']."</a>";
		$str.="<ul>";
		$sql = "SELECT `subcat_id`,`name` FROM movie_subcategory WHERE `parent_cat`=".$category['cat_id']." AND parent_cat IN(1,2,3) AND owner_id=$id ORDER BY name";
		$sql = mysql_query($sql);
		$str.="<li class='current'>";
		if($category['cat_id']!="4"){
			while ($sub_category = mysql_fetch_array($sql)){
				$que="SELECT DISTINCT(movie_id) FROM  user_subcategorizes_movie WHERE `subcat_id`=".$sub_category['subcat_id']." AND user_id=$id";
				$count=mysql_query($que);
				$num_rows = mysql_num_rows($count);
				$str.=" <a href='".base_url()."index.php/movies/show_movies/subcategory/".$sub_category['subcat_id']."' class='orange-button-slim genre'>".trim($sub_category['name'])."(".$num_rows.")"."</span></a>";
			}}
			if($category['cat_id']=="4")
			{
				$que=$this->db->query("SELECT DISTINCT(language) FROM  language WHERE `language` IN('English','Hindi')");
				$result= $que->result();
				foreach($result as $sub_lang){
					$movie_count="SELECT DISTINCT(movie_id) FROM language WHERE language='$sub_lang->language'";
					$movie_count=mysql_query($movie_count);
					$mov_rows = mysql_num_rows($movie_count);
					if($sub_lang){
						$str.=" <a href='".base_url()."index.php/movies/show_movies/language/".$sub_lang->language."'  class='orange-button-slim genre'>".trim($sub_lang->language)."(".$mov_rows.")"."</span></a>";

					}}}
					 
					$str.="</li>";
					$str.="</ul>";
					$str.="</li>";
	}
	$str.="</ui>";
	$str.="</ul>";
	$str.="</li>";
	 
	return $str;
}
public function update_movie_poster($actual_image_name,$movies_id){
	$query = "SELECT poster_id FROM `poster` WHERE `movie_id`=$movies_id";
	$que= mysql_query($query);
	$rows = mysql_num_rows($que);
	if($rows==1){
		//update into poster table
		$this->db->set("filepath", $actual_image_name);
		$this->db->where("movie_id", $movies_id);
		$result = $this->db->update('poster');
	}else{
		//insert into poster table
		$this->db->set("movie_id", $movies_id);
		$this->db->set("filepath", $actual_image_name);
		$result = $this->db->insert('poster');
	}
	return "Upload Sucessfully";
}
public function getSubCAtegoryById($subcatid)
{

	$query = "SELECT `name`,parent_cat FROM `movie_subcategory` WHERE `subcat_id`=$subcatid";
	$query = mysql_query($query);
	return $result = mysql_fetch_assoc($query);
}
}

?>
