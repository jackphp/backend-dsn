<?php
/**
 * Description of rms_model
 *
 * @author pbsl
 */
class rms_model extends CI_Model {
	public function  __construct() {
		parent::__construct();
	}

	public function get_ac_event_data($eid, $room_id = "", $floor_id="", $from_date, $to_date="") {
		if($to_date != "") {
			$sql = "SELECT l.*, lv.*, fr.fid, f.fname, r.room_name, fr.room_id FROM logs AS l LEFT JOIN log_values AS lv ON l.lid = lv.lid LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id  WHERE (Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."') AND l.eid=".$eid;
		}
		else {
			$sql = "SELECT l.*, lv.*, fr.fid, f.fname, r.room_name, fr.room_id FROM logs AS l LEFT JOIN log_values AS lv ON l.lid = lv.lid LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id  WHERE (Date(l.event_start_time) = '".$from_date."' ) AND l.eid=".$eid;
		}

		if($room_id != "") {
			$sql .= " AND l.room_id=".$room_id;
		}

		if($floor_id != "") {
			$sql .= " AND fr.fid=".$floor_id;
		}

		$sql .= " AND lv.name IN('room_temperature','ac_temperature')";
		//echo $sql;
		return $this->db->query($sql);
	}


	public function get_event_data($eid, $room_id = "", $floor_id="", $from_date, $to_date="", $value_name="") {
		if($to_date != "") {
			$sql = "SELECT l.*, fr.fid, f.fname, r.room_name, fr.room_id FROM logs AS l LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id  WHERE (Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."') AND l.eid=".$eid;
		}
		else {
			$sql = "SELECT l.*, fr.fid, f.fname, r.room_name, fr.room_id FROM logs AS l LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id  WHERE (Date(l.event_start_time) = '".$from_date."' ) AND l.eid=".$eid;
		}

		if($room_id != "") {
			$sql .= " AND l.room_id=".$room_id;
		}

		if($floor_id != "") {
			$sql .= " AND fr.fid=".$floor_id;
		}

		return $this->db->query($sql);
	}

	/*select a.value, a.event_start_time, b.event_start_time from (SELECT l.*, v.name,v.value FROM logs as l, log_values as v WHERE v.lid=l.lid and v.name='light_name' and l.eid=8 order by v.value, l.lid) as a, (SELECT l.*, v.name, v.value FROM logs as l, log_values as v WHERE v.lid=l.lid and v.name='light_name' and l.eid=9 order by v.value, l.lid) as b where a.value=b.value */

	public function get_ac_on_of_commands($eid, $room_id = "", $floor_id="", $from_date, $to_date="") {
		$condition = "";
		if($floor_id > 0) {
			$condition = ' AND f.fid = '.$floor_id;
		}

		if($room_id > 0) {
			$condition = ' AND l.room_id = '.$room_id;
		}

		$sql = "SELECT count(l.room_id) AS ctr FROM logs AS l LEFT JOIN floor_rooms AS f ON f.room_id = l.room_id  WHERE (Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."') AND eid= ".$eid." ".$condition." GROUP BY l.room_id";

		return $this->db->query($sql);
	}

	public function get_avg_floor_temperature($eid, $floor_id, $from_date, $to_date="") {

		$avg_room_ac_temp = array('avg_room_temp'=>'', 'avg_ac_temp'=>'');

		$sql = "SELECT AVG(lv.value) AS average FROM logs AS l LEFT JOIN log_values AS lv ON l.lid = lv.lid LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id WHERE (Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."') AND l.eid=".$eid." AND lv.name='room_temperature' AND fr.fid = ".$floor_id;
		$q1 = $this->db->query($sql);
		$avg_room_temp = $q1->result();

		$sql2 = "SELECT AVG(lv.value) AS average FROM logs AS l LEFT JOIN log_values AS lv ON l.lid = lv.lid LEFT JOIN floor_rooms AS fr ON l.room_id = fr.room_id LEFT JOIN floors AS f ON fr.fid = f.fid LEFT JOIN rooms AS r ON r.room_id = fr.room_id WHERE (Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."') AND l.eid=".$eid." AND lv.name='ac_temperature' AND fr.fid = ".$floor_id;
		$q2 = $this->db->query($sql2);
		$avg_ac_temp = $q2->result();

		if(isset($avg_room_temp[0]) && $avg_room_temp[0]->average != "") {
			$avg_room_ac_temp['avg_room_temp'] = (int)$avg_room_temp[0]->average;
		}

		if(isset($avg_ac_temp[0]) && $avg_ac_temp[0]->average != "") {
			$avg_room_ac_temp['avg_ac_temp'] = (int)$avg_ac_temp[0]->average;
		}

		return $avg_room_ac_temp;
	}

	/****
	 * Getting information of room
	 */
	public function get_room_info($room_id) {
		$sql = "SELECT r.*, f.* FROM rooms AS r LEFT JOIN floor_rooms AS fr ON fr.room_id = r.room_id LEFT JOIN floors AS f ON f.fid = fr.fid WHERE r.room_id = ".$room_id;
		return $this->db->query($sql);
	}

	/***
	 * TV Reporting
	 * data
	 */
	public function get_tv_on_off_command_data($from_date, $to_date="") {
		if($from_date != "" && $to_date != "") {
			$condition = "(Date(l.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."')";
		}
		else {
			$condition = "(Date(l.event_start_time) = '".$from_date."')";
		}

		$sql = "SELECT l.*, e.* FROM logs AS l LEFT JOIN log_events AS e ON l.eid = e.eid WHERE e.event_name IN('tv_on','tv_off') AND ".$condition;
		return $this->db->query($sql);
	}


	/***
	 * Lights and Dimmer
	 * Rporting data
	 */
	public function get_lights_on_off_command_data($from_date, $to_date="", $room_id="", $light_name="") {
		if($from_date != "" && $to_date != "") {
			$condition = "(Date(light_on.event_start_time) BETWEEN '".$from_date."' AND '".$to_date."')";
		}
		else {
			$condition = "(Date(light_on.event_start_time) = '".$from_date."')";
		}

		if($room_id != "") {
			$condition .= " AND light_off.room_id IN(".$room_id.") ";
		}

		if($light_name != "") {
			$condition .= " AND light_off.value = '".$light_name."' ";
		}

		$sql = "SELECT light_on.lid AS on_lid, COUNT(light_on.lid) AS ctr, light_off.lid AS off_lid, light_on.event_start_time AS evt_start_time, light_off.event_start_time AS evt_end_time, Date(light_on.event_start_time) AS evt_date, light_off.room_id, TIMEDIFF(light_off.event_start_time, light_on.event_start_time) AS diff, light_off.name, light_off.value, r.room_name FROM light_on, light_off LEFT JOIN rooms AS r ON light_off.room_id = r.room_id WHERE light_off.value = light_on.value AND light_off.room_id = light_on.room_id AND light_off.lid > light_on.lid AND".$condition." GROUP BY light_on.lid ORDER BY evt_date ASC";

		return $this->db->query($sql);
	}

	/**
	 * Getting TV Usages Data
	 *
	 * **/
	public function get_tv_usages_data_room_wise($param = array()) {
		$condition = array();
		if(isset($param['rooms']) && $param['rooms'] != "") {
			$condition[] = " tv.room_no IN(".$param['rooms'].") ";
		}

		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " tv.report_time BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " tv.report_time = '".$param['from_date']."' ";
			}
		}

		if(isset($param['room_type']) && $param['room_type'] != "") {
			$condition[] = " room.room_type = '".$param['room_type']."' ";
		}

		$condition = implode(' AND ', $condition);

		$sql = "SELECT tv.*, SEC_TO_TIME(SUM(TIME_TO_SEC(tv.view_hrs))) AS total_hrs, room.* FROM tv_usages AS tv LEFT JOIN rooms AS room ON room.room_name = tv.room_no WHERE ".$condition." GROUP BY tv.room_no";

		return $this->db->query($sql);
	}

	/**
	 * Getting Channel Usages Data
	 *
	 * **/
	public function get_channel_usages_data($param = array()) {
		$condition = array();
		if(isset($param['rooms']) && $param['rooms'] != "") {
			$condition[] = " channel.room_no IN(".$param['rooms'].") ";
		}

		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " DATE(channel.end_time) BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " DATE(channel.end_time) = '".$param['from_date']."' ";
			}
		}

		if(isset($param['room_type']) && $param['room_type'] != "") {
			$condition[] = " room.room_type = '".$param['room_type']."' ";
		}

		$condition = implode(' AND ', $condition);
		$sql = "SELECT channel.*, SEC_TO_TIME(SUM(TIME_TO_SEC(channel.total_view_hrs))) AS total_hrs, room.* FROM tv_channel_details AS channel LEFT JOIN rooms AS room ON room.room_name = channel.room_no WHERE ".$condition." AND channel.channel_name <> 'NA' GROUP BY channel.channel_name";
		return $this->db->query($sql);
	}


	public function get_tv_channel_data_hour_wise($param = array()) {
		$condition = array();
		if(isset($param['rooms']) && $param['rooms'] != "") {
			$condition[] = " channel.room_no IN(".$param['rooms'].") ";
		}

		if(isset($param['channel_name']) && $param['channel_name'] != "") {
			$condition[] = " channel.channel_name = '".$param['channel_name']."' ";
		}

		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " DATE(channel.end_time) BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " DATE(channel.end_time) = '".$param['from_date']."' ";
			}
		}

		if(isset($param['room_type']) && $param['room_type'] != "") {
			$condition[] = " room.room_type = '".$param['room_type']."' ";
		}

		$condition = implode(' AND ', $condition);

		$sql = "SELECT HOUR(end_time) AS hour, channel_name, SEC_TO_TIME(SUM(TIME_TO_SEC(total_view_hrs))) AS total_hrs, room.* FROM tv_channel_details AS channel LEFT JOIN rooms AS room ON room.room_name = channel.room_no WHERE ".$condition." GROUP BY HOUR(end_time) ORDER BY HOUR(end_time)";
		return $this->db->query($sql);
	}


	public function get_floor_tv_usages_wise_hrs($param = array()) {
		$condition = array();
		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " tv.report_time BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " tv.report_time = '".$param['from_date']."' ";
			}
		}

		$condition = implode(' AND ', $condition);

		$sql = "SELECT f.fname, SEC_TO_TIME(SUM(TIME_TO_SEC(view_hrs))) AS total_hrs FROM floors AS f LEFT JOIN floor_rooms AS fr ON f.fid = fr.fid
                LEFT JOIN rooms AS r ON r.room_id = fr.room_id
                LEFT JOIN tv_usages AS tv ON tv.room_no = r.room_name
                WHERE ".$condition."
                GROUP BY f.fname
                ORDER BY total_hrs DESC";

		return $this->db->query($sql);
	}

	public function average_tv_usages($param = array()) {
		$condition = array();
		if(isset($param['rooms']) && $param['rooms'] != "") {
			$condition[] = " tv.room_no IN(".$param['rooms'].") ";
		}

		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " tv.report_time BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " tv.report_time = '".$param['from_date']."' ";
			}
		}

		if(isset($param['room_type']) && $param['room_type'] != "") {
			$condition[] = " room.room_type = '".$param['room_type']."' ";
		}

		$condition = implode(' AND ', $condition);

		$sql = "SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(tv.view_hrs))) AS avg_hrs FROM tv_usages AS tv LEFT JOIN rooms AS room ON room.room_name = tv.room_no WHERE ".$condition;
		return $this->db->query($sql);
	}

	public function average_tv_channel_viewed($param = array()) {
		$condition = array();
		if(isset($param['rooms']) && $param['rooms'] != "") {
			$condition[] = " room_no IN(".$param['rooms'].") ";
		}

		if(isset($param['channel_name']) && $param['channel_name'] != "") {
			$condition[] = " channel_name = '".$param['channel_name']."' ";
		}

		if(isset($param['from_date']) && $param['from_date'] != "") {
			if(isset($param['to_date']) && $param['to_date'] != "") {
				$condition[] = " DATE(end_time) BETWEEN '".$param['from_date']."' AND '".$param['to_date']."' ";
			}
			else {
				$condition[] = " DATE(end_time) = '".$param['from_date']."' ";
			}
		}

		if(isset($param['room_type']) && $param['room_type'] != "") {
			$condition[] = " room.room_type = '".$param['room_type']."' ";
		}

		$condition = implode(' AND ', $condition);

	}

	public function get_tv_on_off_event_data($from_date, $to_date="", $room_id="") {
		$sql = "SELECT tv_on.lid AS on_lid, tv_on.data AS on_data, tv_off.data AS off_data, tv_on.gid AS guest, COUNT(tv_on.lid) AS ctr, tv_off.lid AS off_lid, tv_on.event_start_time AS evt_start_time, tv_off.event_start_time AS evt_end_time, Date(tv_on.event_start_time) AS evt_date, tv_off.room_id, TIMEDIFF(tv_off.event_start_time, tv_on.event_start_time) AS diff, tv_off.name, tv_off.value, r.room_name
                 FROM tv_on, tv_off LEFT JOIN rooms AS r ON tv_off.room_id = r.room_id
                 WHERE tv_off.value = tv_on.value 
                 AND tv_off.room_id = tv_on.room_id
                 AND tv_off.lid > tv_on.lid
                 AND Date(tv_off.event_start_time) = '2011-05-31'
                 AND Date(tv_on.event_start_time) = '2011-05-31'
                 GROUP BY tv_on.lid ORDER BY evt_date ASC";
	}


	public function get_ac_temperature_data($param = array()) {
		if(count($param) > 0) {
			$condition = array();
			if(isset($param['room']) && $param['room'] != "all") {
				$condition[] = ' l.room_id = '.$param['room'];
			}

			if((isset($param['from_date']) && $param['from_date'] != "") && (isset($param['to_date']) && $param['to_date'] != "")) {
				$condition[] = ' l.event_start_time BETWEEN "'.$param['from_date'].'" AND "'.$param['to_date'].'" ';
			}

			if(count($condition) > 1) {
				$condi = implode(" AND ", $condition);
			}
			else {
				$condi = $condition[0];
			}

			$sql = "SELECT e.event_name, l.event_start_time, lv.name, lv.value FROM logs AS l
LEFT JOIN log_events AS e ON e.eid = l.eid
LEFT JOIN log_values AS lv ON lv.lid = l.lid
WHERE e.eid = 12 AND lv.name='ac_temperature' AND l.event_start_time<>'0000-00-00 00:00:00' AND ".$condi;
		}
		else {
			$sql = "SELECT e.event_name, l.event_start_time, lv.name, lv.value FROM logs AS l
LEFT JOIN log_events AS e ON e.eid = l.eid
LEFT JOIN log_values AS lv ON lv.lid = l.lid
WHERE e.eid = 12 AND lv.name='ac_temperature' AND l.event_start_time<>'0000-00-00 00:00:00'";

		}

		return $this->db->query($sql);
	}

	public function get_room_temperature($param = array()) {
		if(count($param) > 0) {
			//print_r($param); die();
			$condition = array();
			if(isset($param['room']) && $param['room'] != "all") {
				$condition[] = ' l.room_id = '.$param['room'];
			}

			if((isset($param['from_date']) && $param['from_date'] != "") && (isset($param['to_date']) && $param['to_date'] != "")) {
				$condition[] = ' l.event_start_time BETWEEN "'.$param['from_date'].'" AND "'.$param['to_date'].'" ';
			}

			if(count($condition) > 1) {
				$condi = implode(" AND ", $condition);
			}
			else {
				$condi = $condition[0];
			}

			$sql = "SELECT e.event_name, l.event_start_time, lv.name, lv.value FROM logs AS l
LEFT JOIN log_events AS e ON e.eid = l.eid
LEFT JOIN log_values AS lv ON lv.lid = l.lid
WHERE e.eid = 12 AND lv.name='room_temperature' AND ".$condi;
		}
		else {
			$sql = "SELECT e.event_name, l.event_start_time, lv.name, lv.value FROM logs AS l
LEFT JOIN log_events AS e ON e.eid = l.eid
LEFT JOIN log_values AS lv ON lv.lid = l.lid
WHERE e.eid = 12 AND lv.name='room_temperature' AND l.event_start_time<>'0000-00-00 00:00:00'";
		}

		return $this->db->query($sql);
	}

} // End of class
?>