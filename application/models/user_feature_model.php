<?php

class User_Feature_Model extends CI_Model {

	var $data = array();

	public function __construct() {
		$db2 = $this->load->database('iremote', TRUE);
		parent::__construct();
	}

	public function getAllProfile() {
		$query = "SELECT `profile_id`,`profile_name` FROM `dashboard_profile` order by profile_name";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	public function insert_user_module($data) {
		$this->db->set("user_name", $data['user_name']);
		$this->db->set("password", $data['password']);
		$this->db->set("profile_id", $data['profile_id']);
		$this->db->set("isDeleted", $data['isDeleted']);
		$this->db->set("user_display_name", $data['user_display_name']);
		$result = $this->db->insert('dashboard_user');
	}

	public function getAllUser() {
		$query = "SELECT du.*,dp.profile_name FROM dashboard_user AS du JOIN dashboard_profile AS dp ON du.profile_id=dp.profile_id";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	public function editUser($user_id) {
		$query = "SELECT du.*,dp.profile_name FROM dashboard_user AS du JOIN dashboard_profile AS dp ON du.profile_id=dp.profile_id WHERE du.user_id=$user_id";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	public function update_user($data) {
		$this->db->set("user_name", $data['user_name']);
		if (isset($_POST['password'])) {
			$this->db->set("password", $data['password']);
		}
		$this->db->set("profile_id", $data['profile_id']);
		$this->db->set("isDeleted", $data['isDeleted']);
		$this->db->set("user_display_name", $data['user_display_name']);
		$this->db->where("user_id", $data['user_id']);
		$result = $this->db->update('dashboard_user');
	}

	public function delete_user($user_id) {
		$this->db->where("user_id", $user_id);
		$result = $this->db->delete('dashboard_user');
	}

	public function get_module() {
		$query = $this->db->query("SELECT module_id,module_name FROM dashboard_module");
		return $query->result();
	}

	public function get_categories() {
		$query = $this->db->query("SELECT categories_id,categories_name FROM dashboard_module_categories WHERE categories_id=1");
		return $query->result();
	}

	public function insert_user_profile($data) {
		//insert date into profile table
		$this->db->set("profile_name", $data['profile_name']);
		$this->db->set("profile_display_name", $data['profile_display_name']);
		$result = $this->db->insert('dashboard_profile');
		//insert data into module profile table
		$profile_id = $this->db->insert_id();
		foreach ($data['module'] as $k => $v) {
			$mod = stristr($k, '_');
			$mod_id = trim(str_replace("_", "", $mod));
			$this->db->set("profile_id", $profile_id);
			$this->db->set("module_id", $mod_id);
			$result = $this->db->insert('dashboard_profile_module');
		}
	}
	public function get_username_avaliable($username)
	{
		$query = $this->db->query("SELECT `user_id` FROM `dashboard_user` WHERE `user_name` = '$username'");
		return $this->db->affected_rows();
	}

}

?>