<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of activity_model
 *
 * @author pbsl
 */
class activity_model extends CI_Model {
	public function  __construct() {
		parent::__construct();
	}

	public function get_users_activity() {
		$sql = "SELECT COUNT(w.wid) AS ctr, u.name, u.uid FROM watchdog AS w LEFT JOIN users AS u ON w.uid = u.uid WHERE w.uid<>0 GROUP BY u.name";
		return $this->db->query($sql);
	}

	public function get_user_activity($uid) {
		$sql = "SELECT w.module,w.ip, w.message, w.access_time, u.name, u.uid FROM watchdog AS w LEFT JOIN users AS u ON w.uid = u.uid WHERE w.uid=".$uid;
		return $this->db->query($sql);
	}
}
?>
