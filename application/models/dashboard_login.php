<?php
class dashboard_login extends CI_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	function validate_login($username, $password) {
		$db2 = $this->load->database('iremote', TRUE);
		$this->db->where('user_name', $username);
		$this->db->where('password', $password);
		$this->db->where('isDeleted', "1");
		// $this->db->where('is_login', "0");
		$query = $this->db->get('dashboard_user');

		//print_r($query);die();0

		if ($query->num_rows() == 1) {
			foreach ($query->result() as $row) {

				$data['user_id'] = $row->user_id;
				$data['name'] = $row->user_name;
				$data['profile_id'] = $row->profile_id;
			}
			$this->session->set_userdata($data);
			//insert into auditlog table
			$id = $this->session->userdata("user_id");
			$this->db->set("user", $id);
			$ip = $_SERVER['REMOTE_ADDR'];
			$this->db->set("ip", $ip);
			$this->db->set("event", "logged in");
			$result = $this->db->insert('auditlog');
			return 1;
		} else {
			//             $this->session->set_flashdata("msg","Invalid User Name or Password.");
			//             $this->session->set_flashdata("css_class","error");
			//             redirect("/login");
			return 0;
		}
	}

	function update_name($name) {
		$db2 = $this->load->database('iremote', TRUE);
		$this->db->set("user_display_name", $name);
		echo $id = $this->session->userdata("user_id");
		$this->db->where("user_id", $id);
		$result = $this->db->update('dashboard_user');
	}
	//    function update_islogin() {
	//        $db2 = $this->load->database('iremote', TRUE);
	//        $id = $this->session->userdata("user_id");
	//        $this->db->set("is_login",1);
	//        $this->db->where("user_id", $id);
	//        $result = $this->db->update('dashboard_user');
	//    }
	//    function user_logout() {
	//        $db2 = $this->load->database('iremote', TRUE);
	//        $id = $this->session->userdata("user_id");
	//        $this->db->set("is_login",0);
	//        $this->db->where("user_id", $id);
	//        $result = $this->db->update('dashboard_user');
	//    }

	function update_password($pass, $passwd) {
		$db2 = $this->load->database('iremote', TRUE);
		$id = $this->session->userdata("user_id");
		$query = "SELECT password FROM `dashboard_user` WHERE `user_id` = '$id'";
		$password = mysql_query($query);
		$ps = mysql_fetch_assoc($password);
		if ($ps['password'] == $pass) {
			$this->db->set("password", $passwd);
			$this->db->where("user_id", $id);
			$result = $this->db->update('dashboard_user');

			echo "success";
		} else {
			echo "mismatch";
		}
	}

	function do_upload($image_name) {
		$db2 = $this->load->database('iremote', TRUE);
		$path = "/var/www/html/backend/assets/images/dashboard_user/";
		$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
		if (isset($_FILES)) {
			$name = $_FILES['imageFile']['name'];
			$size = $_FILES['imageFile']['size'];
			if (strlen($name)) {
				list($txt, $ext) = explode(".", $name);
				if (in_array($ext, $valid_formats)) {
					if ($size < (1024 * 1024)) { // Image size max 1 MB
						$actual_image_name = time() . "." . $ext;
						$tmp = $_FILES['imageFile']['tmp_name'];
						if (move_uploaded_file($tmp, $path . $actual_image_name)) {
							$this->db->set("thumb_url", $actual_image_name);
							$id = $this->session->userdata("user_id");
							$this->db->where("user_id", $id);
							$result = $this->db->update('dashboard_user');
						}
						else
						echo "failed";
					}
					else
					echo "Image file size max 1 MB";
				}
				else
				echo "Invalid file format..";
			}
			else
			echo "Please select image..!";
		}
	}

	function getUserInfo() {
		$db2 = $this->load->database('iremote', TRUE);
		$id = $this->session->userdata("user_id");
		$query = "SELECT u.user_id,u.user_display_name,u.thumb_url,p.profile_display_name FROM `dashboard_user` as u JOIN dashboard_profile as p ON u.profile_id=p.profile_id WHERE u.user_id = $id";
		$query = mysql_query($query);
		$query = mysql_fetch_assoc($query);
		return $query;
	}

	function user_profile() {
		$db2 = $this->load->database('iremote', TRUE);
		$profile_id = $this->session->userdata("profile_id");
		$query = "SELECT dp.profile_name,dm.module_id,dm.module_name,dm.module_url,dmc.categories_id,dmc.categories_name FROM dashboard_profile as dp JOIN dashboard_profile_module AS dpm ON dp.profile_id=dpm.profile_id JOIN  dashboard_module AS dm ON dm.module_id=dpm.module_id JOIN dashboard_module_categories as dmc ON dmc.categories_id=dm.categories_id WHERE dpm.profile_id=$profile_id ORDER BY dm.module_name";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	function getPageTitle() {
		$db2 = $this->load->database('iremote', TRUE);
		$user_page = $this->uri->segment(2);
		if ($user_page == "front_office") {
			$module_id = 1;
		}
		if ($user_page == "ats") {
			$module_id = 2;
		}
		if ($user_page == "inbox") {
			$module_id = 3;
		}
		$user_p = $this->uri->segment(1);
		if ($user_p == "favourite") {
			$module_id = 4;
		}
		if ($user_p == "service_dir") {
			$module_id = 11;
		}
		if ($user_p == "welcomletter") {
			$module_id = 10;
		}
		if ($user_p == "menuitem") {
			$module_id = 5;
		}
		if ($user_page == "engg") {
			$module_id = 7;
		}
		if ($user_page == "audit_log") {
			$module_id = 8;
		}
		if ($user_page == "message_log") {
			$module_id = 9;
		}
		if ($user_page == "food_addons") {
			$module_id = 10;
		}
		if ($user_page == "room_registration") {
			$module_id = 14;
		}
		if ($user_page == "home") {
			$module_id = 16;
		}
		if ($user_page == "ajax_load_content") {
			$module_id = 16;
		}
		$query = "SELECT module_name FROM dashboard_module WHERE module_id=$module_id";
		$query = mysql_query($query);
		$res = mysql_fetch_assoc($query);
		return $res;
	}

	public function getUserFilter($data) {
		$db2 = $this->load->database('iremote', TRUE);
		$room_no = $data['room_no'];
		$ip = $data['ip'];
		$user_name = $data['user_name'];
		$startdate = $data['startdate'];
		$enddate = $data['enddate'];
		$start = date("Y/m/d", strtotime($startdate));
		$end = date("Y/m/d", strtotime($enddate));
		$query .= "SELECT a.user,u.user_name, a.ip, a.ts, a.event, a.room FROM auditlog as a LEFT JOIN dashboard_user as u on a.user=u.user_id WHERE ";
		$sep = "";
		if (!$room_no == "") {
			$query .=$sep . "room=$room_no";
			$sep = " AND ";
		}
		if (!$ip == "") {
			$query .=$sep . " ip='$ip'";
			$sep = " AND ";
		}
		if (!$user_name == "") {
			//                        $query.="SELECT groupconcat(user_id) FROM dashboard_user WHERE user_name like '%$user_name%' ";
			//                        $res=mysql_query($query);
			//                        $result=mysql_fetch_assoc($res);
			$query .=$sep . " user_name like '%" . $user_name . "%'";
			$sep = " AND ";
		}
		if ((isset($startdate) && ($startdate != "")) && (isset($enddate) && ($startdate != ""))) {
			//$query .=$sep." ts between '$start' AND '$end'";
			if ($sep == "") {
				//$query.=  $sep."a.ts >='$start' OR a.ts <='$end'";
				$query = "SELECT * FROM (" . $query . " 1=1) as a where a.ts BETWEEN '$start 00:00:00' AND '$end 23:59:59' ORDER BY a.ts ASC";
			} else {
				$query = "SELECT * FROM (" . $query . ") as a where a.ts BETWEEN '$start 00:00:00' AND '$end 23:59:59' ORDER BY a.ts ASC";
			}
		}

		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	function getUserUrl() {
		$db2 = $this->load->database('iremote', TRUE);
		$id = $this->session->userdata("profile_id");
		$query = "SELECT dm.module_url FROM dashboard_module AS dm JOIN  dashboard_profile_module AS dpm ON  dpm.module_id=dm.module_id WHERE dpm.profile_id= $id";
		$query = mysql_query($query);
		$query = mysql_fetch_assoc($query);
		return $query;
	}

	function getUserModules($val) {
		$db2 = $this->load->database('iremote', TRUE);
		$profile_id = $this->session->userdata("profile_id");
		$query = "SELECT * FROM `dashboard_profile_module` WHERE `profile_id`=$profile_id AND module_id=$val";
		$this->db->query($query);
		return $affected_rows = $this->db->affected_rows();
	}

	public function getAllRooms() {
		$db2 = $this->load->database('iremote', TRUE);
		$query = "SELECT room_name FROM rooms ORDER BY ABS(room_name) ASC";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	public function getAllUser() {
		$db2 = $this->load->database('iremote', TRUE);
		$query = "SELECT user_id,user_name FROM dashboard_user";
		$query = $this->db->query($query);
		return $result = $query->result_array();
	}

	public function getMessageFilter($data) {
		$db2 = $this->load->database('iremote', TRUE);
		$user_name = $data['user_name'];
		$startdate = $data['startdate'];

		$enddate = $data['enddate'];
		$start = date("Y/m/d", strtotime($startdate));
		$end = date("Y/m/d", strtotime($enddate));
		$enddate = $data['enddate'];
		$query .= "SELECT mm . *,um.`user_name` FROM `messaging_message` AS mm LEFT JOIN messaging_user_message AS um ON mm.`message_id` = um.`message_id` WHERE ";
		$sep = "";
		if (!$user_name == "") {
			$query .=$sep . "mm.`author_id`=$user_name";
			$sep = " AND ";
		}

		if ((isset($startdate) && ($startdate != "")) && (isset($enddate) && ($startdate != ""))) {
			//$query .=$sep." ts between '$start' AND '$end'";
			$query = "SELECT * FROM (" . $query . ") as a where a.sent BETWEEN '$start 00:00:00' AND '$end 23:59:59'";
		}

		$query = $this->db->query($query);
		return $result = $query->result_array();
	}
	public function check_token($token) {
		$db2 = $this->load->database('iremote', TRUE);
		$query = "SELECT room_no,pc_ip,MAC,token FROM digivalet_status WHERE token='$token'";
		$query = mysql_query($query);
		return $result = mysql_fetch_assoc($query);
	}

}
