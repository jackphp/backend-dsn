<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Favourite_model extends CI_Model{
	function Favourite_model()
	{
		parent::Model();
	}

	function add_category($data)
	{
		$this->db->set("category",trim($data["category"]));
		$this->db->set("position",trim($data["position"]));

		$res = $this->db->insert('fav_cat');

		$this->session->set_flashdata("msg","Favourite category added successfully!");
		$this->session->set_flashdata("css_class","success");
	}

	function get_data($rowid)
	{
		$this->db->select('rowid,category,position');
		$query = $this->db->get_where('fav_cat', array('rowid' => $rowid));
		return $query;
	}

	function edit_category($data)
	{
		$this->db->set("category",trim($data["category"]));
		$this->db->set("position",trim($data["position"]));

		$this->db->where("rowid",$data["rowid"]);
		$res = $this->db->update('fav_cat');

		$this->session->set_flashdata("msg","Favourite category successfully!");
		$this->session->set_flashdata("css_class","success");
	}
	function get_fav_cat()
	{
		$this->db->select('category');

		$query = $this->db->get('fav_cat');
		return $query->result();
	}
}
?>
