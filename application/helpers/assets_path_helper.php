<?php
function get_assets_path($asset) {
	switch($asset) {
		case "image":
			return ASSETS_PATH.'images/';
		case "css":
			return ASSETS_PATH.'css/';
		case "js":
			return ASSETS_PATH.'js/';
		case "chart":
			return ASSETS_PATH.'FusionCharts/';
		case "third_party":
			return BASE_URL.'third_party/';
		case "dsn_user_image":
			return ASSETS_PATH.'dsn_user_image/';
		case "flash":
			return ASSETS_PATH.'flash/';

	}
}
?>
