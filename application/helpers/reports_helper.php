<?php
function ac_temperature_report($ac_temp, $room_temp) {
	//echo "<pre>"; print_r($ac_temp); die();
	$category = '<categories>';

	$ac_str = "<dataset seriesName='Set Temperature' color='1D8BD1' anchorBorderColor='000000' anchorBgColor='1D8BD1' anchorSides='8' anchorRadius='5' showToolTipShadow='1' toolTipBorderColor='D9E5F1' toolTipBgColor='D9E5F1'>";
	//print_r($cat); die();
	foreach($ac_temp as $arr) {
		$category .= "<category name='".$arr->event_start_time."' />";
		$ac_str .= "<set value='".$arr->value."' tooltext='".$arr->value.", Room No.: ".$arr->room_name.",{br}Date: ".format_date(convert_date_to_ts($arr->event_start_time), 'custom', 'D, d/m/Y')."' link='javaScript:show_value(".$arr->value.")'/>";
	}
	$category .= "</categories>";
	$ac_str .= "</dataset>";

	$room_str = "<dataset seriesName='Actual Temperature' color='2AD62A' anchorBorderColor='000000' anchorBgColor='2AD62A' anchorSides='8' anchorRadius='5' showToolTipShadow='1' toolTipBorderColor='D9E5F1' toolTipBgColor='D9E5F1'>";
	foreach($room_temp as $arr) {
		$room_str .= "<set value='".$arr->value."' tooltext='".$arr->value.", Room No.: ".$arr->room_name.",{br}Date: ".format_date(convert_date_to_ts($arr->event_start_time), 'custom', 'D, d/m/Y')."' link='javaScript:show_value(".$arr->value.")' />";
	}
	$room_str .= "</dataset>";

	$graph_str = "<chart  hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='0' showvalues='0' numdivlines='10' numVdivlines='0' yaxisminvalue='10' yaxismaxvalue='30'  rotateNames='1' xAxisName='Date' yAxisName='AC-Temperature' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";
	$graph_str .= $category;
	$graph_str .= $ac_str;
	$graph_str .= $room_str;
	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= '</chart>';

	return $graph_str;
}

function light_pie_chart($data, $param = array()) {
	if(isset($param['date'])) {
		$graph_str = "<chart showPercentageInLabel='0' subcaption='".format_date(convert_date_to_ts($param['date'], 'custom', 'D, d/m/Y'), 'custom', 'D, d/m/Y')."' showValues='1' showLabels='1' showLegend='0'>";
	}
	else {
		$graph_str = "<chart showPercentageInLabel='0' showValues='1' showLabels='1' showLegend='0'>";
	}
	$ts = 0;
	$room_id = 1;
	$tool_text = "";
	//echo "<pre>"; print_r($param); die();
	foreach($data as $name=>$row) {
		if(isset($param['date'])) {
			$ts = convert_date_to_ts($param['date']);
		}

		if(isset($param['room_id'])) {
			$room_id = $param['room_id'];
		}

		$graph_str .= "<set value='".$row['total_on_hrs']."' label='".$name."' link='j-create_light_detail_chart-".$ts.",".$name.",".$room_id."' />";
	}
	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";
	return $graph_str;
}

function light_onoff_report($data, $param = array()) {
	//echo "<pre>"; print_r($param); die();
	$graph_str = "";

	$graph_str = "<chart xAxisName='Days' yAxisMinValue='0' yAxisName='Hours' decimalPrecision='1' formatNumberScale='0' showNames='1' showValues='1' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='1D9BFB' divLineAlpha='20' alternateHGridAlpha='5' lineColor='1D9BFB' anchorRadius='5' showToolTipShadow='1' toolTipBorderColor='D9E5F1' toolTipBgColor='D9E5F1'>";

	foreach ($data as $date=>$row) {
		if(strlen($param['room_id'])==1) {
			$room_ids = '0'.','.$param['room_id'];
		}
		else {
			$room_ids = $param['room_id'];
		}
		$ts = convert_date_to_ts($date).', rooms=new Array('.$room_ids.')';
		$graph_str .= "<set label='".format_date(convert_date_to_ts($date), 'custom', 'D, d/m/Y')."' value='".$row['total_on_hrs']."' link='javaScript:create_pie_chart(".$ts.")'/>";
	}

	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";

	return $graph_str;
}


function light_details($data, $param=array()) {
	$values = '';

	foreach($data as $key=>$val) {
		$formatted_val = '';
		$formatted_val = str_replace("-", "", $val->diff);
		$arr = explode(":", $formatted_val);
		$formatted_val = (int)$arr[0].".".(int)$arr[1];

		$from_date = parce_date_time($val->evt_start_time);
		$to_date = parce_date_time($val->evt_end_time);

		$values .= "<set label='".$val->room_name."' value='".$formatted_val."' tooltext='".$val->value.", Date: ".format_date(convert_date_to_ts($from_date[0]), 'custom', 'D, d/m/Y')." {br} ON: ".$from_date[1]." OFF: ".$to_date[1]."' />";
		$light_name = $val->value;
	}

	$graph_str = "<chart palette='1' subcaption='".$light_name."' yAxisName='Hours' xAxisName='Rooms' showValues='1' decimals='1' formatNumberScale='0'>";
	$graph_str .= $values;
	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";

	return $graph_str;
}


function food_report($food_data) {
	 
	$graph_str = "<graph caption='Food order' xAxisName='Items' yAxisName='Quantity' decimalPrecision='0' formatNumberScale='0'>";

	foreach($food_data as $food) {
		$graph_str .= "<set name='".ucfirst($food['name'])."' value='".$food['count']."' color='588526'/>";
	}

	$graph_str .= "</graph>";

	return $graph_str;
}


function rented_reports($data) {
	//echo "<pre>"; print_r($data); die();
	 
	//$grapth_str = "<graph caption='Daily Visits' subcaption='(from 8/6/2006 to 8/12/2006)' hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='0' showvalues='0' numdivlines='3' numVdivlines='0' yaxisminvalue='1000' yaxismaxvalue='1800'  rotateNames='1'>";
	//    $graph_str = "<graph showNames='1' decimalPrecision='0'>";
	//    $graph_str .= "<set name='Unrented' value='".$data['unrented']."'/>";
	//    $graph_str .= "<set name='Rented' value='".$data['rented']."'/>";
	//    $graph_str .= "<set name='Unoccupied' value='".$data['keytag']."'/>";
	//    $graph_str .= "</graph>";
	//echo $graph_str; die();

	$graph_str = "<graph showNames='1' decimalPrecision='0'>";
	$graph_str .= "<set name='Rented Occupied' value='136' color='#FF00FF' />";
	$graph_str .= "<set name='Rented Unoccupied' value='37' color='#800000' />";
	$graph_str .= "<set name='Unrented Occupied' value='25' color='#0000FF' />";
	$graph_str .= "<set name='Unrented Unoccupied' value='352' color='#FFA500' />";
	$graph_str .= "</graph>";

	return $graph_str;
}


function digivalet_reports($data) {
	//echo "<pre>"; print_r($data); die();

	//$grapth_str = "<graph caption='Daily Visits' subcaption='(from 8/6/2006 to 8/12/2006)' hovercapbg='FFECAA' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='0' showvalues='0' numdivlines='3' numVdivlines='0' yaxisminvalue='1000' yaxismaxvalue='1800'  rotateNames='1'>";
	$graph_str = "<graph showNames='1' decimalPrecision='0'>";
	$graph_str .= "<set name='Battry level less ".$data['battryLevel']."%' value='".$data['battry']."' color='F48D4B'/>";
	$graph_str .= "<set name='Controller disconnected' value='".$data['controller_status']."' color='FD0B0B'/>";
	$graph_str .= "<set name='iPod unavailable' value='".$data['ipod_status']."' color='600914'/>";
	$graph_str .= "</graph>";
	//echo $graph_str; die();
	return $graph_str;
}


/*################################ TV Graph starts ####################################### */
function tv_onoff_graph($data, $param = array()) {
	//echo "<pre>"; print_r($param); die();
	$graph_str = "<chart xAxisName='Rooms' yAxisMinValue='0' yAxisName='Hours' decimalPrecision='1' formatNumberScale='0' showNames='1' showValues='1' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='1D9BFB' divLineAlpha='20' alternateHGridAlpha='5' lineColor='1D9BFB' anchorRadius='5' showToolTipShadow='1' toolTipBorderColor='D9E5F1' toolTipBgColor='D9E5F1'>";
	$category = "<categories>";
	foreach($data as $row) {
		$category .= "<category label='".$row['room_no']."' />";
	}
	$category .= "</categories>";

	$graph_str .= $category;
	$graph_str .= "<dataset>";
	foreach ($data as $row) {
		//print_r($row); die();
		if(strlen($row['room_no'])==1) {
			$room_ids = '0'.','.$row['room_no'];
		}
		else {
			$room_ids = $row['room_no'];
		}

		if($row['to_date'] != "") {
			$tool_text = "Room No.: ".$row['room_no'].', TV Usage: '.$row['total_on_hrs'].'{br} From: '.$row['from_date'].', To: '.$row['to_date'];
		}
		else {
			$tool_text = "Room No.: ".$row['room_no'].', TV Usage: '.$row['total_on_hrs'].'{br} Date: '.$row['from_date'];
		}

		$ts = convert_date_to_ts($row['date']).', rooms=new Array('.$room_ids.')';
		$graph_str .= "<set label='".$row['room_no']."' value='".$row['total_on_hrs']."' tooltext='".$tool_text."' />";
	}



	$graph_str .= "</dataset><styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";
	return $graph_str;
}


function tv_channel_graph($data, $param = array()) {
	$graph_str = "<chart palette='2' xAxisName='Channels' yAxisName='Hours' showValues='1' decimals='1' formatNumberScale='0'>";
	$param['rooms'] ? $room_ids = $param['rooms'] : $room_ids = 0;

	foreach($data as $row) {
		//category
		//$graph_str .= "<set label='".$row['channel_name']."' value='".$row['total_on_hrs']."' link='javaScript:channel_details(".$room_ids.")'/>";
		//$graph_str .= "<set label='".$row['channel_name']."' value='".$row['total_on_hrs']."' link='j-channel_details-".$row['channel_name'].",".$room_ids."'/>";
		$graph_str .= "<set label='".$row['channel_name']."' value='".$row['total_on_hrs']."'/>";
	}
	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";

	return $graph_str;
}


function tv_channel_view_hour_wise_graph($param = array()) {
	$tool_text = "";
	$date_text = "";
	$channel_name = $param['channel_name'];

	if($param['to_date'] != "") {
		$date_text = 'From: '.$param['from_date'].'{br}To: '.$param['to_date'];
	}
	else {
		$date_text = 'Date: '.$param['from_date'];
	}

	$channel_name .= ", ".$date_text;
	$graph_str = "<chart xAxisName='Time Line' yAxisMinValue='0' subcaption='".$channel_name."' yAxisName='Users' decimalPrecision='1' formatNumberScale='0' showNames='1' showValues='0' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='1D9BFB' divLineAlpha='20' alternateHGridAlpha='5' lineColor='1D9BFB' anchorRadius='1' showToolTipShadow='1' toolTipBorderColor='D9E5F1' toolTipBgColor='D9E5F1'>";

	$category = "<categories>";
	for($i=0; $i<24; $i++) {

		if($i<10) {
			$label = "0".$i.":00";
		}
		else {
			$label = $i.":00";
		}

		$category .= "<category label='".$label."' />";
	}
	$category .= "</categories>";
	$graph_str .= $category;
	$graph_str .= "<dataset>";

	//echo "<pre>"; print_r($param['data']); die();
	foreach($param['data'] as $row) {
		$tool_text = 'Hours: '.$row['total_hrs'].'{br}'.$date_text;
		$graph_str .= "<set value='".$row['total_hrs']."' tooltext='".$tool_text."' />";
	}

	$graph_str .= "</dataset>";
	$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";

	return $graph_str;
}



function top_tvchannel_comperasion($param = array()) {
	$tool_text = "";
	$date_text = "";
	//$channel_name = $param['channel_name'];

	//    if($param['to_date'] != "") {
	//        //$date_text = 'From: '.$param['from_date'].'{br}To: '.$param['to_date'];
	//    }
	//    else {
	//        //$date_text = 'Date: '.$param['from_date'];
	//    }

	//$channel_name .= ", ".$date_text;
	$graph_str = "<chart xAxisName='Time Line'  yAxisMinValue='0'  yAxisName='Users' showNames='1' showValues='0' showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='1D9BFB' divLineAlpha='20' alternateHGridAlpha='5' showToolTipShadow='1'>";

	$category = "<categories>";
	for($i=0; $i<24; $i++) {

		if($i<10) {
			$label = "0".$i.":00";
		}
		else {
			$label = $i.":00";
		}

		$category .= "<category label='".$label."' />";
	}

	$category .= "</categories>";
	$graph_str .= $category;

	foreach($param['data']  as $channel_data) {
		$first = $channel_data[0];
		$graph_str .= "<dataset seriesName='".$first->channel_name."'>";
		foreach($channel_data as $row) {
			$hrsArr = explode(":", $row->total_hrs);
			$total_time = $hrsArr[0].'.'.$hrsArr[1];
			$tool_text = 'Hours: '.$total_time.'{br}'.$date_text;
			$graph_str .= "<set value='".$total_time."' tooltext='".$tool_text."' />";
		}
		$graph_str .= "</dataset>";
	}


	//$graph_str .= "<styles><definition><style name='myToolTipFont' type='font' fontWeight='bold' font='Verdana' size='12' color='000000' borderColor='3C3B41' bgColor='F4F4F4' borderThickness='2'/></definition><application><apply toObject='ToolTip' styles='myToolTipFont' /></application></styles>";
	$graph_str .= "</chart>";

	return $graph_str;
}


/*############################### TV Graph End ###########################################*/

function quick_room_status($q, $battry_level=25){
	$str = '<table width="100%" cellpadding="2" cellspacing="2"><tr>';
	$i=0;
	foreach($q as $row) {
		$status = "";
		if($i%2==0) {
			$str .= "</tr></td><tr><td>";
		}
		else {
			$str .= "</td><td>";
		}

		if($row->controller_status == 0) {
			$status = 'Controller Disconnected';
		}

		if($row->ipod_status == 0) {
			$status = 'iRmote Unavailable';
		}

		if($row->battry <= $battry_level) {
			$status = 'iRemote battry level '.$row->battry.'%';
		}

		$str .= anchor('ats/floor/'.$row->fid, $row->room_name." ".$status);
		$i++;
	}

	$str .= '</table>';

	return $str;
}


function user_activity_graph($data) {
	$graph_str = "<graph showNames='1' decimalPrecision='0'>";
	foreach($data as $d) {
		$graph_str .= "<set name='".$d['module']."' value='".$d['count']."'/>";
	}
	$graph_str .= "</graph>";
	//echo $graph_str; die();
	return $graph_str;
}
?>