<?php
/***
 * object_to_array()
 * @argument: array(role_id, role_name)
 * @return: array(role_id=>role_name);
 **/

function object_to_array($object) {
	$vals = array();
	if(count($object) > 0) {
		foreach($object as $array) {
			if(isset($array['rid']) && $array['name']) {
				$vals[$array['rid']] = $array['name'];
			}
		}
	}

	return $vals;
}

function send_request($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$op = curl_exec ($curl);
	curl_close($curl);
}


/**
 * isUserLoggedIn()
 * if user is logged in then it will
 * return true else it false
 **/
function isUserLoggedIn() {
	$ob = &get_instance();
	$ob->load->library("user");
	$uid = $ob->user->get_user_info('uid');
	if($uid > 0) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

function get_time_difference( $start, $end ) {

	$uts['start']      =    $start;
	$uts['end']        =    $end;
	if( $uts['start']!==-1 && $uts['end']!==-1 )
	{
		if( $uts['end'] >= $uts['start'] )
		{
			$diff    =    $uts['end'] - $uts['start'];
			if( $days=intval((floor($diff/86400))) )
			$diff = $diff % 86400;
			if( $hours=intval((floor($diff/3600))) )
			$diff = $diff % 3600;
			if( $minutes=intval((floor($diff/60))) )
			$diff = $diff % 60;
			$diff    =    intval( $diff );
			return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
		}
		else
		{
			trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
		}
	}
	else
	{
		trigger_error( "Invalid date/time data detected", E_USER_WARNING );
	}
	return( false );
}

/**
 * return all files array in directory.
 ***/
function directoryToArray($directory, $recursive) {
	$array_items = array();
	if ($handle = opendir($directory)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				if (is_dir($directory. "/" . $file)) {
					if($recursive) {
						$array_items = array_merge($array_items, directoryToArray($directory. "" . $file.'/', $recursive));
					}
					$file = $directory . "/" . $file;
					$array_items[] = preg_replace("/\/\//si", "/", $file);
				} else {
					$file = $directory . "/" . $file;
					$array_items[] = preg_replace("/\/\//si", "/", $file);
				}
			}
		}
		closedir($handle);
	}
	return $array_items;
}

/***
 * result_to_array()
 * @arguments: $result (it should be a query result obj), $key (define new obj key), $val (what will be the value of result object)
 */
function result_to_array($result, $key, $val) {
	$array = array(''=>'--Select--');
	foreach($result as $r) {
		if(isset($r->$key)) {
			$array[$r->$key] = $r->$val;
		}
	}

	return $array;
}

/***
 * permissions_to_array()
 * @argument: array(role_id, 'perm1, perm2, perm3')
 * @return: array(role_id=>perm1, role_id=>perm2)
 ***/
function permissions_to_array($object) {
	//print_r($object); die();
	$vals = array();
	if(count($object) > 0)  {

		foreach($object as $array) {
			$permissions = $array['perm'];
			if(strchr($permissions, ',')) {
				foreach(explode(",", $permissions) as $perm) {
					$vals[] = $perm;
				}
			}
			else {
				$vals[] = $permissions;
			}
		}
	}

	return $vals;
}



/***
 * format_date()
 * @param $timestamp
 *   The exact date to format, as a UNIX timestamp.
 * @param $type
 *   The format to use. Can be "small", "medium" or "large" for the preconfigured
 *   date formats. If "custom" is specified, then $format is required as well.
 * @param $format
 *   A PHP date format string as required by date(). A backslash should be used
 *   before a character to avoid interpreting the character as part of a date
 *   format.
 * @param $timezone
 *   Time zone offset in seconds; if omitted, the user's time zone is used.
 * @param $langcode
 *   Optional language code to translate to a language other than what is used
 *   to display the page.
 * @return
 *   A translated date string in the requested format.
 **/
function format_date($timestamp, $type = 'medium', $format = '', $timezone = NULL, $langcode = NULL) {

	if($timezone == NULL) {
		date_default_timezone_set('Asia/Calcutta');
		$timezone = date_default_timezone_get();
	}

	$timestamp += $timezone;

	switch ($type) {
		case 'small':
			$format = 'm/d/Y - H:i';
			break;
		case 'large':
			$format = 'l, F j, Y - H:i';
			break;
		case 'custom':
			// No change to format.
			break;
		case 'medium':
		default:
			$format = 'D, m/d/Y - H:i';
	}

	$date = date($format, $timestamp);

	return $date;
}

/***
 * Converting date to time stamp
 * format
 */
function convert_date_to_ts($date="") {
	if($date != "") {
		if(strchr($date, " ")) {
			$date_time_arr = explode(" ", $date);
			$date_arr = explode('-', $date_time_arr[0]);
			$time_arr = explode(':', $date_time_arr[1]);
			return mktime($time_arr[0], $time_arr[1], $time_arr[2], $date_arr[1], $date_arr[2], $date_arr[0]);
		}
		else {
			$arr = explode("-", $date);
			return mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]);
		}
	}
}

/***
 * Converting time stamp to date
 * format
 */
function convert_ts_to_date($ts="") {
	if($ts != "") {
		return date('Y-m-d', $ts);
	}
}

/***
 * Difference betwwen two dats
 */
function date_time_diff($from_date, $to_date) {
	$difference = array();

	$dt = date_parse($from_date);
	$now = date_parse($to_date);

	$start = new DateTime($from_date);
	$end = new DateTime($to_date);

	$diff = $end->diff($start);

	return $diff;
}

/****
 * Adding two times
 */
function add_time($start_time, $end_time) {
	$sec = 0;
	$min = 0;
	$hr = 0;
	//echo "<pre>"; print_r($end_time); die();
	$sec = $start_time->s+$end_time->s;
	$min = $start_time->i+$end_time->i;
	$hr = $start_time->h+$end_time->h;


	if($sec >= 60) {
		$min += 1;
		$sec = $sec%60;
	}

	if($min >= 60) {
		$hr += 1;
		$min = $min%60;
	}


	return (object)array('h'=>$hr, 'i'=>$min, 's'=>$sec);
}


/***
 * Add Diffrence time
 */

function add_diff_time($time1, $time2) {
	$sec = 0;
	$min = 0;
	$hr = 0;
	//echo "Time 1=> ".$time1." Time2=> ".$time2; die();
	$time1 = str_replace('-', "", $time1);
	$time2 = str_replace('-', "", $time2);
	$start_time = explode(":", $time1);
	$end_time = explode(":", $time2);

	//echo "<pre>"; print_r($end_time); die();
	$sec = ((int)$start_time[2])+((int)$end_time[2]);
	$min = ((int)$start_time[1])+((int)$end_time[1]);
	$hr = ((int)$start_time[0])+((int)$end_time[0]);


	if($sec >= 60) {
		$min += 1;
		$sec = $sec%60;
	}

	if($min >= 60) {
		$hr += 1;
		$min = $min%60;
	}

	return $hr.":".$min.":".$sec;
	//return (object)array('h'=>$hr, 'i'=>$min, 's'=>$sec);
}


function parce_date_time($date_time = "") {
	$date = array();
	if($date_time != "") {
		$date = explode(" ", $date_time);
	}

	return $date;
}


################### Users Function  #########################

function user_status_text($status_id) {
	if($status_id == 1) {
		return  "Active";
	}
	else {
		return "Deactive";
	}
}

############### Load Variables ############
function load_sys_var($name = "") {
	$obj =& get_instance();
	$obj->load->model('config_model');
	$q = $obj->config_model->get_vars($name);
	$vars = array();
	foreach($q->result() as $row) {
		$vars[] = $row;
	}

	return $vars;
}

############# Returning Variables  ##########
function get_var($name) {
	if($name == "") {
		return;
	}

	return load_sys_var($name);
}



/****
 * Sorting an multiple array
 */

function array_sort($array, $on, $order=SORT_ASC)
{
	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			}
			else  if (is_object($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			}
			else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

/******
 * Converting StdObject into array
 */
function objectsIntoArray($arrObjData, $arrSkipIndices = array()) {
	$arrData = array();

	// if input is object, convert into array
	if (is_object($arrObjData)) {
		$arrObjData = get_object_vars($arrObjData);
	}

	if (is_array($arrObjData)) {
		foreach ($arrObjData as $index => $value) {
			if (is_object($value) || is_array($value)) {
				$value = objectsIntoArray($value, $arrSkipIndices); // recursive call
			}
			if (in_array($index, $arrSkipIndices)) {
				continue;
			}
			$arrData[$index] = $value;
		}
	}
	return $arrData;
}


function sanitize($string, $force_lowercase = true, $anal = false) {
	$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
	$clean = trim(str_replace($strip, "", strip_tags($string)));
	//$clean = preg_replace('/\s+/', "-", $clean);
	$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	return ($force_lowercase) ?
	(function_exists('mb_strtolower')) ?
	mb_strtolower($clean, 'UTF-8') :
	strtolower($clean) :
	$clean;
}

function read_dvd_rip_fifo() {
	//Encoding: task 1 of 1, 92.38 % (100.06 fps, avg 82.69 fps, ETA 00h02m25s)
	$output = get_dvd_rip_last_line();
	//echo $output;
	$trem = "";
	$percentage = "";
	if($output != "") {
		if($exarr = explode(" ", $output)){
			//echo "<pre>";  print_r($exarr[5]); die();
			if(count($exarr) > 13) {
				if(isset($exarr[5])) {
					$percentage = (int)$exarr[5];
				}

				if(isset($exarr[13])) {
					$trem = sanitize($exarr[13]);
				}
			}
			else if(count($exarr) == 5 && isset($exarr[5])) {
				$percentage = $exarr[5];
			}


			return array('trem'=>$trem, 'percentage'=>$percentage);
		}

		else if($output[0]=="Completed.") {
			return array('dvd_rip'=>'done');
		}
	}

	return array();

}

function get_dvd_rip_last_line() {
	if(file_exists(DVD_RIP_FIFO)) {
		$fh = fopen(DVD_RIP_FIFO, 'r');
		$data = fread($fh, filesize(DVD_RIP_FIFO));
		$filedata = explode("\r", $data);
		$line = $filedata[count($filedata)-1];
		return $line;
	}
}

function just_clean($string) {
	// Replace other special chars
	$specialCharacters = array(
        '#' => '',
        '$' => '',
        '%' => '',
        '&' => 'and',
        '@' => '',
	//        '.' => '',
        '�' => '',
        '+' => '',
        '=' => '',
        '�' => '',
        '/' => '',
        '\'' => '',
        '(' => '',
        ')' => '',
        '?' => '',
        '"' => '',
        '!' => '',
        ':' => '',
        ';' => '',
        '~' => '',
        '+' => '',
        '=' => '',
        '>' => '',
        '<' => '',
        '^' => '',
        "'" => "",
	);

	while (list($character, $replacement) = each($specialCharacters)) {
		$string = str_replace($character, $replacement, $string);
	}

	//        $string = strtr($string,
	//        "������? ����������������������������������������������",
	//        "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn"
	//        );
	//
	//        // Remove all remaining other unknown characters
	//        $string = preg_replace('/[^a-zA-Z0-9-]/', ' ', $string);
	//        $string = preg_replace('/^[-]+/', '', $string);
	//        $string = preg_replace('/[-]+$/', '', $string);
	//        $string = preg_replace('/[-]{2,}/', ' ', $string);

	return $string;
}

function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) { return('n/a'); } else {
		return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
}


/**
 * Media Info
 * it will return all
 * info of any media file.
 * **/
function media_info($file="") {
	exec('mediainfo "'.$file.'"', $info);
	return filter_media_info($info);
}

/**
 * Getting movie data in filterd
 * data.
 */
function filter_media_info($data = array()) {
	$param = array();
	foreach($data as $d) {
		if($el = explode(":", $d)) {
			if(isset($el[0]) && isset($el[1])) {
				$param[trim($el[0])] = trim($el[1]);
			}
		}
	}
	return $param;
}

/**
 * Getting Disk free space
 * in Percentage
 * **/
function getDriveSpace($drive) {
	$percent = 0;

	if (is_dir($drive))
	{
		$dt = disk_total_space($drive);
		$df = disk_free_space($drive);

		$percent = round((($dt - $df)/$dt) * 100);
	}
	return $percent;
}


// Convert free space to GB or MB depending on size
function formatFreeSpace($freeSpace) {
	$rtnValue = "";
	$freeSpace = $freeSpace/(1024*1024);

	if ($freeSpace > 1024)
	{
		$rtnValue = number_format($freeSpace/1024, 2)." GB";
	}
	else
	{
		$rtnValue = number_format($freeSpace, 2)." MB";
	}

	return $rtnValue;
}


function get_label_text($text="", $size=50) {
	if(strlen($text) <= $size) {
		return $text;
	}
	else {
		return  substr($text, 0, $size).'...';
	}
}

function qindicator() {
	$obj = &get_instance();
	$obj->load->model('movie_wizard_model');
	$obj->load->library('user');
	$uid = $obj->user->get_user_info('uid');
	if($uid > 0) {
		$q = $obj->movie_wizard_model->get_movie_process_que();
		if($q->num_rows() > 0) {
			$row = $obj->movie_wizard_model->get_current_ripping_movie();
			$current_running = "";
			if(isset($row[0])) {
				$progress = 'Percentage: '.$row[0]->process_status.'%'.', Trem: '.$row[0]->trem;
				if($row[0]->movie_title != "")
				$current_running .= $progress." <hr style='line-height:0.1' /> file: ".$row[0]->file." <br/> Title: ".$row[0]->movie_title;
				else
				$current_running .= $progress."<hr style='line-height:0.1' /> file: ".$row[0]->file;
			}

			$html = "";
			$psq = array('total'=>$q->num_rows(), 'file'=>$current_running);

			if(is_array($psq) && $psq != "" && count($psq) > 0 )  {
				$script = '<script language="javascript" type="text/javascript">$(".tooltip").hover(function(){var tipVal = $(this).attr(\'tipVal\');$(this).siblings("div").html(tipVal);$(this).siblings("div").show();}, function(){ $(this).siblings("div").hide();});</script>';
				$html = $script.'<center><div><div class="tooltip" tipVal="'.$psq['file'].'" style="background:url('.get_assets_path('image').'th_progressbar.gif); width="114px; height="18px;">&nbsp;</div></div><div>'.anchor('movie_wizard/processq', '('.$psq['total'].') Files in process', 'class="tooltip" tipVal="'.$psq['file'].'"')."<div class=\"tip\" style=\"display: none;\">".$psq['file']."</div></div></center>";
			}

			return $html;
		}
	}
}

function get_controller_status() {
	$obj = &get_instance();
	$obj->load->model('movie_wizard_model');
	$obj->load->library('user');
	$uid = $obj->user->get_user_info('uid');
	if($uid > 0) {
		return $obj->movie_wizard_model->get_rip_controller_info();
	}
}

function get_count($table="") {
	$obj = &get_instance();
	$obj->load->model('movies_model');
	return $obj->movies_model->get_count($table);
}


function update_song_log($total=0, $completed=0, $process=0) {
	$fp = fopen(SONGS_PROCESS_FILE, 'w');
	$data = 'total='.$total.',completed='.$completed.',process='.$process;
	fwrite($fp, $data);
	fclose($fp);
}

function open_process($command="", $mode="r") {
	$output = array();
	if(! $handle = popen($command, 'r')) {
		$output[] = "Command is not running..!";
	}
	else{
		$output[] = $handle;
		if($handle) {
			while($tmp = fgets($handle)) {
				$output[] .= $tmp;
			}
			$output[] = "\n\nResult = " . pclose($handle);

		}
	}
	return $output;
}

function get_song_log() {
	$logdata = array();
	if(file_exists(SONGS_PROCESS_FILE)) {
		$fp = fopen(SONGS_PROCESS_FILE, 'r');
		$data = fread($fp, 1024);
		fclose($fp);
		$tmpdata = explode(',', $data);
		foreach($tmpdata as $val) {
			if($arr = explode('=', $val)) {
				$key = $arr[0];
				$val = $arr[1];
				$logdata[$key] = $val;
			}
		}
	}
	//print_r($data); die();
	return $logdata;
}

// Display rip progress bar
function rip_progress($percentage=0) {

	$str = "";
	$str .= '<table cellpadding="2" cellspacing="2" width="90%" align="center">';
	$str .= '<tr><td align="center">';
	$str .= '<div style="border:#000 1px solid; height:10px;background-color:#000; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;width:100%;"><div class="rip_progress" style="width:'.$percentage.'%; float:left; background-color:#7BB425;">&nbsp;</div></div>';
	$str .= '</td><td align="center" width="5px;">'.$percentage.'%</td></tr>';
	$str .= '</table>';
	return $str;
}

// Display the Drive Space Graphical Bar
function displayDriveSpaceBar()
{
	if(is_dir(STORAGE_DISK)) {
		$drivespace = getDriveSpace(STORAGE_DISK);
		$free_space = disk_free_space(STORAGE_DISK);

		$freeSpace = "";

		if ($drivespace > 20)
		{
			$freeSpace = " (".formatFreeSpace($free_space)." Free)";
		}
		?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="2%" valign="top"><b>Storage:</b></td>
		<td width="80%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="20px;" bgcolor="#007800" class="progress_bar"
					width="<?php echo $drivespace; ?>%">&nbsp;</td>
				<td height="20px;" bgcolor="#000"
					background="<?php //echo get_assets_path('image'); ?>noglass.gif"
					width="<?php echo (100 - $drivespace) ?>%"><img
					src="images/blank.gif" width="1" height="3" border="0"></td>
			</tr>
			<tr>
				<td colspan="2">
				<div class="tinypercent" align="center"><?php echo $drivespace."%".$freeSpace ?></div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
		<?php
	}
}
?>
