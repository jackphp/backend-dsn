<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = '172.16.0.11';
$db['default']['username'] = 'root';
$db['default']['password'] = 'ps7778';
$db['default']['database'] = 'backend';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['iremote']['hostname'] = '172.16.0.11';
$db['iremote']['username'] = 'root';
$db['iremote']['password'] = 'ps7778';
$db['iremote']['database'] = 'iremote';
$db['iremote']['dbdriver'] = 'mysql';
$db['iremote']['dbprefix'] = '';
$db['iremote']['pconnect'] = TRUE;
$db['iremote']['db_debug'] = TRUE;
$db['iremote']['cache_on'] = FALSE;
$db['iremote']['cachedir'] = '';
$db['iremote']['char_set'] = 'utf8';
$db['iremote']['dbcollat'] = 'utf8_general_ci';
$db['iremote']['swap_pre'] = '';
$db['iremote']['autoinit'] = TRUE;
$db['iremote']['stricton'] = FALSE;

$db['token']['hostname'] = '172.16.0.11';
$db['token']['username'] = 'root';
$db['token']['password'] = 'ps7778';
$db['token']['database'] = 'iremote';
$db['token']['dbdriver'] = 'mysql';
$db['token']['dbprefix'] = '';
$db['token']['pconnect'] = TRUE;
$db['token']['db_debug'] = TRUE;
$db['token']['cache_on'] = FALSE;
$db['token']['cachedir'] = '';
$db['token']['char_set'] = 'utf8';
$db['token']['dbcollat'] = 'utf8_general_ci';
$db['token']['swap_pre'] = '';
$db['token']['autoinit'] = TRUE;
$db['token']['stricton'] = FALSE;


$db['reports']['hostname'] = '172.16.0.11';
$db['reports']['username'] = 'root';
$db['reports']['password'] = 'ps7778';
$db['reports']['database'] = 'logging';
$db['reports']['dbdriver'] = 'mysql';
$db['reports']['dbprefix'] = '';
$db['reports']['pconnect'] = FALSE;
$db['reports']['db_debug'] = TRUE;
$db['reports']['cache_on'] = FALSE;
$db['reports']['cachedir'] = '';
$db['reports']['char_set'] = 'utf8';
$db['reports']['dbcollat'] = 'utf8_general_ci';
$db['reports']['swap_pre'] = '';
$db['reports']['autoinit'] = TRUE;
$db['reports']['stricton'] = FALSE;


$db['pmsi']['hostname'] = '172.16.0.11';
$db['pmsi']['username'] = 'root';
$db['pmsi']['password'] = 'ps7778';
$db['pmsi']['database'] = 'pmsi';
$db['pmsi']['dbdriver'] = 'mysql';
$db['pmsi']['dbprefix'] = '';
$db['pmsi']['pconnect'] = FALSE;
$db['pmsi']['db_debug'] = TRUE;
$db['pmsi']['cache_on'] = FALSE;
$db['pmsi']['cachedir'] = '';
$db['pmsi']['char_set'] = 'utf8';
$db['pmsi']['dbcollat'] = 'utf8_general_ci';
$db['pmsi']['swap_pre'] = '';
$db['pmsi']['autoinit'] = TRUE;
$db['pmsi']['stricton'] = FALSE;

$db['local']['hostname'] = 'localhost';
$db['local']['username'] = 'root';
$db['local']['password'] = 'jack';
$db['local']['database'] = 'dsn';
$db['local']['dbdriver'] = 'mysql';
$db['local']['dbprefix'] = '';
$db['local']['pconnect'] = FALSE;
$db['local']['db_debug'] = TRUE;
$db['local']['cache_on'] = FALSE;
$db['local']['cachedir'] = '';
$db['local']['char_set'] = 'utf8';
$db['local']['dbcollat'] = 'utf8_general_ci';
$db['local']['swap_pre'] = '';
$db['local']['autoinit'] = TRUE;
$db['local']['stricton'] = FALSE;

//$db['digi_settings']['hostname'] = "";
//$db['digi_settings']['username'] = "";
//$db['digi_settings']['password'] = "";
//$db['digi_settings']['database'] = '"'.get_assets_path('digivale_settings').'"';
//$db['digi_settings']['dbdriver'] = "sqlite";
//$db['digi_settings']['dbprefix'] = "";
//$db['digi_settings']['pconnect'] = TRUE;
//$db['digi_settings']['db_debug'] = TRUE;
//$db['digi_settings']['cache_on'] = FALSE;
//$db['digi_settings']['cachedir'] = "";
//$db['digi_settings']['char_set'] = "utf8";
//$db['digi_settings']['dbcollat'] = "utf8_general_ci";

/* End of file database.php */
/* Location: ./application/config/database.php */
