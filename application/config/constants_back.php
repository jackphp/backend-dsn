<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------
 | File and Directory Modes
 |--------------------------------------------------------------------------
 |
 | These prefs are used when checking and setting modes when working
 | with the file system.  The defaults are fine on servers with proper
 | security, but you may wish (or even need) to change the values in
 | certain environments (Apache running a separate process for each
 | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
 | always be used to set the mode correctly.
 |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
 |--------------------------------------------------------------------------
 | File Stream Modes
 |--------------------------------------------------------------------------
 |
 | These modes are used when working with fopen()/popen()
 |
 */
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('MOVIE_UPLOAD_SIZE', '3000');
define('STORAGE_DISK', '/media');
define('ASSETS_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/mymovieadmin/assets/');
//define('DIGIVALET_SETTINGS_PATH', '/var/www/digivalet_settings/db/settings.sqlite');
define('BASE_PATH', $_SERVER['DOCUMENT_ROOT'].'/mymovieadmin');
define('IMAGE_MANAGER', 'http://'.$_SERVER['HTTP_HOST'].'/mymovieadmin/third_party/imagemanager/');
define("THIRD_PARTY", BASE_PATH.'/third_party/');
define('MOVIE_IMAGES', 'http://'.$_SERVER['HTTP_HOST'].'/mymovieadmin/images/newimovie/');
define('MOVIE_POSTER_IMG', BASE_PATH.'/images/newimovie/');
define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/mymovieadmin/');
define('MOVIE_UPLOAD_PATH', BASE_PATH.'/data/movies/');
define('CATEGORY_IMAGE_PATH', MOVIE_IMAGES.'category/');

define('SONG_COPY_PATH', $_SERVER['DOCUMENT_ROOT'].'/Audiosongs/');

define('MOVIE_FILE_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/movies/');
define('TVSHOW_FILE_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/isongs/');
define('SONG_FILE_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/Audiosongs/');
//define('FOOD_VIDEOS', '/files/food/videos/');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
