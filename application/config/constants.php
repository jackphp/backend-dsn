<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 |--------------------------------------------------------------------------
 | File and Directory Modes
 |--------------------------------------------------------------------------
 |
 | These prefs are used when checking and setting modes when working
 | with the file system.  The defaults are fine on servers with proper
 | security, but you may wish (or even need) to change the values in
 | certain environments (Apache running a separate process for each
 | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
 | always be used to set the mode correctly.
 |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
 |--------------------------------------------------------------------------
 | File Stream Modes
 |--------------------------------------------------------------------------
 |
 | These modes are used when working with fopen()/popen()
 |
 */

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('MOVIE_UPLOAD_SIZE', '3000');
define('STORAGE_DISK', '/media');
define('BASE_DIR', 'backend-dsn');
define('ASSETS_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/assets/');
define('JSON_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/index.php/digivalet_dashboard/');
define('IMAGE_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/assets/img/');
define('JS_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/assets/js/dashboard/');
define('CSS_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/assets/css/');
define('MESSAGE_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/'.BASE_DIR.'/api_dashboard/');

define('BASE_PATH', $_SERVER['DOCUMENT_ROOT'].'/'.BASE_DIR);

define("THIRD_PARTY", BASE_PATH.'/third_party/');
define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/backend-dsn/');

define('FOOD_IMAGES', '/files/food/images/');
define('FOOD_VIDEOS', '/files/food/videos/');
define('FAV_IMAGE', BASE_PATH.'/images/ipad_image/');

define('FOOD_DB_NAME', 'fooddb.sqlite');
define('FOOD_DB_PATH', BASE_PATH.'/'.FOOD_DB_NAME);
define('FAV_DB_NAME', 'Tvchannel.sqlite');
define('FAV_DB_PATH', BASE_PATH.'/'.FAV_DB_NAME);



define('HOTEL_NAME', 'The Address, Dubai Mall');
define('SECURITY_TOKEN', 'paragon5678');


define('TV_CHANNEL_PUSH_UPDATE', 'http://172.16.0.2/iremote/index.php?option=com_pushupdate&type=tvchannels');
define('FOOD_PUSH_UPDATE', 'http://172.16.0.2/iremote/index.php?option=com_pushupdate&type=food');
define('SHOPPING_PUSH_UPDATE', 'http://172.16.0.2/iremote/index.php?option=com_pushupdate&type=pdfupdate');
define('PDF_UPLOAD','/var/www/html/pdf');
define('PROMO_IMAGE_UPLOAD','/var/www/html/backend/images/promo_images');
$browser = getBrowser();
if($browser == 'Internet Explorer') {
	define('MAIN_PAGE_DIR', 'main_pages/ie/');
}
else {
	define('MAIN_PAGE_DIR', 'main_pages/others/');
}


/* End of file constants.php */
/* Location: ./application/config/constants.php */


function getBrowser() {
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	$ub = '';
	if(preg_match('/MSIE/i',$u_agent))
	{
		$ub = "Internet Explorer";
	}
	elseif(preg_match('/Firefox/i',$u_agent))
	{
		$ub = "Mozilla Firefox";
	}
	elseif(preg_match('/Safari/i',$u_agent))
	{
		$ub = "Apple Safari";
	}
	elseif(preg_match('/Chrome/i',$u_agent))
	{
		$ub = "Google Chrome";
	}
	elseif(preg_match('/Flock/i',$u_agent))
	{
		$ub = "Flock";
	}
	elseif(preg_match('/Opera/i',$u_agent))
	{
		$ub = "Opera";
	}
	elseif(preg_match('/Netscape/i',$u_agent))
	{
		$ub = "Netscape";
	}
	return $ub;
}

