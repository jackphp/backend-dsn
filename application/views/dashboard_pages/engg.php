<title><?php print_r($page['module_name']);?></title>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>


<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<!-- Title bar -->
<title><?php print_r($page['module_name']);?></title>

<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>



<!-- Main content -->
<section role="main" id="main">
<div
	class="with-padding">

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title" class="thin">
<h1>Engineering Dashboard</h1>

</hgroup>

<div class="with-padding">
<ul class="files-list white-gradient" style="margin-bottom: 4px">
	<li style="padding: 8px; min-height: 22px; padding-top: 10px"><!--				    <div class="float-right" style="padding-left:10px;border-left: 1px solid grey">
						<label>Show</label> 
						<span class="button-group compact">
							<label	for="filterSelectRadioAll" id="filterSelectRadioAllLabel" class="button grey-active" style='cursor:default'>
								<input type="radio" name="filterSelectRadio" id="filterSelectRadioAll">
								All
							</label>
							<label	for="filterSelectRadioPref" id="filterSelectRadioPrefLabel" class="disabled button grey-active">
								<input type="radio" name="filterSelectRadio" id="filterSelectRadioPref" >
								Preferred View
							</label>
						</span>
					</div>

					 <div style="float:right;margin-left:10px;padding-right:10px">
					    View 
						    <span class="button-group compact">
							    <a href="javascript:void(0)" style='cursor:default' class="button icon-list gray-active active">List</a>
							    <a href="javascript:void(0)" class="button icon-thumbs gray-active">Icon</a>
						    </span>
					        <input id="viewSelect" type="checkbox" style="display:none"/> 
				     </div>--></li>
	<li style="padding: 8px; min-height: 22px;">
	<div class="btnWrap" style="padding-left: 1px;"><!--openUserPreferredView()
						<span class="disabled button icon-download white-gradient compact"  id="savePreferredViewButton" onClick="savePreferredView();" style="cursor:pointer">Save As Preferred View</span>-->
	</div>

	<div class="btnWrap"><!--openComplexModal()--> <span
		class="button icon-search white-gradient compact"
		onClick="filterChanged(true);" style="cursor: pointer"
		id="filterButton">Filter</span></div>

	<div id="legendWrapper"
		style="float: right; margin-right: 5px; margin-left: 10px; border-left: 1px solid grey;">

	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="rentedCheck" id="rentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="rented" class="label">Rented</label></div>
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="unrentedCheck" id="unrentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="unrented" class="label">Unrented</label>
	</div>
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="dndCheck" id="dndCheck" value="1"
		class="checkbox mid-margin-left"> <label for="dnd" class="label">DND</label></div>
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="maintenanceCheck" id="maintenanceCheck"
		value="1" class="checkbox mid-margin-left"> <label for="maintenance"
		class="label">Maintenance</label></div>
	<div
		style="display: inline; background: #9C3; text-shadow: 1px 1px 2px black"><input
		type="checkbox" name="ok" id="okCheck" value="1"
		class="checkbox mid-margin-left"> <label for="ok" class="label">OK</label></div>
	<div
		style="display: none; background: orange; text-shadow: 1px 1px 2px black"><input
		type="checkbox" name="warn" id="warnCheck" value="1"
		class="checkbox mid-margin-left"> <label for="warn" class="label">Warn</label></div>
	<div
		style="display: inline; background: #FF7676; text-shadow: 1px 1px 2px black"><input
		type="checkbox" name="alert1" id="alert1Check" value="1"
		class="checkbox mid-margin-left"> <label for="alert1" class="label">Alert</label></div>
	</div>

	<div style="float: right" id="floorSelect" class="compact">Floor <select
		id='floorinput'
		class='select multiple-as-single easy-multiple-selection check-list allow-empty'
		multiple style="width: 100px"></select></div>
	</li>

</ul>

<!--<div style="clear:both"></div>-->

<div class="viewWrapper"><!--================================icon view====================================-->
<div id="iconView" style="width: 100%">
<div id="dashboard" style="width: 100%; background: white; margin: 1px">


</div>
</div>
<!--================================end icon view====================================-->
<!--================================grid view====================================-->
<div id="gridView" style="width: 100%; display: none">
<table border="1" class="table" id="gridViewTable" style="width: 100%">
	<thead>
		<tr>
			<th style="width: 5%">Room Number</th>
			<th style="width: 10%">DigiValet Status</th>
			<th style="width: 15%">iPad Battery</th>
			<th style="width: 15%">iPad Last Seen</th>
			<th style="width: 10%">Version</th>
			<th style="width: 11%">Rented</th>
			<th style="width: 13%">Occupied</th>
			<th style="width: 11%">DND</th>
			<th style="width: 10%">Alerts</th>
			<th style="width: 10%">Maintenance</th>
		</tr>

	</thead>

	<tbody id="gridViewTableBody">


	</tbody>

	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>
</div>
<!--================================end grid view====================================-->


</div>
</div>



</section> <!-- End main content --> <!-- Side tabs shortcuts -->
<ul id="shortcuts" role="complementary"
	class="children-tooltip tooltip-right">
	<li class="current"><a href="./ats.html" class="shortcut-dashboard"
		title="Dashboard">Dashboard</a></li>
	<li><a href="inbox.html" class="shortcut-messages" title="Messages">Messages</a></li>
	<!--<li><a href="agenda.html" class="shortcut-agenda" title="Agenda">Agenda</a></li>
		<li><a href="tables.html" class="shortcut-contacts" title="Contacts">Contacts</a></li>
		<li><a href="explorer.html" class="shortcut-medias" title="Medias">Medias</a></li>
		<li><a href="sliders.html" class="shortcut-stats" title="Stats">Stats</a></li>-->
	<li><a href="./logs.html" class="shortcut-settings" title="Logs">Logs</a></li>
	<li><span class="shortcut-notes" title="Notes">Notes</span></li>
</ul>

<!-- Sidebar/drop-down menu --> <!-- End sidebar/drop-down menu --> <!-- JavaScript at the bottom for fast page loading -->

<!-- Scripts --> <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>

<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
<!-- Sidebar/drop-down menu --> <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>
<script src="<?php echo get_assets_path('js');?>dashboard/scripts.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/engg.render.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/engg.useraction.js"></script>

</html>
</body>