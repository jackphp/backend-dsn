<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<title><?php print_r($page['module_name']);?></title>
<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_page.php'; ?>
<script
	src="<?php echo get_assets_path('js'); ?>dashboard/libs/jquery-1.7.2.min.js"></script>
<!--        <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>datatable/css/data_table_jui.css">
<link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>datatable/css/demo_page.css">
<link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>datatable/css/jquery.custom.css">-->
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css'); ?>dashboard/styles/table.css?v=1">
<script
	src="<?php echo ASSETS_PATH; ?>datatable/js/media.js"></script>
<script
	src="<?php echo ASSETS_PATH; ?>datatable/js/dataTable.js"></script>
<script
	src="<?php echo ASSETS_PATH; ?>datatable/js/colRecoder.js"></script>
<script
	src="<?php echo ASSETS_PATH; ?>datatable/js/colRecoder.js"></script>
<link
	href="<?php echo ASSETS_PATH; ?>js/tv_channel/jquery.alerts-1.1/jquery.alerts.css"
	rel="stylesheet" type="text/css" />
<script
	type="text/javascript"
	src="<?php echo ASSETS_PATH; ?>js/tv_channel/jquery.alerts-1.1/jquery.alerts.js"></script>
<style>
.sorting_asc {
	cursor: pointer
}
</style>

<script type="text/javascript" charset="utf-8">
    function DateCheck()
    {
        var room_no= document.getElementById('room_no').value;
                        
        var ip= document.getElementById('ip').value;
        var user_name= document.getElementById('user_name').value;
        var StartDate= document.getElementById('startdate').value;
        var EndDate= document.getElementById('enddate').value;
        
        var endDate=EndDate.split("-");
        var endD=endDate[1]+"/"+endDate[0]+"/"+endDate[2];
        var eDate=new Date(endD).getTime();
       
        myDate=StartDate.split("-");
        var startDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
        sDate=new Date(startDate).getTime();
      
      
        if(ip=="" && room_no=="" && user_name=="" && StartDate=="" && EndDate=="" ){
            //alert("Please Fill Atleast One field");
            jAlert('<label class="error">Please fill atleast one field.</label>', 'Error');
            return false;
        }
        if(StartDate!="" && EndDate==""){
            //alert("Please Fill End Date");
             jAlert('<label class="error">Please fill end date.</label>', 'Error');
            return false;
        }
        if(StartDate=="" && EndDate!=""){
            //alert("Please Fill Start Date");
            jAlert('<label class="error">Please fill start date.</label>', 'Error');
            return false;
        }
        if(sDate > eDate)
        {
           // alert("Please ensure that the End Date is greater than or equal to the Start Date.");
            jAlert('<label class="error">Please ensure that the end date is greater than or equal to the start date.</label>', 'Error');
            return false;
        }
    }
</script>

<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME; ?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
<section role="main" id="main">
<!-- Main content -->
<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title"
	class="thin">
<h1>Audit Log</h1>
<br>

</hgroup>
<div class="with-padding">

<div class="content-panel margin-bottom enabled-panels"
	style="padding-left: 0px;">
<div class="panel-content linen">
<div class="panel-control align-right">
<form method="post"
	action="<?php echo site_url(); ?>digivalet_dashboard/audit_log"
	name="user_module_form" onsubmit="return DateCheck()">
<div style="display: inline; padding-right: 15px; padding-bottom: 5px">
<label for="room_no">Room No:</label> <select name="room_no"
	id="room_no" style="font-size: 12px; padding: 3px">
	<option value="">Select Room</option>
	<?php foreach ($allroom as $room) { ?>
	<option value="<?php echo $room['room_name']; ?>"
	<?php if ($room['room_name'] == $this->input->post('room_no')) {
		echo "selected='selected'";
	} ?>><?php echo $room['room_name']; ?></option>
	<?php } ?>
</select></div>

<div style="display: inline; padding-right: 15px"><label for="ip">IP:</label>
<input type="text" class="input tiny" id="ip" name="ip"
	placeholder="IP Address"
	value="<?php if (isset($this->form_validation->error_string)) {
    echo $this->input->post('ip');
} ?>"
	style="width: 115px; font-size: 12px; height: 25px;"></div>
<div style="display: inline; padding-right: 15px"><label for="user_name">User:</label>
<input type="text" class="input " id="user_name" name="user_name"
	placeholder="User Name"
	value="<?php if (isset($this->form_validation->error_string)) {
    echo $this->input->post('user_name');
} ?>"
	style="width: 150px; font-size: 12px; height: 25px;"></div>
<div style="display: inline; padding-right: 15px"><label for="startdate">Start
Date: </label> <input type="text" id="startdate" class="input"
	name="startdate"
	value="<?php if (isset($this->form_validation->error_string)) {
    echo $this->input->post('startdate');
} ?>"
	style="width: 90px; font-size: 12px; height: 25px;" /></div>
<div style="display: inline; padding-right: 15px"><label for="enddate">End
Date: </label> <input type="text" id="enddate" class="input"
	name="enddate"
	value="<?php if (isset($this->form_validation->error_string)) {
    echo $this->input->post('enddate');
} ?>"
	style="width: 90px; font-size: 12px; height: 25px;" /></div>
<button type="submit" class="button icon-search margin-left">Filter</button>
<input type="hidden" name="page" value="filter"></form>
</div>
<div class="back"><span class="back-arrow"></span>Back</div>
<div class="panel-load-target with-padding "
	style="min-height: 500px; position: relative;"><?php if ($aud == "audit") {
		if ($detail) {
			?>

<table class="table" id="audit_table" style="clear: both;">
	<thead>
		<tr>
			<th>S.No.</th>
			<th style="width: 84px;">User Name</th>
			<th>System IP</th>
			<th style="width: 208px;">Date & Time</th>
			<th>Room</th>
			<th>Reason</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i = "1";
	foreach ($detail as $row) {
		?>

		<tr class="even_gradeA">
			<td style="width: 5px; text-align: center;"><?php echo $i; ?></td>
			<td style="text-align: center;"><?php echo $row['user_name']; ?></td>
			<td style="width: 8px; text-align: center;"><?php echo $row['ip']; ?></td>
			<td style="width: 18px; text-align: center;"><?php echo date("j M, Y, g:i a", strtotime($row['ts'])); ?></td>
			<td style="width: 5px; text-align: center;"><?php echo $row['room']; ?></td>
			<td style="text-align: left;"><?php echo $row['event']; ?></td>
		</tr>
		<?php
		$i++;
	}
	?>
	</tbody>

</table>
<br>
<script type="text/javascript" charset="utf-8">
                                $('#audit_table').dataTable({
                                    "aLengthMenu": [[ 25, 50,100, -1], [25, 50, 100,"All"]],
                                    "iDisplayLength": 50,
                                    "sPaginationType": "full_numbers"
                                });
                            </script> <?php } else {
                            	echo "<center><h4>No result found</h4></center>";
                            }
	} ?></div>
</div>
</div>

</div>
</section>

	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_left_page.php'; ?>
	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_middle_rightbar.php'; ?>
	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_footer.php'; ?>
<link
	rel="stylesheet"
	href="<?php echo ASSETS_PATH; ?>js/datepicker/jquery-ui-1.9.2.custom.css" />
<script
	src="<?php echo ASSETS_PATH; ?>js/datepicker/jquery-1.8.2.js"></script>
<script
	src="<?php echo ASSETS_PATH; ?>js/datepicker/jquery-ui.js"></script>


<script type="text/javascript">
    jQuery.noConflict();
    jQuery("#startdate" ).datepicker({ dateFormat: "dd-mm-yy" });
    jQuery("#enddate" ).datepicker({ dateFormat: "dd-mm-yy" });
    
</script>
<style>
#current_table_length {
	font-size: 20px;
	font-weight: bold;
	color: 666666
}

#user_table_length {
	font-size: 20px;
	font-weight: bold;
	color: 666666
}

.dataTables_wrapper,.dataTables_header,.dataTables_footer {
	background: #e6e7ec !important;
	color: black
}

.dataTables_wrapper {
	border-radius: 15px;
	color: #666666;
	box-shadow: 1px 0 0 1px #ececf1 inset, 0 2px 0 rgba(255, 255, 255, 0.35)
		inset !important
}

.paginate_disabled_previous,a.paginate_active,a.paginate_active.first,.paginate_enabled_previous,.paginate_disabled_next,paginate_active,.paging_full_numbers a.paginate_active,.paging_full_numbers a.paginate_active.first,.paging_full_numbers a.paginate_active.last,.paginate_enabled_next,.paging_full_numbers a
	{
	/*        background-image: -webkit-linear-gradient(top, #666666, #666666); */
	background-image: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#666666),
		to(#666666) );
	background-image: -webkit-linear-gradient(top, #666666, #666666);
	background-image: -moz-linear-gradient(center top, #666666, #666666 50%, #666666 50%,
		#666666) !important;
	/*         background-image: -webkit-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666);*/
	border: 1px solid #cfcfcf !important;
}

#user_table_wrapper {
	border: 2px solid #CFCFCF !important
}

#current_table_wrapper {
	border: 2px solid #CFCFCF !important
}
</style>
</body>
</html>
