
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<title><?php print_r($page['module_name']);?></title>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>
<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>



<!-- Main content -->
<section role="main" id="main">
<div class="with-padding">

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title" class="thin">
<h1><?php print_r($page['module_name']);?></h1>

</hgroup>
<ul class="files-list white-gradient" style="margin-bottom: 0px">
	<li style="padding: 8px; min-height: 22px; padding-top: 10px">
	<div
		style="float: right; margin-left: 10px; padding-right: 10px; padding-left: 10px">
	<span class="button-group compact" style='cursor: pointer'> <span
		onclick="printDashboard()" class="button icon-printer">Print</span> <!--<a href="javascript:void(0)" class="button icon-thumbs gray-active">Icon</a>-->
	</span></div>

	<!--				    <div class="float-right" style="padding-left:10px;border-left: 1px solid grey">
						<label>Show</label> 
						<span class="button-group compact">
							<label	for="filterSelectRadioAll" id="filterSelectRadioAllLabel" class="button grey-active" style="cursor:default">
								<input type="radio" name="filterSelectRadio" id="filterSelectRadioAll">
								All
							</label>
							<label	for="filterSelectRadioPref" id="filterSelectRadioPrefLabel" class="disabled button grey-active">
								<input type="radio" name="filterSelectRadio" id="filterSelectRadioPref" >
								Preferred View
							</label>
						</span>
					</div>

					 <div style="float:right;margin-left:10px;padding-right:10px">
					    View 
						    <span class="button-group compact" style="cursor:default">
							    <a href="javascript:void(0)" class="button icon-list gray-active active">List</a>
							    <a href="javascript:void(0)" class="button icon-thumbs gray-active">Icon</a>
						    </span>
					        <input id="viewSelect" type="checkbox" style="display:none"/> 
				     </div>--></li>
	<li style="padding: 8px; min-height: 22px;"><span
		class="button-group float-right compact"
		style="padding-bottom: 3px; margin-top: -2px"> <!--<label for="filterButton" class="button grey-active">
                            <input type="radio" name="filterRadio" id="filterButton" value="1" > Filter 
                        </label>
                        <label for="preferredButton" class="button green-active"> 
                            <input type="radio" name="filterRadio" id="preferredButton" value="2" > Prefered View
                        </label> 
                        <label for="allButton"  class="button grey-active"> 
                            <input type="radio" name="filterRadio" id="allButton" value="3" checked> All 
                        </label> --> </span>


	<div class="btnWrap" style="padding-left: 1px;"><!--openUserPreferredView()
						<span class="disabled button icon-download white-gradient compact"  id="savePreferredViewButton" onClick="savePreferredView();" style="cursor:pointer">Save As Preferred View</span>-->
	</div>

	<div class="btnWrap"><!--openComplexModal()--> <span
		class="button icon-search white-gradient compact"
		onClick="filterChanged(true);" style="cursor: pointer"
		id="filterButton">Filter</span></div>


	<div id="legendWrapper"
		style="float: right; margin-right: 5px; margin-left: 10px;">
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="rentedCheck" id="rentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="rented" class="label">Rented</label></div>
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="unrentedCheck" id="unrentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="unrented" class="label">Unrented</label>
	</div>
	<!--					    <div style="display:inline;color:#4B4B4B;text-shadow:1px 1px 2px white"><input type="checkbox" name="occupied" id="occupiedCheck" value="1" class="checkbox mid-margin-left"> <label for="occupied" class="label">Occupied</label></div>
					    <div style="display:inline;color:#4B4B4B;text-shadow:1px 1px 2px white"><input type="checkbox" name="unoccupied" id="unoccupiedCheck" value="1" class="checkbox mid-margin-left"> <label for="unoccupied" class="label">Unoccupied</label> </div>-->
	<!--					    <div style="display:inline;color:#4B4B4B;text-shadow:1px 1px 2px white"><input type="checkbox" name="dndCheck" id="dndCheck" value="1" class="checkbox mid-margin-left"> <label for="dnd" class="label">DND</label></div>-->
	<div
		style="display: inline; color: #4B4B4B; text-shadow: 1px 1px 2px white"><input
		type="checkbox" name="maintenanceCheck" id="maintenanceCheck"
		value="1" class="checkbox mid-margin-left"> <label for="maintenance"
		class="label">Maintenance</label></div>
	<div
		style="background-color: #cc3333; display: inline; color: white; text-shadow: 1px 1px 2px black"><input
		type="checkbox" name="alertCheck" id="alertCheck" value="1"
		class="checkbox mid-margin-left"> <label for="alertCheck"
		class="label">Rooms with alert</label></div>
	</div>

	<div style="float: right" id="floorSelect" class="compact">Floor <select
		id='floorinput'
		class='select multiple-as-single easy-multiple-selection check-list allow-empty'
		multiple style="width: 100px"></select></div>
	</li>
</ul>

<div style="clear: both"></div>

<div class="viewWrapper"><!--================================icon view====================================-->
<div id="iconView" style="width: 100%; display: none">
<div id="dashboard" style="width: 100%; background: white; margin: 1px">


</div>
</div>
<!--================================end icon view====================================-->
<!--================================grid view====================================-->
<div style="width: 500px; margin: 0px auto" id="errorMessage"></div>
<div id="gridView" style="width: 100%; display: inline-block">
<table border="1" class="table" id="gridViewTable" style="width: 100%">
	<thead style="text-align: center;">
		<tr>
			<th style="width: 3%"></th>
			<th style="width: 12%">Room Number</th>
			<!--<th style="width:7%">Room Desc.</th>-->
			<th style="width: 12%">iPad Last Seen</th>
			<th style="width: 15%">iPad Battery</th>
			<!--                                <th style="width:13%">iPad Last Seen</th>
                                <th style="width:5%">Version</th>-->
			<th style="width: 5%">DigiValet<br>
			Version</th>
			<th style="width: 5%">Rented</th>
			<th style="display: none">DND</th>
			<!--<th style="width:10%">Alerts</th>-->
			<th style="width: 7%">Maintenance</th>
			<th style="width: 13%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
				class='forPrint'>Remark</span></th>
		</tr>

	</thead>

	<tbody id="gridViewTableBody" style="text-align: center">


	</tbody>

	<tfoot>

	</tfoot>
</table>
</div>
<div id='gridPrintContainer'></div>
<!--================================end grid view====================================-->

</div>
</div>
</section>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>

<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
<!-- Sidebar/drop-down menu -->


<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>
<script
	src="<?php echo get_assets_path('js');?>dashboard/scripts.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/ats.render.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/ats.useraction.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/table.css?v=1">
<audio>
<source
	src="<?php echo get_assets_path('css');?>siren.wav">
</audio>
