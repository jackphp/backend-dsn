<title><?php print_r($page['module_name']); ?></title>
<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_page.php'; ?>
<style>
.modal-content {
	overflow: visible !important;
}

.ui-autocomplete {
	max-height: 250px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}

/* IE 6 doesn't suclass="current"pport max-height
    * we use height instead, but this forces the menu to always be this tall
    */
* html .ui-autocomplete {
	height: 250px;
}

.navigable li.current {
	border-left: 5px solid green;
}

.modal-content .custom-vscrollbar {
	display: none !important;
}

#modals .modal {
	overflow: visible !important;
}

/*#composeFormTemplate #ui-id-1{
            z-index: 90; display: block; width: 348px; top: 62px; left: 231px;
    }*/
.active {
	background: #FDF13E;
	color: #C24C47;
}
</style>


<script
	src="<?php echo get_assets_path('js'); ?>dashboard/libs/jquery-ui-1.9.0.custom.min.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css'); ?>dashboard/jquery-ui-1.9.0.custom.min.css">
<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME; ?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>

<!-- Main content -->
<section role="main" id="main">

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title"
	class="thin">
<h1><?php print_r($page['module_name']); ?></h1>

</hgroup>
<div class="with-padding">

<p></p>

<div class="content-panel margin-bottom enabled-panels">

<div class="panel-navigation silver-gradient">

<div class="panel-control"><span class="button-group children-tooltip"><a
	href="#" onClick="refreshView();" class="button icon-undo"
	title="Refresh">Refresh</a></span></div>

<div class="panel-load-target scrollable" id="messageHeaderBox"
	style="height: 550%; position: relative;">

<div class="navigable">

<ul class="unstyled-list">
	<li class="hidden big-menu grey-gradient with-right-arrow"><span
		id="inboxLink">Inbox <span id="unreadCountDiv" class="list-count"></span>
	</span>
	<ul id="inboxSummary" class="message-menu scrollable">

	</ul>
	</li>

	<li class="big-menu grey-gradient with-right-arrow" id="sent_box"><span
		id="sentBoxLink">Sent <!-- <span id="sentCountDiv" class="list-count"></span>-->
	</span>
	<ul id="sentSummary" class="message-menu scrollable">

	</ul>
	</li>
	<li class="big-menu grey-gradient with-right-arrow" id="scheduled_box">
	<span id="scheduledBoxLink">Scheduled</span>
	<ul id="scheduledSummary" class="message-menu scrollable">

	</ul>
	</li>

</ul>

</div>


<!-- <div class="custom-vscrollbar" style="display: none; opacity: 0; "><div></div></div> -->

</div>

</div>

<div class="panel-content linen">

<div class="panel-control align-right">
<div id="messageDisplay" class="align-left float-left"
	style="font-size: 20px; margin-left: 300px;"></div>

<span class="button-group children-tooltip"><a id="newMessageButton"
	href="#" class="button icon-star margin-left" title="New Message">New
message</a></span></div>

<!--					<div class="back"><span class="back-arrow"></span>Back</div>-->
<div class="panel-load-target scrollable with-padding" id="inboxPanel"
	style="height: 550px;">

<h4 id="subjectDiv" class="no-margin-top"></h4>
<h4 id="fromDiv" style="display: inline-block" class="no-margin-top"></h4>

<div id="messageBox" style="display: none"
	class="large-box-shadow white-gradient with-border">

<div
	class="button-height with-mid-padding silver-gradient no-margin-top"><span
	class="button-group children-tooltip"> <a href="#" id="editButton"
	class="button icon-pencil" title="Edit">Edit</a> </span> <span
	class="button-group children-tooltip"> <a href="#" id="deleteButton"
	class="button icon-trash" title="Delete">Delete</a> </span></div>

<div id="bodyContainer" class="with-padding"></div>


</div>



</div>

</div>

</div>



</div>

</section>
<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_left_page.php'; ?>

<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_middle_rightbar.php'; ?>
<!-- Sidebar/drop-down menu -->
<script
	src="<?php echo get_assets_path('js'); ?>digital_datepicker/mobiscroll.custom-2.5.1.min.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css'); ?>digital_datepicker/mobiscroll.custom-2.5.1.min.css">


<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_footer.php'; ?>

<script
	src="<?php echo get_assets_path('js'); ?>dashboard/inbox.scripts.js"></script>

<!--<script src="<?php echo get_assets_path('js'); ?>dashboard/libs/jquery-ui-timepicker-addon.js"></script>
<script src="<?php echo get_assets_path('js'); ?>dashboard/anytime.js"></script>

<link rel="stylesheet" href="<?php echo get_assets_path('css'); ?>dashboard/anytime.css">-->

<div id="tooltips"></div>

