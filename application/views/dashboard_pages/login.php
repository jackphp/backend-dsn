<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->
<html class="no-js linen" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Dashboard Login</title>
<meta name="description" content="">
<meta name="author" content="">


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<!-- For all browsers -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/reset.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/style.armani.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/colors.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/files.css">
<link rel="stylesheet" media="print"
	href="<?php echo get_assets_path('css');?>dashboard/print.css?v=1">
<!-- For progressively larger displays -->
<link rel="stylesheet" media="only all and (min-width: 480px)"
	href="<?php echo get_assets_path('css');?>dashboard/480.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 768px)"
	href="<?php echo get_assets_path('css');?>dashboard/768.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 992px)"
	href="<?php echo get_assets_path('css');?>dashboard/992.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 1200px)"
	href="<?php echo get_assets_path('css');?>dashboard/1200.css?v=1">
<!-- For Retina displays -->
<link rel="stylesheet"
	media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)"
	href="<?php echo get_assets_path('css');?>dashboard/2x.css?v=1">

<!-- Login pages styles -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/login.css">

<!-- Webfonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300'
	rel='stylesheet' type='text/css'>



<!-- Additional styles -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/agenda.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/dashboard.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/form.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/modal.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/progress-slider.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/switches.css?v=1">

<!-- JavaScript at bottom except for Modernizr -->
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/modernizr.custom.js"></script>

<!-- For Modern Browsers -->
<link rel="shortcut icon"
	href="<?php echo get_assets_path('img');?>dashboard/favicons/favicon.png">
<!-- For everything else -->
<!--<link rel="shortcut icon" href="<?php echo get_assets_path('img');?>dashboard/favicons/favicon.ico">-->
<link rel="shortcut icon"
	href="<?php echo get_assets_path('css');?>img/favicons/favicon.ico">
<!-- For retina screens -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo get_assets_path('img');?>dashboard/favicons/apple-touch-icon-retina.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo get_assets_path('img');?>dashboard/favicons/apple-touch-icon-ipad.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed"
	href="<?php echo get_assets_path('img');?>dashboard/avicons/apple-touch-icon.png">

<!-- Jquery dataTable css -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('js');?>dashboard/libs/DataTables/jquery.dataTables.css?v=1">

<!-- iOS web-app metas -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('img');?>dashboard/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('img');?>dashboard/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('img');?>dashboard/splash/iphone.png"
	media="screen and (max-device-width: 320px)">

<!-- Microsoft clear type rendering -->
<meta http-equiv="cleartype" content="on">

<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
<meta name="application-name" content="DigiValet Admin Skin">
<meta name="msapplication-tooltip"
	content="Cross-platform admin template.">

<!-- These custom tasks are examples, you need to edit them to show actual pages -->
<script type="text/javascript">
		var jsroot = '<?php echo JS_PATH; ?>' ;	
		var apiroot ='<?php echo JSON_PATH; ?>' ;
		var imagesroot = '<?php echo IMAGE_PATH; ?>';
		var cssroot ='<?php echo CSS_PATH; ?>';	
                var messageroot ='<?php echo MESSAGE_PATH; ?>';	
	</script>
</head>

<body class="clearfix with-menu with-shortcuts">

<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<!-- Title bar -->

<?php //include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>
</head>

<body>

<div id="container"><hgroup id="login-title" class="large-margin-bottom">
<h1 class="login-title-image" style="width: 292px;">DigiValet Dashboard</h1>
<h5>&copy; DigiValet</h5>
</hgroup>

<form method="post" action="" id="form-login">
<ul class="inputs black-input large">
	<!-- The autocomplete="off" attributes is the only way to prevent webkit browsers from filling the inputs with yellow -->
	<li><span class="icon-user mid-margin-right"></span><input type="text"
		name="login" id="login" value="" class="input-unstyled"
		placeholder="Login" autocomplete="off"></li>
	<li><span class="icon-lock mid-margin-right"></span><input
		type="password" name="pass" id="pass" value="" class="input-unstyled"
		placeholder="Password" autocomplete="off"></li>
</ul>

<button type="submit" class="button glossy full-width huge">Login</button>
</form>

</div>

<!-- JavaScript at the bottom for fast page loading -->

<!-- Scripts -->
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<script src="<?php echo get_assets_path('js');?>dashboard/setup.js"></script>

<!-- Template functions -->
<script
	src="<?php echo get_assets_path('js');?>dashboard/developr.input.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/developr.message.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/developr.notify.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/developr.tooltip.js"></script>

<script>

		/*
		 * How do I hook my login script to this?
		 * --------------------------------------
		 *
		 * This script is meant to be non-obtrusive: if the user has disabled javascript or if an error occurs, the login form
		 * works fine without ajax.
		 *
		 * The only part you need to edit is the login script between the EDIT SECTION tags, which does inputs validation
		 * and send data to server. For instance, you may keep the validation and add an AJAX call to the server with the
		 * credentials, then redirect to the dashboard or display an error depending on server return.
		 *
		 * Or if you don't trust AJAX calls, just remove the event.preventDefault() part and let the form be submitted.
		 */

	    $(document).ready(function () {
	        /*
	        * JS login effect
	        * This script will enable effects for the login page
	        */
	        // Elements
                
                 document.getElementById("login").focus();
	        var doc = $('html').addClass('js-login'),
				container = $('#container'),
				formLogin = $('#form-login'),

	        // If layout is centered
				centered;

	        /******* EDIT THIS SECTION *******/

	        /*
	        * AJAX login
	        * This function will handle the login process through AJAX
	        */
	        formLogin.submit(function (event) {
	            // Values
	            var login = $.trim($('#login').val()),
					pass = $.trim($('#pass').val());

	            // Check inputs
	            if (login.length === 0) {
	                // Display message
	                displayError('Please fill in your login');
	                return false;
	            }
	            else if (pass.length === 0) {
	                // Remove empty login message if displayed
	                formLogin.clearMessages('Please fill in your login');

	                // Display message
	                displayError('Please fill in your password');
	                return false;
	            }
	            else {
                       
	                // Remove previous messages
	                formLogin.clearMessages();

	                // Show progress
	                displayLoading('Checking credentials...');
	                event.preventDefault();

	                // Stop normal behavior
	                event.preventDefault();

	                // This is where you may do your AJAX call, for instance:
	                $.ajax("check_login",
                    {
                        type: 'post',
                        dataType: 'json',
                        data: {
                            'login': login,
                            'pass': pass
                        },
                        success: function (data) {
                           
                            console.log("success called");
                            if (data.logged) {
                                document.location.href = data.newLocation;
                            }
                            else {
                                formLogin.clearMessages();
                                displayError('Invalid user/password, please try again');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                            console.log("error called st:" + textStatus + " er" + errorThrown);
                            formLogin.clearMessages();
                            displayError('Error while contacting server, please try again');
                        }
                    });




	            }
	        });

	        /******* END OF EDIT SECTION *******/

	        // Handle resizing (mostly for debugging)
	        function handleLoginResize() {
	            // Detect mode
	            centered = (container.css('position') === 'absolute');

	            // Set min-height for mobile layout
	            if (!centered) {
	                container.css('margin-top', '');
	            }
	            else {
	                if (parseInt(container.css('margin-top'), 10) === 0) {
	                    centerForm(false);
	                }
	            }
	        };

	        // Register and first call
	        $(window).bind('normalized-resize', handleLoginResize);
	        handleLoginResize();

	        /*
	        * Center function
	        * @param boolean animate whether or not to animate the position change
	        * @param string|element|array any jQuery selector, DOM element or set of DOM elements which should be ignored
	        * @return void
	        */
	        function centerForm(animate, ignore) {
	            // If layout is centered
	            if (centered) {
	                var siblings = formLogin.siblings(),
						finalSize = formLogin.outerHeight();

	                // Ignored elements
	                if (ignore) {
	                    siblings = siblings.not(ignore);
	                }

	                // Get other elements height
	                siblings.each(function (i) {
	                    finalSize += $(this).outerHeight(true);
	                });

	                // Setup
	                container[animate ? 'animate' : 'css']({ marginTop: -Math.round(finalSize / 2) + 'px' });
	            }
	        };

	        // Initial vertical adjust
	        centerForm(false);

	        /**
	        * Function to display error messages
	        * @param string message the error to display
	        */
	        function displayError(message) {
	            // Show message
	            var message = formLogin.message(message, {
	                append: false,
	                arrow: 'bottom',
	                classes: ['red-gradient'],
	                animate: false					// We'll do animation later, we need to know the message height first
	            });

	            // Vertical centering (where we need the message height)
	            centerForm(true, 'fast');

	            // Watch for closing and show with effect
	            message.bind('endfade', function (event) {
	                // This will be called once the message has faded away and is removed
	                centerForm(true, message.get(0));

	            }).hide().slideDown('fast');
	        }

	        /**
	        * Function to display loading messages
	        * @param string message the message to display
	        */
	        function displayLoading(message) {
	            // Show message
	            var message = formLogin.message('<strong>' + message + '</strong>', {
	                append: false,
	                arrow: 'bottom',
	                classes: ['blue-gradient', 'align-center'],
	                stripes: true,
	                darkStripes: false,
	                closable: false,
	                animate: false					// We'll do animation later, we need to know the message height first
	            });

	            // Vertical centering (where we need the message height)
	            centerForm(true, 'fast');

	            // Watch for closing and show with effect
	            message.bind('endfade', function (event) {
	                // This will be called once the message has faded away and is removed
	                centerForm(true, message.get(0));

	            }).hide().slideDown('fast');
	        }
	    });

//		// What about a notification?
//		notify('Alternate login', 'Want to see another login page style? Try the <a href="./login-box.html"><b>box version</b></a> or the <a href="./login-full.html"><b>full version</b></a>.', {
//			autoClose: false,
//			delay: 2500,
//			icon: './img/demo/icon.png'
//		});

	</script>


</body>

</html>
