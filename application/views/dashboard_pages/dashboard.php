<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->
<html
	class="no-js" lang="en">
<!--<![endif]-->
<title><?php print_r($page['module_name']);?></title>
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>
<header role="banner" id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header>

<!-- Button to open/hide menu -->
<?php

//echo '<a href="#" id="close-menu"><span>Menu</span></a>';
echo '<a href="#" id="open-menu"><span>Menu</span></a>';

?>
<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>

<!-- Main content -->
<section role="main" id="main">

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title" class="thin">
<h1><?php print_r($page['module_name']);?></h1>

</hgroup>

<div class="with-padding">
<ul class="files-list white-gradient" style="margin-bottom: 0px">
	<li style="padding: 8px; min-height: 22px; padding-top: 10px">
	<div class="float-right"
		style="padding-left: 10px; border-left: 1px solid grey"><!--						<label>Show</label> 
						<span class="button-group compact">
							<label	for="filterSelectRadioAll" class="button grey-active" >-->
	<input type="radio" name="filterSelectRadio" id="filterSelectRadioAll"
		checked style="cursor: default !important; display: none"> <!--								All
							</label>
							<label	for="filterSelectRadioPref" id="filterSelectRadioPrefLabel" class="disabled button grey-active">-->
	<input type="radio" name="filterSelectRadio" id="filterSelectRadioPref"
		style="display: none"> <!--								Preferred View
							</label> 
						</span>--></div>
	<div
		style="float: right; margin-left: 10px; padding-left: 10px; margin-right: 10px">
	<!--					    <label>View </label>
						    <span class="button-group compact" style="cursor:default!important">
							    <a href="javascript:void(0)" class="button icon-list gray-active">List</a>
							    <a href="javascript:void(0)" class="button icon-thumbs gray-active active">Icon</a>
						    </span>--> <input id="viewSelect" type="checkbox" checked
		style="display: none" /></div>

	</li>
	<li style="padding: 8px; min-height: 22px;" id="secondPane">

	<div class="btnWrap" style="padding-left: 1px;"><!--openUserPreferredView()
						      <span class="disabled button icon-download white-gradient compact" id="savePreferredViewButton" onClick="savePreferredView();" style="cursor:pointer">Save As Preferred View</span>-->
	</div>

	<div class="btnWrap"><!--openComplexModal()--> <span
		class="button-group children-tooltip"><span
		class="button icon-search white-gradient compact"
		onClick="filterChanged(true);" style="cursor: pointer" title="Filter"
		id="filterButton">Filter</span></span></div>

	<div id="legendWrapper"
		style="float: right; margin-left: 10px; padding-left: 5px">

	<div style="background-color: #50B8FD"><input type="checkbox"
		name="rented" id="rentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="rented" class="label">Rented</label></div>
	<div
		style="background-color: #fff; color: grey; text-shadow: 1px 1px 1px #ccc"><input
		type="checkbox" name="unrented" id="unrentedCheck" value="1"
		class="checkbox mid-margin-left"> <label for="unrented" class="label">Unrented</label>
	</div>
	<!--					    <div style="background-color:orange"><input type="checkbox" name="occupied" id="occupiedCheck" value="1" class="checkbox mid-margin-left"> <label for="occupied" class="label">Occupied</label></div>
					    <div style="background-color:#99cc33"><input type="checkbox" name="unoccupied" id="unoccupiedCheck" value="1" class="checkbox mid-margin-left"> <label for="unoccupied" class="label">Unoccupied</label> </div>-->
	<!--					    <div style="background-color:#9933cc"><input type="checkbox" name="mmr" id="mmrCheck" value="1" class="checkbox mid-margin-left"> <label for="mur" class="label">MMR</label></div>
					    <div style="background-color: #cc3333"><input type="checkbox" name="dnd" id="dndCheck" value="1" class="checkbox mid-margin-left"> <label for="dnd" class="label">DND</label></div>-->
	<div style="background-color: #666666"><input type="checkbox"
		name="maintenance" id="maintenanceCheck" value="1"
		class="checkbox mid-margin-left"> <label for="maintenance"
		class="label">Maintenance</label></div>
	</div>

	<div style="float: right" id="floorSelect" class="compact"><label>Floor</label>
	<select id='floorinput'
		class='select multiple-as-single easy-multiple-selection check-list allow-empty'
		multiple style="width: 100px"></select> <!--<select title="Basic example" multiple="multiple" name="example-basic" size="5">
							<option value="option1">Option 1</option>
							<option value="option2">Option 2</option>
							<option value="option3">Option 3</option>
							<option value="option4">Option 4</option>
							<option value="option5">Option 5</option>
							<option value="option6">Option 6</option>
							<option value="option7">Option 7</option>
							<option value="option8">Option 8</option>
							<option value="option9">Option 9</option>
							<option value="option10">Option 10</option>
							<option value="option11">Option 11</option>
							<option value="option12">Option 12</option>
						 </select>--></div>
	</li>
</ul>

<div style="clear: both"></div>

<div class="viewWrapper"><!--================================icon view====================================-->
<div id="iconView" style="width: 100%">
<div id="dashboard" style="width: 100%; background: white; margin: 1px">


</div>
</div>
<!--================================end icon view====================================-->
<!--================================grid view====================================-->
<div id="gridView" style="width: 100%; display: none">
<table border="1" class="table" id="gridViewTable" style="width: 100%">
	<thead>
		<tr>
			<th style="width: 30%">Room Number</th>
			<th style="width: 30%">Guest Name</th>
			<th style="width: 40%">Status</th>
		</tr>

	</thead>

	<tbody id="gridViewTableBody">


	</tbody>

	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>
</div>
<!--================================end grid view====================================-->
<!-- <div class="dataTables_footer">
                    <div class="dataTables_info" id="sorting-advanced_info">
                    </div>
                </div>--></div>

</div>



</section>
<!-- End main content -->

<!-- Side left pages -->
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>

<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
<!-- Sidebar/drop-down menu -->


<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>
<script src="<?php echo get_assets_path('js');?>dashboard/scripts.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/frontoffice.render.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/frontoffice.useraction.js"></script>