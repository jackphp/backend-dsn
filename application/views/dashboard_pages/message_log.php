<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<title><?php echo HOTEL_NAME ;?></title>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<!--        <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/data_table_jui.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/demo_page.css">
        <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/jquery.custom.css">-->
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/table.css?v=1">
<script
	src="<?php echo ASSETS_PATH ;?>datatable/js/media.js"></script>
<script
	src="<?php echo ASSETS_PATH ;?>datatable/js/dataTable.js"></script>
<script
	src="<?php echo ASSETS_PATH ;?>datatable/js/colRecoder.js"></script>

<script type="text/javascript" charset="utf-8">
                function DateCheck()
                        { 
                          var user_name= document.getElementById('user_name').value;
                          var StartDate= document.getElementById('startdate').value;
                          var EndDate= document.getElementById('enddate').value;
                          var eDate = new Date(EndDate);
                          var sDate = new Date(StartDate);
                          if(user_name=="" && StartDate=="" && EndDate=="" ){
                              alert("Please Fill Atleast One field");
                              return false;
                          }
                          if(user_name==""){
                              alert("Please select User Name");
                              return false;
                          }
                          if(StartDate!="" && EndDate==""){
                              alert("Please Fill End Date");
                              return false;
                          }
                          if(StartDate=="" && EndDate!=""){
                              alert("Please Fill Start Date");
                              return false;d
                          }
                          if(StartDate!= '' && StartDate!= '' && sDate> eDate)
                            {
                            alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                            return false;
                            }
                        }
          </script>

<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
<section role="main" id="main">
<!-- Main content -->
<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title"
	class="thin">
<h1>Message Log</h1>
<br>

</hgroup>
<div class="with-padding">

<div class="content-panel margin-bottom enabled-panels"
	style="padding-left: 0px;">
<div class="panel-content linen">
<div class="panel-control align-right">
<form method="post"
	action="<?php echo site_url();?>digivalet_dashboard/message_log"
	name="message_logform" onsubmit="return DateCheck()">
<div style="display: inline; padding-right: 15px; padding-bottom: 5px">
<label for="user_name">User Name:</label> <select name="user_name"
	id="user_name">
	<option value="">Select User</option>
	<?php foreach ($alluser as $user) {?>
	<option value="<?php echo $user['user_id'];?>"
	<?php if($user['user_id']==$this->input->post('user_name')){echo "selected='selected'";}?>><?php echo $user['user_name'];?></option>
	<?php } ?>
</select></div>
<div style="display: inline; padding-right: 15px"><label for="startdate">Start
Date: </label> <input type="text" id="startdate" class="input"
	name="startdate"
	value="<?php if(isset($this->form_validation->error_string)){echo $this->input->post('startdate');} ?>"
	style="width: 80px; font-size: 11px;" /></div>
<div style="display: inline; padding-right: 15px"><label for="enddate">End
Date: </label> <input type="text" id="enddate" class="input"
	name="enddate"
	value="<?php if(isset($this->form_validation->error_string)){echo $this->input->post('enddate');} ?>"
	style="width: 80px; font-size: 11px;" /></div>
<button type="submit" class="button icon-search margin-left">Filter</button>
<input type="hidden" name="page" value="filter"></form>
</div>
<div class="back"><span class="back-arrow"></span>Back</div>
<div class="panel-load-target with-padding "
	style="min-height: 500px; position: relative;"><?php if($aud=="audit"){
		if($detail){ ?>

<table class="table" id="message_table" style="clear: both;">
	<thead>
		<tr>
			<th>S No</th>
			<th>Message Id</th>
			<th>Subject</th>
			<th>Body</th>
			<th>Sent Time</th>
			<th>To</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i="1";
	foreach ($detail as $row){
		?>

		<tr class="even_gradeA">
			<td style="width: 10px;"><?php echo $i;?></td>
			<td style="width: 10px;"><?php echo $row['message_id'];?></td>
			<td class="left"><?php echo $row['subject'];?></td>
			<td class="left""><?php echo nl2br($row['body']);?></td>
			<td class="left"><?php echo date("j M, Y, g:i a",strtotime($row['sent']));?></td>
			<td class="left"><?php echo $row['user_name'];?></td>
		</tr>
		<?php
		$i++;

	} ?>
	</tbody>

</table>
<br>
<script type="text/javascript" charset="utf-8">
                            $('#message_table').dataTable({
                                "aLengthMenu": [[ 25, 50,100, -1], [25, 50, 100,"All"]],
                                 "iDisplayLength": 50,
                                 "sPaginationType": "full_numbers"
                            });
                       </script> <?php }else{echo "<center>No result found</center>";} } ?>

</div>
</div>
</div>

</div>
</section>

	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>
<link
	rel="stylesheet"
	href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script
	src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script
	src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script type="text/javascript">
                jQuery.noConflict();
                jQuery("#startdate" ).datepicker();
                jQuery("#enddate" ).datepicker();
            </script>
</body>
</html>
