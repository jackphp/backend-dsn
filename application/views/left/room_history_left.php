<script language="javascript" type="text/javascript">
    $(function(){

        var view_as = '<?php echo $view_as; ?>';
        
        if(view_as == 'room') {
            $("#rooms").children("select").removeAttr("disabled");
            $("#floors").hide();
            $("#rooms").show();
            $("#room_type").attr("disable", "disable");
            $("#room").attr("checked", "checked");
        }
        
        else if(view_as == 'floor') {
            $("#floors").children("select").removeAttr("disabled");
            $("#floors").show();
            $("#rooms").hide();
            $("#room_type").removeAttr("disable");
            $("#room_type_container").show();
            $("#floor").attr("checked", "checked");
        }
        
        else if(view_as == 'hotel') {
            //$("#floors").show();
            $("#rooms").children("select").attr("disabled", "disabled");
            $("#rooms").hide();
            $("#room_type").removeAttr("disable");
            $("#room_type_container").show();
            $("#floors").children("select").attr("disabled", "disabled");
            $("#hotel").attr("checked", "checked");
        }

        $(".viewopt").click(function(){
            if($(this).val() == "room") {
                $("#floors").hide();
                $("#room_type_container").hide();
                $("#room_type").attr("disable", "disable");
                $("#rooms").show();
                $("#rooms").children("select").removeAttr("disabled");
//                var action = $("#form1").attr("action");
//                var action_arr = action.split("/");
//                var new_action = "";
//                for(var i=0; i<action_arr.length; i++) {
//                    if(action_arr[i] == "floor") {
//                        action_arr[i] = "room";
//                    }
//                    new_action += action_arr[i]+"/";
//                }
//
//                $("#form1").attr("action", new_action);
            }
            if($(this).val() == "floor") {
                $("#floors").children("select").removeAttr("disabled");
                $("#floors").show();
                $("#room_type").removeAttr("disable");
                $("#room_type_container").show();
                $("#rooms").hide();

//                var action = $("#form1").attr("action");
//                var action_arr = action.split("/");
//                var new_action = "";
//                for(var i=0; i<action_arr.length; i++) {
//                    if(action_arr[i] == "room") {
//                        action_arr[i] = "floor";
//                    }
//
//                    new_action += action_arr[i]+"/";
//                }
//
//                   $("#form1").attr("action", new_action);
            }

            if($(this).val() == "hotel") {
                $("#rooms").children("select").attr("disabled", "disabled");
                $("#room_type").removeAttr("disable");
                $("#room_type_container").show();
                //$("#rooms").hide();
                $("#floors").children("select").attr("disabled", "disabled");
            }
        });

        $("#from_date").datepicker({dateFormat:"yy-mm-dd", selectDefaultDate:"<?php echo $from_date; ?>"});
        $("#to_date").datepicker({dateFormat:"yy-mm-dd", selectDefaultDate:"<?php echo $to_date; ?>"});


        
    }) // End Of Jquery

    function chk_report_filter_form() {
        var view_as;
        $(".viewopt").each(function(){
            if($(this).attr("checked") == true) {
                view_as = $(this).val();
            }
        });

        if(view_as == "room") {
            if($("#rooms").children("select").val() == "") {
                alert("Please Select Room.");
                return false;
            }
        }

        if(view_as == "floor") {
            if($("#floors").children("select").val() == "") {
                alert("Please Select Floor.");
                return false;
            }
        }

    }
</script>
<table cellpadding="2" cellspacing="2">
	<tr>
		<td><?php echo form_open($left_action, array('name'=>'form1', 'id'=>'form1', 'method'=>'post', 'onsubmit'=>'return chk_report_filter_form()'))?>
		<table cellpadding="5" cellspacing="0" class="border_tbl" width="100%">
			<tr class="contentheading">
				<td colspan="3">Report Filter:</td>
			</tr>
			<tr>
				<td colspan="3"><input type="radio" name="viewas" value="room"
					class="viewopt" id="room"><label for="room"> Room wise</label>
				&nbsp; <input type="radio" name="viewas" value="floor"
					class="viewopt" id="floor"> <label for="floor">Floor wise</label>
				&nbsp; <input type="radio" name="viewas" value="hotel"
					class="viewopt" id="hotel" /> <label for="hotel">Hotel wise</label></td>
			</tr>
			<tr>
				<td colspan="3"><span style="float: left; margin-right: 2%;">Select:</span>
				&nbsp;
				<div id="rooms" style="float: left;"><?php echo $rooms; ?></div>
				<div id="floors" style="float: left;"><?php echo $floors; ?></div>
				</td>
			</tr>
			<tr>
				<td>From: <?php echo form_input('from_date', $from_date, 'id="from_date" size="7"'); ?></td>
				<td>To: <?php echo form_input('to_date', $to_date, 'id="to_date" size="7"'); ?></td>
				<td><br />
				</td>
			</tr>
			<tr id="room_type_container" style="dispaly: none;">
				<td colspan="3">Room Type: &nbsp; <?php $room_types = get_var('room_type'); $types = array(''=>'--Select--'); foreach($room_types as $type){$types[$type->value] = $type->value;}
				echo form_dropdown('room_type', $types, $room_type, 'id="room_type"');
				?></td>
			</tr>
			<tr>
				<td colspan="3"><b>Age Group:</b></td>
			</tr>
			<tr>
				<td colspan="3">
				<table cellpadding="0" cellspacing="0" width="100%">
				<?php $age_groups = get_var('age_group');

				$i=0;
				foreach($age_groups as $group) {
					if($i%3 == 0) {
						echo "</td></tr><tr><td>";
					}
					else {
						echo "</td><td>";
					}
					echo '<input type="checkbox" name="age_group[]" value="'.$group->value.'" id="edit-age_group-'.$group->value.'"/>  <label for="edit-age_group-'.$group->value.'">'.$group->value.'</label>';
					$i++;
				}
				?>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;<input type="submit" name="submit"
					value="Show Report" /></td>
			</tr>
			<tr>
				<td colspan="3"><input type="hidden" name="form_id"
					value="left_view_form" /></td>
			</tr>
		</table>
		<?php echo form_close();?></td>
	</tr>

	<tr>
		<td>
		<div id="accordion">
		<h3><a href="#">ATS</a></h3>
		<div class="links"><a
			href="<?php echo base_url();?>room_history/ats/<?php echo $view_as."/".$rf_id; ?>">
		<div class="tab">Key Tag</div>
		</a> <a href="#">
		<div class="tab">DND</div>
		</a> <a href="#">
		<div class="tab">Make My Room</div>
		</a> <a href="#">
		<div class="selected">Butler Request</div>
		</a> <a href="#">
		<div class="tab">Under Maintenance</div>
		</a>
		<p></p>
		</div>
		<h3><a href="#">Newspaper</a></h3>
		<div class="links"><a href="#">
		<div class="tab">Most Read Newspaper</div>
		</a> <a href="#">
		<div class="tab">Most Hitted Newspaper</div>
		</a>
		<p></p>
		</div>

		<h3><a href="#">PMS</a></h3>
		<div class="links"><a href="#">
		<div class="tab">Alarm</div>
		</a> <a href="#">
		<div class="tab">View Guest Bill Info</div>
		</a>
		<p></p>
		</div>

		<h3><a href="#">POSI</a></h3>
		<div class="links"><a href="#">
		<div class="tab">Food Orders</div>
		</a>
		<p></p>
		</div>

		<h3><a href="#">RMS</a></h3>
		<div class="links"><a
			href="<?php echo base_url();?>room_history/rms/<?php echo $view_as."/".$rf_id; ?>">
		<div class="tab">AC</div>
		</a> <a
			href="<?php echo base_url();?>tv_reports/reports/<?php echo $view_as."/".$rf_id; ?>">
		<div class="tab">TV</div>
		</a> <!--<div style="margin-left: 5px; display: none;">
                        <a href="<?php echo base_url();?>room_history/tv/<?php //echo $room_id; ?>"><div class="tab">TV</div></a>
                        <a href="#"><div class="tab">Most Viewed Movies</div></a>
                        <a href="#"><div class="tab">Most Liked Songs</div></a>
                        <a href="#"><div class="tab">Most Listen Radio Channels</div></a>
                    </div>--> <a
			href="<?php echo base_url();?>room_history/lights/<?php echo $view_as."/".$rf_id; ?>">
		<div class="tab">Lights & Dimmer</div>
		</a> <a href="#">
		<div class="tab">Save Energy</div>
		</a> <a href="#">
		<div class="tab">Door Bell</div>
		</a>
		<p></p>
		</div>
		</div>
		</td>
	</tr>
	<tr>
		<td>
		<table cellpadding="5" cellspacing="0" class="border_tbl" width="100%">
			<tr class="contentheading">
				<td>Other:</td>
			</tr>
			<tr>
				<td>
				<div class="links"><a href="#">
				<div class="tab">Top Guests</div>
				</a> <a href="#">
				<div class="tab">Most Used DigiValet Feature</div>
				</a> <a href="#">
				<div class="tab">Top Events</div>
				</a>
				<p></p>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
