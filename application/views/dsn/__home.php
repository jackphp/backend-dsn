<?php
$this -> load -> view('dsn/external/header_resource');
echo "<title>".$page['module_name']."</title>";

echo '<header role="banner" id="title-bar">';
echo '<h2>'. HOTEL_NAME .'</h2>';
echo '</header>';


//Button to open/hide menu
echo '<a href="#" id="open-menu"><span>Menu</span> </a>';

//Button to open/hide shortcuts
echo '<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span> </a>';

//Main content
echo '<section role="main" id="main"><noscript class="message black-gradient simpler">Your browser does notsupport JavaScript! Some features won\'t work as expected...</noscript>';
echo '<hgroup id="main-title" class="thin" style="margin-top:5px;!important">';

echo '<h1>'. $page['module_name'].'</h1>';
echo '</hgroup>';
echo '<button id="clickToChat" class="button grey-gradient glossy" style="margin-top: -41px; margin-left: 521px;float:left;">Click To Chat</button>';
?>

<style>
.content-panel {
	padding-left: 0px !important;
}
.wallAction {
	position: relative;
	list-style-type: none;
	font-size: 12px;
	margin-left: 0;
	margin-bottom: 0;
}
.wallAction li {
	display: inline-block;
	padding: 5px;
}
#file_upload {
	padding: 14px 0 0 5px;
}
#file_upload-queue,#uploadPhotoButton {
	margin: 5px;
}

.uploadify-queue-item {
	color: #000;
}
#viewMoreComments:hover {
	background: #ffccccc;
	cursor: pointer;
}
#newsticker-container a {
	color: #D83B97;
	text-decoration: none;
}
#newsticker-container {
	width: auto;
	margin: auto;
	border: 5px solid #eeeeee;
}
#newsticker-container ul li div {
	border-top: 1px solid #e2e2e2;
	background: #ffffff;
	padding: 10px 5px;
}
.postImage {
	border: 2px solid #000;
}

.linkTypeText {
	color: #0059a0;
}
.linkTypeText:hover {
	cursor: pointer;
	background: #ffffaa;
}
/*Ticker css*/
.ticker li h2 {
	margin: 0px;
	font-family: arial, sans-serif;
	font-size: 16px;
	line-height: 20px;
	font-weight: 800;
	color: rgb(195, 0, 25);
}
.ticker li img {
	margin: 5px 10px 10px 0px;
	float: left;
}
#uNameCommented { /*margin: 16px;*/
	font-family: arial, sans-serif;
	font-size: 13px;
	line-height: 20px;
	font-weight: 400;
	margin-bottom: 0px;
}
p {
	-ms-word-break: break-all;
	word-break: break-all; // Non standard for webkit word-break :
	break-word;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	hyphens: auto;
}
.ago {
	color: #666666;
	font-size: 10px;
}
/**/
.boardMessage {
	background: #fff;
	font-size: 14px;
	color: #3c3c3c;
	font-style: italic;
}
#scrollbox3,.feedDetail {
	list-style-type: none;
	list-style-position: inside;
	height: 800px;
	overflow-y: scroll;
}
#contentLiOne:hover,#contentLiTwo:hover {
	cursor: pointer;
}
#update_button, .comment_button
{
	margin-top: 5px;
}
.ticker a
{
	color: #000;
}
.ticker a:hover
{
	background: #F2F5A9;
}
#container
{
	width: 850px;
	height: 440px;
	padding: 0;
	float: right;
	overflow:hidden;
}
textarea
{
	resize: none;
}
button::-moz-focus-inner, input[type="button"]::-moz-focus-inner {
  border: 0;
}
</style>

<div id="chatBoxFlash" class="content-panel" style="height: 400px; position:absolute; width: 37%; background: #fff;display:none;z-index: 999;margin-top:1.7%;margin-left:20.8%;dispaly:none;">
<div style="float:left;">
<?php $this -> load -> view('dsn/SparkWeb'); ?>
</div>
</div>

<div class="dsn_wrapper" style="padding: 15px;">
<div class="viewWrapper">
<div class="columns" style="position: relative;">
<div class="content-panel"
	style="height: 800px; float: left; width: 16.5%;"><!-- Ticker -->
<ul id="scrollbox3" class="ticker" style="margin-left: 2px">
	<li></li>
</ul>
<!--  --> <!--<a class="Prev" href="#">Prev</a>
<a class="Next" href="#">Next</a>--></div>
<div class="content-panel scrollable contentOnScroll"
	style="height: 800px; float: left; width: 78%; overflow-y: scroll; margin-left: 2.70%"
	id="scrollbox3">
<div class="panel-content linen">
<div id="wall_container">
<div id="updateboxarea">
<div>
<ul class="wallAction">
	<li><a href="" id="updateStatus"><span class="icon-pencil icon-size1 icon-green"> Update Status</span></a></li>
	<li><a href="" id="addPhotos"><span class="icon-pictures icon-size1 icon-green"> Add Photos</span></a></li>
</ul>
<input
	type="hidden" name="realtime_l_id" id="realtime_l_id">
<input
	type="hidden" name="realtime_c_id" id="realtime_c_id">
<input
	type="hidden" name="realtime_m_id" id="realtime_m_id">
<input
	type="hidden" name="post_on_same_page" id="post_on_same_page" value="0">
<em>What's up?</em></div>
<form method="post" action="/dsn/uploadStatusImage"
	enctype="multipart/form-data">
<div id="updateStatusContainer"><textarea cols="30" rows="4"
	name="update" id="update" maxlength="200" style="resize: none;"
	placeholder="Write on Wall..." class="input"></textarea></div>
<div style="display: none" id="addPhotosContainer"><textarea cols="73"
	rows="4" name="update" id="updateImageTitle" maxlength="200"
	style="resize: none;" placeholder="Add a title..." class="input"></textarea>
<input type="file" name="statusImageFile" size="20" id="file_upload"> <a
	href="javascript:$('#file_upload').uploadify('upload','*')"
	class="button" id="uploadPhotoButton"
	style="display: none; padding: 5px;">Upload Files</a></div>
<input type="button" value=" Update " id="update_button"
	class="update_button button grey-gradient glossy">

</form>
</div>
<div id='flashmessage'>
<div id="flash" align="left"></div>
</div>
<div id="content"><?php 
$vars['updatesarray'] 		= $updatesarray;
$vars['linkObject'] 		= $linkObject;
$vars['wallObject'] 		= $wallObject;
$vars['timeStampObject'] 	= $timeStampObject;

$this -> load -> view('dsn/load_messages.php', $vars); ?></div>
</div>
</div>
</div>

</div>
</div>
</div>
</section>
<!-- End main content -->
<?php $this -> load -> view('dsn/external/side_left_pages'); ?>
