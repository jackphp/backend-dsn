<?php
if(!isset($ajaxCall))
{
	$i=0;
	$viewMore = "";
	$j=0;
	if($commentsarray!=NULL)
	{
		foreach($commentsarray as $cdata)
		{
			$i++;
			if($i<4)
			{
				$com_id					=	$cdata -> com_id;
				$comment				=	$linkObject -> tolink_evaluate(htmlentities($cdata -> comment));
				$time					=	$cdata -> created;
				$username				=	$cdata -> username;
				$uid					=	$cdata -> uid_fk;
				$msg_id					=	$cdata -> msg_id_fk;
				$cface					=	$wallObject->Gravatar($uid);
				$cappropriate			=	$cdata -> inappropriate;
				?>
<div class="stcommentbody" id="stcommentbody<?php echo $com_id; ?>" style="<?php if($cappropriate==1){ echo "opacity:0.3"; } ?>">
	<div class="stcommentimg">
		<img src="<?php echo $cface; ?>" class='small_face' />
	</div>
	<div class="stcommenttext">
		<a 	class="stcommentdelete" 
			href="#" id='<?php echo $com_id; ?>'
			dir="<?php echo $msg_id; ?>" 
			title='Delete Comment'
			data-spider='<?php echo $uid; ?>'
			>X</a> <b><?php echo $username; ?>
		</b>
		<?php echo $comment; ?>
		<div class="stcommenttime">
		<?php $timeStampObject -> time_stamp_evaluate($time); ?>
			| <a href="javascript:void(0);" id="markInappro<?php echo $com_id; ?>" dir="comment"
				data-set="<?php echo $com_id; ?>"
				data-group="<?php if($cappropriate==0){ echo "ap"; } else { echo "notap"; }?>"
				title="<?php if($cappropriate==0){ echo "Mark As Inappropriate"; } else { echo "Mark as Appropriate"; }?>"><span class="icon-buoy"></span> </a>
		</div>
	</div>
</div>
		<?php
			}
			else
			{

				if($j==0)
				{
					$viewMore 				= 	'<div id="viewMoreComments" style="color: #000;" class="'.$msg_id.'"><span>View More Comments</span></div>';
					echo '<div id="moreComments'.$msg_id.'">';
					$j++;
				}

				$com_id					=	$cdata -> com_id;
				$comment				=	$linkObject -> tolink_evaluate(htmlentities($cdata -> comment));
				$time					=	$cdata -> created;
				$username				=	$cdata -> username;
				$uid					=	$cdata -> uid_fk;
				$cface					=	$wallObject->Gravatar($uid);
				?>

<div class="stcommentbody" id="stcommentbody<?php echo $com_id; ?>"
	style="display: none;">
	<div class="stcommentimg">
		<img src="<?php echo $cface; ?>" class='small_face' />
	</div>
	<div class="stcommenttext">
		<a class="stcommentdelete" href="#" id='<?php echo $com_id; ?>'
			dir="<?php echo $msg_id; ?>" title='Delete Comment'>X</a> <b><?php echo $username; ?>
		</b>
		<?php echo $comment; ?>
		<div class="stcommenttime">
		<?php $timeStampObject -> time_stamp_evaluate($time); ?>
			| <a href="javascript:void(0);" id="markInappro<?php echo $com_id; ?>" dir="comment"
				data-set="<?php echo $com_id; ?>"
				data-group="<?php if($cappropriate==0){ echo "ap"; } else { echo "notap"; }?>"
				title="<?php if($cappropriate==0){ echo "Mark As Inappropriate"; } else { echo "Mark as Appropriate"; }?>"><span class="icon-buoy"></span> </a>
		</div>
	</div>
</div>
		<?php }
		}

		if($viewMore!="")
		{
			echo '</div>';
			echo $viewMore;
		}
	}
}
else
{
	foreach($commentsarray as $cdata)
	{
		$com_id					=	$cdata -> com_id;
		$comment				=	$linkObject -> tolink_evaluate(htmlentities($cdata -> comment));
		$time					=	$cdata -> created;
		$username				=	$cdata -> username;
		$uid					=	$cdata -> uid_fk;
		$msg_id					=	$cdata -> msg_id_fk;
		$cface					=	$wallObject->Gravatar($uid);
		$cappropriate			=	$cdata -> inappropriate;
		?>
<div class="stcommentbody detailCommentBody<?php echo $com_id; ?>"
	id="stcommentbody<?php echo $com_id; ?>">
	<div class="stcommentimg">
		<img src="<?php echo $cface; ?>" class='small_face' />
	</div>
	<div class="stcommenttext">
		<a class="stcommentdelete" href="#" id='<?php echo $com_id; ?>'
			dir="<?php echo $msg_id; ?>" title='Delete Comment'>X</a> <b><?php echo $username; ?>
		</b>
		<?php echo $comment; ?>
		<div class="stcommenttime">
		<?php $timeStampObject -> time_stamp_evaluate($time); ?>
			| <a href="javascript:void(0);" id="markInappro<?php echo $com_id; ?>" dir="comment"
				data-set="<?php echo $com_id; ?>"
				data-group="<?php if($cappropriate==0){ echo "ap"; } else { echo "notap"; }?>"
				title="<?php if($cappropriate==0){ echo "Mark As Inappropriate"; } else { echo "Mark as Appropriate"; }?>"><span class="icon-buoy"></span></a>
		</div>
	</div>
</div>
<?php
	}
}
?>
