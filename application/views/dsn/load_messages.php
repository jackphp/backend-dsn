<?php
$vars['linkObject'] 		= 	$linkObject;
$vars['wallObject'] 		= 	$wallObject;

$vars['timeStampObject'] 	= 	$timeStampObject;

if($updatesarray!=NULL)
{
	foreach($updatesarray as $data)
	{

		$msg_id					=		$data -> msg_id;
		$orimessage				=		trim($data -> message);
		$message				=		$linkObject -> tolink_evaluate(htmlentities($data -> message));
		$time					=		$data -> created;
		$username				=		$data -> username;
		$uid					=		$data -> uid_fk;
		$face					=		$wallObject->Gravatar($uid);
		$attached_image			=		$data -> attached_image;
		$likeStatus				=		$data -> like_status;
		$commentsarray			=		$wallObject->Comments($msg_id);
		$likessarray			=		$wallObject->likes($msg_id);
		$mappropriate			=		$data -> inappropriate_p;
		$vars["commentsarray"]	=		$commentsarray;
		echo "<script>$('#stexpand".$msg_id."').oembed('".$orimessage."', { maxWidth: 400, maxHeight: 300 })</script>";
?>
<div class="stbody" id="stbody<?php echo $msg_id;?>" style="<?php if($mappropriate==1){ echo "opacity:0.2"; } ?>">
	<div class="stimg">
		<img src="<?php echo $face;?>" class='big_face' />
	</div>
	<div class="sttext">
		<div>
			<a class="stdelete" href="#" id="<?php echo $msg_id;?>"
				title="Delete update"
				dir="<?php echo $uid; ?>">X</a> <b><?php echo $username;?> </b> <span
				style="color: #000;"><?php echo $message;?> </span>
		</div>
		<div>
		<?php if($attached_image!=NULL){ ?>
			<span><a
				href="<?php echo $this -> config -> item('getUrl').'upload/dsn/'.$attached_image; ?>"
				rel="lightbox" title="<?php echo $message;?>"> <img
					src="<?php echo $this -> config -> item('getUrl').'upload/dsn/'.$attached_image; ?>"
					width="400px" height="300px" class="postImage"> </a> </span>
					<?php } ?>
		</div>
		<div>
			<div class="sttime">
				<?php $timeStampObject -> time_stamp_evaluate($time);?>
				| <a href='javascript:void(0);'
					class='<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'alcLike'.$msg_id; } else { echo 'like'.$msg_id; } ?>'
					id='<?php echo $msg_id;?>' title='Like this post'><?php if($likeStatus==0){ echo "Like"; } else { echo "Unlike"; } ?>
				</a> | <a href='javascript:void(0);'
					class='<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'commentopenAlc'; } else { echo 'commentopen detailCommentOpen'; } ?>'
					id='<?php echo $msg_id;?>' title='Comment'>Comment</a> | <span
					class="icon-chat icon-size1 icon-blue linkTypeText"
					id="<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'totalComments_alc'.$msg_id; } else { echo 'totalComments'.$msg_id; } ?>"><?php echo count($commentsarray); ?>
				</span> | <span class="icon-like icon-size1 icon-blue linkTypeText"
					id="<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'totalLikes_alc'.$msg_id; } else { echo 'totalLikes'.$msg_id; } ?>"><?php echo count($likessarray); ?>
				</span> | 
				<a href="javascript:void(0);" id="markInapproPost<?php echo $msg_id; ?>" dir="post"
					data-set="<?php echo $msg_id; ?>" 
					title="<?php if($mappropriate==0){ echo "Mark As Inappropriate"; } else { echo "Mark as Appropriate"; }?>"
					data-group="<?php if($mappropriate==0){ echo "ap"; } else { echo "notap"; }?>"
					><span class="icon-buoy"></span> </a>
			</div>
		</div>
		<div id="stexpandbox">
			<div id="stexpand<?php echo $msg_id;?>"></div>
		</div>
		<div class="commentupdate"
			id="<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'commentboxAlc'.$msg_id; } else { echo 'commentbox'.$msg_id; } ?>"
			style="display: none;">
			<div class="stcommentimg">
				<img src="<?php echo $face;?>" class='small_face' />
			</div>
			<div class="stcommenttext">
				<form method="post" action="">
					<textarea name="comment" class="comment input" maxlength="200"
						id="<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'ctextarea_alc'.$msg_id; } else { echo 'ctextarea'.$msg_id; } ?>"
						style="resize: none; max-height: 300px;"
						placeholder="Post Comment..."></textarea>
					<br /> <input type="button" value=" Comment "
						id="<?php echo $msg_id;?>"
						class="<?php if(isset($ajaxLoadContent) && $ajaxLoadContent==TRUE){ echo 'comment_button_alc'; } else { echo 'comment_button'; } ?> button grey-gradient glossy commentButton<?php echo $msg_id;?>" />
				</form>
			</div>
		</div>
		<div class="commentcontainer" id="commentload<?php echo $msg_id;?>">
		<?php $this -> load -> view('dsn/load_comments.php', $vars); ?>
		</div>

	</div>
</div>

<?php
	} ?>
<div id="contentLoader"
	align="center"></div>
<?php }
	else
	{
		/*echo '<div style="color: #c3c3c3;" align="center"><h1>Wall is Empty</h1></div>';*/

	}
?>
