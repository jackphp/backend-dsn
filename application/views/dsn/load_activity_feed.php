<?php
$feed = json_decode($feed);
$likes = $feed[0];
$comments = $feed[1];

if(count($comments) > 0)
{
	foreach($comments as $iComments)
	{
		echo '<li id="contentLiOne">';
		echo '<div><img src="'.BASE_URL.'/upload/dsn/'.$iComments -> user_image.'"></div>';
		echo '<div>';
		echo '<div><p><a href="uNameDetails/'.base64_encode($iComments -> uid).'" id="uNameCommented" class="jTip"><b>'.$iComments -> username.'</b></a></p></div>';
		echo '<p style="margin-bottom:0px;"><a id="postNotificationDetailBox" href="get_message/'.$iComments -> msg_id_fk.'" class="jTip" id="'.$iComments -> msg_id_fk.'"> commented on a post </a></p>';
		if(strlen($iComments -> comment)>20)
		{
			$commentText = substr($iComments -> comment, 0, 20).'..';
			echo '<div><p style="margin-bottom: 20px;"><a id="largeComment" href="showComment/'.base64_encode($iComments -> comment).'/'.base64_encode($iComments -> msg_id_fk).'/'.base64_encode($iComments -> username).'/'.base64_encode($iComments -> com_id).'/'.base64_encode($iComments -> created).'" class="jTip"><b>Comment: </b>'.ucfirst($commentText).'</a></p></div>';
		}
		else
		{
			$commentText = $iComments -> comment;
			echo '<div><p style="margin-bottom: 20px;"><b>Comment: </b>'.ucfirst($commentText).'</p></div>';
		}
		
		echo '</li>';
		
		echo '<script>notify("New Comment Posted", "'.$iComments -> username.' commented on a post. Comment: '.ucfirst($iComments -> comment).'", { showCloseOnHover: false });</script>';
		echo '<script>$.playSound("'.BASE_URL.'/notificationSound.mp3");</script>';
	}
}

if(count($likes) > 0)
{
	foreach($likes as $iLikes)
	{
		echo '<li id="contentLiTwo">';
		echo '<div><img src="'.BASE_URL.'/upload/dsn/'.$iLikes -> user_image.'"></div>';
		echo '<div><p><a href="uNameDetails/'.base64_encode($iLikes -> uid).'" id="uNameCommented" class="jTip"><b>'.$iLikes -> username.'</b><a></p></div>';
		echo '<p style="margin-bottom:0px;"><a href="get_message/'.$iLikes -> msg_id_fk.'" class="jTip" id="'.$iLikes -> msg_id_fk.'"> likes a post </a></p>';
		echo '<div><p style="margin-bottom: 20px;"><b>Post: </b>'.ucfirst($iLikes -> message).'<p></div>';
		echo '</li>';
		echo '<script>notify("One Post Liked", "'.$iLikes -> username.' likes a post. Post: '.ucfirst($iLikes -> message).'", { showCloseOnHover: false });</script>';
		echo '<script>$.playSound("'.BASE_URL.'/notificationSound.mp3");</script>';
	}
}
?>
