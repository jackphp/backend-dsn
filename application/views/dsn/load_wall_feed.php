<?php
$feed = json_decode($wall_feed);
$messages = $feed[0];

if(count($messages) > 0)
{
	foreach($messages as $iMessages)
	{
		$commentsarray				=		$wallUpdates->Comments($iMessages -> msg_id);
		$likessarray				=		$wallUpdates->likes($iMessages -> msg_id);
		$vars['linkObject'] 		= 		$linkObject;
		$vars['wallObject'] 		= 		$wallUpdates;
		$vars['timeStampObject'] 	= 		$timeStampObject;
		$vars['commentsarray']		=		$wallUpdates->Comments($iMessages -> msg_id);
		?>
<div class="stbody" id="stbody<?php echo $iMessages -> msg_id;?>">
<div class="stimg"><img
	src="<?php echo $wallUpdates -> Gravatar($iMessages -> uid); ?>"
	class='big_face' /></div>
<div class="sttext">
<div><a class="stdelete" href="#"
	id="<?php echo $iMessages -> msg_id; ?>" title="Delete update">X</a> <b><?php echo $iMessages -> username;?>
</b> <span style="color: #000;"><?php echo $iMessages -> message;?></span>
</div>

<div><?php if($iMessages -> attached_image!=NULL){ ?> <span><a
	href="<?php echo $this -> config -> item('getUrl').'upload/dsn/'.$iMessages -> attached_image; ?>"
	rel="lightbox" title="<?php echo $iMessages -> message; ?>"> <img
	src="<?php echo $this -> config -> item('getUrl').'upload/dsn/'.$iMessages -> attached_image; ?>"
	width="400px" height="300px" class="postImage"></a></span> <?php } ?></div>
<div>
<div class="sttime"><?php $timeStampObject -> time_stamp_evaluate(time()); ?>
| <a href='javascript:void(0);'
	class='like<?php echo $iMessages -> msg_id;?>'
	id='<?php echo $iMessages -> msg_id;?>' title='Like this post'><?php if($iMessages -> like_status==0){ echo "Like"; } else { echo "Unlike"; } ?></a>
| <a href='#' class='commentopen detailCommentOpen'
	id='<?php echo $iMessages -> msg_id; ?>' title='Comment'>Comment</a> |
<span class="icon-chat icon-size1 icon-blue linkTypeText"
	id="totalComments<?php echo $iMessages -> msg_id; ?>"><?php echo count($commentsarray); ?></span>
| <span class="icon-like icon-size1 icon-blue linkTypeText"
	id="totalLikes<?php echo $iMessages -> msg_id;?>"><?php echo count($likessarray); ?></span></div>
</div>
<div id="stexpandbox">
<div id="stexpand<?php echo $iMessages -> msg_id;?>"></div>
</div>
<div class="commentupdate" style='display: none'
	id='commentbox<?php echo $iMessages -> msg_id;?>'>
<div class="stcommentimg"><img
	src="<?php echo $wallUpdates -> Gravatar($iMessages -> uid); ?>"
	class='small_face' /></div>
<div class="stcommenttext">
<form method="post" action=""><textarea name="comment" class="comment"
	maxlength="200" id="ctextarea<?php echo $iMessages -> msg_id;?>"
	style="resize: none; max-height: 300px;" placeholder="Post Comment..."></textarea>
<br />
<input type="submit" value=" Comment "
	id="<?php echo $iMessages -> msg_id;?>"
	class="button grey-gradient glossy" " /></form>
</div>
</div>
<div class="commentcontainer"
	id="commentload<?php echo $iMessages -> msg_id;?>"><?php $this -> load -> view('dsn/load_comments.php', $vars); ?>
</div>

</div>
</div>
		<?php }
}
?>
