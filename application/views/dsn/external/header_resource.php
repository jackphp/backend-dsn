<?php
/*For the IE Family Browsers*/
echo '<!DOCTYPE html>';
echo '<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->';
echo '<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->';
echo '<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->';
echo '<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9" lang="en"><![endif]-->';
echo '<!--[if (gt IE 9)|(gt IEMobile 7)]><!-->';
echo '<html class="no-js" lang="en">';
echo '<!--<![endif]-->';
/**/

/*Dashboard File for including the previously defined CSS and JS files*/
include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';
/**/

/*DSN Resources*/
echo '<link href="'.get_assets_path('css').'dsn/wall.css" rel="stylesheet">';
echo '<link href="'.get_assets_path('css').'dsn/uploadify.css" rel="stylesheet">';
echo '<link href="'.get_assets_path('css').'dsn/facebook.alert.css" rel="stylesheet">';
echo '<link href="'.get_assets_path('css').'dsn/slimbox2.css" rel="stylesheet">';
echo '<link href="'.get_assets_path('css').'dsn/hoverDetail.css" rel="stylesheet">';
echo '<script src="'.get_assets_path('js').'dsn/hoverDetail.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/jquery.oembed.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/wall.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/jquery.uploadify.min.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/jquery_facebook.alert.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/slimbox2.js"></script>';
echo '<script src="'.get_assets_path('js').'dsn/jquery.playSound.js"></script>';
/**/
?>
<!-- DSN JS Script Resource -->
<script>
	<?php $timestamp = time();?>
		 $(function() {
			$('#file_upload').uploadify({
				'formData'     : {
				'timestamp' : '<?php echo $timestamp;?>',
				'token'     : '<?php echo md5('unique_salt' . $timestamp);?>'
			},
			'swf'      : '<?php echo get_assets_path('flash').'dsn/uploadify.swf'; ?>',
			'uploader' : "uploadStatusImage",
			'auto'		: false,
			'buttonClass' : 'grey-gradient glossy',
			'buttonText'	:	'Select photo',
			'height'		: 40,
			//'uploadLimit'	: 1,
			'width'			: 200,
			'onSelect'		: function(file){
					$("#uploadPhotoButton").css('display', 'inline-block');
			},
			'onUploadStart'	:	function()
			{
				if($("#updateImageTitle").val()=="")
				{
					jAlert("You Must Enter a Title", "Alert");
					$('#file_upload').uploadify('stop');
				}
			},
			'onUploadSuccess'	:	function(file)
			{
					$("#uploadPhotoButton").css('display', 'none');
					var updateval = $("#updateImageTitle").val();
					var imageval = file.name;
					
					var dataString = 'update='+ updateval;
					if(updateval=='')
					{
						jAlert("Please Enter Some Text", "Alert");
					}
					else
					{
						$("#flash").show();
						$("#flash").fadeIn(400).html('Loading Update...');
						$.ajax({
						type: "POST",
						url: "message_image_ajax/"+updateval+"/"+imageval,
						data: dataString,
						cache: false,
						success: function(html)
						{
						get_m_id();
						$("#flash").fadeOut('slow');
						$("#content").prepend(html);
						$("#update").val('');	
						$("#update").focus();
						$("#updateImageTitle").val('');
						$("#stexpand").oembed(updateval);
						callSlimBox();
						likePost();
						}
						 });
					}
				}
		});
			$("#addPhotos").click(function(e){
					e.preventDefault();
					$("#updateStatusContainer").css('display', 'none');
					$("#update_button").css('display', 'none');
					$("#addPhotosContainer").css('display', 'inline-block');
				}); 
			$("#updateStatus").click(function(e){
				e.preventDefault();
				$("#addPhotosContainer").css('display', 'none');
				$("#updateStatusContainer").css('display', 'block');
				$("#update_button").css('display', 'block');
			});
			viewMoreComments();
		$('#scrollbox3, .feedDetail').customScroll({  });
		
	});
	function viewMoreComments()
	{
		$("[id^='viewMoreComments']").click(function(){
			var moreCommentsClass = $(this).attr('class');
			$("#moreComments"+moreCommentsClass).children('div').slideDown();
			$(this).fadeOut('slow');
		});
	}
	$(window).load(function(){
			$("#open-menu").click();
			var from = 11;
			var to = 21;
			$(".contentOnScroll").scroll(function(){
				if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                {
	               last_msg_funtion(from, to);
	               
                }
				else
				{
					$('#contentLoader').hide();
				}
			});
			$('.comment_button_alc').live(
					"click",
					function() {

						var ID = $(this).attr("id");
						
						var comment = $("#ctextarea_alc" + ID).val();
						var dataString = 'comment=' + comment + '&msg_id=' + ID;
						
						if (comment == '') {
							jAlert("Please Enter Comment Text", "Alert");
						} 
						else {
							$.ajax( {
								type : "POST",
								url : "comment_ajax",
								data : dataString,
								cache : false,
								success : function(html) {
									$("#commentboxAlc" + ID).after(html);
									$("#ctextarea_alc" + ID).val('');
									$("#ctextarea_alc" + ID).focus();
									
									$.ajax( {
										type : "POST",
										url : "comment_data",
										data : dataString,
										success : function(data) {
											$("#totalComments_alc" + ID).text(
													jQuery.parseJSON(data).length);
										}
									});
								}
							});
						}
						return false;
					});
			function last_msg_funtion(fromVal, toVal)
			{
				$('#contentLoader').html('<?php echo '<img src='.get_assets_path("image").'contentLoader.gif />'; ?>');
				$.ajax({
					type : "POST",
					url : "ajax_load_content/"+fromVal+"/"+toVal,
					success : function(data) 
					{
						if (data != "") 
						{
							$("#contentLoader").before(data);
							from = fromVal + 10;
							to = to + 10;
							/*ALC Like*/
							$("[class^='alcLike']").click(function() {
							var ID = $(this).attr("id");
							var dataString = 'msg_id='+ID;
							$.ajax( {
								type : "POST",
								url : "like_message_ajax",
								data : dataString,
								success : function(data) {
									if(data==1)
									{
										if ($.trim($(".alcLike"+ID).html())=="Like") 
										{
											$(".alcLike"+ID).html("Unlike");
										} 
										else if($.trim($(".alcLike"+ID).html())=="Unlike")
										{
											$(".alcLike"+ID).html("Like");
										}
										
										$.ajax( {
											type : "POST",
											url : "like_data",
											data : dataString,
											success : function(data) {
												$("#totalLikes_alc"+ID).text(jQuery.parseJSON(data).length);
											}
										});
									}
									return false;
								}
				});
			});
						/**/
							markInappro();	
							alcCommentOpen();
							viewMoreComments();
							tooltipShow();		
						}
							$('[id^="contentLoader"]').html("");
							return false;
						}
				});
			}
});
function alcCommentOpen()
{
	$('.commentopenAlc').live("click", function() {
		var ID = $(this).attr("id");
		$("#commentboxAlc"+ID).slideToggle('slow');
		return false;
	});
	$(".commentopenAlc").toggle(
		function(){
			var ID = $(this).attr("id");
			$("#commentboxAlc"+ID).slideDown();
		},
		function(){
			var ID = $(this).attr("id");
			$("#commentboxAlc"+ID).slideUp();
		}
	);
}
$(function(){
	get_l_c_id();
	setInterval(function(){
		$.ajax({
			type : "POST",
			url : "get_realtime_activity_feeds/"+$("#realtime_l_id").val()+"/"+$("#realtime_c_id").val(),
			success : function(data) 
			{
				if (data != "") 
				{
					get_l_c_id();
					var activity_feed = "activity_feed="+data;
					
					$.ajax({
						type : "POST",
						url : "load_activity_feed",
						data: activity_feed,
						success : function(data) 
						{
							if(data!="")
							{
								$(".ticker").prepend(data);
								$(".ticker li:first-child").hide().fadeIn(2000);
								$(document).ready(JT_init);
							}
						}
						});
				}
			}
			});
		
		}, 4000);
});

function get_l_c_id()
{
	$.ajax({
		type : "POST",
		url : "get_l_c_id",
		success : function(data) 
		{
			setJSONDecodedData(data);
		}
		});
}
function setJSONDecodedData(data)
{
	var data = jQuery.parseJSON(data);
	$("#realtime_l_id").val(data[0]);
	$("#realtime_c_id").val(data[1]);
}

$(function(){
	get_m_id();
	setInterval(function(){
			$.ajax({
					type : "POST",
					url : "get_realtime_wall_feeds/"+$("#realtime_m_id").val(),
					success : function(data) 
					{
						if (data != "") 
						{
							get_m_id();
							var wall_feed = "wall_feed="+data;
							
							$.ajax({
								type : "POST",
								url : "load_wall_feed",
								data: wall_feed,
								success : function(data) 
								{
									if(data!="")
									{
										$("#content").prepend(data);
										markInappro();
									}
								}
								});
						}
					}
					});
			}, 4000);
});

function get_m_id()
{
	$.ajax({
		type : "POST",
		url : "get_m_id",
		success : function(data) 
		{
			setJSONDecodedDataForMessages(data);
		}
		});
}
function setJSONDecodedDataForMessages(data)
{
	var data = jQuery.parseJSON(data);
	$("#realtime_m_id").val(data[0]);
}
/*function submitOnEnter()
{
	$("[id^='ctextarea']").keypress(function(e){
			if(e.which == 13)
			{
				var wholeId = $(this).attr('id');
				wholeIdSubstr = wholeId.replace("ctextarea", "");
				$(".commentButton"+wholeIdSubstr).click();
			}
		});
}
$(function(){
	submitOnEnter();
});*/

$(window).load(function(){
	$("#clickToChat").toggle(
	function()
	{
		$("#chatBoxFlash").slideDown('slow');
		$(this).text("Hide Chat");
	},
	function()
	{
		$("#chatBoxFlash").slideUp('slow');
		$(this).text("Click To Chat");
	}
);
});
function callSlimBox()
{
	$("a[rel^='lightbox']").slimbox({/* Put custom options here */}, null, function(el) {
		return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
	});
}
</script>
<!--  -->
