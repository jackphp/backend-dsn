<html>
<head>
<title></title>
<link href="<?php  echo get_assets_path('css'); ?>others/style.css"
	type="text/css" rel="stylesheet" />
<script src="<?php echo get_assets_path('js'); ?>jquery-1.4.4.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>colorbox/colorbox/jquery.colorbox.js"></script>
<link
	href="<?php  echo get_assets_path('js'); ?>colorbox/example3/colorbox.css"
	type="text/css" rel="stylesheet" />
<script
	src="<?php echo get_assets_path('js'); ?>jquery.multiSelct/jquery.bgiframe.min.js"
	type="text/javascript"></script>
<script
	src="<?php echo get_assets_path('js'); ?>jquery.multiSelct/jquery.multiSelect.js"
	type="text/javascript"></script>
<link
	href="<?php echo get_assets_path('js'); ?>jquery.multiSelct/jquery.multiSelect.css"
	rel="stylesheet" type="text/css" />

<link
	href="<?php echo get_assets_path('js');?>scroll/style/jquery.jscrollpane.css"
	rel="stylesheet" type="text/css" />
<script
	src="<?php echo get_assets_path('js'); ?>scroll/script/jquery.mousewheel.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>scroll/script/jquery.jscrollpane.js"></script>
<script language="javascript" type="text/javascript">
            $(function(){
                $(".colorbox_cancel").click(function(){
                   parent.$.fn.colorbox.close();
                });

                <?php if(isset($colorbox_close) && $colorbox_close == 1) { ?>
                   <?php if(isset($redirect_location) && $redirect_location != ""){ ?>
                   parent.window.location = '<?php echo $redirect_location;?>';
                   <?php } else {?>
                       parent.window.location.reload();
                       <?php }?>
                   parent.$.fn.colorbox.close();
                <?php } ?>


            })
       </script>
<style type="text/css">
body {
	color: #6E6E6E;
	margin: 0px 0px 0px 0px;
	padding: 0px 0px 0px 0px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: normal;
	font-size: 80%;
}
</style>
</head>
<body style="background: #FFFFFF;">

<div id="page_message"><?php if(isset($page_message)) {echo $page_message;}else {echo '<div class="pagemsg">&nbsp;</div>';}?>
</div>
                <?php echo $content; ?>


</body>
</html>

