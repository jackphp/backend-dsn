<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Hotel Name</title>
<meta name="description" content="">
<meta name="author" content="">


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- For all browsers -->
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/reset.css?v=1" />
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/style.css?v=1" />
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/colors.css?v=1" />
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/files.css" />
<link rel="stylesheet" media="print"
	href="<?php  echo get_assets_path('css'); ?>backend/print.css?v=1" />
<!-- For progressively larger displays -->
<link rel="stylesheet" media="only all and (min-width: 480px)"
	href="<?php  echo get_assets_path('css'); ?>backend/480.css?v=1" />
<link rel="stylesheet" media="only all and (min-width: 768px)"
	href="<?php  echo get_assets_path('css'); ?>backend/768.css?v=1" />
<link rel="stylesheet" media="only all and (min-width: 1030px)"
	href="<?php  echo get_assets_path('css'); ?>backend/992.css?v=1" />
<link rel="stylesheet" media="only all and (min-width: 1200px)"
	href="<?php  echo get_assets_path('css'); ?>backend/1200.css?v=1" />
<!-- For Retina displays -->
<link rel="stylesheet"
	media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)"
	href="<?php  echo get_assets_path('css'); ?>backend/2x.css?v=1" />

<!-- Webfonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300'
	rel='stylesheet' type='text/css' />

<!-- Additional styles -->
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/table.css">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/agenda.css?v=1">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/dashboard.css?v=1">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/form.css?v=1">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/modal.css?v=1">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/progress-slider.css?v=1">
<link rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/switches.css?v=1">

<!-- JavaScript at bottom except for Modernizr -->
<script
	src="<?php echo get_assets_path('js'); ?>tv_channel/jquery-1.7.2.min.js"></script>
<script
	src="<?php  echo get_assets_path('js'); ?>dashboard/libs/modernizr.custom.js"></script>

<!-- For Modern Browsers -->
<link rel="shortcut icon"
	href="<?php  echo get_assets_path('css'); ?>img/favicons/favicon.png">
<!-- For everything else -->
<link rel="shortcut icon"
	href="<?php  echo get_assets_path('css'); ?>img/favicons/favicon.ico">
<!-- For retina screens -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php  echo get_assets_path('css'); ?>img/favicons/apple-touch-icon-retina.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php  echo get_assets_path('css'); ?>img/favicons/apple-touch-icon-ipad.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed"
	href="<?php  echo get_assets_path('css'); ?>img/favicons/apple-touch-icon.png">

<!-- Jquery dataTable css -->
<link rel="stylesheet"
	href="<?php  echo get_assets_path('js'); ?>dashboard/libs/DataTables/jquery.dataTables.css?v=1">

<!-- iOS web-app metas -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="<?php  echo get_assets_path('css'); ?>img/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
	href="<?php  echo get_assets_path('css'); ?>img/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"
	href="<?php  echo get_assets_path('css'); ?>img/splash/iphone.png"
	media="screen and (max-device-width: 320px)">

<!-- Microsoft clear type rendering -->
<meta http-equiv="cleartype" content="on">

<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
<meta name="application-name" content="DigiValet Admin Skin">
<meta name="msapplication-tooltip"
	content="Cross-platform admin template.">

<!-- These custom tasks are examples, you need to edit them to show actual pages -->

<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>dashboard/developr.tooltip.js"></script>
<!--<script src="<?php echo get_assets_path('js'); ?>dashboard/developr.input.js" ></script> -->

<!-- <script src="<?php echo get_assets_path('js'); ?>dashboard/jquery-ui-1.1.18.custom.min.js" ></script>-->

<!--  <script src="<?php echo get_assets_path('js'); ?>dashboard/developr.agenda.js" ></script>
        <script src="<?php echo get_assets_path('js'); ?>dashboard/developr.table.js" ></script>
        <script src="<?php echo get_assets_path('js'); ?>dashboard/developr.wizard.js" ></script>
        <script src="<?php echo get_assets_path('js'); ?>dashboard/developr.tabs.js" ></script> -->
<!--<script src="<?php echo get_assets_path('js'); ?>dashboard/setup.js" ></script> -->






<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<!--        <link href="<?php  echo get_assets_path('css'); ?>others/style.css" type="text/css" rel="stylesheet"/>
        <link href="<?php  echo get_assets_path('css'); ?>navbar.css" type="text/css" rel="stylesheet"/>-->
<script
	src="<?php echo get_assets_path('js'); ?>tv_channel/jquery-1.4.4.js"></script>
<script src="<?php echo get_assets_path('js'); ?>tv_channel/jClock.js"></script>
<script src="<?php echo get_assets_path('js'); ?>tv_channel/ext-core.js"></script>
<script src="<?php echo get_assets_path('js'); ?>tv_channel/ext-menu.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>tv_channel/colorbox/colorbox/jquery.colorbox.js"></script>

<link
	href="<?php  echo get_assets_path('js'); ?>tv_channel/colorbox/example3/colorbox.css"
	type="text/css" rel="stylesheet" />
<script type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/autocomplete/jquery.autocomplete.js"></script>
<link
	href="<?php echo get_assets_path('js'); ?>tv_channel/autocomplete/jquery.autocomplete.css"
	type="text/css" rel="stylesheet" />
<style>
.ux-menu a.current {
	background-image: url('../images/menu-item-bg-current.png');
	border-color: #cbc0b7;
}
</style>

<link rel="stylesheet"
	href="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/themes/base/jquery-ui.css">
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.core.js"></script>

<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.accordion.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.ui.tabs.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery-ui-1.8.5/ui/jquery.scrollable.tabs.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.dragsort-0.4.1.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/logging_main_page.js"></script>

<link
	href="<?php echo get_assets_path('js');?>tv_channel/jquery.alerts-1.1/jquery.alerts.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.alerts-1.1/jquery.alerts.js"></script>

<link
	href="<?php echo get_assets_path('js');?>tv_channel/scroll/style/jquery.jscrollpane.css"
	rel="stylesheet" type="text/css" />
<script
	src="<?php echo get_assets_path('js'); ?>tv_channel/scroll/script/jquery.mousewheel.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>tv_channel/scroll/script/jquery.jscrollpane.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jcoockie.js"></script>
<script language="javascript" type="text/javascript">

            Ext.onReady(function() {
                $("#simple-horizontal-menu").show();
                new Ext.ux.Menu('simple-horizontal-menu', {
                    transitionType: 'slide',
                    direction: 'horizontal', // default
                    delay: 0.2,              // default
                    autoWidth: true,         // default
                    transitionDuration: 0.3, // default
                    animate: true,           // default
                    currentClass: 'current'  // default
                });
                new Ext.ux.Menu('simple-vertical-menu', {
                    direction: 'vertical'
                });
             });
        </script>
<script language="javascript" type="text/javascript">
             $(function(){
                  <?php if(isset($active_menu_js) && $active_menu_js != "") {
                            echo $active_menu_js;
                        }
                  ?>
             });
           </script>
<script language="javascript" type="text/javascript">
          $(document).ready(function () {
$("#serverConnectionStatus").html("Connection OK");
        $("#serverConnectionStatus").attr("class", "icon-wifi icon-size2 glossy green");
      $("#serverHealth").html("Server live");
        $("#serverHealth").attr("class", "icon-lightning icon-size2 green");

 }); 
         

            
            $(function(){
                hide_message();
                $(".date").datepicker();
                var options = {
                        utc: true,
                        utc_offset: 5.5
                    };
                //$('.clock').jclock(options);


                $("#simple-horizontal-menu").hide();
                $(".delete").click(function(){
                      var del_location = $(this).attr("href");
                      jConfirm('<label>Are you sure delete this record?</label>', 'Confirmation', function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location;
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                })

                $(".tooltip").parent("td").append("<div class='tip'></div>");

                $(".tooltip").parent("div").append("<div class='tip'></div>");
                $(".tooltip").hover(function(){
                    var tipVal = $(this).attr('tipVal');
                    $(this).siblings("div").html(tipVal);

                    $(this).siblings("div").show();

                }, function(){

                    $(this).siblings("div").hide();
                });
                
                 // Adding Colorbox Popup
                 //$(".popup").colorbox({width:"80%", height:"80%", iframe:true});

		$("#appleNav li").click(function(){
                    var link = $(this).index();
                    var lastSelectedLink = $.cookie('selectedMenu');
                    if(lastSelectedLink > -1) {
                       var obj = $("#appleNav li").get(lastSelectedLink);
                       $(obj).removeClass("liselected");
                    }

                    var newObj = $("#appleNav li").get(link);
                    //$(newObj).addClass("liselected");
                    $.cookie('selectedMenu', link, { expires : 2, path: '/' });
               })

                select_menu();
            })
	    
	 
	    function select_menu() {
                if($.cookie('selectedMenu') > -1) {
                  var l = $.cookie('selectedMenu');
                  var ob = $("#appleNav li").get(l);
                  $(ob).addClass("liselected");
                }
            }	    
				
            function tooltip(obj, str) {
                $(obj).parent(expr)
            }

            function hide_message() {
                setTimeout(function(){
                    $(".pagemsg").hide();
                }, 30000);

            }
        </script>

                  <?php  //include(ASSETS_PATH."FusionCharts/FusionCharts.php");?>
<!--<SCRIPT LANGUAGE="Javascript" SRC="<?php //echo ASSETS_PATH; ?>FusionCharts/Code/FusionCharts/FusionCharts.js"></SCRIPT>-->
</head>
<body class="clearfix with-menu with-shortcuts">
<header id="title-bar" role="banner">
<h2><?php echo HOTEL_NAME; ?></h2>
</header>
<!--  <?php// include_once 'header.php'; ?> -->
                  <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>


<section role="main" id="main">

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title" class="thin">
<h1><?php echo $page_heading;?></h1>

</hgroup>

<div align="center" class="with-padding"><?php  echo $content; ?></div>
</section>
<!--         <div align="center"><?php//  echo $content; ?></div>     -->
                  <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
                  <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>

</body>
</html>
