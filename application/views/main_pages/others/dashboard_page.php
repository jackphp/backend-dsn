<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Hotel Name</title>
<meta name="description" content="">
<meta name="author" content="">


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<!-- For all browsers -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/reset.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/style.armani.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/colors.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/files.css">
<link rel="stylesheet" media="print"
	href="<?php echo get_assets_path('css');?>dashboard/print.css?v=1">
<!-- For progressively larger displays -->
<link rel="stylesheet" media="only all and (min-width: 480px)"
	href="<?php echo get_assets_path('css');?>dashboard/480.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 768px)"
	href="<?php echo get_assets_path('css');?>dashboard/768.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 992px)"
	href="<?php echo get_assets_path('css');?>dashboard/992.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 1024px)"
	href="<?php echo get_assets_path('css');?>dashboard/992.css?v=1">
<link rel="stylesheet" media="only all and (min-width: 1200px)"
	href="<?php echo get_assets_path('css');?>dashboard/1200.css?v=1">
<!-- For Retina displays -->
<link rel="stylesheet"
	media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)"
	href="<?php echo get_assets_path('css');?>dashboard/2x.css?v=1">

<!-- Login pages styles -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/login.css">

<!-- Webfonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300'
	rel='stylesheet' type='text/css'>



<!-- Additional styles -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/agenda.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/dashboard.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/form.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/modal.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/progress-slider.css?v=1">
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/switches.css?v=1">

<!-- JavaScript at bottom except for Modernizr -->
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/modernizr.custom.js"></script>

<!-- For Modern Browsers -->
<link rel="shortcut icon"
	href="<?php echo get_assets_path('css');?>img/favicons/favicon.png">
<!-- For everything else -->
<link rel="shortcut icon"
	href="<?php echo get_assets_path('css');?>img/favicons/favicon.ico">
<!-- For retina screens -->
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<?php echo get_assets_path('css');?>img/favicons/apple-touch-icon-retina.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<?php echo get_assets_path('css');?>img/favicons/apple-touch-icon-ipad.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed"
	href="<?php echo get_assets_path('css');?>img/avicons/apple-touch-icon.png">

<!-- Jquery dataTable css -->
<link rel="stylesheet"
	href="<?php echo get_assets_path('js');?>dashboard/libs/DataTables/jquery.dataTables.css?v=1">

<!-- iOS web-app metas -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('css');?>img/splash/ipad-landscape.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('css');?>img/splash/ipad-portrait.png"
	media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"
	href="<?php echo get_assets_path('css');?>img/splash/iphone.png"
	media="screen and (max-device-width: 320px)">

<!-- Microsoft clear type rendering -->
<meta http-equiv="cleartype" content="on">

<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
<meta name="application-name" content="DigiValet Admin Skin">
<meta name="msapplication-tooltip"
	content="Cross-platform admin template.">
<?php   //$user_page=$this->uri->segment(1);
//if($user_page!="user_form"){
?>
<!-- These custom tasks are examples, you need to edit them to show actual pages -->
<script type="text/javascript">
		var jsroot = '<?php echo JS_PATH; ?>' ;	
		var apiroot ='<?php echo JSON_PATH; ?>' ;
		var imagesroot = '<?php echo IMAGE_PATH; ?>';
		var cssroot ='<?php echo CSS_PATH; ?>';	
                var messageroot ='<?php echo MESSAGE_PATH; ?>';	
	</script>
<?php //}?>
</head>

<body class="clearfix with-menu with-shortcuts">

<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<!-- Title bar -->