<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<script>
$(document).ready(function() {

    var page='<?php echo $this->uri->segment(2);?>';
    var page_info='<?php echo $this->uri->segment(1);?>';
    
    if(page=="front_office"){
    $("#left_menu").addClass("current");
    }
    if(page=="inbox"){
    $("#left_menu2").addClass("current");
    }
    if(page=="ats"){
    $("#left_menu1").addClass("current");
    }
    if(page=="engg"){
    $("#left_menu3").addClass("current");
    }
    if(page=="audit_log"){
    $("#left_menu4").addClass("current");
    }
    if(page_info=="welcomletter"){
    $("#left_menu5").addClass("current");
    }
    });
</script>

<ul id="shortcuts" role="complementary"
	class="children-tooltip tooltip-right">
	<?php  $id=$this->session->userdata("user_id");
	if(($id!="1")&&($id!="4")){

		?>
		<?php foreach($left as $val){

			?>

			<?php if($val['categories_id']=="1" && $val['module_id']=="1" ) {?>
	<li id="left_menu"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/front_office"
		class="shortcut-dashboard" title="<?php echo $val['module_name'];?>"><?php echo $val['module_name'];?></a></li>
		<?php } ?>
		<?php if($val['categories_id']=="1" && $val['module_id']=="2" ) {?>
	<li id="left_menu1"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/ats"
		class="shortcut-settings" title="<?php echo $val['module_name'];?>"><?php echo $val['module_name'];?></a></li>
		<?php } ?>
		<?php if($val['categories_id']=="1" && $val['module_id']=="10" ) {?>
	<li id="left_menu5"><a
		href="<?php echo BASE_URL;?>index.php/welcomletter"
		class="shortcut-contacts" title="<?php echo $val['module_name'];?>"><?php echo $val['module_name'];?></a></li>
		<?php } ?>
		<?php if($val['categories_id']=="1" && $val['module_id']=="3" ) {?>
	<li id="left_menu2"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/inbox"
		class="shortcut-messages" title="<?php echo $val['module_name'];?>"><?php echo $val['module_name'];?></a></li>
		<?php } ?>
		<?php if($val['categories_id']=="1" && $val['module_id']=="8" ) {?>
	<li id="left_menu4"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/audit_log"
		class="shortcut-notes" title="<?php echo $val['module_name'];?>"><?php echo $val['module_name'];?></a></li>
		<?php } ?>

	<!--<li><a href="agenda.html" class="shortcut-agenda" title="Agenda">Agenda</a></li>
		<li><a href="tables.html" class="shortcut-contacts" title="Contacts">Contacts</a></li>
		<li><a href="explorer.html" class="shortcut-medias" title="Medias">Medias</a></li>
		<li><a href="sliders.html" class="shortcut-stats" title="Stats">Stats</a></li>
		<li><a href="form.html" class="shortcut-settings" title="Logs">Logs</a></li>
		<li><span class="shortcut-notes" title="Notes">Notes</span></li>-->
		<?php }
			
	}else{?>

	<li id="left_menu"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/front_office"
		class="shortcut-dashboard" title="Front Office Dashboard">Front Office
	Dashboard</a></li>
	<li id="left_menu1"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/ats"
		class="shortcut-settings" title="Digivalet Dashboard">Digivalet
	Dashboard</a></li>
	<li id="left_menu5"><a
		href="<?php echo BASE_URL;?>index.php/welcomletter"
		class="shortcut-contacts" title="Welcome Letter">Welcome Letter</a></li>
	<li id="left_menu2"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/inbox"
		class="shortcut-messages" title="Messaging">Messaging</a></li>
	<li id="left_menu4"><a
		href="<?php echo BASE_URL;?>index.php/digivalet_dashboard/audit_log"
		class="shortcut-notes" title="Audit Log">Audit Log</a></li>
	<!--<li id="left_menu4"><a href="<?php echo BASE_URL;?>index.php/dsn/home" class="shortcut-notes" title="Digivalet Social Network">Digivalet Social Network</a></li>-->
		<?php } ?>
</ul>

