<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<link href="<?php  echo get_assets_path('css'); ?>ie/style.css"
	type="text/css" rel="stylesheet" />
<!--<link href="<?php  echo get_assets_path('css'); ?>navbar.css" type="text/css" rel="stylesheet"/>-->
<script src="<?php echo get_assets_path('js'); ?>jquery-1.4.4.js"></script>
<script src="<?php echo get_assets_path('js'); ?>jClock.js"></script>
<script src="<?php echo get_assets_path('js'); ?>ext-core.js"></script>
<script src="<?php echo get_assets_path('js'); ?>ext-menu.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>colorbox/colorbox/jquery.colorbox.js"></script>
<script
	src="<?php echo get_assets_path('chart'); ?>Charts/FusionCharts.js"></script>
<link
	href="<?php  echo get_assets_path('js'); ?>colorbox/example3/colorbox.css"
	type="text/css" rel="stylesheet" />
<script type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>autocomplete/jquery.autocomplete.js"></script>
<link
	href="<?php echo get_assets_path('js'); ?>autocomplete/jquery.autocomplete.css"
	type="text/css" rel="stylesheet" />
<link rel="stylesheet"
	href="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/themes/base/jquery-ui.css">
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.core.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.accordion.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.tabs.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.scrollable.tabs.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery.dragsort-0.4.1.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>logging_main_page.js"></script>

<link
	href="<?php echo get_assets_path('js');?>jquery.alerts-1.1/jquery.alerts.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery.alerts-1.1/jquery.alerts.js"></script>

<link
	href="<?php echo get_assets_path('js');?>scroll/style/jquery.jscrollpane.css"
	rel="stylesheet" type="text/css" />
<script
	src="<?php echo get_assets_path('js'); ?>scroll/script/jquery.mousewheel.js"></script>
<script
	src="<?php echo get_assets_path('js'); ?>scroll/script/jquery.jscrollpane.js"></script>
<script type="text/javascript"
	src="<?php echo get_assets_path('js');?>jcoockie.js"></script> <script
	language="javascript" type="text/javascript">

            Ext.onReady(function() {
                $("#simple-horizontal-menu").show();
                new Ext.ux.Menu('simple-horizontal-menu', {
                    transitionType: 'slide',
                    direction: 'horizontal', // default
                    delay: 0.2,              // default
                    autoWidth: true,         // default
                    transitionDuration: 0.3, // default
                    animate: true,           // default
                    currentClass: 'current'  // default
                });
                new Ext.ux.Menu('simple-vertical-menu', {
                    direction: 'vertical'
                });
             });
        </script> <script language="javascript" type="text/javascript">
             $(function(){
                  <?php if(isset($active_menu_js) && $active_menu_js != "") {
                            echo $active_menu_js;
                        }
                  ?>
             });
           </script> <script language="javascript"
	type="text/javascript">

            $(function(){
                hide_message();
                $(".date").datepicker();
                var options = {
                        utc: true,
                        utc_offset: 5.5
                    };
                //$('.clock').jclock(options);


                $("#simple-horizontal-menu").hide();
                $(".delete").click(function(){
                      var del_location = $(this).attr("href");
                      jConfirm('<label>Are you sure delete this record?</label>', 'Confirmation', function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location;
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                })

                $(".tooltip").parent("td").append("<div class='tip'></div>");

                $(".tooltip").parent("div").append("<div class='tip'></div>");
                $(".tooltip").hover(function(){
                    var tipVal = $(this).attr('tipVal');
                    $(this).siblings("div").html(tipVal);

                    $(this).siblings("div").show();

                }, function(){

                    $(this).siblings("div").hide();
                });

                 // Adding Colorbox Popup
                 //$(".popup").colorbox({width:"80%", height:"80%", iframe:true});

		$("#appleNav li").click(function(){
                    var link = $(this).index();
                    var lastSelectedLink = $.cookie('selectedMenu');
                    if(lastSelectedLink > -1) {
                       var obj = $("#appleNav li").get(lastSelectedLink);
                       $(obj).removeClass("liselected");
                    }

                    var newObj = $("#appleNav li").get(link);
                    //$(newObj).addClass("liselected");
                    $.cookie('selectedMenu', link, { expires : 2, path: '/' });
               })

                select_menu();
            })


	    function select_menu() {
                if($.cookie('selectedMenu') > -1) {
                  var l = $.cookie('selectedMenu');
                  var ob = $("#appleNav li").get(l);
                  $(ob).addClass("liselected");
                }
            }

            function tooltip(obj, str) {
                $(obj).parent(expr)
            }

            function hide_message() {
                setTimeout(function(){
                    $(".pagemsg").hide();
                }, 30000);

            }
        </script>

</head>
<body>
<div class="main-out">
<div class="main">
<div class="page">
<div class="top">
<div class="header">
<div class="header-top"><?php
if (isUserLoggedIn()) {$home_location = 'home';}else{$home_location='login';}
?>
<table cellpadding="2" cellspacing="2" width="100%">
	<tr>
		<td valign="middle" style="height: 60px;">
		<div style="float: left; margin-right: 10px;"><a
			href="<?php echo base_url().$home_location; ?>"><img
			src="<?php echo get_assets_path('image').'itcgry.png'; ?>" /></a></div>

		<div style="float: left; font-size: 16px;"><a
			href="<?php echo base_url().$home_location; ?>"
			style="color: #000000; text-decoration: none; text-shadow: 1px 0px 0, #FFFFFF 0 1px 0;">ITC
		Grand Chola</a></div>

		</td>
		<td width="190">
		<div style="color: #000">Digi<font color="red">Valet</font>
		Administration&nbsp;&nbsp;</div>
		<font color="#000000"><?php echo date('d-m-Y');?></font>&nbsp;
		<div class="clock" style="float: right; color: #fceca8;">&nbsp;</div>
		</td>
	</tr>
</table>
<!--<h1>Your <span>Company</span></h1>
<p>Call Us: 000 0000 000</p>--></div>
<?php if (isUserLoggedIn()) {?>
<div class="topmenu"><?php echo $menu; ?></div>
<?php }?></div>
<div class="content">
<div id="page_message"><?php if(isset($page_message)) {echo $page_message;}else {echo '<div class="pagemsg">&nbsp;</div>';}?>
</div>
<div class="content-left"><?php echo $content; ?></div>

</div>
</div>

</div>
</div>
</div>
</body>
</html>
