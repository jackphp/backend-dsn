<?php
error_reporting(E_ALL ^ E_NOTICE);
include BASE_PATH . '/application/views/main_pages/others/dashboard_page.php';
?>
<link
	href="<?php echo ASSETS_PATH; ?>js/tv_channel/jquery.alerts-1.1/jquery.alerts.css"
	rel="stylesheet" type="text/css" />
<script
	type="text/javascript"
	src="<?php echo ASSETS_PATH; ?>js/tv_channel/jquery.alerts-1.1/jquery.alerts.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('#Loading').hide();  
        $('#Loading_room').hide();  
    });
</script>
<script type="text/javascript">
    function getTokenValue(){
        $.ajax({
            url:"<?php echo base_url(); ?>digivalet_dashboard/gettoken",
            method:"POST",
            dataType:'HTML',
            success:function(data) {
                $("#token_val").html(data);
            },
            error:function(err) {
                alert(err);
            }
        });
    }
</script>
<!--
<script>
    function check_token_value(){
        var token = $("#token").val();
        if(token.length > 5){
            var dataSend = {token:token};   
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/check_token",
                method:"POST",
                data:dataSend,
                dataType:'HTML',                
                success:function(data) {
                    $('#Loading').hide();
                    $("#token_detail").html(data);
                    $('#token_detail').show();  
                    //                    $("#token_detail").fadeOut(6000);
                        
                },
                error:function(err) {
                    //                    alert(err);
                    $("#token").message(err, {
                        position: 'bottom',
                        append: false,
                        classes: ['red-gradient'],
                        arrow: 'top',
                        showCloseOnHover: false,
                        groupSimilar: false
                    });
                        
                }
            });
        }else{
            //jAlert('Token Length must be 6 charector');
            $("#token").message('Please enter valid token. It must be 6 charactor long.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
        }
            
    }
</script>-->
<script>
    function check_room_value(){
        //alert("check room value");
        var token = $("#token").val();
        var room_no = $("#room_no").val();
        if(token){
        if(room_no.length > 2){
            $('#Loading_room').show();
            var dataSend = {room_no:room_no};
            //alert(dataSend);
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/check_room",
                method:"POST",
                data:dataSend,
                dataType:'HTML',
                success:function(data) {
                    $('#Loading_room').hide();
                    if(data.length==8){
                        new_room_token();
                    }
                    else if(data.length==9){
                        openModal('This room number already assigned to other iPad'+'s. </br></br>Do you want to add new iPad for this room or you want to replace old iPad with new one.</br></br>Please click on the button as per the requirement.', {
                            'Add iPad': function(modal) { 
                                update_room();
                                /* Some custom code */ modal.closeModal(); },
                            'Replace iPad': function(modal) {
                                modal.closeModal(); 
                                update_allroom_token();
                                modal.closeModal(); 
                            }
                        });
                    }
                    else{
                        if ($("#room_no").text().length <= 0) {
                            $("#room_no").message('Please enter valid room number.', {
                                append: false,
                                classes: ['red-gradient'],
                                groupSimilar: false,
                                showCloseOnHover: false
                            });
                        }
                        $(".message").fadeOut(3000);
                        //                                        $("#room_detail").html(data);
                        //                                        $('#room_detail').show(); 
                        //                                        $("#room_detail").fadeOut(5000);

                    }  
                },
                error:function(err) {
                    alert(err);
                }
            });
        }else{
            //jAlert('Room No must be greater than 2 charector');
           
            $("#room_no").message('Please enter valid room number.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
            
        }}else{
            $("#token_no").message('Please select token number.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
        }
         
    }
    function  update_room(){
        var room_no = $("#room_no").val();
        var new_token = $("#token").val();
        if(new_token!=""){
            var dataSend = {
                token:new_token, room_no:room_no
            };
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/update_room",
                method:"POST",
                data:dataSend,
                dataType:'HTML',
                success:function(data) {
                    resetForms();
                    getTokenValue();
                    $("#sucess_msg").html("");
                    $("#sucess_msg").html(data);
                    $("#sucess_msg").show();
                    $("#sucess_msg").fadeOut(6000);
                       
                    //                         setTimeout(function(){
                    //                         location = ''
                    //                        },6000)
                },
                error:function(err) {
                    alert(err);
                }
            });
            
        }else{
            //            jAlert('Please insert token number');
            $("#token_no").message('Please select token number.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
        }
        
    }
    function  new_room_token(){
        var room_no = $("#room_no").val();
        var new_token = $("#token").val();
        
        if(new_token!=""){
            var dataSend = {
                token:new_token, room_no:room_no
            };
           
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/new_room_token",
                method:"POST",
                data:dataSend,
                dataType:'HTML',
                success:function(data) {
                    resetForms();
                    getTokenValue();
                    $("#sucess_msg").html("");
                    $("#sucess_msg").html(data);
                    $("#sucess_msg").show();
                    $("#sucess_msg").html(data);
                    $("#sucess_msg").fadeOut(6000);
                       
                    //                         setTimeout(function(){
                    //                        location = ''
                    //                        },6000)
                },
                error:function(err) {
                    alert(err);
                }
            });
            
        }else{
            //            jAlert('Please insert token number');
            $("#token_no").message('Please select token number.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
        }
        
    }
    function  update_allroom_token(){
   
        var token = $("#token").val();
        var room_no = $("#room_no").val();
        if(token){
            var dataSend = {
                token:token, room_no:room_no
            };
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/get_room_detail",
                type: "POST",
                data: dataSend,
                dataType:'HTML',
                success: function(data){
                    openModal(data, {
                        'Cancel': function(modal) {modal.closeModal(); }
                    });
                },
                error:function(){
                    alert(err);
                }   
            });
            
        }else{
            //            jAlert('Please insert token number');
            $("#token_no").message('Please select token number.', {
                append: false,
                classes: ['red-gradient'],
                //                    arrow: 'bottom',
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
        }
    }
    function resetForms() {
        for (i = 0; i < document.forms.length; i++) {
            document.forms[i].reset();
        }
        getTokenValue();
    }
    function realeseiPad(){
        
        openModal("<h4>IP Address</h4><input type='text' name='ipaddress' id='ipaddress'/>", {
                            'Release': function(modal) { 
                               get_ip_address();
                                /* Some custom code */// modal.closeModal();
                               }
                        });
    }
    function get_ip_address(){
    
        var ipAdd= $("#ipaddress").val();
        if(ipAdd){
            var dataSend = {
                ipaddress:ipAdd
            };
            $.ajax({
                url:"<?php echo base_url(); ?>digivalet_dashboard/delete_ipaddress",
                type: "POST",
                data: dataSend,
                dataType:'HTML',
                success: function(data){
                    $("#error").html("");
                    $("#error").html(data);
                    $("#error").show();
                    $("#error").fadeOut(4000);
                    $("#ipaddress").val('');
                },
                error:function(){
                    alert(err);
                }   
            });
            
        }else{
            $("#error").message('Please Enter IP Address.', {
                append: false,
                classes: ['red-gradient'],
                groupSimilar: false,
                showCloseOnHover: false
            });
            $(".message").fadeOut(3000);
        }
    }
</script>

<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<!-- Title bar -->
<title><?php print_r($page['module_name']); ?></title>

<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME; ?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>



<!-- Main content -->
<section role="main" id="main">
<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title"
	class="thin">
<h1><?php print_r($page['module_name']); ?></h1>
</hgroup>


<div class="with-padding" style="margin-top: 20px;">
<div id="sucess_msg"></div>
<form name="room_registration" method="post" id="room_registration"
	action="javascript:void(0);">
<fieldset class="fieldset fields-list"><!--	<legend class="legend"></legend>-->

<!-- Standard line -->
<div class="field-block button-height">
<div id="token_no"></div>
<label for="token" class="label" style="cursor: default;"><b>Token</b></label>
<!--<input type="text" name="token" id="token" value="" class="input" onblur="return check_token_value();" >-->
<div id="token_val"><select id="token" name="token" class="select">
<?php echo $token_list; ?>
</select></div>
<span
	style="float: left; margin-left: 160px; margin-top: -25px; cursor: pointer;"><img
	id="tokenList" src="/backend/assets/images/refresh.png" alt=""
	style="vertical-align: middle;" onclick="getTokenValue();" /></span> <span
	id="Loading" style="display: none;"><img
	src="/backend/assets/images/ajax.gif" alt=""
	style="vertical-align: middle;" /></span> <span id="img_confirm_token"
	style="display: none;"><img src="/backend/assets/images/yes.gif"
	alt="Token Found" style="vertical-align: middle;" /></span>

<div id="token_detail" style="text-align: center; width: 450px;"></div>

</div>
<div class="field-block button-height"><!--<small class="input-info">Info above input</small>-->
<label for="room_no" class="label" style="cursor: default;"><b>Room
Number</b></label> <input type="text" name="room_no" id="room_no"
	value="" class="input" style="margin-bottom: 10px;" maxlength="6"
	onkeypress="return isNumberKey(event)"> <span id="Loading_room"><img
	src="/backend/assets/images/ajax.gif" alt=""
	style="vertical-align: middle;" /></span> <!--<small class="input-info">Info below input</small>-->
<div id="room_detail" style="text-align: center; width: 202px;"></div>
</div>
<div class="field-block button-height">
<button class="button glossy mid-margin-right" type="button"
	onclick="return check_room_value();"><span class="button-icon"><span
	class="icon-tick"></span></span> Save</button>
<button class="button glossy mid-margin-right" type="button"
	onclick="realeseiPad();"><span class="button-icon"><span
	class="icon-tick"></span></span> Release iPad</button>
<button class="button glossy" id="tokenList" type="button"
	onclick="return resetForms();"><span class="button-icon red-gradient"
	id="tokenList"><span class="icon-cross-round" id="tokenList"></span></span>
Clear</button>

</div>
</fieldset>
</form>
</div>
</section>


<!-- Sidebar/drop-down menu -->

<!-- End sidebar/drop-down menu -->

<!-- JavaScript at the bottom for fast page loading -->

<!-- Scripts -->
<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_left_page.php'; ?>

<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_middle_rightbar.php'; ?>
<!-- Sidebar/drop-down menu -->


<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_footer.php'; ?>
<script>
    function openModal(content, buttons)
    {
        $.modal({
            title: 'Dashboard - Room Registration',
            content: content,
            buttons: buttons,
            beforeContent: '<div class="carbon" style="padding:10px;">',
            afterContent: '<div style="margin:10px;"></div><div id="error"></div></div>',
            buttonsAlign: 'center',
            resizable: false
        });
    }
    
</script>
<script language=Javascript>
     
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
     
</script>
</html>
</body>
