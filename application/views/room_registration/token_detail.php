<script>
     function updateFunction(room,token,oldtoken){
        
        var old_token=oldtoken;
        var room_no = room;
        var new_token = token;
       
        if(new_token!=""){
            var dataSend = {
                token:new_token, room_no:room_no,old_token:old_token
            };
                $.ajax({
                    url:"<?php echo base_url(); ?>digivalet_dashboard/update_room",
                    method:"POST",
                    data:dataSend,
                    dataType:'HTML',
                    success:function(data) {
                         resetForms();
                         getTokenValue();
                          $("#sucess_msg").html("");
                          $("#sucess_msg").show();
                          $("#sucess_msg").html(data);
                         // modal.closeModal(); 
                          $("#sucess_msg").fadeOut(6000);
                        location.reload(5000);
                    },
                    error:function(err) {
                        //alert(err);
                    }
                });
               
        }else{
            $("#token_no").message('Please select token number.', {
                            append: false,
                            classes: ['red-gradient'],
                            //                    arrow: 'bottom',
                            groupSimilar: false,
                            showCloseOnHover: false
            });
        }
    }
    </script>
<?php if (count($room_no) > 0) { ?>
<div style="width: 560px;">
<table class="table" style="width: 100%; margin-top: 20px; float: left">
	<thead>
		<tr>
			<th scope="col" style="height: 32px; padding-top: 10px;">Room No</th>
			<th scope="col" class="align-center">IP</th>
			<th scope="col" class="align-center">MAC Address</th>
			<th scope="col" class="align-center">Token</th>
			<th scope="col" class="align-center">Action</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	foreach ($rooms as $room) { ?>
		<tr>
			<td class="align-center" style="vertical-align: middle;"><?php echo  $room->room_no;?></td>
			<td class="align-center" style="vertical-align: middle;"><?php echo $room->pc_ip; ?></td>
			<td class="align-center" style="vertical-align: middle;"><?php echo $room->MAC ;?></td>
			<td class="align-center" style="vertical-align: middle;"><?php echo $room->token ;?></td>
			<td class="align-center"
				style="vertical-align: middle; padding: 10px;"><a href="#"
				class="button compact"
				onclick="updateFunction('<?php echo $room_no;?>','<?php echo $token;?>','<?php echo $room->token;?>')">Replace</a></td>
		</tr>
		<?php  } ?>

	</tbody>
</table>
</div>

		<?php } ?>


<script>
function resetForms() {
        for (i = 0; i < document.forms.length; i++) {
            document.forms[i].reset();
        }
        getTokenValue();
    }
 </script>
<script type="text/javascript">
function getTokenValue(){
  $.ajax({
                    url:"<?php echo base_url(); ?>digivalet_dashboard/gettoken",
                    method:"POST",
                    dataType:'HTML',
                    success:function(data) {
                        $("#token_val").html(data);
                    },
                    error:function(err) {
                        //alert(err);
                    }
                });
}
</script>
