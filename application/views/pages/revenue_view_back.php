<link
	href="<?php  echo get_assets_path('js'); ?>jquery.multiSelct/jquery.multiSelect.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery.multiSelct/jquery.multiSelect.js"></script>

<script language="javascript" type="text/javascript">
        
       $(function(){


                    
               
                    $("#search_val").autocomplete({ 
                        url: '<?php echo base_url(); ?>menuitem/search_in',

                        showResult: function(value, data) {
                                return '<span style="test">' + value + data +  '</span>';
                        },
                        onItemSelect: function(value) {
                            $("#hid_edit").attr('href', '<?php echo base_url().'menuitem/item/'; ?>'+value.data);
                            $("#hid_edit").click();
                            $("#search_val").val("");
                           //window.location.href = '<?php //echo base_url().'welcomletter/edit/'; ?>'+value.data;
                        },
                        maxItemsToShow: 20
                });
             
          
           


             $("#filter_cat").multiSelect({selectAll:false}, function(el){
                  var vals = '';
                  vals = $('#filter_cat').parent("th").children("div").children("label").children(":checkbox").map(function() {
                      if($(this).attr("checked")==true) {
                        return $(this).val();
                      }
                  }).get().join(',');
                 
                 $("#hid_filter_cat").val(vals);
                 get_subcategories(vals);
                 var subcat_ids = $("#hid_filter_subcat").val();
                 get_filterd_item(vals, subcat_ids)
                 $(".pager").hide();

             });
             
             $("#filter_subcat").multiSelect({selectAll:false}, function(el){
                 $("#filter_subcat").children("span").html("Subcategory");
                 var checked_val = $(el).val();
                 var hid_filter_opt = $("#hid_filter_cat").val();
                 var vals = '';
                 vals = $('#filter_subcat').parent("th").children("div").children("label").children(":checkbox").map(function() {
                     if($(this).attr("checked")==true) {
                        return $(this).val();
                     }
                  }).get().join(',');
                  
                 $("#hid_filter_subcat").val(vals);
                 var cat_ids = $("#hid_filter_cat").val();
                 get_filterd_item(cat_ids, vals);
                 $(".pager").hide();
             });


             $(".go_but").click(function() {
                alert('Yes');
             });

             setTimeout("setup_category()", 200);
             setTimeout("setup_subcategory()", 200);


             $("#update_room").click(function(){
                  jConfirm('<label>Are you sure to update rooms?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                          window.location.href='<?php echo base_url(); ?>menuitem/generate';
                      }
                  });
             })
        });


     function setup_category() {
         $("#filter_cat").children("span").html("Category");
         $('#filter_cat').parent("th").children("div").children("label").each(function(){
             var obj = $(this).children(":checkbox");
             var tmpObj = obj;
             $(tmpObj).replaceWith('<input type="radio" value="'+$(obj).val()+'" name="filter_cat" class="filterd_cat" text="'+$(this).text()+'" />');
         })

          $(".filterd_cat").click(function(){
             var vals = $(this).val();
	     $("#hid_filter_subcat").val(0);	
             $("#hid_filter_cat").val(vals);
             get_subcategories(vals);
             var subcat_ids = $("#hid_filter_subcat").val();
             $("#filter_cat").children("span").html($(this).attr("text"));
             $(".multiSelectOptions").attr("style", "position: absolute; z-index: 99999; visibility: hidden; width: 121px;");
             get_filterd_item(vals, subcat_ids)
             setTimeout("setup_subcategory()", 200);
             $(".pager").hide();
         })
     }

     function setup_subcategory() {
         $("#filter_subcat").children("span").html("Subcategory");
     }

     function get_subcategories(cat_ids) {
        var subcat_id = $("#hid_filter_subcat").val();
        $.ajax({
            url:'<?php echo base_url(); ?>menuitem/get_filter_subcat',
            type:'POST',
            data:'cat_ids='+cat_ids+'&subcat_ids='+subcat_id,
            success:function(msg){
                $('.listheading th').eq(4).html(msg);
                $("#filter_subcat").multiSelect({selectAll:false}, function(el){
                    var checked_val = $(el).val();
                    var hid_filter_opt = $("#hid_filter_cat").val();

                    var vals = '';
                    vals = $('#filter_subcat').parent("th").children("div").children("label").children(":checkbox").map(function() {
                         if($(this).attr("checked")==true) {
                            return $(this).val();
                         }
                     }).get().join(',');

                    $("#hid_filter_subcat").val(vals);
                    $("#hid_filter_subcat").val(vals);
                    var cat_ids = $("#hid_filter_cat").val();
                    get_filterd_item(cat_ids, vals);
                });
            }
        })
     }

     function get_filterd_item(cat_ids, subcat_ids) {
         $.ajax({
            url:'<?php echo base_url(); ?>menuitem/get_filter_menuitem',
            type:'POST',
            data:'cat_ids='+cat_ids+'&subcat_ids='+subcat_ids,
            success:function(msg){
                $("#table_body").html(msg);
            }
        })
     }

     function in_array(arr, val) {
        for(var i=0; i<arr.length; i++) {
            if (arr[i] == val) return true;
        }
     }


     function sea(){
        if($('#search').val()!=""){
      
        }
        else{
           
             jAlert("Please select the Search In!", "Error");
            return false;
        }
        
         if($('#search_val').val()!=""){
            
         
        }
        else{
           
             jAlert("Please enter the Search Value!", "Error");
            return false;
        }
        
        
            return true;       
            
     } 
     
     function chk(){
         return confirm('Do you want to delete Record');
     }
     
    function check(id,val){
//        alert(val);
        url = "<?php echo base_url()?>menuitem/update_status/"+id+'/'+val;
        $.get(url,function(data){
              window.location.reload();
        })
                       
        
    }
</script>
<style type="text/css">
th {
	text-align: left;
}
</style>

<input
	type="hidden" name="hid_filter_cat" id="hid_filter_cat" value="0" />
<input
	type="hidden" name="hid_filter_subcat" id="hid_filter_subcat" value="0" />

<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<?php }?>

<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 10px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>menuitem/item'">
<div tipval="Add New Menu Item" class="tooltip"><label>Add Menu Item</label></div>
</div>
<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 6px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>food_category'">
<div tipval="Category" class="tooltip"><label>Category</label></div>
</div>
<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 6px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>group_addons'">
<div tipval="Addons Families" class="tooltip"><label>Addons Families</label></div>
</div>
<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 6px; margin-bottom: 5px; margin-top: 5px;"
	id="update_room">
<div tipval="Update Rooms" class="tooltip"><label>Update Rooms</label></div>
</div>

<table cellpadding="2" cellspacing="2" width="100%" class="border_tbl"
	align="center">
	<?php if(isset($search)) {?>
	<tr>
		<td>
		<table width="100%" class="pagecontent border_tbl" bgcolor="d3d3d3"
			cellpadding="2" cellspacing="2">
			<?php if(isset($pager)){?>
			<td align="left" width="30%"><span class="pager"><?php echo $pager; ?></span></td>
			<?php }?>
			<td valign="middle" align="right"><?php echo $search['form_open'];?>
			<input type="text" name="search_val" id="search_val"
				autocomplete="off" placeholder="Search Menu item" /> <?php echo $search['form_close'];?>
			</td>
		</table>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td><?php if(isset($list)) echo $list;?></td>
	</tr>
	<?php if(isset($pager)){?>
	<tr>
		<td align="center" class="border_tbl" bgcolor="d3d3d3" height="30"><span
			class="pager"><?php echo $pager; ?></span></td>
	</tr>
	<?php }else {?>
	<tr>
		<td>
		<button type="button"
			onclick="window.location.href='<?php echo base_url(); ?>menuitem'"
			style="margin-left: 16px;" class="primary btn">
		<div><b>Back</b></div>
		</button>
		</td>
	</tr>
	<?php } ?>
</table>
<br />
<br />
