<!-- Main container Satrt -->

<script la language="javascript" type="text/javascript">
$(function(){
   // $("#smalli").text("i").toLocaleString()
})
</script>
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<?php }?>


<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 10px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>room_history/rms/hotel/'">
<div tipval="Reports" class="tooltip"><label>Reports</label></div>
</div>

<table
	cellpadding="2" cellspacing="2" width="80%" class="pagecontent"
	align="center">
	<!--    <tr><td class="pageheading">&nbsp;</td></tr>-->
	<tr>
		<td valign="top">
		<table cellpadding="3" cellspacing="0" class="border_tbl" width="480">
			<tr class="contentheading">
				<td>&nbsp;&nbsp;<?php echo $block1_heading; ?></td>
			</tr>
			<tr>
				<td height="250" valign="top"><!--<img src="<?php //echo get_assets_path('image');?>unit 1.png" alt="">-->
				<?php if($rented_report != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/FCF_Pie2D.swf", "chart1", $rented_report, "FactorySum", 335, 250);} else {echo "Data not found.!";}?>
				</td>
			</tr>
			<!--<tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" align="center" >
                            <tr>
                                <td style="font-size: 10px;"><?php if($dnd_count > 0) { echo "<a href='#'class='tooltip' tipVal='".$dnd_str."'>DND service on in <b>(".$dnd_count.")</b></a>";} ?></td>
                                <td style="font-size: 10px;"><?php if($make_my_room > 0) { echo "<a href='#' class='tooltip' tipVal='".$makeroom_str."'>Make my room service on in <b>(".$make_my_room.")</b></a>";} ?></td>
                                <td style="font-size: 10px;"><?php if($maintnance > 0) { echo "<a href='#' class='tooltip' tipVal='".$maintenance_str."'>Under maintenance <b>(".$maintnance.")</b></a>"; } ?></td>
                                <td style="font-size: 10px;"><?php if($digivalet_status > 0) { echo "<a href='#' class='tooltip' tipVal='".$digi_str."'>DigiValet Uninstalled in <b>(".$digivalet_status.")</b></a>"; }?></td>
                            </tr>
                        </table>
                    </td>
                </tr>-->
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		</td>
		<td valign="top" style="cursor: pointer;"
			onclick="window.location.href='<?php echo base_url().'ats/floor/1'; ?>'">
		<table cellpadding="0" cellspacing="0" class="border_tbl"
			style="width: 500px;">
			<tr class="contentheading">
				<td>&nbsp;&nbsp;<?php echo $block2_heading;?></td>
			</tr>
			<tr>
				<td valign="top" width="50%">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;">House keeping</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>Do Not Disturb:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: red; color: #FFFFFF; border: #000 1px solid;">06</td>
					</tr>
					<tr>
						<td><label>Makeup Room:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">14</td>
					</tr>
					<tr>
						<td><label>Tray Removal:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: gray; color: #FFFFFF; border: #000 1px solid;">03</td>
					</tr>
					<tr>
						<td><label>Butler Call:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #0B9CFD; color: #FFFFFF; border: #000 1px solid;">16</td>
					</tr>
				</table>
				</fieldset>

				<?php //echo $quick_room_view;?></td>
				<td valign="top">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;">Room Status</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>Rented Occupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: fuchsia; color: #FFFFFF; border: #000 1px solid;">136</td>
					</tr>
					<tr>
						<td><label>Rented Unoccupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: maroon; color: #FFFFFF; border: #000 1px solid;">37</td>
					</tr>
					<tr>
						<td><label>Unrented Occupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: blue; color: #FFFFFF; border: #000 1px solid;">25</td>
					</tr>
					<tr>
						<td><label>Unrented Unoccupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: orange; color: #000; border: #000 1px solid;">352</td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
			<tr>
				<td valign="top">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;">Rented Status</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>Rented:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">173</td>
					</tr>
					<tr>
						<td><label>Unrented:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: red; color: #FFFFFF; border: #000 1px solid;">377</td>
					</tr>
				</table>
				</fieldset>
				</td>
				<td valign="top">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;">Occupancy Status:</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>Occupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #04B4F4; color: #FFFFFF; border: #000 1px solid;">161</td>
					</tr>
					<tr>
						<td><label>Unoccupied:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #FC6C05; color: #FFFFFF; border: #000 1px solid;">389</td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
			<tr>
				<td valign="top">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;">DigiValet Status:</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>DigiValet Installed:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #041CF4; color: #FFFFFF; border: #000 1px solid;">550</td>
					</tr>
					<tr>
						<td><label>Under Maintenance:</label></td>
						<td width="60" height="20" align="center"
							style="color: #000; border: #000 1px solid;">03</td>
					</tr>
				</table>
				</fieldset>
				</td>
				<td valign="top">
				<fieldset
					style="border: #f2f2f2 1px ridge; margin: 0px 5px 0px 5px;"><legend
					style="font-size: 10px;"><span style="text-transform: lowercase;">i</span>Pad
				Status:</legend>
				<table cellpadding="2" cellspacing="2" width="100%">
					<tr>
						<td><label>iPad Battery < 20%:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #BF0603; color: #FFFFFF; border: #000 1px solid;">21</td>
					</tr>
					<tr>
						<td><label>iPad Battery < 60%:</label></td>
						<td width="60" height="20" align="center"
							style="background-color: #EFF704; color: #000; border: #000 1px solid;">47</td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		</td>

	</tr>
	<tr>
		<td style="width: 470px;">&nbsp;</td>
		<td style="width: 470px;">&nbsp;</td>
	</tr>

</table>
