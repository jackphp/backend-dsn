<script
	src="<?php echo get_assets_path('js'); ?>dashboard/inbox.scripts.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-ui-timepicker-addon.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/anytime.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/anytime.css" />
<script language="javascript" type="text/javascript">

 
$(function(){   
    
         
    
    
    
      if ($('#send_later').is(":checked")){  $('#value').val('1'); 
             //add();$('#value').val('1'); 
             $('#DateTimeDemo1').AnyTime_noPicker().AnyTime_picker({ earliest: new Date(),
         format: "%Y-%m-%d %H:%i"
        })
             $('#DateTimeDemo1').removeClass('disabled');
         }
    //alert('yes');
    countChar_onload();
       
       $('#DateTimeDemo').click(
      function(e) {
        $('#DateTimeDemo').AnyTime_noPicker().AnyTime_picker({ earliest: new Date(),
         format: "%Y-%m-%d %H:%i"
        } ).focus();
        e.preventDefault();
      } );

  
//     $('#DateTimeDemo1').click(
//      function(e) {
//        $('#DateTimeDemo1').AnyTime_noPicker().AnyTime_picker({ format: "%Y-%m-%d %H:%i"
//        }).focus();
//        e.preventDefault();
//      } );
    
      $('#save_send').click(function(){
          
           if($('#title').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter title.", "Error");
                   return false;

                 }  
            
            if($('#promotional_message').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter Promotional message.", "Error");
                   return false;

                 }
            if($('#extra_info').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter info.", "Error");
                   return false;

                 }  
 
             
             
             if($('#mid').val()==''){
                     if($('#filen').val()=='') {
                        jAlert("&nbsp;Please select the file to upload.", "Error");
                        return false;
                     } 
              }    
            
               var ext = $('#filen').val().split('.').pop().toLowerCase(); 
            if(ext !=''){ 
                    if(ext!='png' && ext!='jpg' && ext!='jpeg' ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid file extension.", "Error");
                   return false;
                    }  
             } 
            
            
             if($('#promo_page_url').val()=='') {
                        jAlert("&nbsp;Please enter the page url.", "Error");
                        return false;

              }
             var url=$('#promo_page_url').val();
               if(url.substr(0,7) == 'http://'){
                    //url = 'http://' + url;
                }
              else if(url.substr(0,8) == 'https://'){
                  
                  
              }  
              else{
                  url = 'http://' + url;
              }
              //alert(url);
             var pattern = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
                
            
             var bValidIP = pattern.test(url);
           // alert(bValidIP);
             if(url!=''){
                  
                if(bValidIP!=true){
                   jAlert('<label class="error">Page url is invalid.Please enter valid page url</label>', 'Error');
                   return false; 
                }
            }

            
            
            if ($('#send_later').is(":checked")){ 
                if($('#DateTimeDemo1').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select Start date.", "Error");
                   return false;

                 }
                
            }
           
           if($('#DateTimeDemo').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select End date.", "Error");
                   return false;

                 } 
            
             if($('#DateTimeDemo1').val()!='') {
                 if($('#DateTimeDemo1').val() > $('#DateTimeDemo').val() ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start time should be less than Expiry time.", "Error");
                   return false;
  
                 }
             }
            
            
             if($('#pdf').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select PDF type.", "Error");
              return false;

           }
          
       
             if($('#mid').val()!=''){
                uploadFile_index();
             }
            else{
                uploadFile();
            } 
           
        
       // alert($('#promo_page_url').val());
       // var file = document.getElementById('filen').files[0];
       // alert(file.name);
       // var file_data = $("#filen").prop("file")[0];   // Getting the properties of file from file field
        $.post( "<?php echo base_url()?>promo_message_lite/save_send", $("form").serialize(),
                          function( data ) { // alert(data);
                              window.location.href = '<?php echo base_url().'promo_message_lite'; ?>'
                                // window.location.href = '';
                          }
               );
       })
  
      
  
  
  
  
      $('#send_later').click(function(){ 
        
         if ($('#send_later').is(":checked")){ 
             add(); $('#value').val('1');   
             $('#DateTimeDemo1').removeClass('disabled');
         }
         
          else{ remove(); $('#value').val('0');   
                 $('#DateTimeDemo1').addClass('input disabled');
             
            //$('#DateTimeDemo1').AnyTime_noPicker().remove();
            //$('#DateTimeDemo1').removeClass('AnyTime-win AnyTime-pkr ui-widget ui-widget-content ui-corner-all'); 
            //$('#DateTimeDemo1').addClass('input disabled');
        } 
          //alert('es');
          //alert($("#send_later option:selected").val());
      })


        hide_message();
        $("#first").submit(function(){    
            
            if($('#title').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter title.", "Error");
                   return false;

                 }  
            
            if($('#promotional_message').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter Promotional message.", "Error");
                   return false;

                 }
             
             if($('#extra_info').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter info.", "Error");
                   return false;

                 }  

                 
             if($('#mid').val()==''){
                     if($('#filen').val()=='') {
                        jAlert("&nbsp;Please select the file to upload.", "Error");
                        return false;
                     } 
              }    
            
               var ext = $('#filen').val().split('.').pop().toLowerCase(); 
            if(ext !=''){ 
                    if(ext!='png' && ext!='jpg' && ext!='jpeg' ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid file extension.", "Error");
                   return false;
                    }  
             } 
            
            
             if($('#promo_page_url').val()=='') {
                        jAlert("&nbsp;Please enter the page url.", "Error");
                        return false;

              }
             var url=$('#promo_page_url').val();
              var url=$('#promo_page_url').val();
                if(url.substr(0,7) == 'http://'){
                    //url = 'http://' + url;
                }
              else if(url.substr(0,8) == 'https://'){
                  
                  
              }  
              else{
                  url = 'http://' + url;
              }
              //alert(url);
              //alert(url);
             var pattern = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
            
             var bValidIP = pattern.test(url);
           // alert(bValidIP);
             if(url!=''){
                if(bValidIP!=true){
                   jAlert('<label class="error">Page url is invalid.Please enter valid page url</label>', 'Error');
                   return false; 
                }
            }

            
            
            if ($('#send_later').is(":checked")){ 
                if($('#DateTimeDemo1').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select Start date.", "Error");
                   return false;

                 }
                
            }
           
           if($('#DateTimeDemo').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select End date.", "Error");
                   return false;

                 } 
            
             if($('#DateTimeDemo1').val()!='') {
                 if($('#DateTimeDemo1').val() > $('#DateTimeDemo').val() ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start time should be less than Expiry time.", "Error");
                   return false;
  
                 }
             }
            
            
             if($('#pdf').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select PDF type.", "Error");
              return false;

           }
       
                      
          
             
                
          
          
        });

        $("#commit_changes").click(function(){
                jConfirm('<label>Are you sure to update rooms?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                          //alert(del_location);
                          window.location.href='<?php echo base_url();?>generate_pdf/generate';
                      }
                  });
            })
});


  function fileSelected() {
            var file = document.getElementById('filen').files[0];
            if (file) {
               var fileSize = 0;
                if (file.size > 1024 * 1024)
                    fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
                else
                    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
                
                $('#fileName').val(file.name);
                $('#fileSize').val(fileSize);
                $('#fileType').val(file.type);
                
               // document.getElementById('fileName').innerHTML = 'Name: ' + fileType;
                //document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
               // document.getElementById('fileType').innerHTML = 'Type: ' + file.type;
            }
        }
        
     function uploadFile() {
            var fd = new FormData();
            fd.append("file", document.getElementById('filen').files[0]);
            var xhr = new XMLHttpRequest();
           // xhr.upload.addEventListener("progress", uploadProgress, false);
           // xhr.addEventListener("load", uploadComplete, false);
           // xhr.addEventListener("error", uploadFailed, false);
           // xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", "rr_upload");
            xhr.send(fd);
        }    

   function uploadFile_index() {
            var fd = new FormData();
            fd.append("file", document.getElementById('filen').files[0]);
            var xhr = new XMLHttpRequest();
           // xhr.upload.addEventListener("progress", uploadProgress, false);
           // xhr.addEventListener("load", uploadComplete, false);
           // xhr.addEventListener("error", uploadFailed, false);
           // xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", "/backend/index.php/promo_message_lite/rr_upload");
            xhr.send(fd);
        }  



   function remove(){
      $('#DateTimeDemo1').val('');
      // AnyTime.noPicker("field1");
      $('#DateTimeDemo1').AnyTime_noPicker();
   }

   function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }

 function add(){
             // alert('chg');   
            $('#DateTimeDemo1').AnyTime_noPicker().AnyTime_picker({ earliest: new Date(), format: "%Y-%m-%d %H:%i"
        }).focus(); 
       
 }

 function chk(){ alert($(this).val());
   alert('yes');
   //return true;
 }
 
 
 
 
   function countChar(val) {
        var len = val.value.length;
          $('#charNum').text(300 - len);
          if(len >= 300){
                jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Max 300 character limit is over.", "Error");
                return false;
              
          }
          
        
      }
    function countChar_onload() {
        
        var len = $('#promotional_message').val().length;
          $('#charNum').text(300 - len);
           if(len > 300){
                jAlert("&nbsp;&nbsp;&nbsp;&nbsp;Max 300 character limit is over.", "Error");
                return false;
              
          }
        
      }  
      
 
</script>
<style>
#DateTimeDemo {
	size: 36px !important;
}

#filen {
	width: 312px;
}
</style>
<title>Promotional Messages</title>


<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php //echo $page_heading; ?></div>
<div id="msg"
	style="margin-top: 20px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 30%; margin-bottom: 3%; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php } ?>
<br />
<br />
<br />
<!--<div style="margin-left:40px;margin-top: 10px;margin-bottom: 10px;"><button type="button" class="primary btn"   onclick="window.location.href = '<?php echo base_url().'video_song_category/'; ?>'"  ><div><b>Back</b></div></button></div>-->
<?php }?>
<?php echo $form_open; ?>


<fieldset class="border_tbl"
	style="border: #6E6E6E 1px solid; border-radius: 6px; width: 40%; margin-left: 0px; margin-right: 50px">
<legend style="font-size: 14px; font-weight: bold; margin-left: 15px;"><em>Promotional
Messages</em></legend>
<table align="center" style="margin-top: 10px;" width="600px"
	cellpadding="2" cellspacing="2" class="pageform">

	<tr>
		<td colspan="2" align="center" height="400">
		<table width="75%">
			<tr style="padding-top: 25px; height: 5px;">
				<td style="padding-top: 25px"><b>Title : </b></td>
				<td style="padding-top: 25px"><input type="text" name="title"
					size="36" value="<?php echo $title; ?>" id="title" class="input" /></td>
			</tr>
			<tr>
				<td style="padding-top: 35px"><label><b>Message : </b></label></td>
				<td style="padding-top: 30px"><textarea name="promotional_message"
					rows="5" cols="34" maxlength="300" id="promotional_message"
					onkeyup="countChar(this)" style="resize: none;" class="input"><?php echo $msg_body; ?></textarea></br>
				<span style="font-size: 11px; margin-left: 0px"><b>Note :</b><span
					id="charNum">300</span> characters are left.</span></td>
			</tr>
			<tr style="padding-top: 25px; height: 5px;">
				<td style="padding-top: 25px"><b>Extra : </b></td>
				<td style="padding-top: 25px"><input type="text" name="extra_info"
					size="36" value="<?php echo $extra_info; ?>" id="extra_info"
					class="input" /></td>
			</tr>
			<tr>
				<td style="padding-top: 25px"><label><b>Image :</b></label></td>
				<td style="padding-top: 5px" height="60">
				<div style="margin-top: 12px"><?php echo $file; ?></div>
				<div style="font-size: 11px; margin-left: 0px"><b>Note :</b> Image
				dimension should be 300 X 200.Image either be JPEG or PNG.</div>
				</td>
				<input type="hidden" value="" id="fileName" name="fileName" />

				<input type="hidden" value="" id="fileSize" name="fileSize" />

				<input type="hidden" value="" id="fileType" name="fileType" />

			</tr>

			<tr>
				<td style="padding-top: 25px"></td>
				<td style="padding-top: 5px" height="0"><?php if($image!=''){ ?> <span>Current
				uploaded-Image : <a id="image" target="_blank"
					href="<?php echo $promo_msg_img; ?>"><?php echo $image; ?> </a></span>
					<?php } ?></br>
				</br>
				<?php if($image!=''){ ?> <img src="<?php echo $promo_msg_img; ?>"
					width="60" height="60" /> <?php  } ?></td>

			</tr>
			<tr style="height: 45px;">
				<td style="padding-top: 10px"><b>Promotional page url : </b></td>
				<td style="padding-top: 10px"><input type="text"
					name="promo_page_url" size="36"
					value="<?php echo $promo_page_url; ?>" id="promo_page_url"
					class="input" /></td>
			</tr>
			<tr style="height: 55px;">
				<td>&nbsp;</td>
				<td style="vertical-align: middle;"><input type="checkbox"
					name="send_later" <?php echo $checked; ?> id="send_later" />&nbsp;<b>Send
				later / Scheduled this message for later time</b></td>
			</tr>
			<tr style="height: 45px;">
				<td><b>Start Date :</b></td>
				<td><input type="text" name="start_date" size="36"
					value="<?php echo $start_date; ?>" id="DateTimeDemo1"
					autocomplete="off" readonly="true" class="input disabled" /></td>
			</tr>
			<tr style="height: 45px;">
				<td><b>Expiry Date : </b></td>
				<td><input type="text" name="end_date" size="36"
					value="<?php echo $end_date; ?>" id="DateTimeDemo" class="input" />
				<div style="font-size: 11px; margin-left: 0px"><b>Note :</b>
				Promotional message will expire on this date & time.</div>
				</td>
			</tr>
			<input type="hidden" name="value" id="value" value="0" />


			<tr style="height: 45px;">
				<input type="hidden" value="<?php echo $mid; ?>" id="mid" name="mid" />
				<td colspan="2" align="center" style="padding-top: 15px;">
				<button id="" type="submit" style="margin-left: 26px;"
					class="button grey-gradient glossy">
				<div><b>Save as Draft</b></div>
				</button>
				<button id="" type="button" style="margin-left: 26px;"
					onclick="window.location.href = '<?php echo base_url().'promo_message_lite'; ?>'"
					class="button grey-gradient glossy">
				<div><b>Cancel</b></div>
				</button>
				<button id="save_send" type="button" style="margin-left: 26px;"
					class="button grey-gradient glossy">
				<div><b>Save & Send</b></div>
				</button>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
		</td>


	</tr>
</table>
</fieldset>





				<?php echo $form_close;  ?>


