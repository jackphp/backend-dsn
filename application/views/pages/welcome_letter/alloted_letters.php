<script language="javascript" type="text/javascript">
    $(document).ready(function() {   
         hide_message();

      function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }
   
  
    })



</script>
<title>Welcome letter</title>
<div id="msg"
	style="margin-top: 0px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 600px; border: 1px solid black; padding-top: 15px; color: green; font-size: 16px; height: 36px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php } ?>

<div>
<div align="left">

<div class="button grey-gradient glossy" align="left"
	style="margin: 30px 5px 5px 5px;" id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>welcomletter'">
<div title="Back to all letters" class="with-tooltip"><label>Back</label></div>
</div>
</br>
<div>
<table cellpadding="2" cellspacing="2" width="100%" class="table">
	<thead>
		<tr>
			<th align="center" width="10%">#</th>
			<th align="center" width="10%">Room No.</th>
			<th align="center" width="20%">Letter Code</th>
			<th align="center" width="20%">Guest Name</th>
			<th align="center" width="20%">Date</th>
			<th align="center" width="20%">Operations</th>
		</tr>
	</thead>

	<tbody>
	<?php if(is_array($rooms_message) && count($rooms_message) > 0) {
		$i=1;
		foreach($rooms_message as $row) {
			if($i%2==0) {$class="even";}else {$class="odd";}
			?>

		<tr class="<?php echo $class;?>">
			<td width="10%" align="center"><?php echo $i; ?></td>
			<td width="10%" align="center"><?php echo $row->room_no; ?></td>
			<td width="20%" align="center"><?php echo $row->letter_code; ?></td>
			<td width="20%"><?php echo $row->guest_name; ?></td>
			<td width="20%" align="center"><?php echo $row->created; ?></td>
			<td width="20%" align="center"><a
				href="<?php echo base_url().'welcomletter/send_letter/'.$row->room_no.'/'.$row->created; ?>"
				class="welcome_letter">Edit</a> | <a
				href="<?php echo base_url().'welcomletter/delete_letters/'.$row->room_no.'/'.$row->created; ?>"
				class="delete">Delete</a></td>
		</tr>
		<?php $i++;} } else { echo "<tr><td colspan='6' align='center'><label>Letters not available.</label></td></tr>"; }?>
	</tbody>
</table>
</div>
</div>