
<link
	href="<?php  echo get_assets_path('css'); ?>dashboard/reset.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>dashboard/colors.css?v=1">
<link
	href="<?php  echo get_assets_path('css'); ?>dashboard/style.armani.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/form.css?v=1">




<script
	src="<?php echo get_assets_path('js'); ?>dashboard/developr.input.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jquery-1.3.2.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jquery-ui-1.7.2.custom.min.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jqueryui/ui-lightness/jquery-ui-1.7.2.custom.css" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jHtmlArea-0.7.0.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jHtmlArea.css" />

<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/jquery-colorpicker.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/color-picker.css" />
<title>Welcome letter</title>
<script language="javascript" type="text/javascript">
    $(function(){
        
      var val = $("#edit-title").val();       
        $("#edit-title").val('');
        $("#edit-title").focus();
        $("#edit-title").val(val);
 
          
    $('#edit-title').blur(function() {
       //  alert($("#code").val());
         var code=$("#code").val();
          var string = $('#edit-title').val();
           var url1="<?php echo base_url();?>welcomletter/letter_code_check/"+string+"/"+code;
    
    
    
      
    $.ajax({
        url:url1,
        type:"POST",
        data:"name="+string,
        success:function(msg) {
            
             if(msg == "yes")
        {
//           jAlert("Channel Nmae already exist!", "Error"); 
       
             parent.jAlert("Letter Code already exist!", "Error"); 
        
         
            $('#edit-title').val('');
        
//          window.location.href="<?php echo base_url();?>favourite/channels";
          return false;
          //return true;
         
        }
          }
    });
    
    });



        $("#navbar-submit-button").click(function(){
            
//            if($("#edit-type").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select category.</div>');
//                $("#edit-type").focus();
//                return false;
//            }
//
//            if($("#edit-ctype").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select type.</div>');
//                $("#edit-ctype").focus();
//                return false;
//            }
//
//            if($("#edit-annexure").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select annexure.</div>');
//                $("#edit-annexure").focus();
//                return false;
//            }

            if($("#edit-title").val() == "") {
                parent.jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter letter code.", "Error"); 
                //$("#page_message").html('<div class="pagemsg error">Please enter letter code.</div>');
                $("#edit-title").focus();
                return false;
            }

            if($("#edit-name").val() == "") {
                parent.jAlert("Please enter name of letter.", "Error");
                //$("#page_message").html('<div class="pagemsg error">Please enter name of letter.</div>');
                $("#edit-name").focus();
                return false;
            }
            
            var clen = $('#edit-content').htmlarea('toHtmlString');
            if(clen == "" || clen=="<br>") {
                parent.jAlert("Please enter letter content.", "Error");
                //$("#page_message").html('<div class="pagemsg error">Please enter body content.</div>');
                $("#edit-content").focus();
                return false;
            }

            return true;

        })


        $("#edit-content").htmlarea({
                toolbar: [
                    ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                    ["increasefontsize", "decreasefontsize"],
                    ["orderedlist", "unorderedlist"],
                    ["justifyleft", "justifycenter", "justifyright"],
                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                    ["cut", "copy", "paste"]
                ]
                
        });
    })
</script>
<?php echo $form_open; ?>
<table cellpadding="2" cellspacing="2" width="100%" class="pageform"
	style="margin-top: 50px">
	<tr style="display: none;">
		<td colspan="2">
		<table cellpadding="0" cellspacing="2" width="95%">
			<tr>
				<td width="100"><label>Category:<span class="required"><b>*</b></span></label></td>
				<td><?php echo $types; ?></td>
				<td><label>Type:<span class="required"><b>*</b></span></label></td>
				<td><?php echo $type; ?></td>

				<td>
				<div style="float: left; margin-right: 5px;"><label>Annexure:<span
					class="required"><b>*</b></span></label></div>
				<div style="float: left; margin-right: 5px;"><?php echo $annexure; ?></div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td width="100"><label style="margin-left: 25px;">Letter Code:<span
			class="required"><b>*</b></span></label></td>
		<td style="margin-top: 30px"><?php echo $title; ?></td>
		<input type="hidden" id="code" value="<?php echo $code; ?>" />
	</tr>
	<tr style="display: none;">
		<td width="110"></td>
		<td>
		<table cellpadding="0" cellspacing="2" width="80%">
			<tr>
				<td>
				<div style="float: left; margin-right: 5px;"><label>Use Logo:</label>
				<?php echo $use_logo; ?></div>
				</td>
				<td><label>Align Logo:</label> <?php echo $logo_align; ?></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td width="110"><label style="margin-left: 25px;">Name:<span
			class="required"><b>*</b></span></label></td>
		<td><?php echo $name; ?></td>
	</tr>
	<tr>
		<td valign="top"><label style="margin-left: 25px;">Body:<span
			class="required"><b>*</b></span></label></td>
		<td><?php echo $content; ?> <!--<p>Note: For display guest name, use "%guest" token, it will autometically replaced with current check-in guest name's. <em>ie: Dear %guest</em></p>-->
		</td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo $form_id; echo $id; ?>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button type="submit" class="button grey-gradient glossy"
			id="navbar-submit-button">
		<div>Save</div>
		</button>
		</div>
		<div style="height: 32px; vertical-align: middle; float: left;"
			onclick="parent.$.fn.colorbox.close();">
		<button type="button" class="button grey-gradient glossy"
			id="navbar-cancel-button">
		<div>Cancel</div>
		</button>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
				<?php echo $form_close; ?>
