
<link
	href="<?php  echo get_assets_path('css'); ?>dashboard/reset.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>dashboard/colors.css?v=1">
<link
	href="<?php  echo get_assets_path('css'); ?>dashboard/style.armani.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/form.css?v=1">



<!--
-->
<script
	src="<?php echo get_assets_path('js'); ?>dashboard/developr.input.js"></script>
<!--<script type="text/javascript" src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jquery-1.3.2.min.js"></script>-->
<!--<script type="text/javascript" src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jquery-ui-1.7.2.custom.min.js"></script>-->
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jqueryui/ui-lightness/jquery-ui-1.7.2.custom.css" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jHtmlArea-0.7.0.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jHtmlArea.css" />

<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/jquery-colorpicker.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/color-picker.css" />
<title>Welcome letter</title>
<script language="javascript" type="text/javascript">
    $(function(){
        
      var val = $("#edit-title").val();       
        $("#edit-title").val('');
        $("#edit-title").focus();
        $("#edit-title").val(val);
 
          
    $('#edit-title').blur(function() {
       //  alert($("#code").val());
         var code=$("#code").val();
          var string = $('#edit-title').val();
           var url1="<?php echo base_url();?>welcomletter/letter_code_check/"+string+"/"+code;
    
    
    
      
    $.ajax({
        url:url1,
        type:"POST",
        data:"name="+string,
        success:function(msg) {
            
             if(msg == "yes")
        {
//           jAlert("Channel Nmae already exist!", "Error"); 
       
             parent.jAlert("Letter Code already exist!", "Error"); 
        
         
            $('#edit-title').val('');
        
//          window.location.href="<?php echo base_url();?>favourite/channels";
          return false;
          //return true;
         
        }
          }
    });
    
    });



        $("#navbar-submit-button").click(function(){
            
//            if($("#edit-type").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select category.</div>');
//                $("#edit-type").focus();
//                return false;
//            }
//
//            if($("#edit-ctype").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select type.</div>');
//                $("#edit-ctype").focus();
//                return false;
//            }
//
//            if($("#edit-annexure").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select annexure.</div>');
//                $("#edit-annexure").focus();
//                return false;
//            }

            if($.trim($("#edit-title").val()) == "") {
                jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter letter code.", "Error"); 
                //$("#page_message").html('<div class="pagemsg error">Please enter letter code.</div>');
                $("#edit-title").focus();
                return false;
            }

            if($.trim($("#edit-name").val()) == "") {
                jAlert("Please enter name of letter.", "Error");
                //$("#page_message").html('<div class="pagemsg error">Please enter name of letter.</div>');
                $("#edit-name").focus();
                return false;
            }
            
            var clen = $('#edit-content').htmlarea('toHtmlString');
            if(clen == "" || clen=="<br>") {
                jAlert("Please enter letter content.", "Error");
                //$("#page_message").html('<div class="pagemsg error">Please enter body content.</div>');
                $("#edit-content").focus();
                return false;
            }

            return true;

        })


        $("#edit-content").htmlarea({
                toolbar: [
                    ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                    ["increasefontsize", "decreasefontsize"],
                    ["orderedlist", "unorderedlist"],
                    ["justifyleft", "justifycenter", "justifyright"],
                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                    ["cut", "copy", "paste"]
                ]
                
        });
    })
</script>
<style>
.required {
	color: red
}

.jHtmlAreaColorPickerMenu {
	left: 658px !important;
	top: 316px !important;
	margin-top: -48px !important
}

iframe {
	border: 1px solid black;
}

.ToolBar {
	width: 100% !important
}
</style>
<?php echo $form_open; ?>
<fieldset class="fieldset fields-list"
	style="border: #6E6E6E 1px solid; height: auto; width: 60%; margin-top: 25px">
<legend style="font-size: 14px; font-weight: bold;" class="legend"><em>Welcome
Letter</em></legend>

<div>
<div style="width: 100%; height: 40px">
<div style="width: 20%; float: left;" align="left"><label
	style="margin-left: 25px;">Letter Code:<span class="required"><b>*</b></span></label>
</div>
<div style="width: 76%; float: right;" align="left"><?php echo $title; ?>
<input type="hidden" id="code" value="<?php echo $code; ?>" /></div>
</div>
<div style="height: 40px">
<div style="width: 20%; float: left;" align="left"><label
	style="margin-left: 25px; float: left">Name:<span class="required"><b>*</b></span></label>
</div>
<div style="width: 76%; float: right;" align="left"><?php echo $name; ?>
</div>
</div>
<div style="height: 40px">
<div style="width: 20%; float: left;" align="left"><label
	style="margin-left: 25px; float: left">Body:<span class="required"><b>*</b></span></label>
</div>
<div style="width: 76%; float: right;" align="left"><?php echo $content; ?>
</div>
</div>
<div align="center" style="width: 100%; clear: both; padding-top: 15px">
<?php echo $form_id; echo $id; ?>
<button type="submit" class="button grey-gradient glossy"
	id="navbar-submit-button">
<div>Save</div>
</button>


<button type="button" class="button grey-gradient glossy"
	onclick="window.location.href='<?php echo base_url(); ?>welcomletter'"
	id="navbar-cancel-button">
<div>Cancel</div>
</button>

</div>
</div>

<!--                    <table cellpadding="2" cellspacing="2" width="95%" class="pageform" style="margin-top:50px">

                        <tr>
                            <td width="20%" height="40px"></td> <td style="margin-top:30px"></td>
                           
                        </tr>

                        <tr>
                            <td width="20%" height="40px"></td> <td></td>
                        </tr>
                        <tr>
                            <td width="20%" valign="top" style="border:2px solid red;"></td>
                            <td style="border:2px solid blue;" width="70%">
                              
                                    
                                
                               
                             
                             <p>Note: For display guest name, use "%guest" token, it will autometically replaced with current check-in guest name's. <em>ie: Dear %guest</em></p>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="center" style="padding-top:20px;padding-left:20%">
                                <?php echo $form_id; echo $id; ?>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>--></fieldset>
<div style="width: 62%" align="left">Note: Fields marked with <span
	class="required">*</span> are mandatory</div>
                                <?php echo $form_close; ?>
