<script language="javascript" type="text/javascript">
    $(function(){
        $("#navbar-submit-button").click(function(){
            if($("#edit-name").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter category name.</div>');
                $("#edit-name").focus();
                return false;
            }
        })
    })
</script>
<?php echo $form_open; ?>
<table cellpadding="2" cellspacing="2" width="100%" class="pageform">
	<tr>
		<td><label>Category Name:</label></td>
		<td><?php echo $name; ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo $form_id; echo $id; ?>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button type="submit" class="primary btn" id="navbar-submit-button">
		<div>Save</div>
		</button>
		</div>
		<div style="height: 32px; vertical-align: middle; float: left;"
			onclick="parent.$.fn.colorbox.close();">
		<button type="button" class="primary btn" id="navbar-cancel-button">
		<div>Cancel</div>
		</button>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
<?php echo $form_close; ?>