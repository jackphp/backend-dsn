<script type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jquery-ui-1.7.2.custom.min.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jqueryui/ui-lightness/jquery-ui-1.7.2.custom.css" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jHtmlArea-0.7.0.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jHtmlArea.css" />

<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/jquery-colorpicker.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/color-picker.css" />


<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/autocomplete/jquery.autocomplete.js"></script>
<link
	href="<?php echo get_assets_path('js'); ?>tv_channel/autocomplete/jquery.autocomplete.css"
	type="text/css" rel="stylesheet" />
<title>Welcome letter</title>
<script language="javascript" type="text/javascript">
    $(function(){
        
          $("#letter_code").keyup(function(){
                   if($("#letter_code").val()=='')
                   { 
                    $(".acResults").hide();
                         }

                      });


         if($('#radio1').is(':checked')) { $("#gname").hide(); }
           if($('#radio2').is(':checked')) {  $("#gname").show(); }
        
        $('#radio1').click(function() { 
              $('#g_name').val("");
             if($('#radio1').is(':checked')) { $("#gname").hide(); }
         });
        
         $('#radio2').click(function() {
             if($('#radio2').is(':checked')) {  $("#gname").show(); }
         });
        
        
        
        $("#room_no").focus();
        $("#edit-date").datepicker({minDate: new Date()});
        $("#navbar-submit-button").click(function(){
            var msg_opt = $(".msg_opt:checked").val();

            //alert(msg_opt);

            if($("#letter_code").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter letter code.</div>');
                $("#letter_code").focus();
                return false;
            }

            


            if($("#room_no").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter Room.</div>');
                $("#room_no").focus();
                return false;
            }

            if($("#edit-date").val() == "")     {
                $("#page_message").html('<div class="pagemsg error">Please select date.</div>');
                $("#edit-date").focus();
                return false;
            }
            
//            if($("#gname").val() == "")     {
//                $("#page_message").html('<div class="pagemsg error">Please enter Guest Name.</div>');
//                $("#gname").focus();
//                return false;
//            }

            var clen = $('#edit-content').htmlarea('toHtmlString');
            if(clen == "" || clen=="<br>") {
                $("#page_message").html('<div class="pagemsg error">Please enter body content.</div>');
                $("#edit-content").focus();
                return false;
            }

            return true;

        })

         $("#letter_code").autocomplete({
                url: '<?php echo base_url(); ?>welcomletter/get_letter_code',
                
                showResult: function(value, data) {
			return '<span style="test">' + value + '</span>';
		},
		onItemSelect: function(value) {
                   // alert(value.data);
                   $("#letter_code").val(value.data);
                   var path, param;

                   path = 'welcomletter/get_letter_name';
                   param = 'code='+value.data;

                   ajax_all(path, param, $("#letter_name"), 1);
                   get_letter_content(value.data);
		},
		maxItemsToShow: 15
	});

         $("#room_no").autocomplete({
                url: '<?php echo base_url(); ?>welcomletter/get_room_no',

                showResult: function(value, data) {
			return '<span style="test">' + value + '</span>';
		},
		onItemSelect: function(value) {
                    $("#room_no").val(value.data);
		},
		maxItemsToShow: 15
	});

        $(".msg_opt").click(function(){
            if($(this).val() == 'new') {
                $("#letters-category-contener").hide();
                $("#letters-contener").hide();
            }
            else {
                $("#letters-category-contener").show();
                $("#letters-contener").show();
            }
        })

        $("#edit-content").htmlarea({
                toolbar: [
                    ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                    ["increasefontsize", "decreasefontsize"],
                    ["orderedlist", "unorderedlist"],
                    ["justifyleft", "justifycenter", "justifyright"],
                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                    ["cut", "copy", "paste"]
                ]

       });
    })

      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

    function ajax_all(path, param, res_obj, isVal) {
        
        $.ajax({
            url:'<?php echo base_url(); ?>'+path,
            type:'POST',
            data:param,
            success:function(msg){
                if(isVal == 1) {
                    $(res_obj).val(msg);
                }
                else {
                    $(res_obj).html(msg);
                    $("#edit-content").htmlarea({
                    toolbar: [
                        ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                        ["increasefontsize", "decreasefontsize"],
                        ["orderedlist", "unorderedlist"],
                        ["justifyleft", "justifycenter", "justifyright"],
                        ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                        ["cut", "copy", "paste"]
                    ]

                    });
                }
            }
        })
    }

    function get_letter_content(code) {// alert(code);
        if(code != "") {
            ajax_all('welcomletter/get_letter_content/'+code, 'lid='+code, $('#body_container'), 0);
        }
    }

    function show_preview() {
        var html = $('#edit-content').htmlarea('toHtmlString');
        $("#privew_container").html(html);
        $(".preview").show();
    }

    function get_letters() {
        var cat = $("#edit-type").val();
        var type = $("#edit-ctype").val();
        var annaxture = $("#edit-annexure").val();
        var param = 'cat='+cat+'&type='+type+'&annaxture='+annaxture;

        ajax_all('welcomletter/get_letters', param, $("#letters_title"));
    }

</script>

<style>
.pageform input {
	border: medium none;
	border-radius: 3px 0 0 3px;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
	font: 14px Verdana, Arial, sans-serif;
	height: 28px;
	padding: 5px 8px;
}

.border_tbl {
	border: #E8E8E8 1px solid;
	font-size: 12px;
	-moz-border-radius: 4px /*{cornerRadius}*/;
	-webkit-border-radius: 4px /*{cornerRadius}*/;
	border-radius: 4px /*{cornerRadius}*/;
}

.popup {
	background: none repeat scroll 0 0 #FFFFFF;
	border-radius: 10px 10px 10px 10px;
	box-shadow: 0 2px 10px rgba(0, 0, 0, 0.5);
	display: none;
	padding: 0;
	position: absolute;
	z-index: 200;
}

.border_tbl {
	border: 1px solid #E8E8E8;
	border-radius: 4px 4px 4px 4px;
	font-size: 12px;
}

#tbl tr td {
	height: 40
}

#tbl label {
	font-size: 14px
}

.jHtmlArea {
	width: 650px !important
}

.jHtmlArea .toolbar {
	width: 640px !important
}

.jHtmlArea iframe {
	width: 630px !important
}
</style>

<div class="popup preview">
<div id="close"
	style="float: right; margin-right: 10px; margin-top: 5px;"><a href="#"
	onclick="$('.preview').hide()"> Close </a></div>
<div style="line-height: 0.2">&nbsp;</div>
<div id="preview_text"
	style="width: 640px; height: 480px; overflow: auto;">
<table cellpadding="2" cellspacing="2" width="70%" align="center">
	<tr>
		<td align="right" colspan="2"><img
			src="<?php echo get_assets_path('image');?>backend_images/ITC grand chola.jpg"
			style="width: 116px; height: 135px;" /></td>
	</tr>
	<tr>
		<td colspan="2">
		<div id="privew_container"></div>
		</td>
	</tr>
	<tr>
		<td align="center" width="75"><img
			src="<?php echo get_assets_path('image');?>backend_images/ITC 100.jpg"
			style="width: 48px; height: 61px;" />
		<div>
		<center><label>Inspiring Years</label></center>
		</div>
		</td>
		<td valign="bottom"># 63, Mount Road, Guindy, Chennai 600 032, India <em>tel</em>
		+91 44 2220 0000 <em>fax</em> +91 44 2220 0200 Registered Office: ITC
		Limited, Verginiaa House, 37 J. L. Nehru Road, Kolkata 700 071, India
		</td>
	</tr>
	<tr>
		<td colspan="2"><em>ITC Grand Chola, Chennai - a Luxury Collection
		hotel is independently owned and operated by ITC Limited and operated
		under lincense from Sheraton International Inc. </em></td>
	</tr>
</table>
</div>
</div>

<?php echo $form_open; ?>

<table cellpadding="1" cellspacing="2" width="1050px" id="tbl"
	align="center">
	<tbody>
		<tr>
			<td><br />
			<br />
			<br />
			<div class="button grey-gradient glossy" id="send_letter"
				onclick="window.location.href='<?php echo base_url(); ?>welcomletter'">
			<div title="Back to all letters" class="with-tooltip"><label>Back</label></div>
			</div>
			<br />
			<br />
			</td>
			<td></td>

		</tr>

		<tr>
			<td style="padding: 90px; border: 1px solid #AAAAAA">
			<table cellpadding="1" cellspacing="2" width="900px" id="tbl1"
				align="center">
				<tr>
					<td class="inline-label"><label for="input" class="label">Room No.:<span
						class="required"><b>*</b></span></label> <?php echo $room_no; ?></td>
					<td style="margin-left: 10px" class="inline-label"><label
						for="input" class="label">Date:</label> <?php echo $date; ?></td>
				</tr>
				<tr>
					<td class="inline-label"><label for="input" class="label">Letter
					Code:<span class="required"><b>*</b></span></label> <?php echo $letter_code; ?>
					</td>
					<td style="margin-left: 10px" class="inline-label"><label
						class="label">Name of Letter:</label><?php echo $letter_name; ?></td>
				</tr>
				<!--<tr id="letters-category-contener">
        <td><label>Search In</label></td>
        <td>
            
            <table cellpadding="0" cellspacing="2" width="100%">
                <tr>
                    <td><?php// echo $types; ?></td>
                    <td></td> <td><?php// echo $type; ?></td>

                    <td><div style="float: left; margin-right: 5px;"></div><div style="float: left; margin-right: 5px;"> <?php echo $annexure; ?></div></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id="letters-contener">
        <td width="100"><label>&nbsp;Letters:</label></td> <td id="letters_title"><?php echo $titles; ?></td>
    </tr>-->

				<tr>
					<td colspan="2" class="inline-label"><label class="label">Guest
					Name:</label> <!--        <td style=""><span style=" vertical-align:bottom" ><?php echo $option; ?></span><span>Pick from Opera</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $option1; ?> <label>Specify here</label></td>-->
					<?php echo $option; ?>&nbsp;&nbsp;<label for="radio1">Pick from
					Opera</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $option1; ?>&nbsp;&nbsp;<label
						for="radio2">Specify here</label></td>
				</tr>
				<tr id="gname">
					<td colspan="2" class="inline-label"><label class="label"></label>
					<?php echo $salutation; ?> <?php echo $gname; ?></td>
				</tr>
				<tr>
					<td class="inline-label" colspan="2"><label class="label">&nbsp;Body:<span
						class="required"><b>*</b></span></label>
					<div id="body_container"><?php echo $content; ?></div>
					</td>
				</tr>

				<tr>

					<td align="center" colspan="2" style="margin-top: 10px"><?php echo $form_id.$id; ?>
					<div
						style="height: 32px; vertical-align: middle; float: left; margin-right: 10px; margin-left: 370; margin-top: 10px">
					<button type="submit" class="button grey-gradient glossy"
						id="navbar-submit-button">
					<div>Send</div>
					</button>
					</div>
					<!--<div style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;" onclick="show_preview()">
                <button type="button" class="primary btn" id="navbar-cancel-button"><div>Preview</div></button>
            </div>-->
					<div align="left"
						style="height: 32px; vertical-align: middle; margin-left: 350px; margin-top: 10px"
						onclick="window.location.href='<?php echo base_url().$cancel_location;?>'">
					<button type="button" class="button grey-gradient glossy"
						id="navbar-cancel-button">
					<div>Cancel</div>
					</button>
					</div>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</tbody>
</table>
<style>
iframe {
	background: #FFFFFF;
	border: 1px solid !important;
	display: inline-table !important;
}

#main {
	z-index: 0 !important;
}
</style>
					<?php echo $form_close; ?>
