<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<script
	src="<?php echo get_assets_path('js'); ?>dashboard/inbox.scripts.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-ui-timepicker-addon.js"></script>
<script
	src="<?php echo get_assets_path('js');?>dashboard/anytime.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/anytime.css" />

<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>digital_datepicker/mobiscroll1.custom-2.5.1.min.css" />
<script
	src="<?php echo get_assets_path('js');?>digital_datepicker/mobiscroll.custom-2.5.1.min.js"></script>
<script
	src="<?php echo get_assets_path('js');?>moment.js"></script>
<script language="javascript" type="text/javascript">

        
 
$(function(){    
     
           var today = moment(new Date());
              today=today.format("YYYY-MM-DD HH:mm");
          if($('#DateTimeDemo1').val()!=''){
               if($('#DateTimeDemo1').val() < today){
                   $('#DateTimeDemo1').attr('disabled','disabled');
                   $('#send_later').attr('disabled','disabled');
               }
          }
           $('#title').focus();
     
            var now = new Date();

            $('#demo').mobiscroll().datetime({
                minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                theme: 'ios',
                display: 'bubble',
                mode: 'scroller',
                timeFormat : 'HH:ii',
                dateFormat :'yy-mm-dd'
                
            });    
            $('#show').click(function(){
                $('#demo').mobiscroll('show'); 
                return false;
            });
            $('#clear').click(function () {
                $('#demo').val('');
                return false;
            });


    
    
    
    
    
       $('#preview_button').attr('disabled','disabled')
       if($('#promo_page_url').val()!=''){ 
               $('#preview_button').removeAttr('disabled');
           }
       // $('#navbar-query').keyup(function() {
       $("#promo_page_url").keyup(function(){
           if($('#promo_page_url').val()!=''){
               $('#preview_button').removeAttr('disabled');
              // window.location.href = $('#promo_page_url').val(); 
           }
           else{
               $('#preview_button').attr('disabled','disabled');
           }
           //alert('dd');
         //  window.location.href = val;
       })
     
//       $('#preview_button').click(function(){
//           
//           window.location.href=$('#promo_page_url').val();
//           
//       })
     
       $(".preview").click(function(){
           
           
             
            var elementURL= $('#promo_page_url').val();
            
                if(elementURL.substr(0,7) == 'http://'){
                    //url = 'http://' + url;
                }
              else if(elementURL.substr(0,8) == 'https://'){
                  
                  
              }  
              else{
                  elementURL = 'http://' + elementURL;
              }
              //alert(url);
              //alert(url);
             var pattern = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
            
             var bValidIP = pattern.test(elementURL);
           // alert(bValidIP);
             if(elementURL!=''){
                if(bValidIP!=true){
                   jAlert('<label class="error">Promotion url is invalid, please enter valid url</label>', 'Error');
                   return false; 
                }
               else{
                   $(".preview").colorbox({width:"1024",href:elementURL, height:"768", iframe:true, overlayClose:false});
               } 
            }
           else{
                jAlert('<label class="error">Please enter Promotion url.</label>', 'Error');
           } 
            //alert(elementURL);
            
            //$(".welcome_letter").click();

        })        
    
    
    
      if ($('#send_later').is(":checked")){  $('#value').val('1'); 
             //add();$('#value').val('1'); 
             $('#DateTimeDemo1').mobiscroll().datetime({
                minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                 theme: 'ios',
                display: 'bubble',
                mode: 'scroller',
                timeFormat : 'HH:ii',
                dateFormat :'yy-mm-dd'
            }); 
             $('#DateTimeDemo1').removeClass('disabled');
         }
    //alert('yes');
    countChar_onload();
       
       $('#DateTimeDemo').click(
      function(e) {
        $('#DateTimeDemo').AnyTime_noPicker().AnyTime_picker({ earliest: new Date(),
         format: "%Y-%m-%d %H:%i"
        } ).focus();
        e.preventDefault();
      } );

  
//     $('#DateTimeDemo1').click(
//      function(e) {
//        $('#DateTimeDemo1').AnyTime_noPicker().AnyTime_picker({ format: "%Y-%m-%d %H:%i"
//        }).focus();
//        e.preventDefault();
//      } );
    
      $('#save_send').click(function(){
          var today = moment(new Date());
              today=today.format("YYYY-MM-DD HH:mm");//alert(today);
              //alert($('#demo').val());alert('today'+today);
             
              
              
           if($('#title').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter title.", "Error");
                   return false;

                 }  
            
            if($('#promotional_message').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter description of Promotional message.", "Error");
                   return false;

                 }
            if($('#extra_info').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter additional information.", "Error");
                   return false;

                 }  
 
             
             
             if($('#mid').val()==''){
                     if($('#filen').val()=='') {
                        jAlert("&nbsp;Please select the thumbnail image to upload.", "Error");
                        return false;
                     } 
              }    
              
            
            
               var ext = $('#filen').val().split('.').pop().toLowerCase(); 
            if(ext !=''){ 
                    if(ext!='png' && ext!='jpg' && ext!='jpeg' ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid file extension.", "Error");
                   return false;
                    }  
             } 
            
            
             if($('#promo_page_url').val()=='') {
                        jAlert("&nbsp;Please enter the promotion url.", "Error");
                        return false;

              }
             var url=$('#promo_page_url').val();
               if(url.substr(0,7) == 'http://'){
                    //url = 'http://' + url;
                }
              else if(url.substr(0,8) == 'https://'){
                  
                  
              }  
              else{
                  url = 'http://' + url;
              }
              //alert(url);
             var pattern = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
                
            
             var bValidIP = pattern.test(url);
           // alert(bValidIP);
             if(url!=''){
                  
                if(bValidIP!=true){
                   jAlert('<label class="error">Promotion url is invalid.Please enter valid promotion url</label>', 'Error');
                   return false; 
                }
            }

            
            
            if ($('#send_later').is(":checked")){ 
                if($('#DateTimeDemo1').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select Start date.", "Error");
                   return false;

                 }
                
            }
           
           if($('#demo').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select Expiry date.", "Error");
                   return false;

                 } 
                 
             if($('#demo').val()!=''){ 
                if($('#demo').val() < today){// alert($('#demo').val());alert(today);
                     jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expiry date time should be greater than current time.", "Error");
                       return false;

                }      
             }   
                 
            
             if($('#DateTimeDemo1').val()!='') {
                 if($('#DateTimeDemo1').val() > $('#demo').val() ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start time should be less than Expiry time.", "Error");
                   return false;
  
                 }
             }
            
            
             if($('#pdf').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select PDF type.", "Error");
              return false;

           }
          
       
             if($('#mid').val()!=''){
                uploadFile_index();
             }
            else{
                uploadFile();
            } 
           
          if($('#DateTimeDemo1').val()=='') {  
           
                 jConfirm('<label>Do you want to publish Promotions ?</label>', 'Confirmation', function(r) {
                              if(r == true) {
                                         $.post( "<?php echo base_url()?>promo_message_lite/save_send", $("form").serialize(),
                                                      function( data ) { // alert(data);
                                                          window.location.href = '<?php echo base_url().'promo_message_lite'; ?>'
                                                            // window.location.href = '';
                                                      }
                                           );

                              }
                             else{
                                 return false;
                             } 
                          });  
                          return false;
          }     
        
       // alert($('#promo_page_url').val());
       // var file = document.getElementById('filen').files[0];
       // alert(file.name);
       // var file_data = $("#filen").prop("file")[0];   // Getting the properties of file from file field
        $.post( "<?php echo base_url()?>promo_message_lite/save_send", $("form").serialize(),
                          function( data ) { // alert(data);
                              window.location.href = '<?php echo base_url().'promo_message_lite'; ?>'
                                // window.location.href = '';
                          }
               );
       })
  
      
  
  
  
  
      $('#send_later').click(function(){ 
        
         if ($('#send_later').is(":checked")){  $('#DateTimeDemo1').removeClass('disabled');
             add(); $('#value').val('1');   $('#DateTimeDemo1').focus();
             
         }
         
          else{ remove();$('#DateTimeDemo1').removeClass('i-txt');$('#DateTimeDemo1').removeClass('dwtd');   $('#value').val('0');   
                 $('#DateTimeDemo1').addClass('input disabled');
                 $('#DateTimeDemo1').unbind('.dw').removeData('scroller');
                 
                 
             
            //$('#DateTimeDemo1').AnyTime_noPicker().remove();
            //$('#DateTimeDemo1').removeClass('AnyTime-win AnyTime-pkr ui-widget ui-widget-content ui-corner-all'); 
            //$('#DateTimeDemo1').addClass('input disabled');
        } 
          //alert('es');
          //alert($("#send_later option:selected").val());
      })


        hide_message(); 
        $("#first").submit(function(){   
            
            
            
            if($('#title').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter title.", "Error");
                   return false;

                 }  
            
            if($('#promotional_message').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter Promotional message.", "Error");
                   return false;

                 }
             
             if($('#extra_info').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter info.", "Error");
                   return false;

                 }  

          
             var file= $('#filen').val();
                 file = file.replace(/\s+/g, '');
                 $('#filen').val()=file; 
                  //alert($('#filen').val());
                  
              
             if($('#mid').val()==''){
                     if($('#filen').val()=='') {
                        jAlert("&nbsp;Please select the file to upload.", "Error");
                        return false;
                     } 
              }    
            
               var ext = $('#filen').val().split('.').pop().toLowerCase(); 
            if(ext !=''){ 
                    if(ext!='png' && ext!='jpg' && ext!='jpeg' ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid file extension.", "Error");
                   return false;
                    }  
             } 
            
            
             if($('#promo_page_url').val()=='') {
                        jAlert("&nbsp;Please enter the promotion url.", "Error");
                        return false;

              }
             // var url=$('#promo_page_url').val();
              var url=$('#promo_page_url').val();
                if(url.substr(0,7) == 'http://'){
                    //url = 'http://' + url;
                }
              else if(url.substr(0,8) == 'https://'){
                  
                  
              }  
              else{
                  url = 'http://' + url;
              }
              //alert(url);
              //alert(url);
             var pattern = new RegExp(
            "^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
            
             var bValidIP = pattern.test(url);
           // alert(bValidIP);
             if(url!=''){
                if(bValidIP!=true){
                   jAlert('<label class="error">Page url is invalid.Please enter valid page url</label>', 'Error');
                   return false; 
                }
            }

            
            
           
           
           if($('#DateTimeDemo').val()=='') {
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select Expiry date.", "Error");
                   return false;

                 } 
                 
              
            
             if($('#DateTimeDemo1').val()!='') {
                 if($('#DateTimeDemo1').val() > $('#DateTimeDemo').val() ){
                   jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start time should be less than Expiry time.", "Error");
                   return false;
  
                 }
             }
          
          jConfirm('<label>Do you want to publish Promotions ?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                          
                        
                      }
                     else{
                         return false;
                     } 
                  });
          
          
          
          
        });

        $("#commit_changes").click(function(){
                jConfirm('<label>Are you sure to update rooms?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                          //alert(del_location);
                          window.location.href='<?php echo base_url();?>generate_pdf/generate';
                      }
                  });
            })
});


  function fileSelected() {
            var file = document.getElementById('filen').files[0];
            if (file) {
               var fileSize = 0;
                if (file.size > 1024 * 1024)
                    fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
                else
                    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
                
                $('#fileName').val(file.name);
                $('#fileSize').val(fileSize);
                $('#fileType').val(file.type);
                
               // document.getElementById('fileName').innerHTML = 'Name: ' + fileType;
                //document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
               // document.getElementById('fileType').innerHTML = 'Type: ' + file.type;
            }
        }
        
     function uploadFile() {
            var fd = new FormData();
            fd.append("file", document.getElementById('filen').files[0]);
            var xhr = new XMLHttpRequest();
           // xhr.upload.addEventListener("progress", uploadProgress, false);
           // xhr.addEventListener("load", uploadComplete, false);
           // xhr.addEventListener("error", uploadFailed, false);
           // xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", "rr_upload");
            xhr.send(fd);
        }    

   function uploadFile_index() {
            var fd = new FormData();
            fd.append("file", document.getElementById('filen').files[0]);
            var xhr = new XMLHttpRequest();
           // xhr.upload.addEventListener("progress", uploadProgress, false);
           // xhr.addEventListener("load", uploadComplete, false);
           // xhr.addEventListener("error", uploadFailed, false);
           // xhr.addEventListener("abort", uploadCanceled, false);
            xhr.open("POST", "/backend/index.php/promo_message_lite/rr_upload");
            xhr.send(fd);
        }  



   function remove(){
      $('#DateTimeDemo1').val('');
      // AnyTime.noPicker("field1");
     // $('#DateTimeDemo1').AnyTime_noPicker();
   }

   function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }

 function add(){
             // alert('chg');  
             var now = new Date();

           $('#DateTimeDemo1').mobiscroll().datetime({
                minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                theme: 'ios',
                display: 'bubble',
                mode: 'scroller',
                timeFormat : 'HH:ii',
                dateFormat :'yy-mm-dd'
            });  
       
 }

 function chk(){ alert($(this).val());
   alert('yes');
   //return true;
 }
 
 
 
 
   function countChar(val) {
        var len = val.value.length;
          $('#charNum').text(300 - len);
          if(len >= 300){
                jAlert("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Max 300 character limit is over.", "Error");
                return false;
              
          }
          
        
      }
    function countChar_onload() {
        
        var len = $('#promotional_message').val().length;
          $('#charNum').text(300 - len);
           if(len > 300){
                jAlert("&nbsp;&nbsp;&nbsp;&nbsp;Max 300 character limit is over.", "Error");
                return false;
              
          }
        
      }  
      
 
</script>
<style>
iframe {
	background-color: white;
}

#DateTimeDemo {
	size: 36px !important;
}

#filen {
	width: 312px;
}

.required {
	color: red
}
</style>
<title>Promotional Messages</title>


<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php //echo $page_heading; ?></div>
<div id="msg"
	style="margin-top: 20px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 30%; margin-bottom: 3%; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php } ?>
<br />
<br />
<!--<div style="margin-left:40px;margin-top: 10px;margin-bottom: 10px;"><button type="button" class="primary btn"   onclick="window.location.href = '<?php echo base_url().'video_song_category/'; ?>'"  ><div><b>Back</b></div></button></div>-->
<?php }?>
<?php echo $form_open; ?>


<fieldset class="border_tbl"
	style="border: #6E6E6E 1px solid; border-radius: 6px; width: 55%; margin-left: 0px; margin-right: 50px">
<legend style="font-size: 14px; font-weight: bold; margin-left: 15px;"><em>Promotions</em></legend>
<table align="center" style="margin-top: 10px;" width="100%"
	cellpadding="2" cellspacing="2" class="pageform">

	<tr>
		<td colspan="2" align="center" height="400">
		<table width="70%" style="margin-top: 30px;">

			<tr style="height: 50px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><b>Title
				: </b></td>
				<td style="vertical-align: top;"><input type="text" name="title"
					size="36" value="<?php echo $title; ?>" id="title" class="input" /></td>
			</tr>
			<tr style="height: 126px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><label><b>Description
				: </b></label></td>
				<td style="vertical-align: top;"><textarea
					name="promotional_message" rows="5" cols="35" maxlength="300"
					id="promotional_message" onkeyup="countChar(this)"
					style="resize: none;" class="input"><?php echo $msg_body; ?></textarea></br>
				<span style="font-size: 11px; margin-left: 0px"><b>Note :</b><span
					id="charNum">300</span> characters are left.</span></td>
			</tr>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><b>Additional
				Information : </b></td>
				<td style="vertical-align: top;"><input type="text"
					name="extra_info" size="36" value="<?php echo $extra_info; ?>"
					id="extra_info" class="input" /></td>
			</tr>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><label><b>Thumbnail
				Image :</b></label></td>
				<td style="vertical-align: top;" height="60">
				<div style=""><?php echo $file; ?></div>
				<div style="font-size: 11px; margin-left: 0px"><b>Note :</b> Image
				dimension should be 204 X 192.Image either be JPEG or PNG.</div>
				</td>
				<input type="hidden" value="" id="fileName" name="fileName" />

				<input type="hidden" value="" id="fileSize" name="fileSize" />

				<input type="hidden" value="" id="fileType" name="fileType" />

			</tr>
			<?php if($image!=''){ ?>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"></td>
				<td style="vertical-align: top;" height="0"><?php if($image!=''){ ?>
				<span>Current uploaded-Image : <a id="image" target="_blank"
					href="<?php echo $promo_msg_img; ?>"><?php echo $image; ?> </a></span>
					<?php } ?> <?php if($image!=''){ ?> <br />
				<img src="<?php echo $promo_msg_img; ?>" width="60" height="60" /> <?php  } ?>
				</td>

			</tr>
			<?php } ?>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><b>Promotions
				url : </b></td>
				<td style="vertical-align: top;"><input type="text"
					name="promo_page_url" autocomplete="off" size="25"
					value="<?php echo $promo_page_url; ?>" id="promo_page_url"
					class="input" />
				<button id="preview_button" type="button" style="margin-left: 10px;"
					class="button grey-gradient glossy preview">
				<div><b>Preview</b></div>
				</button>
				<!--                                                              <a  class="preview"  >Preview</a>-->

				</td>
			</tr>
			<tr style="">
				<td>&nbsp;</td>
				<td style=""><input type="checkbox" name="send_later"
				<?php echo $checked; ?> id="send_later" />&nbsp;Send later /
				Scheduled this message for later time</td>
			</tr>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"><b>Start Date :</b></td>
				<td style="vertical-align: top;"><input type="text"
					name="start_date" size="36" value="<?php echo $start_date; ?>"
					id="DateTimeDemo1" autocomplete="off" readonly="true"
					class="i-txt input disabled" /></td>
			</tr>
			<tr style="height: 50px;">
				<td style="vertical-align: top;"><span class="required"><b>*</b></span><b>Expiry
				Date : </b></td>
				<td style="vertical-align: top;"><input type="text" name="end_date"
					size="36" value="<?php echo $end_date; ?>" id="demo"
					class="i-txt input" />
				<div style="font-size: 11px; margin-left: 0px"><b>Note :</b>
				Promotional message will expire on this date & time.</div>
				</td>
			</tr>
			<input type="hidden" name="value" id="value" value="0" />


			<tr style="height: 90px">
				<input type="hidden" value="<?php echo $mid; ?>" id="mid" name="mid" />
				<td colspan="2" align="center" style="padding-top: 45px;">

				<button id="save_send" type="button" style="margin-left: 26px;"
					class="button grey-gradient glossy">
				<div><b>Save</b></div>
				</button>
				<button id="" type="button" style="margin-left: 26px;"
					onclick="window.location.href = '<?php echo base_url().'promo_message_lite'; ?>'"
					class="button grey-gradient glossy">
				<div><b>Cancel</b></div>
				</button>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
		</td>


	</tr>
</table>
</fieldset>
<div style="width: 55%" align="left">Note: Fields marked with <span
	class="required">*</span> are mandatory</div>





				<?php echo $form_close;  ?>


