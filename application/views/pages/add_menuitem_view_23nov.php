<style type="text/css">
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 60%;
}

#sortable li {
	margin: 0 3px 3px 3px;
	padding: 0.4em;
	padding-left: 1.5em;
	font-size: 1.4em;
	height: 18px;
}

#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
</style>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.dragsort-0.4.1.js"
	type="text/javascript"></script>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.dragsort-0.4.1.min.js"
	type="text/javascript"></script>


<link
	href="<?php  echo get_assets_path('css'); ?>backend/jquery.ptTimeSelect.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.ptTimeSelect.js"></script>
<link
	href="<?php  echo get_assets_path('js'); ?>tv_channel/jquery.multiSelct/jquery.multiSelect.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.multiSelct/jquery.multiSelect.js"></script>


<!--<link rel="stylesheet" href="themes/base/jquery.ui.all.css">-->

<script
	src="<?php echo get_assets_path('js');?>tv_channel/ui/jquery.ui.core.js"></script>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/ui/jquery.ui.widget.js"></script>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/ui/jquery.ui.mouse.js"></script>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/ui/jquery.ui.sortable.js"></script>
<!--        <link rel="stylesheet" href="demos.css">-->






<script language="javascript" type="text/javascript">

    function set_multiselect() {
        $(".addons_master").multiSelect();
    }

    function addmore_images() {
        var len = $(".image_container tr").length;
        var imgname = len+1;
        var row = '<tr><td width="215"><b>Image '+imgname+':</b></td><td><input type="file" value="" name="'+imgname+'"></td></tr>';
        $(".image_container").append(row);
    }

    $(function(){
        
        
             $("#edit-price").attr('readonly', true);
                 $("#edit-size").attr('readonly', true);
        
        
             if($("#edit-price").val() != '') {
                 $("#edit-price").attr('readonly', true);
                 $("#edit-size").attr('readonly', true);
             }     
        
        
          $("#ch").click(function(){
            if($(this).attr("checked") == true) {
                $("#edit-price").attr('readonly', false);
                 $("#edit-size").attr('readonly', false);
            }
            else {
                $("#edit-price").attr('readonly', true);
                 $("#edit-size").attr('readonly', true);
                            }
        });
        
        
        
        
        
           $(".ck").attr('readonly', true);
         $( "#sortable" ).sortable();
		   
   
//         $("#navbar-submit-button").click(function(){
//            if($("#edit-sid").val() == '') {
//                jAlert('<label class="error">Please select the section.</label>', 'Error');
//                $("#edit-sid").focus();
//                return false;
//             
//
//            }
//             if($("#edit-cid").val() == '') {
//                jAlert('<label class="error">Please select the Main category.</label>', 'Error');
//                $("#edit-cid").focus();
//                return false;
//              }
//
//              if($("#edit-subcat").val() == '') {
//                jAlert('<label class="error">Please select the Sub-category.</label>', 'Error');
//                $("#edit-subcat").focus();
//                return false;
//             
//
//              }
//
//              return true;
//         });
        


        addmore_images();
        $("#add_more_image").click(function(){
            addmore_images();
        })

//        $("#addon_group_list tbody").sortable({
//            handle: ".handle",
//            cursor:     'move',
//            axis:       'y',
//            //items: 'tr:not(.ui-state-disabled)',
//            update: function(e, ui) {
//                $(this).sortable("refresh");
//                var sorted = '';
//                sorted = $("#addon_group_list tr").map(function() { return $(this).attr("id"); }).get();
//                $("#groupids").val(sorted);
//                //alert(sorted);
//            }
//
//        }).disableSelection
      //$(".gallery").dragsort({ dragSelector: "div", dragEnd:function(){}, placeHolderTemplate: "<li class='placeHolder'><div></div></li>"});
            var isDrag = 0;

        setTimeout("set_multiselect()", 50);

        setTimeout("set_checkbox_name()", 300);

        $("#add_more_addon").click(function(){
            $.ajax({
                url: "<?php echo base_url(); ?>menuitem/get_addon_row",
                type:"POST",
                success:function(msg) {
                    $("#sortable").append(msg);
                    $(".addons_master").multiSelect()
                    $( "#sortable" ).sortable();
                }
            })            
        })

        $("#edit-addns_id").attr("multiple", "multiple");
        $("#edit-allowed-addns_id").attr("multiple", "multiple");
        if($("#edit-has_addons").attr("checked") == true) {
            $("#product_list").show();
            $("#addon_list").show();
            //$("#allowed_addon_list").show();
        }
        
        
         if($("#edit-has_addons").attr("checked") == true) {
        
            $("#product_list").show();
            $("#addon_list").show();
            //$("#allowed_addon_list").show();
        }
        
        
        $("#edit-sid").change(function(){
            var param = 'name=section&val='+$(this).val();
            var path = 'menuitem/selectdata';
            make_call(path, param, 'category');
        });
     
        $("#edit-has_addons").click(function(){ 
              if($(this).attr("checked") == "checked") {
                $("#product_list").show();
                $("#addon_list").show();
                //$("#allowed_addon_list").show();
            }
            else {
                $("#product_list").hide();
                $("#addon_list").hide();
                //$("#allowed_addon_list").hide();
            }
        });
        
        
         
        $("#time").change(function(){
            
             //alert(this.value);
            if(this.value == 1) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        });
        
        $("#time2").change(function(){
            
             //alert(this.value);
            if(this.value == 1) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        })
         
         $("#time33").click(function(){
            if($(this).attr("checked") == true) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        });
        
        
        
         
        $('#add_more_time').click(function() {
    //alert('yes');
    url = "<?php echo base_url(); ?>menuitem/add_more_time"
   // alert(url);
    $.get(url,function(data){
        $('#tblid > tbody:last').append(data);
          $('#sample2 input').ptTimeSelect({
            popupImage: ''
        });
    })
}); 
         
               
        $("#edit-size_price option").dblclick(function(){
            $(this).remove();
        });

        $("#add_price").click(function(){
            var price = $("#edit-price").val();
            var size = $("#edit-size").val();
            var uom = $("#edit-uom").val();
            var oldhtml = $("#edit-size_price").html();

            var flag = 0;
            $("#edit-size_price option").each(function(i){
                var arr = $(this).val().split('-');
                if(size == arr[0] && price == arr[1] && uom == arr[2]) {
                   flag = 1;
                }
            });

            if(flag) {
                jAlert('<label class="error">You have already added this criteria.</label>', 'Error');
                //alert('You have already added this criteria.');
                return false;
            }

            if(size == "") {
                jAlert('<label class="error">Please select size.</label>', 'Error');
               // alert("Please select size.");
                return false;
            }

            if(price == "") {
                jAlert('<label class="error">Please enter price.</label>', 'Error');
                //alert("Please enter price.");
                return false;
            }
            
            var opt = '<option value="'+size+'-'+price+'-'+uom+'" ondblclick="$(this).remove()">'+size+'-'+price+'-'+uom+'</option>';
            opt +=oldhtml;
            $("#edit-size_price").html(opt);
	   	
        });


//        <?php //if($mid > 0) { ?>
//            $.ajax({
//               url: '<?php //echo base_url(); ?>menuitem/show_groups/<?php //echo $mid; ?>',
//               type: 'POST',
//               //data: param,
//               success:function(msg){
//                   var ret = msg;
//                   if(ret.indexOf(',') > 0) {
//                       var gArr = ret.split(',');
//                       for(var i=0; i < gArr.length; i++) {
//                           var gid = gArr[i];
//                            $("#a"+gid).attr("checked", "checked");
//                            $("#addon"+gid).show();
//                       }
//                   }
//                   else {
//                      $("#a"+msg).attr("checked", "checked");
//                      $("#addon"+msg).show();
//                   }
//               }
//            });
//        <?php //}?>

    });
    
        function delete_row(obj) {

   // $('#AddRow').show();
    $(obj).parent("td").parent("tr").remove();
} 
         
         
        function submit_form(){
          if($('#sample2').css('display') != 'none'){  
             var tp=0;
            $('.ck').each(function(){ 
    //if statement here 
    // use $(this) to reference the current div in the loop
    //you can try something like...

            
                if($(this).val()==''){
                     jAlert('<label class="error">Please select Addons Time .</label>', 'Error');
                     tp=1;
                     return false;
                     
                }
              

             });
        
            if(tp=='1'){ return false; }
          }  
          
      if($('#product_list').css('display') != 'none'){      
          
          var p=0;
            $('.multiSelect').each(function(){ 
    //if statement here 
    // use $(this) to reference the current div in the loop
    //you can try something like...

           
                if($(this).text()=='Select options'){
                     jAlert('<label class="error">Please select Addon`s Option .</label>', 'Error');
                     p=1;
                     return false;
                     
                }
              

             });

            if(p=='1'){ return false; }  
      }
          
           if($("#edit-revenue").val() == '') {
                jAlert('<label class="error">Please select the Revenue center .</label>', 'Error');
                $("#edit-revenue").focus();
                return false;
             

              }   
           else if($("#edit-en-descriptive_name").val() == '') {
                jAlert('<label class="error">Display Name is required field.</label>', 'Error');
                $("#edit-en-descriptive_name").focus();
                return false;
             

              }  
    
           else  if($("#edit-sid").val() == '') {
                jAlert('<label class="error">Please select the Section.</label>', 'Error');
                $("#edit-sid").focus();
                return false;
             

            }
          else  if($("#edit-cid").val() == '') {
                jAlert('<label class="error">Please select the Main category.</label>', 'Error');
                $("#edit-cid").focus();
                return false;
              }
          else  if($("#edit-size").val() == '') {
                jAlert('<label class="error">Please select the Size.</label>', 'Error');
                $("#edit-size").focus();
                return false;
              }
              
           else  if($("#edit-price").val() == '') {
                jAlert('<label class="error">Please select the Price.</label>', 'Error');
                $("#edit-price").focus();
                return false;
              }   
          
           else   if($("#edit-subcat").val() == '') {
                jAlert('<label class="error">Please select the Sub-category.</label>', 'Error');
                $("#edit-subcat").focus();
                return false;
             

              }

              
            
            
      
	 else if($('#time').attr("checked") == true) {
	
           if($('#from1').val()=='' || $('#to1').val()=='') {
             jAlert('<label class="error">Please select the time.</label>', 'Error');
              return false;
           }
         }
            
            var my = new Array();
            var cg='';
            var sorted = '';
            sorted = $("#addon_group_list tr").map(function() { return $(this).attr("id"); }).get();
            $("#groupids").val(sorted);
           
           
             
           
//          if($("#edit-size_price option").length < 1) {
//              
//                jAlert('<label class="error">Please enter atleast one price and size.</label>', 'Error');
//                return false;
//            }
//            else {
//                $("#edit-size_price option").attr("selected", "selected");
//                 //return true;
//            }

	   return true;
        }

    function group_onchange(obj) {
        $.ajax({
            url: "<?php echo base_url(); ?>menuitem/get_group_addons",
            type:"POST",
            data:"gid="+$(obj).val(),
            success:function(msg){
                $(obj).parent("td").parent("tr").children("td").eq(2).html(msg);
                $(".addons_master").multiSelect();
                $(".multiSelectOptions").children("label").children(" :checkbox").each(function(){
                    $(this).attr("name", "addons_master[]");
                   // $(this).attr("css", "font-size:14px");
                })
            }
        })
    }

    function set_checkbox_name() {
        $(".multiSelectOptions").children("label").children(" :checkbox").each(function(){
            $(this).attr("name", "addons_master[]");
        })
    }

    function get_sub_cat(cid) {
        var param = 'name=cat&val='+cid;
        var path = 'menuitem/selectdata';
        make_call(path, param, 'subcat');
    }

    function remove_image(pos, mid) {
        if(confirm('Are you sure to delete image.!')) {
            var path = 'menuitem/removefood_image';
            var param = 'pos='+pos+'&mid='+mid;
            make_call(path, param, undefined);
            $("#imgfield_"+pos).show();
            $("#imgtext_"+pos).hide();
        }
    }
   
                  
   function check(id){
	var a=new Array();
	a=document.getElementsByName("addons_master[]");
	//alert("Length:"+a.length);
       
	var p=0;
	for(i=0;i<a.length;i++){
		if(a[i].checked){
			//alert(a[i].value);
			p=1;
		}
	}
	if (p==0){
		alert('please select at least one check box');
		return false;
	}
			
	
	return true;
   }
	
      
    
    function make_call(path, param, id) {
        $.ajax({
           url: '<?php echo base_url(); ?>'+path,
           type: 'POST',
           data: param,
           success:function(msg){
               $('#'+id).html(msg);
           }
        });
    }
         
         
        function add_mo() { 
        $.ajax({ 
           url: '<?php echo base_url(); ?>menuitem/add_more',
           type: 'POST',
//           data: param,
           success:function(msg){
               $('#ad').append(msg);
           }
        });
    }  
       
         function dele(t,id) { 
            $(t).parent().remove();
//        window.location='<?php echo base_url(); ?>menuitem/delete_time/'+id;

    }  
         
   function call(id,val) {
        var mid = <?php echo $mid; ?>;

        if(val) {}else {
            var obj = $("#addon"+id).children("table").children("tbody").children("tr").children("td").children("div");
            $(obj).children("input:checkbox").removeAttr("checked");
        }

        $.ajax({
           url: '<?php echo base_url(); ?>menuitem/sub/'+id,
           type: 'POST',
           data: 'id='+id+'&mid='+mid,
           success:function(msg){
               if(val==false){
                 $('#addon'+id).hide();
               }
               else{
                $('#addon'+id).show();
                $('#addon'+id).html(msg);
               }
           }
         });
    }
</script>
<script language="JavaScript">
    $(function(){
        $('#sample2 input').ptTimeSelect({
            popupImage: ''
        });
//         $('#tblid tr td  input').ptTimeSelect({
//            popupImage: ''
//        });
    })
</script>
<style>
a.multiSelect span {
	font-size: 14px;
	padding-top: 3px;
	margin-top: 3px
}

.multiSelectOptions label {
	font-size: 14px
}

.even {
	
}

.odd {
	background: #F7F7F7
}

#table tr td {
	height: 45
}

#tab {
	border-top: 1px solid #E0E0E0;
}

.listheading th {
	background: none repeat scroll 0 0 #2D2D28;
	color: #FFFFFF;
	height: 32px;
	padding-top: 0px;
	font-size: 14px;
}

.border_tbl {
	border: #E8E8E8 1px solid;
	font-size: 12px;
	padding: 0px;
	-moz-border-radius: 4px /*{cornerRadius}*/;
	-webkit-border-radius: 4px /*{cornerRadius}*/;
	border-radius: 4px /*{cornerRadius}*/;
}
</style>
<!-- Main container Satrt -->
<?php if(isset($page_heading)) { ?>
<!--<div class="pageheading"><?php// echo $page_heading; ?></div>-->
<?php }?>
<div class="pagecontent">&nbsp;&nbsp;<?php if(isset($add_link)) echo $add_link; ?></div>
<?php echo $form_open; ?>
<input
	type="hidden" name="groupids" id="groupids" />
<div class="border_tbl" style="width: 70%">
<table cellpadding="2" cellspacing="2" width="80%" id="table"
	class="pageform" style="margin-top: 30px" align="center">
	<tr>
		<td valign="top"><b>Revenue center:</b></td>
		<td><?php echo $rvc; ?></td>
	</tr>
	<tr>
		<td><b>Number:</b></td>
		<td><?php echo $number; ?> <label>(it is unque number for POS)</label></td>
	</tr>
	<tr>
		<td><b>Name:</b></td>
		<td><?php echo $name; ?> <label>(it is unque name for POS)</label></td>
	</tr>
	<?php foreach($descriptive_name as $n=>$field) {?>
	<tr style="padding-top: 15px">
		<td valign="top"><b>Display Name in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>
	<?php foreach($synopsis as $n=>$field) {?>
	<tr>
		<td valign="top" width="210"><b>Synopsis in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>
	<tr>
		<td style="padding-top: 25px; height: 70px"><b>Position:</b></td>
		<td style="padding-top: 25px; height: 70px"><?php echo $position; ?></td>
	</tr>
	<tr>
		<td><b>Delivery Time:</b></td>
		<td><?php echo $delivery_time; ?></td>
	</tr>
	<tr>
		<td colspan="2">
		<fieldset class="fieldset fields-list"
			style="border: #6E6E6E 1px solid; height: 350px;"><legend
			style="font-size: 14px; font-weight: bold;" class="legend"><em>Price
		Details</em></legend>
		<table cellpadding="2" cellspacing="2" width="80%"
			style="font-size: 12px; padding: 30px; margin-left: 65px; margin-top: 25px">

			<tr>
				<input type="hidden" name="up"
					value="<?php echo  $this->uri->segment(4); ?>" />
				<td valign="top" width="215"><b>Size:</b></td>
				<td><?php echo $size; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Price:</b></td>
				<td><?php echo $price; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Measurement Unit:</b></td>
				<td><?php echo $uom; ?><br />
				<label>(If menuitem has not unit of measurement then <em>NULL</em>
				will be the default value)</label></td>
			</tr>
			<tr>
				<td valign="top"></td>
				<td><?php //echo $add_button; ?>
				<div
					style="height: 32px; padding-top: 15px; vertical-align: middle; float: left; margin-right: 10px;">
				<button class="button grey-gradient glossy icon-star" type="button"
					id="add_price" name="add_price">Add Price</button>
				</div>
				</td>
			</tr>
			<tr>
				<td valign="top" style="padding-top: 20px"><b>Selected Criteria:</b></td>
				<td style="padding-top: 20px"><?php echo $size_price; ?><br />
				<label>(<em>Double Click</em> to remove selected criteria)</label></td>
			</tr>
		</table>
		</fieldset>
		</td>
	</tr>

	<!--     <tr>
        <td colspan="2">
            <fieldset class="border_tbl" style="border:#6E6E6E 1px solid;">
                <legend style="font-size: 14px;padding-bottom:10px; font-weight: bold;"><em>Price Details</em></legend>
                <table cellpadding="2" cellspacing="2" width="80%" style="font-size: 12px;">
                <tr>
                    <td valign="top"><b></b></td> <td style="vertical-align: middle;padding-bottom:20px;padding-top:10px">&nbsp;&nbsp;<input type="checkbox" name="ch" id="ch" style="vertical-align: middle;padding-bottom:10px;"  />&nbsp;<label for="ch" style="padding-left:10px; vertical-align: middle">I want to overwrite the Price and Size of Micros</label></td>
                </tr>
                <tr>
                    <input type="hidden" name="up" value="<?php echo  $this->uri->segment(4); ?>" />
                    <td valign="top" width="215"><b>Size:</b></td> <td><?php echo $size; ?></td>
                </tr>
                <tr>
                    <td valign="top"><b>Price:</b></td> <td><?php echo $price; ?></td>
                </tr>
                
                
               
            </table>
                </fieldset>
        </td>
    </tr>-->

	<tr>
		<td colspan="2">
		<fieldset class="fieldset fields-list"
			style="border: #6E6E6E 1px solid; height: auto; padding: 30px; margin-top: 20px">
		<legend style="font-size: 14px; font-weight: bold;" class="legend"><em>Addons</em></legend>
		<table cellpadding="2" cellspacing="2" width="98%"
			style="font-size: 12px; margin-left: 30px">

			<tr>
				<td>
				<table>
					<tr>
						<td width="215" style="padding-left: 5px"><b>Has Addons:</b></td>
						<td style="padding-left: 3px;"><?php echo $has_addons; ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr id="product_list" style="display: none;">
				<!--<td valign="top">
            <b>Select Addon Group:</b>
        </td>-->
				<td colspan="2">
				<div>
				<table cellpadding="2" cellspacing="2"
					style="font-size: 10px; margin-left: 220px" id="addon_group_list">
					<tr>
						<td>

						<div id="sortable"><?php if(count($menuitem_addon_row) > 0) {
							$i=0;
							foreach($menuitem_addon_row as $row) {
								?>

						<div class="ui-state-defaul">
						<table>
							<tr>
								<td>
								<div class="handle"><img class="tabledrag-handle"
									style="cursor: move;"
									src="<?php echo get_assets_path('image'); ?>backend_images/arrow1.png"
									title="Drag to re-order" href="#"></div>
								</td>
								<td style="margin-left: 10px"><?php echo $row['groups']; ?></td>
								<td><?php echo $row['addons'];?></td>
								<td style="margin-left: 10px" width="20"><?php if($i > 0) {?><a
									href="#"
									onclick="$(this).parent().parent().remove(); return false;">[X]</a><?php }?>
								&nbsp;</td>
							</tr>
						</table>
						</div>

						<?php $i++;}} else {?>
						<div class="ui-state-defaul">
						<table>
							<tr>
								<td>
								<div class="handle"><img class="tabledrag-handle"
									style="cursor: move;"
									src="<?php echo get_assets_path('image'); ?>backend_images/arrow1.png"
									title="Drag to re-order" href="#"></div>
								</td>
								<td><?php echo $groups; ?></td>
								<td><?php echo $group_addons;?></td>
								<td width="20">&nbsp;</td>
							</tr>
						</table>
						</div>
						<?php }?></div>
						</td>
					</tr>
				</table>
				<div
					style="height: 32px; vertical-align: middle; float: left; margin-right: 10px; margin-top: 10px; margin-left: 220px">
				<button class="button grey-gradient glossy icon-star" type="button"
					id="add_more_addon" name="add_more_addon">Add More</button>
				</div>
				</div>
				</td>
			</tr>

			<tr id="allowed_addon_list" style="display: none;">
				<td valign="top"><b>Allowed Addon Group:</b></td>
				<td><?php echo $allowed_addons; ?></td>
			</tr>
			<tr id="addon_list" style="display: none;">
				<td valign="top" style="display: none"><b>Is Addon Required:</b></td>
				<td style="display: none"><?php echo $required_addon; ?></td>
			</tr>
		</table>
		</fieldset>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<fieldset class="fieldset fields-list"
			style="border: #6E6E6E 1px solid; height: auto; padding: 30px; margin-top: 20px">
		<legend style="font-size: 14px; font-weight: bold;" class="legend"><em>Special
		Attributes</em></legend>
		<table cellpadding="2" cellspacing="2" width="98%"
			style="font-size: 12px;">

			<tr>
				<td colspan="2">
				<table cellpadding="2" cellspacing="2" width="80%"
					style="margin-left: 30px">
					<tr>
						<td width="215"><b>Is Chef's Special:</b></td>
						<td><?php echo $special; ?></td>
						<td style="margin-left: 20px; padding-left: 10px" align="right"><label
							style="vertical-align: bottom; padding-top: 10px; margin-top: 20px;"><b>Has
						Pork:&nbsp;&nbsp;</b></label></td>
						<td align="center"><?php echo $porch; ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<table cellpadding="2" cellspacing="2" width="80%"
					style="margin-left: 30px">
					<tr>
						<td width="215"><b>Is Veg: </b></td>
						<td><?php echo $is_vag; ?></td>
						<td align="right"><b>In Breakfast:</b></td>
						<td align="center"><?php echo $has_breakfast; ?></td>
					</tr>

				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<table cellpadding="2" cellspacing="2" width="80%"
					style="margin-left: 30px">
					<tr>
						<td width="215"><b>Is Beverages: </b></td>
						<td><?php echo $is_beverages; ?></td>
						<td align="right"></td>
					</tr>

				</table>
				</td>
			</tr>
			<!--   <tr>
         <td>
            <table>
                <tr>  
                   <td width="215"><b>Spicy Level</b></td><td><?php echo $spicy_level; ?></td> 
                   
                </tr>
            </table> 
       </td>
        
    </tr>-->
			<tr>
				<td>
				<table style="margin-left: 30px">
					<tr>
						<td width="215" valign="top" style="padding-top: 8px"><b>Availability</b></td>
						<td width="580">
						<div style="float:left;margin-left:10px;<?php if(!empty($time)){ ?> padding-top:15px;<?php } else { ?> padding-top:10px;<?php } ?> "><?php echo $time2;?><label
							style="vertical-align: middle">All Time</label> <?php echo $time1; ?><label>Restricted
						Time</label></div>


						<?php if(!empty($time)){ ?>

						<div id="sample2"
							style="margin-left: 20px; vertical-align: top; padding-top: 10px;">
						<table id="tblid">
							<tbody>
							<?php  for($i=0;$i<count($time);$i++){  ?>
							<?php if($i==0){  ?>
								<tr>
									<td><em style="margin-left: 10px"><b> From</b></em> <input
										type="text" class="ck input" name="from1[]"
										<?php if (isset($time[$i]['from_time'])){?>
										value="<?php echo $time[$i]['from_time']; ?>"
										<?php }  else { ?> value="" <?php } ?> size="5" id="from1" />
									<em><b>To</b></em> <input type="text" class="ck input" id="to1"
										name="to1[]" <?php if (isset($time[$i]['from_time'])){?>
										value="<?php echo $time[$i]['to_time']; ?>" <?php } else { ?>
										value="" <?php } ?> size="5" />
									<button style="margin-left: 5px"
										class="button grey-gradient glossy icon-star" type="button"
										id="add_more_time" name="add_more_time">Add More</button>
									</td>
								</tr>
								<?php }
								else { ?>
								<tr>
									<td><em style="margin-left: 10px"><b> From</b></em> <input
										type="text" class="ck input" name="from1[]"
										<?php if (isset($time[$i]['from_time'])){?>
										value="<?php echo $time[$i]['from_time']; ?>"
										<?php }  else { ?> value="" <?php } ?> size="5" id="from1" />
									<em><b>To</b></em> <input type="text" class="ck input" id="to1"
										name="to1[]" <?php if (isset($time[$i]['from_time'])){?>
										value="<?php echo $time[$i]['to_time']; ?>" <?php } else { ?>
										value="" <?php } ?> size="5" /><a href="#sample2"
										class="delete" onclick="delete_row(this)"
										title="Click here to remove entry"
										style="color: red; margin-left: 40px;"><strong>X</strong></a>
									</td>
								</tr>
								<?php } } ?>
							</tbody>
						</table>
						</div>
						<?php  }
						else { ?>

						<div id="sample2" style="margin-left: 20px; display: none">
						<table id="tblid">
							<tbody>

								<tr>
									<td><em style="margin-left: 10px"><b> From</b></em> <input
										type="text" class="ck input" name="from1[]" value="" value=""
										size="5" id="from1" /> <em><b>To</b></em> <input type="text"
										class="ck input" id="to1" name="to1[]" value="" size="5" />
									<button style="margin-left: 5px"
										class="button grey-gradient glossy icon-star" type="button"
										id="add_more_time" name="add_more_time">Add More</button>
									</td>
								</tr>

							</tbody>
						</table>
						</div>
						<?php } ?></td>

					</tr>
				</table>
				</td>

			</tr>


		</table>
		</fieldset>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<fieldset class="fieldset fields-list"
			style="border: #6E6E6E 1px solid; height: 160; padding: 30px; margin-top: 20px">
		<legend style="font-size: 14px; font-weight: bold;" class="legend"><em>Cataloging</em></legend>
		<table cellpadding="2" cellspacing="2" width="80%"
			style="font-size: 12px; margin-left: 30px">
			<tr>
				<td width="215" valign="top"><b>Section:</b></td>
				<td><?php echo $sid; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Main Category:</b></td>
				<td id="category"><?php echo $cid; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Subcategory:</b></td>
				<td id="subcat"><?php echo $sub_cat_id; ?></td>
			</tr>
		</table>
		</fieldset>

		</td>
	</tr>


	<tr>
		<td colspan="2">
		<fieldset class="fieldset fields-list"
			style="border: #6E6E6E 1px solid; height: 150; padding: 30px; margin-top: 20px">
		<legend style="font-size: 14px; font-weight: bold;" class="legend"><em>Images
		& Video</em></legend>
		<table cellpadding="2" cellspacing="2" width="80%"
			style="font-size: 12px;">
			<tr>
				<td>
				<table style="margin-left: 30px">
					<tr>
						<td width="215"><b>Video:</b></td>
						<td><?php echo $video; if(isset($selected_video) && $selected_video != "") {echo " Current Video: &nbsp; <a href='".BASE_URL.FOOD_VIDEOS.$selected_video."' target='_blank'>".$selected_video."</a>"; }?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<table cellpadding="2" cellspacing="2" width="90%"
					class="image_container" style="margin-left: 30px">
					<?php if(count($images) > 0) {
						$i=0;
						foreach ($images as $name=>$field) {
							if(isset($field['image']) && $field['image'] != "") {

								?>
					<tr>
						<td width="215"><b>Image <?php echo $name+1;?>:</b></td>
						<td>&nbsp;&nbsp;&nbsp;<?php if(isset($field['image']) && $field['image'] != ""){ echo '<div id="imgfield_'.$field['pos'].'" style="float:left; display:none; margin-right:5px;">'.$field['field']."</div>";}else {echo $field['field'];} if($field['image'] != ""){ echo '<div id="imgtext_'.$field['pos'].'" style="float:left; margin-right:5px;">'."Current Image: &nbsp;".anchor(BASE_URL.FOOD_IMAGES.$field['image'], $field['image'], array('target'=>'_blank')).' | <a href="#" onclick="remove_image('.$field['pos'].', '.$mid.')"><b>[Remove]</b></a> </div>'; }?>
						</td>
					</tr>
					<?php $i++;}} }?>
				</table>
				<div
					style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
				<button class="button grey-gradient glossy" type="button"
					id="add_more_image" name="add_more_image">Add More</button>
				</div>
				</td>
			</tr>
		</table>
		</fieldset>

		</td>
	</tr>


	<tr>
		<td style="padding-top: 20px; padding-left: 60px;" width="215"><b>Is
		active:</b></td>
		<td style="padding-top: 20px; padding: left : 30px;"><?php echo $active; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div style="width: 40%; float: left;">&nbsp;</div>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button id="navbar-submit-button" class="button grey-gradient glossy"
			type="submit">
		<div><b> <?php if(isset($id)){ echo "Save"; ?> <?php }else {echo "Add";}?></b></div>
		</button>

		</div>
		<div
			onclick="window.location.href='<?php echo base_url(); ?>menuitem'"
			style="height: 32px; vertical-align: middle; float: left;">
		<button id="navbar-cancel-button" class="button grey-gradient glossy"
			type="button">
		<div><b>Cancel</b></div>
		</button>
		</div>

		<?php //echo $cancel; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
</div>
		<?php echo $form_id; if(isset($id)) echo $id; ?>
		<?php echo $form_close; ?>
<br />
<br />
