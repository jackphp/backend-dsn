<script language="javascript" type="text/javascript">
    
    
     $(function(){
      
      
       $('#navbar-submit-button').click(function() {
       if($('#edit-section').val()==''){
         jAlert("Please select Section!", "Error"); 
         return false;
    }
     
      if($('#edit-en-display_name').val()==''){
         jAlert("Please enter Dispaly Name!", "Error"); 
         return false;
    }    
    
    if($('#edit-name').val()==''){
         jAlert("Please enter Category Name!", "Error"); 
         return false;
    } 
      
      
      
      
    });  
     
    
     });    
    
    
    function test(){ 
          
    
       
    }
</script>

<title>IRD Management</title>

<style>
#edit-en-description {
	resize: none
}

.even {
	
}

.odd {
	background: #F7F7F7
}

#table tr td {
	height: 40;
	font-size: 14px;
}

.listheading th {
	background: none repeat scroll 0 0 #2D2D28;
	color: #FFFFFF;
	height: 32px;
	padding-top: 0px;
	font-size: 14px;
}

.border_tbl {
	border: #E8E8E8 1px solid;
	font-size: 12px;
	padding: 0px;
	-moz-border-radius: 4px /*{cornerRadius}*/;
	-webkit-border-radius: 4px /*{cornerRadius}*/;
	border-radius: 4px /*{cornerRadius}*/;
}
</style>


<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<!--<div class="pageheading"><?php// echo $page_heading; ?></div>-->
<?php }?>
<div class="pagecontent">&nbsp;&nbsp;<?php if(isset($add_link)) echo $add_link; ?></div>
<?php echo $form_open; ?>
<div class="border_tb" style="width: 60%; margin-top: 30px;">
<fieldset class="fieldset fields-list"
	style="border: #6E6E6E 1px solid; height: auto;"><legend
	style="font-size: 14px; font-weight: bold;" class="legend"><em>Category</em></legend>
<table cellpadding="2" cellspacing="2" width="90%" id="table"
	style="margin-top: 50px" align="center">
	<tr>
		<td><b>Section Name:</b></td>
		<td id="sid"><?php echo $section_id; ?></td>
	</tr>
	<tr>
		<td><b>Category Name:</b></td>
		<td><?php echo $name; ?></br>
		<b style="font-size: 12px;">(Unique POS Name)</b></td>
	</tr>
	<?php foreach($display_name as $n=>$field) {?>
	<tr>
		<td style="height: 60px; padding-top: 25px"><b>Display Name<br />
		in <?php echo $languages[$n];?>:</b></td>
		<td style="padding-top: 20px"><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>



	<?php if(count($images) > 0) {
		foreach ($images as $name=>$field) {
			?>
	<tr>
		<td style="height: 60px; padding-top: 25px"><b>Image For iPad:</b></td>
		<td style="padding-top: 20px"><?php echo $field['field']; if($field['image'] != ""){ echo "<br/><br/> Current Image: &nbsp;".anchor(ASSETS_PATH.FOOD_IMAGES.$field['image'], $field['image'], array('target'=>'_blank')); }?>
		</td>
	</tr>
	<?php } }?>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div style="width: 40%; float: left;">&nbsp;</div>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button id="navbar-submit-button" class="button grey-gradient glossy"
			type="submit">
		<div><b> <?php if(isset($id)){ echo "Save"; ?> <?php }else {echo "Add";}?></b></div>
		</button>
		</div>
		&nbsp;&nbsp;
		<div
			onclick="window.location.href='<?php echo base_url(); ?>food_category'"
			style="height: 32px; vertical-align: middle; float: left;">
		<button id="navbar-cancel-button" class="button grey-gradient glossy"
			type="button">
		<div><b>Cancel</b></div>
		</button>
		</div>

		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
</fieldset>
</div>
	<?php echo $form_id; if(isset($id)) echo $id; ?>
	<?php echo $form_close; ?>
<br />
<br />
