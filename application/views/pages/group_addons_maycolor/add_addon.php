<link
	href="<?php  echo get_assets_path('css'); ?>dashboard/reset.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/style.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>dashboard/colors.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/files.css?v=1">
<link
	rel="stylesheet"
	href="<?php  echo get_assets_path('css'); ?>backend/styles/form.css?v=1">
<script language="javascript" type="text/javascript">
    
    
      $(function(){
       $('#submit').click(function() {
	    if($('#edit-display-name').val()==''){
		 parent.jAlert("Please enter Dispaly Name!", "Error"); 
		 return false;
	    }else{
        	 $.post( "<?php echo base_url()?>group_addons/submit", $("form").serialize(),
		      function( data ) {
                          var m= $(data).find('#page_message').html();  
                          parent.$('#page_message').show().html(m);
                          var content = $( data ).find( '#listWraper' ).html();
                          $( "#listWraper", window.parent.document ).empty().append( content );
			  parent.makeSortableTable();
                          parent.delete_cat();
                          parent.color(); 
			  parent.$.fn.colorbox.close();
                          parent.hide_message();  
		      }
		 );
            }  
       });  
    });    

  

    function test(){ 
          
    
       
    }
</script>
<style>
</style>
<script type="text/javascript">$(".delete").click(function(){ 
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location; 
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                })</script>

<?php if(isset($page_heading)) {?>

<div class="pageheading"><?php echo $page_heading; ?></div>
<br />
<?php }?>
<?php echo $form_open;?>
<table cellpadding="2" cellspacing="2" width="80%"
	style="margin-top: 120px; padding-left: 50px; padding-top: 20px"
	class="border_tbl" align="center">

	<tr>
		<td height="50"><b>POS Addon Family Code :</b></td>
		<td><?php echo $group_code; ?></td>
	</tr>
	<tr>
		<td><b> POS Addon Family Name :</b></td>
		<td><?php echo $group_name; ?></td>
	</tr>
	<?php foreach($display_name as $n=>$field) {?>
	<tr>
		<td><b>Display Name in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>

	<!--<tr>
        <td><b>Display Name</b></td>
        <td><?php echo $display_name; ?></td>
    </tr>-->

	<tr>
		<td><?php echo $form_id.$id; ?></td>
		<td>

		<button type="button" class="button grey-gradient glossy"
			name="submit" id="submit">
		<div><b>Save</b></div>
		</button>
		<button type="button" class="button grey-gradient glossy"
			name="cancel" onclick="parent.$.fn.colorbox.close()"">
		<div><b>Cancel</b></div>
		</button>
		</td>
		<!--        <td><input type="submit" name="submit" value="Save" onclick="test();" /> <input type="button" name="cancel" value="Cancel" onclick="parent.$.fn.colorbox.close()" /></td>-->
	</tr>
</table>
	<?php echo $form_close;?>
