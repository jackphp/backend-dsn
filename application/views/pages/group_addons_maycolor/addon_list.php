
<style type="text/css">
ul {
	list-style-type: none;
}

.gallery4 ul {
	width: 100%;
	list-style-type: none;
	margin: 0px;
	padding: 0px;
}

.gallery4 li div {
	width: 100%;
	height: 100%;
	text-align: center;
	padding-top: 0px;
	padding-bottom: 0px;
}

.placeHolder div {
	background-color: white !important;
	border: dashed 1px gray !important;
}

.tip {
	color: #FFFFFF;
	padding: 5px 5px 5px 5px;
	background-color: #000000;
	border: #CCCCCC 1px solid;
	font-size: 14px;
	display: none;
	zindex: 999;
	position: absolute;
}

span {
	text-align: left;
}

#tbl {
	width: 98% !important
}
</style>

<title>IRD Management</title>

<script
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.dragsort-0.4.1.js"
	type="text/javascript"></script>
<script
	src="<?php echo get_assets_path('js');?>tv_channel/jquery.dragsort-0.4.1.min.js"
	type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	   
	    $(function(){
              //alert($('#page_message').length);
             //   $('#page_message').change(function(){ alert('yes');});
                     
                    hide_message();

		$(".addon_form").click(function(){
		  
		    $(".addon_form").colorbox({title:'<h4>&nbsp;&nbsp;Add New Addon Family</h4>',width:"650", fixed:true, height:"450", iframe:true, href:'<?php echo base_url(); ?>group_addons/add' ,/*onClosed:function(){ location.reload(true); },*/ overlayClose:false, onComplete: function() {
		      var window_height = $(window).height();
		    var colorbox_height = $('#colorbox').height();
		    var top_position = 0;

		    if(window_height > colorbox_height) {
		        top_position = (window_height - colorbox_height) / 2;
		    }

		    // alert(top_position);
		    $('#colorbox').css({'top':top_position, 'position':'fixed'});
		}});

		   })
	       
	 
	
		$(".addon_form2").click(function(){
		  
		    $(".addon_form2").colorbox({title:'<h4>&nbsp;&nbsp;Edit Addon Family</h4>',width:"650",fixed:true, height:"450", iframe:true , /*onClosed:function(){ location.reload(true); },*/ overlayClose:false, onComplete: function() {
		    // alert('window = ' + $(window).height());
		    // alert('colorbox = ' + $('#colorbox').height());

		    var window_height = $(window).height();
		    var colorbox_height = $('#colorbox').height();
		    var top_position = 0;

		    if(window_height > colorbox_height) {
		        top_position = (window_height - colorbox_height) / 2;
		    }

		    // alert(top_position);
		    $('#colorbox').css({'top':top_position, 'position':'fixed'});
		}});
		})
	    })

function delete_cat(){
        $(".delete").click(function(){ 
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location; 
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                }) 
} 


function color(){
             $(".addon_form2").click(function(){
		  
		    $(".addon_form2").colorbox({title:'<h4>&nbsp;&nbsp;Edit Addon Family</h4>',width:"650",fixed:true, height:"450", iframe:true , /*onClosed:function(){ location.reload(true); },*/ overlayClose:false, onComplete: function() {
		    // alert('window = ' + $(window).height());
		    // alert('colorbox = ' + $('#colorbox').height());

		    var window_height = $(window).height();
		    var colorbox_height = $('#colorbox').height();
		    var top_position = 0;

		    if(window_height > colorbox_height) {
		        top_position = (window_height - colorbox_height) / 2;
		    }

		    // alert(top_position);
		    $('#colorbox').css({'top':top_position, 'position':'fixed'});
		}});
		})

}

	    
function makeSortableTable(){
      if($(".noData").length == 0){       
	    $( ".gallery4" ).sortable({
		stop: function( event, ui ) {
		    
		    saveSortOrder();
               $("#listWraper").find("table tr").last().css("border-bottom","1px solid #CCCCCC")
        },
       scroll: false, 
	axis: "y",
        appendTo: '#tbl',
	containment: "#listWraper",
	placeholder: "sortable-placeholder"

	 
		
	    });
      }    
           
                 $( ".gallery4" ).find('td').each(function(elem){
		console.log($(this).width());
		$(this).css('width',$(this).width());
	});
             $(".gallery4").sortable({ scroll: true, scrollSpeed: 10 });
             // $(".gallery4").sortable({ scroll: true, scrollSensitivity: 100, scrollSpeed: 500 });
		$( ".gallery4" ).disableSelection();
}	   
	   
	  
	$(document).ready(function() {
	makeSortableTable();

	      
	     // $(".gallery4 tr").dragsort({ dragBetween: true });
		    var isDrag = 0;
	    
	//        $("#list").sortable({
	//            handle: ".handle",
	//            cursor:     'move',
	//            axis:       'y',
	//            //items: 'tr:not(.ui-state-disabled)',
	//            update: function(e, ui) {
	//                href = '<?php echo base_url(); ?>menuitem/sort_group';
	//                $(this).sortable("refresh");
	//                sorted = $("#list tr").map(function() { return $(this).attr("id"); }).get();
	//                
	//                $.ajax({
	//                        type:   'POST',
	//                        url:    href,
	//                        data:   'ids='+sorted,
	//                        success: function(msg) {
	//                               window.location.reload();
	//                        }
	//                });
	//            }
	// 
	//        }).disableSelection();
		});
		
		function check(id,val){
	//        alert(val);
		   url = "<?php echo base_url()?>food_category/update_cat_status/"+id+'/'+val;
		      
		        $.get(url,function(data){
	//                      window.location.reload();
		        })     
		        }
	    
                 function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }

	    function saveSortOrder() {
		    //alert('rf');
		    var data = $(".gallery4 tr").map(function() { return $(this).attr("id"); }).get();
		    //var cids = $("#sorted_id").val();
		    //alert(data);

		    $.post("<?php echo base_url(); ?>menuitem/sort_save", { "ids[]": data});
		     //window.setTimeout('location.reload()', 100);
		  //  $("#msg").html("<center><label>Saving...</label></center>");
		    //setTimeout("reload()", 4000);
		}
	</script>

<?php if(isset($page_heading)) {?>
<!--<div class="pageheading"><?php// echo $page_heading; ?></div>-->
<?php }?>

<div id="msg"
	style="margin-top: 0px;"></div>
<div id="page_message"
	style="display: none; width: 600px; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php if( $this->session->flashdata('_messages') ){?><script>$('#page_message').show();</script>
<?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?><?php } ?></div>


<div class="button grey-gradient glossy"
	style="float: left; margin-left: 16px; margin-bottom: 5px; margin-top: 35px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>menuitem'">
<div title="Back To Menu Items" class="with-tooltip"><label>Back</label></div>
</div>

<div class="button grey-gradient glossy"
	style="float: left; margin-left: 4px; margin-bottom: 5px; margin-top: 35px;"
	id="send_letter">
<div title="Add New Addon Family" class="with-tooltip addon_form"><label>Add
New Addon Family</label></div>
</div>
<div style="clear: both" id="listWraper">
<center><?php echo $links; ?></center>
<table cellpadding="2" cellspacing="2" style="margin-left: 0px;"
	id="tbl" class="table" width="100%" align="center">
	<thead>
		<tr>
			<th align="center" width="10%" scope="col">#</th>
			<th align="center" width="20%" scope="col">POS Addon Family</th>
			<th align="center" width="20%" scope="col">POS Code</th>
			<th align="center" width="20%" scope="col">Display Name</th>
			<th align="center" width="30%" scope="col">Operations</th>
		</tr>
	</thead>
	<tbody class="gallery4">


		<!-- <ul  class="gallery4" style="list-style-type: none;"> -->
	<?php
	if($addons->result()){
		$i=1;
		foreach($addons->result() as $a) {
			if($i%2==0) {$class="even";} else {$class="odd";};
			?>

		<!--   <li id="<?php echo $a->gid; ?>"  align="center" >-->
		<!--            <div style="width:1502;margin-right:7px; height: 100%;" align=" center" class="<?php echo $class; ?>">-->
		<!--<table cellpadding="2" cellspacing="2" width="100%"   class="table">-->

		<tr id="<?php echo $a->gid; ?>" class="<?php echo $class; ?>">
			<td scope="row" width="10%" align="center">
			<div class="handle"><img class="tabledrag-handle" href="#"
				title="Drag to re-order"
				src="<?php echo get_assets_path('image').'backend_images/arrow1.png'; ?>"
				style="cursor: move;" /></div>
			</td>
			<td scope="row" width="20%" align="left"><?php echo $a->group_name; ?></td>
			<td scope="row" width="20%" align="center">&nbsp;<?php echo $a->group_code; ?></td>
			<td scope="row" width="20%" align="left"><?php echo $a->display_name; ?></td>
			<td scope="row" width="30%" align="center"><a
				href="<?php echo base_url().'food_addons/addon_list/'.$a->gid; ?>">Add
			Addons</a> | <a
				href="<?php echo base_url().'group_addons/add/'.$a->gid; ?>"
				class="addon_form2">Edit</a> | <a
				href="<?php echo base_url().'group_addons/delete_addon/'.$a->gid; ?>"
				class="delete">Delete</a></td>
		</tr>
		<!--              </tbody> 
		        </table>
		   </div> 
		</li> -->
		<?php
		$i++; }
	}
	else{ ?>
		<tr>
			<td align="center" class="noData" colspan="5">No record found</td>
		</tr>
		<?php  }
		?>
	</tbody>
</table>

<br />
<br />
<div>&nbsp;</div>
<center><?php echo $links; ?></center>
<div>&nbsp;</div>
</div>

