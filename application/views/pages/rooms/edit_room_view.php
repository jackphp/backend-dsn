<script type="text/javascript">
    
    
    
    
    
     
    
    
       
    
		$(document).ready(function()
		{
			 
            
                        
                        $(".check_all").click(function()				
			{
				var checked_status = this.checked;
				$(".checks").each(function()
				{
					this.checked = checked_status;
				});
			});					
		});
   function delete_row(obj) {
    $(obj).parent("td").parent("tr").remove();
}             
 function update()	
  {
  
  }
   function check()
  {
      if($("#name").val()=='')
          {
              alert('Please enter the Name');
              return false;
          }
      if($("#desc").val()=='')
          {
              alert('Please enter the Description');
              return false;
          }    
      
  }
  function chk()
  {
      if($("#room").val()=='')
          {
              alert('Please enter the Room No');
              return false;
          }
     else if($("#floor").val()=='')
          {
              alert('Please select  the Floor Type');
              return false;
          } 
     else if($("#room_type").val()=='')
          {
              alert('Please select the Room Type');
              return false;
          }    
      else if($("#controller").val()=='')
          {
              alert('Please select the Controller');
              return false;
          }  
//      else  if($("#ip").val()=='')
//          {
//              alert('Please enter the IP Address ');
//              return false;
//          }  
//       else  if($("#mac").val()=='')
//          {
//              alert('Please enter the MAC Address');
//              return false;
//          }  
        else if($("#friendly").val()=='')
          {
              alert('Please select  the Disable Friendly');
              return false;
          } 
         else  if($("#bed").val()=='')
          {
              alert('Please select the Bed or Living');
              return false;
          } 
      else {
          return true;
      }
  }
		</script>

<center>
<div class="border_tbl maincontent"
	style="width: 60%; text-align: left; margin-top: 60px;"><?php if(isset($page_heading)) {?>
<div class="pageheading" style="background-color: #2D2D28; color: white">&nbsp;&nbsp;<?php echo $page_heading; ?></div>
<?php }?> <?php    if(isset($edit))
{   foreach($edit->result() as $res)   {

	?> <input type="hidden" name="id" value="<?php echo $id; ?>" />


<div style="">
<form method="post" action="<?php echo base_url();?>room/update_room"
	name="room">
<table cellpadding="2" cellspacing="2" width="100%" class="pageform"
	align="center" id="tblid" border="0">

	<!--    <tr><td class="pageheading">&nbsp;</td></tr>-->
	<!--     <tr class="even" >
        
        <td align="center"colspan="2" style="background-color:  gainsboro"><b><h3>Details Of Room Number-<?php echo $res->room_no; ?> </h3></b></td>
        
    </tr>-->
	<tr class="even">

		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Room No</div>
		</strong></td>
		<td><input type="text" value="<?php echo $res->room_no; ?>" id="room"
			name="room_no" size="10" /></td>
	</tr>
	<tr class="odd">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Floor Type</div>
		</strong></td>
		<td><select name="floor" id="floor" style="width: 125px;">
			<option value="">Select</option>
			<?php   foreach($floor_list->result() as $type)
			{  ?>
			<option value="<?php echo $type->fid;?>"
			<?php if($type->fid==$res->fid){echo 'selected';}  ?>><?php echo $type->floor_no;  ?></option>

			<?php     }
			?>
		</select></td>
	</tr>
	<tr class="even">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Room Type</div>
		</strong></td>
		<td><select name="room_type" id="room_type" style="width: 125px;">
			<option value="">Select</option>
			<?php   foreach($room_list->result() as $ty)
			{  ?>
			<option value="<?php echo $ty->room_type_id;?>"
			<?php if($ty->room_type_id==$res->room_type_id){echo 'selected';}  ?>><?php echo $ty->room_type;  ?></option>

			<?php     }
			?>
		</select></td>
	</tr>
	<tr class="odd">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">IP Address</div>
		</strong></td>
		<td><input type="text" value="<?php echo $res->room_ip; ?>" name="ip"
			id="ip" size="10" /></td>
	</tr>
	<tr class="even">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Mac Address</div>
		</strong></td>
		<td><input type="text" value="<?php echo $res->mac_address; ?>"
			id="mac" name="mac" size="10" /></td>
	</tr>
	<tr class="odd">

		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Controller</div>
		</strong></td>
		<td><select name="controller" id="controller" style="width: 125px;">
			<option value="">Select</option>
			<option value="master"
			<?php if($res->controller_type=='master'){echo 'selected';}  ?>>Master</option>
			<option value="av"
			<?php if($res->controller_type=='av'){echo 'selected';}  ?>>AV</option>
		</select></td>
	</tr>
	<tr class="even">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Disable Friendly</div>
		</strong></td>
		<td><select name="friendly" style="width: 125px;" id="friendly">
			<option value="">Select</option>
			<option value="0"
			<?php if($res->disable_friendly=='1'){echo 'selected';}  ?>>Yes</option>
			<option value="1"
			<?php if($res->disable_friendly=='0'){echo 'selected';}  ?>>No</option>
		</select></td>
	</tr>
	<tr class="odd">
		<td style="font-size: 14px;" align="left"><strong>
		<div style="margin-left: 35px; font-size: 14px;">Bed or Living</div>
		</strong></td>
		<td><select name="bed" style="width: 125px;" id="bed">
			<option value="">Select</option>
			<option value="bed"
			<?php if($res->living_or_bed=='bed'){echo 'selected';}  ?>>Bed</option>
			<option value="living"
			<?php if($res->living_or_bed=='living'){echo 'selected';}  ?>>Living</option>
		</select></td>

	</tr>
	<tr class="even" style="margin-top: 10px;">
		<td align="center"><!--                <input style="margin-top:40px" type="submit" name="add" value="Save" id="sub" onclick="return chk();"/>-->
		<div
			style="height: 32px; vertical-align: middle; float: right; margin-right: 10px; margin-top: 10px;">
		<button type="submit" class="primary btn" name="add" id="sub"
			onclick="return chk();">
		<div><b>Save</b></div>
		</button>
		</div>
		</td>
		<td>
		<div
			style="height: 32px; vertical-align: middle; margin-top: 10px; float: left;">
		<button type="button" class="primary btn"
			onclick="window.location.href='<?php echo base_url();?>room/room_list'">
		<div><b>Cancel</b></div>
		</button>
		</div>



		</td>
	</tr>
</table>
</form>
</div>
</div>


			<?php }} ?></center>
