<script language="javascript" type="text/javascript">
    $(function(){
        $("#edit-submit").click(function(){
            
            if($("#edit-name").val() == "") {
                jAlert('<label class="error">Username is required field.</label>', 'Error');
                $("#edit-name").focus();
                return false;
            }

            if($("#edit-email").val() == "") {
                jAlert('<label class="error">Email is required field.</label>', 'Error');
                $("#edit-name").focus();
                return false;
            }

            <?php if(isset($uid)){?>
            if($("#edit-password").val() !="") {
                var pass = $("#edit-password").val();
                if(pass.length < 6) {
                    jAlert('<label class="error">Password should have atleast 6 characters long.</label>', 'Error');
                    $("#edit-password").focus();
                    return false;
                }

                var cpass = $("#edit-retype-password").val();
                if(pass != cpass) {
                    jAlert('<label class="error">Password and Confirm Password should be same.</label>', 'Error');
                    $("#edit-password").focus();
                    return false;
                }
            }
            <?php } else {?>
            if($("#edit-password").val()=="") {
                jAlert('<label class="error">Password is required field.</label>', 'Error');
                $("#edit-password").focus();
                return false;
            }

            var pass = $("#edit-password").val();
            if(pass.length < 6) {
                jAlert('<label class="error">Password should have atleast 6 characters long.</label>', 'Error');
                $("#edit-password").focus();
                return false;
            }

            var cpass = $("#edit-retype-password").val();
            if(pass != cpass) {
                jAlert('<label class="error">Password and Confirm Password should be same.</label>', 'Error');
                $("#edit-password").focus();
                return false;
            }
            <?php }?>
        })
    })
</script>

            <?php if(isset($page_heading)) {?>

<div class="pageheading"><?php echo $page_heading; if(isset($last_access)) echo " (<label>".$last_access."</label>)";?></div>
<br />
            <?php }?>

<table cellpadding="0" cellspacing="0" width="100%" class="pagecontent">
	<tr>
		<td><?php echo $form_open; ?>
		<table cellpadding="5" cellspacing="0" width="50%" class="border_tbl"
			align="center">
			<!--    <tr><td class="pageheading">&nbsp;</td></tr>-->
			<tr>
				<td colspan="2" class="pageheading">Profile</td>
			</tr>
			<tr>
				<td><strong>Username:</strong></td>
				<td><?php echo $username; ?></td>
			</tr>
			<tr>
				<td><strong>Email:</strong></td>
				<td><?php echo $email; ?></td>
			</tr>
			<tr>
				<td><strong>Department:</strong></td>
				<td><?php echo $departments; ?></td>
			</tr>
			<tr>
				<td><strong>Password:</strong></td>
				<td><?php echo $password; ?></td>
			</tr>
			<tr>
				<td><strong>Confirm Password:</strong></td>
				<td><?php echo $retype_password; ?></td>
			</tr>
			<tr>
				<td valign="top"><strong>Status:</strong></td>
				<td><?php echo $status; ?> Block <br />
				<?php echo $status2; ?> Active</td>
			</tr>
			<tr>
				<td valign="top"><strong>Roles:</strong></td>
				<td><?php echo $roles;?></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><?php echo " ".$form_id." "; if(isset($uid)){echo $uid;} ?>
				<div style="width: 40%; float: left;">&nbsp;</div>
				<div
					style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
				<button id="edit-submit" class="primary btn" type="submit">
				<div><b> <?php if(isset($uid)){ echo "Save"; ?> <?php }else {echo "Add";}?></b></div>
				</button>

				</div>
				<div
					onclick="window.location.href='<?php echo base_url(); ?>admin/users'"
					style="height: 32px; vertical-align: middle; float: left;">
				<button id="navbar-cancel-button" class="primary btn" type="button">
				<div><b>Cancel</b></div>
				</button>
				</div>
				</td>
			</tr>
		</table>
		<?php echo $form_close; ?></td>
		<?php if(isset($uid)) {?>
		<td valign="top">
		<table cellpadding="0" cellspacing="0" class="border_tbl">
			<tr>
				<td class="pageheading">Activity log module wise</td>
			</tr>
			<tr>
				<td><?php if($graph_str != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/FCF_Pie2D.swf", "chart1", $graph_str, "FactorySum", 300, 250);} else {echo "Data not found.!";}?></td>
			</tr>
			<tr>
				<td align="center"><?php echo $full_details; ?></td>
			</tr>
		</table>
		</td>
		<?php } ?>
	</tr>
</table>
