

<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>jhtmlarea/style/jqueryui/ui-lightness/jquery-ui-1.7.2.custom.css" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>jhtmlarea/scripts/jHtmlArea-0.7.0.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>jhtmlarea/style/jHtmlArea.css" />

<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>jhtmlarea/jquery-colorpicker.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>jhtmlarea/color-picker.css" />
<script language="javascript" type="text/javascript">
    $(function(){

         $("#edit_page_id").change(function(){
            var page_id = $(this).val();
            //alert(page_id);
            if(page_id > 0) {
                ajax_call('service_dir/get_page/'+page_id, 'page_id='+page_id, $('#content_container'), null);
            }
        })


        

        $("#navbar-submit-button").click(function(){

//            if($("#edit-type").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select category.</div>');
//                $("#edit-type").focus();
//                return false;
//            }
//
//            if($("#edit-ctype").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select type.</div>');
//                $("#edit-ctype").focus();
//                return false;
//            }
//
//            if($("#edit-annexure").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select annexure.</div>');
//                $("#edit-annexure").focus();
//                return false;
//            }

            if($("#edit-title").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter letter code.</div>');
                $("#edit-title").focus();
                return false;
            }

            if($("#edit-name").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter name of letter.</div>');
                $("#edit-name").focus();
                return false;
            }

            var clen = $('#edit-content').htmlarea('toHtmlString');
            if(clen == "" || clen=="<br>") {
                $("#page_message").html('<div class="pagemsg error">Please enter body content.</div>');
                $("#edit_content").focus();
                return false;
            }

            return true;

        })


        $("#edit_content").htmlarea({
                toolbar: [
                    ['html'],["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                    ["increasefontsize", "decreasefontsize"],
                    ["orderedlist", "unorderedlist"],
                    ["justifyleft", "justifycenter", "justifyright"],
                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                    ["cut", "copy", "paste"]
                ]

        });
    })


    function ajax_call(path, param, res_obj, isVal) {
        $.ajax({
            url:'<?php echo base_url(); ?>'+path,
            type:'POST',
            data:param,
            success:function(msg){
                $(res_obj).html(msg);
                $("#edit_content").htmlarea({
                    toolbar: [
                        ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                        ["increasefontsize", "decreasefontsize"],
                        ["orderedlist", "unorderedlist"],
                        ["justifyleft", "justifycenter", "justifyright"],
                        ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                        ["cut", "copy", "paste"]
                    ]

                });
            }
        })
    }

</script>
<?php echo $form_open; ?>
<table cellpadding="2" cellspacing="2" width="98%" align="center"
	class="pageform border_tbl">
	<tr>
		<td><label>Sections:</label></td>
		<td><?php echo $page_id; ?></td>
	</tr>
	<tr>
		<td valign="top"><label>Content:</label></td>
		<td id="content_container"><?php echo $content; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button type="submit" class="primary btn" id="navbar-submit-button">
		<div>Save</div>
		</button>
		</div>
		</td>
	</tr>
</table>
<?php echo $form_close; ?>