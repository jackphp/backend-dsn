

<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jqueryui/ui-lightness/jquery-ui-1.7.2.custom.css" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/scripts/jHtmlArea-0.7.0.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/style/jHtmlArea.css" />

<script
	type="text/javascript"
	src="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/jquery-colorpicker.js"></script>
<link
	rel="Stylesheet" type="text/css"
	href="<?php echo get_assets_path('js'); ?>tv_channel/jhtmlarea/color-picker.css" />
<script language="javascript" type="text/javascript">
    $(function(){
         $('#edit_content').attr('disabled',true);
         $('div').find('input, textarea,h3').attr('disabled','disabled');


         $("#edit_page_id").change(function(){
            var page_id = $(this).val();
    
            //alert(page_id);
            if(page_id > 0) {
                ajax_call('service_dir/get_page/'+page_id, 'page_id='+page_id, $('#content_container'), null);
            }
        })


        

        $("#navbar-submit-button").click(function(){

//            if($("#edit-type").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select category.</div>');
//                $("#edit-type").focus();
//                return false;
//            }
//
//            if($("#edit-ctype").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select type.</div>');
//                $("#edit-ctype").focus();
//                return false;
//            }
//
//            if($("#edit-annexure").val() == "") {
//                $("#page_message").html('<div class="pagemsg error">Please select annexure.</div>');
//                $("#edit-annexure").focus();
//                return false;
//            }

            if($("#edit-title").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter letter code.</div>');
                $("#edit-title").focus();
                return false;
            }

            if($("#edit-name").val() == "") {
                $("#page_message").html('<div class="pagemsg error">Please enter name of letter.</div>');
                $("#edit-name").focus();
                return false;
            }

            var clen = $('#edit-content').htmlarea('toHtmlString');
            if(clen == "" || clen=="<br>") {
                $("#page_message").html('<div class="pagemsg error">Please enter body content.</div>');
                $("#edit_content").focus();
                return false;
            }

            return true;

        })


        $("#edit_content").htmlarea({
                toolbar: [
                    ['html'],["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                    ["increasefontsize", "decreasefontsize"],
                    ["orderedlist", "unorderedlist"],
                    ["justifyleft", "justifycenter", "justifyright"],
                    ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                    ["cut", "copy", "paste"]
                ]

        });
    })


    function ajax_call(path, param, res_obj, isVal) {
        $.ajax({
            url:'<?php echo base_url(); ?>'+path,
            type:'POST',
            data:param,
            success:function(msg){
                $(res_obj).html($(msg).val());
		
                /*$("#edit_content").htmlarea({
                    toolbar: [
                        ["bold", "italic", "underline", "forecolor", "strikethrough", "|", "subscript", "superscript"],
                        ["increasefontsize", "decreasefontsize"],
                        ["orderedlist", "unorderedlist"],
                        ["justifyleft", "justifycenter", "justifyright"],
                        ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                        ["cut", "copy", "paste"]
                    ]

                });*/
            }
        })
    }

</script>
<style>
iframe {
	background: #FFFFFF;
	border: 1px solid !important;
	display: inline-table !important;
	width: 525px !important;
}

.ToolBar {
	display: none !important
}
</style>
<style>
.pageform input {
	border: medium none;
	border-radius: 3px 0 0 3px;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
	font: 14px Verdana, Arial, sans-serif;
	height: 28px;
	padding: 5px 8px;
}

.border_tbl {
	border: #E8E8E8 1px solid;
	font-size: 12px;
	-moz-border-radius: 4px /*{cornerRadius}*/;
	-webkit-border-radius: 4px /*{cornerRadius}*/;
	border-radius: 4px /*{cornerRadius}*/;
}

.popup {
	background: none repeat scroll 0 0 #FFFFFF;
	border-radius: 10px 10px 10px 10px;
	box-shadow: 0 2px 10px rgba(0, 0, 0, 0.5);
	display: none;
	padding: 0;
	position: absolute;
	z-index: 200;
}

.border_tbl {
	border: 1px solid #E8E8E8;
	border-radius: 4px 4px 4px 4px;
	font-size: 12px;
}

#tbl tr td {
	height: 40
}

#tbl label {
	font-size: 14px;
	font-weight: bold
}
</style>
<?php echo $form_open; ?>
<div style="padding-top: 0px">
<table cellpadding="2" cellspacing="2" id="tbl" width="68%"
	align="center" style="padding: 30px; margin-top: 30px;"
	class="pageform border_tbl">
	<tr>
		<td style="padding-left: 50px; padding-top: 55px"><label>Sections:</label></td>
		<td style="padding-top: 50px"><?php echo $page_id; ?></td>
	</tr>
	<tr>
		<td valign="top" style="padding-top: 10px; padding-left: 50px;"><label>Content:</label></td>
		<td style="padding-top: 10px; readonly: true" id="content_container">
		<?php echo $content; ?></td>
	</tr>
	<!--    <tr>
        <td></td>
        <td>
            <div style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;padding-top:10px;padding-bottom:12px">
                <button type="submit" class="button grey-gradient glossy" id="navbar-submit-button"><div>Save</div></button>
             </div>
        </td>
    </tr> -->
</table>
</div>
		<?php echo $form_close; ?>
