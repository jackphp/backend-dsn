<script language="javascript" type="text/javascript">
    $(function(){
        var tmp_width = ($("#logincontainer").width()/2);
        var page_width = ($("#container").width()/2);
        var popup_margine = (page_width-tmp_width);
        $("#logincontainer").fadeIn(2000);
        $("#edit-name").focus();

        $("#navbar-submit2-button").click(function(){
            if($("#edit-name").val()=="") {
                jAlert('<label class="error">Username is required field.</label>', 'Error');
                $("#edit-name").focus();
                return false;
            }

            if($("#edit-password").val()=="") {
                jAlert('<label class="error">Password is required field.</label>', 'Error');
                $("#edit-password").focus();
                return false;
            }

            
            $.ajax({
                url:"<?php echo base_url(); ?>login/authentication",
                type:"post",
                data:"name="+$("#edit-name").val()+"&password="+$("#edit-password").val(),
                success:function(msg){
                    if(msg==0) {
                        jAlert('<label class="error">Invalid username or password.</label>', 'Error');
                        reset_form();
                    }
                    else if(msg == 2) {
                        jAlert('<label class="error">You are already loggedin from other system, please logout first.</label>', 'Error');
                        reset_form();
                    }
                    else {
                        $.cookie('selectedMenu', -1, { expires : 2, path: '/' });
                        window.location.href="<?php echo base_url(); ?>home";
                    }
                }
            });

            return false;
        })
    })

    function reset_form() {
        $("#edit-name").val("");
        $("#edit-password").val("");
        $("#edit-name").focus();
    }
</script>

<style type="text/css">
.logintext {
	display: -moz-inline-stack;
	display: inline-block;
	width: 120px;
	height: 36px;
	vertical-align: text-middle;
	text-align: center;
	color: #282828;
	font-family: DejaVuSansCondensed;
	font-size: 42px;
	font-weight: bold;
	font-style: normal;
	text-shadow: #dddddd 0px 1px 0;
}
</style>

<p><br />
<br />
</p>
<p><br />
<br />
</p>
<center>
<div id="logincontainer" style="display: none; width:400px; height:400px; background:url(<?php echo get_assets_path('image').'loginbg.png'; ?>)">
<div style="text-align: left;"><?php echo $form_open; ?>
<table cellpadding="2" cellspacing="2" width="100%"
	class="pagecontent pageform" align="center" style="margin-left: 50px;">
	<!--<tr>
       <td>
        <?php if(isset($page_heading)) {?>
            <div class="pageheading" style="margin-top: 25px; width: 270px;"><img src="<?php echo get_assets_path('image').'/2c_logoff.png';?>" style="width: 25px; height: 25px;"/>&nbsp;&nbsp;<?php echo $page_heading; ?></div>
        <?php }?>
        </td>
    </tr>-->
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<!--  <td style="padding-left: 35px;"> <h3 class="logintext">Digi<span style="color:#FF0033;">Valet</span></h3> <img src="<?php //echo get_assets_path('image'); ?>digilogo.png" /></td>-->
		<td style="padding-left: 36px;"><img
			src="<?php echo get_assets_path('image'); ?>digivalet icon2.png" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $name; ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?php echo $password; ?></td>
	</tr>
	<tr>
		<td align="center">
		<div style="height: 45px; vertical-align: middle; float: left;">
		<button type="submit" class="primary btn" id="navbar-submit2-button"><img
			src="<?php echo get_assets_path('image').'loginbtn.png'; ?>" /></button>
			<?php echo " &nbsp;"; //$forgot_password; ?></div>
		</td>
	</tr>
</table>
			<?php echo $form_close; ?></div>
</div>
</center>
