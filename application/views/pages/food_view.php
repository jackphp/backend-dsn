<!-- Main container Satrt -->
<link
	href="<?php echo get_assets_path('js'); ?>jquery-ui-1.8.5/themes/base/jquery-ui-custom.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery-ui.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.datepicker.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.ui.widget.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-1.8.5/ui/jquery.effects.slide.js"></script>
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript">
	$(function() {
            $("#edit-from-date").datetimepicker({dateFormat:"yy-mm-dd", timeFormat:"hh:mm:ss"});
            $("#edit-to-date").datetimepicker({dateFormat:"yy-mm-dd", timeFormat:"hh:mm:ss"});
	});
	</script>

<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<br />
<?php }?>

<?php echo $form_open; ?>
<table align="center" cellpadding="2" cellspacing="2" width="50%">

	<tr>
		<td colspan="2"><strong>Generate Reports:</strong></td>
	</tr>
	<tr>
		<td colspan="2">Select Room: &nbsp;&nbsp; <?php echo $rooms;?></td>
	</tr>
	<tr>
		<td>From date:&nbsp;&nbsp;<?php echo $from_date;?></td>
		<td>To date:&nbsp;&nbsp;<?php echo $to_date;?></td>
	</tr>
	<tr>
		<td><?php echo $submit;?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2"></td>
	</tr>
</table>
<?php echo $form_close; ?>

<table cellpadding="0" cellspacing="0" width="50%" class="border_tbl"
	align="center">

	<tr>
		<td class="pageheading"><?php echo $subheading; ?></td>
	</tr>

	<tr>
		<td align="center"><?php if($graph_str != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/FCF_Column2D.swf", "", $graph_str, "FactorySum", 750, 370);} else {echo "Data not found.!";}?>
		</td>
	</tr>
</table>
<br />
<br />
