<link
	href="<?php  echo get_assets_path('css'); ?>jquery.ptTimeSelect.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery.ptTimeSelect.js"></script>
<link
	href="<?php  echo get_assets_path('js'); ?>jquery.multiSelct/jquery.multiSelect.css"
	type="text/css" rel="stylesheet" />
<script
	type="text/javascript"
	src="<?php echo get_assets_path('js');?>jquery.multiSelct/jquery.multiSelect.js"></script>
<script language="javascript" type="text/javascript">

    function set_multiselect() {
        $(".addons_master").multiSelect();
    }

    function addmore_images() {
        var len = $(".image_container tr").length;
        var imgname = len+1;
        var row = '<tr><td width="215"><b>Image '+imgname+':</b></td><td><input type="file" value="" name="'+imgname+'"></td></tr>';
        $(".image_container").append(row);
    }

    $(function(){
        addmore_images();
        $("#add_more_image").click(function(){
            addmore_images();
        })

        $("#addon_group_list tbody").sortable({
            handle: ".handle",
            cursor:     'move',
            axis:       'y',
            //items: 'tr:not(.ui-state-disabled)',
            update: function(e, ui) {
                $(this).sortable("refresh");
                var sorted = '';
                sorted = $("#addon_group_list tr").map(function() { return $(this).attr("id"); }).get();
                $("#groupids").val(sorted);
                //alert(sorted);
            }

        }).disableSelection();

        setTimeout("set_multiselect()", 50);

        setTimeout("set_checkbox_name()", 300);

        $("#add_more_addon").click(function(){
            $.ajax({
                url: "<?php echo base_url(); ?>menuitem/get_addon_row",
                type:"POST",
                success:function(msg) {
                    $("#addon_group_list").append(msg);
                    $(".addons_master").multiSelect()
                }
            })            
        })

        $("#edit-addns_id").attr("multiple", "multiple");
        $("#edit-allowed-addns_id").attr("multiple", "multiple");
        if($("#edit-has_addons").attr("checked") == true) {
            $("#product_list").show();
            $("#addon_list").show();
            //$("#allowed_addon_list").show();
        }
        
        
         if($("#edit-has_addons").attr("checked") == true) {
            $("#product_list").show();
            $("#addon_list").show();
            //$("#allowed_addon_list").show();
        }
        
        
        $("#edit-sid").change(function(){
            var param = 'name=section&val='+$(this).val();
            var path = 'menuitem/selectdata';
            make_call(path, param, 'category');
        });
     
        $("#edit-has_addons").click(function(){
            if($(this).attr("checked") == true) {
                $("#product_list").show();
                $("#addon_list").show();
                //$("#allowed_addon_list").show();
            }
            else {
                $("#product_list").hide();
                $("#addon_list").hide();
                //$("#allowed_addon_list").hide();
            }
        });
         
         
        $("#time").change(function(){
            
             //alert(this.value);
            if(this.value == 1) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        });
        
        $("#time2").change(function(){
            
             //alert(this.value);
            if(this.value == 1) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        });
         
         $("#time33").click(function(){
            if($(this).attr("checked") == true) {
                $("#sample2").show();
            }
            else {
                $("#sample2").hide();
            }
        });
         
         
        $("#form1").submit(function(){    
          if($('#time').attr("checked") == true) {

           if($('#from1').val()=='' || $('#to1').val()=='') {
              alert('Please Select the Time');
              return false;

           }
        }
            
            var my = new Array();
            var cg='';
            var sorted = '';
            sorted = $("#addon_group_list tr").map(function() { return $(this).attr("id"); }).get();
            $("#groupids").val(sorted);
            
            if($("#edit-size_price option").length < 1) {
                alert("Please enter aleast one price and size.");
                return false;
            }
            else {
                $("#edit-size_price option").attr("selected", "selected");
            }
//            var i=0;
//            $(".from_time").each(function() {
//              i++;
//              my[i]=$this.val();
//             // alert(my[i])
//             if($(this).val()=='') {
//             alert('Please Select the Time');
//             cg=1;
//             
//             }
//            
//            
//         }); 
//          if(cg==1){
//                 return false;
//             }
        });
         
        
         
        
        
        $("#edit-size_price option").dblclick(function(){
            $(this).remove();
        });

        $("#add_price").click(function(){
            var price = $("#edit-price").val();
            var size = $("#edit-size").val();
            var uom = $("#edit-uom").val();
            var oldhtml = $("#edit-size_price").html();

            var flag = 0;
            $("#edit-size_price option").each(function(i){
                var arr = $(this).val().split('-');
                if(size == arr[0] && price == arr[1] && uom == arr[2]) {
                   flag = 1;
                }
            });

            if(flag) {
                alert('You have already added this criteria.');
                return false;
            }

            if(size == "") {
                alert("Please select size.");
                return false;
            }

            if(price == "") {
                alert("Please enter price.");
                return false;
            }
            
            var opt = '<option value="'+size+'-'+price+'-'+uom+'" ondblclick="$(this).remove()">'+size+'-'+price+'-'+uom+'</option>';
            opt +=oldhtml;
            $("#edit-size_price").html(opt);
        });


//        <?php //if($mid > 0) { ?>
//            $.ajax({
//               url: '<?php //echo base_url(); ?>menuitem/show_groups/<?php //echo $mid; ?>',
//               type: 'POST',
//               //data: param,
//               success:function(msg){
//                   var ret = msg;
//                   if(ret.indexOf(',') > 0) {
//                       var gArr = ret.split(',');
//                       for(var i=0; i < gArr.length; i++) {
//                           var gid = gArr[i];
//                            $("#a"+gid).attr("checked", "checked");
//                            $("#addon"+gid).show();
//                       }
//                   }
//                   else {
//                      $("#a"+msg).attr("checked", "checked");
//                      $("#addon"+msg).show();
//                   }
//               }
//            });
//        <?php //}?>

    });

    function group_onchange(obj) {
        $.ajax({
            url: "<?php echo base_url(); ?>menuitem/get_group_addons",
            type:"POST",
            data:"gid="+$(obj).val(),
            success:function(msg){
                $(obj).parent("td").parent("tr").children("td").eq(2).html(msg);
                $(".addons_master").multiSelect();
                $(".multiSelectOptions").children("label").children(" :checkbox").each(function(){
                    $(this).attr("name", "addons_master[]");
                })
            }
        })
    }

    function set_checkbox_name() {
        $(".multiSelectOptions").children("label").children(" :checkbox").each(function(){
            $(this).attr("name", "addons_master[]");
        })
    }

    function get_sub_cat(cid) {
        var param = 'name=cat&val='+cid;
        var path = 'menuitem/selectdata';
        make_call(path, param, 'subcat');
    }

    function remove_image(pos, mid) {
        if(confirm('Are you sure to delete image.!')) {
            var path = 'menuitem/removefood_image';
            var param = 'pos='+pos+'&mid='+mid;
            make_call(path, param, undefined);
            $("#imgfield_"+pos).show();
            $("#imgtext_"+pos).hide();
        }
    }
   
  function c(){
            var total=0;
            var a=new Array();
	a=document.getElementsByName("graduate[]");
            for(var i=0; i < a.length; i++){
    if(a.location[i].checked) {
      total++;
    }
  }
  if( total == 0 ) {
    return false;
  } else {
    return true;
  }
}
        
            
   function check(id){
	var a=new Array();
	a=document.getElementsByName("addons_master[]");
	//alert("Length:"+a.length);
       
	var p=0;
	for(i=0;i<a.length;i++){
		if(a[i].checked){
			//alert(a[i].value);
			p=1;
		}
	}
	if (p==0){
		alert('please select at least one check box');
		return false;
	}
			
	
	return true;
   }
	
      
    
    function make_call(path, param, id) {
        $.ajax({
           url: '<?php echo base_url(); ?>'+path,
           type: 'POST',
           data: param,
           success:function(msg){
               $('#'+id).html(msg);
           }
        });
    }
         
         
        function add_mo() { 
        $.ajax({ 
           url: '<?php echo base_url(); ?>menuitem/add_more',
           type: 'POST',
//           data: param,
           success:function(msg){
               $('#ad').append(msg);
           }
        });
    }  
       
         function dele(t,id) { 
            $(t).parent().remove();
//        window.location='<?php echo base_url(); ?>menuitem/delete_time/'+id;

    }  
         
   function call(id,val) {
        var mid = <?php echo $mid; ?>;

        if(val) {}else {
            var obj = $("#addon"+id).children("table").children("tbody").children("tr").children("td").children("div");
            $(obj).children("input:checkbox").removeAttr("checked");
        }

        $.ajax({
           url: '<?php echo base_url(); ?>menuitem/sub/'+id,
           type: 'POST',
           data: 'id='+id+'&mid='+mid,
           success:function(msg){
               if(val==false){
                 $('#addon'+id).hide();
               }
               else{
                $('#addon'+id).show();
                $('#addon'+id).html(msg);
               }
           }
         });
    }
</script>
<script language="JavaScript">
    $(function(){
        $('#sample2 input').ptTimeSelect({
            popupImage: ''
        });

        $('code').each(
            function() {
                eval($(this).html());
            }
        )
    })
</script>
<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<?php }?>
<div class="pagecontent">&nbsp;&nbsp;<?php if(isset($add_link)) echo $add_link; ?></div>
<?php echo $form_open; ?>
<input
	type="hidden" name="groupids" id="groupids" />
<table cellpadding="2" cellspacing="2" width="80%"
	class="border_tbl pageform" align="center">
	<tr>
		<td valign="top"><b>Revenue center:</b></td>
		<td><?php echo $rvc; ?></td>
	</tr>
	<tr>
		<td><b>Number:</b></td>
		<td><?php echo $number; ?> <label>(it is unque number for POS)</label></td>
	</tr>
	<tr>
		<td><b>Name:</b></td>
		<td><?php echo $name; ?> <label>(it is unque name for POS)</label></td>
	</tr>
	<?php foreach($descriptive_name as $n=>$field) {?>
	<tr>
		<td valign="top"><b>Display Name in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>
	<?php foreach($synopsis as $n=>$field) {?>
	<tr>
		<td valign="top"><b>Synopsis in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>

	<tr>
		<td colspan="2">
		<fieldset class="border_tbl"><legend
			style="font-size: 14px; font-weight: bold;"><em>Price Details</em></legend>
		<table cellpadding="2" cellspacing="2" width="80%"
			style="font-size: 12px;">

			<tr>
				<td valign="top"><b>Size:</b></td>
				<td><?php echo $size; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Price:</b></td>
				<td><?php echo $price; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>UOM:</b></td>
				<td><?php echo $uom; ?><br />
				<label>(If menuitem has not unit of measurement then <em>NULL</em>
				will be the default value)</label></td>
			</tr>
			<tr>
				<td valign="top"></td>
				<td><?php //echo $add_button; ?>
				<div
					style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
				<button class="primary btn" type="button" id="add_price"
					name="add_price">Add Price</button>
				</div>
				</td>
			</tr>
			<tr>
				<td valign="top"><b>Slected Criteria:</b></td>
				<td><?php echo $size_price; ?></td>
			</tr>
		</table>
		</fieldset>
		</td>
	</tr>

	<tr>
		<td valign="top"><b>Has Addons:</b></td>
		<td><?php echo $has_addons; ?></td>
	</tr>
	<tr id="product_list" style="display: none;">
		<!--<td valign="top">
            <b>Select Addon Group:</b>
        </td>-->
		<td colspan="2">
		<div>
		<table cellpadding="2" cellspacing="2" style="font-size: 10px;"
			id="addon_group_list">
			<?php if(count($menuitem_addon_row) > 0) {
				$i=0;
				foreach($menuitem_addon_row as $row) {
					?>
			<tr>
				<td>
				<div class="handle"><img class="tabledrag-handle"
					style="cursor: move;" src="/foodmenu/images/arrow1.png"
					title="Drag to re-order" href="#"></div>
				</td>
				<td><?php echo $row['groups']; ?></td>
				<td><?php echo $row['addons'];?></td>
				<td width="20"><?php if($i > 0) {?><a href="#"
					onclick="$(this).parent().parent().remove(); return false;">[X]</a><?php }?>
				&nbsp;</td>
			</tr>
			<?php $i++;}} else {?>
			<tr>
				<td>
				<div class="handle"><img class="tabledrag-handle"
					style="cursor: move;" src="/foodmenu/images/arrow1.png"
					title="Drag to re-order" href="#"></div>
				</td>
				<td><?php echo $groups; ?></td>
				<td><?php echo $group_addons;?></td>
				<td width="20">&nbsp;</td>
			</tr>
			<?php }?>
		</table>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button class="primary btn" type="button" id="add_more_addon"
			name="add_more_addon">Add More</button>
		</div>
		</div>
		</td>
	</tr>

	<tr id="allowed_addon_list" style="display: none;">
		<td valign="top"><b>Allowed Addon Group:</b></td>
		<td><?php echo $allowed_addons; ?></td>
	</tr>
	<tr id="addon_list" style="display: none;">
		<td valign="top" style="display: none"><b>Is Addon Required:</b></td>
		<td style="display: none"><?php echo $required_addon; ?></td>
	</tr>
	<tr>
		<td colspan="2">
		<table cellpadding="2" cellspacing="2" width="50%">
			<tr>
				<td valign="top" width="215"><b>Is Chef's Special:</b></td>
				<td><?php echo $special; ?></td>
				<td valign="top" align="right"><b>Has Pork:</b></td>
				<td><?php echo $porch; ?></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<table cellpadding="2" cellspacing="2" width="50%">
			<tr>
				<td width="215"><b>Is Veg: </b></td>
				<td><?php echo $is_vag; ?></td>
				<td valign="top" align="right"><b>In Breakfast:</b></td>
				<td><?php echo $has_breakfast; ?></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td><b>Spicy Level</b></td>
		<td><?php echo $spicy_level; ?></td>
	</tr>

	<tr>
		<td valign="top"><b>Availability</b></td>
		<td><?php echo $time2;?><label>All</label> <?php echo $time1; ?><label>Restricted
		Time</label>
		<div id="sample2" <?php if(!empty($time)){  }  else { ?>
			style="display: none" <?php } ?>>
		<form onsubmit="return void(0);">From <input name="from1"
		<?php if (isset($time['from_time'])){?>
			value="<?php echo $time['from_time']; ?>" <?php }  else { ?> value=""
			<?php } ?> size="6" id="from1" /> To <input id="to1" name="to1"
			<?php if (isset($time['from_time'])){?>
			value="<?php echo $time['to_time']; ?>" <?php } else { ?> value=""
			<?php } ?> size="6" /></form>
		</div>
		</td>
	</tr>
	<tr>
		<td valign="top"><b>Section:</b></td>
		<td><?php echo $sid; ?></td>
	</tr>
	<tr>
		<td valign="top"><b>Main Category:</b></td>
		<td id="category"><?php echo $cid; ?></td>
	</tr>
	<tr>
		<td valign="top"><b>Subcategory:</b></td>
		<td id="subcat"><?php echo $sub_cat_id; ?></td>
	</tr>

	<tr>
		<td><b>Video:</b></td>
		<td><?php echo $video; if(isset($selected_video) && $selected_video != "") {echo " Current Video: &nbsp;".anchor(ASSETS_PATH.FOOD_VIDEOS.$selected_video, $selected_video, array('target'=>'_blank'));}?></td>
	</tr>
	<tr>
		<td colspan="2">
		<table cellpadding="2" cellspacing="2" width="80%"
			class="image_container">
			<?php if(count($images) > 0) {
				$i=0;
				foreach ($images as $name=>$field) {
					if(isset($field['image']) && $field['image'] != "") {

						?>
			<tr>
				<td><b>Image <?php echo $name+1;?>:</b></td>
				<td><?php if(isset($field['image']) && $field['image'] != ""){ echo '<div id="imgfield_'.$field['pos'].'" style="float:left; display:none; margin-right:5px;">'.$field['field']."</div>";}else {echo $field['field'];} if($field['image'] != ""){ echo '<div id="imgtext_'.$field['pos'].'" style="float:left; margin-right:5px;">'."Current Image: &nbsp;".anchor(ASSETS_PATH.FOOD_IMAGES.$field['image'], $field['image'], array('target'=>'_blank')).' | <a href="#" onclick="remove_image('.$field['pos'].', '.$mid.')"><b>[Remove]</b></a> </div>'; }?>
				</td>
			</tr>
			<?php $i++;}} }?>
		</table>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button class="primary btn" type="button" id="add_more_image"
			name="add_more_image">Add More</button>
		</div>
		</td>
	</tr>
	<tr>
		<td><b>Active:</b></td>
		<td><?php echo $active; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div style="width: 40%; float: left;">&nbsp;</div>
		<div
			style="height: 32px; vertical-align: middle; float: left; margin-right: 10px;">
		<button id="navbar-submit-button" class="primary btn" type="submit">
		<div><?php if(isset($id)){ echo "Update"; ?> <?php }else {echo "Add";}?></div>
		</button>
		</div>
		&nbsp;&nbsp;
		<div
			onclick="window.location.href='<?php echo base_url(); ?>menuitem'"
			style="height: 32px; vertical-align: middle; float: left;">
		<button id="navbar-cancel-button" class="primary btn" type="button">
		<div>Cancel</div>
		</button>
		</div>

		<?php //echo $cancel; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
		<?php echo $form_id; if(isset($id)) echo $id; ?>
		<?php echo $form_close; ?>
<br />
<br />
