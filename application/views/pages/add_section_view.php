<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<br />
<?php }?>
<div class="pagecontent">&nbsp;&nbsp;<?php if(isset($add_link)) echo $add_link; ?></div>
<?php echo $form_open; ?>
<table cellpadding="2" cellspacing="2" width="50%" class="border_tbl"
	align="center">
	<tr>
		<td><b>Revenue:</b></td>
		<td><?php echo $rvc; ?></td>
	</tr>
	<tr>
		<td><b>Name:</b></td>
		<td><?php echo $name; ?></td>
	</tr>
	<?php foreach($display_name as $n=>$field) {?>
	<tr>
		<td><b>Section display name in <?php echo $languages[$n];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>

	<?php foreach($description as $name=>$field) {?>
	<tr>
		<td valign="top"><b>Description in <?php echo $languages[$name];?>:</b></td>
		<td><?php echo $field['field']; ?></td>
	</tr>
	<?php } ?>

	<?php if(count($images) > 0) {
		foreach ($images as $name=>$field) {
			?>
	<tr>
		<td><b>Image For <?php echo $name;?>:</b></td>
		<td><?php echo $field['field']; if($field['image'] != ""){ echo " Current Image: &nbsp;".anchor(ASSETS_PATH.FOOD_IMAGES.$field['image'], $field['image'], array('target'=>'_blank')); }?>
		</td>
	</tr>
	<?php } }?>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?php echo $submit; ?>&nbsp;&nbsp;<?php echo $cancel; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
	<?php echo $form_id; if(isset($id)) echo $id; ?>
	<?php echo $form_close; ?>
<br />
<br />
