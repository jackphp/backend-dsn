<script language="javascript" type="text/javascript">
    $(function(){
        $(".addon_form").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form").colorbox({width:"650", height:"450", iframe:true, href:'<?php echo base_url(); ?>food_allowed_addons/add/0/<?php echo $gid;?>',/*onClosed:function(){ location.reload(true); },*/ overlayClose:false,onComplete:function(){
                           var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
        
        
	$(".addon_form2").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form2").colorbox({width:"650", height:"450" ,/*onClosed:function(){ location.reload(true); },*/ iframe:true, overlayClose:false,onComplete:function(){
            var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
        
        
    })
  function color(){
     $(".addon_form2").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form2").colorbox({width:"650", height:"450" ,/*onClosed:function(){ location.reload(true); },*/ iframe:true, overlayClose:false,onComplete:function(){
            var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
   
}

function delete_cat(){
        $(".delete").click(function(){ 
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location; 
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                }) 
} 
</script>

<?php if(isset($page_heading)) {?>
<!--<div class="pageheading"><?php // echo $page_heading; ?></div>-->
<?php }?>
<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px; margin-bottom: 5px; margin-top: 45px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>group_allowed_addons'">
<div title="Back To Allowed Addon Group" class="with-tooltip"><label>Back</label></div>
</div>



<div class="button grey-gradient glossy"
	style="float: left; margin-bottom: 5px; margin-left: 5px; margin-top: 45px;"
	id="send_letter">
<div title="Add New Addon" class="with-tooltip"><label
	class="addon_form">Add Allowed Addon</label></div>
</div>
<div class="pagecontent">&nbsp;&nbsp;</div>

<div style="clear: both" id="listWraper">
<center><?php echo $links; ?></center>
<table cellpadding="2" cellspacing="2" width="98%" class="table"
	align="center">
	<thead>
		<tr class="listheading">
			<th align="center" width="10%" scope="col">#</th>
			<th align="center" width="20%" scope="col">Name</th>
			<th align="center" width="20%" scope="col">Code</th>
			<th align="center" width="20%" scope="col">Display Name</th>
			<th align="center" width="10%" scope="col">Price</th>
			<th align="center" width="20%" scope="col">Operations</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i=1;
	if(count($addons->result()) > 0) {
		foreach($addons->result() as $a) {
			if($i%2==0) {$class="even";} else {$class="odd";};
			?>
		<tr class="<?php echo $class; ?>">
			<td scope="row" width="10%" align="center"><?php echo $i; ?></td>
			<td scope="row" width="20%" align="left"><?php echo $a->addon_name; ?></td>
			<td scope="row" width="20%" align="center"><?php echo $a->addon_code; ?></td>
			<td scope="row" width="20%" align="left"><?php echo $a->display_name; ?></td>
			<td scope="row" width="10%" align="center"><?php echo $a->price; ?></td>
			<td scope="row" width="20%" align="center"><a
				href="<?php echo base_url().'food_allowed_addons/add/'.$a->addon_id;?>"
				class="addon_form2">Edit</a> | <a
				href="<?php echo base_url().'food_allowed_addons/delete_addon/'.$a->addon_id.'/'.$gid; ?>"
				class="delete">Delete</a></td>
		</tr>

		<?php
		$i++; }
	}
	else {
		echo '<tr><td colspan="6" scope="row" align="center"></td></tr><tr><td colspan="6" align="center">Records not available.</td></tr><tr><td colspan="6" align="center"></td></tr>';
	}
	?>
	</tbody>
</table>

<div>&nbsp;</div>
<center><?php echo $links; ?></center>
<div>&nbsp;</div>
</div>
