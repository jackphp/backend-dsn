<script language="javascript" type="text/javascript">
    $(document).ready(function() {
         //alert('dd');
//         $("#list").sortable({
//      handle : '.handle',
//      update : function () {
//		  var order = $('#list').sortable('serialize');
//  		$("#info").load("process-sortable.php?"+order);
//      }
//    });
    
        $("#list table tbody").sortable({
            handle: ".handle",
            cursor:     'move',
            axis:       'y',
            //items: 'tr:not(.ui-state-disabled)',
            update: function(e, ui) {
                href = '<?php echo base_url(); ?>food_category/sort';
                $(this).sortable("refresh");
                sorted = $("#list tr").map(function() { return $(this).attr("id"); }).get();
                
                $.ajax({
                        type:   'POST',
                        url:    href,
                        data:   'ids='+sorted,
                        success: function(msg) {
                               window.location.reload();
                        }
                });
            }
 
        }).disableSelection();
        });
        
        function check(id,val){
//        alert(val);
           url = "<?php echo base_url()?>food_category/update_cat_status/"+id+'/'+val;
              
                $.get(url,function(data){
                      window.location.reload();
                })     
                }

</script>
<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<?php }?>


<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 20px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>menuitem'">
<div tipval="Back To Menu Items" class="tooltip"><label>Back</label></div>
</div>

<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 5px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>food_category/add'">
<div tipval="Add New Category" class="tooltip"><label>Add Category</label></div>
</div>




<table
	cellpadding="2" cellspacing="2" width="98%" class="border_tbl"
	align="center">
	<?php if(isset($search)) {?>
	<tr>
		<td>
		<table width="100%" class="pagecontent border_tbl" bgcolor="#d3d3d3"
			cellpadding="2" cellspacing="2">
			<?php if(isset($pager)){?>
			<td align="right" width="20%"><?php echo $pager; ?></td>
			<?php }?>
			<td valign="middle" align="right"><?php echo $search['form_open'];?>
			<b>Search In:</b> <select name="search_term">
				<option value="">--Select--</option>
				<option value="name">Name</option>
				<option value="display_name">Display Name</option>
			</select> | <b>Search Value:</b> <input type="text" name="search_val"
				id="search_val" /> <input type="submit" name="submit" value="Go" />
				<?php echo $search['form_close'];?></td>
		</table>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td id="list"><?php if(isset($list)) echo $list;?></td>
	</tr>
	<?php if(isset($pager)){?>
	<tr>
		<td align="center" class="border_tbl" bgcolor="#d3d3d3" height="30"><?php echo $pager; ?></td>
	</tr>
	<?php }else {?>
	<!--    <tr><td align="center"><a href="#" onclick="javascript:history.go(-1);">Back</a></td></tr>-->
	<?php }?>
</table>
<br />
<br />
