<!-- Main container Satrt -->
<script language="javascript" type="text/javascript">
    $(function(){
        if($("#reporting").attr('checked') == true) {
            $("#report_path").show();
        }
        else {
            $("#report_path").hide();
        }

        $("#reporting").click(function(){
            if($(this).attr("checked") == true) {
                $("#report_path").show();
            }
            else {
                $("#report_path").hide();
            }
        });
    })
</script>
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<br />
<?php }?>
<div class="pagecontent">&nbsp;&nbsp;<?php if(isset($department_list_link)) echo $role_list_link; ?></div>
<?php echo $form_open; ?>
<table
	cellpadding="2" cellspacing="2" width="50%"
	class="pagecontent border_tbl" align="center">
	<!--    <tr><td class="pageheading">&nbsp;</td></tr>-->

	<tr>
		<td width="20%"><strong>Name:</strong></td>
		<td><?php echo $name; ?></td>
	</tr>
	<tr>
		<td><strong>URL:</strong></td>
		<td><?php echo $url; ?></td>
	</tr>
	<tr>
		<td><strong>Status:</strong></td>
		<td><?php echo $status; ?></td>
	</tr>
	<tr>
		<td><strong>Reporting:</strong></td>
		<td><?php echo $reporting; ?></td>
	</tr>
	<tr id="report_path">
		<td><strong>Reporting Path:</strong></td>
		<td><?php echo $reporting_path; ?></td>
	</tr>
	<tr>
		<td><strong>Permission:</strong></td>
		<td><?php echo $perm; ?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?php echo $submit." ".$form_id." "; if(isset($cancel)){echo $cancel;} if(isset($id)){echo $id.$menu_name;} ?>
		</td>
	</tr>
</table>
<?php echo $form_close; ?>
<br />
<br />
