
<style type="text/css">
ul {
	list-style-type: none;
}

.gallery ul {
	width: 100%;
	list-style-type: none;
	margin: 0px;
	padding: 0px;
}

.gallery li div {
	width: 100%;
	height: 100%;
	text-align: center;
	padding-top: 0px;
	padding-bottom: 0px;
}

.placeHolder div {
	background-color: white !important;
	border: dashed 1px gray !important;
}

.tip {
	color: #FFFFFF;
	padding: 5px 5px 5px 5px;
	background-color: #000000;
	border: #CCCCCC 1px solid;
	font-size: 14px;
	display: none;
	zindex: 999;
	position: absolute;
}

span {
	text-align: left;
}
</style>
<script
	src="<?php echo get_assets_path('js');?>jquery.dragsort-0.4.1.js"
	type="text/javascript"></script>
<script
	src="<?php echo get_assets_path('js');?>jquery.dragsort-0.4.1.min.js"
	type="text/javascript"></script>
<script language="javascript" type="text/javascript">
   
    $(function(){
        $(".addon_form").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form").colorbox({width:"650", height:"450", iframe:true, href:'<?php echo base_url(); ?>group_addons/add' ,onClosed:function(){ location.reload(true); }, overlayClose:false,onComplete:function(){
                $("#colorbox").css("top", t+"px");
            }});
        })

	
	$(".addon_form2").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form2").colorbox({width:"650", height:"450", iframe:true ,onClosed:function(){ location.reload(true); }, overlayClose:false,onComplete:function(){
                $("#colorbox").css("top", t+"px");
            }});
        })
    })
    
   
   
  
          $(document).ready(function() {
         //alert('dd');
//         $("#list").sortable({
//      handle : '.handle',
//      update : function () {
//		  var order = $('#list').sortable('serialize');
//  		$("#info").load("process-sortable.php?"+order);
//      }
//    });
      $(".gallery").dragsort({ dragSelector: "div", dragEnd: saveSortOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>"});
            var isDrag = 0;
    
//        $("#list").sortable({
//            handle: ".handle",
//            cursor:     'move',
//            axis:       'y',
//            //items: 'tr:not(.ui-state-disabled)',
//            update: function(e, ui) {
//                href = '<?php echo base_url(); ?>menuitem/sort_group';
//                $(this).sortable("refresh");
//                sorted = $("#list tr").map(function() { return $(this).attr("id"); }).get();
//                
//                $.ajax({
//                        type:   'POST',
//                        url:    href,
//                        data:   'ids='+sorted,
//                        success: function(msg) {
//                               window.location.reload();
//                        }
//                });
//            }
// 
//        }).disableSelection();
        });
        
        function check(id,val){
//        alert(val);
           url = "<?php echo base_url()?>food_category/update_cat_status/"+id+'/'+val;
              
                $.get(url,function(data){
//                      window.location.reload();
                })     
                }
    
    function saveSortOrder() {
            //alert('rf');
            var data = $(".gallery li").map(function() { return $(this).attr("id"); }).get();
            //var cids = $("#sorted_id").val();
            //alert(data);

            $.post("<?php echo base_url(); ?>menuitem/sort_save", { "ids[]": data});
             window.setTimeout('location.reload()', 100);
            $("#msg").html("<center><label>Saving...</label></center>");
            //setTimeout("reload()", 4000);
        }
</script>
<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php echo $page_heading; ?></div>
<?php }?>
<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 13px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>menuitem'">
<div tipval="Back To Menu Items" class="tooltip"><label>Back</label></div>
</div>

<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 4px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter">
<div tipval="Add New Addon Family" class="tooltip addon_form"><label>Add
Addon Family</label></div>
</div>
<div class="pagecontent">&nbsp;&nbsp;</div>
<center><?php echo $links; ?></center>
<table style="margin-left: 12px;" cellpadding="2" cellspacing="2"
	width="99%" class="border_tbl" align="center">

	<tr width="95%" style="marggin-left: 15px;" class="listheading">
		<th width="61">#</th>
		<th width="181">Name</th>
		<th width="102">Code</th>
		<th width="180">Display Name</th>
		<th width="350">-</th>
	</tr>
</table>

<ul class="gallery">
<?php
$i=1;
foreach($addons->result() as $a) {
	if($i%2==0) {$class="even";} else {$class="odd";};
	?>

	<li id="<?php echo $a->gid; ?>" align="left">
	<div style="width: 98%; height: 100%; margin-left: 16px;"
		class="<?php echo $class; ?>">
	<table cellpadding="2" cellspacing="2" width="98%" border="0">
		<tr width="98%">
			<td width="63" style="border-right: solid #FFFFFF;" align="center">
			<div class="handle"><img class="tabledrag-handle" href="#"
				title="Drag to re-order"
				src="<?php echo get_assets_path('image').'arrow1.png'; ?>"
				style="cursor: move;" /></div>
			</td>
			<td width="194" style="border-right: solid #FFFFFF;" align="left"><label><?php echo $a->group_name; ?></label></td>
			<td width="108" style="border-right: solid #FFFFFF;" align="left"><label>&nbsp;<?php echo $a->group_code; ?></label></td>
			<td width="192" style="border-right: solid #FFFFFF;" align="left"><label><?php echo $a->display_name; ?></label></td>
			<td width="350" align="center"><a
				href="<?php echo base_url().'food_addons/addon_list/'.$a->gid; ?>">Add
			Addons</a> | <a
				href="<?php echo base_url().'group_addons/add/'.$a->gid; ?>"
				class="addon_form2">Edit</a> | <a
				href="<?php echo base_url().'group_addons/delete_addon/'.$a->gid; ?>"
				class="delete">Delete</a></td>
		</tr>

	</table>
	</div>
	</li>
	<?php
	$i++; }
	?>
</ul>


<div>&nbsp;</div>
<center><?php echo $links; ?></center>
<div>&nbsp;</div>
