<?php
/**
 * Description of revenue
 *
 * @author pbsl
 */
class revenue extends CI_Controller {
	var $data;
	var $logo;
	var $description;
	var $video;

	public function  __construct() {
		parent::__construct();

		$this->data['title'] = "Food Revenue Centers";
		$perm = array('revenue management');
		$this->user->set_access_permission($perm);
		$menu = array('menu'=>'Food Management',
                     'links'=>array(array('label'=>'Revenue Centers', 'path'=>'revenue', 'permission'=>'revenue management', 'menu_order'=>0),
		array('label'=>'Add Revenue', 'path'=>'revenue/add', 'permission'=>'revenue management', 'menu_order'=>1)
		));
		$this->menu->set_menu($menu);
		$this->load->library('food_library');
		$this->load->library('food_item_library');
		$this->load->model('food_application_model');
	}

	public function index($id=0) {
		if($this->user->is_user_access()) {
			$q = $this->food_application_model->get_revenue_list();
			$header = array(array('data'=>'#'), array('data'=>'Name'), array('data'=>'Number'), array('data'=>'Active'), array('data'=>'-'), 'class'=>'listheading');
			$rows = array();
			$i = 0;
			foreach($q->result() as $row) {
				$i++;
				if($i%2==0) {
					$class = "even";
				}
				else {
					$class = "odd";
				}

				if($row->is_active == 0) { $active = "Deactive"; } else { $active = "Active"; }

				$rows[] = array(
				array('data'=>$i, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('revenue/edit/'.$row->rvc_id, $row->rvc_name)),
				array('data'=>$row->rvc_number),
				array('data'=>$active, 'attributes'=>array('align'=>'center')),
				array('data'=>anchor('revenue/edit/'.$row->rvc_id, 'edit')." | ".anchor('revenue/delete/'.$row->rvc_id, 'delete', array('class'=>'delete')), 'attributes'=>array('align'=>'center')),
                         'attributes'=>array('class'=>$class));
			}

			$list = $this->theme->generate_list($header, $rows);
			$this->data['list'] = $list;
			$this->data['add_link'] = anchor('revenue/add', 'Add Revenue');
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/revenue_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function add($param = array()) {
		if($this->user->is_user_access()) {
			$this->data = $this->food_library->revenue_form();
			$this->data['title'] = 'Add Revenue Center';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_revenue_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}


	public function edit($rv_id = 0) {
		if($this->user->is_user_access()) {
			if($rv_id == 0) { redirect('revenue'); }

			$param = array();
			$q = $this->food_application_model->get_revenue_list($rv_id);
			foreach($q->result_array() as $row) {
				$param = $row;
			}
			$this->data = $this->food_library->revenue_form($param);
			$this->data['title'] = 'Update Revenue Center';
			$this->data['page_heading'] = $this->data['title'];
			$vars = $this->theme->theme_vars('pages/add_revenue_view', $this->data);
			$this->load->view(MAIN_PAGE_DIR.'main_page', $vars);
		}
		else {
			$this->user->user_access_denied();
		}
	}

	public function delete($id="") {
		if($id > 0) {
			if($this->food_application_model->delete_revenue($id)) {
				$this->message->set('Revenue successfully deleted.', 'success', TRUE);
				redirect('revenue');
			}
			else {
				$this->message->set('Error in revenue deletion.', 'error', TRUE);
				redirect('revenue');
			}
		}
	}

	public function submit() {
		$form_id = $this->input->post('form_id');
		switch ($form_id) {
			case "add_revenue":
				if(!$this->food_library->revenue_validate($_POST)) {
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');
					if(isset($videos['video']['file_name'])) {
						$details_param = array('description'=>$this->input->post('description'), 'video'=>$videos['video']['file_name']);
					}
					else {
						$details_param = array('description'=>$this->input->post('description'), 'video'=>'');
					}

					$primary_param = array('number'=>$this->input->post('number'), 'name'=>$this->input->post('name'), 'is_changed'=>1, 'modified'=>time(), 'created'=>time(), 'active'=>$this->input->post('active'));
					$param = array('primary'=>$primary_param, 'details'=>$details_param);

					$rv_id = $this->food_application_model->save_revenue($param);

					if($rv_id > 0) {
						$this->food_application_model->save_images($images, 'revenue', $rv_id);
						$this->message->set('Revenue successfully added.', 'success', TRUE);
						redirect('revenue');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in adding revenue.', 'error', TRUE);
						redirect('revenue/add');
					}
				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('revenue/add');
				}
				break;

			case "edit_revenue":
				if(!$this->food_library->revenue_validate($_POST)) {
					$images = $this->food_library->upload_files($_FILES); // In Images case the key will application ID (it is a name of image field)
					$videos = $this->food_library->upload_files(array('video'=>$_FILES['video']), 'video');
					//echo "<pre>"; print_r($videos); die();
					if(isset($videos['video']['file_name'])) {
						$details_param = array('description'=>$this->input->post('description'), 'video'=>$videos['video']['file_name']);
					}
					else {
						$details_param = array('description'=>$this->input->post('description'), 'video'=>'');
					}

					$primary_param = array('rvc_number'=>$this->input->post('number'),'rvc_name'=>$this->input->post('name'), 'modified'=>time(), 'created'=>time(), 'is_active'=>$this->input->post('active'), 'is_changed'=>1);
					$param = array('primary'=>$primary_param, 'details'=>$details_param);

					$rv_id = $this->food_application_model->save_revenue($param, $_POST['rvc_id']);
					if($rv_id > 0) {
						$this->food_application_model->save_images($images, 'revenue', $rv_id);
						$this->message->set('Revenue successfully updated.', 'success', TRUE);
						redirect('revenue');
					}
					else {
						$this->get_last_post->set($_POST, '', TRUE);
						$this->message->set('Error in revenue updation.', 'error', TRUE);
						redirect('revenue/edit/'.$_POST['rvc_id']);
					}
				}
				else {
					$this->get_last_post->set($_POST, '', TRUE);
					redirect('revenue/edit/'.$_POST['rvc_id']);
				}

				break;
		}
	}
}
?>