<script language="javascript" type="text/javascript">
    $(function(){
        //alert($('#channel_viewd').width());
    });

    function channel_details(data) {
        //alert(data);
        //return;
        var path = 'tv_reports/hour_wise_channel_data/channel_hour_wise';
        var params = data.split(",");
        var ts = '<?php echo convert_date_to_ts($to_date);?>';
        var light_name = params[0];
        var room_id = "";

        if(params.length > 0) {
            var len = params.length;
            for(var i=0; i < len; i++) {
                if(i > 1) {
                    room_id += params[i];
                    if(i < len-1) {
                        room_id += ',';
                    }
                }
            }
        }

        var param='';
        if(ts != "") {
            param = 'from_date=<?php echo convert_date_to_ts($from_date); ?>&to_date=<?php echo convert_date_to_ts($to_date);?>&room_id='+room_id+'&channel_name='+light_name+'&room_type=<?php echo $room_type;?>';
        }
        else{
            param = 'from_date=<?php echo convert_date_to_ts($from_date); ?>&channel_name='+light_name+'&room_id='+room_id+'&room_type=<?php echo $room_type;?>';
        }

        $("#light_detail_view").show();

        ajax_request(path, param, 'channel_onoff_details', "light_detail_heading", "light_detail_content");
    }

    function set_block_width(block) {
        var width = 0;
        var height = 0;
        width = $("#"+block).width();
        height = $("#"+block).height();
        //$("#"+block).children("img").width(width);
        //$("#"+block).children("img").height(height);
    }

    function ajax_request(path, param, chart_div, list_heading_div, list_div) {
        $.ajax({
            url: '<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/itc/index.php/';?>'+path,
            type: 'POST',
            data: param,
            success: function(msg){
                //alert(msg);
               var chartObj = FusionCharts(chart_div);
               //alert(chartObj.toString());
               var index = msg.indexOf('~');
                if(list_div != null) {
                   var grph_list = msg.split('~');
                   chartObj.setXMLData(grph_list[0]);
                   $("#"+list_heading_div).html(grph_list[1]);
                   $("#"+list_div).html(grph_list[2]);
                }
                else {
                    chartObj.setXMLData(msg);
                }
            }
        });
    }

</script>
<table cellpadding="5" cellspacing="0" width="98%"
	class="border_tbl ui-corner-all tbl_background">
	<tr class="contentheading">
		<td><?php echo $page_heading; ?></td>
	</tr>
	<tr>
		<td>
		<table width="100%" cellpadding="2" cellspacing="2">

			<tr>
				<td align="center">
				<ul id="gallery">
					<li style="float: left;">
					<table cellpadding="5" cellspacing="0" class="border_tbl"
						width="100%">
						<tr class="report-panel-heading">
							<td>
							<div>Channels viewed <label style="font-size: 10px;">(click on
							channel name to view hour wise graph.)</label></div>
							</td>
						</tr>
						<tr>
							<td align="center" id="channel_viewd"><?php if($data['channel_graph'] != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/Pie2D.swf", "cahnnel_on_off", $data['channel_graph'], "cahnnel_on_off", "$('#channel_viewd').width()", 250); } else {echo "Data not found.!";}?></td>
						</tr>
					</table>
					</li>
					<li style="float: right; width: 280px;">
					<table cellpadding="5" cellspacing="0" class="border_tbl"
						width="100%">
						<tr class="report-panel-heading">
							<td>
							<div>Channel viewed in a day</div>
							<br />
							</td>
						</tr>
						<tr>
							<td id="channel_detail"><?php  //echo renderChart(ASSETS_PATH."FusionCharts/Charts/MSLine.swf", "channel_onoff_details", "Data Not Found", "channel_onoff_details", 270, 250); ?>
							<img src="<?php echo get_assets_path('image').'bbc.png';?>"
								style="border: #CCCCCC 3px solid;" /></td>
						</tr>
						<tr>
							<td style="line-height: 0.8px;">&nbsp;</td>
						</tr>
					</table>
					</li>
				</ul>
				</td>
			</tr>
			<!--<tr>
                    <td>
                        <table cellpadding="5" cellspacing="0" class="border_tbl" width="100%">
                            <tr class="report-panel-heading">
                                <td><div>TV Usages (For zoom, over the mouse and drag one point to another)</div></td>
                            </tr>
                            <tr>
                                <td id="temp_graph"><?php //if($data['tv_graph'] != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/ZoomLine.swf", "tv_on_off", $data['tv_graph'], "tv_on_off", "$('#temp_graph').width()", 250); } else {echo "Data not found.!";}?></td>
                            </tr>
                        </table>
                    </td>
                </tr>-->

			<tr>
				<td>
				<table cellpadding="3" cellspacing="0" class="border_tbl"
					width="100%">
					<tr class="report-panel-heading">
						<td colspan="5">
						<div>Other Static:</div>
						</td>
					</tr>
					<tr>
					<?php //if(isset($data['floor_wise_tv_usages'])){?>
						<!--<td valign="top" width="20%" style="border-right: #CCCCCC 1px solid;">
                        <table cellpadding="2" cellspacing="2" class="pagecontent" width="100%">
                            <tr><td colspan="4"><b>Floors TV Usages:</b></td></tr>
                            <tr><td colspan="4" class="hr"><hr/></td></tr>
                            <tr><td><?php //echo $data['floor_wise_tv_usages'];?></td></tr>
                        </table>
                    </td>
                    <?php //} if(isset($data['top_rooms'])){?>
                    <td valign="top" width="20%" style="border-right: #CCCCCC 1px solid;">
                        <table cellpadding="2" cellspacing="2" class="pagecontent" width="100%">
                            <tr><td colspan="4"><b>Top Rooms in TV Usages:</b></td></tr>
                            <tr><td colspan="4" class="hr"><hr/></td></tr>
                            <tr><td><?php //echo $data['top_rooms'];?></td></tr>
                        </table>
                    </td>-->
                    <?php //}
                    if(isset($data['top_channels'])){?>
						<td valign="top" width="25%"
							style="border-right: #CCCCCC 1px solid;">
						<table cellpadding="2" cellspacing="2" class="pagecontent"
							width="100%">
							<tr>
								<td colspan="2"><b>Top Viewed Channels:</b></td>
							</tr>
							<tr>
								<td colspan="2" class="hr">
								<hr />
								</td>
							</tr>
							<!--<tr>
                                <td valign="top"><?php //echo $data['top_channels']; ?></td>
                            </tr>-->
							<tr>
								<td><label>DW TV</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">43hr</td>
							</tr>
							<tr>
								<td><label>BBC News</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">34hr</td>
							</tr>
							<tr>
								<td><label>HBO</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">28hr</td>
							</tr>
							<tr>
								<td><label>Qatar TV</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">28hr</td>
							</tr>

							<tr>
								<td><label>Star World</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #0B740B; color: #FFFFFF; border: #000 1px solid;">19hr</td>
							</tr>
						</table>
						</td>
						<td valign="top" width="25%"
							style="border-right: #CCCCCC 1px solid;">
						<table cellpadding="2" cellspacing="2" class="pagecontent"
							width="100%">
							<tr>
								<td colspan="2"><b>Least Viewed Channels:</b></td>
							</tr>
							<tr>
								<td colspan="2" class="hr">
								<hr />
								</td>
							</tr>
							<!--<tr>
                                <td valign="top"><?php //echo $data['top_channels']; ?></td>
                            </tr>-->

							<tr>
								<td><label>Star Gold</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #B13F3F; color: #FFFFFF; border: #000 1px solid;">00hr</td>
							</tr>
							<tr>
								<td><label>ETC</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #B13F3F; color: #FFFFFF; border: #000 1px solid;">07hr</td>
							</tr>
							<tr>
								<td><label>IBN-7</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #B13F3F; color: #FFFFFF; border: #000 1px solid;">02hr</td>
							</tr>
							<tr>
								<td><label>TV5</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #B13F3F; color: #FFFFFF; border: #000 1px solid;">05hr</td>
							</tr>
							<tr>
								<td><label>Zee TV</label></td>
								<td width="60" height="20" align="center"
									style="background-color: #B13F3F; color: #FFFFFF; border: #000 1px solid;">12hr</td>
							</tr>

						</table>
						</td>
						<?php } //if(isset($data['top_category'])) {?>
						<!--<td valign="top" width="20%" style="border-right: #CCCCCC 1px solid;">
                        <table cellpadding="2" cellspacing="2" class="pagecontent" width="100%">
                            <tr><td colspan="2">Top Categories:</td></tr>
                            <tr><td colspan="2" class="hr"><hr/></td></tr>
                            <tr>
                                <td><b>#</b></td><td><b>Category</b></td>
                            </tr>
                            <tr>
                                <td>1</td><td>Sports</td>
                            </tr>
                            <tr>
                                <td>2</td><td>Entertainment</td>
                            </tr>
                            <tr>
                                <td>3</td><td>News</td>
                            </tr>
                        </table>
                    </td>
                    <?php //} if($room_type == "") {?>
                    <td valign="top" width="15%">
                        <table cellpadding="2" cellspacing="2" class="pagecontent" width="100%">
                            <tr><td colspan="2">Aggregates:</td></tr>
                            <tr><td colspan="2" class="hr"><hr/></td></tr>
                            <tr>
                                <td><b><?php //echo $data['avg_tv_use']; ?></b> hours average TV usages per room.</td>
                            </tr>
                            
                        </table>
                    </td>-->
                    <?php //} ?>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
				<table cellpadding="5" cellspacing="0" class="border_tbl"
					width="100%">
					<tr class="report-panel-heading">
						<td>
						<div>News Channel Comparison</div>
						</td>
					</tr>
					<tr>
						<td id="temp_graph"><?php if($data['tv_compair_graph'] != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/MSLine.swf", "tv_on_off", $data['tv_compair_graph'], "tv_on_off", "$('#temp_graph').width()", 250); } else {echo "Data not found.!";}?></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<div id="statusbar">
		<ul id="wins"></ul>
		</div>
		</td>
	</tr>
</table>
