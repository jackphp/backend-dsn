<style type="text/css">
#img_list .ch_list {
	width: 110px;
	height: 110px;
	background-color: #F2F2F2;
	border: #CCCCCC 1px solid;
	padding: 2px 2px 2px 2px;
	font-size: 12px;
}

#img_list .ch_list:hover {
	background-color: orange;
}

#img_list a {
	text-decoration: none;
	color: #000000
}

#img_list a:hover {
	text-decoration: underline;
	color: #000000
}

/*#channels_contener{height: 620px; overflow:auto;}*/
ul {
	list-style-type: none;
}

.gallery ul {
	width: 350px;
	list-style-type: none;
	margin: 0px;
	padding: 0px;
}

.gallery li {
	float: left;
	padding: 5px;
	width: 110px;
	height: 110px;
}

.gallery li div {
	width: 90px;
	height: 70px;
	border: solid 1px black;
	background-color: #E0E0E0;
	text-align: center;
	padding-top: 40px;
}

.placeHolder .ch_list {
	background-color: white !important;
	border: dashed 1px gray !important;
}

.tip {
	color: #FFFFFF;
	padding: 5px 5px 5px 5px;
	background-color: #000000;
	border: #CCCCCC 1px solid;
	font-size: 14px;
	display: none;
	zindex: 999;
	position: absolute;
}

span {
	text-align: left;
}

.ch_list {
	cursor: move !important
}
</style>

<!--<script src="<?php echo base_url();?>js/jquery.dragsort-0.4.1.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.dragsort-0.4.1.min.js" type="text/javascript"></script>-->

<title>TV Channel Management</title>
<script type="text/javascript">
        $(function(){
             
 
           //setTimeout(function(){alert("Hello")},3000)
            // hide_message();
            var item1; 
            var item2;
            $(".gallery").dragsort({ dragSelector: "div", dragEnd: saveOrder,scrollSpeed: 15, placeHolderTemplate: "<li class='placeHolder'><div></div></li>"});
            var isDrag = 0;

//            $(".pos").change(function(){
//
//            });

            $("#button1").click(function(){
                alert("First=> "+$("#first").val()+" Second=> "+$("#second").val());
            });

            // $(".gallery").dragsort({ scrollSpeed: 100 });

            $(".delete1").click(function(){
                var loc = $(this).attr("href");
                jConfirm('<label>Are you sure to disable this channel?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                     //     return true;
                          window.location.href=loc;
                      }
                });
                return false;
            });


            
           
              
            $("#commit_changes").click(function(){
                jConfirm('<label>Are you sure to update rooms?</label>', 'Confirmation', function(r) {
                      if(r == true) {
                          //alert(del_location);
                          window.location.href='<?php echo base_url();?>favourite/generate';
                      }
                  });
            })
        })

        function set_time_out() {
            $(".gallery").dragsort({ dragSelector: "div", dragEnd: saveOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>"});
        }

        function swap_channel(obj) {
            var current_ele = '';
            var current_html = '';
            var next_ele = '';
            var next_html = '';
            var filter_id = '';

            current_ele = $(obj).parent("div").attr("id");
            next_ele = $(obj).val();

            filter_id = current_ele.split("_");

//            resetAttribute($("#"+current_ele).children("select").children("option"));
//            resetAttribute($("#pos_"+next_ele).children("select").children("option"));
//            set_attr($("#"+current_ele).children("select").children("option"), next_ele);
//            set_attr($("#pos_"+next_ele).children("select").children("option"), filter_id[1]);

            //alert("current=> "+next_ele+" next=> "+filter_id[1]);
            saveOrder(next_ele, filter_id[1]);

            $("#"+current_ele).hide("slow");
            $("#pos_"+next_ele).hide("slow");

            next_html = $("#pos_"+next_ele).html();
            current_html = $("#"+current_ele).html();

            //alert(next_html);

            $("#"+current_ele).html(next_html);
            $("#pos_"+next_ele).html(current_html);

            $("#"+current_ele).delay(100).show("slow");
            $("#pos_"+next_ele).delay(500).show("slow");

        }

        function resetAttribute(obj) {
            //alert(val);
            $(obj).each(function(){
                //alert($(this).attr("value"));
                if($(this).attr("selected")) {
                    //alert($(this).attr('value'));
                    $(this).removeAttr("selected");
                }
            });
        }

        function set_attr(obj, val) {
            $(obj).each(function(){
                if($(this).attr("value") == val) {
                    //alert(val);
                    $(this).attr("selected", "selected");
                }
            });
        }

        
        function saveOrder(tag1, tag2) {
            //$("#sorted_id").attr("value", tmp_data);
            $("#msg").addClass("notis");
            $("#msg").html('<div style="width:700px;border:1px solid black;padding-top:10px;color:green;font-size:16px;height:40px;border-radius:8px;background:-moz-linear-gradient(center top , #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;">* Changes made in this table will not be saved until the form is saved. <input type="button" class="button grey-gradient glossy" name="button1" value="Save" onclick="saveSortOrder()" /><input type="button" style="margin-left:8px" class="button grey-gradient glossy" name="button2" value="Reset" onclick="window.location.href=\'<?php echo base_url();?>favourite\'"/></div>');
            $("#msg").show();
        }

        function reload() {
            window.location.reload();
        }

        function saveSortOrder() {
            var data = $(".gallery li").map(function() { return $(this).attr("itemID"); }).get();
            //var cids = $("#sorted_id").val();
            //alert(data);

            $.post("<?php echo base_url(); ?>favourite/sort_save", { "ids[]": data});
            $("#msg").html("<center><label>Saving...</label></center>");
            setTimeout(function(){$(".notis").hide();}, 1000);
            //setTimeout("reload()", 4000);
        }
       
       function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                     $(".notis").hide();        
                }, 4000);

             



            }
     
</script>

<input type="hidden"
	name="first" id="first" />
<input type="hidden"
	name="second" id="second" />
<!--<input type="button" name="button1" id="button1" value="Show Value" />-->
<div id="msg"
	style="margin-top: 0px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 600px; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php   } ?>
<div class="border_tbl"
	style="width: 100%; text-align: left; margin-top: 0px"><!--z-index: 4000;-->
<div
	style="float: left; margin: 5px; position: relative; margin-top: 20px">
<div class="button grey-gradient glossy" style="float: left;"
	onclick="window.location.href='<?php echo base_url();?>favourite/channels'">
<div class="with-tooltip" title="Add new Channel"><label>Add New</label></div>
</div>
<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px;"
	onclick="window.location.href='<?php echo base_url();?>favourite/unfavorites'">
<div class="with-tooltip" title="Disabled Channels"><label>Disabled</label></div>
</div>

<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px;"
	onclick="window.location.href='<?php echo base_url();?>favourite/category'">
<div class="with-tooltip" title="Manage Channel Category"><label>Category</label></div>
</div>

<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px;" id="commit_changes">
<div class="with-tooltip" title="Update Rooms"><label>Update Rooms</label></div>
</div>
</div>
<div style="clear: both"></div>



<form method="post">

<div id="channels_contener">
<div id="img_list"><!--            z-index: 12-->
<div id="tabs" style="display: block; width: 100%; float: left;">

<ul>
	<li><a href="#tabs-1">All</a></li>
	<?php foreach($category as $cat) {?>
	<li><a class=""
		href="<?php echo base_url().'favourite/cat_channels/'.$cat->id; ?>"><?php echo $cat->category; ?></a></li>
		<?php }?>
</ul>

<div id="tabs-1">
<ul class="gallery" style="padding-left: 10px">
<?php
if(count($query) > 0) {
	$i = 0; foreach($query as $row) {
			
		echo '<li itemID="'.$row->chid.'" id="'.$row->chid.'"><div class="ch_list" title="'.$row->chname.': '.$row->Channel_No.'">
                        '.$images[$row->chid].'
                        <br/>
                        <a href="'.base_url().'favourite/edit/'.$row->chid.'/1">Edit</a> | <a href="'.base_url().'favourite/delete_channel/'.$row->chid.'/1" class="delete1">Disable</a>
                        </div>
                        </li><input type="checkbox" name="channelid[]" value="'.$row->chid.'" checked="checked" style="display:none"/>';
		?>
		<?php $i++; } }?>

</ul>
<br />
</div>
</div>
</div>
</div>

<br />
<input type="hidden" name="sorted_id" id="sorted_id" /></form>
</div>

<script>
 $('#tabs').tabs({ajaxOptions: {cache: false, error: function( xhr, status, index, anchor ) {$( anchor.hash ).html("Couldn't load this tab. We'll try to fix this as soon as possible. " +"If this wouldn't be a demo." );}}});
 
 $("#tabs").show();
  $("#tabs").scrollabletab({cache: false});

</script>


