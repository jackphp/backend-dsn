<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title; ?></title>

<!-- UTF-8 Character Support -->
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />

<!--Include CSS Files -->
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet"
	type="text/css" />

<!-- Include all JS files -->
<script src="<?php echo base_url(); ?>js/jquery.min.js"
	type="text/javascript" charset="utf-8"></script>

<!-- Favicon link added by Raivndra -->
<!-- <link href="<?=base_url(); ?>/home_automation.ico" rel="shortcut icon" type="image/ico" />-->

<!-- All Jquery and Javascript is placed here -->

</head>
<body>
<center>
<div id="container"><!-- Header -->
<div id="header"><?php $this->load->view('inc_header'); ?></div>

<!--Main Menu -->
<div id="navbar"><?php $this->load->view('inc_main_menu'); ?></div>

<!--page heading -->
<div id="page_heading_main">
<h1><?php echo $page_heading ;?></h1>
</div>

<!--submenu block -->
<div id="sub_navbar"></div>

<!--Show all error/success message here  -->
<div id="show_msg"><?php
$this->load->view('inc_msg');
?> <?php
if (isset($this->form_validation->error_string))
{

	?>
<table align="center" width="100%" border="0">
	<tr align="center">
		<td>
		<div id="showMsg"><?php
		echo form_error('category', '<div class="error">', '</div>');
		echo form_error('position', '<div class="error">', '</div>');
		?></div>
		<script language="javascript" type="text/javascript">
                                            setTimeout('document.getElementById("showMsg").style.display="none"',6000);
                                    </script></td>
	</tr>
</table>
		<?php
}
?></div>

<!--page content block start here -->
<div id="content-block"><?php if ($this->session->userdata('mode')=="add"){ ?>
<!--Add Customer Form we show this form if user click on add Customer button -->
<div id="add_cust_tag">
<form name="frm_add_tag" method="post" action="">
<table cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td colspan="4" align="right">Fields marked with <span
			class="red_star"><small>*</small></span> are mandatory</td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Category:</td>
		<td colspan="3" class="form_field"><input type="text" name="category"
			size="50"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('category');} ?>" /></td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Position:</td>
		<td colspan="3" class="form_field"><input type="text" name="position"
			size="10" maxlength="4"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('position');} ?>" /></td>
	</tr>
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="center">
		<div class="buttons">
		<button type="submit" class="positive" name="submit"><img
			src="<?=base_url()?>/images/save.png" height="24" alt="" border="0" />
		Save</button>

		<a href="<?=site_url()?>/favourite/category" class="negative"> <img
			src="<?=base_url()?>/images/cancel.png" height="24" alt="" border="0" />
		Cancel </a></div>
		<input type="hidden" name="action" value="add" /></td>
	</tr>

</table>
</form>
</div>
<!--End of add form --> <?php }else if($this->session->userdata('mode')=="edit"){?>
<!--Edit Customer Form -->
<div id="edit_cust_form"><?php foreach ($qry_result->result() as $row) { //print_r($row);//die();?>
<form name="frm_edit_cust" method="post" action="">
<table cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Category:</td>
		<td colspan="3" class="form_field"><input type="text" name="category"
			size="5"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('category');}else{echo $row->category ;} ?>" /></td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Position:</td>
		<td colspan="3" class="form_field"><input type="text" name="position"
			size="10" maxlength="4"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('position');}else{echo $row->position ;} ?>" /></td>
	</tr>
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="center">
		<div class="buttons">
		<button type="submit" class="positive" name="submit"><img
			src="<?=base_url()?>/images/save.png" height="24" alt="" border="0" />
		Save</button>

		<a href="<?=site_url()?>/favourite/category" class="negative"> <img
			src="<?=base_url()?>/images/cancel.png" height="24" alt="" border="0" />
		Cancel </a></div>
		<input type="hidden" name="action" value="edit" /> <input
			type="hidden" name="rowid"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('rowid');}else{echo $row->rowid ;} ?>" />
		</td>
	</tr>
</table>
</form>
<?php }?></div>
<!--End of edit form --> <?php }?></div>
<!--page content block end here --> <!--Page Footer -->
<div id="footer"><?php $this->load->view('inc_footer'); ?></div>
<!--page footer ends here --></div>
<!--container div ends here --></center>
<script type="text/javascript" charset="utf-8">
            $('.tbl_border tr:odd').css('background','#e3e3e3');
        </script>
</body>

</html>
