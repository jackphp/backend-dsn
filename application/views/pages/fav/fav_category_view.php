<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title; ?></title>

<!-- UTF-8 Character Support -->
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />

<!--Include CSS Files -->
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet"
	type="text/css" />

<!-- Include all JS files -->
<script src="<?php echo base_url(); ?>js/jquery.min.js"
	type="text/javascript" charset="utf-8"></script>

<!-- Favicon link added by Raivndra -->
<!-- <link href="<?=base_url(); ?>/home_automation.ico" rel="shortcut icon" type="image/ico" />-->

<!-- All Jquery and Javascript is placed here -->
<script type="text/javascript">
            function delete_fav_cat(rowid)
            {
                if(confirm("Do you want to delete this category?") == true)
                {
                    window.location="<?php echo base_url();?>index.php/favourite/delete_category/"+rowid;
                }
            }

            
            function chk_form() {
                var name = document.getElementById("name").value;
                var position = document.getElementById("position").value;

                if(name == "") {
                    alert("Please enter name.");
                    document.getElementById("name").focus();
                    return false;
                }
                if(position == "") {
                    alert("Please enter position.");
                    document.getElementById("position").focus();
                    return false;
                }

                return true;
            }

        </script>
</head>
<body>
<center>
<div id="container"><!-- Header -->
<div id="header"><?php $this->load->view('inc_header'); ?></div>

<!--Main Menu -->
<div id="navbar"><?php $this->load->view('inc_main_menu'); ?></div>

<!--page heading -->
<div id="page_heading_main">
<h1><?php echo $page_heading ;?></h1>
</div>

<!--submenu block -->
<div id="sub_navbar"></div>

<!--Show all error/success message here  -->
<div id="show_msg"><?php
$this->load->view('inc_msg');
?> <?php
if (isset($this->form_validation->error_string))
{

	?>
<table align="center" width="100%" border="0">
	<tr align="center">
		<td>
		<div id="showMsg"><?php



		?></div>
		<script language="javascript" type="text/javascript">
                                            setTimeout('document.getElementById("showMsg").style.display="none"',6000);
                                    </script></td>
	</tr>
</table>
		<?php
}
?></div>

<!--page content block start here -->
<div id="content-block"><!--Add option tag Button -->
<div class="buttons" style="padding-left: 10px; width: 425px;"><a
	href="<?=site_url()?>/favourite/add_category" title="Add Category"
	class="regular"> <img src="<?=base_url()?>/images/add.png" height="24"
	alt="" border="0" /> Add Category </a></div>

<div id="show_list" style="clear: both;">
<table cellspacing="0" cellpadding="2" border="0" class="tbl_border">
	<!--Table headings -->
	<tr style="height: 30px">
		<th style="width: 40px; text-align: left; padding-left: 10px;">S.No.</th>
		<th style="width: 150px; text-align: left; padding-left: 10px;">Category</th>
		<th style="width: 80px; text-align: left; padding-left: 10px;">Position</th>
		<th>-</th>
		<th>-</th>
	</tr>
	<?php
	$i=1;
	foreach ($query as $row) { ?>
	<tr>
		<td><?=$i?></td>
		<td><?=$row->category?></td>
		<td><?=$row->position?></td>
		<td><a href="<?=site_url()?>/favourite/edit_category/<?=$row->rowid?>"
			title="Edit Category"><img src="<?=base_url()?>/images/edit.png"
			height="28" alt="" border="0" /></a></td>
		<td><a href="#" onclick="delete_fav_cat('<?=$row->rowid?>')"
			title="Delete Category"><img src="<?=base_url()?>/images/delete.png"
			height="28" alt="" border="0" /></a></td>

	</tr>
	<?php $i++;}?>
</table>
	<?php //echo $paginator; //prints Pagination links?></div>
<!--End of show list --></div>
<!--page content block end here --> <!--Page Footer -->
<div id="footer"><?php $this->load->view('inc_footer'); ?></div>
<!--page footer ends here --></div>
<!--container div ends here --></center>
<script type="text/javascript" charset="utf-8">
            $('.tbl_border tr:odd').css('background','#e3e3e3');
        </script>
</body>

</html>
