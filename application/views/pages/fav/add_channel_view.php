<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $page_title; ?></title>

<!-- UTF-8 Character Support -->
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />

<!--Include CSS Files -->
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet"
	type="text/css" />

<!-- Include all JS files -->
<script src="<?php echo base_url(); ?>js/jquery.min.js"
	type="text/javascript" charset="utf-8"></script>

<!-- Favicon link added by Raivndra -->
<!-- <link href="<?=base_url(); ?>/home_automation.ico" rel="shortcut icon" type="image/ico" />-->

<!-- All Jquery and Javascript is placed here -->
<script type="text/javascript">
            function checkChannelNum(chnum)
            {
                url="<?php echo base_url();?>index.php/favourite/check_channel/"+chnum;

                $.get(url,function(data)
                {

                    if(data == "No")
                    {
                     return true;
                    }
                    else
                    {
                        var answer=confirm("Dublicate entry occure on same channel number.\n"+data);
                        if (answer)
                            return true ;
                        else
                            return false ;
                    }

                });

               
            }
        </script>
</head>
<body>
<center>
<div id="container"><!-- Header -->
<div id="header"><?php $this->load->view('inc_header'); ?></div>

<!--Main Menu -->
<div id="navbar"><?php $this->load->view('inc_main_menu'); ?></div>

<!--page heading -->
<div id="page_heading_main">
<h1><?php echo $page_heading ;?></h1>
</div>

<!--submenu block -->
<div id="sub_navbar"></div>

<!--Show all error/success message here  -->
<div id="show_msg"><?php
$this->load->view('inc_msg');
?> <?php if($this->session->userdata("msg")!=""){?>
<table align="center" width="75%" border="0">
	<tr align="center">
		<td>
		<div id="showMsg"
			class="<?php echo $this->session->userdata("css_class");?>"><?php echo $this->session->userdata("msg");?>
		</div>
		<script language="javascript" type="text/javascript">
                                        setTimeout('document.getElementById("showMsg").style.display="none"',6000);
                                </script></td>
	</tr>
</table>
<?php $this->session->unset_userdata("msg"); }?> <?php
if (isset($this->form_validation->error_string))
{

	?>
<table align="center" width="100%" border="0">
	<tr align="center">
		<td>
		<div id="showMsg"><?php
		echo form_error('ch_name', '<div class="error">', '</div>');
		echo form_error('tag', '<div class="error">', '</div>');
		echo form_error('ch_no', '<div class="error">', '</div>');
		echo form_error('layout', '<div class="error">', '</div>');
		echo form_error('url', '<div class="error">', '</div>');
		?></div>
		<script language="javascript" type="text/javascript">
                                            setTimeout('document.getElementById("showMsg").style.display="none"',6000);
                                    </script></td>
	</tr>
</table>
		<?php
}
?></div>

<!--page content block start here -->
<div id="content-block"><?php if ($this->session->userdata('mode')=="add"){ ?>
<!--Add Customer Form we show this form if user click on add Customer button -->
<div id="add_cust_tag">
<form name="frm_add_channel" method="post" action=""
	enctype="multipart/form-data">
<table cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td colspan="4" align="right">Fields marked with <span
			class="red_star"><small>*</small></span> are mandatory</td>
	</tr>
	<tr>
		<td class="form_label">Channel Type:</td>
		<td colspan="3" class="form_field"><select name="ch_type"
			style="width: 125px;">
			<option value="RF"
			<?php if (isset($this->form_validation->error_string)){ if($this->input->post('ch_type')=='RF')echo "selected='selected'";} ?>>2
			Digit Channel</option>
			<option value="IR"
			<?php if (isset($this->form_validation->error_string)){ if($this->input->post('ch_type')=='IR')echo "selected='selected'";} ?>>3
			Digit Channel</option>
		</select></td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Channel
		Name:</td>
		<td colspan="3" class="form_field"><input type="text" name="ch_name"
			size="50"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('ch_name');} ?>" /></td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Tag:</td>
		<td colspan="3" class="form_field"><input type="text" name="tag"
			size="10" maxlength="3"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('tag');} ?>" /><small
			style="color: grey;">&nbsp;(Tag/position starts from 0.)</small></td>
	</tr>
	<tr>
		<td class="form_label"><span class="red_star"><small>*</small></span>Channel
		Number:</td>
		<td colspan="3" class="form_field"><input type="text" name="ch_no"
			size="10" maxlength="3"
			value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('ch_no');} ?>" /><small
			style="color: grey;">&nbsp;(Enter channel number in 2 digit or 3
		digit. For ex. 01 or 001)</small></td>
	</tr>
	<tr>
		<td class="form_label">Category:</td>
		<td colspan="3" class="form_field"><select name="category"
			id="category" style="width: 155px;">
			<option value="">(choose one)</option>
			<?php
			for($i=0;$i<count($fav_cat);$i++){
				?>
			<option value="<?php echo $fav_cat[$i]->category;?>"
			<?php if($fav_cat[$i]->category== $this->input->post('category')){echo "selected";}?>><?php echo $fav_cat[$i]->category;?></option>
			<?
			}
			?>
		</select></td>
	</tr>
	<tr style="height: 50px;">
		<td class="form_label"><span class="red_star"><small>*</small></span>Channel
		Image</td>
		<td><input type="file" name="ch_image" id="ch_image" size="50" /></td>
	</tr>
	<!--                           <tr>
                               <td class="form_label"><span class="red_star"><small>*</small></span>URL:</td>
                               <td colspan="3" class="form_field"><input type="text" name="url"  size="50" value="<?php if (isset($this->form_validation->error_string)){echo $this->input->post('url');} ?>"/></td>
                           </tr>-->
	<tr>
		<td colspan="4">
		<hr />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="center">
		<div class="buttons">
		<button type="submit" class="positive" name="submit"><img
			src="<?=base_url()?>/images/save.png" height="24" alt="" border="0" />
		Save</button>

		<a href="<?=site_url()?>/favourite/index" class="negative"> <img
			src="<?=base_url()?>/images/cancel.png" height="24" alt="" border="0" />
		Cancel </a></div>
		<input type="hidden" name="action" value="add" /></td>
	</tr>
</table>
</form>
</div>
<!--End of add form --> <?php }?></div>
<!--page content block end here --> <!--Page Footer -->
<div id="footer"><?php $this->load->view('inc_footer'); ?></div>
<!--page footer ends here --></div>
<!--container div ends here --></center>
<script type="text/javascript" charset="utf-8">
            $('.tbl_border tr:odd').css('background','#e3e3e3');
        </script>
</body>

</html>
