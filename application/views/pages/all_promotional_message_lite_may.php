
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<link
	rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/table.css?v=1">
<script
	src="<?php echo ASSETS_PATH ;?>datatable/js/media.js"></script>
<script
	src="<?php echo ASSETS_PATH ;?>datatable/js/dataTable.js"></script>

<script language="javascript" type="text/javascript">

 
$(function(){   
    
    
    
    
    
    
    //$('#user_table_length').hide();
   // $('#current_table_length').hide();
    
   
     hide_message();
})


function send_message(mid) {
    $.post("promo_message_lite/send_message/"+mid,function(data){
        
         window.location.href = '<?php echo base_url().'promo_message_lite'; ?>';
    } );
}

function  delete_message(mid){
           
                      jConfirm("<label>Are you sure you would like to delete this record ?</label>", "Confirmation", function(r) {
                                      if(r == true) {
                                          //alert(del_location);
                                          $.post("promo_message_lite/delete_message/"+mid,function(data){
                                          window.location.href = '<?php echo base_url().'promo_message_lite'; ?>';
                                          });   
                                      }
                                    else{
                                           return false;
                                     }
                              })  
 }   
 
 
 function  delete_send_message(mid){
           
                      jConfirm("<label>Are you sure you would like to delete this record ?</label>", "Confirmation", function(r) {
                                      if(r == true) {
                                          //alert(del_location);
                                          $.post("promo_message_lite/delete_send/"+mid,function(data){
                                          window.location.href = '<?php echo base_url().'promo_message_lite'; ?>';
                                          });   
                                      }
                                    else{
                                           return false;
                                     }
                              })  
 }   
            


function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }
</script>

<title>Promotional Messages</title>


<?php if(isset($page_heading)) {?>
<div class="pageheading"><?php //echo $page_heading; ?></div>
<div id="msg"
	style="margin-top: 10px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 30%; margin-bottom: 3%; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php } ?>

<!--<div style="margin-left:40px;margin-top: 10px;margin-bottom: 10px;"><button type="button" class="primary btn"   onclick="window.location.href = '<?php echo base_url().'video_song_category/'; ?>'"  ><div><b>Back</b></div></button></div>-->
<?php }?>




<div style="clear: both;"></div>
<div align="left" style="margin-bottom: 6px;"><a
	href="<?php echo base_url(); ?>promo_message_lite/edit" class="button">Create
Promotion</a></div>
<div style="width: 100%; text-align: left;">
<table class="table" id="user_table">
	<thead>
		<tr>
			<th align="center">S. No.</th>
			<th align="center">Title</th>
			<th align="center">Image</th>
			<th align="center">Start Date</th>
			<th align="center">Expiry Date</th>
			<th align="center">Status</th>
			<th align="center">Operations</th>
		</tr>
	</thead>
	<tbody>

	<?php  $i=1; foreach($s_result as $row){      ?>
		<tr class="gradeA">
			<td width="10%" style="vertical-align: middle" align="center"><?php echo $i; ?></td>
			<td width="15%" style="vertical-align: middle" align="left"><?php echo $row->title; ?></td>

			<td width="15%" align="center"><img width="50" height="50"
				src="<?php echo $row->promo_msg_img;  ?>" /></td>
			<td width="15%" style="vertical-align: middle" align="center"><?php $date = $row->start_date;$d= date('d-m-Y H:i', strtotime($date));if($d=='01-01-1970 05:30'){ echo '-';}else{echo $d;} ?></td>
			<td width="15%" style="vertical-align: middle" align="center"><?php $date = $row->end_date;echo date('d-m-Y H:i', strtotime($date)); ?></td>
			<td width="15%" style="vertical-align: middle" align="left"><?php $date = $row->start_date;//echo date('d-m-Y H:i', strtotime($date)); 
	$date2 = date('Y-m-d H:i');
	//$date2 = "2013-04-20 13:44:01";

	$diff = abs(strtotime($date2) - strtotime($date));

	$years   = floor($diff / (365*60*60*24));
	$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

	$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

	$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

	//$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));

	if($years){echo $years;}
	if($months){echo $months.'months, ';}
	if($days){
		if ($days == '1') {
			echo $days . ' day, ';
		} else {
			echo $days . ' days, ';
		}
	}
	if ($hours) {
		if ($hours == '1') {
			echo $hours . ' hour ';
		} else {
			echo $hours . ' hours ';
		}
	}
	if($minuts){
		if ($minuts == '1') {
			echo $minuts . 'minute';
		} else {
			echo $minuts . ' minutes ';
		}
	}
	echo ' to go.'; ?></td>
			<td width="15%" style="vertical-align: middle" align="center"><a
				class="button icon-pencil"
				href="promo_message_lite/edit/<?php echo $row->promo_msg_id; ?>" />Edit</a>
			<a href="#" class="button icon-trash"
				onclick="delete_message('<?php echo $row->promo_msg_id; ?>');" />Delete</a></td>
		</tr>

		<?php $i++; } ?>
	</tbody>

</table>
<!--      <script type="text/javascript" charset="utf-8">
                            $('#user_table').dataTable({
                               
                                 "sPaginationType": "full_numbers",
                                 "iDisplayLength": 5
                            });
       </script>--> <script type="text/javascript" charset="utf-8">
                                $('#user_table').dataTable({
                                    "iDisplayLength": 5,
                                    "sPaginationType": "full_numbers",
                                    "aaSorting": [[ 3, "asc" ]],
                                    "aoColumnDefs": [
                                 { 'bSortable': false, 'aTargets': [-1,2]}]
                                });
                            </script></div>

<div style="width: 100%; text-align: left; margin-top: 10px">
<table class="table" id="current_table">
	<thead>
		<tr>
			<th align="center">S. No.</th>
			<th align="center">Title</th>
			<th align="center">Image</th>
			<th align="center">Start Date</th>
			<th align="center">Expiry Date</th>
			<th align="center">Status</th>
			<th align="center">Operations</th>
		</tr>
	</thead>
	<tbody>

	<?php  $i=1; foreach($p_result as $row){      ?>
		<tr class="gradeA">
			<td width="10%" style="vertical-align: middle" align="center"><?php echo $i; ?></td>
			<td width="15%" style="vertical-align: middle" align="left"><?php echo $row->title; ?></td>


			<td width="15%" align="center"><img width="50" height="50"
				src="<?php echo $row->promo_msg_img;  ?>" /></td>
			<td width="15%" style="vertical-align: middle" align="center"><?php $date = $row->start_date;$d= date('d-m-Y H:i', strtotime($date));if($d=='01-01-1970 05:30'){ echo '-';}else{echo $d;} ?></td>
			<td width="15%" style="vertical-align: middle" align="center"><?php $date = $row->end_date;echo date('d-m-Y H:i', strtotime($date)); ?></td>
			<td width="15%" style="vertical-align: middle" align="left"><?php $date = $row->start_date;//echo date('d-m-Y H:i', strtotime($date)); 
	$date2 = date('Y-m-d H:i');
	//$date2 = "2013-04-20 13:44:01";

	$diff = abs(strtotime($date2) - strtotime($date));

	$years   = floor($diff / (365*60*60*24));
	$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

	$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

	$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

	$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
	if($minuts){
		echo 'Since ';
		if($years){echo $years;}
		if($months){echo $months.'months,';}
		if($days){
			if ($days == '1') {
				echo $days . ' day,';
			} else {
				echo $days . ' days,';
			}
		}
		if ($hours) {
			if ($hours == '1') {
				echo $hours . ' hour ';
			} else {
				echo $hours . ' hours ';
			}
		}
		if($minuts){
			if ($minuts == '1') {
				echo $minuts . ' minute';
			} else {
				echo $minuts . ' minutes ';
			}
		}
	}
	//             else{
	//                 echo 'Since from '.$seconds.' seconds';
	//             }
	//printf("%d years, %d months, %d days, %d hours, %d minuts\n", $years, $months, $days, $hours, $minuts);    ?></td>
			<td width="15%" style="vertical-align: middle" align="center"><a
				class="button icon-pencil"
				href="promo_message_lite/edit/<?php echo $row->promo_msg_id; ?>" />Edit</a>
			<a href="#" class="button icon-trash"
				onclick="delete_send_message('<?php echo $row->promo_msg_id; ?>');" />Delete</a></td>
		</tr>

		<?php $i++; } ?>
	</tbody>

</table>
<script type="text/javascript" charset="utf-8">
//                            $('#current_table').dataTable({
//                                 "sPaginationType": "full_numbers",
//                                 "iDisplayLength": 5
//                            });
                              $('#current_table').dataTable({
                                    "iDisplayLength": 5,
                                    "sPaginationType": "full_numbers",
                                    "aaSorting": [[ 4, "asc" ]],
                                    "aoColumnDefs": [
                                 { 'bSortable': false, 'aTargets': [-1,2]}]
                                });
       </script></div>

<style>
#current_table_length {
	font-size: 20px;
	font-weight: bold;
	color: 666666
}

#user_table_length {
	font-size: 20px;
	font-weight: bold;
	color: 666666
}

.dataTables_wrapper,.dataTables_header,.dataTables_footer {
	background: #e6e7ec !important;
	color: black
}

.dataTables_wrapper {
	border-radius: 15px;
	color: #666666;
	box-shadow: 1px 0 0 1px #ececf1 inset, 0 2px 0 rgba(255, 255, 255, 0.35)
		inset !important
}

.paginate_disabled_previous,a.paginate_active,a.paginate_active.first,.paginate_enabled_previous,.paginate_disabled_next,paginate_active,.paging_full_numbers a.paginate_active,.paging_full_numbers a.paginate_active.first,.paging_full_numbers a.paginate_active.last,.paginate_enabled_next,.paging_full_numbers a
	{
	/*        background-image: -webkit-linear-gradient(top, #666666, #666666); */
	background-image: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#666666),
		to(#666666) );
	background-image: -webkit-linear-gradient(top, #666666, #666666);
	background-image: -moz-linear-gradient(center top, #666666, #666666 50%, #666666 50%,
		#666666) !important;
	/*         background-image: -webkit-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666);*/
	border: 1px solid #cfcfcf !important;
}

#user_table_wrapper {
	border: 2px solid #CFCFCF !important
}

#current_table_wrapper {
	border: 2px solid #CFCFCF !important
}

.sorting_asc {
	cursor: pointer
}

.sorting_desc {
	cursor: pointer
}
</style>

<script>
$('#user_table_length').text('Scheduled Messages');
$('#current_table_length').text('Published Messages');

//<div align="center" class="dataTables_length" id="user_table_length" style=" font-family: 'Open Sans',sans-serif;font-size:20px;font-weight:bold;">Scheduled Messages</div>

</script>







