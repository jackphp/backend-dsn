<script language="javascript" type="text/javascript">
    $(document).ready(function() {
          hide_message();
         //alert('dd');
//         $("#list").sortable({
//      handle : '.handle',
//      update : function () {
//		  var order = $('#list').sortable('serialize');
//  		$("#info").load("process-sortable.php?"+order);
//      }
//    });
//       $(".gallery2").dragsort({ dragSelector: "div", dragEnd: saveSortOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>"});
//            var isDrag = 0;
//    
       $( ".gallery2" ).sortable({
        stop: function( event, ui ) {
            saveSortOrder();
		$("#galleryWrapper").find("table tr").last().css("border-bottom","1px solid #CCCCCC")
        },
	scroll: false, 
	axis: "y",
	containment: "#galleryWrapper",
	placeholder: "sortable-placeholder"
    });
	$( ".gallery2" ).find('td').each(function(elem){
		$(this).css('width',$(this).width());
	});

          $(".gallery2").sortable({ scroll: true, scrollSpeed: 10 });

        $( ".gallery2" ).disableSelection();
    
    
        });
        
 
        
        function check(id,val){
//        alert(val);
           url = "<?php echo base_url()?>food_category/update_cat_status/"+id+'/'+val;
              
                $.get(url,function(data){
                   //   window.location.reload();
                })     
                }
                
          function saveSortOrder() {
            //alert('rf');
            var data = $(".gallery2 tr").map(function() { return $(this).attr("id"); }).get();
            //var cids = $("#sorted_id").val();
            //alert(data);

            $.post("<?php echo base_url(); ?>food_category/sort_save", { "ids[]": data});
             //window.setTimeout('location.reload()', 100);
            //$("#msg").html("<center><label>Saving...</label></center>");
            //setTimeout("reload()", 4000);
        }    
        function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }
      
       
</script>

<title>IRD Management</title>


<style>
.even {
	
}

.odd {
	background: #F7F7F7
}

#table tr td {
	height: 30
}

.listheading th {
	background: none repeat scroll 0 0 #2D2D28;
	color: #FFFFFF;
	height: 32px;
	padding-top: 0px;
	font-size: 14px;
}

.vborder_tbl {
	border: #E8E8E8 1px solid;
	font-size: 12px;
	padding: 0px;
	-moz-border-radius: 4px /*{cornerRadius}*/;
	-webkit-border-radius: 4px /*{cornerRadius}*/;
	border-radius: 4px /*{cornerRadius}*/;
}
/*.message{display:none}*/
</style>
<!-- Main container Satrt -->
<?php if(isset($page_heading)) {?>
<!--<div class="pageheading"><?php// echo $page_heading; ?></div>-->
<?php }?>

<div id="msg"
	style="margin-top: 0px;"></div>
<?php if( $this->session->flashdata('_messages') ){?>
<div id="page_message"
	style="width: 600px; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?></div>
<?php } ?>
<div class="button grey-gradient glossy"
	style="float: left; margin-left: 2px; margin-bottom: 5px; margin-top: 35px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>menuitem'">
<div title="Back To Menu Items" class="with-tooltip"><label>Back</label></div>
</div>

<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px; margin-bottom: 5px; margin-top: 35px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>food_category/add'">
<div title="Add New Category" class="with-tooltip"><label>Add New
Category</label></div>
</div>
<br />

<div style="clear: both" id="galleryWrapper">

<table cellpadding="2" cellspacing="2" width="98%" class="table"
	align="center">
	<thead>
		<tr width="98%">
			<th width="10%" scope="col" align="center">#</th>
			<th width="20%" scope="col" align="center">POS Category Name</th>
			<th width="20%" scope="col" align="center">Display Name</th>
			<th width="20%" scope="col" align="center">Is Active</th>
			<th width="30%" scope="col" align="center">Operations</th>
		</tr>
	</thead>
	<tbody class="gallery2">

	<?php
	$i=1;
	foreach($categ->result() as $a) {
			
		if($i%2==0) {$class="even";} else {$class="odd";};
		?>
		<?php   $c='0';
		if($a->is_active=='1'){$c='checked';} ?>

		<tr id="<?php echo $a->maincategory_id; ?>" width="98%"
			class="<?php echo $class; ?>">

			<td scope="row" width="10%" align="center">
			<div class="handle"><img class="tabledrag-handle" href="#"
				title="Drag to re-order"
				src="<?php echo get_assets_path('image').'backend_images/arrow1.png'; ?>"
				style="cursor: move;" /></div>
			</td>
			<td scope="row" width="20%" align="left"><?php echo $a->name; ?></td>
			<td scope="row" width="20%" align="left">&nbsp;<?php echo $a->display_name; ?></td>
			<td scope="row" width="20%" align="center"><input type="checkbox"
				name="chk[]"
				onclick="check(<?php echo $a->maincategory_id.','. $c?>);"
				<?php echo $c;  ?> /></td>
			<td scope="row" width="30%" align="center"><a
				href="<?php echo base_url().'food_category/subcategory/'.$a->maincategory_id ?>">Add
			Subcategories</a> | <a
				href="<?php echo base_url().'food_category/edit/'.$a->maincategory_id ?>"
				class="addon_form2">Edit</a> | <a
				href="<?php echo base_url().'food_category/delete/cat/'.$a->maincategory_id; ?>"
				class="delete">Delete</a></td>
		</tr>
		<?php
		$i++; }
		?>
		<?php if(isset($pager) && ($pager!='')){?>
		<tr>
			<td align="center" class="border_tbl" bgcolor="#d3d3d3" height="30"><?php echo $pager; ?></td>
		</tr>
		<?php }else {?>
		<!--    <tr><td align="center"><a href="#" onclick="javascript:history.go(-1);">Back</a></td></tr>-->
		<?php }?>
	</tbody>
</table>

<br />
<br />
</div>
