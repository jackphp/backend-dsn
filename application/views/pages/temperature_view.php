<script language="javascript" type="text/javascript">
    function show_value(val) {
        alert(val);
    }
</script>

<div class="tab-button tab-button-primary"
	style="float: left; margin-left: 10px; margin-bottom: 5px; margin-top: 5px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>dashboard'">
<div tipval="Back To Dashboard" class="tooltip"><label>Back</label></div>
</div>
<table cellpadding="5" cellspacing="0" width="100%"
	class="border_tbl ui-corner-all tbl_background">
	<tr class="contentheading">
		<td><?php echo $page_heading; ?></td>
	</tr>
	<tr>
		<td align="center">
		<table width="95%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
				<div style="float: left;"><?php echo $previous_form['form_open'].$previous_form['previous'].$previous_form['from_date'].$previous_form['to_date'].$previous_form['form_id'].$previous_form['rooms'].$previous_form['floors'].$previous_form['viewas'].$previous_form['form_close']; ?></div>
				<div style="float: right"><?php echo $next_form['form_open'].$next_form['next'].$next_form['from_date'].$next_form['to_date'].$next_form['form_id'].$next_form['rooms'].$next_form['floors'].$next_form['viewas'].$next_form['form_close']; ?></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" id="temp_graph"><?php if($data['graph_xml'] != ""){ echo renderChart(ASSETS_PATH."FusionCharts/Charts/MSLine.swf", "", $data['graph_xml'], "FactorySum", "$('#temp_graph').width()", 350);} else {echo "Data not found.!";}?></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center">
		<table cellpadding="5" cellspacing="0" width="95%" class="border_tbl">
			<tr class="report-panel-heading">
				<td colspan="2">Other Statistics:</td>
			</tr>
			<tr>
				<td valign="top">
				<table>
					<tr>
						<td><label>Total AC on commands: </label> &nbsp;<b><?php if($data['on_commands'] != "" && $data['on_commands'] > 0){ echo $data['on_commands'];?></b>
						<label><?php echo $link_on_command; } else {echo "NA"; } ?></label></td>
					</tr>
					<tr>
						<td class="hr">
						<hr />
						</td>
					</tr>
					<tr>
						<td><label>Total AC off commands: </label> &nbsp;<b><?php if($data['off_commands'] != "" && $data['off_commands'] > 0) { echo $data['off_commands'];?></b>
						<label><?php echo $link_off_command; } else {echo "NA";}?></label></td>
					</tr>
					<tr>
						<td class="hr">
						<hr />
						</td>
					</tr>
					<tr>
						<td><label>Total AC temperature change commands :</label> &nbsp;<b><?php if($data['count_temp_change'] != "" && $data['count_temp_change'] > 0) { echo $data['count_temp_change'];?></b>
						<label><?php echo $link_temp_change_command; } else {echo "NA";}?></label></td>
					</tr>
				</table>
				</td>
				<td align="right" valign="top">
				<table>
				<?php if($view_as == "room") {?>
					<tr>
						<td><label>Average Set Temperature:</label> &nbsp;<b><?php if($data['avg_ac_temp'] != "" && $data['avg_ac_temp'] > 0){ echo $data['avg_ac_temp'].'&deg;C'; } else {echo "NA";}?></b></td>
					</tr>
					<tr>
						<td class="hr">
						<hr />
						</td>
					</tr>
					<tr>
						<td><label>Average Actual Temperature:</label> &nbsp;<b><?php if($data['avg_room_temp'] != "" && $data['avg_room_temp'] > 0){ echo $data['avg_room_temp'].'&deg;C'; } else {echo "NA";}?></b></td>
					</tr>
					<tr>
						<td class="hr">
						<hr />
						</td>
					</tr>
					<?php }?>
					<tr>
						<td><label>Average Set Temperature of <?php if($view_as != 'hotel') echo 'Floor '.$fname; else echo "All Floors"; ?>
						:</label> &nbsp;<b><?php if($data['avg_room_temp'] != "" && $data['avg_room_temp'] > 0){ echo $data['avg_floor_rooms_ac'].'&deg;C  &nbsp;'; } else {echo "NA";}?>
						</b></td>
					</tr>
					<tr>
						<td class="hr">
						<hr />
						</td>
					</tr>
					<tr>
						<td><label>Average Actual Temperature of <?php if($view_as != 'hotel') echo 'Floor '.$fname; else echo "All Floors"; ?>
						:</label> &nbsp;<b><?php if($data['avg_room_temp'] != "" && $data['avg_room_temp'] > 0){  echo $data['avg_floor_rooms_room'].'&deg;C &nbsp;'; }else {echo "NA";}?>
						</b></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>

		</td>
	</tr>
	<tr>
		<td><br />
		<br />
		<br />
		<br />
		</td>
	</tr>
</table>
