<script language="javascript" type="text/javascript">
    $(function(){
        $("#navbar-cancel-button").click(function(){
            close_popup();
        })

    })

    function close_popup() {
        //parent.$.fn.colorbox.close();
    }
</script>

<div class="pageheading"><?php echo $page_heading; ?></div>
<table cellpadding="0" cellspacing="2" width="100%" align="center"
	class="pagecontent">

	<tr>
		<td align="center"><?php echo $form_open;?>
		<table cellpadding="2" cellspacing="0" width="500" class="border_tbl">

			<tr>
				<td><b>Old Password:</b></td>
				<td><?php echo $old_password; ?></td>
			</tr>
			<tr>
				<td><b>New Password:</b></td>
				<td><?php echo $password; ?></td>
			</tr>
			<tr>
				<td valign="top"><b>Confirm Password:</b></td>
				<td valign="top"><?php echo $confirm_password; ?></td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="padding-left: 35px;">

				<div style="padding-left: 150px;">
				<div
					style="height: 32px; vertical-align: middle; float: left; margin-left: 5px;">
				<button type="submit" class="primary btn" id="navbar-submit-button">Save</button>
				</div>

				<!--<div style="height: 32px; vertical-align: middle; float: left; margin-left: 5px;">
                                <button type="button" class="primary btn" id="navbar-cancel-button">Cancel</button>
                            </div>--></div>
				</td>
			</tr>
		</table>
		<?php echo $form_close; ?></td>
	</tr>
	<tr>
		<td>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		</td>
	</tr>
</table>
