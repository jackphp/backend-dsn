<script language="javascript" type="text/javascript">
    $(function(){
           hide_message();
        $(".addon_form").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form").colorbox({title:'<h4>&nbsp;&nbsp;Add New Addon</h4>',width:"650", height:"450", iframe:true, href:'<?php echo base_url(); ?>food_addons/add/0/<?php echo $gid;?>',/*onClosed:function(){ location.reload(true); },*/ overlayClose:false,onComplete:function(){
                          var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
        
        
	$(".addon_form2").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form2").colorbox({title:'<h4>&nbsp;&nbsp;Edit Addon</h4>',width:"650", height:"450" ,/*onClosed:function(){ location.reload(true); },*/ iframe:true, overlayClose:false,onComplete:function(){
            var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
        
        
    })

         function color(){
     $(".addon_form2").click(function(){
            var tmpt = $(this).position();
            var t = (tmpt.top-100);
            $(".addon_form2").colorbox({title:'<h4>&nbsp;&nbsp;Edit Addon</h4>',width:"650", height:"450" ,/*onClosed:function(){ location.reload(true); },*/ iframe:true, overlayClose:false,onComplete:function(){
            var window_height = $(window).height();
            var colorbox_height = $('#colorbox').height();
            var top_position = 0;

            if(window_height > colorbox_height) {
                top_position = (window_height - colorbox_height) / 2;
            }

            // alert(top_position);
            $('#colorbox').css({'top':top_position, 'position':'fixed'});
            }});
        })
   
}

 
                 function hide_message() {
                setTimeout(function(){
                    $("#page_message").hide();
                }, 3000);

            }


function delete_cat(){
        $(".delete").click(function(){ 
                      var del_location = $(this).attr("href");
                      jConfirm("<label>Are you sure delete this record?</label>", "Confirmation", function(r) {
                          if(r == true) {
                              //alert(del_location);
                              window.location.href=del_location; 
                          }
                      });

//                    if(confirm("Are you sure delete this record.")) {
//                        return true;
//                    }
//                    else {
//                        return false;
//                    }
                        return false;
                }) 
} 



</script>

<title>IRD Management</title>




<div id="msg"
	style="margin-top: 0px;"></div>
<div id="page_message"
	style="display: none; width: 600px; border: 1px solid black; padding-top: 10px; color: green; font-size: 16px; height: 30px; border-radius: 8px; background: -moz-linear-gradient(center top, #EFEFF4, #D6DADF) repeat scroll 0 0 transparent;"><?php if( $this->session->flashdata('_messages') ){?><script>$('#page_message').show();</script>
<?php  $r=$this->session->flashdata('_messages'); echo $r[0]->message;?><?php } ?></div>




<div class="button grey-gradient glossy"
	style="float: left; margin-left: 5px; margin-bottom: 5px; margin-top: 45px;"
	id="send_letter"
	onclick="window.location.href='<?php echo base_url(); ?>group_addons'">
<div title="Back To Addon Family" class="with-tooltip"><label>Back</label></div>
</div>



<div class="button grey-gradient glossy"
	style="float: left; margin-bottom: 5px; margin-left: 5px; margin-top: 45px;"
	onclick="window.location.href='<?php echo base_url(); ?>food_addons/add/0/<?php echo $gid;?>'">
<div title="Add New Addon" class="with-tooltip"><label>Add New Addon</label></div>
</div>
<div class="pagecontent">&nbsp;&nbsp;</div>
<div style="clear: both" id="listWraper">
<center><?php echo $links; ?></center>
<table cellpadding="2" cellspacing="2" width="98%" class="table"
	align="center">
	<thead>
		<tr class="listheading">
			<th align="center" width="10%" scope="col">S.No</th>
			<th align="center" width="20%" scope="col">POS Addon Name</th>
			<th align="center" width="20%" scope="col">POS Code</th>
			<th align="center" width="20%" scope="col">Display Name</th>
			<th align="center" width="10%" scope="col">Price</th>
			<th align="center" width="20%" scope="col">Operations</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i=1;
	if(count($addons->result()) > 0) {
		foreach($addons->result() as $a) {
			if($i%2==0) {$class="even";} else {$class="odd";};
			?>
		<tr class="<?php echo $class; ?>">
			<td scope="row" width="10%" align="center"><?php echo $i; ?></td>
			<td scope="row" width="20%" align="left"><?php echo $a->addon_name; ?></td>
			<td scope="row" width="20%" align="center"><?php echo $a->addon_code; ?></td>
			<td scope="row" width="20%" align="left"><?php echo $a->display_name; ?></td>
			<td scope="row" width="10%" align="center"><?php echo $a->price; ?></td>
			<td scope="row" width="20%" align="center"><a
				href="<?php echo base_url().'food_addons/add/'.$a->addon_id;?>">Edit</a>
			| <a
				href="<?php echo base_url().'food_addons/delete_addon/'.$a->addon_id.'/'.$gid; ?>"
				class="delete">Delete</a></td>
		</tr>

		<?php
		$i++; }
	}
	else {
		echo '<tr><td colspan="6" align="center">Records not available.</td></tr>';
	}
	?>
	</tbody>
</table>
<div>&nbsp;</div>
<center><?php echo $links; ?></center>
<div>&nbsp;</div>
</div>
