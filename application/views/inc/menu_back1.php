<script language="javascript" type="text/javascript">
    $(function(){
        $("#foodmainmenu").hover(function(){
            $("#food_submenu").show();
        },
        function () {
            $("#food_submenu").hide();
        })

        $("#reports").hover(function(){
            $("#reports_submenu").show();
        },
        function () {
            $("#reports_submenu").hide();
        })
    })


</script>

<!--<div id="main_menu">
    <ul id="simple-horizontal-menu">
<?php 
//if(isset($menus)){
//    //echo "<pre>"; print_r($menus); die();
//    foreach($menus as $key=>$menu) {
//        if(is_array($menu)) {
//            echo '<li><a href="#">'.$key.'&nbsp;</a>';
//            echo '<ul style="visibility: hidden;" class="sub_menu">';
//            foreach($menu as $m){
//                if(is_array($m))
//                    echo '<li>'.$m[0].'</li>';
//                else
//                    echo '<li>'.$m.'</li>';
//            }
//            echo '</ul></li>';
//        }
//        else {
//            echo "<li>".$menu."</li>";
//        }
//    }
//    //prepare_menu_items($menus);
//}
//
//function prepare_menu_items($items) {
//    foreach($items as $item) {
//        if(is_array($item)) {
//            //prepare_menu_items($items);
//        }
//        else {
//            echo '<li>'.$item.'</li>';
//        }
//    }
//}

?>
 </ul>
</div>-->

<?php
if (isUserLoggedIn()) {
	?>
<!--<div id="main_menu_btn" style="float:left; margin-left: 10px; margin-top: 5px;">
        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>floor'">
            <div class="tooltip" tipval="Manage Floors"><img src="<?php echo get_assets_path('image').'flor.png'; ?>" style="float: left; margin-top: 2px; margin-right: 2px;"/>Floors</div>
        </div>

        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>room/room_list'">
            <div class="tooltip" tipval="Manage Rooms"><img src="<?php echo get_assets_path('image').'Room1.png'; ?>" style="float: left; margin-top: 2px; margin-right: 2px; "/>Rooms</div>
        </div>
        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>welcomletter'">
            <div class="tooltip" tipval="Welcome Letters"><img src="<?php echo get_assets_path('image').'letters1.png'; ?>" style="float: left; margin-top: 2px; margin-right: 2px;"/>Letters</div>
        </div>

        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>service_dir'">
            <div class="tooltip" tipval="Service Directory"> <img src="<?php echo get_assets_path('image').'book_1.png'; ?>" style="float: left; margin-top: 2px; margin-right: 2px; width: 24px; height: 24px; "/>Service Directory</div>
        </div>

        <div class="tab-button tab-button-primary" style="float: left;" id="foodmainmenu">
            <div>
                <div>
                    <img src="<?php echo get_assets_path('image').'food1.png'; ?>" style="float: left; margin-top: 2px; margin-right: 2px; width: 24px; height: 24px; "/>Food Management<img src="<?php echo get_assets_path('image').'point1.png'; ?>" style="float: right; margin-top: 10px; margin-left: 4px;"/>
                </div>
            </div>
            <div id="food_submenu" class="popup" style="display: none; position: absolute; width: 150px; height: 110px; text-align: left; padding-left: 5px; padding-top: 5px;">
                <div><a href="<?php echo base_url(); ?>menuitem">Menu Items</a></div>
                <div><a href="<?php echo base_url(); ?>food_category">Category</a></div>
                <div><a href="<?php echo base_url(); ?>group_addons">Addons Families</a></div>
                <div><a href="<?php echo base_url(); ?>menuitem/generate">Commit Changes</a></div>
            </div>
        </div>

        <div class="tab-button tab-button-primary" style="float: left;" id="reports">
            <div>
                <div>
                    <img src="<?php echo get_assets_path('image').'graf2.png'; ?>" style="float: left; margin-top: 1px; margin-right: 2px; width: 24px; height: 24px; "/>Reports<img src="<?php echo get_assets_path('image').'point1.png'; ?>" style="float: right; margin-top: 10px; margin-left: 4px;"/>
                </div>
            </div>
            <div id="reports_submenu" class="popup" style="display: none; position: absolute; width: 150px; height: 65px; text-align: left; padding-left: 5px; padding-top: 5px;">
                <div><a href="<?php echo base_url();?>dashboard">Dashboard</a></div>
                <div><a href="<?php echo base_url();?>room_history/rms/hotel/">Reports</a></div>
            </div>
        </div>
	<div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>favourite'">
            <div class="tooltip" tipval="TV Channels"><label>TV Cahnnels</label></div>
        </div>
        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>login/logout'">
            <div class="tooltip" tipval="Logout"><img src="<?php echo get_assets_path('image').'logout1.png'; ?>" style="float: left; margin-right: 2px; margin-top: 2px; width: 24px; height: 24px; margin-right: 2px;"/>Logout</div>
        </div>
    </div>-->

<ul id="appleNav">
	<li><a href="<?php echo base_url(); ?>floor" title="Floor">Floor</a></li>
	<li><a href="<?php echo base_url(); ?>room/room_list" title="Rooms">Rooms</a></li>
	<li><a href="<?php echo base_url(); ?>welcomletter"
		title="Welcome Letter">Letters</a></li>
	<li><a href="<?php echo base_url(); ?>service_dir"
		title="ITC Service Directory">ITC Directory</a></li>
	<li>
	<div id="foodmainmenu">
	<div>
	<div><a href="#">Food <!--<img src="<?php echo get_assets_path('image').'point1.png'; ?>" style="float: right; margin-top: 10px; margin-left: 4px;"/>--></a>
	</div>
	</div>
	<div id="food_submenu" class="popup"
		style="background: #636363; display: none; position: absolute; width: 150px; height: 150px; text-align: left; padding-left: 5px; padding-top: 5px;">
	<div><a href="<?php echo base_url(); ?>menuitem">Menu Items</a></div>
	<div><a href="<?php echo base_url(); ?>food_category">Category</a></div>
	<div><a href="<?php echo base_url(); ?>group_addons">Addons Families</a></div>
	<div><a href="<?php echo base_url(); ?>menuitem/generate">Commit
	Changes</a></div>
	</div>
	</div>
	</li>
	<li>
	<div id="reports">
	<div>
	<div><a href="#">Reports</a><!--<img src="<?php echo get_assets_path('image').'point1.png'; ?>" style="float: right; margin-top: 10px; margin-left: 4px;"/>-->
	</div>
	</div>
	<div id="reports_submenu" class="popup"
		style="background: #636363; display: none; position: absolute; width: 150px; height: 65px; text-align: left; padding-left: 5px; padding-top: 5px;">
	<div><a href="<?php echo base_url();?>dashboard">Dashboard</a></div>
	<div><a href="<?php echo base_url();?>room_history/rms/hotel/">Reports</a></div>
	</div>
	</div>
	</li>
	<li><a href="<?php echo base_url(); ?>favourite" title="TV-Channels">TV-Channels</a></li>
	<li><a href="<?php echo base_url(); ?>generate_pdf"
		title="Shopping PDF">Shopping PDF</a></li>
	<li><a href="<?php echo base_url(); ?>login/logout" title="Logout">Logout</a></li>
	<li></li>
</ul>
	<?php
}
else {
	?>
<!--    <div style="float:left; margin-left: 10px; margin-top: 5px;">
        <div class="tab-button tab-button-primary" style="float: left;" onclick="window.location.href='<?php echo base_url(); ?>login'">
            <div class="tooltip" tipval="User Login"><img src="<?php echo get_assets_path('image').'lock.png'; ?>" style="float: left; margin-top: 1px; width: 24px; height: 24px; margin-right: 5px;"/>Login</div>
        </div>
    </div>-->
	<?php
}

if(isset($menu_section) && $menu_section == 'food') {
	?>

	<?php
}
?>