<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title><?php echo "User Management";?></title>
<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<!--       <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/data_table_jui.css">
       <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/demo_page.css">
       <link rel="stylesheet" href="<?php echo ASSETS_PATH ;?>datatable/css/jquery.custom.css">
	<script src="<?php echo ASSETS_PATH ;?>datatable/js/media.js"></script>
        <script src="<?php echo ASSETS_PATH ;?>datatable/js/dataTable.js"></script>
        <script src="<?php echo ASSETS_PATH ;?>datatable/js/colRecoder.js"></script>-->
<link rel="stylesheet"
	href="<?php echo get_assets_path('css');?>dashboard/styles/table.css?v=1">
<script src="<?php echo ASSETS_PATH ;?>datatable/js/media.js"></script>
<script src="<?php echo ASSETS_PATH ;?>datatable/js/dataTable.js"></script>
<script src="<?php echo ASSETS_PATH ;?>datatable/js/colRecoder.js"></script>
<style>
.sorting_asc {
	cursor: pointer
}
</style>
<script>
            function delete_fm(id)
            {
               
                if(confirm("Do you want to delete this user ?") == true)
                {
                    window.location="<?php echo base_url();?>user_form/delete_user/"+id;
                }
            }
            
        </script> <?php error_reporting(E_ALL ^ E_NOTICE); ?> <header
	role="banner" id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header> <!-- Button to open/hide menu --> <a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts --> <a href="#" id="open-shortcuts"><span
	class="icon-thumbs"></span></a> <section role="main" id="main"> <!-- Main content -->
<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>
<hgroup id="main-title" class="thin">
<h1>User List</h1>
</hgroup>
<div class="with-padding">
<div class="panel-control align-right"><span
	class="button-group children-tooltip"><a title="Create User"
	href="<?php echo BASE_URL;?>index.php/user_form/create_user"
	class="button icon-plus margin-left">Create User</a></span></div>
<form>


<table class="table" id="user_table" style="clear: both;">
	<thead>
		<tr>
			<th style="width: 10px;">S.No.</th>
			<th align="center">User Name</th>
			<th align="center">Profile</th>
			<th align="center">User Display Name</th>
			<th align="center">Status</th>
			<th align="center">Operation</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$i="1";
	foreach ($user as $row)  { ?>
		<tr class="gradeA">
			<td align="center"><?php echo $i; ?></td>
			<td align="center"><?php echo $row['user_name'];?></td>
			<td align="center"><?php echo $row['profile_name']?></td>
			<td align="center"><?php echo $row['user_display_name']?></td>
			<td align="center"><?php 
			if($row['isDeleted']=="1"){
				echo "Active";}
				else {echo "Inactive";}

				?></td>
			<td align="center"><span class="button-group children-tooltip"><a
				class="button icon-pencil" title="Edit"
				href="<?php echo site_url();?>user_form/user_edit/<?php echo $row['user_id'];?>">Edit
			</a></span> <a title="Delete" href="#" class="button icon-trash"
				onclick="delete_fm(<?php echo $row['user_id'];?>)">Delete </a> </span>
			<span class="button-group children-tooltip"></td>

		</tr>
		<?php
		$i++;

	} ?>
	</tbody>
</table>
<script type="text/javascript" charset="utf-8">
                            $('#user_table').dataTable({
                                 "sPaginationType": "full_numbers",
                                  "aoColumnDefs": [
                                 { 'bSortable': false, 'aTargets': [-1]}
    ]
                            });
                       </script></form>

</div>

</section> <?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>

</body>

</html>
<style>
#current_table_length {
	font-size: 20px;
	font-weight: bold;
	color: 666666
}

#user_table_length {
	color: 666666
}

.dataTables_wrapper,.dataTables_header,.dataTables_footer {
	background: #e6e7ec !important;
	color: black
}

.dataTables_wrapper {
	border-radius: 15px;
	color: #666666;
	box-shadow: 1px 0 0 1px #ececf1 inset, 0 2px 0 rgba(255, 255, 255, 0.35)
		inset !important
}

.paginate_disabled_previous,a.paginate_active,a.paginate_active.first,.paginate_enabled_previous,.paginate_disabled_next,paginate_active,.paging_full_numbers a.paginate_active,.paging_full_numbers a.paginate_active.first,.paging_full_numbers a.paginate_active.last,.paginate_enabled_next,.paging_full_numbers a
	{
	/*        background-image: -webkit-linear-gradient(top, #666666, #666666); */
	background-image: -webkit-gradient(linear, 0% 0%, 0% 50%, from(#666666),
		to(#666666) );
	background-image: -webkit-linear-gradient(top, #666666, #666666);
	background-image: -moz-linear-gradient(center top, #666666, #666666 50%, #666666 50%,
		#666666) !important;
	/*         background-image: -webkit-linear-gradient(center top , #666666, #666666 50%, #666666 50%, #666666);*/
	border: 1px solid #cfcfcf !important;
}

#user_table_wrapper {
	border: 2px solid #CFCFCF !important
}

#current_table_wrapper {
	border: 2px solid #CFCFCF !important
}
</style>
