<title><?php echo "User Management"; ?></title>
<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_page.php'; ?>

<?php error_reporting(E_ALL ^ E_NOTICE); ?>

<header role="banner"
	id="title-bar">
<h2><?php echo HOTEL_NAME; ?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
<section role="main" id="main">
<!-- Main content -->
<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title"
	class="thin">
<h1>Create User</h1>

</hgroup>

<div class="with-padding">
<div style="height: auto; position: relative; padding-left: 200px;"
	class="panel-load-target with-padding linen">
<div id="show_msg"><?php
if ($this->form_validation->error_string() != "") {
	$this->load->view('user_pages/inc_msg');
}
?></div>
<form method="post"
	action="<?php echo site_url(); ?>user_form/user_insert"
	name="user_module_form">
<div class="heading" style="padding: 10px">
<h4>User Name<span style="color: red; font-weight: normal;"> *</span></h4>
<div class="inner_heading" style="margin-top: -8px;"><input type="text"
	class="input" id="user_name" name="user_name"
	onblur="check_availability();" placeholder="User Name"
	value="<?php if (isset($this->form_validation->error_string)) {
                    echo $this->input->post('user_name');
                } ?>">&nbsp <span class="info-spot"><span
	class="icon-info-round"></span><span style="min-width: 200px"
	class="info-bubble">User Name to use at the time of login</span></span>
<div id="username_availability_result"></div>
</div>
</div>
<div class="heading" style="padding: 10px">
<h4>User Password<span style="color: red; font-weight: normal;"> *</span></h4>
<div class="inner_heading" style="margin-top: -8px;"><input
	type="password" placeholder="Password" class="input" id="password"
	name="password"
	value="<?php if (isset($this->form_validation->error_string)) {
                    echo $this->input->post('password');
                } ?>">&nbsp;&nbsp;<span class="info-spot"><span
	class="icon-info-round"></span><span style="min-width: 200px"
	class="info-bubble">Choose password at least 6 characters</span></span>
</div>
</div>
<div class="heading" style="padding: 10px">
<h4>Confirm Password<span style="color: red; font-weight: normal;"> *</span></h4>
<div class="inner_heading" style="margin-top: -8px;"><input
	type="password" placeholder="Confirm Password" class="input"
	id="passconf" name="ConfirmPassword">&nbsp;&nbsp;<span
	class="info-spot"><span class="icon-info-round"></span><span
	style="min-width: 200px" class="info-bubble">Confirm password</span></span>
</div>
</div>

<div class="heading" style="padding: 10px">
<h4>User Display Name</h4>
<div class="inner_heading" style="margin-top: -8px;"><input type="text"
	class="input" id="user_display_name" name="user_display_name"
	value="<?php if (isset($this->form_validation->error_string)) {
                    echo $this->input->post('user_display_name');
                } ?>"
	placeholder="User Display Name">&nbsp;&nbsp;<span class="info-spot"><span
	class="icon-info-round"></span><span style="min-width: 200px"
	class="info-bubble">Display name to be displayed on dashboard</span></span>
</div>

</div>
<div class="heading" style="padding: 10px">
<h4>User Status</h4>
<div class="inner_heading" style="margin-top: -8px;"><input type="radio"
	name="isDeleted" class="radio" value="1" CHECKED>Active &nbsp; <input
	type="radio" name="isDeleted" class="radio" value="0">Inactive &nbsp; <span
	class="info-spot"><span class="icon-info-round"></span><span
	style="min-width: 200px" class="info-bubble">Active Or Inactive user</span></span>
</div>
</div>
<div class="heading" style="padding: 10px">
<h4>Profile Assign<span style="color: red; font-weight: normal;"> *</span></h4>
<div class="inner_heading" style="margin-top: -8px;"><select
	name="profile_id" class="select" id="selectclass"
	onfocus="myFunction();" onblur="myFunction1();";>
	<option value="">Select Profile</option>
	<?php foreach ($user_profile as $profile) { ?>
	<option value="<?php echo $profile['profile_id']; ?>"><?php echo $profile['profile_name']; ?></option>
	<?php } ?>
</select> &nbsp;&nbsp <span class="info-spot"><span
	class="icon-info-round"></span><span style="min-width: 200px"
	class="info-bubble">Grant privileges to user</span></span></div>
</div>
<div style="padding: 10px">
<button class="button glossy mid-margin-right" type="submit"><span
	class="button-icon"><span class="icon-tick"></span></span> Submit</button>
<button class="button glossy" type="button"
	onclick="window.location = '<?php echo  base_url();?>user_form/all_user' " />
<span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
Cancel
</button>
</div>
</form>
</div>
</div>
</section>
	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_left_page.php'; ?>
	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_middle_rightbar.php'; ?>
	<?php include BASE_PATH . '/application/views/main_pages/others/dashboard_footer.php'; ?>
<script>
    document.getElementById("user_name").focus();
</script>
<script>
    function myFunction()
    {
        $("#selectclass").addClass("input focus");
    }
    function myFunction1()
    {
        $("#selectclass").removeClass("input focus");
    }
</script>
<script>
function check_availability(){  
        //get the username  
        var username = $('#user_name').val();  
        //alert(username);
        //use ajax to run the check  
        $.post("check_username", { username: username },  
            function(result){  
                //if the result is 1  
                if(result == 1){  
                    //show that the username is available  
                    $('#username_availability_result').html(username + ' is already taken.Please choose new one'); 
                    document.getElementById("user_name").focus();
                    $("#username_availability_result").css('color','red');
                }else{  
                    //show that the username is NOT available  
                    $('#username_availability_result').html(username + ' is Available');  
                    $("#username_availability_result").css('color','white');
                }  
                
        });  
  
} 
</script>
</body>
</html>

