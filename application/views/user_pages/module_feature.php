<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_page.php';?>

<body>


<?php error_reporting(E_ALL ^ E_NOTICE); ?>

<header role="banner" id="title-bar">
<h2><?php echo HOTEL_NAME ;?></h2>
</header>

<!-- Button to open/hide menu -->
<a href="#" id="open-menu"><span>Menu</span></a>

<!-- Button to open/hide shortcuts -->
<a href="#" id="open-shortcuts"><span class="icon-thumbs"></span></a>
<section role="main" id="main">
<!-- Main content -->

<noscript class="message black-gradient simpler">Your browser does not
support JavaScript! Some features won't work as expected...</noscript>

<hgroup id="main-title" class="thin">
<h1>Create Profile</h1>

</hgroup>
</section>
<div class="center" style="margin: 150px;";>
<div id="container">
<div id="show_msg" align="center"><?php
if ($this->form_validation->error_string()!="")
{
	$this->load->view('user_pages/inc_msg');
}
?></div>
<div id="container"><!--content-->
<div id="content"><!--main content-->
<div id="main-content">
<form method="post"
	action="<?php echo site_url();?>user_form/insert_user_profile"
	name="user_role_form">


<div class="heading" style="padding: 10px">
<h4>Profile Name</h4>
<div class="inner_heading" style="margin-top: -8px;"><input type="text"
	class="input icon-lock focus" id="profile_name" name="profile_name"
	placeholder="Profile Name"></div>
</div>
<div class="heading" style="padding: 10px">
<h4>Profile Display Name</h4>
<div class="inner_heading" style="margin-top: -8px;"><input type="text"
	class="input icon-lock focus" id="profile_display_name"
	name="profile_display_name" placeholder="Profile Display Name"></div>
</div>
<h4>Module Assign</h4>
<?php foreach ($categories as $list){ ?>
<ul class="checkboxTree" id="checkboxes" style="list-style: none;">
	<li style="list-style: none;"><input type="checkbox"
		id="category<?php echo $list->categories_id;?>" name="category[]"
		value="<?php echo $list->categories_id;?>" /><?php echo $list->categories_name;?>
		<?php $query="SELECT module_id,module_name FROM dashboard_module WHERE categories_id=$list->categories_id;";
		$query = $this->db->query($query);
		$result=$query->result_array() ;
		foreach($result as $val){?>
	<ul id="ls1" style="list-style: none;">
		<li style="list-style: none;"><input type="checkbox"
			id="module_<?php echo $val['module_id'];?>"
			name="module[<?php echo $list->categories_id;?>_<?php echo $val['module_id'];?>]"
			value="<?php echo $val['module_id'];?>" /><?php echo $val['module_name'];?>
		</li>
	</ul>
	<?php } ?></li>
</ul>
	<?php } ?> <input type="submit" value="Submit"></form>
</div>
<!--/main content--></div>
<!--/content--></div>
<script
	src="<?php echo get_assets_path('js');?>dashboard/libs/jquery-1.7.2.min.js"></script>
<script>
(function($){
   
	$('ul#checkboxes input[type="checkbox"]').each (
		function () {
                   
			$(this).bind('click change', function (){
				if($(this).is(':checked')) {
					$(this).siblings('ul').find('input[type="checkbox"]').attr('checked', 'checked');
					$(this).parents('ul').siblings('input[type="checkbox"]').attr('checked', 'checked');
                } else {
                    $(this).siblings('ul').find('input[type="checkbox"]').removeAttr('checked', 'checked');
                }
			});
		}
	);
	
})(jQuery);</script></div>
</div>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_left_page.php';?>

	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_middle_rightbar.php';?>
	<?php include BASE_PATH.'/application/views/main_pages/others/dashboard_footer.php';?>

</body>
</html>


