<?php
/************************************************************************************************************************************************
 * 																																			*
 * 											Project Name	: Digivalet Social Network														*									*
 * 											Author			: Paras Sahu		 															*
 * 											Creation Date	: 4 July 2013 - 22 July 2013													*										*
 * 											Snippet Name	: DSN Wall Updates Library														*
 **************************************************************************************************************************************************/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wall_Updates {

	public function getCIinstance()
	{
		$ci =& get_instance();
		$db_dsn = $ci->load->database('local', TRUE);
		return $db_dsn;
	}
	// Updates
	public function Updates($uid="", $from=0, $to=10)
	{
		$db_dsn = $this -> getCIinstance();
		$ci =& get_instance();
		if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
		{
			if(($ci -> uri -> segment(4))!="" && ($ci -> uri -> segment(5))!="")
			{
				$from 		= 	$ci -> uri -> segment(4);
				$to 		= 	$ci -> uri -> segment(5);
			}
		}
		$query = $db_dsn -> query("SELECT M.msg_id,
												M.uid_fk, 
												M.message, 
												M.attached_image,
												M.created,
												M.inappropriate_p,
												U.username,
												U.user_image,
												L.uid,
												L.msg_id_fk,
												L.like_status
												FROM messages M 
												INNER JOIN users U  
												ON M.uid_fk=U.uid 
												LEFT JOIN likes L
												ON M.msg_id=L.msg_id_fk
												order by M.msg_id desc 
												limit $from , $to
												");
		if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
		{
			$row = $query -> result_array();
			$i=0;
			
			foreach ($row as $iData)
			{
				foreach($iData as $key => $value)
				{
					if($key=='msg_id')
					{
						$this -> Comments($value, 1);
						$row[$i]['no_of_comments'] = count($this -> Comments($value, 1));
						$i++;
					}
				}
			}
		}
		else 
		{
			$row = $query -> result();
		}
		
		foreach($row as $iRow)
		{
			$data[]=$iRow;
		}
		if(isset($data) && count($data) > 0)
		{
			return $data;
		}
		else
		{
			return false;
		}
	}
	//Comments
	public function Comments($msg_id="", $mode=0)
	{
		$db_dsn = $this -> getCIinstance();
		$ci =& get_instance();
		if($msg_id=="")
		{
			$msg_id = $ci -> input -> post('msg_id');
		}
		if($mode==0)
		{
			if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
			{
				$msg_id = $ci -> uri -> segment(4);
			}
		}
		
		$query = $db_dsn -> query("SELECT C.msg_id_fk, C.com_id, C.uid_fk, C.comment, C.created, C.inappropriate, U.username, U.user_image FROM comments C, users U WHERE C.uid_fk=U.uid and C.msg_id_fk='$msg_id' order by C.com_id DESC ") or die(mysql_error());
		
		if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
		{
			$row = $query -> result_array();
			$timeStampObj = new Time_stamp();
			$i=0;
			foreach($row as $iContent)
			{
				foreach($iContent as $key => $value)
				{
					if($key=='created')
					{
						$row[$i]["created"] = $timeStampObj -> time_stamp_evaluate($value);
						$i++;
					}
				}
			}
		}
		else 
		{
			$row = $query -> result();
		}
		$data = array();
		foreach($row as $iRow)
		{
			$data[]=$iRow;
		}
		return $data;
	}
	
	//likes

	public function likes($msg_id="")
	{
		$db_dsn = $this -> getCIinstance();
		$ci =& get_instance();

		if($msg_id=="")
		{
			$msg_id = $ci -> input -> post('msg_id');
		}
		$query = $db_dsn -> query("SELECT L.like_status, L.msg_id_fk, L.uid FROM likes L WHERE L.msg_id_fk='$msg_id' and L.like_status=1");
		$row = $query -> result();
		$data = array();
		foreach($row as $iRow)
		{
			$data[]=$iRow;
		}
		return $data;
	}

	//Avatar Image
	public function Gravatar($uid)
	{
		$db_dsn = $this -> getCIinstance();
		$query = $db_dsn -> query("SELECT * FROM `users` WHERE uid='$uid'") or die(mysql_error());
		$row=$query -> result();

		if(!empty($row))
		{
			$email=$row[0] -> email;
			$lowercase = strtolower($email);
			$imagecode = md5( $lowercase );
			$data=get_assets_path('dsn_user_image').$row[0] -> user_image;
			return $data;
		}
		else
		{
		 $data="default.jpg";
		 return $data;
		}
	}

	//Insert Update
	public function Insert_Update($uid, $update)
	{
		$update		=	urldecode(htmlentities($update));
		$time		=	time();
		$ip			=	$_SERVER['REMOTE_ADDR'];
		$db_dsn 	= 	$this -> getCIinstance();
		$query 		= 	$db_dsn -> query("SELECT msg_id,message FROM `messages` WHERE uid_fk='$uid' order by msg_id desc limit 1");
		$result 	= 	$query -> row_array();
		$query 		= $db_dsn -> query("INSERT INTO `messages` (message, uid_fk, ip,created) VALUES ('$update', '$uid', '$ip','$time')") or die(mysql_error());
		$newquery 	= $db_dsn -> query("SELECT M.msg_id, M.uid_fk, M.message, M.created, U.username FROM messages M, users U where M.uid_fk=U.uid and M.uid_fk='$uid' order by M.msg_id desc limit 1 ");
		$result 	= $newquery -> row_array();
		return $result;
	}

	public function Insert_Update_Image($uid, $update, $image)
	{
		$update		=	htmlentities($update);
		$time		=	time();
		$ip			=	$_SERVER['REMOTE_ADDR'];
		$db_dsn 	= 	$this -> getCIinstance();
		$query 		= 	$db_dsn -> query("SELECT msg_id,message, attached_image FROM `messages` WHERE uid_fk='$uid' order by msg_id desc limit 1");
		$result 	= 	$query -> row_array();
		$query 		= $db_dsn -> query("INSERT INTO `messages` (attached_image, message, uid_fk, ip,created) VALUES ('$image', '$update', '$uid', '$ip','$time')") or die(mysql_error());
		$newquery 	= $db_dsn -> query("SELECT M.msg_id, M.uid_fk, M.message, M.attached_image, M.created, U.username FROM messages M, users U where M.uid_fk=U.uid and M.uid_fk='$uid' order by M.msg_id desc limit 1 ");
		$result 	= $newquery -> row_array();
		return $result;
	}

	//Delete update
	public function Delete_Update($uid, $msg_id)
	{
		$db_dsn 		= 	$this -> getCIinstance();
		$query = $db_dsn -> query("DELETE FROM `comments` WHERE msg_id_fk = '$msg_id' ") or die(mysql_error());
		$query = $db_dsn -> query("DELETE FROM `messages` WHERE msg_id = '$msg_id' and uid_fk='$uid'") or die(mysql_error());
		return true;
	}
	//Like update
	public function Like_Update($uid, $msg_id)
	{
		$db_dsn 		= 	$this -> getCIinstance();
		$ip				=	$_SERVER['REMOTE_ADDR'];
		$time			=	time();
		$query 			= 	$db_dsn -> get_where('likes', array('msg_id_fk' => $msg_id));
		$noOfRecords 	= 	$query -> num_rows();
		if($noOfRecords>0)
		{
			$db_dsn -> query("UPDATE `likes` SET `like_status`=NOT(`like_status`) WHERE msg_id_fk='$msg_id'");
		}
		else
		{
			$db_dsn -> query("INSERT INTO likes(`like_status`, `msg_id_fk`, `uid`, `ip`, `created`) values(1, '$msg_id', '$uid', '$ip', '$time') ");
		}

		return true;
	}

	//Insert Comments
	public function Insert_Comment($uid, $msg_id, $comment)
	{
		$comment			=		urldecode(htmlentities($comment));
		$time				=		time();
		$ip					=		$_SERVER['REMOTE_ADDR'];
		$db_dsn 			= 		$this -> getCIinstance();
		$query 				= 		$db_dsn -> query("SELECT com_id, comment FROM `comments` WHERE uid_fk='$uid' and msg_id_fk='$msg_id' order by com_id desc limit 1 ");

		$result 			= 		$query -> row_array();
		//print_r($result);
		/*if ( $comment!=$result['comment'] )
		 {*/
		$query = $db_dsn -> query("INSERT INTO `comments` (comment, uid_fk,msg_id_fk,ip,created) VALUES ('$comment', '$uid','$msg_id', '$ip','$time')");
		$newquery = $db_dsn -> query("SELECT C.com_id, C.uid_fk, C.comment, C.msg_id_fk, C.created, U.username FROM comments C, users U where C.uid_fk=U.uid and C.uid_fk='$uid' and C.msg_id_fk='$msg_id' order by C.com_id desc limit 1 ");
		$result = $newquery -> row_array();
			
		return $result;
		/*}
		 else
		 {
			return false;
			}*/

	}

	//Delete Comments
	public function Delete_Comment($uid, $com_id)
	{
		$db_dsn 		= 	$this -> getCIinstance();
		$query = $db_dsn -> query("DELETE FROM `comments` WHERE uid_fk='$uid' and com_id='$com_id'");
		return true;
	}
	public function get_real_time_updates()
	{
		$db_dsn 			= 		$this -> getCIinstance();
		$time				=		time();
		$ci 				=		& get_instance();
		$l_like_id			=		$ci -> uri -> segment(3);
		$l_com_id			=		$ci -> uri -> segment(4);
		$result_likes 		= 		NULL;
		$result_comments 	=	 	NULL;
		$like_data			=		array();
		$comment_data		=		array();
		$query_like_id 		= 		$db_dsn -> query("SELECT l.*, u.*, m.* FROM likes l LEFT JOIN users u ON l.uid=u.uid LEFT JOIN messages m ON m.msg_id=l.msg_id_fk WHERE like_id > '$l_like_id'");
		$query_com_id 		=	 	$db_dsn -> query("SELECT c.*, u.* FROM comments c LEFT JOIN users u ON c.uid_fk=u.uid WHERE c.com_id > '$l_com_id' ");

		if($query_like_id -> num_rows() > 0)
		{
			$result_likes			=	 $query_like_id -> result();
			foreach($result_likes as $iRow)
			{
				$like_data[]		=	$iRow;
			}
		}
		if($query_com_id -> num_rows() > 0)
		{
			$result_comments		=	$query_com_id -> result();
			foreach($result_comments as $iRow)
			{
				$comment_data[]		=	$iRow;
			}
		}
		echo json_encode(array($like_data, $comment_data));
	}
	function get_l_c_id()
	{
		$db_dsn 				= 		$this -> getCIinstance();
		$result_like_id			=		NULL;
		$result_comment_id		=		NULL;

		$query_like_id 			= 		$db_dsn -> query("SELECT `like_id` FROM likes ORDER BY like_id DESC limit 1");
		$query_comment_id 		= 		$db_dsn -> query("SELECT `com_id` FROM comments ORDER BY com_id DESC limit 1");
		$result_like_id			=		$query_like_id -> row_array();
		$result_comment_id		=		$query_comment_id -> row_array();

		if($result_like_id!=NULL)
		{
			$result_like_id			=		$result_like_id['like_id'];
		}
		if($result_comment_id!=NULL)
		{
			$result_comment_id		=		$result_comment_id['com_id'];
		}

		echo json_encode(array($result_like_id, $result_comment_id));
	}
	function get_m_id()
	{
		$db_dsn 				= 		$this -> getCIinstance();
		$result_m_id			=		NULL;

		$query_message_id 		= 		$db_dsn -> query("SELECT `msg_id` FROM messages ORDER BY msg_id DESC limit 1");
		$result_message_id		=		$query_message_id -> row_array();

		if($result_message_id!=NULL)
		{
			$result_message_id			=		$result_message_id['msg_id'];
		}

		echo json_encode(array($result_message_id));
	}
	function get_message_by_id($msg_id)
	{
		$db_dsn = $this -> getCIinstance();
		$ci =& get_instance();
		if($msg_id!="")
		{
			$query = $db_dsn -> query("SELECT m.*, c.*, l.*, u.* FROM messages m LEFT JOIN comments c ON m.msg_id=c.msg_id_fk LEFT JOIN likes l ON m.msg_id=l.msg_id_fk LEFT JOIN users u ON m.uid_fk=u.uid WHERE m.msg_id='$msg_id'");

			$row = $query -> result();

			$data = array();
			foreach($row as $iRow)
			{
				$data[]=$iRow;
			}
			return $data;
		}

	}
	function get_real_time_wall_updates()
	{
		$db_dsn 			= 		$this -> getCIinstance();
		$time				=		time();
		$ci 				=		& get_instance();
		$m_id				=		$ci -> uri -> segment(3);
		$result_messages 	= 		NULL;
		$message_data		=		array();
		$query_message_id 	= 		$db_dsn -> query("SELECT m.*, l.*, c.*, u.*  FROM messages m LEFT JOIN users u ON m.uid_fk=u.uid LEFT JOIN likes l ON m.msg_id=l.msg_id_fk LEFT JOIN comments c ON m.msg_id=c.msg_id_fk WHERE msg_id > '$m_id'");

		if($query_message_id -> num_rows() > 0)
		{
			$result_messages	=	 $query_message_id -> result();
			foreach($result_messages as $iRow)
			{
				$message_data[]		=	$iRow;
			}
		}

		echo json_encode(array($message_data));
	}
}
