<?php

/**
 * Description of User
 *
 * @author pbsl
 */
###### User Data Format ######
//Array (
//  [0] => Array ( [uid] => 1 [name] => admin [pass] => MTExMTEx [mail] => admin@gmail.com [created] => 0 [access] => 0 [login] => 0 [status] => 0 [picture] => )
//  [roles] => Array ( [1] => administrator [2] => authenticate )
//  [perm] => Array ( [0] => all [1] => access content [2] => view reports [3] => add user [4] => delete user [5] => view profile ) )
//
########### End User data Format #########

class user {

	var $user = array();
	var $access_perm = array();
	var $CI;

	public function user() {
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->load->model('user_model');
		$this->CI->load->model('common_model');
		//$this->CI->load->library('watchdog');
		$this->user = $this->CI->session->userdata('user');
	}

	/***
	 * encode_password()
	 * @param password
	 * Description: encoding user password
	 ***/
	public function encode_password($password) {
		return base64_encode($password);
	}

	/***
	 * decode_password()
	 * @param password
	 * Description: decoding user password
	 ***/
	public function decode_password($pssword) {
		return base64_decode($password);
	}

	/***
	 * set_user_info()
	 * @param user data
	 * Description: setting user data
	 ***/
	public function set_user_info($user) {
		$this->CI->session->set_userdata(array('user'=>$user));
	}

	public function unset_user_info() {
		$this->CI->session->unset_userdata(array('user'=>''));
		$this->access_perm = array();
	}

	public function is_admin() {
		$roles = $this->get_user_roles();
		if(count($roles) > 0 && in_array('administrator', $roles)) {
			return true;
		}
		else {
			return false;
		}
	}

	/***
	 * get_user_info()
	 * @param key (field of session data)
	 * Description: getting user data
	 ***/
	public function get_user_info($key = "") {
		$data = $this->CI->session->userdata('user');
		//print_r($data);
		if($key != "") {
			return $data[0][$key];
		}
		else {
			return $data;
		}
	}

	public function get_users_id() {
		if($this->user[0]['uid'] > 0) {
			return $this->user[0]['uid'];
		}
		else {
			return false;
		}
	}

	public function get_user_name() {
		if($this->user[0]['uid'] > 0) {
			return $this->user[0]['name'];
		}
		else {
			return false;
		}
	}

	public function get_user_roles() {
		if($this->user[0]['uid'] > 0) {
			return $this->user['roles'];
		}
		else {
			return false;
		}
	}

	public function get_user_permissions() {
		if($this->user[0]['uid'] > 0) {
			return $this->user['perm'];
		}
		else {
			return false;
		}
	}

	/***
	 * user_load()
	 * @param: user id
	 * Loading users all information
	 ***/
	public function user_load($uid) {
		if($uid > 0) {
			$user_info = array();
			$permissions = array();
			$q = $this->CI->common_model->get_records_by_field('users', 'uid', $uid);
			$res = $q->result_array();
			$roles_query = $this->CI->user_model->user_roles($uid);
			$roles_res = $roles_query->result_array();
			$roles = object_to_array($roles_res);
			if(count($roles) > 0) {
				$tmp_roles = implode(",", array_keys($roles));
				$permissions_query = $this->CI->user_model->user_permission($tmp_roles);
				$permissions_res = $permissions_query->result_array();
				$permissions = permissions_to_array($permissions_res);
			}

			$user_info[] = $res[0];
			$user_info['roles'] = $roles;
			$user_info['perm'] = $permissions;
			return $user_info;
		}
		else {
			return 0;
		}
	}
	/***
	 * set_access_permission()
	 * @argument array(perm1, perm2)
	 * setting user permissions in
	 * every controller
	 ***/
	public function set_access_permission($perms = array()) {
		if(count($perms) > 0)  {
			foreach($perms as $perm) {
				if( count( $this->is_permission_exists($perm) ) == 0 ){
					$this->save_permission($perm);
				}
			}
		}

		$this->access_perm = $perms;
	}

	/***
	 * save_permission()
	 * @argument: permission name
	 **/
	public function save_permission($perm_name) {
		$this->CI->common_model->save_permission($perm_name);
	}

	/***
	 * is_permission_exist($perm_name)
	 * @argument: permission name
	 ***/
	public function is_permission_exists($perm_name) {
		$row = array();
		$res = $this->CI->common_model->is_permission_exist($perm_name);
		$row = $res->result();
		if(isset($row[0])) {
			return $row[0];
		}
		else {
			return $row;
		}
	}

	/***
	 * is_user_access()
	 * checking controller permission
	 * and users permission is same
	 * if same return true else return false
	 * @return true or false
	 ****/
	public function is_user_access($perm="") {
		$perms = $this->get_user_permissions();
		$user_perms  = $this->access_perm;
		$access = FALSE;

		if($perm != "") {
			return $this->is_user_menu_access($perm);
		}


		if(count($user_perms) > 0 && !empty($perms)) {
			foreach($perms as $perm) {
				foreach($user_perms as $uperm) {
					if($perm == $uperm) {
						$access = TRUE;
					}
				}
			}
		}

		return $access;
	}


	/**
	 * is_user_menu_access($perm)
	 * Checking menu permission
	 * if user have access menu
	 * return true
	 * else
	 * return false
	 * **/

	public function is_user_menu_access($perm) {
		$perms = $this->get_user_permissions();
		return in_array($perm, $perms);
	}


	/***
	 * redirecting access denied page
	 * if userdo not have permission
	 * to access any page
	 **/
	public function user_access_denied() {
		$this->CI->watchdog->save('error', 'user', 6);
		redirect('permission/access_denied');
	}
} // End of class
?>