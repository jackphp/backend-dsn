<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of theme_renderer
 *
 * @author pbsl
 */
class theme {
	var $CI;
	public function theme() {
		$this->CI =& get_instance();
		$this->CI->load->library("menu");
		$this->CI->load->library("message");
		$this->CI->load->model("dashboard_login");
	}



	public function theme_vars($content_page, $data, $param = array()) {
		//echo $page; die();
		$menus['menus']                   =     $this->CI->menu->load_menu();
		$page_message['page_messages']     =     $this->CI->message->get();
		$page['header']                   =     $this->CI->load->view('inc/header', $data, TRUE);
		$page['page_message']              =     $this->CI->load->view('inc/message', $page_message, TRUE);
		$page['menu']                     =     $this->CI->load->view('inc/menu', $menus, TRUE);
		$page['footer']                   =     $this->CI->load->view('inc/footer', $data, TRUE);
		$page['content']                  =     $this->CI->load->view($content_page, $data, TRUE);
		if(isset($param['left']) && $param['left'] != "") {
			$data['left_action'] = $param['left_items']['left_action'];
			$page['left'] = $this->CI->load->view($param['left'], $data, TRUE);
			 
			if(isset($param['left_items']) && $param['left_items'] != "") {
				$page['active_menu_js'] = $this->activ_menu_tab($param['left_items']['tab'], $param['left_items']['menu1'], $param['left_items']['menu2']);
			}
		}

		if(isset($param['right']) && $param['right'] != "") {
			$page['right'] = $this->CI->load->view($param['right'], $data, TRUE);
		}

		$result = $this->CI->dashboard_login->getUserInfo();
		$page['user_info']=$result;
		$page['profile']=$this->CI->dashboard_login->user_profile();

		$page['left']=$this->CI->dashboard_login->user_profile();



		return $page;
	}

	/***
	 * set_col_left($param)
	 * @argument: $param
	 * @argument type: array()
	 */
	public function set_col_left($param) {

	}

	/***
	 * generate_list()
	 * @arguments $header, $rows
	 * $header format: array(array('data'=>item1), array('data'=>item2, 'sorting_field'=>'field_name', 'url'=>'where the sorted data fatched'))
	 * $row format: array('data'=>array('data'=>item1, 'attributes'=>array('class'=>'class_name', 'onclick'=>'function_name')), 'attributes'=>array('class'=>'class_name', 'onclick'=>'function_name'))
	 **/
	function generate_list($header, $rows) {
		$output = "";

		if(count($header) > 0 ) {
			$output .= "<thead>";
			if(isset($header['class'])) {
				$output .= "<tr class='".$header['class']."'>";
				unset($header['class']);
			}
			else {
				$output .= "<tr>";
			}

			// Parsing header
			foreach($header as $item) {
				if(isset($item['data'])) {
					if(isset($item['sort_field']) && isset($item['url'])) {
						$output .= "<th style='vertical-align:middle'><a href='".$item['url']."'>".$item['data']."</a></th>";
					}
					else {
						$output .= "<th style='vertical-align:middle' align='center'>".$item['data']."</th>";
					}
				}
			}

			// Ending Header Row
			$output .= '</tr>';
			$output .= "</thead>";
		}

		if(count($header)) {
			$output .= '<tbody id="table_body">';
		}

		if(count($rows) > 0 && count($rows) > 0) {

			// Parsing Rows
			$cnt = 0;
			foreach($rows as $row) {
				$rowCnt = count($row);
				if(isset($row['attributes']) && is_array($row['attributes'])) {
					$attribs = "";
					foreach($row['attributes'] as $key=>$val) {
						$attribs .= ' '.$key.'="'.$val.'"';
					}
					$output .= "<tr ".$attribs.">";
					$rowCnt -= 1;
				}
				else{
					$output .= "<tr>";
				}
				 
				for($i=0; $i<$rowCnt; $i++) {
					//print_r($row); die();
					$attribs = "";
					if(isset($row[$i]['attributes'])) {
						foreach($row[$i]['attributes'] as $key=>$val) {
							$attribs .= ' '.$key.'="'.$val.'"';
						}
						$output .= "<td '".$attribs."'>".$row[$i]['data']."</td>";
					}
					else {
						$output .= "<td>".$row[$i]['data']."</td>";
					}
				}

				$output .= "</tr>";
				 
				$cnt++;
			}

			if(count($header) > 0) {
				$output .= "</tbody>";
				$output = "<table cellpadding='2' id='table' cellspacing='2' width='100%' class='table'>".$output."</table>";
				return $output;
			}
			else {
				return $output;
			}
		}
		else {
			$output .="<tr><td align='center' colspan='8'><h4>No menuitem found</h4></td></tr></tbody> ";
			$output = "<table cellpadding='2' id='table' cellspacing='2' width='100%' class='table'>".$output."</table>";
			return $output;
		}
	}


	function activ_menu_tab($tab=0, $menu1, $menu2="") {
		$js = '$( "#accordion" ).accordion({ active: '.$tab.' });';
		$js .= '$("#accordion").children("h3").each(function(){
                                        $(this).next("div").children("a").children("div").removeClass("selected");
                                        $(this).next("div").children("a").children("div").addClass("tab");
                                 });';

		$js .= '$("#accordion").children("h3").eq('.$tab.').next("div").children("a").children("div").each(function(){';
		if($menu2 != "") {
			$js .= 'if($(this).parent("a").attr("class") == "expand") {
                    $(this).parent("a").siblings("div").children("a").children("div").each(function(){
                         //alert("reached..!");
                        if($(this).text() == "'.$menu2.'") {
                            $(this).removeClass("tab");
                            $(this).addClass("selected");
                        }
                    });
                    
                    $(this).parent("a").siblings("div").show();
                    $(this).parent("a").removeClass("expand");
                    $(this).parent("a").addClass("colasp");
                    $(this).parent("a").children("div").children("span").text("-");
                }';
		}

		$js .= 'if($(this).text() == "'.$menu1.'") {
                  $(this).removeClass("tab");
                  $(this).addClass("selected");
                }
            });';

		return $js;
	}

} // End of class
?>
