<?php
/**
 * Description of Menu
 *
 * @author pbsl
 */
class menu {

	var $CI;
	public function menu() {
		$this->CI =& get_instance();
		$this->CI->load->library('user');
		$this->CI->load->model('menu_model');
	}

	public function load_menu() {
		$uid = $this->CI->user->get_users_id();
		if($uid == "") {
			return array(anchor('main', 'Home'), anchor('login', 'Login'));
		}
		else if($uid) {

			$menu = array();

			$menu[] = anchor('home', 'Home');
			$userObj = $this->CI->user->get_user_info();
			$permession = $userObj['perm'];

			$perms = array();
			foreach($permession as $p) {
				$perms[] = "'".$p."'";
			}


			$perm_str = '';
			if(count($perms) > 0) {
				$perm_str = implode(",", $perms);
			}

			$q = $this->CI->menu_model->load_application_menu($perm_str);

			foreach($q->result() as $m) {
				if($m->name != "") {
					if( array_key_exists($m->name, $menu) ) {
						array_push($menu[$m->name], array(anchor($m->path, $m->label)));
					}
					else {
						$menu[$m->name] = array(anchor($m->path, $m->label));
					}
				}
				else {
					$menu[] = anchor($m->path, $m->label);
				}
			}

			//echo "<pre>"; print_r($menu); die();
			///echo "<pre>"; print_r($menu); die();
			//            if($this->CI->user->is_user_menu_access('user management')) {
			//                $menu['User Management'] = array(anchor('admin/users', 'Users'), anchor('admin/roles', 'Roles'), anchor('admin/departments', 'Departments'), anchor('admin/permissions', 'Permissions'));
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('room management')) {
			//                $menu['Room Management'] = array(anchor('room_management', 'Floors'), anchor('room_management/rooms', 'Rooms'),anchor('#', 'DigiValet Status'));
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('access ats reports')) {
			//                $menu['Reports'][0] = anchor('ats', 'ATS');
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('access rms reports')) {
			//                $menu['Reports'][1] = anchor('rms', 'RMS');
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('access food reports')) {
			//                $menu['Food Management'] = array(anchor('http://192.168.0.56/food_menu', 'Food ', 'target="blank"'), anchor('food', 'Reports'));
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('access e-merchant reports')) {
			//                $menu['e-Merchant Management'] = array(anchor('http://192.168.0.56/drupal/?q=node/36', 'Inroom Shopping', 'target="blank"'), anchor('#', 'Reports'));
			//            }
			//
			//            if($this->CI->user->is_user_menu_access('access activity')) {
			//                $menu[] = anchor('activity', 'Audit Trail');
			//            }

				$menu['My Account'] = array(anchor('admin/user/edit/'.$uid, 'Profile'), anchor('login/logout', 'Logout'));

				//            array(anchor('home', 'Home'),
				//                    'User Management'=>array(anchor('admin/users', 'Users'), anchor('admin/roles', 'Roles'), anchor('admin/departments', 'Departments'), anchor('admin/permissions', 'Permissions')),
				//                    'Room Management'=>array(anchor('room_management', 'Floors'), anchor('room_management/rooms', 'Rooms'),anchor('#', 'DigiValet Status')),
				//                    'Reports '=>array(anchor('ats', 'ATS'), anchor('rms', 'RMS')),
				//                    'Food Management'=>array(anchor('http://192.168.0.56/food_menu', 'Food ', 'target="blank"'), anchor('#', 'Reports')),
				//                    'e-Merchant Management'=>array(anchor('http://192.168.0.56/drupal/?q=node/36', 'Inroom Shopping', 'target="blank"'), anchor('#', 'Reports')),
				//                     anchor('activity', 'Audit Trail'),
				//                    'My Account'=> array(anchor('admin/user/edit/'.$uid, 'Profile'), anchor('login/logout', 'Logout')),
				//                );

				return $menu;
			}
		}


		public function set_menu($menus) {
			$mid = 0;
			$m = $menus['menu'];
			$links = $menus['links'];

			if($m != "") {
				//echo $m; die();
				$q = $this->CI->menu_model->is_menu_exist($m);
				$menu_row = $q->result();

				if(isset($menu_row[0]->mid) && $menu_row[0]->mid > 0) {
					$mid = $menu_row[0]->mid;
				}

				if(!$mid) {
					$mid = $this->CI->menu_model->save_menu($m);
				}
				else {
					$this->CI->menu_model->save_menu($m, $mid);
				}
			}

			foreach($links as $link) {
				$q1 = $this->CI->menu_model->is_menu_link_exist($mid, $link['label']);
				$link_row = $q1->result();
				if(isset($link_row[0]->link_id) && $link_row[0]->link_id > 0) {
					$link_id = $link_row[0]->link_id;
				}
				else {
					$link_id = 0;
				}

				if(!$link_id) {
					$this->CI->menu_model->save_menu_link(array($mid, $link['path'], $link['label'], $link['permission'], $link['menu_order']));
				}
				else {
					$this->CI->menu_model->save_menu_link($link, $link_id);
				}
			}

		}


	}
	?>