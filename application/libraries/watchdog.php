<?php
/**
 * Description of watchdog
 *
 * @author pbsl
 */
class watchdog {
	var $uid;
	var $instance;
	var $server_param;

	public function  __construct() {
		$this->instance =& get_instance();
		$this->instance->load->library('user');
		$this->instance->load->model('watchdog_model');
		$this->uid = $this->instance->user->get_users_id();
		$this->server_param = $_SERVER;
	}


	public function save($type, $module, $mid, $row_id=0, $name="") {
		$message = $this->get_message($mid);
		$message = str_replace("%name", $name, $message);
		//(uid, type, message, location, referer, record_unique_val, access_time)
		if(!isset($this->server_param['HTTP_REFERER'])) {
			$this->server_param['HTTP_REFERER'] = $this->server_param['PHP_SELF'];
		}

		$param = array('uid'=>$this->uid, 'type'=>$type, 'module'=>$module,'message'=>$module." ".$message, 'location'=>$this->server_param['PHP_SELF'], 'referer'=>$this->server_param['HTTP_REFERER'], 'ip'=>$this->server_param['REMOTE_ADDR'], 'record_unique_val'=>$row_id, 'access_time'=>time());
		$this->instance->watchdog_model->save($param);
	}

	public function get_message($mid) {
		$messages = $this->messages();
		return $messages[$mid];
	}

	public function messages() {
		$messages = array(
		0=>'add.',
		1=>'update.',
		2=>'delete.',
		3=>'changed.',
		4=>'Room in under maintenance.',
		5=>'Room out of under maintenance.',
		6=>'access denied.',
		7=>'error in changing room status.',
            'menuitem_edit'=>'%name Menuitem edited',
            'menuitem_delete'=>'%name Menuitem deleted',
            'menuitem_add'=>'%name Menuitem added',
            'letter_add'=>'%name Letter added',
            'letter_edit'=>'%name Letter edited',
            'letter_delete'=>'%name Letter deleted',
            'letter_sent'=>'%name Letter Sent',
            'food_category_add'=>'%name Food category added',
            'food_category_edit'=>'%name Food category edited',
            'food_category_delete'=>'%name Food category deleted',
            'food_subcategory_add'=>'%name Food subcategory added',
            'food_subcategory_edit'=>'%name Food subcategory edited',
            'food_subcategory_delete'=>'%name Food subcategory deleted',
            'addon_add'=>'%name Addon added',
            'addon_edit'=>'%name Addon edited',
            'addon_deleted'=>'%name Addon deleted',
            'addon_family_add'=>'%name Addon Family added',
            'addon_family_edit'=>'%name Addon Family edited',
            'adddon_family_deleted'=>'%name Addon Family deleted',
            'update_room'=>'%name Room Updated',
            'channel_add'=>'%name Channel added',
            'channel_edit'=>'%name Channel edited ',
            'channel_disable'=>'%name Channel disabled',
            'channel_category_add'=>'%name Channel category added',
            'channel_category_edit'=>'%name Channel category edited ',
            'channel_category_delete'=>'%name Channel category deleted',
            'shopping_pdf_changed'=>'%name Shopping PDF Changed ',
            'shopping_pdf_update'=>'%name Shopping PDF Updated in all Rooms ',
            'channel_update'=>'%name Channel Updated in all Rooms ',
            'floor_type_add'=>'%name Floor Type Added ',
            'floor_type_edit'=>'%name Floor Type Edited ',
            'floor_type_delete'=>'%name Floor Type Deleted',
            'floor_type_selected_delete'=>'%name Selected Floor type Deleted',
            'floor_add'=>'%name  Floor Added',
            'floor_edit'=>'%name  Floor Edited',
            'floor_delete'=>'%name  Floor Deleted',
            'floor_selected_delete'=>'%name  Selected Floors Deleted',
            'room_add'=>'%name Room Added ',
            'room_edit'=>'%name Room Edited  ',
            'room_delete'=>'%name Room Deleted ',
            'room_selected_delete'=>'%name Selected Rooms Deleted ',
            'bulk_room_add'=>'%name Bulk Room Added  ',
            'room_type_add'=>'%name Room Type Added  ',
            'room_type_edit'=>'%name Room Type Edited ',
            'room_type_delete'=>'%name Room Type Deleted ',
            'room_type_selected_delete'=>'%name Selected Room Type Deleted ',


		);

		return $messages;
	}
}
?>
