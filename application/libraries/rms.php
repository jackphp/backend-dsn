<?php
/**
 * This file contains all reports
 * related with RMS server
 *
 * 1) AC
 * 2) TV
 * 3) Light & Dimmers
 * 4) Save Energy
 * 5) Door Bell
 * 6) Door Open
 *
 * @author pbsl
 */
class rms {
	var $eid;
	var $room_id;
	var $floor_id;
	var $from_date;
	var $to_date;
	var $view_as;
	var $CI;

	public function __construct($param) {
		//echo "<pre>"; print_r($param); die();
		$this->room_id = $param['room_id'];
		$this->from_date = $param['from_date'];
		$this->to_date = $param['to_date'];
		$this->floor_id = $param['floor_id'];
		$this->view_as = $param['view_as'];
		$this->CI = get_instance();
		$this->CI->load->model("rms_model");
		$this->CI->load->helper("reports");
	}

	public function get_graph_data($graph_name) {
		switch ($graph_name) {
			case "temperature":
				return $this->get_temp_data();
				break;
		}
	}


	public function get_temp_data() {
		$this->eid = $this->get_event_id("temperature_change");
		$q = $this->CI->rms_model->get_ac_event_data($this->eid, $this->room_id, $this->floor_id, $this->from_date, $this->to_date);
		$results = $q->result();
		$ac_temperature = array();
		$room_temperature = array();
		$avg_ac_temp = 0;
		$avg_room_temp = 0;
		$fid = 0;
		//echo "<pre>"; print_r($results); die();

		foreach($results as $result) {
			if($result->name == 'room_temperature') {
				$room_temperature[] = $result;
				$avg_room_temp = ($avg_room_temp+$result->value);
				$fid = $result->fid;
			}
			else {
				$ac_temperature[] = $result;
				$avg_ac_temp = ($avg_ac_temp+$result->value);
				$fid = $result->fid;
			}
		}

		$room_temp_count = count($room_temperature);
		$ac_temp_count = count($ac_temperature);

		$avg_room_temp = $avg_room_temp > 0 ? (int)($avg_room_temp/$room_temp_count) : "NA";
		$avg_ac_temp = $avg_ac_temp > 0 ? (int)($avg_ac_temp/$ac_temp_count) : "NA";

		$avg_floor_data = $this->CI->rms_model->get_avg_floor_temperature($this->eid, $fid, $this->from_date, $this->to_date);


		$count_temp_change = $room_temp_count < $ac_temp_count ? $room_temp_count : $ac_temp_count;

		$graph_xml = ac_temperature_report($ac_temperature, $room_temperature);

		$onoff_data = $this->get_ac_on_off_data($this->room_id, $this->floor_id, $this->from_date, $this->to_date);

		$data = array('avg_room_temp'=>$avg_room_temp, 'avg_ac_temp'=>$avg_ac_temp, 'avg_floor_rooms_room'=>$avg_floor_data['avg_room_temp'], 'avg_floor_rooms_ac'=>$avg_floor_data['avg_ac_temp'], 'count_temp_change'=>$count_temp_change, 'graph_xml'=>$graph_xml, 'ac_temperature'=>$ac_temperature, 'room_temperature'=>$room_temperature, 'on_commands'=>$onoff_data['on_commands'], 'off_commands'=>$onoff_data['off_commands']);

		return $data;
	}

	public function get_ac_on_off_data($room_id, $floor_id, $start_date, $end_date) {
		$ac_on_off_data = array('on_commands'=>'', 'off_commands'=>'');

		// AC On commands
		$ac_on_eid = $this->get_event_id('ac_on');
		$q = $this->CI->rms_model->get_ac_on_of_commands($ac_on_eid, $room_id, $floor_id, $start_date, $end_date);
		$r = $q->result();
		if(isset($r[0]) && $r[0]->ctr > 0) {
			$ac_on_off_data['on_commands'] = $r[0]->ctr;
		}

		// AC OFF Commands
		$ac_on_eid = $this->get_event_id('ac_off');
		$q = $this->CI->rms_model->get_ac_on_of_commands($ac_on_eid, $room_id, $floor_id, $start_date, $end_date);
		$r = $q->result();
		if(isset($r[0]) && $r[0]->ctr > 0) {
			$ac_on_off_data['off_commands'] = $r[0]->ctr;
		}

		return $ac_on_off_data;
	}

	public function calculate_next_previous_date($start_date, $end_date) {
		//echo "Start Date=> ".$start_date."End Date=> ".$end_date;

		$start_ts = convert_date_to_ts($start_date);
		$end_ts = convert_date_to_ts($end_date);

		$end_date_element = explode("-", $end_date);
		$start_date_element = explode("-", $start_date);

		$date_diffrence = $end_ts-$start_ts;
		$diffrence_in_days = floor($date_diffrence/(60*60*24));
		 
		//echo $diffrence_in_days; die();

		$next_date = mktime(null, null, null, date($end_date_element['1']), date($end_date_element['2'])+$diffrence_in_days, date($end_date_element['0']));
		$pre_date = mktime(null, null, null, date($start_date_element['1']), date($start_date_element['2'])-$diffrence_in_days, date($start_date_element['0']));

		$next_date_element = array('from_date'=>$end_date, 'to_date'=>date('Y-m-d',$next_date));
		$previous_date_element = array('from_date'=>date('Y-m-d', $pre_date), 'to_date'=>$start_date);
		//echo "<pre>"; print_r(array('next_date'=>$next_date_element, 'previous_date'=>$previous_date_element));
		return array('next_date'=>$next_date_element, 'previous_date'=>$previous_date_element);
	}


	public function get_device_on_off_data($results, $data = array()) {
		echo "<pre>"; print_r($results); die();
		//echo "<pre>"; print_r($results); die();
		//SELECT l1.lid, l1.event_start_time, l2.event_start_time, l2.lid, TIMEDIFF(l1.event_start_time, l2.event_start_time) AS diff FROM logs AS l1, logs AS l2  WHERE l1.eid = 8 AND l2.eid = 9 AND l1.room_id = l2.room_id GROUP BY l2.lid, l1.lid
		//select a.value, a.event_start_time, b.event_start_time, TIMEDIFF(a.event_start_time, b.event_start_time) AS diff from (SELECT l.*, v.name ,v.value FROM logs as l, log_values as v WHERE v.lid=l.lid and v.name='light_name' and l.eid=8 order by v.value, l.lid) as a, (SELECT l.*, v.name,v.value FROM logs as l, log_values as v WHERE v.lid=l.lid and v.name='light_name' and l.eid=9 order by v.value, l.lid) as b where a.value=b.value
		$on_off = array();
		$diffs = array();
		foreach($results as $row) {
			$date = explode(' ', $row->event_start_time);
			if(isset($on_off[$date[0]])) {
				array_push($on_off[$date[0]], $row);
			}
			else {
				$on_off[$date[0]] = array($row);
			}

		}
		 
		if(count($on_off) > 0) {
			foreach($on_off as $date=>$row) {
				$diffs[$date] = $this->calculat_device_on_time($row, $data);
			}
		}

		$chart = light_onoff_report($diffs);
		$diffs['onoff_graph'] = $chart;
		$light_on_off_data = $diffs;
		 
		return $light_on_off_data;
	}
	 
	public function calculat_device_on_time($events = array(), $event_names = array('on'=>'', 'off'=>'')) {
		//echo "<pre>"; print_r($event_names); die();
		$diffrence = array();
		$tmpdiff = (object)array('h'=>0, 'i'=>0, 's'=>0);
		//echo "<pre>"; print_r($events); die();
		foreach($events as $evt) {
			if($evt->event_name == $event_names['on']) {
				$on = $evt->event_start_time;
			}

			if($evt->event_name == $event_names['off']) {
				if($on != "") {
					$diff = date_time_diff($on, $evt->event_start_time);
					$on_off = array("on"=>$on, "off"=>$evt->event_start_time);
					$tmpdiff = add_time($tmpdiff, $diff);
					$diffrence[] = array('diff'=>$diff, 'row'=>$evt, 'total_on_time'=>$tmpdiff);
					$tmpdiff = $diff;
					$on = "";
				}
			}
		}

		return $diffrence;
	}

	/***
	 * Getting light ON OFF data
	 * Date wise
	 */
	public function get_lights_on_off_data($results, $params = array()) {
		$on_off = array();
		$diffs = array();
		foreach($results as $row) {
			if(isset($row->evt_date) && array_key_exists($row->evt_date, $on_off)) {
				array_push($on_off[$row->evt_date], $row);
			}
			else {
				$on_off[$row->evt_date] = array($row);
			}
		}

		$light_wise_data = $this->get_data_light_wise($results);

		 
		$light_on_off_data = $this->total_lights_on_time($on_off);
		 
		$chart = light_onoff_report($light_on_off_data, $params);
		 
		$pie_chart = light_pie_chart($light_wise_data, $params);

		$light_on_off_data['graph_xml'] = $chart;
		$light_on_off_data['pie_chart'] = $pie_chart;
		 
		return $light_on_off_data;
	}


	public function get_data_light_wise($result) {
		$light_name_wise_on_time = array();
		foreach($result as $row) {
			if(isset($light_name_wise_on_time[$row->value])) {
				array_push($light_name_wise_on_time[$row->value], $row);
			}
			else {
				$light_name_wise_on_time[$row->value] = array($row);
			}
		}

		$on_data = $this->total_lights_on_time($light_name_wise_on_time);

		return $on_data;
	}

	/***
	 * Calculating light ON OFF Time
	 *
	 */
	public function total_lights_on_time($data) {
		$diffrence = array();
		foreach($data as $date=>$rows) {
			$tmpdiff = '00:00:00';
			foreach($rows as $row) {
				if($tmpdiff != "") {
					$tmpdiff = add_diff_time($tmpdiff, $row->diff);
				}
			}

			$timeArr = explode(":", $tmpdiff);
			$hrs = (int)$timeArr[0].".".$timeArr[1];
			$diffrence[$date] = array('rows'=>$rows, 'total_on_time'=>$tmpdiff, 'total_on_hrs'=>$hrs);
		}
		 
		return $diffrence;
	}

	public function tv_report_graph() {
		 
	}

	public function dimmer_graph() {
		 
	}

	public function door_bell_graph() {
		 
	}

	public function get_event_id($name) {
		if($name != "") {
			$q = $this->CI->common_model->get_records_by_field('log_events', 'event_name', $name);
			$row = $q->result();
			if(isset($row[0]) && $row[0]->eid > 0) {
				return $row[0]->eid;
			}
		}
	}

}
?>