<?php
/**
 * Description of watchdog
 *
 * @author pbsl
 */
class watchdog {
	var $uid;
	var $instance;
	var $server_param;

	public function  __construct() {
		$this->instance =& get_instance();
		$this->instance->load->library('user');
		$this->instance->load->model('watchdog_model');
		$this->uid = $this->instance->user->get_users_id();
		$this->server_param = $_SERVER;
	}


	public function save($type, $module, $mid, $row_id=0) {
		$message = $this->get_message($mid);
		//(uid, type, message, location, referer, record_unique_val, access_time)
		if(!isset($this->server_param['HTTP_REFERER'])) {
			$this->server_param['HTTP_REFERER'] = $this->server_param['PHP_SELF'];
		}

		$param = array('uid'=>$this->uid, 'type'=>$type, 'module'=>$module,'message'=>$module." ".$message, 'location'=>$this->server_param['PHP_SELF'], 'referer'=>$this->server_param['HTTP_REFERER'], 'ip'=>$this->server_param['REMOTE_ADDR'], 'record_unique_val'=>$row_id, 'access_time'=>time());
		$this->instance->watchdog_model->save($param);
	}

	public function get_message($mid) {
		$messages = $this->messages();
		return $messages[$mid];
	}

	public function messages() {
		$messages = array(
		0=>'add.',
		1=>'update.',
		2=>'delete.',
		3=>'changed.',
		4=>'Room in under maintenance.',
		5=>'Room out of under maintenance.',
		6=>'access denied.',
		7=>'error in changing room status.',
		);

		return $messages;
	}
}
?>
