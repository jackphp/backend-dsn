<?php
/**
 * Description of tv_library
 *
 * @author pbsl
 */
class tv_library {
	var $eid;
	var $room_id;
	var $floor_id;
	var $from_date;
	var $to_date;
	var $view_as;
	var $room_type;
	var $CI;

	public function  __construct($param) {
		$this->CI = &get_instance();
		$this->from_date = $param['from_date'];
		$this->to_date = $param['to_date'];
		$this->room_type = $param['room_type'];
	}

	public function tv_graphs($param, $condition_param = array()) {

		$tv_graph = $this->tv_usages_graph(array('data'=>$param['tv_data'], 'rooms'=>$param['rooms']));
		$channel_graph = $this->channel_usage_graph(array('data'=>$param['channel_data'], 'rooms'=>$param['rooms']));
		$floor_wise_tv_use = $this->floor_wise_tv_usages(array('data'=>$param['floor_wise_tv_use']));

		$top_rooms_array = array_sort($param['tv_data'], 'total_hrs', SORT_DESC);
		$top_rooms = $this->top_rooms(array('data'=>$top_rooms_array));

		$top_channels_array = array_sort($this->get_top_channels($param['channel_data']), 'total_hrs', SORT_DESC);

		$top_chennels_data = array();
		$i=0;
		foreach($top_channels_array as $channels) {
			//channel_name
			if($i <= 6) {
				if($channels->category == "News") {
					$condition_param['channel_name'] = $channels->channel_name;
					$top_chennels_data[] = $this->top_channels_comperasion_data($condition_param);

					$i++;
				}
			}
			else {
				break;
			}
		}

		$compair_graph = top_tvchannel_comperasion(array('data'=>$top_chennels_data));

		//echo "<pre>"; print_r($top_chennels_data); die();

		$top_channels = $this->top_channels(array('data'=>$top_channels_array));

		$avg_tv_use = $this->aggreates(array('data'=>$param['avg_tv_use']));

		return array('tv_graph'=>$tv_graph, 'tv_compair_graph'=>$compair_graph, 'channel_graph'=>$channel_graph, 'floor_wise_tv_usages'=>$floor_wise_tv_use, 'top_rooms'=>$top_rooms, 'top_channels'=>$top_channels, 'avg_tv_use'=>$avg_tv_use['avg_tv_use']);
	}

	public function tv_usages_graph($param) {
		$graph_data = array();
		$rows = array();
		foreach($param['data'] as $row) {
			$hrsArr = explode(":", $row->total_hrs);
			$total_time = $hrsArr[0].'.'.$hrsArr[1];
			$rows[] = array('room_no'=>$row->room_no, 'total_on_hrs'=>$total_time, 'date'=>$row->report_time, 'from_date'=>$this->from_date, 'to_date'=>$this->to_date);
		}

		return tv_onoff_graph($rows, array('rooms'=>$param['rooms']));
	}

	public function channel_usage_graph($param) {
		$graph_data = array();
		$rows = array();
		foreach($param['data'] as $row) {
			$hrsArr = explode(":", $row->total_hrs);
			$total_time = $hrsArr[0].'.'.$hrsArr[1];
			$row->category ? $category = $row->category : $category = 'NA';
			$rows[] = array('channel_name'=>ucwords($row->channel_name), 'total_on_hrs'=>$total_time, 'from_date'=>$this->from_date, 'to_date'=>$this->to_date, 'category'=>$category);
		}

		return tv_channel_graph($rows, array('rooms'=>$param['rooms']));
	}

	public function floor_wise_tv_usages($param = array()) {
		$rows = array();
		$header = array(
		array('data'=>'#'),
		array('data'=>'Floor Name'),
		array('data'=>'Usages (hours)'),
		);

		$i=0;
		foreach($param['data'] as $row) {
			$i++;
			if($i%2==0) {
				$class = "even";
			}
			else {
				$class = "odd";
			}

			$view_hrs = explode(":", $row->total_hrs);

			$rows[] = array(
			array('data'=>$i, 'attributes'=>array('align'=>'center')),
			array('data'=>$row->fname),
			array('data'=>$view_hrs[0].":".$view_hrs[1], 'attributes'=>array('align'=>'center')),
                     'attributes'=>array('class'=>$class));
		}

		return $this->CI->theme->generate_list($header, $rows);
	}

	public function top_rooms($param = array()) {
		$rows = array();
		$header = array(
		array('data'=>'#'),
		array('data'=>'Room No.'),
		array('data'=>'Usages (hours)'),
		);

		$i=0;
		foreach($param['data'] as $row) {
			$i++;
			if($i%2==0) {
				$class = "even";
			}
			else {
				$class = "odd";
			}

			$view_hrs = explode(":", $row->total_hrs);

			$rows[] = array(
			array('data'=>$i, 'attributes'=>array('align'=>'center')),
			array('data'=>$row->room_name.'-'.$row->room_type),
			array('data'=>$view_hrs[0].":".$view_hrs[1], 'attributes'=>array('align'=>'center')),
                     'attributes'=>array('class'=>$class));

			if($i==6) break;
		}

		return $this->CI->theme->generate_list($header, $rows);
	}

	public function top_channels($param = array()) {
		$rows = array();
		$header = array(
		array('data'=>'#'),
		array('data'=>'Channel'),
		array('data'=>'Ctegory'),
		array('data'=>'Watched (hours)'),
		);

		$i=0;
		foreach($param['data'] as $row) {
			$i++;
			if($i%2==0) {
				$class = "even";
			}
			else {
				$class = "odd";
			}

			$view_hrs = $row->total_hrs; //explode(":", $row->total_hrs);

			$category = "";
			if($row->category == "") {
				if(ucwords($row->channel_name) == "Star Plus") {
					$category = 'Entertainment';
				}

				if(ucwords($row->channel_name) == "Neo Sports") {
					$category = 'Sports';
				}

				if(ucwords($row->channel_name) == "IBN7") {
					$category = 'Business News';
				}
				if(ucwords($row->channel_name) == "Aaj Tak") {
					$category = 'News';
				}
				if($category == "") {
					$category = 'NA';
				}
			}
			else {
				$category = $row->category;
			}

			$min = rand(10, 60);

			$rows[] = array(
			array('data'=>$i, 'attributes'=>array('align'=>'center')),
			array('data'=>ucwords($row->channel_name)),
			array('data'=>$category ? $category : "NA"),
			array('data'=>$view_hrs."hr", 'attributes'=>array('align'=>'center')),
                     'attributes'=>array('class'=>$class));

			if($i==6) break;
		}

		return $this->CI->theme->generate_list($header, $rows);
	}

	public function top_category() {
		 
	}

	public function get_top_channels($channels=array()) {
		$channels_hrs = array();
		foreach($channels as $row) {
			$hrsArr = explode(":", $row->total_hrs);

			if($hrsArr[1] > 45) {
				$hr = ($hrsArr[0]+1);
			}
			else {
				$hr = $hrsArr[0];
			}

			$row->total_hrs = $hr;

			$channels_hrs[] = $row;
		}

		return $channels_hrs;
	}

	public function top_channels_comperasion_data($param=array()) {
		$q = $this->CI->rms_model->get_tv_channel_data_hour_wise($param);
		return $q->result();
	}

	public function aggreates($param = array()) {
		foreach($param['data'] as $row) {
			$avg_hrs = explode(":", $row->avg_hrs);
		}
		if(isset($avg_hrs[0]) && isset($avg_hrs[1])) {
			return array('avg_tv_use'=>$avg_hrs[0].":".$avg_hrs[1]);
		}
	}
}
?>