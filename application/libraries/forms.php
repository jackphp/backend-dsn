<?php
/**
 * Description of form_validator
 *
 * @author pbsl
 */
class forms {

	var $CI;
	var $data = array();
	public function forms() {
		$this->CI =& get_instance();
		$this->CI->load->library('message');
		$this->CI->load->model('common_model');
		$this->CI->load->helper('form');
		$this->CI->load->library('get_last_post');
	}

	/***
	 * Validating add user form
	 * @argument: $param = array();
	 *
	 **/
	public function add_user_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		if(count($post) > 0 ) {
			//echo "<pre>"; print_r($post); die();
			$username = $post['username']->message;
			$email = $post['email']->message;
			$password = $post['password']->message;
			$retype_password = $post['retype_password']->message;
			$status = $post['status']->message;
			$department = $post['department']->message;
			$status2 = 0;
			if($status == 0) {
				$status = 1;
			}
			else {
				$status2 = 1;
			}
			$roels = $post['roles']->message;
		}
		else {
			$username = '';
			$email = '';
			$password = '';
			$retype_password = '';
			$status = '';
			$status2 = '';
			$department = '';
			$roels = array();
		}

		$roles_res = $this->CI->common_model->get_record_by_condition('role', 'name<>"anonymous user"');
		$roles = array();
		$roles_str = '';
		foreach($roles_res->result() as $row) {
			$roles[$row->rid] = $row->name;
			if($row->name == 'authenticated user') {
				$roles_str .= form_checkbox('roles[]', $row->name, 1, 'disabled="disabled"')." ".$row->name." <br/>";
			}
			else {
				if(count($roels) > 0) {
					foreach($roles as $role) {
						if($row->name == $role) {
							$roles_str .= form_checkbox('roles[]', $row->name, $row->name)." ".$row->name." <br/>";
						}
					}
				}
				else {
					$roles_str .= form_checkbox('roles[]', $row->name)." ".$row->name." <br/>";
				}
			}
		}

		$departments_res = $this->CI->common_model->get_records('departments', 'name');
		$departments = array('0'=>'--Select--');
		foreach ($departments_res->result() as $row) {
			$departments[$row->did] = $row->name;
		}
		//echo "<pre>"; print_r($param); die();
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('admin/submit', $attributes);
		$this->data['username'] = form_input('name', $username, 'id="edit-name"');
		$this->data['email'] = form_input('email', $email, 'id="edit-email"');
		$this->data['password'] = form_password('password', $password, 'id="edit-password"');
		$this->data['retype_password'] = form_password('retype_password', $retype_password, 'id="edit-retype-password"');
		$this->data['status'] = form_radio('status', 0, $status);
		$this->data['status2'] = form_radio('status', 1, $status2);
		$this->data['roles'] = $roles_str;
		$this->data['departments'] = form_dropdown('department', $departments, $department);
		//$this->data['roles'] = form_checkbox($data, $value, $checked);

		$this->data['form_id'] = form_hidden('form_id', 'edit-add-user');
		$this->data['submit'] = form_submit('submit', 'Submit', 'id="edit-submit"');
		$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'admin/users\'"');
		$this->data['form_close'] = form_close();

		return $this->data;
	}


	public function edit_user_form($param = array(), $cancel=TRUE) {

		if(count($param) > 0 ) {
			//echo "<pre>"; print_r($param); die();
			$username = $param[0]['name'];
			$email = $param[0]['mail'];
			$password = '';
			$retype_password = '';
			$status = $param[0]['status'];
			$user_roles = $param['roles'];
			$department = $param[0]['department_id'];
		}
		else {
			$username = '';
			$email = '';
			$password = '';
			$retype_password = '';
			$status = '';
			$department = '';
			$user_roles = array();
		}
		//print_r($roles); die();
		$roles_res = $this->CI->common_model->get_record_by_condition('role', 'name<>"anonymous user"');
		$roles_str = array();
		foreach($roles_res->result() as $row) {
			$roles[$row->rid] = $row->name;
			foreach($user_roles as $ur) {
				if($ur != $row->name) {
					$roles_str[$row->name] = form_checkbox('roles[]', $row->name). " ".$row->name."<br/>";
				}
			}
		}

		foreach($user_roles as $ur) {
			if($ur == 'authenticated user')
			$roles_str[$ur] = form_checkbox('roles[]', $ur, $ur, 'disabled="disabled"'). " ".$ur."<br/>";
			else
			$roles_str[$ur] = form_checkbox('roles[]', $ur, $ur). " ".$ur."<br/>";
		}
		 
		$departments_res = $this->CI->common_model->get_records('departments', 'name');
		$departments = array('0'=>'--Select--');
		foreach ($departments_res->result() as $row) {
			$departments[$row->did] = $row->name;
		}

		//echo "<pre>"; print_r($param); die();
		$roles_str = implode("<br/>", $roles_str);

		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('admin/submit', $attributes);
		$this->data['username'] = form_input('name', $username, 'id="edit-name"');
		$this->data['email'] = form_input('email', $email, 'id="edit-email"');
		$this->data['departments'] = form_dropdown('department', $departments, $department);
		$this->data['password'] = form_password('password', $password, 'id="edit-password"');
		$this->data['retype_password'] = form_password('retype_password', $retype_password, 'id="edit-retype-password"');
		if($status == 0) {
			$this->data['status'] = form_radio('status', 0, 1);
			$this->data['status2'] = form_radio('status', 1);
		}
		else {
			$this->data['status2'] = form_radio('status', 1, $status);
			$this->data['status'] = form_radio('status', 0);
		}
		$this->data['roles'] = $roles_str;
		$this->data['uid'] = form_hidden('uid', $param[0]['uid'], 'edit-form-id');

		//$this->data['roles'] = form_checkbox($data, $value, $checked);
		$this->data['form_id'] = form_hidden('form_id', 'update-user');
		$this->data['submit'] = form_submit('submit', 'Submit', 'id="edit-submit"');
		if($cancel) {
			$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'admin/users\'"');
		}
		$this->data['form_close'] = form_close();

		return $this->data;
	}

	public function add_user_validate($param = array()) {
		$error = false;
		if(count($param) > 0) {
			if(isset($param['username']) && strlen($param['username']) > 0) {
				if($param['uid'] > 0) {
					$user_exist_qry = $this->CI->common_model->get_record_by_condition('users', 'name="'.$param['username'].'" AND uid<>'.$param['uid']);
				}
				else{
					$user_exist_qry = $this->CI->common_model->get_records_by_field('users', 'name', $param['username']);
				}
				$user = $user_exist_qry->result();
				if(isset($user[0]->uid) && $user[0]->uid > 0) {
					$this->CI->message->set("Username already exist.", "error", TRUE);
					$error = TRUE;
				}
			}
			else {
				$this->CI->message->set("Username is required field.", "error", TRUE);
				$error = TRUE;
			}

			if(isset($param['email']) && strlen($param['email']) > 0) {
				if(isset($param['uid']) && $param['uid'] > 0) {
					$user_exist_qry = $this->CI->common_model->get_record_by_condition('users', 'mail="'.$param['email'].'" AND uid<>'.$param['uid']);
				}
				else{
					$user_exist_qry = $this->CI->common_model->get_records_by_field('users', 'mail ', $param['email']);
				}

				$user = $user_exist_qry->result();
				if(isset($user[0]->uid) && $user[0]->uid > 0) {
					$this->CI->message->set("Email already exist.", "error", TRUE);
					$error = TRUE;
				}
			}
			else {
				$this->CI->message->set("Email is required field.", "error", TRUE);
				$error = TRUE;
			}
			if(isset($param['uid']) && $param['uid'] > 0) {
				if(isset($param['password']) && strlen($param['password']) > 0) {
					if($param['password'] != $param['retype_password']) {
						$this->CI->message->set("Password and confirm password must be same.", "error", TRUE);
						$error = TRUE;
					}
				}
			}
			else {
				if(isset($param['password']) && strlen($param['password']) > 0) {
					if($param['password'] != $param['retype_password']) {
						$this->CI->message->set("Password and confirm password must be same.", "error", TRUE);
						$error = TRUE;
					}
				}
				else {
					$this->CI->message->set("Password is required field.", "error", TRUE);
					$error = TRUE;
				}
			}

		} else {
			$this->CI->message->set("Username is required field.", "error", TRUE);
			$this->CI->message->set("Email is required field.", "error", TRUE);
			$this->CI->message->set("Password is required field.", "error", TRUE);
			//$this->CI->message->set("Username is required field.", "error", TRUE);
			$error = TRUE;
		}


		$this->CI->get_last_post->set($param, "post", TRUE);
		return $error;
	}


	public function role_form($param = array()) {
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('admin/submit', $attributes);
		if(count($param) > 0 && isset($param['rid']) > 0) {
			$this->data['role_name'] = form_input('name', $param['name'], 'id="edit-role-name"');
			$this->data['submit'] = form_submit('submit', 'Update role', 'id="edit-update-role-submit"');
			$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'admin/roles\'"');
			$this->data['rid'] = form_hidden('rid', $param['rid']);
			$this->data['form_id'] = form_hidden('form_id', 'edit-role');
		}
		else {
			$param = $this->CI->get_last_post->get();
			if(isset ($param) && isset($param['name'])) {
				$name = $param['name'];
			}
			else {
				$name = '';
			}
			$this->data['role_name'] = form_input('name', $name, 'id="edit-role-name"');
			$this->data['submit'] = form_submit('submit', 'Add role', 'id="edit-add-role-submit"');
			$this->data['form_id'] = form_hidden('form_id', 'add-role');
		}
		$this->data['form_close'] = form_close();

		return $this->data;
	}


	public function role_validate($param) {
		$error = FALSE;
		if(count($param) > 0) {

			if(isset($param['name']) && strlen($param['name']) > 0) {

				if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['name'])) {
					$error = TRUE;
					$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
				}

				if(isset($param['rid']) && $param['rid'] > 0) {
					$role_exist_qry = $this->CI->common_model->get_record_by_condition('role', 'name="'.$param['name'].'" AND rid<>'.$param['rid']);
				}
				else {
					$role_exist_qry = $this->CI->common_model->get_records_by_field('role', 'name', $param['name']);
				}
				$res = $role_exist_qry->result_array();
				if(isset($res[0]) && $res[0]['rid'] > 0) {
					$error = TRUE;
					$this->CI->message->set('Role already exist.', 'error', TRUE);
				}
			}
			else {
				$error = TRUE;
				$this->CI->message->set('Role name is reqired.', 'error', TRUE);
			}
		}
		else {
			$error = TRUE;
		}

		return $error;
	}



	public function permission_form($roles, $permissions) {
		if(count($roles) > 0) {
			$header = array(array('data'=>'Permissions'), 'class'=>'listheading');
			$rows = array();
			foreach($roles as $role) {
				$header[] = array('data'=>$role);
			}

			$permq = $this->CI->common_model->get_records('perms');
			$perms = array();
			foreach ($permq->result() as $row) {
				$perms[$row->pid] = $row->perm;
			}
			//echo "<pre>"; print_r($perm); die();

			//echo "<pre>"; print_r($permissions); die();
			$i=0;

			if(count($permissions) > 0) {
				foreach($permissions as $pid=>$rids) {
					$perm_row = array();
					$perm_row[] = array('data'=>'<b>'.$perms[$pid].'</b>');
					foreach($roles as $rid=>$name) {
						if(in_array($rid, $rids)) {
							$perm_row[] = array('data'=>form_checkbox($rid.'[]', $pid, 1), 'attributes'=>array('align'=>'center'));
						}
						else {
							$perm_row[] = array('data'=>form_checkbox($rid.'[]', $pid), 'attributes'=>array('align'=>'center'));
						}
					}

					$i++;
					if($i%2==0) {
						$class = "even";
					}
					else {
						$class = "odd";
					}

					$perm_row['attributes'] = array('class'=>$class);
					$rows[] = $perm_row;
				}
			}

			return array('header'=>$header, 'rows'=>$rows);
		}
		else {
			return false;
		}
	}


	public function floor_form($param = array()) {
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('room_management/submit', $attributes);
		if(count($param) > 0 && isset($param['fid']) > 0) {
			$this->data['floor_name'] = form_input('name', $param['fname'], 'id="edit-floor-name"');
			$this->data['submit'] = form_submit('submit', 'Update floor', 'id="edit-update-floor-submit"');
			$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'room_management\'"');
			$this->data['fid'] = form_hidden('fid', $param['fid']);
			$this->data['form_id'] = form_hidden('form_id', 'add-floor');
		}
		else {
			$param = $this->CI->get_last_post->get();
			if(isset ($param) && isset($param['name'])) {
				$name = $param['name'];
			}
			else {
				$name = '';
			}
			$this->data['floor_name'] = form_input('name', $name, 'id="edit-floor-name"');
			$this->data['submit'] = form_submit('submit', 'Add floor', 'id="edit-add-floor-submit"');
			$this->data['form_id'] = form_hidden('form_id', 'add-floor');
		}
		 
		$this->data['form_close'] = form_close();

		return $this->data;
	}


	public function floor_validate($param) {
		$error = FALSE;
		if(count($param) > 0) {

			if(isset($param['name']) && strlen($param['name']) > 0) {

				if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['name'])) {
					$error = TRUE;
					$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
				}

				if(isset($param['fid']) && $param['fid'] > 0) {
					$role_exist_qry = $this->CI->common_model->get_record_by_condition('floors', 'fname= "'.$param['name'].'" AND fid <>'.$param['fid']);
				}
				else {
					$role_exist_qry = $this->CI->common_model->get_records_by_field('floors', 'fname', $param['name']);
				}
				$res = $role_exist_qry->result_array();
				if(isset($res[0]) && $res[0]['fid'] > 0) {
					$error = TRUE;
					$this->CI->message->set('Floor already exist.', 'error', TRUE);
				}
			}
			else {
				$error = TRUE;
				$this->CI->message->set('Floor name is reqired.', 'error', TRUE);
			}
		}
		else {
			$error = TRUE;
		}

		return $error;
	}


	public function room_form($param = array()) {
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('room_management/submit', $attributes);
		$q = $this->CI->common_model->get_records('floors', 'fname');
		$floors = array('0'=>'-- Select Floor--');
		foreach($q->result() as $row) {
			$floors[$row->fid] = $row->fname;
		}
		if(count($param) > 0 && isset($param['fid'])) {
			$this->data['floor'] = form_dropdown('fid', $floors, $param['fid']);
			$this->data['room_name'] = form_input('name', $param['name'], 'id="edit-room-name"');
			$this->data['submit'] = form_submit('submit', 'Update room', 'id="edit-update-room-submit"');
			$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'room_management/rooms\'"');
			$this->data['room_id'] = form_hidden('room_id', $param['room_id']);
			$this->data['form_id'] = form_hidden('form_id', 'add-room');
			$this->data['maintenance'] = form_checkbox('maintenance', 1, $param['maintenance'], 'id="edit-room-maintenance"');
		}
		else {
			$param = $this->CI->get_last_post->get();
			//print_r($param); die();
			if(isset ($param) && isset($param['name'])) {
				$name = $param['name']->message;
			}
			else {
				$name = '';
			}

			$this->data['maintenance'] = form_checkbox('maintenance', 1, '', 'id="edit-room-maintenance"');
			$this->data['room_name'] = form_input('name', $name, 'id="edit-floor-name"');
			$this->data['floor'] = form_dropdown('fid', $floors);
			$this->data['submit'] = form_submit('submit', 'Add Room', 'id="edit-add-floor-submit"');
			$this->data['form_id'] = form_hidden('form_id', 'add-room');
		}

		$this->data['form_close'] = form_close();

		return $this->data;
	}

	public function room_validate($param) {
		$error = FALSE;
		if(count($param) > 0) {

			if(isset($param['name']) && strlen($param['name']) > 0) {

				if(isset($param['fid']) && $param['fid'] == 0) {
					$error = TRUE;
					$this->CI->message->set('Floor field is required.', 'error', TRUE);
					$this->CI->get_last_post->set($param, '', TRUE);
				}

				if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['name'])) {
					$error = TRUE;
					$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
				}

				if(isset($param['room_id']) && $param['room_id'] > 0) {
					$role_exist_qry = $this->CI->common_model->get_record_by_condition('rooms', 'room_name= "'.$param['name'].'" AND room_id <>'.$param['room_id']);
				}
				else {
					$role_exist_qry = $this->CI->common_model->get_records_by_field('rooms', 'room_name', $param['name']);
				}
				$res = $role_exist_qry->result_array();
				if(isset($res[0]) && $res[0]['room_id'] > 0) {
					$error = TRUE;
					$this->CI->message->set('Room already exist.', 'error', TRUE);
				}
			}
			else {
				$error = TRUE;
				$this->CI->message->set('Room name is reqired.', 'error', TRUE);
			}
		}
		else {
			$error = TRUE;
		}

		return $error;
	}


	public function department_form($param = array()) {
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('admin/submit', $attributes);
		if(count($param) > 0 && isset($param['did']) > 0) {
			$this->data['department_name'] = form_input('name', $param['name'], 'id="edit-department-name"');
			$this->data['submit'] = form_submit('submit', 'Update', 'id="edit-update-floor-submit"');
			$this->data['cancel'] = form_button('cancel', 'Cancel', 'id="edit-submit" onclick="location.href=\''.base_url().'admin/departments\'"');
			$this->data['did'] = form_hidden('did', $param['did']);
			$this->data['form_id'] = form_hidden('form_id', 'edit-department');
		}
		else {
			$param = $this->CI->get_last_post->get();
			if(isset ($param) && isset($param['name'])) {
				$name = $param['name'];
			}
			else {
				$name = '';
			}
			$this->data['department_name'] = form_input('name', $name, 'id="edit-department-name"');
			$this->data['submit'] = form_submit('submit', 'Add department', 'id="edit-add-department-submit"');
			$this->data['form_id'] = form_hidden('form_id', 'add-department');
		}

		$this->data['form_close'] = form_close();

		return $this->data;
	}

	public function department_validate($param) {
		$error = FALSE;
		if(count($param) > 0) {
			if(isset($param['name']) && strlen($param['name']) > 0) {

				if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['name'])) {
					$error = TRUE;
					$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
				}

				if(isset($param['did']) && $param['did'] > 0) {
					$department_exist_qry = $this->CI->common_model->get_record_by_condition('departments', 'name= "'.$param['name'].'" AND did <>'.$param['did']);
				}
				else {
					$department_exist_qry = $this->CI->common_model->get_records_by_field('departments', 'name', $param['name']);
				}
				$res = $department_exist_qry->result_array();
				if(isset($res[0]) && $res[0]['did'] > 0) {
					$error = TRUE;
					$this->CI->message->set('Department already exist.', 'error', TRUE);
				}
			}
			else {
				$error = TRUE;
				$this->CI->message->set('Department name is reqired.', 'error', TRUE);
			}
		}
		else {
			$error = TRUE;
		}

		return $error;
	}



	public function add_menu($param = array()) {
		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('config/submit', $attributes);
		 
		if(count($param) > 0 && isset($param['lid']) > 0) {

		}
		else {
			 
		}
	}


	public function add_addons($param = array()) {
		$post = $this->CI->get_last_post->get();
		$q = $this->CI->common_model->get_records('perms', 'perm');
		$prems = array('0'=>'--Select--');
		foreach($q->result() as $row) {
			$prems[$row->perm] = $row->perm;
		}
		//echo "<pre>"; print_r($param); die();
		if(count($post) < 1) {
			$this->CI->get_last_post->set($param, "post", TRUE);
			$post = $this->CI->get_last_post->get();
		}
		 

		if(count($post) > 0 || count($param) > 0) {
			$name = $post['name']->message;
			$url = $post['url']->message;
			$status = $post['status']->message;
			$reporting = $post['reporting']->message;
			$reporting_path = $post['reporting_path']->message;
			$perm = $post['perm']->message;
		}
		else {
			$name = "";
			$url = "";
			$status = "";
			$reporting = "";
			$reporting_path = "";
			$perm = '';
		}

		$attributes = array('form_id'=>'form1', 'method'=>'POST');
		$this->data['form_open'] = form_open_multipart('config/submit', $attributes);
		$this->data['form_close'] = form_close();
		if(count($param) > 0 && isset($param['id']) > 0) {
			$this->data['id']  = form_hidden('id', $param['id']);
			$this->data['form_id'] = form_hidden('form_id', 'edit-update-addons');
			$this->data['menu_name'] = form_hidden('menu_name', $param['name']);
		}
		else {
			$this->data['form_id'] = form_hidden('form_id', 'edit-add-addons');
		}
		 
		$this->data['name'] = form_input('name', $name);
		$this->data['url'] = form_input('url', $url, 'size="50"');
		$this->data['status'] = form_dropdown('status', array('1'=>'Active', '0'=>'Deactive'), $status);
		$this->data['reporting'] = form_checkbox('reporting', 1, $reporting, 'id="reporting"');
		$this->data['reporting_path'] = form_input('reporting_path', $reporting_path);
		$this->data['perm'] = form_dropdown('perm', $prems, $perm);

		$this->data['submit'] = form_submit('submit', 'Submit');
		$this->data['cancel'] = form_button('cancel', 'Cancel');
		//test comment
		return $this->data;
	}

	public function validate_addons($param = array()) {
		$error = FALSE;
		//echo "<pre>".$param['perm']; die();
		if(count($param) > 0) {
			if(isset($param['name']) && strlen($param['name']) > 0) {

				if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['name'])) {
					$error = TRUE;
					$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
				}

				if($param['reporting'] == 1 && $param['reporting_path'] == "") {
					$error = TRUE;
					$this->CI->message->set('Reporting path is required.', 'error', TRUE);
				}

				if(strlen($param['perm']) < 2) {
					$error = TRUE;
					$this->CI->message->set('Permission is required.', 'error', TRUE);
				}

				if(isset($param['id']) && $param['id'] > 0) {
					$addon_exist_qry = $this->CI->common_model->get_record_by_condition('digivale_addons', 'name= "'.$param['name'].'" AND id <>'.$param['id']);
				}
				else {
					$addon_exist_qry = $this->CI->common_model->get_records_by_field('digivale_addons', 'name', $param['name']);
				}
				$res = $addon_exist_qry->result_array();
				if(isset($res[0]) && $res[0]['id'] > 0) {
					$error = TRUE;
					$this->CI->message->set('Addon already exist.', 'error', TRUE);
				}

			}
			else {
				$error = TRUE;
				$this->CI->message->set('Addon name is reqired.', 'error', TRUE);
			}
		}
		else {
			$error = TRUE;
		}

		return $error;

	}

	public function add_menu_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		if(isset($param['menu_name'])){$menu_name = $param['menu_name'];} else if(isset($post['menu_name']->message) ) {$menu_name = $post['menu_name']->message;} else{$menu_name = '';}
		if(isset($param['link_name'])){$link_name = $param['link_name'];} else if(isset($post['link_name']->message) ) {$link_name = $post['link_name']->message;} else {$link_name = '';}
		if(isset($param['path'])){$path = $param['path'];} else if(isset($post['path']->message) ) {$path = $post['path']->message;} else {$path = '';}
		if(isset($param['permission'])){$permission = $param['permission'];} else if(isset($post['permission']->message) ) {$permission = $post['permission']->message;} else {$permission = '';}
		if(isset($param['menu_order'])){$menu_order = $param['menu_order'];} else if(isset($post['menu_order']->message) ) {$menu_order = $post['menu_order']->message;} else {$menu_order = '';}
		if(isset($param['status'])){$status = $param['status'];} else if(isset($post['status']->message) ) {$status = $post['status']->message;} else {$status = '';}
		if(isset($param['id'])){$id = $param['id'];} else if(isset($post['id']->message) ) {$id = $post['id']->message;} else {$id = '';}

		$menus = array('--Select--');
		$perms = array('--Select--');

		$q = $this->CI->common_model->get_records('menu', 'name');
		$menus = result_to_array($q->result(), 'mid', 'name');

		$q = $this->CI->common_model->get_records('perms', 'perm');
		$perms = result_to_array($q->result(), 'perm', 'perm');

		if($id > 0) {
			$this->data['id'] = form_hidden('id', $id);
			$this->data['submit'] = form_submit('submit', 'Update Menu');
		}
		else {

			$this->data['submit'] = form_submit('submit', 'Add Menu');
		}

		$this->data['form_open'] = form_open('config/submit');
		$this->data['form_close'] = form_close();
		$this->data['form_id'] = form_hidden('form_id','add_menu_form');
		$this->data['add_parent_menu_link'] = anchor('config/add_parent_menu', 'Add New', 'class="popup"');
		$this->data['menu_name'] = form_dropdown('menu_name', $menus, $menu_name);
		$this->data['link_name'] = form_input('link_name', $link_name);
		$this->data['path'] = form_input('path', $path);
		$this->data['menu_order'] = form_dropdown('menu_order', array('--Select--',1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10), $menu_order);
		$this->data['status'] = form_dropdown('status', array(''=>'--Select--', '1'=>'Active', '0'=>'Deactive'), $status);
		$this->data['permission'] = form_dropdown('permission', $perms, $permission);
		$this->data['cancel'] = form_button('cancel', 'Cancel', 'onclick="javascript:location.href=\''.base_url().'config/menus\'"');
		return $this->data;
	}

	public function add_menu_form_validate($param = array()) {
		$error = FALSE;

		if($param['menu_name'] == '') {
			$error = TRUE;
			$this->CI->message->set('Parent Menu is required field.', 'error', TRUE);
		}

		if($param['link_name'] == '') {
			$error = TRUE;
			$this->CI->message->set('Link Name is required field.', 'error', TRUE);
		}
		else {
			if ( ! preg_match("/^[a-z 0-9:_\/-]+$/i", $param['link_name'])) {
				$error = TRUE;
				$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
			}
		}

		if($param['path'] == '') {
			$error = TRUE;
			$this->CI->message->set('Menu Path is required field.', 'error', TRUE);
		}
		else {
			if( ! preg_match("/^[a-z:_\/-]+$/i", $param['path'])) {
				$error = TRUE;
				$this->CI->message->set('Invalid Path, field should be (path, path/path).', 'error', TRUE);
			}
		}

		if($param['permission'] == '') {
			$error = TRUE;
			$this->CI->message->set('Permission is required field.', 'error', TRUE);
		}

		return $error;

	}


	public function parent_menu_form($param) {
		$post = $this->CI->get_last_post->get();
		if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else{$name = '';}
		if(isset($param['menu_order'])){$menu_order = $param['menu_order'];} else if(isset($post['menu_order']->message) ) {$menu_order = $post['menu_order']->message;} else {$menu_order = '';}
		if(isset($param['id'])){$id = $param['id'];} else if(isset($post['id']->message) ) {$id = $post['id']->message;} else {$id = '';}

		if($id > 0) {
			$this->data['id'] = form_hidden('id', $id);
			$this->data['submit'] = form_submit('submit', 'Update Parent Menu');
		}
		else {
			$this->data['submit'] = form_submit('submit', 'Add Parent Menu');
		}

		$this->data['form_open'] = form_open('config/submit');
		$this->data['form_close'] = form_close();
		$this->data['form_id'] = form_hidden('form_id','add_parent_menu_form');
		$this->data['menu_order'] = form_dropdown('menu_order', array('--Select--',1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9, 10=>10), $menu_order);
		$this->data['name'] = form_input('name', $name);
		$this->data['cancel'] = form_button('cancel', 'Cancel', 'onclick="javascript:location.href=\''.base_url().'config/menus\'" class="colorbox_cancel"');

		return $this->data;
	}

	public function add_parent_menu_form_validate($param = array()) {
		$error = FALSE;
		if($param['name'] == '') {
			$error = TRUE;
			$this->CI->message->set('Parent Menu Name is required field.', 'error', TRUE);
		}
		else {
			if ( ! preg_match("/^[a-z 0-9\s:_\/-]+$/i", $param['name'])) {
				$error = TRUE;
				$this->CI->message->set('Only alphanumeric charecters are allowed.', 'error', TRUE);
			}
		}
		return $error;
	}
	 
	/**
	 * Temperature next previous
	 * Form
	 */
	public function next_form($action, $from_date, $to_date, $viewas, $rooms, $floors) {
		$this->data['form_open'] = form_open($action, array('name'=>'next', 'id'=>'next', 'method'=>'post'));
		$this->data['form_close'] = form_close();
		$this->data['next'] = form_submit('next', '>>');
		$this->data['from_date'] = form_hidden('from_date', $from_date);
		$this->data['to_date'] = form_hidden('to_date', $to_date);
		$this->data['viewas'] = form_hidden('viewas', $viewas);
		$this->data['rooms'] = form_hidden('rooms', $rooms);
		$this->data['floors'] = form_hidden('floors', $floors);
		$this->data['form_id'] = form_hidden('form_id', 'left_view_form');
		return $this->data;
	}

	public function previous_form($action, $from_date, $to_date, $viewas, $rooms, $floors) {
		$this->data['form_open'] = form_open($action, array('name'=>'previous', 'id'=>'previous', 'method'=>'post'));
		$this->data['form_close'] = form_close();
		$this->data['previous'] = form_submit('previous', '<<');
		$this->data['from_date'] = form_hidden('from_date', $from_date);
		$this->data['to_date'] = form_hidden('to_date', $to_date);
		$this->data['viewas'] = form_hidden('viewas', $viewas);
		$this->data['rooms'] = form_hidden('rooms', $rooms);
		$this->data['floors'] = form_hidden('floors', $floors);
		$this->data['form_id'] = form_hidden('form_id', 'left_view_form');
		return $this->data;
	}
}// End Of Class
?>