<?php
/************************************************************************************************************************************************
	 * 																																			*
	 * 											Project Name	: Digivalet Social Network														*									*
	 * 											Author			: Paras Sahu		 															*
	 * 											Creation Date	: 4 July 2013 - 22 July 2013													*										*
	 * 											Snippet Name	: DSN To Link Library															*
	 **************************************************************************************************************************************************/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tolink
{
	function tolink_evaluate($text){
		$text = html_entity_decode($text);
		$text = " ".$text;
		$text = preg_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)',
				'<a href="\\1">\\1</a>', $text);
		$text = preg_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)',
				'<a href="\\1">\\1</a>', $text);
		//$text = preg_replace("([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)", "\\1<a href='http://\\2'>\\2</a>", $text);
		$text = preg_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})', '<a href="mailto:\\1">\\1</a>', $text);
		return $text;
	}
}