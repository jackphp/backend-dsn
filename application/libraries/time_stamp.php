<?php
/************************************************************************************************************************************************
 * 																																			*
 * 											Project Name	: Digivalet Social Network														*									*
 * 											Author			: Paras Sahu		 															*
 * 											Creation Date	: 4 July 2013 - 22 July 2013													*										*
 * 											Snippet Name	: DSN Time Stamp Library														*
 **************************************************************************************************************************************************/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Time_stamp
{
	function time_stamp_evaluate($session_time)
	{
		$ci =& get_instance();
		$time_difference = time() - $session_time ;
		$seconds = $time_difference ;
		$minutes = round($time_difference / 60 );
		$hours = round($time_difference / 3600 );
		$days = round($time_difference / 86400 );
		$weeks = round($time_difference / 604800 );
		$months = round($time_difference / 2419200 );
		$years = round($time_difference / 29030400 );

		if($seconds <= 60)
		{
			if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
			{
				return "$seconds seconds ago";
			}
			else
			{
				echo"$seconds seconds ago";
			}
		}
		else if($minutes <=60)
		{
			if($minutes==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one minute ago";
				}
				else
				{
					echo"one minute ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$minutes minutes ago";
				}
				else
				{
					echo"$minutes minutes ago";
				}
			}
		}
		else if($hours <=24)
		{
			if($hours==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one hour ago";
				}
				else
				{
					echo"one hour ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$hours hours ago";
				}
				else
				{
					echo"$hours hours ago";
				}
			}
		}
		else if($days <=7)
		{
			if($days==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one day ago";
				}
				else
				{
					echo"one day ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$days days ago";
				}
				else
				{
					echo"$days days ago";
				}
			}
		}
		else if($weeks <=4)
		{
			if($weeks==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one week ago";
				}
				else
				{
					echo"one week ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$weeks weeks ago";
				}
				else
				{
					echo"$weeks weeks ago";
				}
			}
		}
		else if($months <=12)
		{
			if($months==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one month ago";
				}
				else
				{
					echo"one month ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$months months ago";
				}
				else
				{
					echo"$months months ago";
				}
			}
		}

		else
		{
			if($years==1)
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "one year ago";
				}
				else
				{
					echo"one year ago";
				}
			}
			else
			{
				if($ci -> session -> userdata('webService') && $ci -> session -> userdata('webService')==1)
				{
					return "$years years ago";
				}
				else
				{
					echo"$years years ago";
				}
			}
		}
	}
}