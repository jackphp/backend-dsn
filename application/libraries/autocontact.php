<?php
/**
 * Description of autocontact
 *
 * @author pbsl
 */
class autocontact {
	var $CI;

	public function autocontact() {
		$this->CI = get_instance();
		$this->CI->load->library('email');
	}


	public function send_mail($vars = array()) {
		if(count($vars) > 0) {
			$this->CI->email->from($this->vars['from_email'], $this->vars['from_name']);
			$this->CI->email->to($this->vars['to']);

			if(isset($this->vars['cc'])){
				$this->CI->email->cc($this->vars['cc']);
			}

			if(isset($this->vars['bcc'])){
				$this->CI->email->bcc($this->vars['bcc']);
			}

			$this->CI->email->subject($this->vars['subject']);
			$this->CI->email->message($this->vars['message']);

			if($this->CI->email->send()) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}

} // End of class
?>
