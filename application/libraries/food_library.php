<?php
/**
 * Description of food_library
 * @author pbsl
 */
class food_library {
	var $data;
	var $applications;
	var $CI;
	var $sections = array('--Select Section--');
	var $revenue = array('--Select Revenue Center--');
	var $category = array('--Select Category--');
	var $sub_category = array('0'=>'--Select Subcategory--');
	var $items = array('--Select Menu Item--');
	var $addons = array();
	var $addon_groups = array(''=>'--Select Addon Family--');
	var $allowed_addon_group = array(''=>'--Select--');
	var $lang = array();
	 
	public function  __construct() {
		$this->CI = &get_instance();
		$this->CI->load->library('get_last_post');
		$this->CI->load->library('upload');
		$this->CI->load->model('food_application_model');
		$q = $this->CI->common_model->get_record_by_condition('food_application', 'is_active=1');
		$this->applications = array();
		foreach($q->result() as $row) {
			$this->applications[$row->aid] = $row->name;
		}

		$q = $this->CI->common_model->get_record_by_condition('food_languages', 'is_active=1');
		$this->lang = array();
		foreach($q->result() as $row) {
			$this->lang[$row->lang_code] = $row->lang_name;
		}

		$q = $this->CI->food_application_model->get_food_addons_group();
		foreach($q->result() as $row) {
			$this->addon_groups[$row->group_number] = $row->group_name;
		}
		 
		$this->addon_groups['other'] = 'Add New';

		$q = $this->CI->food_application_model->get_food_allowed_addons_group();
		foreach($q->result() as $row) {
			$this->allowed_addon_group[$row->group_number] = $row->group_name;
		}

		$this->allowed_addon_group['other'] = 'Add New';
	}

	public function initialize_food_vars($param=array()) {
		if(isset($param['revenue'])) {
			$this->revenue = $param['revenue'];
		}

		if(isset($param['sections'])) {
			$this->sections = $param['sections'];
		}

		if(isset($param['category'])) {
			$this->category = $param['category'];
		}

		if(isset($param['subcat'])) {
			$this->sub_category = $param['subcat'];
		}

		if(isset($param['menuitem'])) {
			$this->items = $param['menuitem'];
		}
	}


	public function restaurant_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		if(isset($param['name'])){$name = $param ['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = array();}
		if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = array();}
		if(isset($param['restaurant_name'])){$restaurant_name = $param['restaurant_name'];} else if(isset($post['restaurant_name']->message) ) {$restaurant_name = $post['restaurant_name']->message;} else {$restaurant_name = '';}
		if(isset($param['is_active'])){$active = $param['is_active'];} else if(isset($post['active']->message) ) {$active = $post['active']->message;} else {$active = '';}

		if(isset($param['rvc_id'])){$rvc_id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else {$rvc_id = array();}
		 
		if(isset($param['restaurant_id'])){$id = $param['restaurant_id'];} else if(isset($post['restaurant_id']->message) ) {$id = $post['restaurant_id']->message;} else {$id = '';}

		$form = array();

		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_locale('restaurant', $id);
			$description = array();
			foreach($q->result() as $row) {
				$name[$row->lang_code] = $row->name;
				$description[$row->lang_code] = $row->description;
			}


			$q = $this->CI->common_model->get_record_by_condition('food_restaurant_rvc', 'restaurant_id='.$id);
			foreach($q->result() as $row) {
				$rvc_id[$row->rvc_number] = $row->rvc_number;
			}
		}

		//print_r(array($name, $description));

		$form['languages'] = $this->lang;
		$form['form_open'] = form_open_multipart('restaurants/submit', array('id=>form1'));
		$form['form_close'] = form_close();
		$form['name'] =   $this->locale_fields('name', 'text', $name);
		$form['description'] = $this->locale_fields('description', 'textarea', $description);
		//echo "<pre>"; print_r($form['description']); die();
		$form['cancel'] = anchor('restaurants', 'Cancel');
		$images = array();
		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_images('restaurant', $id);
			foreach($q->result() as $row) {
				$images[$row->aid] = $row->image_name;
			}
		}

		$form['images'] = $this->draw_image_fields($images);
		$form['video'] = form_upload('video');
		$form['is_active'] = form_dropdown('active', array('1'=>'Yes', '0'=>'No'), $active);
		$revenue = array();
		//echo "<pre>"; print_r($this->revenue); die();
		foreach($this->revenue as $r) {
			$revenue[$r->rvc_number] = $r->rvc_name;
		}

		$form['rvc_id'] = form_dropdown('rvc_id[]', $revenue, $rvc_id, 'multiple="multiple"');

		if($id > 0) {
			$form['id'] = form_hidden('restaurant_id', $id);
			$form['form_id'] = form_hidden('form_id', 'edit_restaurant');
			$form['submit'] = form_submit('submit', 'Update');
			$form['selected_video'] = $param['video'];
		}
		else {
			$form['form_id'] = form_hidden('form_id', 'add_restaurant');
			$form['submit'] = form_submit('submit', 'Add');
		}

		return $form;
	}

	public function restaurant_validate($param = array()) {
		if(count($param) > 0) {
			//echo "<pre>"; print_r($param); die();
			$description = $param['description'];
			$name = $param['name'];
			$post_name = "";
			$restaurant_id = $param['restaurant_id'];
			$rvc_id = $param['rvc_id'];
			$error = FALSE;
			foreach($this->lang as $c=>$val) {
				if($name[$c] == "") {
					$this->CI->message->set('Restaurant name for '.$val.' is required field.', 'error', TRUE);
					$error = TRUE;
				}
			}

			if($restaurant_id > 0) {
				$q = $this->CI->common_model->get_record_by_condition('food_restaurant_details', 'restaurant_name="'.$name['en'].'" AND restaurant_id NOT IN('.$restaurant_id.')');
			}
			else {
				$q = $this->CI->common_model->get_record_by_condition('food_restaurant_details', 'restaurant_name="'.$name['en'].'"');
			}

			if($q->num_rows() > 0) {
				$this->CI->message->set('Restaurant name is already exist.', 'error', TRUE);
				$error = TRUE;
			}

			foreach($this->lang as $c=>$val) {
				if($description[$c] == "") {
					$this->CI->message->set('Restaurant description for '.$val.' is required field.', 'error', TRUE);
					$error = TRUE;
				}
			}

			if($rvc_id == "") {
				$this->CI->message->set('Please Select revenue center.', 'error', TRUE);
				$error = TRUE;
			}

			return $error;
		}
	}
	 
	public function revenue_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		if(isset($param['rvc_number'])){$number = $param['rvc_number'];} else if(isset($post['number']->message) ) {$number = $post['number']->message;} else{$number = '';}
		if(isset($param['rvc_name'])){$name = $param['rvc_name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
		if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = '';}
		if(isset($param['permission'])){$permission = $param['permission'];} else if(isset($post['permission']->message) ) {$permission = $post['permission']->message;} else {$permission = '';}
		if(isset($param['menu_order'])){$menu_order = $param['menu_order'];} else if(isset($post['menu_order']->message) ) {$menu_order = $post['menu_order']->message;} else {$menu_order = '';}
		if(isset($param['is_active'])){$active = $param['is_active'];} else if(isset($post['is_active']->message) ) {$active = $post['is_active']->message;} else {$active = '';}
		if(isset($param['rvc_id'])){$id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$id = $post['rvc_id']->message;} else {$id = '';}

		$form = array();
		$form['form_open'] = form_open_multipart('revenue/submit', array('id=>form1'));
		$form['form_close'] = form_close();
		$form['number'] = form_input('number', $number, 'id="edit-number"');
		$form['name'] = form_input('name', $name, 'id="edit-name"');
		$form['cancel'] = anchor('revenue', 'Cancel');

		$form['is_active'] = form_dropdown('active', array('1'=>'Yes', '0'=>'No'), $active);

		if($id > 0) {
			$form['id'] = form_hidden('rvc_id', $id);
			$form['form_id'] = form_hidden('form_id', 'edit_revenue');
			$form['submit'] = form_submit('submit', 'Update');
		}
		else {
			$form['form_id'] = form_hidden('form_id', 'add_revenue');
			$form['submit'] = form_submit('submit', 'Add');
		}

		return $form;
	}


	public function revenue_validate($param = array()) {
		if(count($param) > 0) {
			$number = $param['number'];
			$name = $param['name'];
			$rvc_id = $param['rvc_id'];
			$error = FALSE;
			if($number == "") {
				$error = TRUE;
				$this->CI->message->set('Revenue number is required field.', 'error', TRUE);
			}

			if($number != "") {
				if($rvc_id != "") {
					$q = $this->CI->common_model->get_record_by_condition('food_revenue_primary', 'rvc_number="'.$number.'" AND rvc_id <> "'.$rvc_id.'"');
				}
				else {
					$q = $this->CI->common_model->get_record_by_condition('food_revenue_primary', 'rvc_number="'.$number.'"');
				}
				if($q->num_rows() > 0) {
					$error = TRUE;
					$this->CI->message->set('Revenue number is already exist.', 'error', TRUE);
				}
			}


			if($name == "") {
				$error = TRUE;
				$this->CI->message->set('Revenue Name is required field.', 'error', TRUE);
			}

			if($name != "") {
				if($rvc_id != "") {
					$q1 = $this->CI->common_model->get_record_by_condition('food_revenue_primary', 'rvc_name="'.$name.'" AND rvc_id <> "'.$rvc_id.'"');
				}
				else {
					$q1 = $this->CI->common_model->get_record_by_condition('food_revenue_primary', 'rvc_name="'.$name.'"');
				}

				if($q1->num_rows() > 0) {
					$error = TRUE;
					$this->CI->message->set('Revenue name is already exist.', 'error', TRUE);
				}
			}

			return $error;
		}
	}

	public function section_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		 
		if(isset($param['rvc_id'])){$rvc_id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else{$rvc_id = '';}
		if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = "";}
		if(isset($param['display_name'])){$display_name['en'] = $param['display_name'];} else if(isset($post['display_name']->message) ) {$display_name = $post['display_name']->message;} else {$display_name = array();}
		if(isset($param['description'])){$description['en'] = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = array();}
		if(isset($param['section_id'])){$id = $param['section_id'];} else if(isset($post['sid']->message) ) {$id = $post['sid']->message;} else {$id = '';}
		$form = array();
		$form['languages'] = $this->lang;

		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_locale('section', $id);
			//  $description = array();
			foreach($q->result() as $row) {
				$display_name[$row->lang_code] = $row->name;
				$description[$row->lang_code] = $row->description;
			}
		}

		$form['form_open'] = form_open_multipart('sections/submit', array('id=>form1'));
		$form['form_close'] = form_close();
		$form['rvc'] = form_dropdown('rvc_id', $this->revenue, $rvc_id, 'id="edit-revenue"');
		$form['name'] = form_input('name', $name);
		$form['display_name'] = $this->locale_fields('display_name', 'text', $display_name);
		$form['description'] = $this->locale_fields('description', 'textarea', $description);
		$form['cancel'] = anchor('sections', 'Cancel');
		$images = array();
		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_images('section', $id);
			foreach($q->result() as $row) {
				$images[$row->aid] = $row->image_name;
			}
		}

		$form['images'] = $this->draw_image_fields($images);
		$form['video'] = form_upload('video');

		if($id > 0) {
			$form['id'] = form_hidden('sid', $id);
			$form['form_id'] = form_hidden('form_id', 'edit_section');
			$form['submit'] = form_submit('submit', 'Update');
		}
		else {
			$form['form_id'] = form_hidden('form_id', 'add_section');
			$form['submit'] = form_submit('submit', 'Add');
		}

		return $form;
	}

	public function section_validate($param = array()) {
		//echo "<pre>"; print_r($param); die();
		if(count($param) > 0) {
			$sid = $param['sid'];
			$rvc_id = $param['rvc_id'];
			$name = $param['name'];
			$display_name = $param['display_name'];
			$description = $param['description'];
			$error = FALSE;
			if($rvc_id == "") {
				$error = TRUE;
				$this->CI->message->set('Revenue is required field.', 'error', TRUE);
			}

			if($name == "") {
				$this->CI->message->set('Section name is required field.', 'error', TRUE);
				$error = TRUE;
			}

			foreach($this->lang as $c=>$val) {
				if($display_name[$c] == "") {
					$this->CI->message->set('Section name for '.$val.' is required field.', 'error', TRUE);
					$error = TRUE;
				}
			}

			if($sid > 0) {
				$q = $this->CI->common_model->get_record_by_condition('food_section', 'display_name="'.$display_name['en'].'" AND section_id NOT IN('.$sid.')');
			}
			else {
				$q = $this->CI->common_model->get_record_by_condition('food_section', 'display_name="'.$display_name['en'].'"');
			}

			if($q->num_rows() > 0) {
				$this->CI->message->set('Section name is already exist.', 'error', TRUE);
				$error = TRUE;
			}

			foreach($this->lang as $c=>$val) {
				if($description[$c] == "") {
					$this->CI->message->set('Section description for '.$val.' is required field.', 'error', TRUE);
					$error = TRUE;
				}
			}

			return $error;
		}
	}

	public function category_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		//echo "<pre>"; print_r($param); die();
		if(isset($param['rvc_id'])){$rvc_id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else{$rvc_id = '';}
		if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
		if(isset($param['display_name'])){$display_name = $param['display_name'];} else if(isset($post['display_name']->message) ) {$display_name = $post['display_name']->message;} else {$display_name = '';}
		if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = '';}
		if(isset($param['section_id'])){$sid = $param['section_id'];} else if(isset($post['section_id']->message) ) {$sid = $post['section_id']->message;} else {$sid = '';}
		if(isset($param['maincategory_id'])){$id = $param['maincategory_id'];} else if(isset($post['cid']->message) ) {$id = $post['cid']->message;} else {$id = '';}

		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_locale('category', $id);
			$description = array();
			$display_name = array();
			foreach($q->result() as $row) {
				$display_name[$row->lang_code] = $row->name;
				$description[$row->lang_code] = $row->description;
			}
		}
		 
		$form = array();
		$form['languages'] = $this->lang;
		$form['form_open'] = form_open_multipart('food_category/submit', array('id=>form1'));
		$form['form_close'] = form_close();
		$form['rvc'] = form_dropdown('rvc_id', $this->revenue, $rvc_id, 'id="edit-revenue"');
		$form['section_id'] = form_dropdown('section_id', $this->sections, $sid, 'id="edit-section" class="select" ');
		$form['name'] = form_input('name', $name,'id="edit-name" class="input" ');
		$form['display_name'] = $this->locale_fields('display_name', 'text', $display_name);
		$form['description'] = $this->locale_fields('description', 'textarea', $description);
		$form['cancel'] = anchor('food_category', 'Cancel');
		$images = array();
		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_images('category', $id);
			foreach($q->result() as $row) {
				$images[$row->aid] = $row->image_name;
			}
		}

		$form['images'] = $this->draw_image_fields($images);

		if($id > 0) {
			$form['id'] = form_hidden('cid', $id);
			$form['form_id'] = form_hidden('form_id', 'edit_category');
			$form['submit'] = form_submit('submit', 'Update');
		}
		else {
			$form['form_id'] = form_hidden('form_id', 'add_category');
			$form['submit'] = form_submit('submit', 'Add');
		}

		return $form;
	}

	public function addon_form($param = array()) {
		$post = $this->CI->get_last_post->get();
		//echo "<pre>"; print_r($param); die();
		//if(isset($param['rvc_id'])){$rvc_id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else{$rvc_id = '';}
		//if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
		if(isset($param['display_name'])){$display_name = $param['display_name'];} else if(isset($post['display_name']->message) ) {$display_name = $post['display_name']->message;} else {$display_name = '';}
		//if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = '';}
		//if(isset($param['section_id'])){$sid = $param['section_id'];} else if(isset($post['section_id']->message) ) {$sid = $post['section_id']->message;} else {$sid = '';}
		if(isset($param['addon_id'])){$id = $param['addon_id'];} else if(isset($post['cid']->message) ) {$id = $post['cid']->message;} else {$id = '';}

		if($id > 0) {
			$q = $this->CI->food_application_model->get_food_locale('addon', $id);
			//$description = array();
			$display_name = array();
			foreach($q->result() as $row) {
				$display_name[$row->lang_code] = $row->name;
				//  $description[$row->lang_code] = $row->description;
			}
		}
		 
		$form = array();
		$form['languages'] = $this->lang;
		//$form['form_open'] = form_open_multipart('food_addons/submit', array('id=>form1'));
		//$form['form_close'] = form_close();
		//$form['rvc'] = form_dropdown('rvc_id', $this->revenue, $rvc_id, 'id="edit-revenue"');
		//$form['section_id'] = form_dropdown('section_id', $this->sections, $sid, 'id="edit-section" class="select" ');
		//$form['name'] = form_input('name', $name,'id="edit-name" class="input" ');
		$form['display_name'] = $this->locale_fields('display_name', 'text', $display_name);
		//$form['description'] = $this->locale_fields('description', 'textarea', $description);
		//$form['cancel'] = anchor('food_category', 'Cancel');
		//$images = array();
		//if($id > 0) {
		//    $q = $this->CI->food_application_model->get_food_images('category', $id);
			//    foreach($q->result() as $row) {
			//        $images[$row->aid] = $row->image_name;
				//    }
				// }

				//$form['images'] = $this->draw_image_fields($images);

				//if($id > 0) {
				//    $form['id'] = form_hidden('cid', $id);
				//     $form['form_id'] = form_hidden('form_id', 'edit_category');
				//     $form['submit'] = form_submit('submit', 'Update');
				//  }
				//  else {
				//      $form['form_id'] = form_hidden('form_id', 'add_category');
					//      $form['submit'] = form_submit('submit', 'Add');
					//  }

					return $form;
				}
				 
				public function group_addon_form($param = array()) {
					$post = $this->CI->get_last_post->get();
					//echo "<pre>"; print_r($param); die();
					//if(isset($param['rvc_id'])){$rvc_id = $param['rvc_id'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else{$rvc_id = '';}
					//if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
					if(isset($param['display_name'])){$display_name = $param['display_name'];} else if(isset($post['display_name']->message) ) {$display_name = $post['display_name']->message;} else {$display_name = '';}
					//if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = '';}
					//if(isset($param['section_id'])){$sid = $param['section_id'];} else if(isset($post['section_id']->message) ) {$sid = $post['section_id']->message;} else {$sid = '';}
					if(isset($param['gid'])){$id = $param['gid'];} else if(isset($post['cid']->message) ) {$id = $post['cid']->message;} else {$id = '';}

					if($id > 0) {
						$q = $this->CI->food_application_model->get_food_locale('group_addon', $id);
						//$description = array();
						$display_name = array();
						foreach($q->result() as $row) {
							$display_name[$row->lang_code] = $row->name;
							//  $description[$row->lang_code] = $row->description;
						}
					}
					 
					$form = array();
					$form['languages'] = $this->lang;
					//$form['form_open'] = form_open_multipart('food_addons/submit', array('id=>form1'));
					//$form['form_close'] = form_close();
					//$form['rvc'] = form_dropdown('rvc_id', $this->revenue, $rvc_id, 'id="edit-revenue"');
					//$form['section_id'] = form_dropdown('section_id', $this->sections, $sid, 'id="edit-section" class="select" ');
					//$form['name'] = form_input('name', $name,'id="edit-name" class="input" ');
					$form['display_name'] = $this->locale_fields('display_name', 'text', $display_name);


					return $form;
   }
    
    
    
   public function category_validate($param = array()) {
   	//echo "<pre>"; print_r($param); die();
   	//$rvc_id = $param['rvc_id'];
   	$sid = $param['section_id'];
   	$cid = $param['cid'];
   	$name = $param['name'];
   	$display_name = $param['display_name'];
   	//$description = $param['description'];
   	$error = FALSE;
   	//       if($sid == "") {
   	//           $error = TRUE;
   	//           $this->CI->message->set('Section is required field.', 'error', TRUE);
   	//       }

   	//       if($name == "") {
   	//           $error = TRUE;
   	//           $this->CI->message->set('Section Name required field.', 'error', TRUE);
   	//       }

   	//         foreach($this->lang as $c=>$val) {
   	//            if($display_name[$c] == "") {
   	//                $this->CI->message->set('Display Name for '.$val.' is required field.', 'error', TRUE);
   	//                $error = TRUE;
   	//            }
   	//          }

   	if($cid > 0) {
   		$q = $this->CI->common_model->get_record_by_condition('food_maincategory', 'name="'.$name.'" AND maincategory_id NOT IN('.$cid.')');
   	}
   	else {
   		$q = $this->CI->common_model->get_record_by_condition('food_maincategory', 'name="'.$name.'"');
   	}

   	if($q->num_rows() > 0) {
   		$this->CI->message->set('Category name is already exist.', 'error', TRUE);
   		$error = TRUE;
   	}

   	//          foreach($this->lang as $c=>$val) {
   	//            if($description[$c] == "") {
   	//                $this->CI->message->set('Description for '.$val.' is required field.', 'error', TRUE);
   	//                $error = TRUE;
   	//            }
   	//          }
   	 
   	return $error;
   	}


   	public function sub_category_form($param = array()) {
   		$post = $this->CI->get_last_post->get();
   		//echo "<pre>"; print_r($param); die();
   		if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
   		//if(isset($param['helping_text'])){$helping_text = $param['helping_text'];} else if(isset($post['helping_text']->message) ) {$helping_text = $post['helping_text']->message;} else {$helping_text = '';}
   		if(isset($param['display_name'])){$display_name = $param['display_name'];} else if(isset($post['display_name']->message) ) {$display_name = $post['display_name']->message;} else {$display_name = '';}
   		//if(isset($param['description'])){$description = $param['description'];} else if(isset($post['description']->message) ) {$description = $post['description']->message;} else {$description = '';}
   		if(isset($param['maincategory_id'])){$cid = $param['maincategory_id'];} else if(isset($post['maincategory_id']->message) ) {$cid = $post['maincategory_id']->message;} else {$cid = '';}
   		if(isset($param['sub_cat_id'])){$id = $param['sub_cat_id'];} else if(isset($post['id']->message) ) {$id = $post['id']->message;} else {$id = '';}

   		if($id > 0) { //  echo $id;die();
   			$q = $this->CI->food_application_model->get_food_locale('subcategory', $id);
   			$description = array();
   			$display_name = array();
   			foreach($q->result() as $row) {
   				$display_name[$row->lang_code] = $row->name;
   				$description[$row->lang_code] = $row->description;
   			}
   		}


   		$form = array();
   		$form['languages'] = $this->lang;
   		$form['form_open'] = form_open_multipart('food_category/submit', array('id=>form1'));
   		$form['form_close'] = form_close();
   		$form['cid'] = form_dropdown('maincategory_id', $this->category, $cid, 'id="edit-section" class="select"');
   		$form['name'] = form_input('name', $name,'id="edit-name" class="input" ');
   		//$form['helping_text'] = form_input('helping_text', $helping_text,'id="edit-helping-text" class="input" ');
   		$form['display_name'] = $this->locale_fields('display_name', 'text', $display_name);
   		//$form['description'] = $this->locale_fields('description', 'textarea', $description);
   		$form['cancel'] = 'food_category/subcategory/'.$cid;
   		$images = array();
   		if($id > 0) {
   			$q = $this->CI->food_application_model->get_food_images('subcategory', $id);
   			foreach($q->result() as $row) {
   				$images[$row->aid] = $row->image_name;
   			}
   		}

   		$form['images'] = $this->draw_image_fields($images);

   		if($id > 0) {
   			$form['id'] = form_hidden('id', $id);
   			$form['form_id'] = form_hidden('form_id', 'edit_subcategory');
   			$form['submit'] = form_submit('submit', 'Update');
   		}
   		else {
   			$form['form_id'] = form_hidden('form_id', 'add_subcategory');
   			$form['submit'] = form_submit('submit', 'Add');
   		}

   		return $form;
   	}

   	public function sub_category_validate($param = array()) {
   		$cid = $param['maincategory_id'];
   		$name = $param['name'];
   		//$description = $param['description'];
   		$error = FALSE;

   		//       if($cid == "") {
   		//           $error = TRUE;
   		//           $this->CI->message->set('Category is required field.', 'error', TRUE);
   		//       }

   		//         foreach($this->lang as $c=>$val) {
   		//            if($name[$c] == "") {
   		//                $this->CI->message->set('Subcategory name for '.$val.' is required field.', 'error', TRUE);
   		//                $error = TRUE;
   		//            }
   		//          }

   		if($cid > 0) {
   			$q = $this->CI->common_model->get_record_by_condition('food_subcategory', 'name="'.$name['en'].'" AND sub_cat_id NOT IN('.$cid.')');
   		}
   		else {
   			$q = $this->CI->common_model->get_record_by_condition('food_subcategory', 'name="'.$name['en'].'"');
   		}

   		//          if($q->num_rows() > 0) {
   		//             $this->CI->message->set('Subcategory name is already exist.', 'error', TRUE);
   		//             $error = TRUE;
   		//          }

   		//          foreach($this->lang as $c=>$val) {
   		//            if($description[$c] == "") {
   		//                $this->CI->message->set('Subcategory description for '.$val.' is required field.', 'error', TRUE);
   		//                $error = TRUE;
   		//            }
   		//          }
   		 
   		return $error;
   		}


   		public function menuitem_form($param = array(),$time=array()) {
   			$post = $this->CI->get_last_post->get();
   			//echo "<pre>";  print_r($post); die();
   			//print_r($time);die();
   			if(isset($param['name'])){$name = $param['name'];} else if(isset($post['name']->message) ) {$name = $post['name']->message;} else {$name = '';}
   			if(isset($param['menuitem_number'])){$number = $param['menuitem_number'];} else if(isset($post['number']->message) ) {$number = $post['number']->message;} else {$number = '';}
   			if(isset($param['rvc_number'])){$rvc_id = $param['rvc_number'];} else if(isset($post['rvc_id']->message) ) {$rvc_id = $post['rvc_id']->message;} else{$rvc_id = '';}
   			if(isset($param['price'])){$price = $param['price'];} else if(isset($post['price']->message) ) {$price = $post['price']->message;} else {$price = '';}
   			if(isset($param['size'])){$size = $param['size'];} else if(isset($post['size']->message) ) {$size = $post['size']->message;} else {$size = '';}
   			if(isset($param['position'])){$position = $param['position'];} else if(isset($post['position']->message) ) {$position = $post['position']->message;} else {$position = '';}
   			if(isset($param['uom'])){$uom = $param['uom'];} else if(isset($post['uom']->message) ) {$uom = $post['uom']->message;} else {$uom = 'NULL';}
   			if(isset($param['size_price'])){$size_price = $param['size_price'];} else if(isset($post['size_price']->message) ) {$size_price = $post['size_price']->message;} else {$size_price = array();}
   			if(isset($param['has_addons'])){$has_addons = $param['has_addons'];} else if(isset($post['has_addons']->message) ) {$has_addons = $post['has_addons']->message;} else {$has_addons = '';}
   			if(isset($param['time1'])){$time1 = $param['time1'];}  else {$time1 = '';}

   			if(isset($param['delivery_time'])){$delivery_time = $param['delivery_time'];} else if(isset($post['delivery_time']->message) ) {$delivery_time = $post['delivery_time']->message;} else {$delivery_time = '';}



   			if(isset($param['addon_id'])){$addon_id = $param['addon_id'];} else if(isset($post['addon_id']->message) ) {$addon_id = $post['addon_id']->message;} else {$addon_id = array();}
   			if(isset($param['allowed_addon'])){$allowed_addon = $param['allowed_addon'];} else if(isset($post['allowed_addon']->message) ) {$allowed_addon = $post['allowed_addon']->message;} else {$allowed_addon = array();}
   			if(isset($param['section_id'])){$sid = $param['section_id'];} else if(isset($post['section_id']->message) ) {$sid = $post['section_id']->message;} else {$sid = '';}
   			if(isset($param['maincategory_id'])){$cid = $param['maincategory_id'];} else if(isset($post['maincategory_id']->message) ) {$cid = $post['maincategory_id']->message;} else {$cid = '';}
   			if(isset($param['sub_cat_id'])){$sub_cat_id = $param['sub_cat_id'];} else if(isset($post['sub_cat_id']->message) ) {$sub_cat_id = $post['sub_cat_id']->message;} else {$sub_cat_id = '';}
   			if(isset($param['is_breakfast'])){$has_breakfast = $param['is_breakfast'];} else if(isset($post['has_breakfast']->message) ) {$has_breakfast = $post['has_breakfast']->message;} else {$has_breakfast = '';}

   			if(isset($param['is_special'])){$special = $param['is_special'];} else if(isset($post['special']->message) ) {$special = $post['special']->message;} else {$special = '';}
   			if(isset($param['is_porch'])){$porch = $param['is_porch'];} else if(isset($post['porch']->message) ) {$porch = $post['porch']->message;} else {$porch = '';}

   			if(isset($time)){$time = $time;}  else {$time = '';}


   			if(isset($param['is_active'])){$active = $param['is_active'];} else if(isset($post['active']->message) ) {$active = $post['active']->message;} else {$active = '';}

   			if(isset($param['is_veg'])){$is_vag = $param['is_veg'];} else if(isset($post['is_vag']->message) ) {$is_vag = $post['is_vag']->message;} else {$is_vag = '';}

   			if(isset($param['show_veg_nonveg'])){$is_beverages = $param['show_veg_nonveg'];} else if(isset($post['is_beverages']->message) ) {$is_beverages = $post['is_beverages']->message;} else {$is_beverages = '';}

   			if(isset($param['spicy_level'])){$spicy_level = $param['spicy_level'];} else if(isset($post['spicy_level']->message) ) {$spicy_level = $post['spicy_level']->message;} else {$spicy_level = '';}
   			if(isset($param['required_addon'])){$required_addon = $param['required_addon'];} else if(isset($post['required_addon']->message) ) {$required_addon = $post['required_addon']->message;} else {$required_addon = '';}

   			if(isset($param['descriptive_name'])){$descriptive_name = $param['descriptive_name'];} else if(isset($post['descriptive_name']->message) ) {$descriptive_name = $post['descriptive_name']->message;} else {$descriptive_name = '';}
   			if(isset($param['synopsis'])){$synopsis = $param['synopsis'];} else if(isset($post['synopsis']->message) ) {$synopsis = $post['synopsis']->message;} else {$synopsis = '';}
   			if(isset($param['video'])){$video = $param['video'];} else if(isset($post['video']->message) ) {$video = $post['video']->message;} else {$video = '';}

   			if(isset($param['mid'])){$id = $param['mid'];} else if(isset($post['mid']->message) ) {$id = $post['mid']->message;} else {$id = '0';}



   			//echo "<pre>"; print_r($addon_id); die();
   			if(count($addon_id) < 1) {
   				$addon_id = $this->items;
   			}

   			if($id > 0) {
   				//$addon_id = unset();
   				unset($this->items[$id]);
   			}

   			$form = array();
   			$form['languages'] = $this->lang;
   			$form['form_open'] = form_open_multipart('menuitem/submit', 'onsubmit=" return submit_form()"', array('id'=>'form1'));
   			$form['form_close'] = form_close();

   			$images = array();
   			if($id > 0) {
   				$form['selected_video'] = $param['video'];
   				$q = $this->CI->food_application_model->get_food_images('menuitem', $id);
   				$i=1;
   				foreach($q->result() as $row) {
   					$images[$row->position] = $row->image_name;
   					$i++;
   				}

   				//echo "<pre>"; print_r($images); die();
   				$q = $this->CI->food_application_model->get_food_locale('menuitem', $id);
   				$description = array();
   				$synopsis = array();
   				foreach($q->result() as $row) {
   					$descriptive_name[$row->lang_code] = $row->name;
   					$synopsis[$row->lang_code] = $row->description;
   				}

   				$q = $this->CI->food_application_model->get_menuitem_price($id);
   				foreach($q->result() as $row) {
   					if($row->uom == "") $uom = "NULL";
   					else{$uom=$row->uom;}

   					$arrp = explode('.', $row->price);
   					$size_price[$row->size."-".$arrp[0]."-".$uom] = $row->size."-".$arrp[0]."-".$uom;
   					$price=$row->price;
   					$size=$row->size;
   				}

   			}

   			if($is_beverages==1)
   			$is_beverages=0;
   			else
   			$is_beverages=1;

   			if($id > 0 && $is_vag == 0) {
   				$is_vag = 0;
   			}
   			else {
   				$is_vag = 1;
   			}

   			$form['name'] = form_input('name', $name, 'id="edit-name" class="input" ');
   			$form['number'] = form_input('number', $number, 'id="edit-number" class="input" ');
   			$form['descriptive_name'] = $this->locale_fields('descriptive_name', 'text', $descriptive_name);
   			$form['synopsis'] = $this->locale_fields('synopsis', 'textarea', $synopsis);
   			$form['video'] = form_upload('video', $video, 'id="edit-video"class="file"  ');

   			$form['images'] = $this->draw_menuitem_image_fields($images);


   			$form['position'] = form_input('position', $position, 'id="edit-position" class="input" ');


   			$form['rvc'] = form_dropdown('rvc_id', $this->revenue, $rvc_id, 'id="edit-revenue" class="select" ');
   			//        $form['size'] = form_dropdown('size', array(''=>'--Select--', '1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10), $size, 'id="edit-size"');
   			$form['size'] = form_input('size',  $size, 'id="edit-size" size="5" maxlength="5" class="input" ');
   			$form['price'] = form_input('price', $price, 'id="edit-price" size="5" class="input" ');
   			$form['uom'] = form_input('uom', $uom, 'id="edit-uom" class="input" onclick="if(this.value == \'NULL\') this.value = \'\'" onblur="if(this.value == \'\') this.value = \'NULL\'"');
   			$form['size_price'] = form_dropdown('size_price[]', $size_price, $size_price, ' id="edit-size_price" multiple="true"  style="width:150px;"');
   			$form['add_button'] = form_button('add_price', 'Add Price', 'id="add_price"');
   			$form['has_addons'] = form_checkbox('has_addons', 1, $has_addons, 'id="edit-has_addons"   ');
   			$form['time1'] = form_radio('time', 1, $time, 'id="time" style="vertical-align: middle;margin-left:10px" ');
   			$form['time2'] = form_radio('time', 0, true, 'id="time2" style="vertical-align: middle"');
   			$form['addons'] = form_dropdown('addons[]', $this->addon_groups, $addon_id, 'id="edit-addns_id"  ');
   			$form['allowed_addons'] = form_dropdown('allowed_addons[]', $this->allowed_addon_group, $allowed_addon, 'id="edit-allowed-addns_id"  ');
   			$form['sid'] = form_dropdown('section_id', $this->sections, $sid, 'id="edit-sid" style="width:243px" class="select"');
   			$form['cid'] = form_dropdown('maincategory_id', $this->category, $cid, 'id="edit-cid" onchange="get_sub_cat(this.value)" style="width:243px" class="select" ');
   			$form['sub_cat_id'] = form_dropdown('sub_cat_id', $this->sub_category, $sub_cat_id, 'id="edit-subcat" style="width:243px" class="select" ');
   			$form['has_breakfast'] = form_checkbox('has_breakfast', 1, $has_breakfast, 'id="edit-breakfast" ');
   			$form['special'] = form_checkbox('special', 1, $special, 'id="edit-is_special" ');

   			$form['delivery_time']=form_dropdown('delivery_time',array(0=>'No Time',5=>'5 Minutes',10=>'10 Minutes',15=>'15 Minutes',20=>'20 Minutes',25=>'25 Minutes',30=>'30 Minutes',35=>'35 Minutes',40=>'40 Minutes',45=>'45 Minutes',50=>'50 Minutes',55=>'55 Minutes'),$delivery_time,'id="edit-delivery_time" class="select"');

   			$form['porch'] = form_checkbox('porch', 1, $porch, 'id="edit-is_porch" style="vertical-align: bottom;"  ');
   			$form['is_vag'] = form_checkbox('is_vag', '1', $is_vag );
   			$form['is_beverages'] = form_checkbox('is_beverages', '1', $is_beverages);
   			$form['spicy_level'] = form_dropdown('spicy_level', array(0=>'0', 1=>'1', 2=>'2', 3=>'3'), $spicy_level, 'id="edit-spicy"');
   			$form['time']=$time;

   			$form['from_time'] = form_dropdown('from_time[]', array(''=>'Time',0=>'00.00',1=>'01.00', 2=>'02.00', 3=>'03.00', 4=>'04.00', 5=>'05.00', 6=>'06.00', 7=>'07.00', 8=>'08.00', 9=>'09.00', 10=>'10.00', 11=>'11.00', 12=>'12.00', 13=>'13.00', 14=>'14.00', 15=>'15.00', 16=>'16.00', 17=>'17.00', 18=>'18.00', 19=>'19.00', 20=>'20.00', 21=>'21.00', 22=>'22.00', 23=>'23.00', 0=>'00.00'), 'id="from_time"');
   			$form['to_time'] = form_dropdown('to_time[]', array(''=>'Time',0=>'00.00',1=>'01.00', 2=>'02.00', 3=>'03.00', 4=>'04.00', 5=>'05.00', 6=>'06.00', 7=>'07.00', 8=>'08.00', 9=>'09.00', 10=>'10.00', 11=>'11.00', 12=>'12.00', 13=>'13.00', 14=>'14.00', 15=>'15.00', 16=>'16.00', 17=>'17.00', 18=>'18.00', 19=>'19.00', 20=>'20.00', 21=>'21.00', 22=>'22.00', 23=>'23.00', 0=>'00.00'), 'id="to_time"');
   			 
   			$form['required_addon'] = form_checkbox('required_addon', '1', $required_addon);
   			$form['active'] = form_dropdown('active', array(1=>'Yes', 0=>'No'), $active, 'id="edit-active" class="select" ');
   			$form['cancel'] = anchor('menuitem', 'Cancel');
   			$form['mid'] = $id;

   			if($id > 0) {
   				$form['id'] = form_hidden('mid', $id);
   				$form['form_id'] = form_hidden('form_id', 'edit_menuitem');
   				$form['submit'] = form_submit('submit', 'Update');
   			}
   			else {
   				$form['form_id'] = form_hidden('form_id', 'add_menuitem');
   				$form['submit'] = form_submit('submit', 'Add');
   			}

   			return $form;
   		}

   		public function menuitem_validate($param=array()) {
   			//echo "<pre>";  print_r($param); die();
   			$rvc_id = $param['rvc_id'];
   			$number = $param['number'];
   			$name = $param['name'];
   			if(isset($param['mid'])) {
   				$mid = $param['mid'];
   				if($number != "") {
   					if($mid > 0) {
   						$q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_number="'.$number.'" AND mid NOT IN('.$mid.')');
   					}
   					else {
   						$q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_number="'.$number.'"');
   					}

   					if($q->num_rows() > 0) {
   						$this->CI->message->set('Menuitem number is already exist.', 'error', TRUE);
   						$error = TRUE;
   					}
       }
   			}
   			$descriptive_name = $param['descriptive_name'];
   			$synopsis = $param['synopsis'];
   			$price = $param['price'];
   			if(isset($param['addons'])) {
   				$addons = $param['addons'];
   			}
   			if(isset($param['has_addons'])) {
   				$has_addons = $param['has_addons'];
   			}
   			else {
   				$has_addons = '';
   			}
   			 
   			//$addons = $param['addons'];
   			 
   			$error = FALSE;


      // if($rvc_id == "") {
   			//   $error = TRUE;
   			// $this->CI->message->set('Rvenue center is required field.', 'error', TRUE);
   			//  }

   			//       if($number == "") {
   			//           $error = TRUE;
   			//           $this->CI->message->set('Number is required field.', 'error', TRUE);
   			//       }

   			//       if($number != "") {
   			//           if($mid > 0) {
   			//                $q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_number="'.$number.'" AND mid NOT IN('.$mid.')');
   			//           }
   			//           else {
   			//                $q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_number="'.$number.'"');
   			//           }
   			//
   			//           if($q->num_rows() > 0) {
   			//                $this->CI->message->set('Menuitem number is already exist.', 'error', TRUE);
   			//                $error = TRUE;
   			//           }
   			//       }

   			//       if($name == "") {
   			//           $error = TRUE;
   			//           $this->CI->message->set('Name is required field.', 'error', TRUE);
   			//       }

   			//       if($name != "") {
   			//           if($mid > 0) {
   			//                $q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_name="'.$name.'" AND mid NOT IN('.$mid.')');
   			//           }
   			//           else {
   			//                $q = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'menuitem_name="'.$name.'"');
   			//           }
   			//
   			//           if($q->num_rows() > 0) {
   			//                $this->CI->message->set('Menuitem name is already exist.', 'error', TRUE);
   			//                $error = TRUE;
   			//           }
   			//       }

   			//       foreach($this->lang as $c=>$val) {
   			//         if($descriptive_name[$c] == "") {
   			//            $this->CI->message->set('Dispaly name for '.$val.' is required field.', 'error', TRUE);
   			//            $error = TRUE;
   			//         }
   			//       }

   			//       if(count($descriptive_name)  > 0) {
   			//           if($mid > 0) {
   			//                $q2 = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'display_name="'.$descriptive_name['en'].'" AND mid NOT IN('.$mid.')');
   			//           }
   			//           else {
   			//                $q2 = $this->CI->common_model->get_record_by_condition('food_menuitem_details', 'display_name="'.$descriptive_name['en'].'"');
   			//           }
   			//
   			//           if($q2->num_rows() > 0) {
   			//                $this->CI->message->set('Display name is already exist.', 'error', TRUE);
   			//                $error = TRUE;
   			//           }
   			//       }


   			//        foreach($this->lang as $c=>$val) {
   			//         if($synopsis[$c] == "") {
   			//            $this->CI->message->set('Synopsis for '.$val.' is required field.', 'error', TRUE);
   			//            $error = TRUE;
   				//       }
   				//       }

   				//       if($price == "") {
   				//           $error = TRUE;
   				//           $this->CI->message->set('Price is required field.', 'error', TRUE);
   				//       }

   				//       if($has_addons) {
   				//           if($addons == "") {
   				//               $error = TRUE;
   				//               $this->CI->message->set('Please select adons.', 'error', TRUE);
   				//           }
   				//       }
       return $error;
   				}

   				public function upload_files($files = array(), $type='image') {

   					$files_data = array();
   					if($type == 'image') {
   						$config['upload_path'] = BASE_PATH.FOOD_IMAGES;
   						$config['allowed_types'] = 'gif|png|jpg|jpeg|tiff';

   					}
   					else {
   						$config['upload_path'] = BASE_PATH.FOOD_VIDEOS;
   						$config['allowed_types'] = 'mov|mpeg|mp4|m4v|mpg';
   					}

   					$this->CI->upload->initialize($config);
   					foreach($files as $name=>$file) {

   						if(!$this->CI->upload->do_upload($name)) {
   							//$files_data[$name] = $this->CI->upload->display_errors();
   						}
   						else {
   							$files_data[$name] = $this->CI->upload->data();
   						}
   					}

   					return $files_data;
   				}

   				public function draw_menuitem_image_fields($images = array()) {
   					$fields = array();
   					if(count($this->applications) > 0) {
   						foreach($this->applications as $aid=>$name) {
   							 
   							for($i=0; $i<10; $i++) {
   								if(isset($images[$i])) {
   									$fields[$i] = array('field'=>form_upload('img',$i,'id="me" class="file"'), 'image'=>$images[$i],'id="me" class="file"', 'pos'=>$i);
   								}
   								else {
   									$fields[$i] = array('field'=>form_upload('img',$i,'id="me" class="file"'), 'image'=>'','id="me" class="file"');
   								}
   							}
   							 
   						}
   					}
   					 
   					return $fields;
   				}

   				public function draw_image_fields($images = array()) {
   					$fields = array();
   					if(count($this->applications) > 0) {
   						foreach($this->applications as $aid=>$name) {
   							if(isset($images[$aid])) {
   								$fields[$name] = array('field'=>form_upload('img',$aid,'id="md" class="file"'), 'image'=>$images[$aid]);
   							}
   							else {
   								$fields[$name] = array('field'=>form_upload('img',$aid,'id="md" class="file"'), 'image'=>'');
   							}
   						}
   					}

   					return $fields;
   				}


   				/**
   				 *  locale_fields($lang = array(), $field_name="name", $field_type="text")
   				 *  It returns fields for support multilanguage.
   				 *
   				 ***/
   				public function locale_fields($field_name="name", $field_type="text", $value=array()) {
   					$field_arr = array();
   					 
   					switch ($field_type) {
   						case "text":
   							foreach($this->lang as $c=>$val) {
   								if(isset($value[$c])) {
   									$field_arr[$c] = array('field'=>form_input($field_name."[".$c."]", $value[$c], 'id="edit-'.$c.'-'.$field_name.'" class="input"  size="20"'));
   								}
   								else {
   									$field_arr[$c] = array('field'=>form_input($field_name."[".$c."]", '', 'id="edit-'.$c.'-'.$field_name.'" class="input" size="20"'));
   								}
   							}
   							break;
   							 
   						case "textarea":
   							foreach($this->lang as $c=>$val) {
   								if(isset($value[$c])) {
   									$field_arr[$c] = array('field'=>form_textarea(array('name'=>$field_name."[".$c."]", 'rows'=>'5', 'cols'=>'40'), $value[$c], 'id="edit-'.$c.'-'.$field_name.'" style="resize:none"  class="input"'));
   								}
   								else {
   									$field_arr[$c] = array('field'=>form_textarea(array('name'=>$field_name."[".$c."]", 'rows'=>'5', 'cols'=>'40'), '', 'id="edit-'.$c.'-'.$field_name.'" style="resize:none"  class="input"'));
   								}
   							}
   							break;
   					}
   					 
   					return $field_arr;
   				}

   				public function prepare_locale_row($field, $type, $type_id) {
   					$row = array();
   					foreach($field['name'] as $k=>$l) {
   						$row[$k] = array('lang_code'=>$k, 'type'=>$type, 'type_id'=>$type_id, 'name'=>$l);
   					}

   					foreach($field['description'] as $k=>$l) {
   						if(isset($row[$k])) {
   							$row[$k]['description'] = $l;
   						}
   					}

   					return $row;
   				}

   				public function prepare_price_details($field, $mid) {
   					$row = array();
   					foreach($field as $val) {
   						$param = explode('-', $val);
   						$row[] = array('mid'=>$mid, 'price'=>$param[1], 'size'=>$param[0], 'uom'=>$param[2]);
   					}

   					return $row;
   				}

   				public function get_table_data($table, $key="", $val="", $condition="", $order_by="") {
   					$rows = array();
   					if($condition != "") {
   						$q = $this->CI->common_model->get_record_by_condition($table, $condition);
   					}
   					else {
   						$q = $this->CI->common_model->get_records($table, $order_by);
   					}

   					if($val != "") {
   						$rows = result_to_array($q->result(), $key, $val);
   					}
   					else {
   						foreach ($q->result() as $row) {
   							if($key != "") {
   								$rows[$row->$key] = $row;
   							} else {
   								$rows[] = $row;
   							}
   						}
   					}
   					return $rows;
   				}
   			}
   			?>
