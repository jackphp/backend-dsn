<html>
<head>
<title>Directory Browser</title>
<script src="jquery-1.4.4.js" type="text/javascript"></script>
<script src="jqueryFileTree.js" type="text/javascript"></script>
<link href="jqueryFileTree.css" rel="stylesheet" type="text/css"
	media="screen" />
<style type="text/css">
BODY,HTML {
	padding: 0px;
	margin: 0px;
}

BODY {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	background: #EEE;
	padding: 15px;
}

H1 {
	font-family: Georgia, serif;
	font-size: 20px;
	font-weight: normal;
}

H2 {
	font-family: Georgia, serif;
	font-size: 16px;
	font-weight: normal;
	margin: 0px 0px 10px 0px;
}

.example {
	float: left;
	margin: 15px;
}

.demo {
	width: 400px;
	height: 400px;
	border-top: solid 1px #BBB;
	border-left: solid 1px #BBB;
	border-bottom: solid 1px #FFF;
	border-right: solid 1px #FFF;
	background: #FFF;
	overflow: scroll;
	padding: 5px;
}

P.note {
	color: #999;
	clear: both;
}
</style>
<script language="javascript" type="text/javascript">
        $(document).ready( function() {
            var path = '<?php if(isset($_GET['dir']) && $_GET['dir'] == 'song') {echo "/media/";}else{echo "/media/";} ?>';
            $('#container_id').fileTree({
                root: '/media/',
                script: 'jqueryFileTree.php',
                expandSpeed: 500,
                collapseSpeed: 500,
                multiFolder: false
            }, function(file) {
                alert(file);
            });

        $("#ok").click(function(){
            if(opener.document.getElementById("import_location") != null) {
                opener.document.getElementById("import_location").value = $("#loc").val();
            }
            else {
                if($(opener.document.forms[0].export_location).length > 0) {
                    opener.document.forms[0].export_location.value = $("#loc").val();
                }
                else {
                    opener.document.forms[1].export_location.value = $("#loc").val();
                }
            }
            window.close();
        })

        });
        
        function get_location(loc) {
            $("#loc").val(loc);
        }

        
        </script>
</head>
<body>
<div class="example">
<div><b>Default Location: /media/</b></div>
<br />
<div id="container_id" class="demo"></div>
<div>&nbsp;</div>
<b>Selected Location:</b> <input type="text" name="loc" id="loc"
	size="29" /> <input type="button" name="button" value="OK" id="ok" /></div>
</body>
</html>

